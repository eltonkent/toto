package toto.util.collections.tree;

/**
 * Implements a <a
 * href="http://en.wikipedia.org/wiki/Red%E2%80%93black_tree">RedBlackTree</a>
 * 
 * 
 * @param <T>
 */
public class RedBlackTree<T extends Comparable<? super T>> implements Tree<T> {
	/**
	 * Construct the tree.
	 */
	public RedBlackTree() {
		nullNode = new RedBlackNode<T>(null);
		nullNode.left = nullNode.right = nullNode;
		header = new RedBlackNode<T>(null);
		header.left = header.right = nullNode;
	}

	/**
	 * Compare item and t.element, using compareTo, with caveat that if t is
	 * header, then item is always larger. This routine is called if is possible
	 * that t is header. If it is not possible for t to be header, use compareTo
	 * directly.
	 */
	private final int compare(final T item, final RedBlackNode<T> t) {
		if (t == header)
			return 1;
		else
			return item.compareTo(t.element);
	}

	/**
	 * Insert into the tree.
	 * 
	 * @param item
	 *            the item to insert.
	 * @throws DuplicateItemException
	 *             if item is already present.
	 */
	public void insert(final T item) {
		current = parent = grand = header;
		nullNode.element = item;

		while (compare(item, current) != 0) {
			great = grand;
			grand = parent;
			parent = current;
			current = compare(item, current) < 0 ? current.left : current.right;

			// Check if two red children; fix if so
			if (current.left.color == RED && current.right.color == RED)
				handleReorient(item);
		}

		// Insertion fails if already present
		if (current != nullNode)
			return;
		current = new RedBlackNode<T>(item, nullNode, nullNode);

		// Attach to parent
		if (compare(item, parent) < 0)
			parent.left = current;
		else
			parent.right = current;
		handleReorient(item);
	}

	/**
	 * Remove from the tree.
	 * 
	 * @param x
	 *            the item to remove.
	 * @throws UnsupportedOperationException
	 *             if called.
	 */
	public void remove(final T x) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Find the smallest item the tree.
	 * 
	 * @return the smallest item or null if empty.
	 */
	public T findMin() {
		if (isEmpty())
			return null;

		RedBlackNode<T> itr = header.right;

		while (itr.left != nullNode)
			itr = itr.left;

		return itr.element;
	}

	/**
	 * Find the largest item in the tree.
	 * 
	 * @return the largest item or null if empty.
	 */
	public T findMax() {
		if (isEmpty())
			return null;

		RedBlackNode<T> itr = header.right;

		while (itr.right != nullNode)
			itr = itr.right;

		return itr.element;
	}

	/**
	 * Find an item in the tree.
	 * 
	 * @param x
	 *            the item to search for.
	 * @return true if x is found; otherwise false.
	 */
	public boolean contains(final T x) {
		nullNode.element = x;
		current = header.right;

		for (;;) {
			if (x.compareTo(current.element) < 0)
				current = current.left;
			else if (x.compareTo(current.element) > 0)
				current = current.right;
			else if (current != nullNode)
				return true;
			else
				return false;
		}
	}

	/**
	 * Make the tree logically empty.
	 */
	public void clear() {
		header.right = nullNode;
	}

	/**
	 * Print the tree contents in sorted order.
	 */
	public String vizualize() {
		final StringBuilder builder = new StringBuilder();
		printTree(header.right, builder);
		return builder.toString();
	}

	/**
	 * Internal method to print a subtree in sorted order.
	 * 
	 * @param t
	 *            the node that roots the tree.
	 */
	private void printTree(final RedBlackNode<T> t, final StringBuilder builder) {
		if (t != nullNode) {
			printTree(t.left, builder);
			builder.append(t.element);
			builder.append("\n");
			printTree(t.right, builder);
		}
	}

	/**
	 * Test if the tree is logically empty.
	 * 
	 * @return true if empty, false otherwise.
	 */
	public boolean isEmpty() {
		return header.right == nullNode;
	}

	/**
	 * Internal routine that is called during an insertion if a node has two red
	 * children. Performs flip and rotations.
	 * 
	 * @param item
	 *            the item being inserted.
	 */
	private void handleReorient(final T item) {
		// Do the color flip
		current.color = RED;
		current.left.color = BLACK;
		current.right.color = BLACK;

		if (parent.color == RED) // Have to rotate
		{
			grand.color = RED;
			if ((compare(item, grand) < 0) != (compare(item, parent) < 0))
				parent = rotate(item, grand); // Start dbl rotate
			current = rotate(item, great);
			current.color = BLACK;
		}
		header.right.color = BLACK; // Make root black
	}

	/**
	 * Internal routine that performs a single or double rotation. Because the
	 * result is attached to the parent, there are four cases. Called by
	 * handleReorient.
	 * 
	 * @param item
	 *            the item in handleReorient.
	 * @param parent
	 *            the parent of the root of the rotated subtree.
	 * @return the root of the rotated subtree.
	 */
	private RedBlackNode<T> rotate(final T item, final RedBlackNode<T> parent) {
		if (compare(item, parent) < 0)
			return parent.left = compare(item, parent.left) < 0 ? rotateWithLeftChild(parent.left)
					: // LL
					rotateWithRightChild(parent.left); // LR
		else
			return parent.right = compare(item, parent.right) < 0 ? rotateWithLeftChild(parent.right)
					: // RL
					rotateWithRightChild(parent.right); // RR
	}

	/**
	 * Rotate binary tree node with left child.
	 */
	private static <AnyType> RedBlackNode<AnyType> rotateWithLeftChild(
			final RedBlackNode<AnyType> k2) {
		final RedBlackNode<AnyType> k1 = k2.left;
		k2.left = k1.right;
		k1.right = k2;
		return k1;
	}

	/**
	 * Rotate binary tree node with right child.
	 */
	private static <AnyType> RedBlackNode<AnyType> rotateWithRightChild(
			final RedBlackNode<AnyType> k1) {
		final RedBlackNode<AnyType> k2 = k1.right;
		k1.right = k2.left;
		k2.left = k1;
		return k2;
	}

	private static class RedBlackNode<AnyType> {
		// Constructors
		RedBlackNode(final AnyType theElement) {
			this(theElement, null, null);
		}

		RedBlackNode(final AnyType theElement, final RedBlackNode<AnyType> lt,
				final RedBlackNode<AnyType> rt) {
			element = theElement;
			left = lt;
			right = rt;
			color = RedBlackTree.BLACK;
		}

		AnyType element; // The data in the node
		RedBlackNode<AnyType> left; // Left child
		RedBlackNode<AnyType> right; // Right child
		int color; // Color
	}

	private final RedBlackNode<T> header;
	private final RedBlackNode<T> nullNode;

	private static final int BLACK = 1; // BLACK must be 1
	private static final int RED = 0;

	// Used in insert routine and its helpers
	private RedBlackNode<T> current;
	private RedBlackNode<T> parent;
	private RedBlackNode<T> grand;
	private RedBlackNode<T> great;

}