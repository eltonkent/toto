package toto.util.collections.tree;

public interface Tree<T> {

	/**
	 * Insert into the tree;
	 * 
	 * @param x
	 *            the item to insert.
	 */
	public void insert(final T x);

	/**
	 * Remove from the tree. Nothing is done if x is not found.
	 * 
	 * @param x
	 *            the item to remove.
	 */
	public void remove(final T x);

	/**
	 * Find the smallest item in the tree. returns null if its not found or if
	 * the tree is empty.
	 * 
	 * @return smallest item or null if empty.
	 */
	public T findMin();

	/**
	 * Find the largest item in the tree.returns null if its not found or if the
	 * tree is empty.
	 * 
	 * @return the largest item of null if empty.
	 */
	public T findMax();

	/**
	 * Find an item in the tree.
	 * 
	 * @param x
	 *            the item to search for.
	 * @return true if not found.
	 */
	public boolean contains(final T x);

	/**
	 * Make the tree logically empty.
	 */
	public void clear();

	/**
	 * returns the contents of the tree for vizualization.
	 */
	public String vizualize();
}
