package toto.util.collections.tree;

import toto.util.random.RandomPrimitives;

/**
 * Implements a <a href="http://en.wikipedia.org/wiki/Treap">Treap</a>
 * datastructure.
 * <p>
 * <div> </div>
 * </p>
 * 
 * @param <T>
 */
public class Treap<T extends Comparable<? super T>> implements Tree<T> {
	/**
	 * Construct the treap.
	 */
	public Treap() {
		nullNode = new TreapNode<T>(null);
		nullNode.left = nullNode.right = nullNode;
		nullNode.priority = Integer.MAX_VALUE;
		root = nullNode;
	}

	/**
	 * Insert into the tree. Does nothing if x is already present.
	 * 
	 * @param x
	 *            the item to insert.
	 */
	public void insert(final T x) {
		root = insert(x, root);
	}

	/**
	 * Remove from the tree. Does nothing if x is not found.
	 * 
	 * @param x
	 *            the item to remove.
	 */
	public void remove(final T x) {
		root = remove(x, root);
	}

	/**
	 * Find the smallest item in the tree.
	 * 
	 * @return the smallest item, or null.
	 */
	public T findMin() {
		if (isEmpty())
			return null;

		TreapNode<T> ptr = root;

		while (ptr.left != nullNode)
			ptr = ptr.left;

		return ptr.element;
	}

	/**
	 * Find the largest item in the tree.
	 * 
	 * @return the largest item, or null.
	 */
	public T findMax() {
		if (isEmpty())
			return null;

		TreapNode<T> ptr = root;

		while (ptr.right != nullNode)
			ptr = ptr.right;

		return ptr.element;
	}

	/**
	 * Find an item in the tree.
	 * 
	 * @param x
	 *            the item to search for.
	 * @return true if x is found.
	 */
	public boolean contains(final T x) {
		TreapNode<T> current = root;
		nullNode.element = x;

		for (;;) {
			final int compareResult = x.compareTo(current.element);

			if (compareResult < 0)
				current = current.left;
			else if (compareResult > 0)
				current = current.right;
			else
				return current != nullNode;
		}
	}

	/**
	 * Make the tree logically empty.
	 */
	public void clear() {
		root = nullNode;
	}

	/**
	 * Test if the tree is logically empty.
	 * 
	 * @return true if empty, false otherwise.
	 */
	public boolean isEmpty() {
		return root == nullNode;
	}

	/**
	 * Print the tree contents in sorted order.
	 */
	public String vizualize() {
		if (isEmpty())
			return toString();
		else {
			final StringBuilder b = new StringBuilder();
			printTree(root, b);
			return b.toString();
		}
	}

	/**
	 * Internal method to insert into a subtree.
	 * 
	 * @param x
	 *            the item to insert.
	 * @param t
	 *            the node that roots the subtree.
	 * @return the new root of the subtree.
	 */
	private TreapNode<T> insert(final T x, TreapNode<T> t) {
		if (t == nullNode)
			return new TreapNode<T>(x, nullNode, nullNode);

		final int compareResult = x.compareTo(t.element);

		if (compareResult < 0) {
			t.left = insert(x, t.left);
			if (t.left.priority < t.priority)
				t = rotateWithLeftChild(t);
		} else if (compareResult > 0) {
			t.right = insert(x, t.right);
			if (t.right.priority < t.priority)
				t = rotateWithRightChild(t);
		}
		// Otherwise, it's a duplicate; do nothing

		return t;
	}

	/**
	 * Internal method to remove from a subtree.
	 * 
	 * @param x
	 *            the item to remove.
	 * @param t
	 *            the node that roots the subtree.
	 * @return the new root of the subtree.
	 */
	private TreapNode<T> remove(final T x, TreapNode<T> t) {
		if (t != nullNode) {
			final int compareResult = x.compareTo(t.element);

			if (compareResult < 0)
				t.left = remove(x, t.left);
			else if (compareResult > 0)
				t.right = remove(x, t.right);
			else {
				// Match found
				if (t.left.priority < t.right.priority)
					t = rotateWithLeftChild(t);
				else
					t = rotateWithRightChild(t);

				if (t != nullNode) // Continue on down
					t = remove(x, t);
				else
					t.left = nullNode; // At a leaf
			}
		}
		return t;
	}

	/**
	 * Internal method to print a subtree in sorted order.
	 * 
	 * @param t
	 *            the node that roots the tree.
	 */
	private void printTree(final TreapNode<T> t, final StringBuilder b) {
		if (t != t.left) {
			printTree(t.left, b);
			b.append(t.element.toString());
			b.append("\n");
			printTree(t.right, b);
		}
	}

	/**
	 * Rotate binary tree node with left child.
	 */
	private TreapNode<T> rotateWithLeftChild(final TreapNode<T> k2) {
		final TreapNode<T> k1 = k2.left;
		k2.left = k1.right;
		k1.right = k2;
		return k1;
	}

	/**
	 * Rotate binary tree node with right child.
	 */
	private TreapNode<T> rotateWithRightChild(final TreapNode<T> k1) {
		final TreapNode<T> k2 = k1.right;
		k1.right = k2.left;
		k2.left = k1;
		return k2;
	}

	private static class TreapNode<AnyType> {
		// Constructors
		TreapNode(final AnyType theElement) {
			this(theElement, null, null);
		}

		TreapNode(final AnyType theElement, final TreapNode<AnyType> lt,
				final TreapNode<AnyType> rt) {
			element = theElement;
			left = lt;
			right = rt;
			priority = RandomPrimitives.randomInt();
		}

		// Friendly data; accessible by other package routines
		AnyType element; // The data in the node
		TreapNode<AnyType> left; // Left child
		TreapNode<AnyType> right; // Right child
		int priority; // Priority

	}

	private TreapNode<T> root;
	private final TreapNode<T> nullNode;

}