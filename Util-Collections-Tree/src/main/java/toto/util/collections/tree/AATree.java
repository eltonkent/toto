package toto.util.collections.tree;

/**
 * 
 * Implements a <a href="http://en.wikipedia.org/wiki/AA_tree">AA-tree</a>
 * <p>
 * <div>
 * <h3>Usage</h3>
 * AATrees are used to sort and retreiving ordered data efficiently. All classes
 * using the AA-Tree should implement the {@link Comparable} interface. </div>
 * </p>
 * 
 * @param <T>
 */
public class AATree<T extends Comparable<? super T>> implements Tree<T> {
	/**
	 * Construct the tree.
	 */
	public AATree() {
		nullNode = new AANode<T>(null, null, null);
		nullNode.left = nullNode.right = nullNode;
		nullNode.level = 0;
		root = nullNode;
	}

	/**
	 * Insert into the tree.
	 * 
	 * @param x
	 *            the item to insert.
	 */
	public void insert(final T x) {
		root = insert(x, root);
	}

	/**
	 * Remove from the tree.
	 * 
	 * @param x
	 *            the item to remove.
	 */
	public void remove(final T x) {
		deletedNode = nullNode;
		root = remove(x, root);
	}

	/**
	 * Find the smallest item in the tree.
	 * 
	 * @return the smallest item or null.
	 */
	public T findMin() {
		if (isEmpty())
			return null;

		AANode<T> ptr = root;

		while (ptr.left != nullNode)
			ptr = ptr.left;

		return ptr.element;
	}

	/**
	 * Find the largest item in the tree.
	 * 
	 * @return the largest item or null.
	 */
	public T findMax() {
		if (isEmpty())
			return null;

		AANode<T> ptr = root;

		while (ptr.right != nullNode)
			ptr = ptr.right;

		return ptr.element;
	}

	/**
	 * Find an item in the tree.
	 * 
	 * @param x
	 *            the item to search for.
	 * @return true if x is found.
	 */
	public boolean contains(final T x) {
		AANode<T> current = root;
		nullNode.element = x;

		for (;;) {
			final int compareResult = x.compareTo(current.element);

			if (compareResult < 0)
				current = current.left;
			else if (compareResult > 0)
				current = current.right;
			else if (current != nullNode)
				return true;
			else
				return false;
		}
	}

	/**
	 * Make the tree logically empty.
	 */
	public void clear() {
		root = nullNode;
	}

	/**
	 * Test if the tree is logically empty.
	 * 
	 * @return true if empty, false otherwise.
	 */
	public boolean isEmpty() {
		return root == nullNode;
	}

	/**
	 * Internal method to insert into a subtree.
	 * 
	 * @param x
	 *            the item to insert.
	 * @param t
	 *            the node that roots the subtree.
	 * @return the new root of the subtree.
	 */
	private AANode<T> insert(final T x, AANode<T> t) {
		if (t == nullNode)
			return new AANode<T>(x, nullNode, nullNode);

		final int compareResult = x.compareTo(t.element);

		if (compareResult < 0)
			t.left = insert(x, t.left);
		else if (compareResult > 0)
			t.right = insert(x, t.right);
		else
			return t;

		t = skew(t);
		t = split(t);
		return t;
	}

	/**
	 * Internal method to remove from a subtree.
	 * 
	 * @param x
	 *            the item to remove.
	 * @param t
	 *            the node that roots the subtree.
	 * @return the new root of the subtree.
	 */
	private AANode<T> remove(final T x, AANode<T> t) {
		if (t != nullNode) {
			// Step 1: Search down the tree and set lastNode and deletedNode
			lastNode = t;
			if (x.compareTo(t.element) < 0)
				t.left = remove(x, t.left);
			else {
				deletedNode = t;
				t.right = remove(x, t.right);
			}

			// Step 2: If at the bottom of the tree and
			// x is present, we remove it
			if (t == lastNode) {
				if (deletedNode == nullNode
						|| x.compareTo(deletedNode.element) != 0)
					return t; // Item not found; do nothing
				deletedNode.element = t.element;
				t = t.right;
			}

			// Step 3: Otherwise, we are not at the bottom; rebalance
			else if (t.left.level < t.level - 1 || t.right.level < t.level - 1) {
				if (t.right.level > --t.level)
					t.right.level = t.level;
				t = skew(t);
				t.right = skew(t.right);
				t.right.right = skew(t.right.right);
				t = split(t);
				t.right = split(t.right);
			}
		}
		return t;
	}

	/**
	 * Skew primitive for AA-trees.
	 * 
	 * @param t
	 *            the node that roots the tree.
	 * @return the new root after the rotation.
	 */
	private AANode<T> skew(AANode<T> t) {
		if (t.left.level == t.level)
			t = rotateWithLeftChild(t);
		return t;
	}

	/**
	 * Split primitive for AA-trees.
	 * 
	 * @param t
	 *            the node that roots the tree.
	 * @return the new root after the rotation.
	 */
	private AANode<T> split(AANode<T> t) {
		if (t.right.right.level == t.level) {
			t = rotateWithRightChild(t);
			t.level++;
		}
		return t;
	}

	/**
	 * Rotate binary tree node with left child.
	 */
	private AANode<T> rotateWithLeftChild(final AANode<T> k2) {
		final AANode<T> k1 = k2.left;
		k2.left = k1.right;
		k1.right = k2;
		return k1;
	}

	/**
	 * Rotate binary tree node with right child.
	 */
	private AANode<T> rotateWithRightChild(final AANode<T> k1) {
		final AANode<T> k2 = k1.right;
		k1.right = k2.left;
		k2.left = k1;
		return k2;
	}

	private static class AANode<T> {
		// Constructors
		AANode(final T theElement, final AANode<T> lt, final AANode<T> rt) {
			element = theElement;
			left = lt;
			right = rt;
			level = 1;
		}

		T element; // The data in the node
		AANode<T> left; // Left child
		AANode<T> right; // Right child
		int level; // Level
	}

	private AANode<T> root;
	private final AANode<T> nullNode;

	private AANode<T> deletedNode;
	private AANode<T> lastNode;

	@Override
	public String vizualize() {
		return toString();
	}
}
