package toto.util.collections.tree;

/**
 * Binary search tree implementation.
 * <p>
 * <div>
 * <h3>Usage</h3>
 * Implements an unbalanced binary search tree. All classes using the binary
 * search tree should implement the {@link Comparable} interface. </div>
 * </p>
 * 
 * 
 * @param <T>
 *            Tree type.
 */
public class BinarySearchTree<T extends Comparable<? super T>> implements
		Tree<T> {
	/**
	 * Construct the tree.
	 */
	public BinarySearchTree() {
		root = null;
	}

	/**
	 * Insert into the tree; duplicates are ignored.
	 * 
	 * @param x
	 *            the item to insert.
	 */
	public void insert(final T x) {
		root = insert(x, root);
	}

	/**
	 * Remove from the tree. Nothing is done if x is not found.
	 * 
	 * @param x
	 *            the item to remove.
	 */
	public void remove(final T x) {
		root = remove(x, root);
	}

	/**
	 * Find the smallest item in the tree. returns null if its not found or if
	 * the tree is empty.
	 * 
	 * @return smallest item or null if empty.
	 */
	public T findMin() {
		if (isEmpty())
			return null;
		return findMin(root).element;
	}

	/**
	 * Find the largest item in the tree.returns null if its not found or if the
	 * tree is empty.
	 * 
	 * @return the largest item of null if empty.
	 */
	public T findMax() {
		if (isEmpty())
			return null;
		return findMax(root).element;
	}

	/**
	 * Find an item in the tree.
	 * 
	 * @param x
	 *            the item to search for.
	 * @return true if not found.
	 */
	public boolean contains(final T x) {
		return contains(x, root);
	}

	/**
	 * Make the tree logically empty.
	 */
	public void clear() {
		root = null;
	}

	/**
	 * Test if the tree is logically empty.
	 * 
	 * @return true if empty, false otherwise.
	 */
	public boolean isEmpty() {
		return root == null;
	}

	/**
	 * Print the tree contents in sorted order.
	 */
	public String vizualize() {
		if (isEmpty())
			return toString();
		else {
			final StringBuilder b = new StringBuilder();
			printTree(root, b);
			return b.toString();
		}
	}

	/**
	 * Internal method to insert into a subtree.
	 * 
	 * @param x
	 *            the item to insert.
	 * @param t
	 *            the node that roots the subtree.
	 * @return the new root of the subtree.
	 */
	private BinaryNode<T> insert(final T x, final BinaryNode<T> t) {
		if (t == null)
			return new BinaryNode<T>(x, null, null);

		final int compareResult = x.compareTo(t.element);

		if (compareResult < 0)
			t.left = insert(x, t.left);
		else if (compareResult > 0)
			t.right = insert(x, t.right);
		else
			; // Duplicate; do nothing
		return t;
	}

	/**
	 * Internal method to remove from a subtree.
	 * 
	 * @param x
	 *            the item to remove.
	 * @param t
	 *            the node that roots the subtree.
	 * @return the new root of the subtree.
	 */
	private BinaryNode<T> remove(final T x, BinaryNode<T> t) {
		if (t == null)
			return t; // Item not found; do nothing

		final int compareResult = x.compareTo(t.element);

		if (compareResult < 0)
			t.left = remove(x, t.left);
		else if (compareResult > 0)
			t.right = remove(x, t.right);
		else if (t.left != null && t.right != null) // Two children
		{
			t.element = findMin(t.right).element;
			t.right = remove(t.element, t.right);
		} else
			t = (t.left != null) ? t.left : t.right;
		return t;
	}

	/**
	 * Internal method to find the smallest item in a subtree.
	 * 
	 * @param t
	 *            the node that roots the subtree.
	 * @return node containing the smallest item.
	 */
	private BinaryNode<T> findMin(final BinaryNode<T> t) {
		if (t == null)
			return null;
		else if (t.left == null)
			return t;
		return findMin(t.left);
	}

	/**
	 * Internal method to find the largest item in a subtree.
	 * 
	 * @param t
	 *            the node that roots the subtree.
	 * @return node containing the largest item.
	 */
	private BinaryNode<T> findMax(BinaryNode<T> t) {
		if (t != null)
			while (t.right != null)
				t = t.right;

		return t;
	}

	/**
	 * Internal method to find an item in a subtree.
	 * 
	 * @param x
	 *            is item to search for.
	 * @param t
	 *            the node that roots the subtree.
	 * @return node containing the matched item.
	 */
	private boolean contains(final T x, final BinaryNode<T> t) {
		if (t == null)
			return false;

		final int compareResult = x.compareTo(t.element);

		if (compareResult < 0)
			return contains(x, t.left);
		else if (compareResult > 0)
			return contains(x, t.right);
		else
			return true; // Match
	}

	/**
	 * Internal method to print a subtree in sorted order.
	 * 
	 * @param t
	 *            the node that roots the subtree.
	 */
	private void printTree(final BinaryNode<T> t, final StringBuilder b) {
		if (t != null) {

			printTree(t.left, b);
			b.append(t.element);
			b.append("\n");
			printTree(t.right, b);
		}
	}

	/**
	 * Internal method to compute height of a subtree.
	 * 
	 * @param t
	 *            the node that roots the subtree.
	 */
	private int height(final BinaryNode<T> t) {
		if (t == null)
			return -1;
		else
			return 1 + Math.max(height(t.left), height(t.right));
	}

	// Basic node stored in unbalanced binary search trees
	private static class BinaryNode<T> {
		// Constructors

		BinaryNode(final T theElement, final BinaryNode<T> lt,
				final BinaryNode<T> rt) {
			element = theElement;
			left = lt;
			right = rt;
		}

		T element; // The data in the node
		BinaryNode<T> left; // Left child
		BinaryNode<T> right; // Right child
	}

	/** The tree root. */
	private BinaryNode<T> root;

}