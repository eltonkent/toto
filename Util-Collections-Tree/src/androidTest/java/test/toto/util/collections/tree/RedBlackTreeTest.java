package test.toto.util.collections.tree;

import test.toto.ToToTestCase;
import toto.util.collections.tree.RedBlackTree;

public class RedBlackTreeTest extends ToToTestCase {

	public void testRedBlackTree() {
		final RedBlackTree<Integer> t = new RedBlackTree<Integer>();
		final int NUMS = 400000;
		final int GAP = 35461;

		System.out.println("Checking... (no more output means success)");

		for (int i = GAP; i != 0; i = (i + GAP) % NUMS)
			t.insert(i);

		if (t.findMin() != 1 || t.findMax() != NUMS - 1)
			System.out.println("FindMin or FindMax error!");

		for (int i = 1; i < NUMS; i++)
			if (!t.contains(i))
				System.out.println("Find error1!");
	}
}
