package test.toto.util.collections.tree;

import test.toto.ToToTestCase;
import toto.util.collections.tree.Treap;

public class TreapTest extends ToToTestCase {

	public void testTreap() {
		final Treap<Integer> t = new Treap<Integer>();
		final int NUMS = 40000;
		final int GAP = 307;

		System.out.println("Checking... (no bad output means success)");

		for (int i = GAP; i != 0; i = (i + GAP) % NUMS)
			t.insert(i);
		System.out.println("Inserts complete");

		for (int i = 1; i < NUMS; i += 2)
			t.remove(i);
		System.out.println("Removes complete");

		if (NUMS < 40)
			t.vizualize();
		if (t.findMin() != 2 || t.findMax() != NUMS - 2)
			System.out.println("FindMin or FindMax error!");

		for (int i = 2; i < NUMS; i += 2)
			if (!t.contains(i))
				System.out.println("Error: find fails for " + i);

		for (int i = 1; i < NUMS; i += 2)
			if (t.contains(i))
				System.out.println("Error: Found deleted item " + i);
	}
}
