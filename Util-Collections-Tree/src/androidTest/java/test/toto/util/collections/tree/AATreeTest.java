package test.toto.util.collections.tree;

import test.toto.ToToTestCase;
import toto.util.collections.tree.AATree;

public class AATreeTest extends ToToTestCase {

	public void testAATree() {
		final AATree<Integer> t = new AATree<Integer>();
		final int NUMS = 40000;
		final int GAP = 307;

		System.out.println("Checking... (no bad output means success)");

		t.insert(NUMS * 2);
		t.insert(NUMS * 3);
		for (int i = GAP; i != 0; i = (i + GAP) % NUMS)
			t.insert(i);
		System.out.println("Inserts complete");

		t.remove(t.findMax());
		for (int i = 1; i < NUMS; i += 2)
			t.remove(i);
		t.remove(t.findMax());
		System.out.println("Removes complete");

		if (t.findMin() != 2 || t.findMax() != NUMS - 2)
			System.out.println("FindMin or FindMax error!");

		for (int i = 2; i < NUMS; i += 2)
			if (!t.contains(i))
				System.out.println("Error: find fails for " + i);

		for (int i = 1; i < NUMS; i += 2)
			if (t.contains(i))
				System.out.println("Error: Found deleted item " + i);
	}

}
