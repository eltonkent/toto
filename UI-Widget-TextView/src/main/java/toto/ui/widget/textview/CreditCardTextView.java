/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.textview;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import toto.ui.widget.textview.EditTexts.DigitsLengthFilter;
import toto.ui.widget.textview.EditTexts.DigitsLengthFilter.MaxDigitsGetter;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.AttributeSet;

/**
 * An EditText that formats text like credit card numbers
 * 
 * @author elton.stephen.kent
 * 
 */
class CreditCardTextView extends
		toto.ui.widget.textview.EditTexts.FormattedEditText<String> {
	public static final String AMEX_CREDIT = "AMEX_CREDIT";
	public static final String AMEX_DEBIT = "AMEX_DEBIT";

	public static final String BANK_ACCOUNT = "BANK_ACCOUNT";
	private static Map<Integer, String> cardTypes = new HashMap<Integer, String>();
	public static final String CREDIT_CARD = "_CREDIT";
	public static final String DEBIT_CARD = "_DEBIT";
	public static final String DISCOVER_CREDIT = "DISCOVER_CREDIT";
	public static final String DISCOVER_DEBIT = "DISCOVER_DEBIT";
	private static final int LENGTH = 16;
	private static final int LENGTH_AMEX = 15;
	public static final String MASTERCARD_CREDIT = "MASTERCARD_CREDIT";
	public static final String MASTERCARD_DEBIT = "MASTERCARD_DEBIT";

	public static final String VISA_CREDIT = "VISA_CREDIT";

	public static final String VISA_DEBIT = "VISA_DEBIT";
	static {
		cardTypes.put(4, VISA_CREDIT);
		cardTypes.put(3, AMEX_CREDIT);
		cardTypes.put(6011, DISCOVER_CREDIT);
		cardTypes.put(5, MASTERCARD_CREDIT);
	}

	public static String getMaskedLast4(final String last4) {
		return "**** **** **** " + last4;
	}

	private String source = "";

	public CreditCardTextView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}

	public CreditCardTextView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void doCustomize(final Context context) {
		setInputType(InputType.TYPE_CLASS_PHONE);
		setFilters(new InputFilter[] { new DigitsLengthFilter(
				new MaxDigitsGetter() {
					@Override
					public int getMax(final CharSequence source) {
						return getMaxLength();
					}
				}) {
			@Override
			protected String getFormattedDigits(final Spanned dest) {
				return source;
			}
		} });
		// setHint("Card Number");
		// setHint("xxxx xxxx xxxx xxxx");
		// setTag("Card Number");
		super.doCustomize(context);
		addTextChangedListener(new FormattedTextWatcher() {
			@Override
			public void onTextChanged(final CharSequence arg0, final int arg1,
					final int arg2, final int arg3) {
				final int sourceIndex = arg0.toString().replace(" ", "")
						.length();
				if (arg3 == 1 && arg2 == 0) {
					source = source.subSequence(0, sourceIndex - 1).toString()
							+ arg0.subSequence(arg0.length() - 1, arg0.length());
				} else if (arg3 == 0 && arg2 == 1) {
					source = source.subSequence(0, sourceIndex).toString();
				}
			}
		});
	}

	private String doObscure(final String formatted) {
		final char[] chars = formatted.toCharArray();
		final int trailing = (getCardType() != null && getCardType()
				.equalsIgnoreCase(AMEX_CREDIT)) ? 3 : 4;

		for (int i = formatted.length(); i > 0; i--) {
			final char c = chars[i - 1];
			if (c != ' ' && i < getMaxLength(true, true) - trailing
					&& i < formatted.length())
				chars[i - 1] = '*';
		}
		return new String(chars);
	}

	@Override
	protected void format(final Editable arg0) {
		final CharSequence dollars = doObscure(getFormatted(getValue()));
		replaceText(arg0, dollars);
	}

	public String getCardType() {
		if (getValue().length() == 0)
			return null;
		final String actual = getValue();
		String digit = actual.substring(0, 1);
		if (actual.length() > 0) {
			try {
				if (digit.equals("6") && actual.length() > 3) {
					digit = actual.substring(0, 4);
				} else {
					digit = actual.substring(0, 1);
				}
				return cardTypes.get(Integer.parseInt(digit));
			} catch (final Exception e) {
				return null;
			}
		} else
			return null;
	}

	@Override
	protected String getFormatted(final String typedText) {
		String regex = "(\\d{0,4})(\\d{0,4})(\\d{0,4})(\\d{0,4})";
		String replace = "$1 $2 $3 $4";
		if (getCardType() != null
				&& getCardType().equalsIgnoreCase(AMEX_CREDIT)) {
			regex = "(\\d{0,4})(\\d{0,6})(\\d{0,5})";
			replace = "$1 $2 $3";
		}
		final Matcher matcher = Pattern.compile(regex).matcher(typedText);
		return matcher.replaceFirst(replace).trim();
	}

	protected int getMaxLength() {
		return getMaxLength(false, true);
	}

	protected int getMaxLength(final boolean padded, final boolean isCredit) {
		if (isCredit && getCardType() != null
				&& getCardType().equalsIgnoreCase(AMEX_CREDIT))
			return LENGTH_AMEX + (padded ? 2 : 0);
		else
			return LENGTH + (padded ? 3 : 0);
	}

	@Override
	public String getValue() {
		return source;
	}

	@Override
	protected void setFocus() {
		String text = getText().toString();
		text = Pattern.compile("[\\s]+$").matcher(text).replaceFirst("");
		setSelection(text.length());
	}

	@Override
	public boolean validate() {
		return getValue().length() == getMaxLength();
	}

	public boolean validate(final boolean isCredit) {
		return getValue().length() == getMaxLength(false, isCredit)
				&& getValue().matches("\\d{13,16}");// check if the card number
													// is all numeric
	}

	public boolean validateCardType(final boolean isCredit) {
		final String cardType = getCardType();
		if (cardType == null)
			return false;
		if (!isCredit)
			return cardType.equalsIgnoreCase(VISA_CREDIT)
					|| cardType.equalsIgnoreCase(MASTERCARD_CREDIT);
		return true;
	}
}
