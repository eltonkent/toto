/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.textview;

import java.util.regex.Pattern;

import toto.ui.widget.textview.EditTexts.CustomEditText;
import android.content.Context;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * EditText that formats the text entered like phone numbers
 * 
 * @author elton.stephen.kent
 * 
 */
class PhoneTextView extends CustomEditText<String> {
	private class LeadingOnesLengthFilter extends EditTexts.DigitsLengthFilter {

		public LeadingOnesLengthFilter(final int max) {
			super(max);
		}

		@Override
		protected String getFormattedDigits(final Spanned dest) {
			return getValue();
		}

		@Override
		protected boolean isEdgeCase(final CharSequence source) {
			return "".equals(EditTexts.extractNumber(source.toString()));
		}
	}

	private static final int LENGTH = 10;

	protected static String extractWithoutLeadingOne(final TextView tv) {
		final String number = EditTexts.extractDigits(tv);
		return Pattern.compile("^1").matcher(number).replaceFirst("");
	}

	public PhoneTextView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}

	public PhoneTextView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void doCustomize(final Context context) {
		setInputType(InputType.TYPE_CLASS_PHONE);
		// setTag("Phone");
		// setHint("Phone #");
		setFilters(new InputFilter[] { new LeadingOnesLengthFilter(LENGTH) });
		addTextChangedListener(new PhoneNumberFormattingTextWatcher());
	}

	@Override
	public String getValue() {
		return extractWithoutLeadingOne(this);
	}

	@Override
	public boolean validate() {
		return LENGTH == getValue().length();
	}
}
