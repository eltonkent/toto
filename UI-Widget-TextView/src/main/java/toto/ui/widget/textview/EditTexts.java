/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.textview;

import java.util.regex.Pattern;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

class EditTexts {
	static abstract class CustomEditText<T> extends EditText {
		public CustomEditText(final Context context, final AttributeSet attrs) {
			super(context, attrs);
			doCustomizeInner(context);
		}

		public CustomEditText(final Context context, final AttributeSet attrs,
				final int defStyle) {
			super(context, attrs, defStyle);
			doCustomizeInner(context);
		}

		protected abstract void doCustomize(Context context);

		private void doCustomizeInner(final Context context) {
			// TODO setKey the hint color
			// setHintTextColor(0x000000);
			doCustomize(context);
		}

		protected String extractNumber() {
			return EditTexts.extractDigits(this);
		}

		public abstract T getValue();

		public abstract boolean validate();
	}

	static class DigitsLengthFilter implements InputFilter {
		protected interface MaxDigitsGetter {
			int getMax(CharSequence source);
		}

		private MaxDigitsGetter mMaxGetter;

		public DigitsLengthFilter(final int max) {
			mMaxGetter = new MaxDigitsGetter() {
				@Override
				public int getMax(final CharSequence source) {
					return max;
				}
			};
		}

		public DigitsLengthFilter(final MaxDigitsGetter getter) {
			mMaxGetter = getter;
		}

		@Override
		public CharSequence filter(final CharSequence source, final int start,
				final int end, final Spanned dest, final int dstart,
				final int dend) {
			final int keep = mMaxGetter.getMax(source)
					- (getFormattedDigits(dest).length() - (dend - dstart));

			if (keep == 0 && isEdgeCase(source))
				return null;
			if (keep <= 0) {
				return "";
			} else if (keep >= end - start) {
				return null; // keep original
			} else {
				return source.subSequence(start, start + keep);
			}
		}

		protected String getFormattedDigits(final Spanned dest) {
			return extractNumber(dest.toString());
		}

		protected boolean isEdgeCase(final CharSequence source) {
			return false;
		}
	}

	static abstract class FormattedEditText<T> extends CustomEditText<T> {
		protected class FormattedTextWatcher implements TextWatcher {
			private boolean editing = false;

			@Override
			public synchronized void afterTextChanged(final Editable arg0) {
				if (!editing) {
					editing = true;
					format(arg0);
				}
				setFocus();
				editing = false;
			}

			@Override
			public void beforeTextChanged(final CharSequence arg0,
					final int arg1, final int arg2, final int arg3) {
			}

			@Override
			public void onTextChanged(final CharSequence arg0, final int arg1,
					final int arg2, final int arg3) {
			}
		}

		FormattedEditText(final Context context, final AttributeSet attrs) {
			super(context, attrs);
		}

		FormattedEditText(final Context context, final AttributeSet attrs,
				final int defStyle) {
			super(context, attrs, defStyle);
		}

		@Override
		protected void doCustomize(final Context context) {
			addTextChangedListener(new FormattedTextWatcher());
		}

		protected void format(final Editable arg0) {
			final CharSequence dollars = getFormatted(getValue());
			replaceText(arg0, dollars);
		}

		protected abstract CharSequence getFormatted(T typedText);

		protected void replaceText(final Editable arg0,
				final CharSequence dollars) {
			if (!dollars.equals(getText().toString()))
				arg0.replace(0, arg0.length(), dollars);
		}

		protected void setFocus() {
		}
	}

	static class NumericFilter implements InputFilter {

		@Override
		public CharSequence filter(final CharSequence arg0, final int arg1,
				final int arg2, final Spanned arg3, final int arg4,
				final int arg5) {
			return extractNumber(arg0.toString());
		}
	}

	static String extractDigits(final TextView tv) {
		return extractNumber(tv.getText().toString());
	}

	static String extractNumber(final String tv) {
		final String expression = "\\D";
		final Pattern pattern = Pattern.compile(expression,
				Pattern.CASE_INSENSITIVE);
		return pattern.matcher(tv).replaceAll("");
	}
}
