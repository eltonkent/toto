/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.textview;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Pair;
import android.widget.TextView;

import com.mi.toto.ui.textview.R;

/**
 * TextView that has shadow(inner,outer), fg/bg color, stroke(width,miter)
 * customizations
 * 
 * @author elton.kent
 * 
 */
public class FancyTextView extends TextView {
	private ArrayList<Shadow> outerShadows;
	private ArrayList<Shadow> innerShadows;

	private WeakHashMap<String, Pair<Canvas, Bitmap>> canvasStore;

	private Canvas tempCanvas;
	private Bitmap tempBitmap;

	private Drawable foregroundDrawable;

	private float strokeWidth;
	private Integer strokeColor;
	private Join strokeJoin;
	private float strokeMiter;

	private int[] lockedCompoundPadding;
	private boolean frozen = false;

	public FancyTextView(final Context context) {
		super(context);
		init(null);
	}

	public FancyTextView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public FancyTextView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public void init(final AttributeSet attrs) {
		outerShadows = new ArrayList<Shadow>();
		innerShadows = new ArrayList<Shadow>();
		if (canvasStore == null) {
			canvasStore = new WeakHashMap<String, Pair<Canvas, Bitmap>>();
		}

		if (attrs != null) {
			final TypedArray a = getContext().obtainStyledAttributes(attrs,
					R.styleable.FancyTextView);

			final String typefaceName = a
					.getString(R.styleable.FancyTextView_typeface);
			if (typefaceName != null) {
				final Typeface tf = Typeface.createFromAsset(getContext()
						.getAssets(), String.format("fonts/%s.ttf",
						typefaceName));
				setTypeface(tf);
			}

			if (a.hasValue(R.styleable.FancyTextView_foreground)) {
				final Drawable foreground = a
						.getDrawable(R.styleable.FancyTextView_foreground);
				if (foreground != null) {
					this.setForegroundDrawable(foreground);
				} else {
					this.setTextColor(a.getColor(
							R.styleable.FancyTextView_foreground, 0xff000000));
				}
			}

			if (a.hasValue(R.styleable.FancyTextView_background)) {
				final Drawable background = a
						.getDrawable(R.styleable.FancyTextView_background);
				if (background != null) {
					this.setBackgroundDrawable(background);
				} else {
					this.setBackgroundColor(a.getColor(
							R.styleable.FancyTextView_background, 0xff000000));
				}
			}

			if (a.hasValue(R.styleable.FancyTextView_innerShadow_color)) {
				this.addInnerShadow(
						a.getFloat(
								R.styleable.FancyTextView_innerShadow_radius, 0),
						a.getFloat(R.styleable.FancyTextView_innerShadow_dx, 0),
						a.getFloat(R.styleable.FancyTextView_innerShadow_dy, 0),
						a.getColor(R.styleable.FancyTextView_innerShadow_color,
								0xff000000));
			}

			if (a.hasValue(R.styleable.FancyTextView_outerShadow_color)) {
				this.addOuterShadow(
						a.getFloat(
								R.styleable.FancyTextView_outerShadow_radius, 0),
						a.getFloat(R.styleable.FancyTextView_outerShadow_dx, 0),
						a.getFloat(R.styleable.FancyTextView_outerShadow_dy, 0),
						a.getColor(R.styleable.FancyTextView_outerShadow_color,
								0xff000000));
			}

			if (a.hasValue(R.styleable.FancyTextView_stroke_color)) {
				final float strokeWidth = a.getFloat(
						R.styleable.FancyTextView_stroke_width, 1);
				final int strokeColor = a.getColor(
						R.styleable.FancyTextView_stroke_color, 0xff000000);
				final float strokeMiter = a.getFloat(
						R.styleable.FancyTextView_stroke_miter, 10);
				Join strokeJoin = null;
				switch (a.getInt(R.styleable.FancyTextView_stroke_joinStyle, 0)) {
				case (0):
					strokeJoin = Join.MITER;
					break;
				case (1):
					strokeJoin = Join.BEVEL;
					break;
				case (2):
					strokeJoin = Join.ROUND;
					break;
				}
				this.setStroke(strokeWidth, strokeColor, strokeJoin,
						strokeMiter);
			}
		}
	}

	public void setStroke(final float width, final int color, final Join join,
			final float miter) {
		strokeWidth = width;
		strokeColor = color;
		strokeJoin = join;
		strokeMiter = miter;
	}

	public void setStroke(final float width, final int color) {
		setStroke(width, color, Join.MITER, 10);
	}

	public void addOuterShadow(float r, final float dx, final float dy,
			final int color) {
		if (r == 0) {
			r = 0.0001f;
		}
		outerShadows.add(new Shadow(r, dx, dy, color));
	}

	public void addInnerShadow(float r, final float dx, final float dy,
			final int color) {
		if (r == 0) {
			r = 0.0001f;
		}
		innerShadows.add(new Shadow(r, dx, dy, color));
	}

	public void clearInnerShadows() {
		innerShadows.clear();
	}

	public void clearOuterShadows() {
		outerShadows.clear();
	}

	public void setForegroundDrawable(final Drawable d) {
		this.foregroundDrawable = d;
	}

	public Drawable getForeground() {
		return this.foregroundDrawable == null ? this.foregroundDrawable
				: new ColorDrawable(this.getCurrentTextColor());
	}

	@Override
	public void onDraw(final Canvas canvas) {
		super.onDraw(canvas);

		freeze();
		final Drawable restoreBackground = this.getBackground();
		final Drawable[] restoreDrawables = this.getCompoundDrawables();
		final int restoreColor = this.getCurrentTextColor();

		this.setCompoundDrawables(null, null, null, null);

		for (final Shadow shadow : outerShadows) {
			this.setShadowLayer(shadow.r, shadow.dx, shadow.dy, shadow.color);
			super.onDraw(canvas);
		}
		this.setShadowLayer(0, 0, 0, 0);
		this.setTextColor(restoreColor);

		if (this.foregroundDrawable != null
				&& this.foregroundDrawable instanceof BitmapDrawable) {
			generateTempCanvas();
			super.onDraw(tempCanvas);
			final Paint paint = ((BitmapDrawable) this.foregroundDrawable)
					.getPaint();
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
			this.foregroundDrawable.setBounds(canvas.getClipBounds());
			this.foregroundDrawable.draw(tempCanvas);
			canvas.drawBitmap(tempBitmap, 0, 0, null);
			tempCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
		}

		if (strokeColor != null) {
			final TextPaint paint = this.getPaint();
			paint.setStyle(Style.STROKE);
			paint.setStrokeJoin(strokeJoin);
			paint.setStrokeMiter(strokeMiter);
			this.setTextColor(strokeColor);
			paint.setStrokeWidth(strokeWidth);
			super.onDraw(canvas);
			paint.setStyle(Style.FILL);
			this.setTextColor(restoreColor);
		}
		if (innerShadows.size() > 0) {
			generateTempCanvas();
			final TextPaint paint = this.getPaint();
			for (final Shadow shadow : innerShadows) {
				this.setTextColor(shadow.color);
				super.onDraw(tempCanvas);
				this.setTextColor(0xFF000000);
				paint.setXfermode(new PorterDuffXfermode(
						PorterDuff.Mode.DST_OUT));
				paint.setMaskFilter(new BlurMaskFilter(shadow.r,
						BlurMaskFilter.Blur.NORMAL));

				tempCanvas.save();
				tempCanvas.translate(shadow.dx, shadow.dy);
				super.onDraw(tempCanvas);
				tempCanvas.restore();
				canvas.drawBitmap(tempBitmap, 0, 0, null);
				tempCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

				paint.setXfermode(null);
				paint.setMaskFilter(null);
				this.setTextColor(restoreColor);
				this.setShadowLayer(0, 0, 0, 0);
			}
		}

		if (restoreDrawables != null) {
			this.setCompoundDrawablesWithIntrinsicBounds(restoreDrawables[0],
					restoreDrawables[1], restoreDrawables[2],
					restoreDrawables[3]);
		}
		this.setBackgroundDrawable(restoreBackground);
		this.setTextColor(restoreColor);

		unfreeze();
	}

	private void generateTempCanvas() {
		final String key = String.format("%dx%d", getWidth(), getHeight());
		final Pair<Canvas, Bitmap> stored = canvasStore.get(key);
		if (stored != null) {
			tempCanvas = stored.first;
			tempBitmap = stored.second;
		} else {
			tempCanvas = new Canvas();
			tempBitmap = Bitmap.createBitmap(getWidth(), getHeight(),
					Bitmap.Config.ARGB_8888);
			tempCanvas.setBitmap(tempBitmap);
			canvasStore.put(key, new Pair<Canvas, Bitmap>(tempCanvas,
					tempBitmap));
		}
	}

	// Keep these things locked while onDraw in processing
	public void freeze() {
		lockedCompoundPadding = new int[] { getCompoundPaddingLeft(),
				getCompoundPaddingRight(), getCompoundPaddingTop(),
				getCompoundPaddingBottom() };
		frozen = true;
	}

	public void unfreeze() {
		frozen = false;
	}

	@Override
	public void requestLayout() {
		if (!frozen)
			super.requestLayout();
	}

	@Override
	public void postInvalidate() {
		if (!frozen)
			super.postInvalidate();
	}

	@Override
	public void postInvalidate(final int left, final int top, final int right,
			final int bottom) {
		if (!frozen)
			super.postInvalidate(left, top, right, bottom);
	}

	@Override
	public void invalidate() {
		if (!frozen)
			super.invalidate();
	}

	@Override
	public void invalidate(final Rect rect) {
		if (!frozen)
			super.invalidate(rect);
	}

	@Override
	public void invalidate(final int l, final int t, final int r, final int b) {
		if (!frozen)
			super.invalidate(l, t, r, b);
	}

	@Override
	public int getCompoundPaddingLeft() {
		return !frozen ? super.getCompoundPaddingLeft()
				: lockedCompoundPadding[0];
	}

	@Override
	public int getCompoundPaddingRight() {
		return !frozen ? super.getCompoundPaddingRight()
				: lockedCompoundPadding[1];
	}

	@Override
	public int getCompoundPaddingTop() {
		return !frozen ? super.getCompoundPaddingTop()
				: lockedCompoundPadding[2];
	}

	@Override
	public int getCompoundPaddingBottom() {
		return !frozen ? super.getCompoundPaddingBottom()
				: lockedCompoundPadding[3];
	}

	private class Shadow {
		float r;
		float dx;
		float dy;
		int color;

		public Shadow(final float r, final float dx, final float dy,
				final int color) {
			this.r = r;
			this.dx = dx;
			this.dy = dy;
			this.color = color;
		}
	}
}