package toto.ui.widget.textview;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

/**
 * TextView that adjusts its text to fit its declared dimensions
 * 
 * @author elton.kent
 * 
 */
public class AutofitTextView extends TextView {

	// Minimum size of the text in pixels
	private static final int DEFAULT_MIN_TEXT_SIZE = 8; // px
	// How precise we want to be when reaching the target textWidth size
	private static final float PRECISION = 0.5f;

	// Attributes
	private float mMinTextSize;
	private float mMaxTextSize;
	private float mPrecision;
	private Paint mPaint;

	public AutofitTextView(final Context context) {
		super(context);
		init();
	}

	public AutofitTextView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		mMinTextSize = DEFAULT_MIN_TEXT_SIZE;
		mMaxTextSize = getTextSize();
		mPrecision = PRECISION;
		mPaint = new Paint();
	}

	// Getters and Setters

	public float getMinTextSize() {
		return mMinTextSize;
	}

	public void setMinTextSize(final int minTextSize) {
		mMinTextSize = minTextSize;
	}

	public float getMaxTextSize() {
		return mMaxTextSize;
	}

	public void setMaxTextSize(final int maxTextSize) {
		mMaxTextSize = maxTextSize;
	}

	public float getPrecision() {
		return mPrecision;
	}

	public void setPrecision(final float precision) {
		mPrecision = precision;
	}

	/**
	 * Re size the font so the specified text fits in the text box assuming the
	 * text box is the specified width.
	 */
	private void refitText(final String text, final int width) {
		if (width > 0) {
			final Context context = getContext();
			Resources r = Resources.getSystem();

			final int targetWidth = width - getPaddingLeft()
					- getPaddingRight();
			float newTextSize = mMaxTextSize;
			final float high = mMaxTextSize;
			final float low = 0;

			if (context != null) {
				r = context.getResources();
			}

			mPaint.set(getPaint());
			mPaint.setTextSize(newTextSize);

			if (mPaint.measureText(text) > targetWidth) {
				newTextSize = getTextSize(r, text, targetWidth, low, high);

				if (newTextSize < mMinTextSize) {
					newTextSize = mMinTextSize;
				}
			}

			setTextSize(TypedValue.COMPLEX_UNIT_PX, newTextSize);

		}
	}

	// Recursive binary search to find the best size for the text
	private float getTextSize(final Resources resources, final String text,
			final float targetWidth, final float low, final float high) {
		final float mid = (low + high) / 2.0f;

		mPaint.setTextSize(TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_PX, mid, resources.getDisplayMetrics()));
		final float textWidth = mPaint.measureText(text);

		if ((high - low) < mPrecision) {
			return low;
		} else if (textWidth > targetWidth) {
			return getTextSize(resources, text, targetWidth, low, mid);
		} else if (textWidth < targetWidth) {
			return getTextSize(resources, text, targetWidth, mid, high);
		} else {
			return mid;
		}
	}

	@Override
	protected void onTextChanged(final CharSequence text, final int start,
			final int lengthBefore, final int lengthAfter) {
		super.onTextChanged(text, start, lengthBefore, lengthAfter);
		refitText(text.toString(), this.getWidth());
	}

	@Override
	protected void onSizeChanged(final int w, final int h, final int oldw,
			final int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if (w != oldw) {
			refitText(getText().toString(), w);
		}
	}

	@Override
	protected void onMeasure(final int widthMeasureSpec,
			final int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
		refitText(getText().toString(), parentWidth);
	}
}