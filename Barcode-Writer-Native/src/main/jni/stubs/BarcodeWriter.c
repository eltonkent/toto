#include "BarcodeWriter.h"

#include <string.h>
#include <stdio.h>
#include <android/log.h>

#include "zint.h"

#define ZINT_SYMBOL_FROM_JINT(zint_symbol_var, jint_var)	\
	struct zint_symbol * zint_symbol_var;					\
	zint_symbol_var = (void *)jint_var;

const char * LOG_CAT_TAG = "ToTo";


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setSymbology
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iSymbology) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->symbology = (int)iSymbology;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setHeight
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iHeight) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->height = (int)iHeight;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setBitmapWidth
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iHeight) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->bitmap_width = (int)iHeight;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}

JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setBitmapHeight
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iHeight) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->bitmap_height = (int)iHeight;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}



JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setWhitespace_1width
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iWhiteSpace_width) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->whitespace_width = (int)iWhiteSpace_width;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setBorder_1width
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iBorder_Width) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->border_width = (int)iBorder_Width;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setOutput_1options
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iOutput_options) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->output_options = (int)iOutput_options;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setFgcolour
  (JNIEnv * env, jclass clazz, jint ipSymbol, jstring iFgColour) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	const char * fgcolour = (*env)->GetStringUTFChars(env, iFgColour, 0);
	if (0 != fgcolour) {
		int length = (*env)->GetStringUTFLength(env, iFgColour);
		memset(my_symbol->fgcolour, 0, sizeof(char) * 10);
		strncpy(my_symbol->fgcolour, fgcolour, length);
	}
	(*env)->ReleaseStringUTFChars(env, iFgColour, fgcolour);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setBgcolour
  (JNIEnv * env, jclass clazz, jint ipSymbol, jstring iBgcolour) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	const char * bgcolour = (*env)->GetStringUTFChars(env, iBgcolour, 0);
	if (0 != bgcolour) {
		int length = (*env)->GetStringUTFLength(env, iBgcolour);
		memset(my_symbol->bgcolour, 0, sizeof(char) * 10);
		strncpy(my_symbol->bgcolour, bgcolour, length);
	}
	(*env)->ReleaseStringUTFChars(env, iBgcolour, bgcolour);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setOutfile
  (JNIEnv * env, jclass clazz, jint ipSymbol, jstring iOutfile) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	const char * outfile = (*env)->GetStringUTFChars(env, iOutfile, 0);
	if (0 != outfile) {
		int length = (*env)->GetStringUTFLength(env, iOutfile);
		memset(my_symbol->outfile, 0, sizeof(char) * 256);
		strncpy(my_symbol->outfile, outfile, length);
	}
	(*env)->ReleaseStringUTFChars(env, iOutfile, outfile);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setScale
  (JNIEnv * env, jclass clazz, jint ipSymbol, jfloat iScale) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->scale=(float)iScale;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setOption_11
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iOption_1) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->option_1 = (int)iOption_1;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setOption_12
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iOption_2) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->option_2 = (int)iOption_2;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setOption_13
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iOption_3) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->option_3 = (int)iOption_3;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setShow_1hrt
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iShow_hrt) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->show_hrt = (int)iShow_hrt;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setInput_1mode
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint iInput_mode) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	my_symbol->input_mode = (int)iInput_mode;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setText
  (JNIEnv * env, jclass clazz, jint ipSymbol, jstring iText) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	const char * text = (*env)->GetStringUTFChars(env, iText, 0);
	if (0 != text) {
		int length = (*env)->GetStringUTFLength(env, iText);
		memset(my_symbol->text, 0, sizeof(char) * 128);
		strncpy(my_symbol->text, text, length);
	}
	(*env)->ReleaseStringUTFChars(env, iText, text);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_setPrimary
  (JNIEnv * env, jclass clazz, jint ipSymbol, jstring iPrimary) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	const char * primary = (*env)->GetStringUTFChars(env, iPrimary, 0);
	if (0 != primary) {
		int length = (*env)->GetStringUTFLength(env, iPrimary);
		memset(my_symbol->primary, 0, sizeof(char) * 128);
		strncpy(my_symbol->primary, primary, length);
	}
	(*env)->ReleaseStringUTFChars(env, iPrimary, primary);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT jstring JNICALL Java_rage_di_barcode_BarcodeWriter_getErrtxt
  (JNIEnv * env, jclass clazz, jint ipSymbol ) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s : %s", __func__, my_symbol->errtxt);
	return (*env)->NewStringUTF(env, my_symbol->errtxt);
}


JNIEXPORT jint JNICALL Java_rage_di_barcode_BarcodeWriter_create
  (JNIEnv * env, jclass clazz) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	struct zint_symbol *my_symbol;
	my_symbol = ZBarcode_Create();
	return (jint)my_symbol;
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s : %d", __func__, (int)my_symbol);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_clear
  (JNIEnv * env, jclass clazz, jint ipSymbol) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	ZBarcode_Clear(my_symbol);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT void JNICALL Java_rage_di_barcode_BarcodeWriter_delete
  (JNIEnv * env, jclass clazz, jint ipSymbol) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	ZBarcode_Delete(my_symbol);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s", __func__);
}


JNIEXPORT jint JNICALL Java_rage_di_barcode_BarcodeWriter_encode
  (JNIEnv * env, jclass clazz, jint ipSymbol, jstring iInput) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	jint RC = 0;
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	const char * input = (*env)->GetStringUTFChars(env, iInput, 0);
	if (0 != input) {
		int length = (*env)->GetStringUTFLength(env, iInput);
		unsigned char * casted = (unsigned char *)input;
		RC = ZBarcode_Encode(my_symbol, casted, length);
	}
	(*env)->ReleaseStringUTFChars(env, iInput, input);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s : %d", __func__, RC);
	return RC;
}


JNIEXPORT jint JNICALL Java_rage_di_barcode_BarcodeWriter_print
  (JNIEnv * env, jclass clazz, jint ipSymbol, jint rotation) {
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "==> %s", __func__);
	ZINT_SYMBOL_FROM_JINT(my_symbol,ipSymbol);
	jint RC = ZBarcode_Print(my_symbol, rotation);
	__android_log_print(ANDROID_LOG_DEBUG,LOG_CAT_TAG, "<== %s : %d", __func__, RC);
	return RC;
}
