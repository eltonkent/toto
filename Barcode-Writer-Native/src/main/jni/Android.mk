#
#
#
LOCAL_PATH := $(call my-dir)
LIBPNG_PATH := $(LOCAL_PATH)/libpng
include $(CLEAR_VARS)
ZINT_VERSION	:=-DZINT_VERSION=\"2.4.2\"

ZINT_ONEDIM	:= 	libzint/code.c \
				libzint/code128.c \
				libzint/2of5.c \
				libzint/upcean.c \
				libzint/telepen.c \
				libzint/medical.c \
				libzint/plessey.c \
				libzint/rss.c
ZINT_POSTAL	:= 	libzint/postal.c \
				libzint/auspost.c \
				libzint/imail.c
ZINT_TWODIM	:= 	libzint/code16k.c \
				libzint/dmatrix.c \
				libzint/pdf417.c \
				libzint/qr.c \
				libzint/maxicode.c \
				libzint/composite.c \
				libzint/aztec.c \
				libzint/code49.c \
				libzint/code1.c \
				libzint/gridmtx.c
ZINT_COMMON	:= 	libzint/common.c \
				libzint/render.c \
				libzint/png.c \
				libzint/library.c \
				libzint/ps.c \
				libzint/large.c \
				libzint/reedsol.c \
				libzint/gs1.c \
				libzint/svg.c \
JNI_STUBS	:= 	stubs/BarcodeWriter.c \


include $(CLEAR_VARS)
LOCAL_MODULE    		:= BarcodeWriter
LOCAL_SRC_FILES			:= $(JNI_STUBS) $(ZINT_COMMON) $(ZINT_ONEDIM) $(ZINT_TWODIM) $(ZINT_POSTAL) $(LIBPNG_PATH)
LOCAL_C_INCLUDES		:= $(LOCAL_PATH)/libzint $(LIBPNG_PATH)
LOCAL_LDLIBS 			:= -lz -llog

LOCAL_CPPFLAGS += -ffunction-sections -fdata-sections -fvisibility=hidden
LOCAL_CFLAGS += -ffunction-sections -fdata-sections -Wall $(ZINT_VERSION) 
LOCAL_LDFLAGS += -Wl,--gc-sections


include $(BUILD_SHARED_LIBRARY)