package toto.xc.xml;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;

import org.xmlpull.v1.XmlPullParser;

import toto.xc.json.Json;
import toto.xc.json.JsonBuilder;
import toto.xc.json.JsonIOException;
import toto.xc.json.JsonSyntaxException;
import toto.xc.json.Primitives;
import toto.xc.json.io.JsonReader;
import toto.xc.json.io.JsonToken;
import toto.xc.json.io.MalformedJsonException;

/**
 * Simple XML deserializer
 * <p>
 * <b>Usage:</b> <br/>
 * <code>
 * <pre>
 * 	XmlDeserializer xml = new XmlDeserializer.Builder().setXmlParser(
 * 					XmlPullParserFactory.newInstance().newPullParser())
 * 					.create();
 * 	String xmlString = "<model><name>my name</name><age>22</age><address>Test address</address></model>";
 * 	TestXML deser = xml.fromXml(xmlString, TestXML.class);
 * </pre>
 * </code>
 * 
 * @see Builder </p>
 * 
 */
public class XmlDeserializer {

	/** Core object. */
	private final Json core;

	/** XML parser creator. */
	private final XmlPullParser xmlParserCreator;

	/** Option. */
	private final XmlReader.Options options;

	XmlDeserializer(final Json gson, final XmlPullParser xmlParserCreator,
			final XmlReader.Options options) {
		if (xmlParserCreator == null) {
			throw new NullPointerException(
					"rage.xc.xml.XmlParserCreator is null");
		}
		this.core = gson;
		this.xmlParserCreator = xmlParserCreator;
		this.options = options;
	}

	public <T> T fromXml(final String xml, final Class<T> classOfT)
			throws JsonSyntaxException {
		final Object object = fromXml(xml, (Type) classOfT);
		return Primitives.wrap(classOfT).cast(object);
	}

	@SuppressWarnings("unchecked")
	public <T> T fromXml(final String xml, final Type typeOfT)
			throws JsonSyntaxException {
		if (xml == null) {
			return null;
		}
		final StringReader reader = new StringReader(xml);
		final T target = (T) fromXml(reader, typeOfT);
		return target;
	}

	public <T> T fromXml(final Reader xml, final Class<T> classOfT)
			throws JsonSyntaxException, JsonIOException {
		final XmlReader jsonReader = new XmlReader(xml, xmlParserCreator,
				options); // change reader
		final Object object = fromXml(jsonReader, classOfT);
		assertFullConsumption(object, jsonReader);
		return Primitives.wrap(classOfT).cast(object);
	}

	@SuppressWarnings("unchecked")
	public <T> T fromXml(final Reader xml, final Type typeOfT)
			throws JsonIOException, JsonSyntaxException {
		final XmlReader jsonReader = new XmlReader(xml, xmlParserCreator,
				options); // change reader
		final T object = (T) fromXml(jsonReader, typeOfT);
		assertFullConsumption(object, jsonReader);
		return object;
	}

	private static void assertFullConsumption(final Object obj,
			final JsonReader reader) {
		try {
			if (obj != null && reader.peek() != JsonToken.END_DOCUMENT) {
				throw new JsonIOException(
						"JSON document was not fully consumed.");
			}
		} catch (final MalformedJsonException e) {
			throw new JsonSyntaxException(e);
		} catch (final IOException e) {
			throw new JsonIOException(e);
		}
	}

	/**
	 * Reads the next XML value from {@code reader} and convert it to an object
	 * of type {@code typeOfT}. Since Type is not parameterized by T, this
	 * method is type unsafe and should be used carefully
	 * 
	 * @throws JsonIOException
	 *             if there was a problem writing to the Reader
	 * @throws JsonSyntaxException
	 *             if json is not a valid representation for an object of type
	 */
	public <T> T fromXml(final XmlReader reader, final Type typeOfT)
			throws JsonIOException, JsonSyntaxException {
		return core.fromJson(reader, typeOfT);
	}

	@Override
	public String toString() {
		return core.toString();
	}

	/**
	 * Use this builder for constructing {@link XmlDeserializer} object. All
	 * methods are very similar to {@link toto.xc.json.JsonBuilder}.
	 */
	public static class Builder {

		/** Core builder. */
		private JsonBuilder coreBuilder;

		/** Factory for XML parser. */
		private XmlPullParser xmlParserCreator;

		/** Options. */
		private final XmlReader.Options options = new XmlReader.Options();
		{
			// Parse option: whether to skip root element
			options.skipRoot = true;
			// Parse option: whether to treat XML namespaces.
			options.namespaces = false;
			// Parse option: list a created from a set of elements with the same
			// name without a grouping element.
			options.sameNameList = false;
		}

		/**
		 * @param gsonBuilder
		 *            instance of {@link toto.xc.json.JsonBuilder}
		 * @return this instance for chaining
		 */
		public Builder wrap(final JsonBuilder gsonBuilder) {
			this.coreBuilder = gsonBuilder;
			return this;
		}

		/**
		 * Set a factory for XML pull parser.
		 * 
		 * @param xmlParserCreator
		 *            instance of {@link XmlPullParser }
		 * @return this instance for chaining
		 */
		public Builder setXmlParser(final XmlPullParser xmlParserCreator) {
			this.xmlParserCreator = xmlParserCreator;
			return this;
		}

		/**
		 * Here's the difference.<br/>
		 * <b>Skip root: on</b>
		 * 
		 * <pre>
		 *   &lt;root>&ltname>value&lt;/name>&lt;/root>
		 *   ==>
		 *   {name : 'value'}
		 * </pre>
		 * 
		 * <b>Skip root: off</b>
		 * 
		 * <pre>
		 *   &lt;root>&ltname>value&lt;/name>&lt;/root>
		 *   ==>
		 *   {root : {name : 'value'}}
		 * </pre>
		 * 
		 * @param value
		 *            true to skip root element
		 * @return this instance for chaining
		 */
		public Builder setSkipRoot(final boolean value) {
			this.options.skipRoot = value;
			return this;
		}

		/**
		 * Here's the difference.<br/>
		 * <b>Treat namespaces: on</b>
		 * 
		 * <pre>
		 *   &lt;root>&lt;ns:name>value&lt;/ns:name>&lt;/root>
		 *   ==>
		 *   {'&lt;ns>name' : 'value'}
		 * </pre>
		 * 
		 * <b>Treat namespaces: off</b>
		 * 
		 * <pre>
		 *   &lt;root>&lt;ns:name>value&lt;/ns:name>&lt;/root>
		 *   ==>
		 *   {name : 'value'}
		 * </pre>
		 * 
		 * @param value
		 *            true to treat namespaces
		 * @return this instance for chaining
		 */
		public Builder setTreatNamespaces(final boolean value) {
			this.options.namespaces = value;
			return this;
		}

		/**
		 * Here's the difference.<br/>
		 * <b>Same name lists: on</b>
		 * 
		 * <pre>
		 *   &lt;root>
		 *     &lt;name>value&lt;/name>
		 *     &lt;item>value1&lt;/item>
		 *     &lt;item>value2&lt;/item>
		 *   &lt;/root>
		 *   ==>
		 *   {name : 'value', item : ['value1', 'value2']}
		 * </pre>
		 * 
		 * <b>Treat namespaces: off</b>
		 * 
		 * <pre>
		 *   &lt;root>
		 *     &lt;name>value&lt;/name>
		 *     &lt;items>
		 *       &lt;ignored>value1&lt;/ignored>
		 *       &lt;ignored>value2&lt;/ignored>
		 *     &lt;/items>
		 *   &lt;/root>
		 *   ==>
		 *   {name : 'value', items : ['value1', 'value2']}
		 * </pre>
		 * 
		 * @param value
		 *            true for same name list policy
		 * @return this instance for chaining
		 */
		public Builder setSameNameLists(final boolean value) {
			this.options.sameNameList = value;
			return this;
		}

		/**
		 * If set to true than arrays can contain primitive values. If false
		 * only arrays can contain objects only. When set to true you cannot
		 * parse the next sample:
		 * 
		 * <pre>
		 *   &lt;list>
		 *     &lt;item>
		 *       text node value
		 *       &lt;field-name>field value&lt;/field-name>
		 *     &lt;/item>
		 *     &lt;item>value2&lt;/item>
		 *   &lt;/list>
		 * </pre>
		 * 
		 * It's caused by the fact that parser meats 'text node value' and makes
		 * a decision that this item is primitive.
		 * 
		 * @param primitiveArrays
		 *            value for primitive arrays policy
		 * @return this instance for chaining
		 */
		public Builder setPrimitiveArrays(final boolean primitiveArrays) {
			this.options.primitiveArrays = primitiveArrays;
			return this;
		}

		/**
		 * When set to true and the root element is parsed as a collection this
		 * collection items are treated as primitives.
		 * 
		 * @see #setPrimitiveArrays(boolean)
		 * @param rootArrayPrimitive
		 *            flag for 'root array primitive' policy
		 * @return this instance for chaining
		 */
		public Builder setRootArrayPrimitive(final boolean rootArrayPrimitive) {
			this.options.rootArrayPrimitive = rootArrayPrimitive;
			return this;
		}

		/**
		 * Creates a {@link XmlDeserializer} instance based on the current
		 * configuration. This method is free of side-effects to this
		 * {@code rage.xc.xml.XmlBuilder} instance and hence can be called
		 * multiple times.
		 * 
		 * @return an instance of rage.xc.xml.Xml configured with the options
		 *         currently set in this builder
		 */
		public XmlDeserializer create() {
			if (coreBuilder == null) {
				coreBuilder = new JsonBuilder();
			}
			return new XmlDeserializer(coreBuilder.create(), xmlParserCreator,
					options);
		}

	}

}
