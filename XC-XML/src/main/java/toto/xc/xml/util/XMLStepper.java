/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.xc.xml.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.LinkedList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

/**
 * Allows you to walk (up/down) a XML document {@link #stepDown(String)}
 * {@link #stepUp()}
 * 
 * @author elton.kent
 * 
 */
public class XMLStepper {

	private final XmlPullParser parser;

	private final LinkedList<Descent> stack = new LinkedList<Descent>();

	public XMLStepper(final InputStream input) throws IOException {
		this(input, null);
	}

	public XMLStepper(final Reader reader) throws IOException {
		parser = Xml.newPullParser();

		try {
			parser.setInput(reader);
		} catch (final XmlPullParserException ex) {
			final IOException ioException = new IOException();
			ioException.initCause(ex);
			throw ioException;
		}

		stack.add(new Descent(0, null));
	}

	public XMLStepper(final InputStream input, final String encoding)
			throws IOException {

		parser = Xml.newPullParser();

		try {
			parser.setInput(input, encoding);
		} catch (final XmlPullParserException ex) {
			final IOException ioException = new IOException();
			ioException.initCause(ex);
			throw ioException;
		}

		stack.add(new Descent(0, null));
	}

	/**
	 * Descent the XML tree to the descendent with the given name
	 * 
	 * @param name
	 *            name of descendent to descent to
	 * @return {@code true} id descendent was found
	 */
	public boolean stepDown(final String name) throws IOException {
		if (stack.getLast().consumed) {
			return false;
		}

		int depth = 0;

		while (true) {
			final int event = next();

			if (event == XmlPullParser.START_TAG) {
				depth++;

				if (name.equals(parser.getName())) {
					stack.add(new Descent(depth, parser.getName()));
					return true;
				}
			} else if (event == XmlPullParser.END_TAG
					|| event == XmlPullParser.END_DOCUMENT) {
				if (depth == 0) {
					stack.getLast().consumed = true;
					return false;
				}

				depth--;
			}
		}
	}

	/**
	 * Ascent back from a previous {@link #stepDown(String)}.
	 */
	public void stepUp() throws IOException {
		if (stack.size() == 1) {
			throw new IllegalStateException();
		}

		final Descent descent = stack.removeLast();

		if (descent.consumed) {
			return;
		}

		int depth = descent.depth;

		while (true) {
			final int event = next();
			if (event == XmlPullParser.START_TAG) {
				depth++;
			} else if (event == XmlPullParser.END_TAG) {
				depth--;

				if (depth == 0) {
					return;
				}
			}
		}
	}

	/**
	 * Get the name at the current descent.
	 */
	public String getName() {
		return stack.getLast().name;
	}

	/**
	 * Get the attribute value for the given name at the current descent.
	 */
	public String getAttributeValue(final String name) {
		return parser.getAttributeValue(null, name);
	}

	/**
	 * Get the text of the current node.
	 */
	public String getText() throws IOException {
		if (next() != XmlPullParser.TEXT) {
			throw new IOException("text expected");
		}

		return parser.getText();
	}

	/**
	 * Convenience method to descent to the node with the given name, get its
	 * text and ascent again.
	 * 
	 * @param name
	 *            name of node to descent to
	 * @return text
	 * @throws IOException
	 *             if not present
	 */
	public String getText(final String name) throws IOException {
		if (!stepDown(name)) {
			throw new IOException("tag expected '" + name + "'");
		}

		final String text = getText();

		stepUp();

		return text;
	}

	private int next() throws IOException {
		try {
			return parser.next();
		} catch (final XmlPullParserException ex) {
			final IOException ioException = new IOException();
			ioException.initCause(ex);
			throw ioException;
		}
	}

	private class Descent {
		public final int depth;

		public final String name;

		public boolean consumed;

		public Descent(final int depth, final String name) {
			this.depth = depth;
			this.name = name;
		}
	}
}
