package test.toto.xc.xml;

import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import test.toto.ToToTestCase;
import toto.xc.xml.XmlDeserializer;


public class SimpleXMLTest extends ToToTestCase {
	
	public void testXMLDeserial() {
		logH("XC:XML", "XML Deserialization");
		try {
			XmlDeserializer xml = new XmlDeserializer.Builder().setXmlParser(
					XmlPullParserFactory.newInstance().newPullParser())
					.create();
			String xmlString = "<model><name>ToTo</name><age>3</age><address>Mobifluence Interactive</address></model>";
			TestDAO dao = xml.fromXml(xmlString, TestDAO.class);
			assertNotNull(dao);
			assertEquals("ToTo", dao.getName());
			assertEquals(3, dao.getAge());
			assertEquals("Mobifluence Interactive", dao.getAddress());
			logH("XC:XML", "Deserial: " + dao.toString());
			
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	}
}
