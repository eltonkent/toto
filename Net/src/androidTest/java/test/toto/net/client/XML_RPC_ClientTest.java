package test.toto.net.client;

import android.util.Log;
import test.toto.ToToTestCase;
import toto.net.client.http.xmlrpc.TXMLRPCClient;
import toto.net.client.http.xmlrpc.XMLRPCException;

public class XML_RPC_ClientTest extends ToToTestCase {

	public void testXMLRPC(){
		TXMLRPCClient client=new TXMLRPCClient("https://api.flickr.com/services/xmlrpc/");
		try {
			Object obj=client.call("flickr.test.echo", "test","value");
			Log.d("ToTo", "XML-RPC"+obj);
		} catch (XMLRPCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
