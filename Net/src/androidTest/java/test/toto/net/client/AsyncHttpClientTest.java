package test.toto.net.client;

import test.toto.ToToTestCase;
import toto.async.AsyncCallback;
import toto.net.client.asynchttp.AsyncHttpClient;

public class AsyncHttpClientTest extends ToToTestCase {
	
	AsyncCallback<String> callback =new AsyncCallback<String>() {
		
		@Override
		public void onSuccess(String data) {
			logH("AsyncHttp", "Data"+data);
		}
		
		@Override
		public void onFailure(String data, Throwable t) {
		}
	};
	
	boolean gotData=false;
	public void testGet(){
		AsyncHttpClient client=new AsyncHttpClient();
		logH("AsyncHttp", "Starting...");
		client.get("http://www.google.com",callback );
	}
}
