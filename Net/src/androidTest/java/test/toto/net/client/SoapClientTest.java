package test.toto.net.client;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;

import test.toto.ToToTestCase;
import toto.net.client.http.soap.SoapEnvelope;
import toto.net.client.http.soap.SoapObject;
import toto.net.client.http.soap.SoapPrimitive;
import toto.net.client.http.soap.SoapSerializationEnvelope;
import toto.net.client.http.soap.transport.HttpTransportSE;

public class SoapClientTest extends ToToTestCase {

	public void testSOAPClient() {

		SoapObject rpc = new SoapObject("ns1", "add");
		rpc.addProperty("x", 4);
		rpc.addProperty("y", 4);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER10);
		envelope.setOutputSoapObject(rpc);

		// resultItem.setLabel(symbol);

		HttpTransportSE ht = new HttpTransportSE(
				"http://ws1.parasoft.com/glue/calculator");
		// ht.debug = true;

		try {
			ht.call("ns1:add", envelope);
			float result = ((SoapPrimitive) envelope.getResponse())
					.getValueAsFloat();
			assertEquals(result, 8);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	}

}
