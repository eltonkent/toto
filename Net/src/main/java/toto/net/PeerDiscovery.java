/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

/**
 * Performs broadcast and multicast peer detection.
 * <p>
 * <div>
 * <h3>
 * Usage</h3>
 * 
 * <pre>
 * int group = 6969;
 * PeerDiscovery mp = new PeerDiscovery(group, 6969);
 * Peer[] peers = mp.getPeers(100, (byte) 0);
 * System.out.println(peers.length + &quot; peers found&quot;);
 * for (Peer p : peers)
 * 	System.out.println(&quot;\t&quot; + p);
 * </pre>
 * 
 * </div>
 * </p>
 * *
 */
public class PeerDiscovery {
	/**
	 * Record of a peer
	 * 
	 * @author ryanm
	 */
	public class Peer {
		/**
		 * The data of the peer
		 */
		public final int data;

		/**
		 * The ip of the peer
		 */
		public final InetAddress ip;

		private Peer(final InetAddress ip, final int data) {
			this.ip = ip;
			this.data = data;
		}

		@Override
		public String toString() {
			return ip.getHostAddress() + " " + data;
		}
	}

	private static final byte QUERY_PACKET = 80;

	private static final byte RESPONSE_PACKET = 81;

	private static int decode(final byte[] b, final int index) {
		int i = 0;

		i |= b[index] << 24;
		i |= b[index + 1] << 16;
		i |= b[index + 2] << 8;
		i |= b[index + 3];

		return i;
	}

	private static void encode(final int i, final byte[] b, final int index) {
		b[index] = (byte) (i >> 24 & 0xff);
		b[index + 1] = (byte) (i >> 16 & 0xff);
		b[index + 2] = (byte) (i >> 8 & 0xff);
		b[index + 3] = (byte) (i & 0xff);
	}



	/**
	 * Redefine this to be notified of exceptions on the listen thread. Default
	 * behaviour is to print to stdout. Can be left as null for no-op
	 */
	// public ExceptionHandler rxExceptionHandler = new ExceptionHandler();

	private final Thread bcastListen = new Thread(
			PeerDiscovery.class.getSimpleName() + " broadcast listen thread") {
		@Override
		public void run() {
			try {
				final byte[] buffy = new byte[5];
				final DatagramPacket rx = new DatagramPacket(buffy,
						buffy.length);

				while (!shouldStop) {
					try {
						buffy[0] = 0;

						bcastSocket.receive(rx);

						final int recData = decode(buffy, 1);

						if ((buffy[0] == QUERY_PACKET) && (recData == group)) {
							final byte[] data = new byte[5];
							data[0] = RESPONSE_PACKET;
							encode(peerData, data, 1);

							final DatagramPacket tx = new DatagramPacket(data,
									data.length, rx.getAddress(), port);

							lastResponseDestination = rx.getAddress();

							bcastSocket.send(tx);
						} else if (buffy[0] == RESPONSE_PACKET) {
							if ((responseList != null)
									&& !rx.getAddress().equals(
											lastResponseDestination)) {
								synchronized (responseList) {
									responseList.add(new Peer(rx.getAddress(),
											recData));
								}
							}
						}
					} catch (final SocketException se) {
						// someone may have called disconnect()
					}
				}

				bcastSocket.disconnect();
				bcastSocket.close();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		};
	};

	private final DatagramSocket bcastSocket;

	private final InetSocketAddress broadcastAddress;

	/**
	 * The group identifier. Determines the setKey of peers that are able to
	 * discover each other
	 */
	private final int group;

	/**
	 * Used to detect and ignore this peers response to it's own query. When we
	 * send a response packet, we setKey this to the destination. When we
	 * receive a response, if this matches the source, we know that we're
	 * talking to ourselves and we can ignore the response.
	 */
	private InetAddress lastResponseDestination = null;

	/**
	 * Data returned with discovery
	 */
	private int peerData;

	/**
	 * The port number that we operate on
	 */
	private final int port;

	private List<Peer> responseList = null;

	private boolean shouldStop = false;

	/**
	 * Constructs a UDP broadcast-based peer
	 * 
	 * @param group
	 *            The identifier shared by the peers that will be discovered.
	 * @param port
	 *            a valid port, i.e.: in the range 1025 to 65535 inclusive
	 * @throws IOException
	 */
	public PeerDiscovery(final int group, final int port) throws IOException {
		this.group = group;
		this.port = port;

		bcastSocket = new DatagramSocket(port);
		broadcastAddress = new InetSocketAddress("255.255.255.255", port);

		bcastListen.setDaemon(true);
		bcastListen.start();
	}

	/**
	 * Signals this {@link PeerDiscovery} to shut down. This call will block
	 * until everything's timed out and closed etc.
	 */
	public void disconnect() {
		shouldStop = true;

		bcastSocket.close();
		bcastSocket.disconnect();

		try {
			bcastListen.join();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Queries the network and finds the addresses of other peers in the same
	 * group
	 * 
	 * @param timeout
	 *            How long to wait for responses, in milliseconds. Call will
	 *            block for this long, although you can
	 *            {@link Thread#interrupt()} to cut the wait short
	 * @param peerType
	 *            The type flag of the peers to look for
	 * @return The addresses of other peers in the group
	 * @throws IOException
	 *             If something goes wrong when sending the query packet
	 */
	public Peer[] getPeers(final int timeout, final byte peerType)
			throws IOException {
		responseList = new ArrayList<Peer>();

		// send query byte, appended with the group id
		final byte[] data = new byte[5];
		data[0] = QUERY_PACKET;
		encode(group, data, 1);

		final DatagramPacket tx = new DatagramPacket(data, data.length,
				broadcastAddress);

		bcastSocket.send(tx);

		// wait for the listen thread to do its thing
		try {
			Thread.sleep(timeout);
		} catch (final InterruptedException e) {
		}

		Peer[] peers;
		synchronized (responseList) {
			peers = responseList.toArray(new Peer[responseList.size()]);
		}

		responseList = null;

		return peers;
	}
}
