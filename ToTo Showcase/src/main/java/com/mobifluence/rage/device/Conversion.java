package com.mobifluence.rage.device;

import toto.device.location.coor.LatLongCoordinate;
import toto.jobs.TJobResponse;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

/**
 * Created by ekent4 on 1/29/14.
 */
public class Conversion extends ProcessingActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splitpane);
        final LinearLayout top = (LinearLayout) findViewById(R.id.top);
        LinearLayout bottom = (LinearLayout) findViewById(R.id.bottom);
        View bottomChild = getLayoutInflater().inflate(R.layout.device_location_convert, null);
        bottom.addView(bottomChild);
        final EditText lat1 = (EditText) bottomChild.findViewById(R.id.editText);
        final EditText long1 = (EditText) bottomChild.findViewById(R.id.editText2);
        lat1.setText("40.714623");
        long1.setText("-74.006605");
        Button calculate = (Button) bottomChild.findViewById(R.id.button);
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    top.removeAllViews();
                    double l1 = Double.parseDouble(lat1.getText().toString());
                    double ln1 = Double.parseDouble(long1.getText().toString());
                    LatLongCoordinate one =new LatLongCoordinate(l1,ln1);
                    top.addView(getKeyValueView("To DMS", ""+one.toDMSString()));
                    top.addView(getKeyValueView("To MGRS", ""+one.toMGRS()));
                    top.addView(getKeyValueView("To UTM", ""+one.toUTM()));
                    top.addView(getKeyValueView("To OSGB", ""+one.toOSGB()));
                }catch(Exception e){
                    Toast.makeText(getApplicationContext(), "Invalid coordinates", 3000).show();
                }
            }
        });
    }

    @Override
    protected void success(TJobResponse responses) {

    }
}
