package com.mobifluence.rage.device;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

/**
 * Created by ekent4 on 1/29/14.
 */
public class DeviceMainActivity extends MainFeatureActivity {

    @Override
    protected List<FeatureItem> getFeatureList() {
        List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("Location utilities",
                "Coordinate system conversion and distance estimation",
                Location.class));
        listAdapter.add(createItem("Root utilities",
                "Utilities for rooted devices.",
                RootUtilities.class));
        return listAdapter;
    }
}
