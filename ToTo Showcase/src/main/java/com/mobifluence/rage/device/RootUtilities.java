package com.mobifluence.rage.device;

import toto.device.root.RootTools;
import toto.jobs.TJobResponse;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

/**
 * Created by ekent4 on 1/29/14.
 */
public class RootUtilities extends ProcessingActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splitpane);
        LinearLayout top = (LinearLayout) findViewById(R.id.top);
        LinearLayout bottom = (LinearLayout) findViewById(R.id.bottom);

        top.addView(getKeyValueView("Does this app have root access?", ""+RootTools.isRootAccessGiven()));
        top.addView(getKeyValueView("Is Busybox installed?", ""+RootTools.hasUtil("","busybox")));
        top.addView(getKeyValueView("Has test binary", ""+RootTools.findBinary("busybox")));
    }
    @Override
    protected void success(TJobResponse responses) {

    }
}
