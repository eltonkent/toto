package com.mobifluence.rage.device;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

/**
 * Created by ekent4 on 1/29/14.
 */
public class Location extends MainFeatureActivity {

    @Override
    protected List<FeatureItem> getFeatureList() {
        List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("Coordinate Conversion",
                "Convert between the different coordinate systems",
                Conversion.class));
        listAdapter.add(createItem("Vicinity Distance",
                "Vicinity distance between two coordintates",
                VicinityDistance.class));
        listAdapter.add(createItem("Law of Cosines Distance",
                "Law of Cosines distance between two coordintates",
                CosinesDistance.class));
        listAdapter.add(createItem("Haversine Distance",
                "Haversine distance between two coordintates",
                HaversineDistance.class));

        listAdapter.add(createItem("Simple Distance",
                "Simple distance between two coordintates",
                SimpleDistance.class));


        return listAdapter;
    }
}
