package com.mobifluence.rage;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.mobifluence.rage.ai.vision.VIMainActivity;
import com.mobifluence.rage.db.DBMainActivity;
import com.mobifluence.rage.device.DeviceMainActivity;
import com.mobifluence.rage.graphics.GraphicsMainActivity;
import com.mobifluence.rage.security.SecurityActivity;
import com.mobifluence.rage.ui.UImainActivity;
import com.mobifluence.rage.util.UtilMainActivity;

public class RageMainActivity extends MainFeatureActivity {
	private static boolean hasEnteredCode;

	protected void onCreate(Bundle instance) {
		super.onCreate(instance);
//		String test = "tewsedf sdkjfnsdf eregejnvjkngs sdkjf sdf sdkjf sdf retes t tekibr  sdkjfnsdf eregejnvjkngs sdkjf sdf sdkjf sdf retes t tekibr ksdkjfnsdf eregejnvjkngs sdkjf sdf sdkjf sdf retes t tekibr ksdkjfnsdf eregejnvjkngs sdkjf sdf sdkjf sdf retes t tekibr kk  fsdjfbsdfkjs sfjskd fskjdf sdkjfbsdfbsdkjfbsdjfkbskjdf skfdjbsdkjfbsdkjfbruitergter erugebrgteirgbere rgjerbgergberbgeigebgerkg er gueigbegebrgiegberggergeg";
//		long time = System.nanoTime();
//		String hash = new Murmur2Hash().toHexString(test.getBytes());
//		Log.d("RageTest", "mur:" + (System.nanoTime() - time) + " " + hash);
//
//		time = System.nanoTime();
//		hash = new CRC32().toHexString(test.getBytes());
//		Log.d("RageTest", "crc:" + (System.nanoTime() - time) + " " + hash);
//		
//		time = System.nanoTime();
//		hash = new Fletcher32().toHexString(test.getBytes());
//		Log.d("RageTest", "fle:" + (System.nanoTime() - time) + " " + hash);
//		
//		time = System.nanoTime();
//		hash = new JustinSobel64().toHexString(test.getBytes());
//		Log.d("RageTest", "sob:" + (System.nanoTime() - time) + " " + hash);
//		
//		time = System.nanoTime();
//		hash = new Adler32().toHexString(test.getBytes());
//		Log.d("RageTest", "adl:" + (System.nanoTime() - time) + " " + hash);

		
		// if(!hasEnteredCode)
		// showAuthDialog();
	}

	private void showAuthDialog() {
		final Dialog dialog = new Dialog(RageMainActivity.this);// builder.create();
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_auth);
		dialog.setCancelable(false);

		final EditText text = (EditText) dialog.findViewById(R.id.editText);
		dialog.findViewById(R.id.button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						finish();
					}
				});
		dialog.findViewById(R.id.button2).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {

						if (text.getText().toString().trim().equals("79338")) {
							hasEnteredCode = true;
							dialog.dismiss();
						} else {
							Toast.makeText(RageMainActivity.this,
									"Invalid Auth Code", 3000).show();
						}
						text.setText("");
					}
				});
		dialog.show();
	}

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("User Interface",
				"Custom views, layouts and drawables", UImainActivity.class));
		listAdapter.add(createItem("Graphics",
				"Host of graphics related utilities",
				GraphicsMainActivity.class));
		listAdapter.add(createItem("Database",
				"DB utils and a few embedded DB implementations",
				DBMainActivity.class));

		listAdapter.add(createItem("Device Intelligence",
				"Harvesting information from bitmaps", VIMainActivity.class));
		// listAdapter.add(createItem("Application",
		// "Application / Intent functions", AppMainActivity.class));

		listAdapter.add(createItem("Device Utilities",
				"Location, Root access and Sensor utilities",
				DeviceMainActivity.class));
		// listAdapter.add(createItem("I/O",
		// "I/O modules and file manipulation utilities",
		// GraphicsMainActivity.class));
		// listAdapter
		// .add(createItem("Networking",
		// "Network clients and IP utilities",
		// GraphicsMainActivity.class));
		// listAdapter.add(createItem("Text",
		// "Fast text manipulation and processing",
		// GraphicsMainActivity.class));

		// listAdapter.add(createItem("Collections",
		// "Collection implementations that are not part of the toolkit",
		// GraphicsMainActivity.class));
		listAdapter.add(createItem("Security",
				"Hashing, checksum and other security utilities",
				SecurityActivity.class));

		listAdapter.add(createItem("Utilities", "Random, PDF, Zip utilities",
				UtilMainActivity.class));
		return listAdapter;
	}
}
