package com.mobifluence.rage.app;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;
import com.mobifluence.rage.ai.vision.VIMainActivity;

public class AppMainActivity extends MainFeatureActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("Visual Intelligence",
				"components related extraction information from visual ",
				VIMainActivity.class));
		return listAdapter;
	}

}
