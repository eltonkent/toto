package com.mobifluence.rage;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public abstract class MainFeatureActivity  extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feature_list);
		ListView lv = (ListView) findViewById(R.id.listView);
		lv.setAdapter(getAdapter());
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				FeatureItem item = (FeatureItem) arg0.getItemAtPosition(arg2);
				if (item.getActivityClass() != null) {
					Intent intent = new Intent(getApplicationContext(), item
							.getActivityClass());
					startActivity(intent);
				} else {
					Toast.makeText(getApplicationContext(),
							"No Implementation found!", 3000).show();
				}
			}
		});

	}


	
	protected abstract List<FeatureItem> getFeatureList();
	

	private FeatureAdapter getAdapter() {
		FeatureAdapter adapter = new FeatureAdapter(getApplicationContext(), 0,
				getFeatureList());
		return adapter;
	}
	
	
	protected FeatureItem createItem(String caption, String secondLine,
			Class<? extends Activity> clz) {
		FeatureItem item = new FeatureItem();
		item.setActivityClass(clz);
		item.setDescription(secondLine);
		item.setName(caption);
		return item;
	}

}
