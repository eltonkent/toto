package com.mobifluence.rage.db;

import java.io.File;
import java.io.IOException;

import toto.db.kvdb.KVDb;
import toto.io.file.SDCardUtils;
import toto.jobs.TJobResponse;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class KVDBActivity extends ProcessingActivity {

	private LinearLayout top;
	private LinearLayout bottom;
	private String path = SDCardUtils.getDirectory().toString() + "/KVDB/";
	private File dbPath = new File(path);
	private KVDb db;
	private EditText key;
	private EditText value;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);
		top = (LinearLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		View view = getLayoutInflater().inflate(R.layout.db_leveldb_bottom, null);
		bottom.addView(view);

		key = (EditText) view.findViewById(R.id.editText1);
		value = (EditText) view.findViewById(R.id.editText2);
		final View insert = view.findViewById(R.id.button1);
		insert.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				top.removeAllViews();
				insert(key.getText().toString(), value.getText().toString());
			}
		});
		final View delete = view.findViewById(R.id.button2);
		delete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				top.removeAllViews();
				delete(key.getText().toString());
			}
		});

		final View read = view.findViewById(R.id.button3);
		read.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				top.removeAllViews();
				read(key.getText().toString());
			}
		});
		View remove=view.findViewById(R.id.button4);
		remove.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				top.removeAllViews();
				db.destroy();
				read.setVisibility(View.GONE);
				delete.setVisibility(View.GONE);
				value.setVisibility(View.GONE);
				key.setVisibility(View.GONE);
				insert.setVisibility(View.GONE);
			}
		});
		if(!SDCardUtils.isMounted()){
			Toast.makeText(getApplicationContext(), "This feature requires an SD card!", 3000).show();
			finish();
		}
		initDB();
	}

	private void insert(String key, String value) {
		long time = System.nanoTime();
		db.put(key.getBytes(), value.getBytes());
		
		top.addView(getKeyValueView(key + " inserted!:[",
				(System.nanoTime() - time) + "(ns)]"));
	}

	private void delete(String key) {
		long time = System.nanoTime();
		db.delete(key.getBytes());
		top.addView(getKeyValueView(key + " deleted!:[",
				 (System.nanoTime() - time) + "(ns)]"));
		this.key.setText("");
		this.value.setText("");
	}

	private void read(String key) {
		long time = System.nanoTime();
		byte[] val=db.get(key.getBytes());
		if(val!=null){
			top.addView(getKeyValueView("Value:",
					 new String(val)+"["+ (System.nanoTime() - time) + "(ns)]"));
		}
	}

	private void initDB() {
		if (!dbPath.exists()) {
			top.addView(getKeyValueView("Creating DB @:", path));
			dbPath.mkdir();
		}
		try {
			db = new KVDb(dbPath);
			db.open();
		} catch (IOException e) {
			e.printStackTrace();
		}
		top.addView(getKeyValueView("Info", "Database open!"));
	}

	@Override
	protected void success(TJobResponse responses) {

	}

	public void onBackPressed() {
		db.close();
		finish();
	}

}
