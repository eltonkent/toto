package com.mobifluence.rage.db;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class DBMainActivity extends MainFeatureActivity{

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("KVDb",
				"Super fast Key Value based Database", KVDBActivity.class));
		listAdapter.add(createItem("ODb",
				"RAGE Object Oriented Database ", ODBActivity.class));
		return listAdapter;
	}
}
