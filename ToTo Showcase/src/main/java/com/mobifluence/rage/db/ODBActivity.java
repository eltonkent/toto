package com.mobifluence.rage.db;

import java.io.File;

import toto.db.odb.Index;
import toto.db.odb.ODb;
import toto.db.odb.ODbException;
import toto.db.odb.ODbFactory;
import toto.io.file.SDCardUtils;
import toto.jobs.TJobResponse;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class ODBActivity extends ProcessingActivity {

	private LinearLayout top;
	private LinearLayout bottom;
	private String path = SDCardUtils.getDirectory().toString() + "/ODB/";
	private File dbPath = new File(path);
	private ODb db;
	private EditText key;
	private EditText value;
	private Index<Car> idx;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);
		top = (LinearLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		View v = getLayoutInflater().inflate(R.layout.db_odb_bottom, null);
		bottom.addView(v);
		if (!SDCardUtils.isMounted()) {
			Toast.makeText(getApplicationContext(),
					"This feature requires an SD card!", 3000).show();
			finish();
			return;
		}

		Button btn = (Button) bottom.findViewById(R.id.button1);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showAuthDialog();
			}
		});

		btn = (Button) bottom.findViewById(R.id.button2);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				delete();
			}
		});

		btn = (Button) bottom.findViewById(R.id.button3);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				read();
			}
		});

		btn = (Button) bottom.findViewById(R.id.button4);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				top.removeAllViews();
			}
		});
		initDB();
	}

	private void insert(Car value) {
		idx.set("ODB", value);
		db.commit();
		top.addView(getKeyValueView("ODB", "Car Object saved! Hashcode: "
				+ value.hashCode()));

	}

	private void delete() {
		try {
			idx.remove("ODB");
			db.commit();
			top.addView(getKeyValueView("ODB", "Object Deleted!"));
		} catch (ODbException e) {
			Toast.makeText(getApplicationContext(), "Requested Object not available in ODb", 3000).show();
		}
	}

	private void read() {
		Car car = idx.get("ODB");
		if (car != null) {
			top.addView(getKeyValueView("ODB", car.toString()));
			top.addView(getKeyValueView("ODB", "Hashcode: " + car.hashCode()));

		} else {
			top.addView(getKeyValueView("ODB", "Object not saved!"));
		}
	}

	private void initDB() {
		File file = new File(path);
		if (!file.exists()) {
			file.mkdir();
		}
		path = path + "rage.odb";
		db = ODbFactory.createOODB();
		db.open(path);
		idx = db.getRootObject();
		if (idx == null) {
			idx = db.createIndex(String.class, true);
			db.setRootObject(idx);
		}
		top.addView(getKeyValueView("ODB", "open at" + path));

		// ODb db = ODbFactory.createOODB();
		// db.open(path);
		// Index<Person> idx = db.getRootObject();
		// if (idx == null) {
		// idx = db.createIndex(String.class, true);
		// db.setRootObject(idx);
		// Log.d("RAGE","Root object created");
		// }
		// Person p1 = new Person();
		// p1.add("tefsdfs");
		// idx.put("test", p1);
		// db.commit();
		// //
		// Person p1=(Person) idx.get("test");
		// Log.d("RAGE","data: "+p1.test.get(0).name);
		// db.close();
	}

	@Override
	protected void success(TJobResponse responses) {

	}

	public void onBackPressed() {
		db.close();
		finish();
	}

	private void showAuthDialog() {
		final Dialog dialog = new Dialog(ODBActivity.this);// builder.create();
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.db_object_dialog);
		final EditText make = (EditText) dialog.findViewById(R.id.make);
		final EditText manu = (EditText) dialog.findViewById(R.id.manu);
		final EditText year = (EditText) dialog.findViewById(R.id.year);
		final EditText type = (EditText) dialog.findViewById(R.id.type);
		Button btn = (Button) dialog.findViewById(R.id.add);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Car car = new Car();
				car.make = make.getText().toString();
				car.manufacturer = manu.getText().toString();
				car.year = year.getText().toString();
				car.type = type.getText().toString();
				insert(car);
				dialog.dismiss();
			}
		});
		dialog.show();
	}
}
