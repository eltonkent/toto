package com.mobifluence.rage.db;

import toto.db.odb.ODbObject;

public class Car  extends ODbObject {
	String make;
	String manufacturer;
	String year;
	String type;

	@Override
	public String toString() {
		return "Car [make=" + make + ", manufacturer=" + manufacturer
				+ ", year=" + year + ", type=" + type + "]";
	}

}
