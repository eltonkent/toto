package com.mobifluence.rage;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class FeatureAdapter extends ArrayAdapter<FeatureItem> {

	public FeatureAdapter(Context context, int textViewResourceId,
			List<FeatureItem> objects) {
		super(context, textViewResourceId, objects);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.listview_featurelist, null);
		FeatureItem item = getItem(position);
		TextView tv = (TextView) view.findViewById(R.id.textView1);
		tv.setText(item.getName());
		tv = (TextView) view.findViewById(R.id.textView2);
		tv.setText(item.getDescription());
		return view;
	}
}
