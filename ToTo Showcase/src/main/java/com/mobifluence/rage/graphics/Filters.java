package com.mobifluence.rage.graphics;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.RageMainActivity;

public class Filters extends RageMainActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();

		listAdapter.add(createItem("Color Filters",
				"Bitmap color manipulation filters", BitmapColorFilters.class));

		listAdapter.add(createItem("Edge Filters",
				"Bitmap edge detection filters", BitmapEdgeFilters.class));

		listAdapter.add(createItem("Blur Filters",
				"Bitmap blur filters", BitmapBlurFilters.class));
		listAdapter.add(createItem("Enhancement Filters",
				"Filter to enhance bitmap appearance", BitmapEnhancementFilters.class));
		listAdapter.add(createItem("Pixelate Filters",
				"Pixelation filters", PixelateFilters.class));
		
		
		listAdapter.add(createItem("Other Utilities", "Other Bitmap Utilities.",
				OtherBitmapUtils.class));
		// listAdapter.add(createItem("Stack Blur",
		// "Stack blurring algorithm", StackBlur.class));

		return listAdapter;
	}

}
