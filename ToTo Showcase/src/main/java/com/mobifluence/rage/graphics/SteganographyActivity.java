package com.mobifluence.rage.graphics;

import toto.bitmap.ToToJavaBitmap;
import toto.bitmap.filters.crypto.Steganography;
import toto.jobs.TJobResponse;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;


public class SteganographyActivity extends ProcessingActivity {


    public void onCreate(Bundle instance) {
        super.onCreate(instance);
//        RageNativeBitmap bitmap= ReadFile.n_readRealBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.hydrangeas));
//        Log.d("Bitmap depth ->",""+bitmap.getDepth());
        ToToJavaBitmap bitmap = new ToToJavaBitmap(getApplicationContext(), R.drawable.girlskin, new BitmapFactory.Options());
        Steganography.encodeMessage(bitmap, "Sefhfghfghfghfgh fgh ffghfghf fghfghfg cret text");
        ImageView iv = new ImageView(getApplicationContext());
        iv.setImageBitmap(bitmap.getBitmap());
        setContentView(iv);
        String text = Steganography.decodeMessage(bitmap);
        Log.d("SECRET TEXT ->", "" + text);

    }


    @Override
    protected void success(TJobResponse responses) {

    }

}
