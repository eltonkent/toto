package com.mobifluence.rage.graphics;

import java.io.IOException;
import java.io.InputStream;

import toto.bitmap.filters.BlurFilters;
import toto.io.IOUtils;
import toto.jobs.TJobResponse;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class BlurDetectActivity extends ProcessingActivity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.threefourthpane);
		LinearLayout bottom=(LinearLayout) findViewById(R.id.bottom);
		LinearLayout top=(LinearLayout) findViewById(R.id.top);
		try {
			InputStream is =getAssets().open("blur.jpg");
			byte[] arr=IOUtils.toByteArray(is);
			ImageView iv=new ImageView(getApplicationContext());
			Bitmap bitmap=BitmapFactory.decodeByteArray(arr, 0, arr.length);
			iv.setImageBitmap(bitmap);
			top.addView(iv);
			boolean isBlurred=BlurFilters.isBlurredNative(arr, bitmap.getWidth(), bitmap.getHeight());
			bottom.addView(getKeyValueView("Image 1 :Is Blurred", ""+isBlurred));
		} catch (IOException e) {
			e.printStackTrace();
		}
//		try {
//			InputStream is =getAssets().open("notblur.jpg");
//			byte[] arr=IOUtils.toByteArray(is);
//			ImageView iv=new ImageView(getApplicationContext());
//			Bitmap bitmap=BitmapFactory.decodeByteArray(arr, 0, arr.length);
//			iv.setImageBitmap(bitmap);
//			top.addView(iv);
//			boolean isBlurred=BlurFilters.n_isBlurred(arr,320, 400);
//			bottom.addView(getKeyValueView("Image 2 :Is Blurred", ""+isBlurred));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		
		
	}
	@Override
	protected void success(TJobResponse responses) {

	}

}
