package com.mobifluence.rage.graphics;

import toto.bitmap.ToToBitmap;
import toto.bitmap.ToToJavaBitmap;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.mobifluence.rage.R;
import toto.bitmap.ToToJavaBitmap;

public abstract class BaseBitmapFiltersActivity extends Activity {

	ImageView image;
	SeekBar slider1;
	SeekBar slider2;

	public void onCreate(Bundle instance) {
		super.onCreate(instance);
		setContentView(R.layout.graphics_bitmapfilters);
		Button apply = (Button) findViewById(R.id.apply);
		image = (ImageView) findViewById(R.id.imageView);
		final ToToBitmap bitmap = new ToToJavaBitmap(this,
				R.drawable.testimage, null);
		image.setImageBitmap(bitmap.getBitmap());
		if (showSilder1()) {
			slider1 = (SeekBar) findViewById(R.id.seekBar1);
			slider1.setMax(getSlider1Max());
			slider1.setVisibility(View.VISIBLE);
			TextView sliderText = (TextView) findViewById(R.id.textView1);
			sliderText.setVisibility(View.VISIBLE);
			sliderText.setText(getSilder1Name());
		}
		if (showSilder2()) {
			slider2 = (SeekBar) findViewById(R.id.seekBar2);
			slider2.setMax(getSlider2Max());
			slider2.setVisibility(View.VISIBLE);
			TextView sliderText = (TextView) findViewById(R.id.textView2);
			sliderText.setVisibility(View.VISIBLE);
			sliderText.setText(getSilder2Name());
		}
		apply.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				int s1 = slider1 != null ? slider1.getProgress() : 0;
				int s2 = slider2 != null ? slider2.getProgress() : 0;
				BitmapProcessInfo info = new BitmapProcessInfo();
				info.bitmap = bitmap;
				info.s1 = s1;
				info.s2 = s2;
				showDialog(1);
				new ProcessFilter().execute(info);

			}
		});
	}

	protected Dialog onCreateDialog(int id, Bundle args) {
		Dialog dialog = new Dialog(BaseBitmapFiltersActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_progress);
		dialog.setCancelable(false);
		return dialog;
	}

	private class BitmapProcessInfo {
		ToToBitmap bitmap;
		int s1, s2;
	}

	private class ProcessFilter extends
			AsyncTask<BitmapProcessInfo, Void, Bitmap> {
		@Override
		protected Bitmap doInBackground(BitmapProcessInfo... params) {
			BitmapProcessInfo info = params[0];
			ToToBitmap res=process(info.bitmap, info.s1, info.s2);
			if(res==null){
				return process(info.bitmap.getBitmap(),info.s1, info.s2);
			}else{
				return res.getBitmap();
			}
		}

		protected void onPostExecute(Bitmap result) {
			dismissDialog(1);
			image.setImageBitmap(result);
		}
	}

	protected abstract boolean showSilder1();

	protected abstract String getSilder1Name();


	protected abstract int getSlider1Max();

	protected abstract boolean showSilder2();

	protected abstract String getSilder2Name();


	protected abstract int getSlider2Max();

	protected abstract ToToBitmap process(ToToBitmap bitmap, int slider1,
			int slider2);
	
	protected abstract Bitmap process(Bitmap bitmap, int slider1,
			int slider2);
}
