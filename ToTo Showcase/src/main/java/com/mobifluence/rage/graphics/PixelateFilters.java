package com.mobifluence.rage.graphics;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.RageMainActivity;

public class PixelateFilters extends RageMainActivity {
	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();

		listAdapter.add(createItem("Pixelate", "Pixelate filter implemented in native code.",
				Pixelate.class));
		return listAdapter;
	}
	
}
