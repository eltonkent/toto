package com.mobifluence.rage.graphics;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.RageMainActivity;

public class BitmapEdgeFilters extends RageMainActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();

		listAdapter.add(createItem("Emboss Filter",
				"Emboss Filter", EmbossFilter.class));
		
		listAdapter.add(createItem("Simple Emboss Filter",
				"Simple Emboss Filter", SimpleEmbossFilter.class));
		
		listAdapter.add(createItem("Canny Edge Filter",
				"Edge detection using canny edge", CannyEdgeFilter.class));

		return listAdapter;
	}

}
