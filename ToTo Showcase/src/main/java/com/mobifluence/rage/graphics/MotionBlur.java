package com.mobifluence.rage.graphics;

import toto.bitmap.ToToBitmap;
import toto.bitmap.filters.BlurFilters;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

public class MotionBlur extends BaseBitmapFiltersActivity {

	@Override
	protected boolean showSilder1() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected String getSilder1Name() {
		// TODO Auto-generated method stub
		return "Angle";
	}


	@Override
	protected int getSlider1Max() {
		// TODO Auto-generated method stub
		return 360;
	}

	@Override
	protected boolean showSilder2() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected String getSilder2Name() {
		// TODO Auto-generated method stub
		return "Rotation";
	}


	@Override
	protected int getSlider2Max() {
		// TODO Auto-generated method stub
		return 180;
	}

	@Override
	protected ToToBitmap process(ToToBitmap bitmap, int slider1, int slider2) {
		
		return null;
	}

	@Override
	protected Bitmap process(Bitmap bitmap, int slider1, int slider2) {
		return BlurFilters.motionBlur(bitmap,  slider1, 0, slider2, 0, true, false, Config.ARGB_8888);
//		return null;
	}

}
