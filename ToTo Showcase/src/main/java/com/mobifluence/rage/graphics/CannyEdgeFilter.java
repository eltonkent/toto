package com.mobifluence.rage.graphics;

import toto.bitmap.ToToBitmap;
import toto.bitmap.filters.EdgeFilters;
import android.graphics.Bitmap;

public class CannyEdgeFilter extends BaseBitmapFiltersActivity {

	@Override
	protected boolean showSilder1() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected String getSilder1Name() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected int getSlider1Max() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected boolean showSilder2() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected String getSilder2Name() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected int getSlider2Max() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected ToToBitmap process(ToToBitmap bitmap, int slider1, int slider2) {
		EdgeFilters.cannyEdge(bitmap);
		return bitmap;
	}

	@Override
	protected Bitmap process(Bitmap bitmap, int slider1, int slider2) {
		// TODO Auto-generated method stub
		return null;
	}

}
