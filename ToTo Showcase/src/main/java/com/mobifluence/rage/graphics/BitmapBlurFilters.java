package com.mobifluence.rage.graphics;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.RageMainActivity;

public class BitmapBlurFilters extends RageMainActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();

		listAdapter.add(createItem("Gaussian Blur", "Gaussian Blur implementation",
				FastGaussian.class));
		listAdapter.add(createItem("Fast Gaussian Blur", "Fast Gaussian Blur implementation",
				Gaussian.class));
		listAdapter.add(createItem("Box Blur", "Box blur implementation",
				BoxBlur.class));
		listAdapter.add(createItem("Stack Blur", "Stack blur implementation",
				StackBlur.class));
		listAdapter.add(createItem("Fractional Blur", "Fractional blur implementation",
				FractionalBlur.class));
		listAdapter.add(createItem("Motion Blur", "MotionBlur implementation",
				MotionBlur.class));
		
		listAdapter.add(createItem("Detect Blur",
				"Detect if a bitmap is blurred.", BlurDetectActivity.class));
		return listAdapter;
	}

}
