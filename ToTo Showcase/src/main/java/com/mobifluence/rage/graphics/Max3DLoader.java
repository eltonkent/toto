package com.mobifluence.rage.graphics;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

import toto.graphics._3D.Camera;
import toto.graphics._3D.FrameBuffer;
import toto.graphics._3D.Light;
import toto.graphics._3D.ModelLoader;
import toto.graphics._3D.T3DObject;
import toto.graphics._3D.T3DScene;
import toto.graphics._3D.SimpleVector;
import toto.graphics.color.RGBColor;
import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MotionEvent;


/**
 * A simple demo. This shows more how to use jPCT-AE than it shows how to write
 * a proper application for Android. It includes basic activity management to
 * handle pause and resume.
 *
 */
public class Max3DLoader extends Activity {

    // Used to handle pause and resume...
    private static Max3DLoader master = null;

    private GLSurfaceView mGLView;
    private MyRenderer renderer = null;
    private FrameBuffer fb = null;
    private T3DScene rage3DScene = null;
    private RGBColor back = new RGBColor(50, 50, 100);

    private float touchTurn = 0;
    private float touchTurnUp = 0;

    private float xpos = -1;
    private float ypos = -1;

    //    private Rage3DObject cube = null;
    private int fps = 0;

    private Light sun = null;

    protected void onCreate(Bundle savedInstanceState) {

//        Logger.log("onCreate");

        if (master != null) {
            copy(master);
        }

        super.onCreate(savedInstanceState);
        mGLView = new GLSurfaceView(getApplication());

        mGLView.setEGLConfigChooser(new GLSurfaceView.EGLConfigChooser() {
            public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
                // Ensure that we get a 16bit framebuffer. Otherwise, we'll fall
                // back to Pixelflinger on some device (read: Samsung I7500)
                int[] attributes = new int[] { EGL10.EGL_DEPTH_SIZE, 16, EGL10.EGL_NONE };
                EGLConfig[] configs = new EGLConfig[1];
                int[] result = new int[1];
                egl.eglChooseConfig(display, attributes, configs, 1, result);
                return configs[0];
            }
        });

        renderer = new MyRenderer();
        mGLView.setRenderer(renderer);
        setContentView(mGLView);

    }

    @Override
    protected void onPause() {
        super.onPause();
        mGLView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGLView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void copy(Object src) {
        try {
//            Logger.log("Copying data from master Activity!");
            Field[] fs = src.getClass().getDeclaredFields();
            for (Field f : fs) {
                f.setAccessible(true);
                f.set(this, f.get(src));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean onTouchEvent(MotionEvent me) {

        if (me.getAction() == MotionEvent.ACTION_DOWN) {
            xpos = me.getX();
            ypos = me.getY();
            return true;
        }

        if (me.getAction() == MotionEvent.ACTION_UP) {
            xpos = -1;
            ypos = -1;
            touchTurn = 0;
            touchTurnUp = 0;
            return true;
        }

        if (me.getAction() == MotionEvent.ACTION_MOVE) {
            float xd = me.getX() - xpos;
            float yd = me.getY() - ypos;

            xpos = me.getX();
            ypos = me.getY();

            touchTurn = xd / -100f;
            touchTurnUp = yd / -100f;
            return true;
        }

        try {
            Thread.sleep(15);
        } catch (Exception e) {
            // No need for this...
        }

        return super.onTouchEvent(me);
    }

    protected boolean isFullscreenOpaque() {
        return true;
    }

    class MyRenderer implements GLSurfaceView.Renderer {

        private long time = System.currentTimeMillis();

        public MyRenderer() {
            loadModel();
        }

        public void onSurfaceChanged(GL10 gl, int w, int h) {
            if (fb != null) {
                fb.dispose();
            }
            fb = new FrameBuffer(gl, w, h);

            if (master == null) {

                rage3DScene = new T3DScene();
                rage3DScene.setAmbientLight(20, 20, 20);

                sun = new Light(rage3DScene);
                sun.setIntensity(250, 250, 250);

                // Create a texture out of the icon...:-)
//                Texture texture = new Texture(BitmapHelper.rescale(BitmapHelper.convert(getResources().getDrawable(R.drawable.icon)), 64, 64));
//                TextureCache.getInstance().addTexture("texture", texture);
//
//                cube = Rage3DPrimitives.getCube(10);
//                cube.calcTextureWrapSpherical();
////                cube.setTexture("texture");
//                cube.strip();
//                cube.build();

                rage3DScene.addObject(o3d);
//                world.addObject(cube);


                Camera cam = rage3DScene.getCamera();
                cam.moveCamera(Camera.CAMERA_MOVEOUT, 50);
                cam.lookAt(o3d.getTransformedCenter());
//                cam.lookAt(cube.getTransformedCenter());

                SimpleVector sv = new SimpleVector();
//                sv.setKey(cube.getTransformedCenter());
                sv.set(o3d.getTransformedCenter());
                sv.y -= 100;
                sv.z -= 100;
                sun.setPosition(sv);
//                MemoryHelper.compact();

                if (master == null) {
//                    Logger.log("Saving master Activity!");
                    master = Max3DLoader.this;
                }
            }
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        }

        public void onDrawFrame(GL10 gl) {
            if (touchTurn != 0) {
                o3d.rotateY(touchTurn);
                touchTurn = 0;
            }

            if (touchTurnUp != 0) {
                o3d.rotateX(touchTurnUp);
                touchTurnUp = 0;
            }

            fb.clear(back);
            rage3DScene.renderScene(fb);
            rage3DScene.draw(fb);
            fb.display();

            if (System.currentTimeMillis() - time >= 1000) {
                fps = 0;
                time = System.currentTimeMillis();
            }
            fps++;
        }

        T3DObject o3d;
        private void loadModel() {
            try {
//                InputStream is = getAssets().open("ogro.md2");
//                o3d = ModelLoader.loadMD2(is, 1f);
//                o3d.build();

                InputStream is = getAssets().open("monster_high.3ds");
                T3DObject[] model = ModelLoader.load3DS(is, 1f);
                o3d = model[0];
                o3d.scale(2f);
//                Rage3DObject temp = null;
//                for (int i = 0; i < model.length; i++) {
//                    temp = model[i];
//                    temp.setCenter(SimpleVector.ORIGIN);
//                    temp.rotateX((float) (-.5 * Math.PI));
//                    temp.rotateMesh();
//                    temp.setRotationMatrix(new Matrix());
//                    o3d = Rage3DObject.mergeObjects(o3d, temp);
//                    o3d.build();
//                }
                o3d.build();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//        private void loadModel() {
//            try {
//                InputStream is = getAssets().open("airboat.obj");
//                Rage3DObject[] model = ModelLoader.loadOBJ(is, null, 2f);
//                o3d= Rage3DObject.mergeAll(model);
////                 o3d = model[0];//new Rage3DObject(0);
////                Rage3DObject temp = null;
////                for (int i = 1; i < model.length; i++) {
////                    temp = model[i];
////                    temp.setCenter(SimpleVector.ORIGIN);
////                    temp.rotateX((float)( -.5*Math.PI));
////                    temp.rotateMesh();
////                    temp.setRotationMatrix(new Matrix());
////                    o3d = Rage3DObject.mergeObjects(o3d, temp);
////                    o3d.build();
////                }
////                Log.d("Total 3d", "length->" + model.length);
//////                o3d=model[0];
////                o3d.build();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }
}
