package com.mobifluence.rage.graphics;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.RageMainActivity;

public class BitmapColorFilters extends RageMainActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();

		listAdapter.add(createItem("GrayScale",
				"Grayscale level", Grayscale.class));
		listAdapter.add(createItem("Temperature",
				"Temperature level", Temperature.class));
		listAdapter.add(createItem("Posterize",
				"Image Posterization", Posterize.class));
		listAdapter.add(createItem("Transparency",
				"Adjust image alpha", Transparency.class));
		listAdapter.add(createItem("Invert",
				"Invert image colors", Invert.class));
		listAdapter.add(createItem("Sepia",
				"Adjust brown pigmentation", Sepia.class));
		listAdapter.add(createItem("Saturate",
				"Adjust image saturation", Saturate.class));
		listAdapter.add(createItem("Apply Glow",
				"Image glow", Glow.class));
		listAdapter.add(createItem("Apply Plasma",
				"Image Plasma", Plasma.class));
		listAdapter.add(createItem("Tint",
				"Pixel Tint", Tint.class));
		listAdapter.add(createItem("Decrease Depth",
				"Decrease Image Depth", DecreaseDepth.class));
		listAdapter.add(createItem("Oil Paint",
				"Oil Paint effect", OilPaint.class));
		listAdapter.add(createItem("Solarize",
				"Solarize effect", Solarize.class));
		
		
		return listAdapter;
	}

}
