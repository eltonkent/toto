package com.mobifluence.rage.graphics;

import toto.bitmap.ToToBitmap;
import toto.bitmap.ToToJavaBitmap;
import toto.bitmap.filters.ArtisticFilters;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

public class Plasma extends BaseBitmapFiltersActivity {

	@Override
	protected boolean showSilder1() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected String getSilder1Name() {
		// TODO Auto-generated method stub
		return "Turbulence";
	}


	@Override
	protected int getSlider1Max() {
		// TODO Auto-generated method stub
		return 100;
	}

	@Override
	protected boolean showSilder2() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected String getSilder2Name() {
		// TODO Auto-generated method stub
		return "";
	}


	@Override
	protected int getSlider2Max() {
		// TODO Auto-generated method stub
		return 100;
	}

	@Override
	protected ToToBitmap process(ToToBitmap bitmap, int slider1, int slider2) {
		return null;
		
	}

	protected Bitmap process(Bitmap bitmap, int slider1, int slider2) {
		ToToBitmap rbm=new ToToJavaBitmap(bitmap);
		ArtisticFilters.plasma(rbm, slider1 / 100, false,
				false, Config.ARGB_8888);
		return rbm.getBitmap();
	}

}
