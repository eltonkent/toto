package com.mobifluence.rage.graphics;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.RageMainActivity;

public class OtherBitmapUtils extends RageMainActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();

		
		listAdapter.add(createItem("Steganography", "Steganography using LSB.",
				SteganographyActivity.class));
		// listAdapter.add(createItem("Stack Blur",
		// "Stack blurring algorithm", StackBlur.class));

		return listAdapter;
	}

}
