package com.mobifluence.rage.graphics;

import toto.bitmap.ToToBitmap;
import toto.bitmap.filters.BlurFilters;
import android.graphics.Bitmap;

public class FractionalBlur extends BaseBitmapFiltersActivity {

	@Override
	protected boolean showSilder1() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected String getSilder1Name() {
		// TODO Auto-generated method stub
		return "Radius";
	}


	@Override
	protected int getSlider1Max() {
		// TODO Auto-generated method stub
		return 100;
	}

	@Override
	protected boolean showSilder2() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected String getSilder2Name() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected int getSlider2Max() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected ToToBitmap process(ToToBitmap bitmap, int slider1, int slider2) {
		int[] pixels=bitmap.getPixels();
		int[] out=new int[bitmap.getWidth()*bitmap.getHeight()];
		BlurFilters.fractionalBLur(pixels, out, bitmap.getWidth(), bitmap.getHeight(), slider1);
		bitmap.setPixels(out, bitmap.getWidth(), bitmap.getHeight());
//		 BlurFilters.stackBlur(bitmap, slider1);
		 return bitmap;
	}

	@Override
	protected Bitmap process(Bitmap bitmap, int slider1, int slider2) {
		
		return null;
	}

}
