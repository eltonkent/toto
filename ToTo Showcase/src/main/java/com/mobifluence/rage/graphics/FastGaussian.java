package com.mobifluence.rage.graphics;

import toto.bitmap.ToToBitmap;
import toto.bitmap.filters.BlurFilters;
import android.graphics.Bitmap;

public class FastGaussian extends BaseBitmapFiltersActivity {

	@Override
	protected boolean showSilder1() {
		return true;
	}

	@Override
	protected String getSilder1Name() {
		return "Brightness";
	}


	@Override
	protected int getSlider1Max() {
		return 200;
	}

	@Override
	protected boolean showSilder2() {
		return false;
	}

	@Override
	protected String getSilder2Name() {
		return null;
	}


	@Override
	protected int getSlider2Max() {
		return 0;
	}

	@Override
	protected ToToBitmap process(ToToBitmap bitmap, int slider1, int slider2) {
		BlurFilters.fastGaussianBlur(bitmap, slider1);
		return bitmap;
	}

	@Override
	protected Bitmap process(Bitmap bitmap, int slider1, int slider2) {
		return null;
	}

}
