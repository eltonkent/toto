package com.mobifluence.rage.graphics;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

/**
 * Created by Elton Kent on 12/31/13.
 */
public class ThreeDBaseActivity extends MainFeatureActivity {
    @Override
    protected List<FeatureItem> getFeatureList() {
        List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("Load 3DS file",
                "Loading a 3DS file format",
                Max3DLoader.class));
        listAdapter.add(createItem("Load OBJ file",
                "Loading a OBJ file format",
                ObjLoader.class));
        listAdapter.add(createItem("Load MD2 file",
                "Loading a MD2 file format",
                MD2Loader.class));
        return listAdapter;
    }
}
