package com.mobifluence.rage.graphics;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.RageMainActivity;

public class BitmapEnhancementFilters extends RageMainActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();

		listAdapter.add(createItem("Sharpen", "Performs simple 3x3 sharpening",
				Sharpen.class));
		listAdapter.add(createItem("Gain and Bias", "Set Gain and Bias",
				GainBias.class));
		listAdapter.add(createItem("Exposure", "Set Exposure",
				Exposure.class));
		listAdapter.add(createItem("Reduce Noise", "Simple Noise reduction",
				ReduceNoise.class));
		listAdapter.add(createItem("DeSpeckle", "Remove Speckles",
				Despeckle.class));
		listAdapter.add(createItem("Brightness and Contrast", "Set Brightness and contrast",
				BrightnessAndContrast.class));
		return listAdapter;
	}

}
