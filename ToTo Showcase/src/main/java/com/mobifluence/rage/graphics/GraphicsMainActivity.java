package com.mobifluence.rage.graphics;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;
import com.mobifluence.rage.ui.GIFImageView;
import com.mobifluence.rage.ui.SVGImageView;

public class GraphicsMainActivity extends MainFeatureActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("3D Graphics",
				"3D loaders, renderers and utilities",
				ThreeDBaseActivity.class));
		listAdapter.add(createItem("Bitmap Filters",
				"Color, Enhancement, Edge, Pixellate and Blur filters",
				Filters.class));
        listAdapter.add(createItem("SVG Rendering",
                "Rendering SVG files",
                SVGImageView.class));
        listAdapter.add(createItem("GIF Rendering",
                "Rendering Animated GIF files",
                GIFImageView.class));
	return listAdapter;
	}

}
