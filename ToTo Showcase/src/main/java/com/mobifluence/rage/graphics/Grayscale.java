package com.mobifluence.rage.graphics;

import toto.bitmap.ToToBitmap;
import toto.bitmap.filters.ColorFilters;
import android.graphics.Bitmap;

public class Grayscale extends BaseBitmapFiltersActivity {

	@Override
	protected boolean showSilder1() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected String getSilder1Name() {
		// TODO Auto-generated method stub
		return "Saturation";
	}


	@Override
	protected int getSlider1Max() {
		// TODO Auto-generated method stub
		return 100;
	}

	@Override
	protected boolean showSilder2() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected String getSilder2Name() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected int getSlider2Max() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected ToToBitmap process(ToToBitmap bitmap, int slider1, int slider2) {
		ColorFilters.grayScale(bitmap, slider1);
		return bitmap;
	}

	@Override
	protected Bitmap process(Bitmap bitmap, int slider1, int slider2) {
		// TODO Auto-generated method stub
		return null;
	}

}
