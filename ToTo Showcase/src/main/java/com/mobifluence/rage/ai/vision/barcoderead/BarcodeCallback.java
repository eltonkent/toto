package com.mobifluence.rage.ai.vision.barcoderead;

/**
 * Gets the results of image processing.
 */
public interface BarcodeCallback {
	public void process(String result, byte[] data, int w, int h);
}
