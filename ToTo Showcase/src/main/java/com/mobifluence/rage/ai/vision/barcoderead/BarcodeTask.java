package com.mobifluence.rage.ai.vision.barcoderead;

import toto.di.barcode.ToToBarcode;
import android.util.Log;

/**
 * Class performs asynchronous processing of images. <br/>
 * Will be instanced inside Camera.PreviewCallback and using inside ThreadPool.
 * 
 * <pre>
 * The <b>constructor</b> method accepts a byte array, width, height and BarcodeCallback implementation 
 * the width and height of an image taken from the outer class.
 * </pre>
 */
public class BarcodeTask {

	private static final String TAG = "RageBarcode";

	private final BarcodeCallback callback;

	private final byte[] data;
	private final int width;
	private final int height;

	String result;

	public BarcodeTask(final byte[] data, final int width, final int height,
			final BarcodeCallback callback) {
		this.data = data;
		this.width = width;
		this.height = height;

		this.callback = callback;
	}

	/**
	 * This method will be called from thread inside ThreadPool.<br/>
	 * Result will be sending to BarcodeCallback.
	 * 
	 * @see com.blogspot.lashchenko.azbar.barcode.BarcodeCallback
	 */
	public void process() {
		Log.v(TAG, "process()");

		start();
		end();
	}

	private void start() {
		Log.v(TAG, "start()");

		if (data == null) {
			Log.v(TAG, "byte[] data is NULL! Nothing to handling.");
			return;
		}

		final String decoded = ToToBarcode.decodeNative(width, height, data);
		result = decoded;

		// if( decoded != null ) {
		// BarcodeContent content = BarcodeParser.getInstance().parse(decoded);
		// mResult = content.toString();
		// }
	}

	private void end() {
		Log.v(TAG, "end()");
		callback.process(result, data, width, height);
	}
}
