package com.mobifluence.rage.ai.vision.ocr;

import android.content.SharedPreferences;


public class OcrCharacterHelper {
  
  public static final String KEY_CHARACTER_BLACKLIST_ENGLISH = "preference_character_blacklist_english";
  
  
  private OcrCharacterHelper() {} // Private constructor to enforce noninstantiability
  
  public static String getDefaultBlacklist(String languageCode) {
    //final String DEFAULT_BLACKLIST = "`~|";
    return "";
  }
  
  public static String getDefaultWhitelist(String languageCode) {
    return "!?@#$%&*()<>_-+=/.,:;'\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; 
    
  }

  public static String getBlacklist(SharedPreferences prefs, String languageCode) {

     return prefs.getString(KEY_CHARACTER_BLACKLIST_ENGLISH, getDefaultBlacklist(languageCode)); 

     
  }
  
  public static String getWhitelist(SharedPreferences prefs, String languageCode) {

    	 return prefs.getString(KEY_CHARACTER_BLACKLIST_ENGLISH, getDefaultWhitelist(languageCode));
    	  
  }
  
  public static void setBlacklist(SharedPreferences prefs, String languageCode, String blacklist) {

    if (languageCode.equals("eng")) { prefs.edit().putString(KEY_CHARACTER_BLACKLIST_ENGLISH, blacklist).commit(); }
    
    else {
      throw new IllegalArgumentException();
    }    
  }
  
  public static void setWhitelist(SharedPreferences prefs, String languageCode, String whitelist) {
    prefs.edit().putString(KEY_CHARACTER_BLACKLIST_ENGLISH, whitelist).commit();
     
  }
}
