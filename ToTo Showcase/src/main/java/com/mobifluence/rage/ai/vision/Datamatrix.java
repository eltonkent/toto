package com.mobifluence.rage.ai.vision;

import java.util.HashMap;
import java.util.Map;

import toto.di.barcode.ToToBarcode;
import toto.di.barcode.ToToBarcode.EncodeHints;
import toto.di.barcode.BarcodeType;
import toto.jobs.TJobResponse;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

/**
 * Created by Elton Kent on 12/26/13.
 */
public class Datamatrix extends ProcessingActivity {

    int width;
    int height;
    ImageView iv;
    CheckBox cb;

    protected void onCreate(Bundle instance) {
        super.onCreate(instance);
        setContentView(R.layout.splitpane);
        LinearLayout top = (LinearLayout) findViewById(R.id.top);
        LinearLayout bottom = (LinearLayout) findViewById(R.id.bottom);
        final EditText text = new EditText(getApplicationContext());
        text.setText("Rage By Mobifluence");
        text.setHint("Datamatrix Data");
        bottom.addView(text);
        Button btnCheck = new Button(getApplicationContext());
        btnCheck.setText("Generate Datamatrix");
        bottom.addView(btnCheck);
        cb=new CheckBox(getApplicationContext());
        cb.setText("Custom color");
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                setData(text.getText().toString());
            }
        });
        bottom.addView(cb);
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setData(text.getText().toString());
            }
        });

        iv = new ImageView(getApplicationContext());
        iv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                width = iv.getWidth();
                height = 400;
                setData("Rage By Mobifluence");
            }
        });
        top.addView(iv);
    }

    public void setData(String data) {
        try {
            Bitmap bm=null;
            if(cb.isChecked()){
            	Map<ToToBarcode.EncodeHints,Integer> hints=new HashMap<ToToBarcode.EncodeHints, Integer>();
            	hints.put(EncodeHints.HEIGHT, height);
            	hints.put(EncodeHints.WIDTH, width);
            	hints.put(EncodeHints.BG_COLOR, 0xFFFFFFFF);
            	hints.put(EncodeHints.FG_COLOR, 16738816);
                bm = ToToBarcode.encode(BarcodeType.DATA_MATRIX, data,hints);
            }else{
            bm = ToToBarcode.encode(BarcodeType.DATA_MATRIX, data, width, height);
            }
            iv.setImageBitmap(bm);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void success(TJobResponse responses) {

    }
}
