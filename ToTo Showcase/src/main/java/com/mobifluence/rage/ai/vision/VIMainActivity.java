package com.mobifluence.rage.ai.vision;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;
import com.mobifluence.rage.ai.vision.barcoderead.BCReadActivity;
import com.mobifluence.rage.ai.vision.ocr.OCRActivity;

public class VIMainActivity extends MainFeatureActivity {

	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("Optical Character Recognition",
				"Read text from android bitmaps",
				OCRActivity.class));
		listAdapter.add(createItem("Skin Detection",
				"Detect skin regions from android bitmaps",
				SkinDetection.class));
//        listAdapter.add(createItem("Read Barcode",
//                "Detect and Read barcode",
//                BarcodeActivity.class));
        listAdapter.add(createItem("Reading Barcode",
                "Super fast Barcode reading using native code.",
                BCReadActivity.class));
        listAdapter.add(createItem("Encode Barcode",
                "Features of the Rage Barcode component",
                BarcodeActivity.class));
		return listAdapter;
	}
}
