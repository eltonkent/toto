package com.mobifluence.rage.ai.vision;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class BarcodeActivity extends MainFeatureActivity {

    protected List<FeatureItem> getFeatureList() {
        List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("QR Code",
                "Generating QR codes",
                QRCode.class));
        listAdapter.add(createItem("Aztec",
                "Generating 2D Aztec codes",
                Aztec.class));
        listAdapter.add(createItem("Codabar",
                "Generating Codabar codes",
                Codabar.class));
        listAdapter.add(createItem("Code39",
                "Generating Codabar codes",
                Code39.class));
        listAdapter.add(createItem("Code128",
                "Generating Code128 codes",
                Code128.class));
        listAdapter.add(createItem("DataMatrix",
                "Generating DataMatrix",
                Datamatrix.class));
        listAdapter.add(createItem("PDF417",
                "Generating PDF417",
                PDF417.class));
        listAdapter.add(createItem("ITF",
                "Generating PDF417",
                ITF.class));
        listAdapter.add(createItem("EAN-8",
                "Generating EAN-8",
                EAN8.class));
        listAdapter.add(createItem("EAN-13",
                "Generating EAN-13",
                EAN13.class));
        listAdapter.add(createItem("UPC-A", "Generating UPC-A",
                UPC_A.class));
        return listAdapter;
    }
}
