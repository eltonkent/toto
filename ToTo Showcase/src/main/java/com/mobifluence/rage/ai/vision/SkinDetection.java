package com.mobifluence.rage.ai.vision;

import toto.di.skindetect.SkinDetector;
import toto.bitmap.ToToBitmap;
import toto.bitmap.ToToJavaBitmap;
import toto.jobs.TJobResponse;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class SkinDetection extends ProcessingActivity {

	private ImageView iv;
	private LinearLayout paramParent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);
		LinearLayout top=(LinearLayout) findViewById(R.id.top);
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.girlskin);
		ToToBitmap rBitmap=new ToToJavaBitmap(bitmap);
		iv =new ImageView(getApplicationContext());
		iv.setImageBitmap(bitmap);
		top.addView(iv);
		paramParent = (LinearLayout) findViewById(R.id.bottom);
        int[] values=new int[3];
        values[0]=SkinDetector.getSkinPixelCount(rBitmap);
        values[1]=SkinDetector.getSkinRegions(rBitmap).size();
        values[2]=0;
        paramParent.addView(getKeyValueView("Skin Pixel Count", ""+values[0]));
        paramParent.addView(getKeyValueView("Skin Regions", ""+2));
        paramParent.addView(getKeyValueView("Excessive Skin", ""+true));
	}

    @Override
    protected void success(TJobResponse responses) {

    }
	
	/*private class SkinDetect extends RageJob{
		public SkinDetect(Context context) {
			super(context);
		}

		@Override
		protected RageJobResponse<int[]> doInBackground(Object... params) {
			RageJobResponse<int[]> resp=new RageJobResponse<int[]>();
			int[] values=new int[3];
            values[0]=SkinDetector.getSkinPixelCount((RageBitmap)params[0]);
            values[1]=SkinDetector.getSkinRegions((RageBitmap)params[0]).size();
            values[2]=0;
			resp.setData(values);
			return resp;
		}
		
	}*/

	/*@Override
	public void success(RageJobResponse responses) {
		
		int[] result=(int[]) responses.getData();
		paramParent.addView(getKeyValueView("Pixel Count", ""+result[0]));
		paramParent.addView(getKeyValueView("Skin Regions", ""+2));
		paramParent.addView(getKeyValueView("Excessive Skin", ""+true));
	}*/

	
}
