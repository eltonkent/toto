package com.mobifluence.rage.security;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class Hashing  extends MainFeatureActivity{

	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("XXHash",
				"32 bit XXhash implementation",
				XXhashActivity.class));
		listAdapter.add(createItem("Bob Jenkins Hash",
				"64 bit Jenkins hash implementation",
				JenkinsHashActivity.class));
		listAdapter.add(createItem("Murmur2 Hash",
				"32 bit Murmur2 Hash implementation",
				Mumur2HashActivity.class));
		
		listAdapter.add(createItem("Sip Hash",
				"64 bit Sip Hash implementation",
				SipHashActivity.class));
		listAdapter.add(createItem("Spooky Hash",
				"64/32 bit Spooky Hash implementation",
				SpookyHashActivity.class));
		listAdapter.add(createItem("City Hash",
				"64/32 bit City Hash implementation",
				CityHashActivity.class));
		listAdapter.add(createItem("City Hash With Seed",
				"64/32w bit City Hash implementation",
				CityHashSeedActivity.class));
		
		return listAdapter;
	}
}
