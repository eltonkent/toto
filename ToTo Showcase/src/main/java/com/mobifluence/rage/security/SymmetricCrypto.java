package com.mobifluence.rage.security;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public  class SymmetricCrypto extends MainFeatureActivity {

    protected List<FeatureItem> getFeatureList() {
        List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("FOG",
                "FOG cryptography",
                FOGCrypto.class));
        listAdapter.add(createItem("ICE",
                "ICE cryptography",
                Hashing.class));
        listAdapter.add(createItem("XTEA",
                "XTEA cryptography",
                Hashing.class));
        return listAdapter;
    }
}
