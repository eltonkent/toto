package com.mobifluence.rage.security;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class SecurityActivity extends MainFeatureActivity {

	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
//        listAdapter.add(createItem("Symmetric Cryptography",
//                "Numerous Symmetric Cryptography algorithms",
//                SymmetricCrypto.class));
		listAdapter.add(createItem("Non-Cryptographic Hashing",
				"Numerous non crypto hashing functions",
				Hashing.class));
		listAdapter.add(createItem("Cryptographic Hashing",
				"Numerous crypto hashing functions",
				CryptoHashing.class));
		return listAdapter;
	}
}
