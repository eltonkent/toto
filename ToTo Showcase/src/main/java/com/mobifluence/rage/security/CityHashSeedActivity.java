package com.mobifluence.rage.security;

import toto.jobs.TJobResponse;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class CityHashSeedActivity extends ProcessingActivity {

	private LinearLayout top;
	private LinearLayout bottom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);

		top = (LinearLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		final EditText seed = new EditText(getApplicationContext());
		seed.setHint("Hash Seed");
		seed.setInputType(InputType.TYPE_CLASS_NUMBER);
		bottom.addView(seed);
		View bottomView = getLayoutInflater().inflate(
				R.layout.util_snappy_bottom, bottom);

		final EditText text = (EditText) bottomView
				.findViewById(R.id.editText1);
		text.setHint("Text to hash");
		Button compress = (Button) bottomView.findViewById(R.id.compress);
		compress.setText("Get City Hash");
		compress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!seed.getText().toString().equals("")) {
					top.removeAllViews();
					String txt = text.getText().toString();
					byte[] byt = txt.getBytes();
					long hsh = toto.security.hash.nativ.CityHash
							.digest(byt, 0, byt.length,
									Long.parseLong(seed.getText().toString()));
					top.addView(getKeyValueView("City Hash", "" + hsh));
				}
			}
		});
	}

	@Override
	protected void success(TJobResponse responses) {

	}

}
