package com.mobifluence.rage.security;

import toto.jobs.TJobResponse;
import toto.security.hash.BobJenkinsHash;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class JenkinsHashActivity extends ProcessingActivity {

	
	private LinearLayout  top;
	private LinearLayout bottom;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);
		
		top = (LinearLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		View bottomView=getLayoutInflater().inflate(R.layout.util_snappy_bottom, bottom);
		final EditText text=(EditText) bottomView.findViewById(R.id.editText1);
		text.setHint("Text to hash");
		Button compress=(Button) bottomView.findViewById(R.id.compress);
		compress.setText("Get Jenkins Hash");
		compress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				top.removeAllViews();
				String txt=text.getText().toString();
				byte[] byt=txt.getBytes();
				BobJenkinsHash hash=new BobJenkinsHash();
				long hsh =hash.hash(byt);
				top.addView(getKeyValueView("Jenkins Hash",""+hsh));
			}
		});
	}
	
	
	@Override
	protected void success(TJobResponse responses) {
		
	}

}
