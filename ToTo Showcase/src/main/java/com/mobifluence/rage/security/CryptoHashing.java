package com.mobifluence.rage.security;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class CryptoHashing  extends MainFeatureActivity{

	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("Haval Hash",
				"Haval Fingerprints implementation",
				HavalHashActivity.class));
		listAdapter.add(createItem("RipeMD128",
				"RipeMD128 implementation",
				RipeMD128HashActivity.class));
		listAdapter.add(createItem("RipeMD160",
				"RipeMD160 Fingerprints implementation",
				RipeMD160HashActivity.class));
		listAdapter.add(createItem("Tiger Hash",
				"Tiger Hash implementation",
				TigerHashActivity.class));
		listAdapter.add(createItem("Whirlpool Hash",
				"Whirlpool Hash implementation",
				WhirlpoolHashActivity.class));
		
		return listAdapter;
	}
}
