package com.mobifluence.rage.security;

import toto.jobs.TJobResponse;
import toto.security.HexUtils;
import toto.security.hash.crypto.HavalHash;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class HavalHashActivity extends ProcessingActivity {

	
	private LinearLayout  top;
	private LinearLayout bottom;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);
		
		top = (LinearLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		View bottomView=getLayoutInflater().inflate(R.layout.util_snappy_bottom, bottom);
		
		final EditText text=(EditText) bottomView.findViewById(R.id.editText1);
		text.setHint("Text to hash");
		Button compress=(Button) bottomView.findViewById(R.id.compress);
		compress.setText("Get Haval Hash");
		
		compress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				top.removeAllViews();
				String txt=text.getText().toString();
				byte[] byt=txt.getBytes();
				HavalHash hsh=new HavalHash();
				hsh.update(byt);
				byte[] fin=hsh.digest();
				
				top.addView(getKeyValueView("Haval Hash",HexUtils.toHexString(fin)));
			}
		});
	}
	
	
	@Override
	protected void success(TJobResponse responses) {
		
	}

}
