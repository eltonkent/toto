package com.mobifluence.rage.security;

import toto.jobs.TJobResponse;
import toto.security.HexUtils;
import toto.security.crypto.symmetric.ICE;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class FOGCrypto extends ProcessingActivity {
    private LinearLayout top;
    private LinearLayout bottom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splitpane);

        top = (LinearLayout) findViewById(R.id.top);
        bottom = (LinearLayout) findViewById(R.id.bottom);
        View bottomView=getLayoutInflater().inflate(R.layout.util_snappy_bottom, bottom);

        final EditText text=(EditText) bottomView.findViewById(R.id.editText1);
        text.setHint("Text to encrypt");

        final EditText key=new EditText(getApplicationContext());
        key.setHint("Encryption Key");
        key.setInputType(InputType.TYPE_CLASS_NUMBER);
        bottom.addView(key,0);

        Button compress=(Button) bottomView.findViewById(R.id.compress);
        compress.setText("Get Fog Crypto");

        compress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                top.removeAllViews();
                String txt=text.getText().toString();
                byte[] byt=txt.getBytes();
                ICE fg=new ICE(4);
                fg.setKey(key.getText().toString().getBytes());
                fg.encrypt(byt,0,byt.length-1);
                top.addView(getKeyValueView("Fog Crypto", HexUtils.toHexString(byt)));
            }
        });
    }

    @Override
    protected void success(TJobResponse responses) {

    }
}
