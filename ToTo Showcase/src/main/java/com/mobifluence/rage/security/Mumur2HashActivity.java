package com.mobifluence.rage.security;

import toto.jobs.TJobResponse;
import toto.security.hash.Murmur2Hash;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class Mumur2HashActivity extends ProcessingActivity {

	
	private LinearLayout  top;
	private LinearLayout bottom;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);
		
		top = (LinearLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		View bottomView=getLayoutInflater().inflate(R.layout.util_snappy_bottom, bottom);
		
		final EditText text=(EditText) bottomView.findViewById(R.id.editText1);
		text.setHint("Text to hash");
		Button compress=(Button) bottomView.findViewById(R.id.compress);
		compress.setText("Get Murmur2 Hash");
		
		compress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				top.removeAllViews();
				String txt=text.getText().toString();
				byte[] byt=txt.getBytes();
				int hsh =Murmur2Hash.digest(byt, 0, byt.length);
				top.addView(getKeyValueView("Mumur2 Hash",""+hsh));
			}
		});
	}
	
	
	@Override
	protected void success(TJobResponse responses) {
		
	}

}
