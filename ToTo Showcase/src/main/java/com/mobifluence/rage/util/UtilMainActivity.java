package com.mobifluence.rage.util;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class UtilMainActivity extends MainFeatureActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter
				.add(createItem(
						"Data Compression",
						"Various Compression Algorithm implementations",
						Compression.class));
		listAdapter
		.add(createItem(
				"Text Validation",
				"Validation for text input using RegEx and Checksum verification. ",
				TextValidation.class));
		listAdapter
		.add(createItem(
				"PDF Generation",
				"Create PDF files",
				PDFCreator.class));

		return listAdapter;

	}
}
