package com.mobifluence.rage.util;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class Compression extends MainFeatureActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter
				.add(createItem(
						"Snappy Compression",
						"Demonstrates the compression speed using the wrapper built for snappy",
						SnappyActivity.class));
		listAdapter.add(createItem("QuickLZ Compression",
				"Samples test for QuickLZ", QuickLZActivity.class));
		listAdapter.add(createItem("LZ4 Compression",
				"Samples test for LZ4", LZ4Activity.class));

		return listAdapter;
	}

}
