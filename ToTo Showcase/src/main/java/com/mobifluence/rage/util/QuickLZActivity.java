package com.mobifluence.rage.util;

import toto.jobs.TJobResponse;
import toto.util.zip.QuickLZ;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class QuickLZActivity extends ProcessingActivity {
	LinearLayout top;
	LinearLayout bottom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);
		top = (LinearLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		View bottomView = getLayoutInflater().inflate(
				R.layout.util_snappy_bottom, bottom);
		final EditText text = (EditText) bottomView.findViewById(R.id.editText1);
		View compress = bottomView.findViewById(R.id.compress);
		compress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				top.removeAllViews();
				byte[] test= text.getText().toString().getBytes();
				Log.d("Test","actual  length "+test.length);
				long time=System.nanoTime();
				byte[] dest=QuickLZ.doCompress(test, 1);
				time=System.nanoTime()-time;
				Log.d("Test","dest  length "+dest.length+" ["+time+" (ns)]");
			}
		});
	}

	@Override
	protected void success(TJobResponse responses) {

	}

}
