package com.mobifluence.rage.util;

import java.io.IOException;

import toto.jobs.TJobResponse;
import toto.util.zip.nativ.Snappy;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class SnappyActivity extends ProcessingActivity {

	LinearLayout  top;
	LinearLayout bottom;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);
		top = (LinearLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		View bottomView=getLayoutInflater().inflate(R.layout.util_snappy_bottom, bottom);
		final EditText text=(EditText) bottomView.findViewById(R.id.editText1);
		View compress=bottomView.findViewById(R.id.compress);
		compress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				top.removeAllViews();
				String txt=text.getText().toString();
				byte[] uncompressed=txt.getBytes();
				top.addView(getKeyValueView("Before Compression","Bytes "+ uncompressed.length));
				try {
					long time=System.nanoTime();
					byte[] compressed=Snappy.doCompress(uncompressed);
					time=System.nanoTime()-time;
					top.addView(getKeyValueView("After Compression","Bytes: "+compressed.length+" ["+time+" ns]"));
					time=System.nanoTime();
					uncompressed=Snappy.uncompress(compressed);
					time=System.nanoTime()-time;
					top.addView(getKeyValueView("Decompression","Bytes: "+uncompressed.length+" ["+time+" ns]"));
					top.addView(getKeyValueView("Value",new String(uncompressed)));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
	@Override
	protected void success(TJobResponse responses) {

	}

}
