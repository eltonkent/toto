package com.mobifluence.rage.util;

import java.io.IOException;

import toto.jobs.TJobResponse;
import toto.util.zip.nativ.LZ4;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class LZ4Activity extends ProcessingActivity {
	LinearLayout top;
	LinearLayout bottom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splitpane);
		top = (LinearLayout) findViewById(R.id.top);
		bottom = (LinearLayout) findViewById(R.id.bottom);
		View bottomView = getLayoutInflater().inflate(
				R.layout.util_snappy_bottom, bottom);
		final EditText text = (EditText) bottomView.findViewById(R.id.editText1);
		View compress = bottomView.findViewById(R.id.compress);
		compress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				top.removeAllViews();
				byte[] test= text.getText().toString().getBytes();
				top.addView(getKeyValueView("Before Compression","Bytes: "+test.length));
				long time=System.nanoTime();
				byte[] dest;
				try {
					dest = LZ4.compressHC(test);
					time=System.nanoTime()-time;
					top.addView(getKeyValueView("After Compression","Bytes: "+dest.length+" ["+time+" (ns)]"));
					time=System.nanoTime();
					byte[] decompres=LZ4.decompressFast(dest,test.length);
					time=System.nanoTime()-time;
					top.addView(getKeyValueView("Decompression","Bytes: "+decompres.length+" ["+time+" (ns)]"));
					top.addView(getKeyValueView("Value",new String(decompres)));
					
				} catch (IOException e) {
					e.printStackTrace();
					top.addView(getKeyValueView("Error","Could not perform de/compression"));
				}
			}
		});
	}

	@Override
	protected void success(TJobResponse responses) {

	}

}
