package com.mobifluence.rage.util;

import toto.text.ValidationUtils;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobifluence.rage.R;

public class TextValidation extends Activity {

	public void onCreate(Bundle instance) {
		super.onCreate(instance);
		setContentView(R.layout.util_textvalidation);
		final EditText zip=(EditText) findViewById(R.id.editText1);
		final EditText ssn=(EditText) findViewById(R.id.editText2);
		final EditText cc=(EditText) findViewById(R.id.editText3);
		
		findViewById(R.id.validate).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				StringBuilder builder=new StringBuilder();
				String text=zip.getText().toString();
				
				if(!ValidationUtils.isValidUSZip(text)){
					builder.append("Invalid Zip code");
					builder.append("\n");
				}
				text=ssn.getText().toString();
				if(!ValidationUtils.isValidSNN(text)){
					builder.append("Invalid SSN");
					builder.append("\n");
				}
				text=cc.getText().toString();
				if(!ValidationUtils.isValidCreditCard(text)){
					builder.append("Invalid credit or debit card");
				}
				if(!(builder.length()>0)){
					builder.append("No issues. Everything looks good!");
				}
				
				Toast.makeText(getApplicationContext(), builder.toString(), 6000).show();
			}
		});
		
	}
}
