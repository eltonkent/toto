package com.mobifluence.rage.ui;

import toto.ui.utils.ViewUtils;
import android.app.Activity;
import android.graphics.BitmapFactory.Options;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobifluence.rage.R;

public class WebImageView extends Activity {

	public void onCreate(Bundle instance) {
		super.onCreate(instance);
		setContentView(R.layout.splitpane);
		LinearLayout top = (LinearLayout) findViewById(R.id.top);
		LinearLayout bottom = (LinearLayout) findViewById(R.id.bottom);
		top.addView(getLayoutInflater().inflate(R.layout.ui_webimageview, null));
		final toto.ui.widget.imageview.TImageView iv = (toto.ui.widget.imageview.TImageView) findViewById(R.id.zoom);
        iv.allowZoomGestures(true);
        CheckBox cb= (CheckBox) top.findViewById(R.id.checkBox);
        cb.setChecked(true);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                iv.allowZoomGestures(isChecked);
            }
        });
		bottom.addView(getLayoutInflater().inflate(R.layout.ui_webimage_bottom,
				null));
		final EditText text = (EditText) bottom.findViewById(R.id.editText1);
		text.setText("https://www.google.com/images/srpr/logo11w.png");
		Button btn = (Button) bottom.findViewById(R.id.button1);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				iv.setImageUrl(text.getText().toString(), new Options());
			}
		});
		iv.setImageUrl("https://www.google.com/images/srpr/logo11w.png",
				new Options());
        ViewUtils.hideSoftInput(text);
	}
}
