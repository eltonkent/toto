package com.mobifluence.rage.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.mobifluence.rage.R;

public class GlassDrawable extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ui_glassdrawable);
		View view=findViewById(R.id.glass1);
		view.setBackground(new toto.ui.drawable.GlassDrawable(getApplicationContext()));
		
	}
}
