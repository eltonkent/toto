package com.mobifluence.rage.ui;

import toto.ui.widget.adapterview.ScrollingInfoListView;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mobifluence.rage.R;

public class ScrollingInfo extends Activity {

	public void onCreate(Bundle instance) {
		super.onCreate(instance);
		setContentView(R.layout.ui_scrollinfo);
		toto.ui.widget.adapterview.ScrollingInfoListView lv = (toto.ui.widget.adapterview.ScrollingInfoListView) findViewById(R.id.sortableListview);
		String[] values = getResources().getStringArray(R.array.listviewinfo_rage);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getApplicationContext(), R.layout.listview_rage,
				R.id.textView1, values);
		lv.setOnPositionChangedListener(new ScrollingInfoListView.OnPositionChangedListener() {
			
			public void onPositionChanged(ScrollingInfoListView listView, int position,
					View scrollBarPanel) {
				TextView tv=(TextView) scrollBarPanel.findViewById(R.id.textView);
				tv.setText("Current:"+position);
				
			}
		});
		lv.setAdapter(adapter);
	}
}
