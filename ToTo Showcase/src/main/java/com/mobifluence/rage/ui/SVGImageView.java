package com.mobifluence.rage.ui;

import toto.jobs.TJobResponse;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

public class SVGImageView extends ProcessingActivity {

	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.threefourthpane);
		LinearLayout top=(LinearLayout) findViewById(R.id.top);
		top.addView(getLayoutInflater().inflate(R.layout.ui_svgimageview, null));
		final toto.ui.widget.imageview.SVGImageView iv=(toto.ui.widget.imageview.SVGImageView) top.findViewById(R.id.svg);
		iv.setSVGFromResource(R.raw.giraffe);
		Button btn =new Button(getApplicationContext());
		btn.setText("Zoom In");
		Button btnZommOut =new Button(getApplicationContext());
		btnZommOut.setText("Zoom Out");
		LinearLayout bottom=(LinearLayout) findViewById(R.id.bottom);
		bottom.addView(btn);
		bottom.addView(btnZommOut);
		btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int zoom=iv.getZoomFactor();
				if(zoom<400){
					iv.setZoomFactor((zoom+10));
				}
			}
		});
		btnZommOut.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int zoom=iv.getZoomFactor();
				if(zoom>20){
					iv.setZoomFactor((zoom-10));
				}
			}
		});
//		iv.setSVGZoomFactor(100);
	}
	
	@Override
	protected void success(TJobResponse responses) {

	}

}
