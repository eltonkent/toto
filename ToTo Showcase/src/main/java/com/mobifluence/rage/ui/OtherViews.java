package com.mobifluence.rage.ui;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

/**
 * Created by ekent4 on 12/16/13.
 */
public class OtherViews extends MainFeatureActivity {
    @Override
    protected List<FeatureItem> getFeatureList() {
        List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("Signature View",
                "Capture and Save Signatures",
                SignatureViewActivity.class));
        listAdapter.add(createItem("Color Picker View",
                "Color Picker using the RGB model",
                ColorPickerViewActivity.class));
        listAdapter.add(createItem("Custom Progress Wheels",
                "Progress Wheels",
                ProgressWheelActivity.class));
//        listAdapter.add(createItem("Circular seekbar",
//                "Seekbar thats circular",
//                CircularSeek.class));
        listAdapter.add(createItem("Circular Color Picker",
                "Color picker thats circular",
                CircleColorPicker.class));


        return listAdapter;
    }
}
