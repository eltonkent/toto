package com.mobifluence.rage.ui;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

/**
 * Created by ekent4 on 12/17/13.
 */
public class ViewActivity extends MainFeatureActivity {

    @Override
    protected List<FeatureItem> getFeatureList() {
        List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("Graphs",
                "Custom Graph Implementations with zooming and scrolling support.",
                Graphs.class));
        listAdapter.add(createItem("ImageView",
                "Custom ImageView Implementations",
                ImageViewActivity.class));

        listAdapter.add(createItem("ListView",
                "Custom ListView Implementations",
                ListViewActivity.class));
        listAdapter.add(createItem("TextView",
                "Custom TextView Implementations",
                TextViewActivity.class));
        listAdapter.add(createItem("Drawables",
                "Custom Drawable Implementations",
                Drawables.class));
        listAdapter.add(createItem("Other Views",
                "Other uncommon view Implementations",
                OtherViews.class));
        return listAdapter;
    }
}
