package com.mobifluence.rage.ui;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

/**
 */
public class Graphs extends MainFeatureActivity {
    @Override
    protected List<FeatureItem> getFeatureList() {
        List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("Bar Graph",
                "Bar Graph view",
                BarGraph.class));
        listAdapter.add(createItem("Line Graph",
                "Line Graph view",
                LineGraph.class));
        listAdapter.add(createItem("Realtime Graph",
                "Realtime Line Graph view",
                RealtimeLineGraph.class));

        return listAdapter;
    }
}
