package com.mobifluence.rage.ui;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.mobifluence.rage.R;

public class ZoomableImageView extends Activity {

    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        setContentView(R.layout.ui_zoomableimageview);
//		LinearLayout top=(LinearLayout) findViewById(R.id.top);
//		top.addView(getLayoutInflater().inflate(R.layout.ui_zoomableimageview, null));
        final toto.ui.widget.imageview.TImageView iv = (toto.ui.widget.imageview.TImageView) findViewById(R.id.zoom);
        iv.setImageResource(R.drawable.hydrangeas);
        CheckBox cb = (CheckBox) findViewById(R.id.checkBox);
        cb.setChecked(true);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    iv.reset();
                    iv.allowZoomGestures(isChecked);
            }
        });
//		LinearLayout bottom=(LinearLayout) findViewById(R.id.bottom);
//		Button btn=new Button(getApplicationContext());
//		btn.setText("Reset");
//		btn.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onAreaClicked(View v) {
//				iv.reset();
//			}
//		});
//		bottom.addView(btn);
    }
}
