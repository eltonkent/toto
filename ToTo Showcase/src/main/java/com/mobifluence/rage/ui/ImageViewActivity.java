package com.mobifluence.rage.ui;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class ImageViewActivity extends MainFeatureActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("Cover flow gallery",
                "Smooth cover flow gallery",
                GLCoverflow.class));
        listAdapter.add(createItem("Flowing Image View",
                "ImageView that scrolls h/v slowly",
                FlowingImageview.class));
		listAdapter.add(createItem("GIF ImageView",
				"ImageView with animated GIF files", GIFImageView.class));
		listAdapter.add(createItem("SVG ImageView",
				"SVG file rendering on an image view", SVGImageView.class));
		listAdapter.add(createItem("Zoomable ImageView",
				"ImageView with zoom gestures", ZoomableImageView.class));
		listAdapter.add(createItem("Web ImageView",
				"ImageView with that loads images from the internet", WebImageView.class));
		listAdapter.add(createItem("Circular ImageView",
				"ImageView is circular", CircleIv.class));
		listAdapter.add(createItem("Map ImageView",
				"ImageView that allows interaction with regions of the image", ImageMapView.class));
		return listAdapter;
	}

}
