package com.mobifluence.rage.ui;

import toto.ui.widget.ProgressWheel;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.os.Bundle;

import com.mobifluence.rage.R;

/**
 * Created by ekent4 on 12/17/13.
 */
public class ProgressWheelActivity extends Activity {

    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        setContentView(R.layout.ui_progresswheel);
        ProgressWheel wheel= (ProgressWheel) findViewById(R.id.shade1);
        wheel.setRimShader(new LinearGradient(0,0,0,50, Color.BLUE,Color.GREEN, Shader.TileMode.MIRROR));
        wheel.setBarShader(new LinearGradient(0, 0, 0, 50, Color.RED, Color.YELLOW, Shader.TileMode.MIRROR));
        ProgressWheel wheel2= (ProgressWheel) findViewById(R.id.shade2);
        wheel2.setRimShader(new RadialGradient(0,0,100, Color.BLUE,Color.GREEN, Shader.TileMode.MIRROR));
    }


}
