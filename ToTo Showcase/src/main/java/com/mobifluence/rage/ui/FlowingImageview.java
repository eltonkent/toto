package com.mobifluence.rage.ui;

import toto.ui.widget.imageview.FlowingImageView;
import android.app.Activity;
import android.os.Bundle;

import com.mobifluence.rage.R;

/**
 * Created by ekent4 on 2/11/14.
 */
public class FlowingImageview extends Activity {
    public void onCreate(Bundle instance){
        super.onCreate(instance);
        FlowingImageView iv=new FlowingImageView(this);
        iv.setImageResource(R.drawable.four);
        iv.setEdgeDelay(2000);
        iv.setFlowVelocity(50f);
        setContentView(iv);
    }
}
