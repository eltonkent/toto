package com.mobifluence.rage.ui;

import android.app.Activity;
import android.os.Bundle;

import com.mobifluence.rage.R;

public class GIFImageView extends Activity {

	public void onCreate(Bundle instance){
		super.onCreate(instance);
		setContentView(R.layout.ui_gifimageview);
		toto.ui.widget.imageview.GIFImageView iv=(toto.ui.widget.imageview.GIFImageView) findViewById(R.id.gif);
		iv.setAutoStartAnimation(true);
		iv.setImageResource(R.raw.animation);
	}
}
