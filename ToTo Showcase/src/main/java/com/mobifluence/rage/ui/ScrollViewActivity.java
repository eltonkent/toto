package com.mobifluence.rage.ui;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class ScrollViewActivity extends MainFeatureActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("Over Scroll ScrollView",
				"ScrollView with overscroll capabilities",
				OverscrollView.class));
		return listAdapter;
	}

}
