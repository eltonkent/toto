package com.mobifluence.rage.ui;

import toto.jobs.TJobResponse;
import toto.ui.widget.SignatureView;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;


public class SignatureViewActivity extends ProcessingActivity {


    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        setContentView(R.layout.splitpane);
        final LinearLayout top = (LinearLayout) findViewById(R.id.top);
        LinearLayout bottom = (LinearLayout) findViewById(R.id.bottom);
        View view = getLayoutInflater().inflate(R.layout.ui_signatureview, top);
        final SignatureView sign = (SignatureView) view.findViewById(R.id.sign);
        View viewBottom = getLayoutInflater().inflate(R.layout.ui_signatureview_bottom, bottom);
        Button btn = (Button) viewBottom.findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (top.getChildCount() > 1) {
                    top.removeViewAt(1);
                }
                sign.clear();
            }
        });
        btn = (Button) viewBottom.findViewById(R.id.button2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (top.getChildCount() > 1) {
                    top.removeViewAt(1);
                }
                Bitmap bmp = sign.getImage(Bitmap.Config.ARGB_8888);
                ImageView iv = new ImageView(getApplicationContext());
                iv.setImageBitmap(bmp);
                top.addView(iv);
            }
        });
    }

    @Override
    protected void success(TJobResponse responses) {

    }
}
