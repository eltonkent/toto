package com.mobifluence.rage.ui;

import toto.ui.widget.CoverFlowGallery;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.mobifluence.rage.R;

public class GLCoverflow extends Activity {

    private static int[] SAMPLE_IMAGES = new int[]{
            R.drawable.one,
            R.drawable.two,
            R.drawable.three,
            R.drawable.four,
            R.drawable.one,
            R.drawable.hydrangeas,
            R.drawable.girlskin,
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Toast.makeText(GLCoverflow.this, (String) msg.obj, 1000);
        }
    };

    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        CoverFlowGallery gl = new CoverFlowGallery(this);
        gl.setBitmapProvider(new CoverFlowGallery.BitmapProvider() {
            @Override
            public int getCount(CoverFlowGallery view) {
                return SAMPLE_IMAGES.length;
            }

            @Override
            public Bitmap getImage(CoverFlowGallery anotherCoverFlow, int position) {
                return BitmapFactory.decodeResource(getResources(), SAMPLE_IMAGES[position]);
            }
        });
        gl.setEventListener(new CoverFlowGallery.EventListener() {

            @Override
            public void tileOnTop(CoverFlowGallery view, int position) {
// you can control what will happen when one image is in middle
                mHandler.obtainMessage(0, String.format("Image %d is on top.", position)).sendToTarget();
            }

            @Override
            public void topTileClicked(CoverFlowGallery view, int position) {
                // you can control what will happen when the image in middle is clicked
                mHandler.obtainMessage(0, String.format("Image %d is clicked", position)).sendToTarget();
            }
        });
        setContentView(gl);
    }
}
