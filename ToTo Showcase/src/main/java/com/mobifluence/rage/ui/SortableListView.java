package com.mobifluence.rage.ui;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.mobifluence.rage.R;

public class SortableListView extends Activity {
	
	public void onCreate(Bundle instance) {
		super.onCreate(instance);
		setContentView(R.layout.ui_sortablelistview);
		toto.ui.widget.adapterview.SortableListView lv=(toto.ui.widget.adapterview.SortableListView) findViewById(R.id.sortableListview);
		String[] values=getResources().getStringArray(R.array.listview_rage);
		ArrayAdapter<String> adapter=new ArrayAdapter<String>(getApplicationContext(), R.layout.listview_rage, R.id.textView1, values);
		lv.setAdapter(adapter);
	}

}
