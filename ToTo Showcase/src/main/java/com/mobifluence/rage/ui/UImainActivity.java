package com.mobifluence.rage.ui;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class UImainActivity extends MainFeatureActivity{

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
        listAdapter.add(createItem("Custom Views",
                "Custom ImageView Implementations",
                ViewActivity.class));
//        listAdapter.add(createItem("Animations",
//                "Animation Implementations",
//                Animation.class));
        listAdapter.add(createItem("Custom View Groups",
                "Custom ViewGroup Implementations",
                LayoutActivity.class));

		return listAdapter;
	}

}
