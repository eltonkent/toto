package com.mobifluence.rage.ui;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.RageMainActivity;

/**
 * Created by ekent4 on 12/10/13.
 */
public class LayoutActivity extends RageMainActivity {
	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("ScrollView",
				"Custom ScrollView Implementations", ScrollViewActivity.class));
		listAdapter
				.add(createItem("Folding Layout",
						"Layout that has a folding effect",
						FoldingLayoutActivity.class));
		listAdapter.add(createItem("Zoomable Layout",
				"Layout that allows zooming with pinch gestures",
				ZoomLayoutActivity.class));
		listAdapter
				.add(createItem("Circle Layout",
						"Layout that arranges children in a circle",
						CircleLayout.class));
		listAdapter
		.add(createItem("Sortable GirdView",
				"Gridview that allows sorting with drag and drop",
				SortableGridView.class));
		listAdapter
		.add(createItem("Slide Up Panel",
				"UI Panel that can slide up by dragging",
				SlideUpPanel.class));
		
		return listAdapter;
	}
}
