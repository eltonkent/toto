package com.mobifluence.rage.ui;

import toto.ui.widget.layout.SlideUpPanelLayout;
import android.app.Activity;
import android.os.Bundle;

import com.mobifluence.rage.R;

public class SlideUpPanel extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ui_slideuppanel);
		SlideUpPanelLayout layout = (SlideUpPanelLayout) findViewById(R.id.sliding_layout);
		layout.setAnchorPoint(0.3f);
	}

}
