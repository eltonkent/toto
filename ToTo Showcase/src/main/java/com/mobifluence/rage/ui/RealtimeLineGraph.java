package com.mobifluence.rage.ui;

import toto.ui.widget.graph.GraphData;
import toto.ui.widget.graph.GraphSeries;
import toto.ui.widget.graph.LineGraphView;
import toto.ui.widget.graph.ToToGraphView;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.widget.LinearLayout;

import com.mobifluence.rage.R;

public class RealtimeLineGraph extends Activity {
	private final Handler mHandler = new Handler();
	private Runnable mTimer1;
	private ToToGraphView graphView;
	private GraphSeries exampleSeries1;
	private double graph2LastXValue = 5d;
	private GraphSeries exampleSeries3;

	private double getRandom() {
		double high = 3;
		double low = 0.5;
		return Math.random() * (high - low) + low;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ui_graph);

		// init example series data
		exampleSeries1 = new GraphSeries(new GraphData[] {
				new GraphData(1, 2.0d),
				new GraphData(2, 1.5d),
				new GraphData(2.5, 3.0d) // another frequency
				, new GraphData(3, 2.5d), new GraphData(4, 1.0d),
				new GraphData(5, 3.0d) });
		exampleSeries3 = new GraphSeries(new GraphData[] {});
		exampleSeries3.getStyle().color = Color.CYAN;

		// graph with dynamically genereated horizontal and vertical labels
		graphView = new LineGraphView(this);

		graphView.addSeries(exampleSeries1); // data
		graphView.addSeries(exampleSeries3);

		LinearLayout layout = (LinearLayout) findViewById(R.id.parent);
		layout.addView(graphView);

	}

	@Override
	protected void onPause() {
		mHandler.removeCallbacks(mTimer1);
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mTimer1 = new Runnable() {
			@Override
			public void run() {
				exampleSeries1.resetData(new GraphData[] {
						new GraphData(1, getRandom()),
						new GraphData(2, getRandom()),
						new GraphData(2.5, getRandom()) // another frequency
						, new GraphData(3, getRandom()),
						new GraphData(4, getRandom()),
						new GraphData(5, getRandom()) });
				exampleSeries3.resetData(new GraphData[] {
						new GraphData(2, getRandom()),
						new GraphData(2.5, getRandom()) // another frequency
						, new GraphData(3, getRandom()),
						new GraphData(4, getRandom()) });
				mHandler.postDelayed(this, 300);
			}
		};
		mHandler.postDelayed(mTimer1, 300);

	}

}
