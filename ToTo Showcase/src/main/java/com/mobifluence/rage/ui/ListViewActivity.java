package com.mobifluence.rage.ui;

import java.util.ArrayList;
import java.util.List;

import com.mobifluence.rage.FeatureItem;
import com.mobifluence.rage.MainFeatureActivity;

public class ListViewActivity extends MainFeatureActivity {

	@Override
	protected List<FeatureItem> getFeatureList() {
		List<FeatureItem> listAdapter = new ArrayList<FeatureItem>();
		listAdapter.add(createItem("Rage ListView",
				"Base ListView with overscroll animations",
				RageListView.class));
        listAdapter.add(createItem("Flipping ListView",
                "ListView that allows scrolling using flipping gestures",
                FlipListView.class));
        listAdapter.add(createItem("Sortable ListView",
                "ListView that allows sorting",
                SortableListView.class));
        listAdapter.add(createItem("Scrolling Info ListView",
                "ListView that shows information when scrolling",
                ScrollingInfo.class));

		return listAdapter;
	}

}
