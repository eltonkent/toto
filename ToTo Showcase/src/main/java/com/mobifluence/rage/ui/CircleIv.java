package com.mobifluence.rage.ui;

import toto.ui.widget.imageview.CircularImageView;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader.TileMode;
import android.os.Bundle;

import com.mobifluence.rage.R;

public class CircleIv extends Activity {

	public void onCreate(Bundle instance) {
		super.onCreate(instance);
		setContentView(R.layout.ui_circleiv);
		CircularImageView iv = (CircularImageView) findViewById(R.id.circle1);
		iv.setImageResource(R.drawable.girlskin);
		iv.setBorderWidth(5);
		iv.setBorderColor(Color.RED);
		
		
		 iv = (CircularImageView) findViewById(R.id.circle);
		iv.setImageResource(R.drawable.girlskin);
		iv.setBorderShader(new LinearGradient(0, 0, 0, 250, Color.WHITE, Color.BLUE, TileMode.MIRROR));
		iv.setBorderWidth(5);
	}

}
