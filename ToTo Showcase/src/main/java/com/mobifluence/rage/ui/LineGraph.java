package com.mobifluence.rage.ui;

import toto.ui.widget.graph.GraphData;
import toto.ui.widget.graph.GraphSeries;
import toto.ui.widget.graph.LineGraphView;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.mobifluence.rage.R;


public class LineGraph extends Activity {

    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        setContentView(R.layout.ui_graph);
        // init example series data
        GraphSeries exampleSeries = new GraphSeries(new GraphData[] {
                new GraphData(1, 2.0d)
                , new GraphData(2, 1.5d)
                , new GraphData(3, 2.5d)
                , new GraphData(4, 1.0d)

                , new GraphData(5, 1.5d)
                , new GraphData(6, 2.5d)
                , new GraphData(7, 1.0d)
                , new GraphData(8, 1.5d)
                , new GraphData(9, 2.5d)
                , new GraphData(10, 1.0d)
                , new GraphData(11, 1.5d)
                , new GraphData(12, 2.5d)
                , new GraphData(13, 1.0d)
        });


        LineGraphView graphView = (LineGraphView) findViewById(R.id.lineGraph);
        graphView.setAllowZoom(true);
        graphView.addSeries(exampleSeries); // data
        graphView.setVisibility(View.VISIBLE);
    }
}
