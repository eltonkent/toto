package com.mobifluence.rage.ui;

import toto.ui.widget.adapterview.RListView;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.mobifluence.rage.R;

public class RageListView extends Activity {
	
	public void onCreate(Bundle instance) {
		super.onCreate(instance);
		setContentView(R.layout.ui_ragelistview);
		RListView lv=(RListView) findViewById(R.id.rageListView);
		String[] values=getResources().getStringArray(R.array.listview_rage);
		ArrayAdapter<String> adapter=new ArrayAdapter<String>(getApplicationContext(), R.layout.listview_rage, R.id.textView1, values);
		lv.setAdapter(adapter);
	}

}
