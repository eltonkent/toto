package com.mobifluence.rage.ui;

import java.util.ArrayList;
import java.util.List;

import toto.ui.widget.adapterview.FlipView;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.mobifluence.rage.R;

/**
 *
 *
 */
public class FlipListView extends Activity{

    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        setContentView(R.layout.ui_flipview);
        List<Integer> listItems=new ArrayList<Integer>();
        listItems.add(R.drawable.one);
        listItems.add(R.drawable.two);
        listItems.add(R.drawable.three);
        listItems.add(R.drawable.four);

        FlipAdapter adapter=new FlipAdapter(getApplicationContext(),0,listItems);
        FlipView fv= (FlipView) findViewById(R.id.flip);

        fv.setAdapter(adapter);
    }

    private class FlipAdapter extends ArrayAdapter{
        List<Integer> objects;
        public FlipAdapter(Context context, int resource, List<Integer> objects) {
            super(context, resource, objects);
            this.objects=objects;
        }

        public View getView (int position, View convertView, ViewGroup parent){
            ImageView iv=new ImageView(getContext());
            iv.setImageResource(objects.get(position));
            return iv;
        }
    }
}
