package com.mobifluence.rage.ui;

import toto.jobs.TJobResponse;
import toto.ui.widget.SquareColorPickerView;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.mobifluence.rage.ProcessingActivity;
import com.mobifluence.rage.R;

/**
 * Created by ekent4 on 12/17/13.
 */
public class ColorPickerViewActivity extends ProcessingActivity {


    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        setContentView(R.layout.splitpane);
        final LinearLayout top = (LinearLayout) findViewById(R.id.top);
        final LinearLayout bottom = (LinearLayout) findViewById(R.id.bottom);
        View view=getLayoutInflater().inflate(R.layout.ui_colorpicker,null);
        SquareColorPickerView picker= (SquareColorPickerView) view.findViewById(R.id.picker);
        top.addView(view);
        picker.setOnColorChangedListener(new SquareColorPickerView.OnColorChangedListener() {
            @Override
            public void onColorChanged(int color) {
                bottom.removeAllViews();
                bottom.addView(getKeyValueView("Color Picked",""+ color));
            }
        });
    }

    @Override
    protected void success(TJobResponse responses) {

    }


}
