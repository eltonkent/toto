package com.mobifluence.rage;

import toto.jobs.TJobResponse;
import toto.jobs.TJob.JobNotifier;
import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;


public abstract class ProcessingActivity extends Activity {

	protected View getKeyValueView(String key, String value) {
		View view = getLayoutInflater().inflate(R.layout.key_value, null);
		TextView tv = (TextView) view.findViewById(R.id.key);
		tv.setText(key);
		tv = (TextView) view.findViewById(R.id.value);
		tv.setText(value);
		return view;
	}

	private Dialog dialog;
	
	protected JobNotifier notifier=new JobNotifier() {

		@Override
		public void onJobCompletedNonUI(TJobResponse response) {
		}

		@Override
		public void onJobStarted() {
			showDialog();
		}

		@Override
		public void onJobCompletedUI(TJobResponse response) {
			hideDialog();
			success(response);
		}

		@Override
		public void onError(Throwable t) {
			hideDialog();
		}
	};

	private void showDialog() {
		if (dialog == null) {
			dialog = new Dialog(ProcessingActivity.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.progress_dialog);
			dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			dialog.setCanceledOnTouchOutside(false);
		}
		dialog.show();
	}

	private void hideDialog() {
		dialog.dismiss();
	}

	protected abstract void success(TJobResponse responses);
}
