package com.mobifluence.rage;

import android.app.Activity;

public class FeatureItem {

	String name;
	String description;
	Class<? extends Activity> activityClass;
	
	protected String getName() {
		return name;
	}
	protected void setName(String name) {
		this.name = name;
	}
	protected String getDescription() {
		return description;
	}
	protected void setDescription(String description) {
		this.description = description;
	}
	protected Class<? extends Activity> getActivityClass() {
		return activityClass;
	}
	protected void setActivityClass(Class<? extends Activity> activityClass) {
		this.activityClass = activityClass;
	}
}
