/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.security.hash;

import toto.lang.PrimitiveUtils;
import toto.lang.bitshift.BitShiftUtils;

/**
 * <a href="http://en.wikipedia.org/wiki/SipHash">SipHash</a> implementation
 * <p>
 * 
 * 
 * @author ekent4
 * 
 */
public class SipHash {
	/**
	 * Key for Siphash. must be 16 bytes (128 bit) long
	 * 
	 * @author ekent4
	 * 
	 */
	public static class SipHashKey {
		private final byte[] key;

		public SipHashKey(final byte[] key) {
			if (key == null || key.length != 16)
				throw new RuntimeException("SipHash key must be 16 bytes");
			this.key = key;
		}

		long getLeftHalf() {
			return PrimitiveUtils.binToIntOffset(key, 0);
		}

		long getRightHalf() {
			return PrimitiveUtils.binToIntOffset(key, 8);
		}
	}

	public static long digest(final SipHashKey key, final byte[] data) {
		long m;
		final State s = new State(key);
		final int iter = data.length / 8;

		for (int i = 0; i < iter; i++) {
			m = PrimitiveUtils.binToIntOffset(data, i * 8);
			s.processBlock(m);
		}

		m = lastBlock(data, iter);
		s.processBlock(m);
		s.finish();
		return s.digest();
	}

	private static long lastBlock(final byte[] data, final int iter) {
		long last = ((long) data.length) << 56;
		final int off = iter * 8;

		switch (data.length % 8) {
		case 7:
			last |= ((long) data[off + 6]) << 48;
		case 6:
			last |= ((long) data[off + 5]) << 40;
		case 5:
			last |= ((long) data[off + 4]) << 32;
		case 4:
			last |= ((long) data[off + 3]) << 24;
		case 3:
			last |= ((long) data[off + 2]) << 16;
		case 2:
			last |= ((long) data[off + 1]) << 8;
		case 1:
			last |= data[off];
			break;
		case 0:
			break;
		}
		return last;
	}

	private static class State {
		private long v0;
		private long v1;
		private long v2;
		private long v3;

		public State(final SipHashKey key) {
			v0 = 0x736f6d6570736575L;
			v1 = 0x646f72616e646f6dL;
			v2 = 0x6c7967656e657261L;
			v3 = 0x7465646279746573L;

			final long k0 = key.getLeftHalf();
			final long k1 = key.getRightHalf();

			v0 ^= k0;
			v1 ^= k1;
			v2 ^= k0;
			v3 ^= k1;
		}

		private void compress() {
			v0 += v1;
			v2 += v3;
			v1 = BitShiftUtils.rotateLeft(v1, 13);
			v3 = BitShiftUtils.rotateLeft(v3, 16);
			v1 ^= v0;
			v3 ^= v2;
			v0 = BitShiftUtils.rotateLeft(v0, 32);
			v2 += v1;
			v0 += v3;
			v1 = BitShiftUtils.rotateLeft(v1, 17);
			v3 = BitShiftUtils.rotateLeft(v3, 21);
			v1 ^= v2;
			v3 ^= v0;
			v2 = BitShiftUtils.rotateLeft(v2, 32);
		}

		private void compressTimes(final int times) {
			for (int i = 0; i < times; i++) {
				compress();
			}
		}

		public void processBlock(final long m) {
			v3 ^= m;
			compressTimes(2);
			v0 ^= m;
		}

		public void finish() {
			v2 ^= 0xff;
			compressTimes(4);
		}

		public long digest() {
			return v0 ^ v1 ^ v2 ^ v3;
		}
	}
}