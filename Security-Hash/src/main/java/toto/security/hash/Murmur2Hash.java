package toto.security.hash;

/**
 * <a href="http://en.wikipedia.org/wiki/MurmurHash">Murmur</a> 2 hash
 * implementation
 * 
 * @author ekent4
 * 
 */
public final class Murmur2Hash extends ToToHash {

	public static final Murmur2Hash INSTANCE = new Murmur2Hash();

	public Murmur2Hash() {
	}

	public static int digest(final byte[] data, final int seed,
			final int offset, final int len) {
		final int m = 0x5bd1e995;
		final int r = 24;
		int h = seed ^ len;
		final int len_4 = len >> 2;
		for (int i = 0; i < len_4; i++) {
			final int i_4 = offset + (i << 2);
			int k = data[i_4 + 3];
			k = k << 8;
			k = k | (data[i_4 + 2] & 0xff);
			k = k << 8;
			k = k | (data[i_4 + 1] & 0xff);
			k = k << 8;
			k = k | (data[i_4 + 0] & 0xff);
			k *= m;
			k ^= k >>> r;
			k *= m;
			h *= m;
			h ^= k;
		}
		final int len_m = len_4 << 2;
		final int left = len - len_m;
		if (left != 0) {
			if (left >= 3) {
				h ^= data[offset + len - 3] << 16;
			}
			if (left >= 2) {
				h ^= data[offset + len - 2] << 8;
			}
			if (left >= 1) {
				h ^= data[offset + len - 1];
			}
			h *= m;
		}
		h ^= h >>> 13;
		h *= m;
		h ^= h >>> 15;
		return h;
	}

	/**
	 * Generates 32 bit hash from byte array with default seed value.
	 * 
	 * @param data
	 *            byte array to hash
	 * @param offset
	 *            the start position in the array to hash
	 * @param len
	 *            length of the array elements to hash
	 * @return 32 bit hash of the given array
	 */
	public static final int digest(final byte[] data, final int offset,
			final int len) {
		return Murmur2Hash.digest(data, 0x9747b28c, offset, len);
	}

	@Override
	public long hash(final byte[] data) {
		return Murmur2Hash.digest(data, 0x9747b28c, 0, data.length);
	}

}