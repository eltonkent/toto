/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.security.hash;

import toto.lang.bitshift.BitConverter;
import toto.lang.bitshift.BitMath;
import toto.lang.bitshift.BitShiftUtils;

/**
 * Jenkins hash is a non cryptographic hash.
 * <p>
 * The Jenkins hash functions are hash functions for multi-byte keys designed by
 * Bob Jenkins<br/>
 * http://en.wikipedia.org/wiki/Jenkins_hash_function
 * </p>
 * 
 * @author elton.kent
 * 
 */
public class BobJenkinsHash extends ToToHash {

	// internal variables used in the various calculations
	private long a;
	private long b;
	private long c;

	/**
	 * Mix up the values in the hash function.
	 */
	private void hashMix() {
		a = BitMath.subtract(a, b);
		a = BitMath.subtract(a, c);
		a = BitMath.xor(a, c >> 13);
		b = BitMath.subtract(b, c);
		b = BitMath.subtract(b, a);
		b = BitMath.xor(b, BitShiftUtils.leftShift(a, 8));
		c = BitMath.subtract(c, a);
		c = BitMath.subtract(c, b);
		c = BitMath.xor(c, (b >> 13));
		a = BitMath.subtract(a, b);
		a = BitMath.subtract(a, c);
		a = BitMath.xor(a, (c >> 12));
		b = BitMath.subtract(b, c);
		b = BitMath.subtract(b, a);
		b = BitMath.xor(b, BitShiftUtils.leftShift(a, 16));
		c = BitMath.subtract(c, a);
		c = BitMath.subtract(c, b);
		c = BitMath.xor(c, (b >> 5));
		a = BitMath.subtract(a, b);
		a = BitMath.subtract(a, c);
		a = BitMath.xor(a, (c >> 3));
		b = BitMath.subtract(b, c);
		b = BitMath.subtract(b, a);
		b = BitMath.xor(b, BitShiftUtils.leftShift(a, 10));
		c = BitMath.subtract(c, a);
		c = BitMath.subtract(c, b);
		c = BitMath.xor(c, (b >> 15));
	}

	/**
	 * Hash a variable-length key into a 32-bit value. Every bit of the key
	 * affects every bit of the return value. Every 1-bit and 2-bit delta
	 * achieves avalanche. The best hash table sizes are powers of 2.
	 * 
	 * @param buffer
	 *            Byte array that we are hashing on.
	 * @param initialValue
	 *            Initial value of the hash if we are continuing from a previous
	 *            run. 0 if none.
	 * @return Hash value for the buffer.
	 */
	long digest(final byte[] buffer, final long initialValue) {
		int len, pos;

		// setKey up the internal state
		// the golden ratio; an arbitrary value
		a = 0x09e3779b9L;
		// the golden ratio; an arbitrary value
		b = 0x09e3779b9L;
		// the previous hash value
		c = initialValue;

		// handle most of the key
		pos = 0;
		for (len = buffer.length; len >= 12; len -= 12) {
			a = BitMath.add(a, BitConverter.fourByteToLong(buffer, pos));
			b = BitMath.add(b, BitConverter.fourByteToLong(buffer, pos + 4));
			c = BitMath.add(c, BitConverter.fourByteToLong(buffer, pos + 8));
			hashMix();
			pos += 12;
		}

		c += buffer.length;

		// all the case statements fall through to the next on purpose
		switch (len) {
		case 11:
			c = BitMath.add(
					c,
					BitShiftUtils.leftShift(
							BitConverter.byteToLong(buffer[pos + 10]), 24));
		case 10:
			c = BitMath.add(
					c,
					BitShiftUtils.leftShift(
							BitConverter.byteToLong(buffer[pos + 9]), 16));
		case 9:
			c = BitMath.add(
					c,
					BitShiftUtils.leftShift(
							BitConverter.byteToLong(buffer[pos + 8]), 8));
			// the first byte of c is reserved for the length
		case 8:
			b = BitMath.add(
					b,
					BitShiftUtils.leftShift(
							BitConverter.byteToLong(buffer[pos + 7]), 24));
		case 7:
			b = BitMath.add(
					b,
					BitShiftUtils.leftShift(
							BitConverter.byteToLong(buffer[pos + 6]), 16));
		case 6:
			b = BitMath.add(
					b,
					BitShiftUtils.leftShift(
							BitConverter.byteToLong(buffer[pos + 5]), 8));
		case 5:
			b = BitMath.add(b, BitConverter.byteToLong(buffer[pos + 4]));
		case 4:
			a = BitMath.add(
					a,
					BitShiftUtils.leftShift(
							BitConverter.byteToLong(buffer[pos + 3]), 24));
		case 3:
			a = BitMath.add(
					a,
					BitShiftUtils.leftShift(
							BitConverter.byteToLong(buffer[pos + 2]), 16));
		case 2:
			a = BitMath.add(
					a,
					BitShiftUtils.leftShift(
							BitConverter.byteToLong(buffer[pos + 1]), 8));
		case 1:
			a = BitMath.add(a, BitConverter.byteToLong(buffer[pos + 0]));
			// case 0: nothing left to add
		}
		hashMix();

		return c;
	}

	/**
	 * See hash(byte[] buffer, long initialValue)
	 * 
	 * @param buffer
	 *            Byte array that we are hashing on.
	 * @return Hash value for the buffer.
	 */
	public long hash(final byte[] buffer) {
		return digest(buffer, 0);
	}

}
