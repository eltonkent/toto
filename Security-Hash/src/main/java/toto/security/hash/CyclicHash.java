/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.security.hash;

import java.util.Random;

public class CyclicHash {
	int hashvalues[] = new int[1 << 16];

	// myn is the length in characters of the blocks you want to hash
	public CyclicHash(final int myn) {
		final Random r = new Random();
		for (int k = 0; k < hashvalues.length; ++k)
			hashvalues[k] = r.nextInt();
		n = myn;
		if (n > wordsize) {
			throw new IllegalArgumentException();
		}

	}

	private int fastleftshiftn(final int x) {
		return (x << n) | (x >>> (wordsize - n));
	}

	private static int fastleftshift1(final int x) {
		return (x << 1) | (x >>> (wordsize - 1));
	}

	// add new character (useful to initiate the hasher)
	// to get a strongly universal hash value, you have to ignore the last or
	// first (n-1) bits.
	public int eat(final char c) {
		hashvalue = fastleftshift1(hashvalue);
		hashvalue ^= hashvalues[c];
		return hashvalue;
	}

	// remove old character and add new one
	// to get a strongly universal hash value, you have to ignore the last or
	// first (n-1) bits.
	public int update(final char outchar, final char inchar) {
		final int z = fastleftshiftn(hashvalues[outchar]);
		hashvalue = fastleftshift1(hashvalue) ^ z ^ hashvalues[inchar];
		return hashvalue;
	}

	public final static int wordsize = 32;
	public int hashvalue;
	int n;
	int myr;

}
