package toto.security.hash;

public abstract class ToToHash {

	public abstract long hash(byte[] data);

	public String toHexString(final byte[] data) {
		return Long.toHexString(hash(data));
	}
}
