package toto.security.hash.crypto;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Cryptographic Hash functions.
 * 
 * @author Mobifluence Interactive
 * @see rage.security.hash.HashUtils
 */
public class CryptoHashUtils {

	/**
	 * Perform a MD5 checksum on the given byte array
	 * 
	 * @param data
	 * @return Hash array of 16 bytes
	 */
	public static byte[] doMD5(final byte[] data) {
		final MD5 md5 = new MD5(data);
		return md5.Final();
	}

	/**
	 * Perform a MD5 checksum on the given object
	 * 
	 * @param object
	 * @return Hash array of 16 bytes
	 */
	public static byte[] doMD5(final Object object) {
		final MD5 md5 = new MD5(object);
		return md5.Final();
	}

	/**
	 * Perform a MD5 checksum on the given byte array
	 * 
	 * @param data
	 * @return String representation of the MD5 checksum
	 */
	public static String doMD5asHEX(final byte[] data) {
		final MD5 md5 = new MD5(data);
		return md5.asHex();
	}

	/**
	 * Perform a MD5 checksum on the given object
	 * 
	 * @param object
	 * @return String representation of the MD5 checksum
	 */
	public static String doMD5asHEX(final Object object) {
		final MD5 md5 = new MD5(object);
		return md5.asHex();
	}

	/**
	 * Get the SHA-1 signature for a given string.
	 * 
	 * @param value
	 * @param secretKey
	 * @return
	 */
	public static String getSHASignature(final String value,
			final String secretKey) {
		try {
			final Mac localMac = Mac.getInstance("HmacSHA1");
			localMac.init(new SecretKeySpec(secretKey.getBytes(), "HmacSHA1"));
			String str1 = new BigInteger(1, localMac.doFinal(value.getBytes()))
					.toString(16);
			if (str1.length() % 2 != 0)
				str1 = "0" + str1;
			final String str2 = str1.replace("-", "").toLowerCase(
					Locale.getDefault());
			return str2;
		} catch (final NoSuchAlgorithmException e1) {
			e1.printStackTrace();
			return "";
		} catch (final InvalidKeyException e2) {
			e2.printStackTrace();
			return "";
		}
	}

	/**
	 * Check if the given string is an MD5 checksum
	 * 
	 * @param md5
	 *            to validate
	 * @return
	 */
	public static boolean isMD5(final String md5) {
		final String pattern = "^\\d{5}(-\\d{4})?$";
		final Pattern patt = Pattern.compile(pattern);
		final Matcher matcher = patt.matcher(md5);
		return matcher.matches();
	}
}
