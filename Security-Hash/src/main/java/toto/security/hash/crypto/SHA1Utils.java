/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.security.hash.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Arrays;

import toto.io.IOUtils;

/**
 * 
 *
 */
public class SHA1Utils {

	final static String CLASS_NAME = "SHA1Util";

	/**
	 * This function encodes byte[] into a hex
	 * 
	 * @param b
	 * @return
	 */
	public static String byteArrayToHexString(final byte[] b) {
		if (b == null)
			return null;

		final StringBuffer sb = new StringBuffer(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			final int v = b[i] & 0xff;
			if (v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		return sb.toString().toUpperCase();
	}

	/**
	 * This function compares two bytes[]
	 * 
	 * @param b1
	 * @param b2
	 * @return
	 */
	public static boolean compareByteArrays(final byte[] b1, final byte[] b2) {
		return b1 != null && b2 != null && Arrays.equals(b1, b2);
	}

	/**
	 * This function compares two Strings.
	 * 
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static boolean compareHexString(final String s1, final String s2) {
		return s1 != null && s2 != null && s1.equalsIgnoreCase(s2);
	}

	/**
	 * This function generates a SHA1 byte[] from another byte[].
	 * 
	 * @param bytes
	 * @return
	 */
	public static byte[] generateSHA1(final byte[] bytes) {
		byte[] encryted = null;
		try {
			final MessageDigest digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
			digest.update(bytes);
			encryted = digest.digest();

		} catch (final Exception e) {
			e.printStackTrace();
		}
		return encryted;
	}

	// Genera SHA-1 de un char[]
	public static byte[] generateSHA1(final char chars[]) {
		return generateSHA1(new String(chars));
	}

	/**
	 * This function generates a SHA1 byte[] from a file
	 * 
	 * @param file
	 * @return
	 */
	public static byte[] generateSHA1(final File file) {
		try {
			return generateSHA1(new FileInputStream(file));
		} catch (final Exception e) {
			return null;
		}
	}

	// Genera SHA-1 de un InputStream
	public static byte[] generateSHA1(final InputStream is) {
		try {
			return generateSHA1(IOUtils.toByteArray(is));
		} catch (final Exception e) {
			return null;
		}
	}

	// Genera SHA-1 de un String
	public static byte[] generateSHA1(final String str) {
		return generateSHA1(str.getBytes());
	}

	/**
	 * This function converts an InputStream into a SHA1 String
	 * 
	 * @param is
	 * @return
	 */
	public static String generateSHA1toString(final InputStream is) {
		try {
			return new String((generateSHA1(IOUtils.toByteArray(is))),
					"ISO-8859-1");
		} catch (final Exception e) {
			return null;
		}
	}

	/**
	 * This function converts a string without conding into a String encoded
	 * into a SHA1
	 * 
	 * @param str
	 * @return
	 */
	public static String generateSHA1toString(final String str) {
		try {
			final byte[] datos = generateSHA1(str.getBytes());
			return byteArrayToHexString(datos);

		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * This function encodes a Hex String into a byte[]
	 * 
	 * @param s
	 * @return
	 */
	public static byte[] hexStringToByteArray(final String s) {
		if (s == null)
			return null;

		final byte[] b = new byte[s.length() / 2];
		for (int i = 0; i < b.length; i++) {
			final int index = i * 2;
			final int v = Integer.parseInt(s.substring(index, index + 2), 16);
			b[i] = (byte) v;
		}
		return b;
	}

}
