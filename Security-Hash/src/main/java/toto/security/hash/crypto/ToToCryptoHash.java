package toto.security.hash.crypto;

public interface ToToCryptoHash {

	public byte[] digest();

	public void update(final byte b);

	public void update(final byte[] b);

	public void update(final byte[] b, final int offset, final int len);

	public void reset();

}
