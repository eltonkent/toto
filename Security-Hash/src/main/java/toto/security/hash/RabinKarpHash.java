/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.security.hash;

import java.util.Random;

public class RabinKarpHash {

	private final int hashvalues[] = new int[1 << 16];

	// myn is the length in characters of the blocks you want to hash
	public RabinKarpHash(final int myn) {
		final Random r = new Random();
		for (int k = 0; k < hashvalues.length; ++k)
			hashvalues[k] = r.nextInt();
		n = myn;
		BtoN = 1;
		for (int i = 0; i < n; ++i) {
			BtoN *= B;
		}
	}

	// add new character (useful to initiate the hasher)
	// return 32 bits (not even universal)
	public int eat(final char c) {
		hashvalue = B * hashvalue + hashvalues[c];
		return hashvalue;
	}

	// remove old character and add new one
	// return 32 bits (not even universal)
	public int update(final char outchar, final char inchar) {
		hashvalue = B * hashvalue + hashvalues[inchar] - BtoN
				* hashvalues[outchar];
		return hashvalue;
	}

	// this is purely for testing purposes

	public int hashvalue;
	int n;
	int BtoN;
	final static int B = 31;

}
