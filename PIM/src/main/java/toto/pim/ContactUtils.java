/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.pim;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import toto.db.orm.BeanPopulator;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;

/**
 * Utility for accessing phone contacts
 * 
 */
public final class ContactUtils {

	public static List<Contact> fetchContacts(final Context context) {
		final ContentResolver cr = context.getContentResolver();
		final Cursor namesCursor = cr.query(
				ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		try {
			return BeanPopulator.toBeanList(namesCursor, Contact.class);
		} catch (final IllegalArgumentException e) {
			e.printStackTrace();
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
		} catch (final InstantiationException e) {
			e.printStackTrace();
		} catch (final toto.beans.IntrospectionException e) {
			e.printStackTrace();
		} catch (final InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Save a number to the contacts
	 * 
	 * @param context
	 *            Application context
	 * @param number
	 *            Phone number to save
	 */
	public static void saveToContact(final Context context, final String number) {
		final Intent intent = new Intent(Contacts.Intents.Insert.ACTION,
				Contacts.People.CONTENT_URI);
		intent.putExtra(Contacts.Intents.Insert.PHONE, number);
		context.startActivity(intent);
	}

	/**
	 * 
	 * Get the bitmap for the given contact ID
	 * 
	 * @param context
	 * @param contactId
	 * @return
	 */
	public static Bitmap getBitmapForContact(final Context context,
			final int contactId) {
		Bitmap bitmap = null;
		final ContentResolver contentResolver = context.getContentResolver();

		try {
			final Uri uri = ContentUris.withAppendedId(
					ContactsContract.Contacts.CONTENT_URI, contactId);
			final InputStream input = ContactsContract.Contacts
					.openContactPhotoInputStream(contentResolver, uri);
			if (input != null) {
				bitmap = BitmapFactory.decodeStream(input);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return bitmap;
	}
}
