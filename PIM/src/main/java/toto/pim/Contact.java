/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.pim;

/**
 * Contact information
 * 
 */
public class Contact {
	private String _id;
	private String custom_ringtone;
	private String display_name;
	private int has_phone_number;
	private String last_time_contacted;
	private String lookup;
	private int times_contacted;

	public String getCustomRingtone() {
		return custom_ringtone;
	}

	public String getDisplayName() {
		return display_name;
	}

	public String getID() {
		return _id;
	}

	public String getLastTimeContacted() {
		return last_time_contacted;
	}

	public String getLookup() {
		return lookup;
	}

	public int getTimesContacted() {
		return times_contacted;
	}

	public int hasPhoneNumber() {
		return has_phone_number;
	}

	public void setCustomRingtone(final String customRingtone) {
		custom_ringtone = customRingtone;
	}

	public void setDisplayName(final String displayName) {
		display_name = displayName;
	}

	public void setHasPhoneNumber(final int hasPhoneNumber) {
		has_phone_number = hasPhoneNumber;
	}

	public void setID(final String id) {
		_id = id;
	}

	public void setLastTimeContacted(final String lastTimeContacted) {
		last_time_contacted = lastTimeContacted;

	}

	public void setLookup(final String lookup) {
		this.lookup = lookup;
	}

	public void setTimesContacted(final int timesContacted) {
		times_contacted = timesContacted;
	}

	@Override
	public String toString() {
		return "Contact [_id=" + _id + ", custom_ringtone=" + custom_ringtone
				+ ", display_name=" + display_name + ", has_phone_number="
				+ has_phone_number + ", last_time_contacted="
				+ last_time_contacted + ", lookup=" + lookup
				+ ", times_contacted=" + times_contacted + "]";
	}

}
