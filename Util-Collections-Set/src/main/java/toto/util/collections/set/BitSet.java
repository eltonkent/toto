package toto.util.collections.set;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.LongBuffer;
import java.util.Arrays;

import toto.lang.ArrayUtils;

/**
 * The {@code BitSet} class implements a <a
 * href="http://en.wikipedia.org/wiki/Bit_array">bit array</a>. Each element is
 * either true or false. A {@code BitSet} is created with a given size and grows
 * automatically if this size is exceeded.
 * <p>
 * BitSet with the methods that were missing in Android 2.2.
 * </p>
 */
public class BitSet implements Serializable, Cloneable {
	private static final long serialVersionUID = 7997698588986878753L;

	private static final long ALL_ONES = ~0L;

	/**
	 * The bits. Access bit n thus:
	 * 
	 * boolean bit = (bits[n / 64] | (1 << n)) != 0;
	 * 
	 * Note that Java's shift operators truncate their rhs to the log2 size of
	 * the lhs. That is, there's no "% 64" needed because it's implicit in the
	 * shift.
	 * 
	 * TODO: would int[] be significantly more efficient for Android at the
	 * moment?
	 */
	private long[] bits;

	/**
	 * The number of elements of 'bits' that are actually in use (non-zero).
	 * Amongst other things, this guarantees that isEmpty is cheap, because we
	 * never have to examine the array.
	 */
	private transient int longCount;

	/**
	 * Updates 'longCount' by inspecting 'bits'. Assumes that the new longCount
	 * is <= the current longCount, to avoid scanning large tracts of empty
	 * array. This means it's safe to call directly after a clear operation that
	 * may have cleared the highest setKey bit, but not safe after an xor
	 * operation that may have cleared the highest setKey bit or made a new
	 * highest setKey bit. In that case, you'd need to setKey 'longCount' to a
	 * conservative estimate before calling this method.
	 */
	private void shrinkSize() {
		int i = longCount - 1;
		while (i >= 0 && bits[i] == 0) {
			--i;
		}
		this.longCount = i + 1;
	}

	/**
	 * Creates a new {@code BitSet} with size equal to 64 bits.
	 */
	public BitSet() {
		this(new long[1]);
	}

	/**
	 * Creates a new {@code BitSet} with size equal to {@code bitCount}, rounded
	 * up to a multiple of 64.
	 * 
	 * @throws NegativeArraySizeException
	 *             if {@code bitCount < 0}.
	 */
	public BitSet(final int bitCount) {
		if (bitCount < 0) {
			throw new NegativeArraySizeException(Integer.toString(bitCount));
		}
		this.bits = arrayForBits(bitCount);
		this.longCount = 0;
	}

	private BitSet(final long[] bits) {
		this.bits = bits;
		this.longCount = bits.length;
		shrinkSize();
	}

	private static long[] arrayForBits(final int bitCount) {
		return new long[(bitCount + 63) / 64];
	}

	@Override
	public Object clone() {
		try {
			final BitSet clone = (BitSet) super.clone();
			clone.bits = bits.clone();
			clone.shrinkSize();
			return clone;
		} catch (final CloneNotSupportedException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof BitSet)) {
			return false;
		}
		final BitSet lhs = (BitSet) o;
		if (this.longCount != lhs.longCount) {
			return false;
		}
		for (int i = 0; i < longCount; ++i) {
			if (bits[i] != lhs.bits[i]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Ensures that our long[] can hold at least 64 * desiredLongCount bits.
	 */
	private void ensureCapacity(final int desiredLongCount) {
		if (desiredLongCount <= bits.length) {
			return;
		}
		final int newLength = Math.max(desiredLongCount, bits.length * 2);
		final long[] newBits = new long[newLength];
		System.arraycopy(bits, 0, newBits, 0, longCount);
		this.bits = newBits;
		// 'longCount' is unchanged by this operation: the long[] is larger,
		// but you're not yet using any more of it.
	}

	@Override
	public int hashCode() {
		// The RI doesn't use Arrays.hashCode, and explicitly specifies this
		// algorithm.
		long x = 1234;
		for (int i = 0; i < longCount; ++i) {
			x ^= bits[i] * (i + 1);
		}
		return (int) ((x >> 32) ^ x);
	}

	/**
	 * Returns the bit at index {@code index}. Indexes greater than the current
	 * length return false.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code index < 0}.
	 */
	public boolean get(final int index) {
		if (index < 0) { // TODO: until we have an inlining JIT.
			checkIndex(index);
		}
		final int arrayIndex = index / 64;
		if (arrayIndex >= longCount) {
			return false;
		}
		return (bits[arrayIndex] & (1L << index)) != 0;
	}

	/**
	 * Sets the bit at index {@code index} to true.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code index < 0}.
	 */
	public void set(final int index) {
		if (index < 0) { // TODO: until we have an inlining JIT.
			checkIndex(index);
		}
		final int arrayIndex = index / 64;
		if (arrayIndex >= bits.length) {
			ensureCapacity(arrayIndex + 1);
		}
		bits[arrayIndex] |= (1L << index);
		longCount = Math.max(longCount, arrayIndex + 1);
	}

	/**
	 * Clears the bit at index {@code index}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code index < 0}.
	 */
	public void clear(final int index) {
		if (index < 0) { // TODO: until we have an inlining JIT.
			checkIndex(index);
		}
		final int arrayIndex = index / 64;
		if (arrayIndex >= longCount) {
			return;
		}
		bits[arrayIndex] &= ~(1L << index);
		shrinkSize();
	}

	/**
	 * Flips the bit at index {@code index}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code index < 0}.
	 */
	public void flip(final int index) {
		if (index < 0) { // TODO: until we have an inlining JIT.
			checkIndex(index);
		}
		final int arrayIndex = index / 64;
		if (arrayIndex >= bits.length) {
			ensureCapacity(arrayIndex + 1);
		}
		bits[arrayIndex] ^= (1L << index);
		longCount = Math.max(longCount, arrayIndex + 1);
		shrinkSize();
	}

	private void checkIndex(final int index) {
		if (index < 0) {
			throw new IndexOutOfBoundsException("index < 0: " + index);
		}
	}

	private void checkRange(final int fromIndex, final int toIndex) {
		if ((fromIndex | toIndex) < 0 || toIndex < fromIndex) {
			throw new IndexOutOfBoundsException("fromIndex=" + fromIndex
					+ " toIndex=" + toIndex);
		}
	}

	/**
	 * Returns a new {@code BitSet} containing the range of bits
	 * {@code [fromIndex, toIndex)}, shifted down so that the bit at
	 * {@code fromIndex} is at bit 0 in the new {@code BitSet}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code fromIndex} or {@code toIndex} is negative, or if
	 *             {@code toIndex} is smaller than {@code fromIndex}.
	 */
	public BitSet get(final int fromIndex, int toIndex) {
		checkRange(fromIndex, toIndex);

		final int last = 64 * longCount;
		if (fromIndex >= last || fromIndex == toIndex) {
			return new BitSet(0);
		}
		if (toIndex > last) {
			toIndex = last;
		}

		final int firstArrayIndex = fromIndex / 64;
		final int lastArrayIndex = (toIndex - 1) / 64;
		final long lowMask = ALL_ONES << fromIndex;
		final long highMask = ALL_ONES >>> -toIndex;

		if (firstArrayIndex == lastArrayIndex) {
			final long result = (bits[firstArrayIndex] & (lowMask & highMask)) >>> fromIndex;
			if (result == 0) {
				return new BitSet(0);
			}
			return new BitSet(new long[] { result });
		}

		final long[] newBits = new long[lastArrayIndex - firstArrayIndex + 1];

		// first fill in the first and last indexes in the new BitSet
		newBits[0] = bits[firstArrayIndex] & lowMask;
		newBits[newBits.length - 1] = bits[lastArrayIndex] & highMask;

		// fill in the in between elements of the new BitSet
		for (int i = 1; i < lastArrayIndex - firstArrayIndex; i++) {
			newBits[i] = bits[firstArrayIndex + i];
		}

		// shift all the elements in the new BitSet to the right
		final int numBitsToShift = fromIndex % 64;
		int actualLen = newBits.length;
		if (numBitsToShift != 0) {
			for (int i = 0; i < newBits.length; i++) {
				// shift the current element to the right regardless of
				// sign
				newBits[i] = newBits[i] >>> (numBitsToShift);

				// apply the last x bits of newBits[i+1] to the current
				// element
				if (i != newBits.length - 1) {
					newBits[i] |= newBits[i + 1] << -numBitsToShift;
				}
				if (newBits[i] != 0) {
					actualLen = i + 1;
				}
			}
		}
		return new BitSet(newBits);
	}

	/**
	 * Sets the bit at index {@code index} to {@code state}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code index < 0}.
	 */
	public void set(final int index, final boolean state) {
		if (state) {
			set(index);
		} else {
			clear(index);
		}
	}

	/**
	 * Sets the range of bits {@code [fromIndex, toIndex)} to {@code state}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code fromIndex} or {@code toIndex} is negative, or if
	 *             {@code toIndex} is smaller than {@code fromIndex}.
	 */
	public void set(final int fromIndex, final int toIndex, final boolean state) {
		if (state) {
			set(fromIndex, toIndex);
		} else {
			clear(fromIndex, toIndex);
		}
	}

	/**
	 * Clears all the bits in this {@code BitSet}. This method does not change
	 * the capacity. Use {@code clear} if you want to reuse this {@code BitSet}
	 * with the same capacity, but create a new {@code BitSet} if you're trying
	 * to potentially reclaim memory.
	 */
	public void clear() {
		Arrays.fill(bits, 0, longCount, 0L);
		longCount = 0;
	}

	/**
	 * Sets the range of bits {@code [fromIndex, toIndex)}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code fromIndex} or {@code toIndex} is negative, or if
	 *             {@code toIndex} is smaller than {@code fromIndex}.
	 */
	public void set(final int fromIndex, final int toIndex) {
		checkRange(fromIndex, toIndex);
		if (fromIndex == toIndex) {
			return;
		}
		final int firstArrayIndex = fromIndex / 64;
		final int lastArrayIndex = (toIndex - 1) / 64;
		if (lastArrayIndex >= bits.length) {
			ensureCapacity(lastArrayIndex + 1);
		}

		final long lowMask = ALL_ONES << fromIndex;
		final long highMask = ALL_ONES >>> -toIndex;
		if (firstArrayIndex == lastArrayIndex) {
			bits[firstArrayIndex] |= (lowMask & highMask);
		} else {
			int i = firstArrayIndex;
			bits[i++] |= lowMask;
			while (i < lastArrayIndex) {
				bits[i++] |= ALL_ONES;
			}
			bits[i++] |= highMask;
		}
		longCount = Math.max(longCount, lastArrayIndex + 1);
	}

	/**
	 * Clears the range of bits {@code [fromIndex, toIndex)}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code fromIndex} or {@code toIndex} is negative, or if
	 *             {@code toIndex} is smaller than {@code fromIndex}.
	 */
	public void clear(final int fromIndex, int toIndex) {
		checkRange(fromIndex, toIndex);
		if (fromIndex == toIndex || longCount == 0) {
			return;
		}
		final int last = 64 * longCount;
		if (fromIndex >= last) {
			return;
		}
		if (toIndex > last) {
			toIndex = last;
		}
		final int firstArrayIndex = fromIndex / 64;
		final int lastArrayIndex = (toIndex - 1) / 64;

		final long lowMask = ALL_ONES << fromIndex;
		final long highMask = ALL_ONES >>> -toIndex;
		if (firstArrayIndex == lastArrayIndex) {
			bits[firstArrayIndex] &= ~(lowMask & highMask);
		} else {
			int i = firstArrayIndex;
			bits[i++] &= ~lowMask;
			while (i < lastArrayIndex) {
				bits[i++] = 0L;
			}
			bits[i++] &= ~highMask;
		}
		shrinkSize();
	}

	/**
	 * Flips the range of bits {@code [fromIndex, toIndex)}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code fromIndex} or {@code toIndex} is negative, or if
	 *             {@code toIndex} is smaller than {@code fromIndex}.
	 */
	public void flip(final int fromIndex, final int toIndex) {
		checkRange(fromIndex, toIndex);
		if (fromIndex == toIndex) {
			return;
		}
		final int firstArrayIndex = fromIndex / 64;
		final int lastArrayIndex = (toIndex - 1) / 64;
		if (lastArrayIndex >= bits.length) {
			ensureCapacity(lastArrayIndex + 1);
		}

		final long lowMask = ALL_ONES << fromIndex;
		final long highMask = ALL_ONES >>> -toIndex;
		if (firstArrayIndex == lastArrayIndex) {
			bits[firstArrayIndex] ^= (lowMask & highMask);
		} else {
			int i = firstArrayIndex;
			bits[i++] ^= lowMask;
			while (i < lastArrayIndex) {
				bits[i++] ^= ALL_ONES;
			}
			bits[i++] ^= highMask;
		}
		longCount = Math.max(longCount, lastArrayIndex + 1);
		shrinkSize();
	}

	/**
	 * Returns true if {@code this.and(bs)} is non-empty, but may be faster than
	 * computing that.
	 */
	public boolean intersects(final BitSet bs) {
		final long[] bsBits = bs.bits;
		final int length = Math.min(this.longCount, bs.longCount);
		for (int i = 0; i < length; ++i) {
			if ((bits[i] & bsBits[i]) != 0L) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Logically ands the bits of this {@code BitSet} with {@code bs}.
	 */
	public void and(final BitSet bs) {
		final int minSize = Math.min(this.longCount, bs.longCount);
		for (int i = 0; i < minSize; ++i) {
			bits[i] &= bs.bits[i];
		}
		Arrays.fill(bits, minSize, longCount, 0L);
		shrinkSize();
	}

	/**
	 * Clears all bits in this {@code BitSet} which are also setKey in
	 * {@code bs}.
	 */
	public void andNot(final BitSet bs) {
		final int minSize = Math.min(this.longCount, bs.longCount);
		for (int i = 0; i < minSize; ++i) {
			bits[i] &= ~bs.bits[i];
		}
		shrinkSize();
	}

	/**
	 * Logically ors the bits of this {@code BitSet} with {@code bs}.
	 */
	public void or(final BitSet bs) {
		final int minSize = Math.min(this.longCount, bs.longCount);
		final int maxSize = Math.max(this.longCount, bs.longCount);
		ensureCapacity(maxSize);
		for (int i = 0; i < minSize; ++i) {
			bits[i] |= bs.bits[i];
		}
		if (bs.longCount > minSize) {
			System.arraycopy(bs.bits, minSize, bits, minSize, maxSize - minSize);
		}
		longCount = maxSize;
	}

	/**
	 * Logically xors the bits of this {@code BitSet} with {@code bs}.
	 */
	public void xor(final BitSet bs) {
		final int minSize = Math.min(this.longCount, bs.longCount);
		final int maxSize = Math.max(this.longCount, bs.longCount);
		ensureCapacity(maxSize);
		for (int i = 0; i < minSize; ++i) {
			bits[i] ^= bs.bits[i];
		}
		if (bs.longCount > minSize) {
			System.arraycopy(bs.bits, minSize, bits, minSize, maxSize - minSize);
		}
		longCount = maxSize;
		shrinkSize();
	}

	/**
	 * Returns the capacity in bits of the array implementing this
	 * {@code BitSet}. This is unrelated to the length of the {@code BitSet},
	 * and not generally useful. Use {@link #nextSetBit} to iterate, or
	 * {@link #length} to find the highest setKey bit.
	 */
	public int size() {
		return bits.length * 64;
	}

	/**
	 * Returns the number of bits up to and including the highest bit setKey.
	 * This is unrelated to the {@link #size} of the {@code BitSet}.
	 */
	public int length() {
		if (longCount == 0) {
			return 0;
		}
		return 64 * (longCount - 1)
				+ (64 - Long.numberOfLeadingZeros(bits[longCount - 1]));
	}

	/**
	 * Returns a string containing a concise, human-readable description of the
	 * receiver: a comma-delimited list of the indexes of all setKey bits. For
	 * example: {@code " 0,1,8}"}.
	 */
	@Override
	public String toString() {
		// System.err.println("BitSet[longCount=" + longCount + ",bits=" +
		// Arrays.toString(bits) + "]");
		final StringBuilder sb = new StringBuilder(longCount / 2);
		sb.append('{');
		boolean comma = false;
		for (int i = 0; i < longCount; ++i) {
			if (bits[i] != 0) {
				for (int j = 0; j < 64; ++j) {
					if ((bits[i] & 1L << j) != 0) {
						if (comma) {
							sb.append(", ");
						} else {
							comma = true;
						}
						sb.append(64 * i + j);
					}
				}
			}
		}
		sb.append('}');
		return sb.toString();
	}

	/**
	 * Returns the index of the first bit that is setKey on or after
	 * {@code index}, or -1 if no higher bits are setKey.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code index < 0}.
	 */
	public int nextSetBit(final int index) {
		checkIndex(index);
		int arrayIndex = index / 64;
		if (arrayIndex >= longCount) {
			return -1;
		}
		final long mask = ALL_ONES << index;
		if ((bits[arrayIndex] & mask) != 0) {
			return 64 * arrayIndex
					+ Long.numberOfTrailingZeros(bits[arrayIndex] & mask);
		}
		while (++arrayIndex < longCount && bits[arrayIndex] == 0) {
		}
		if (arrayIndex == longCount) {
			return -1;
		}
		return 64 * arrayIndex + Long.numberOfTrailingZeros(bits[arrayIndex]);
	}

	/**
	 * Returns the index of the first bit that is clear on or after
	 * {@code index}. Since all bits past the end are implicitly clear, this
	 * never returns -1.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code index < 0}.
	 */
	public int nextClearBit(final int index) {
		checkIndex(index);
		int arrayIndex = index / 64;
		if (arrayIndex >= longCount) {
			return index;
		}
		final long mask = ALL_ONES << index;
		if ((~bits[arrayIndex] & mask) != 0) {
			return 64 * arrayIndex
					+ Long.numberOfTrailingZeros(~bits[arrayIndex] & mask);
		}
		while (++arrayIndex < longCount && bits[arrayIndex] == ALL_ONES) {
		}
		if (arrayIndex == longCount) {
			return 64 * longCount;
		}
		return 64 * arrayIndex + Long.numberOfTrailingZeros(~bits[arrayIndex]);
	}

	/**
	 * Returns the index of the first bit that is setKey on or before
	 * {@code index} , or -1 if no lower bits are setKey or {@code index == -1}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code index < -1}.
	 * @since 1.7
	 */
	public int previousSetBit(final int index) {
		if (index == -1) {
			return -1;
		}
		checkIndex(index);
		// TODO: optimize this.
		for (int i = index; i >= 0; --i) {
			if (get(i)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Returns the index of the first bit that is clear on or before
	 * {@code index}, or -1 if no lower bits are clear or {@code index == -1}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if {@code index < -1}.
	 * @since 1.7
	 */
	public int previousClearBit(final int index) {
		if (index == -1) {
			return -1;
		}
		checkIndex(index);
		// TODO: optimize this.
		for (int i = index; i >= 0; --i) {
			if (!get(i)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Returns true if all the bits in this {@code BitSet} are setKey to false,
	 * false otherwise.
	 */
	public boolean isEmpty() {
		return (longCount == 0);
	}

	/**
	 * Returns the number of bits that are {@code true} in this {@code BitSet}.
	 */
	public int cardinality() {
		int result = 0;
		for (int i = 0; i < longCount; ++i) {
			result += Long.bitCount(bits[i]);
		}
		return result;
	}

	/**
	 * Equivalent to {@code BitSet.valueOf(LongBuffer.wrap(longs))}, but likely
	 * to be faster. This is likely to be the fastest way to create a
	 * {@code BitSet} because it's closest to the internal representation.
	 * 
	 * @since 1.7
	 */
	public static BitSet valueOf(final long[] longs) {
		return new BitSet(longs.clone());
	}

	/**
	 * Returns a {@code BitSet} corresponding to {@code longBuffer}, interpreted
	 * as a little-endian sequence of bits. This method does not alter the
	 * {@code LongBuffer}.
	 * 
	 * @since 1.7
	 */
	public static BitSet valueOf(final LongBuffer longBuffer) {
		// The bulk get would mutate LongBuffer (even if we reset position
		// later), and it's not
		// clear that's allowed. My assumption is that it's the long[] variant
		// that's the common
		// case anyway, so copy the buffer into a long[].
		final long[] longs = new long[longBuffer.remaining()];
		for (int i = 0; i < longs.length; ++i) {
			longs[i] = longBuffer.get(longBuffer.position() + i);
		}
		return BitSet.valueOf(longs);
	}

	/**
	 * Equivalent to {@code BitSet.valueOf(ByteBuffer.wrap(bytes))}.
	 * 
	 * @since 1.7
	 */
	public static BitSet valueOf(final byte[] bytes) {
		return BitSet.valueOf(ByteBuffer.wrap(bytes));
	}

	/**
	 * Returns a {@code BitSet} corresponding to {@code byteBuffer}, interpreted
	 * as a little-endian sequence of bits. This method does not alter the
	 * {@code ByteBuffer}.
	 * 
	 * @since 1.7
	 */
	public static BitSet valueOf(ByteBuffer bb) {
		bb = bb.slice().order(ByteOrder.LITTLE_ENDIAN);
		int n;
		for (n = bb.remaining(); n > 0 && bb.get(n - 1) == 0; n--)
			;
		final long[] words = new long[(n + 7) / 8];
		bb.limit(n);
		int i = 0;
		while (bb.remaining() >= 8)
			words[i++] = bb.getLong();
		for (int remaining = bb.remaining(), j = 0; j < remaining; j++)
			words[i] |= (bb.get() & 0xffL) << (8 * j);
		return BitSet.valueOf(words);
	}

	/**
	 * Returns a new {@code long[]} containing a little-endian representation of
	 * the bits of this {@code BitSet}, suitable for passing to {@code valueOf}
	 * to reconstruct this {@code BitSet}.
	 * 
	 * @since 1.7
	 */
	public long[] toLongArray() {
		return ArrayUtils.copyOf(bits, longCount);
	}

	/**
	 * Returns a new {@code byte[]} containing a little-endian representation
	 * the bits of this {@code BitSet}, suitable for passing to {@code valueOf}
	 * to reconstruct this {@code BitSet}.
	 * 
	 * @since 1.7
	 */
	public byte[] toByteArray() {
		final int bitCount = length();
		final byte[] result = new byte[(bitCount + 7) / 8];
		for (int i = 0; i < result.length; ++i) {
			final int lowBit = 8 * i;
			final int arrayIndex = lowBit / 64;
			result[i] = (byte) (bits[arrayIndex] >>> lowBit);
		}
		return result;
	}

	private void readObject(final ObjectInputStream ois) throws IOException,
			ClassNotFoundException {
		ois.defaultReadObject();
		// The serialized form doesn't include a 'longCount' field, so we'll
		// have to scan the array.
		this.longCount = this.bits.length;
		shrinkSize();
	}
}
