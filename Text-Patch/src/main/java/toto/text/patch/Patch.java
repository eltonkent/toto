package toto.text.patch;

import com.mi.toto.ToTo;

/**
 * Utility for applying patches to binary files.
 * <p>
 * Uses the implementation of the BSD Patch project
 * </p>
 */
public class Patch {

	/**
	 * Apply a patch file to an old file to generate a new file.
	 * 
	 * @param oldFile
	 * @param newFile
	 *            the expected path of the patched file. The file is created
	 *            after the path is complete
	 * @param patchFile
	 * @return 0 if the process failed >0 to indicate a success.
	 */
	public static int applyPatch(String oldFile, String newFile,
			String patchFile) {
		return patch(oldFile, newFile, patchFile);
	}

	private static native int patch(String old_file, String new_file,
			String patch_file);

	static {
		ToTo.loadLib("Text-Patch");
	}
}
