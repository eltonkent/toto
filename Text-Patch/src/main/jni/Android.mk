LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CPPFLAGS += -ffunction-sections -fdata-sections -fvisibility=hidden
LOCAL_CFLAGS += -ffunction-sections -fdata-sections
LOCAL_LDFLAGS += -Wl,--gc-sections
LOCAL_MODULE    := Text_Patch
LOCAL_C_INCLUDES :=$(LOCAL_PATH)/patch
LOCAL_SRC_FILES := Patch.cpp patch/blocksort.c patch/bzlib.c patch/compress.c patch/crctable.c patch/decompress.c patch/huffman.c patch/randtable.c patch/patch.c
include $(BUILD_SHARED_LIBRARY)
