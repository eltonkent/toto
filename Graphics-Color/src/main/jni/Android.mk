LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CPPFLAGS += -ffunction-sections -fdata-sections -fvisibility=hidden
LOCAL_CFLAGS += -ffunction-sections -fdata-sections  
LOCAL_LDFLAGS += -Wl,--gc-sections
LOCAL_MODULE    := Graphics_Color
LOCAL_CPPFLAGS  := -Wall -Werror -O2 -std=gnu++11
LOCAL_SRC_FILES := ColorConverterNative.cpp
LOCAL_LDLIBS    += -llog 
-std=gnu++11

include $(BUILD_SHARED_LIBRARY)