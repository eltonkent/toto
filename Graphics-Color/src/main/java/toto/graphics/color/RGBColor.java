package toto.graphics.color;

import java.io.Serializable;

public class RGBColor implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final RGBColor BLACK = new RGBColor(0, 0, 0);
	public static final RGBColor WHITE = new RGBColor(255, 255, 255);
	public static final RGBColor RED = new RGBColor(255, 0, 0);
	public static final RGBColor GREEN = new RGBColor(0, 255, 0);
	public static final RGBColor BLUE = new RGBColor(0, 0, 255);
	private static final float COLOR_INV = 0.003921569F;
	int red = 0;
	int blue = 0;

	int green = 0;
	public int alpha = 0;

	float nred = 0.0F;
	float nblue = 0.0F;
	float ngreen = 0.0F;
	float nalpha = 0.0F;

	public RGBColor() {
	}

	public RGBColor(int r, int g, int b) {
		this(r, g, b, 255);
	}

	public RGBColor(int r, int g, int b, int a) {
		this.red = Math.max(0, Math.min(255, r));
		this.blue = Math.max(0, Math.min(255, b));
		this.green = Math.max(0, Math.min(255, g));
		this.alpha = Math.max(0, Math.min(255, a));

		this.nred = (this.red * 0.003921569F);
		this.nblue = (this.blue * 0.003921569F);
		this.ngreen = (this.green * 0.003921569F);
		this.nalpha = (this.alpha * 0.003921569F);
	}

	public void setTo(int r, int g, int b, int a) {
		this.red = Math.max(0, Math.min(255, r));
		this.blue = Math.max(0, Math.min(255, b));
		this.green = Math.max(0, Math.min(255, g));
		this.alpha = Math.max(0, Math.min(255, a));

		this.nred = (this.red * 0.003921569F);
		this.nblue = (this.blue * 0.003921569F);
		this.ngreen = (this.green * 0.003921569F);
		this.nalpha = (this.alpha * 0.003921569F);
	}

	public float getNormalizedRed()
	/*     */{
		/* 87 */
		return this.nred;
		/*     */
	}

	public float getNormalizedGreen()
	/*     */{
		/* 95 */
		return this.ngreen;
		/*     */
	}

	public float getNormalizedBlue()
	/*     */{
		/* 103 */
		return this.nblue;
		/*     */
	}

	public float getNormalizedAlpha()
	/*     */{
		/* 110 */
		return this.nalpha;
		/*     */
	}

	public int getRed()
	/*     */{
		/* 118 */
		return this.red;
		/*     */
	}

	public int getBlue()
	/*     */{
		/* 126 */
		return this.blue;
		/*     */
	}

	public int getGreen()
	/*     */{
		/* 134 */
		return this.green;
		/*     */
	}

	public int getAlpha()
	/*     */{
		/* 142 */
		return this.alpha;
		/*     */
	}

	public int getRGB() {
		return this.red << 16 | this.green << 8 | this.blue;
	}

	public int getARGB() {
		return this.alpha << 24 | this.red << 16 | this.green << 8 | this.blue;
	}
}
