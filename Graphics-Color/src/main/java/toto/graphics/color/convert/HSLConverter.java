package toto.graphics.color.convert;

public class HSLConverter {

	/**
	 * Convert HSL values to a RGB Color.
	 * 
	 * @param h
	 *            Hue is specified as degrees in the range 0 - 360.
	 * @param s
	 *            Saturation is specified as a percentage in the range 1 - 100.
	 * @param l
	 *            Lumanance is specified as a percentage in the range 1 - 100.
	 * @param alpha
	 *            the alpha value between 0 - 1
	 * 
	 * @returns the RGB Color object
	 */
	public static int toRGB(float h, float s, float l, final float alpha) {
		if (s < 0.0f || s > 100.0f) {
			final String message = "Color parameter outside of expected range - Saturation";
			throw new IllegalArgumentException(message);
		}

		if (l < 0.0f || l > 100.0f) {
			final String message = "Color parameter outside of expected range - Luminance";
			throw new IllegalArgumentException(message);
		}

		if (alpha < 0.0f || alpha > 1.0f) {
			final String message = "Color parameter outside of expected range - Alpha";
			throw new IllegalArgumentException(message);
		}

		// Formula needs all values between 0 - 1.

		h = h % 360.0f;
		h /= 360f;
		s /= 100f;
		l /= 100f;

		float q = 0;

		if (l < 0.5)
			q = l * (1 + s);
		else
			q = (l + s) - (s * l);

		final float p = 2 * l - q;

		float r = Math.max(0, HueToRGB(p, q, h + (1.0f / 3.0f)));
		float g = Math.max(0, HueToRGB(p, q, h));
		float b = Math.max(0, HueToRGB(p, q, h - (1.0f / 3.0f)));

		r = Math.min(r, 1.0f);
		g = Math.min(g, 1.0f);
		b = Math.min(b, 1.0f);
		final int cR = (int) (r * 255 + 0.5);
		final int cG = (int) (g * 255 + 0.5);
		final int cB = (int) (b * 255 + 0.5);
		final int cA = (int) (alpha * 255 + 0.5);
		return ((cA & 0xFF) << 24) | ((cR & 0xFF) << 16) | ((cG & 0xFF) << 8)
				| ((cB & 0xFF) << 0);
	}

	private static float HueToRGB(final float p, final float q, float h) {
		if (h < 0)
			h += 1;

		if (h > 1)
			h -= 1;

		if (6 * h < 1) {
			return p + ((q - p) * 6 * h);
		}

		if (2 * h < 1) {
			return q;
		}

		if (3 * h < 2) {
			return p + ((q - p) * 6 * ((2.0f / 3.0f) - h));
		}

		return p;
	}

}
