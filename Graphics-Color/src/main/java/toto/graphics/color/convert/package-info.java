/**
 * Convert between RGB, HSB, HSL, YUV color models
 * @author ekent4
 *
 */
package toto.graphics.color.convert;