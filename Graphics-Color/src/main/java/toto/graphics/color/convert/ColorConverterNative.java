package toto.graphics.color.convert;

import com.mi.toto.ToTo;

/**
 * Implements color model conversion in native code.
 * 
 * @author Mobifluence Interactive
 * 
 */
class ColorConverterNative {

	static native void decodeNV21(int[] rgba, byte[] yu12, int width, int height);

	static native void decodeYUY2(int[] rgba, byte[] yu12, int width, int height);

	static {
		ToTo.loadLib("Graphics_Color");
	}
}
