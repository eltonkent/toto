package toto.graphics.color.convert;

/**
 * YUV (Luminance/Chrominance) Conversion utilities
 * 
 * @author ekent4
 * 
 */
public class YUVConverter {

	/**
	 * Decode a YUV420SP image to RGB.
	 * <p>
	 * YUV420SP is the image format returned by the camera preview callback
	 * <b>onPreviewFrame(byte[] data, Camera cam)</b>
	 * </p>
	 * 
	 * @param yuv420sp
	 *            Byte array representing a YUV420SP image.
	 * @param argb
	 *            Array to decode yuv values. or null
	 * @param width
	 *            Width of the image.
	 * @param height
	 *            Height of the image.
	 * @throws NullPointerException
	 *             if <code>yuv420sp</code> byte array is NULL.
	 * @return rgb array (for convienience)
	 */
	public static int[] toRGB(final byte[] yuv420sp, int[] argb,
			final int width, final int height) {
		if (yuv420sp == null)
			throw new NullPointerException();

		final int frameSize = width * height;
		if (argb == null) {
			argb = new int[frameSize];
		}

		for (int j = 0, yp = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
			for (int i = 0; i < width; i++, yp++) {
				int y = (0xff & (yuv420sp[yp])) - 16;
				if (y < 0)
					y = 0;
				if ((i & 1) == 0) {
					v = (0xff & yuv420sp[uvp++]) - 128;
					u = (0xff & yuv420sp[uvp++]) - 128;
				}
				final int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				int b = (y1192 + 2066 * u);

				if (r < 0)
					r = 0;
				else if (r > 262143)
					r = 262143;
				if (g < 0)
					g = 0;
				else if (g > 262143)
					g = 262143;
				if (b < 0)
					b = 0;
				else if (b > 262143)
					b = 262143;

				argb[yp] = 0xff000000 | ((r << 6) & 0xff0000)
						| ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
			}
		}
		return argb;
	}

	/**
	 * YUV to RGB array implemented in native code.
	 * 
	 * @param yuv
	 * @param width
	 * @param height
	 * @return
	 */
	public static int[] toRGBNative(final byte[] yuv, final int width,
			final int height) {
		final int[] argb = new int[width * height];
		ColorConverterNative.decodeYUY2(argb, yuv, width, height);
		return argb;
	}

	/**
	 * Decode a YUV420SP image to Luma.
	 * 
	 * @param yuv420sp
	 *            Byte array representing a YUV420SP image.
	 * @param width
	 *            Width of the image.
	 * @param height
	 *            Height of the image.
	 * @param luma
	 *            Array representing the luma image. or null
	 * @throws NullPointerException
	 *             if yuv420sp byte array is NULL or luma is null
	 * @throws IllegalArgumentException
	 *             if <code>lima</code> length does not match [
	 *             <code>width</code> *<code>height</code>]
	 */
	public static int[] toLuma(final byte[] yuv420sp, final int[] luma,
			final int width, final int height) {
		if (yuv420sp == null)
			throw new NullPointerException();

		final int frameSize = width * height;
		if (luma.length != frameSize) {
			throw new IllegalArgumentException(
					"luma length does not match image dimensions");
		}
		for (int j = 0, yp = 0; j < height; j++) {
			for (int i = 0; i < width; i++, yp++) {
				int y = (0xff & (yuv420sp[yp])) - 16;
				if (y < 0)
					y = 0;
				luma[yp] = y;
			}
		}
		return luma;
	}
}
