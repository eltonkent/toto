package toto.graphics.color.convert;

/**
 * YCrCb format used for images, which uses the NV21 encoding format.
 * 
 * @author Mobifluence Interactive
 * 
 */
public class NV21Converter {

	public static int[] toRGBNative(final byte[] yuv, final int width,
			final int height) {
		final int[] argb = new int[width * height];
		ColorConverterNative.decodeNV21(argb, yuv, width, height);
		return argb;
	}
}
