package toto.graphics.color.convert;

import android.graphics.Color;

/**
 * RGB (additive model) conversion utilities
 * 
 * @author ekent4
 * 
 */
public class RGBConverter {

	/**
	 * Convert from RGB to HSB color
	 * 
	 * @param r
	 * @param g
	 * @param b
	 * @param hsbvals
	 *            the array used to return the three HSB values, or null
	 * 
	 * @return a float array containing hue[0], saturation[1], brightness[2].
	 */
	public static float[] toHSB(final int r, final int g, final int b,
			float[] hsbvals) {
		float hue, saturation, brightness;
		if (hsbvals == null) {
			hsbvals = new float[3];
		}
		int cmax = (r > g) ? r : g;
		if (b > cmax)
			cmax = b;
		int cmin = (r < g) ? r : g;
		if (b < cmin)
			cmin = b;

		brightness = (cmax) / 255.0f;
		if (cmax != 0)
			saturation = ((float) (cmax - cmin)) / ((float) cmax);
		else
			saturation = 0;
		if (saturation == 0)
			hue = 0;
		else {
			final float redc = ((float) (cmax - r)) / ((float) (cmax - cmin));
			final float greenc = ((float) (cmax - g)) / ((float) (cmax - cmin));
			final float bluec = ((float) (cmax - b)) / ((float) (cmax - cmin));
			if (r == cmax)
				hue = bluec - greenc;
			else if (g == cmax)
				hue = 2.0f + redc - bluec;
			else
				hue = 4.0f + greenc - redc;
			hue = hue / 6.0f;
			if (hue < 0)
				hue = hue + 1.0f;
		}
		hsbvals[0] = hue;
		hsbvals[1] = saturation;
		hsbvals[2] = brightness;
		return hsbvals;
	}

	/**
	 * Convert color array to HSV color model
	 * <p>
	 * The HSL value is setKey in the RGB array with <code>H</code> at 0,
	 * <code>S</code> at 1, <code>L</code> at 2.
	 * </p>
	 * 
	 * @param rgb
	 *            The RGB color with <code>R</code> at 0, <code>G</code> at 1,
	 *            <code>B</code> at 2
	 * @return hsv color (for convienience)
	 */
	public static int[] toHSV(final int[] rgb) {
		for (int i = 0; i < rgb.length; i++) {

			final int nR = Color.red(rgb[i]);
			final int nG = Color.green(rgb[i]);
			final int nB = Color.blue(rgb[i]);
			int nMax, nMid, nMin;
			int nHueOffset;
			// determine color order
			if ((nR > nG) && (nR > nB)) {
				// red is max
				nMax = nR;
				nHueOffset = 0;
				if (nG > nB) {
					nMid = nG;
					nMin = nB;
				} else {
					nMid = nB;
					nMin = nG;
				}
			} else if ((nG > nR) && (nG > nB)) {
				// green is max
				nMax = nG;
				nHueOffset = 80;
				if (nR > nB) {
					nMid = nR;
					nMin = nB;
				} else {
					nMid = nB;
					nMin = nR;
				}
			} else {
				// blue is max
				nMax = nB;
				nHueOffset = 160;
				if (nR > nG) {
					nMid = nR;
					nMin = nG;
				} else {
					nMid = nG;
					nMin = nR;
				}
			}
			// if the max value is Byte.MIN_VALUE the RGB value
			// = 0 so the HSV value = 0 and needs no change.
			if (nMax > Byte.MIN_VALUE) {
				if (nMax == nMin) {
					// color is gray. Hue, saturation are 0.
					rgb[i] = Color.rgb(Byte.MIN_VALUE, Byte.MIN_VALUE,
							(byte) nMax);
				} else {
					// compute hue scaled from 0-240.
					final int nHue = Math.min(239, nHueOffset
							+ (40 * (nMid - nMin)) / (nMax - nMin));
					// compute saturation scaled from 0-255.
					final int nSat = Math.min(255, (256 * (nMax - nMin))
							/ (nMax - Byte.MIN_VALUE));
					rgb[i] = Color.rgb((byte) (nHue + Byte.MIN_VALUE),
							(byte) (nSat + Byte.MIN_VALUE), (byte) nMax);
				}
			}
		}
		return rgb;
	}

	/**
	 * Convert a color to grayscale
	 * 
	 * @param argb
	 *            color
	 * @return
	 */
	public static final int toGrayscale(final int argb) {
		final int[] colors = new int[4];
		colors[0] = (argb >> 24) & 0xff;
		colors[1] = (argb >> 16) & 0xff;
		colors[2] = (argb >> 8) & 0xff;
		colors[3] = argb & 0xff;
		return toGrayscale(colors);
	}

	/**
	 * Convert a argb color to grayscale
	 * 
	 * @param argb
	 *            color with <code>Alpha</code> at 0, <code>Red</code> at 1,
	 *            <code>Green</code> at 2 and <code>Blue</code> at 3
	 * @return grayscale color
	 */
	public static final int toGrayscale(final int[] argb) {
		final int gray = (argb[1] + argb[2] + argb[3]) / 3;
		return (argb[0] << 24) | (gray << 16) | (gray << 8) | gray;
	}

	/**
	 * Convert rgb values to yuv luminance
	 * 
	 * @param argb
	 * @param yuv420sp
	 *            array of (w*h) to populate yuv luminance. can be null.
	 * @param width
	 *            of the rgb array
	 * @param height
	 *            of the rgb array
	 * @return yuv420sp array (for convienience)
	 */
	public static final byte[] toYUV(final int[] rgb, byte[] yuv420sp,
			final int width, final int height) {
		if (yuv420sp == null) {
			yuv420sp = new byte[width * height];
		}
		float red, green, blue;
		int luminance;
		for (int i = 0; i < width * height; i++) {
			red = (rgb[i] >> 16) & 0xff;
			green = (rgb[i] >> 8) & 0xff;
			blue = (rgb[i]) & 0xff;
			luminance = (int) ((0.257f * red) + (0.504f * green)
					+ (0.098f * blue) + 16);
			yuv420sp[i] = (byte) (0xff & luminance);
		}
		return yuv420sp;

	}
}
