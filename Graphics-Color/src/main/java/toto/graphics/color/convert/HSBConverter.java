package toto.graphics.color.convert;

/**
 * 
 * @author ekent4
 * 
 */
public class HSBConverter {

	/**
	 * Convert from HSB color to RGB
	 * 
	 * @param hsb
	 *            with hue[0], saturation[1], brightness[2].
	 * @return RGB value
	 */
	public static int toRGB(final float[] hsb) {
		int i = 0;
		int j = 0;
		int k = 0;
		if (hsb[1] == 0.0F) {
			i = j = k = (int) (hsb[2] * 255F + 0.5F);
		} else {
			final float f3 = (hsb[0] - (float) Math.floor(hsb[0])) * 6F;
			final float f4 = f3 - (float) Math.floor(f3);
			final float f5 = hsb[2] * (1.0F - hsb[1]);
			final float f6 = hsb[2] * (1.0F - hsb[1] * f4);
			final float f7 = hsb[2] * (1.0F - hsb[1] * (1.0F - f4));
			switch ((int) f3) {
			case 0: // '\0'
				i = (int) (hsb[2] * 255F + 0.5F);
				j = (int) (f7 * 255F + 0.5F);
				k = (int) (f5 * 255F + 0.5F);
				break;

			case 1: // '\001'
				i = (int) (f6 * 255F + 0.5F);
				j = (int) (hsb[2] * 255F + 0.5F);
				k = (int) (f5 * 255F + 0.5F);
				break;

			case 2: // '\002'
				i = (int) (f5 * 255F + 0.5F);
				j = (int) (hsb[2] * 255F + 0.5F);
				k = (int) (f7 * 255F + 0.5F);
				break;

			case 3: // '\003'
				i = (int) (f5 * 255F + 0.5F);
				j = (int) (f6 * 255F + 0.5F);
				k = (int) (hsb[2] * 255F + 0.5F);
				break;

			case 4: // '\004'
				i = (int) (f7 * 255F + 0.5F);
				j = (int) (f5 * 255F + 0.5F);
				k = (int) (hsb[2] * 255F + 0.5F);
				break;

			case 5: // '\005'
				i = (int) (hsb[2] * 255F + 0.5F);
				j = (int) (f5 * 255F + 0.5F);
				k = (int) (f6 * 255F + 0.5F);
				break;
			}
		}
		return 0xff000000 | i << 16 | j << 8 | k << 0;
	}

	/**
	 * Convert from HSB color to RGB
	 * 
	 * @param hue
	 * @param saturation
	 * @param brightness
	 * @return RGB value of given hue, saturation, brightness.
	 */
	public static int toRGB(final float hue, final float saturation,
			final float brightness) {
		return toRGB(new float[] { hue, saturation, brightness });
	}
}
