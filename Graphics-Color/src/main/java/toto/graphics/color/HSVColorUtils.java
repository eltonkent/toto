package toto.graphics.color;

import toto.graphics.color.convert.RGBConverter;

public class HSVColorUtils {

	public static float[] toHSV(final int rgb) {

		final int[] rgbValues = new int[3];
		RGBUtils.getRGB(rgb, rgbValues);
		final float[] hsvValues = new float[3];
		RGBConverter.toHSB(rgbValues[0], rgbValues[1], rgbValues[2], hsvValues);
		return hsvValues;
	}
}
