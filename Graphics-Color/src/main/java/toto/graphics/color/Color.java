/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.graphics.color;

import android.util.Log;

public class Color {
	public int argb;

	public Color(final int argb) {
		this.argb = argb;
	}

	public int getRed() {
		return android.graphics.Color.red(argb);
	}

	public int getGreen() {
		return android.graphics.Color.green(argb);
	}

	public int getBlue() {
		return android.graphics.Color.blue(argb);
	}

	public void setColor(final int alpha, final int red, final int green,
			final int blue) {
		argb = android.graphics.Color.argb(alpha, red, green, blue);
	}

	public void getHSVColor(final float[] hsv) {
		android.graphics.Color.RGBToHSV(getRed(), getGreen(), getBlue(), hsv);
	}

	/**
	 * Convert int representation of a color to hex.
	 * 
	 * @return
	 */
	public static String asHexValue(final int color) {
		String hex = Integer.toHexString(color);
		if (hex.length() > 6)
			hex = hex.substring(2);
		Log.v("Zint", hex);
		return hex;
	}

}
