package toto.graphics.color;

import toto.graphics.color.convert.HSLConverter;

/**
 * The HSLColor class provides methods to manipulate HSL (Hue, Saturation
 * Luminance) values to create a corresponding Color object using the RGB
 * ColorSpace.
 * 
 * The HUE is the color, the Saturation is the purity of the color (with respect
 * to grey) and Luminance is the brightness of the color (with respect to black
 * and white)
 * 
 * The Hue is specified as an angel between 0 - 360 degrees where red is 0,
 * green is 120 and blue is 240. In between you have the colors of the rainbow.
 * Saturation is specified as a percentage between 0 - 100 where 100 is fully
 * saturated and 0 approaches gray. Luminance is specified as a percentage
 * between 0 - 100 where 0 is black and 100 is white.
 * 
 * In particular the HSL color space makes it easier change the Tone or Shade of
 * a color by adjusting the luminance value.
 * 
 * @author ekent4
 * 
 */
public class HSLUtils {

	private final float[] hsl;
	private final float alpha;

	/**
	 * Create a HSLColor object using an RGB Color object.
	 * 
	 * @param rgb
	 *            the aRGB Color object
	 */
	public HSLUtils(final int argb) {
		hsl = fromRGB(argb);
		alpha = RGBUtils.getAlpha(argb);
	}

	/**
	 * Create a RGB Color object based on this HSLColor with a different Hue
	 * value. The degrees specified is an absolute value.
	 * 
	 * @param degrees
	 *            - the Hue value between 0 - 360
	 * @return the RGB Color object
	 */
	public int adjustHue(final float degrees) {
		return HSLConverter.toRGB(degrees, hsl[1], hsl[2], alpha);
	}

	/**
	 * Create a RGB Color object based on this HSLColor with a different
	 * Luminance value. The percent specified is an absolute value.
	 * 
	 * @param percent
	 *            - the Luminance value between 0 - 100
	 * @return the RGB Color object
	 */
	public int adjustLuminance(final float percent) {
		return HSLConverter.toRGB(hsl[0], hsl[1], percent, alpha);
	}

	/**
	 * Create a RGB Color object based on this HSLColor with a different
	 * Saturation value. The percent specified is an absolute value.
	 * 
	 * @param percent
	 *            - the Saturation value between 0 - 100
	 * @return the RGB Color object
	 */
	public int adjustSaturation(final float percent) {
		return HSLConverter.toRGB(hsl[0], percent, hsl[2], alpha);
	}

	/**
	 * Create a RGB Color object based on this HSLColor with a different Shade.
	 * Changing the shade will return a darker color. The percent specified is a
	 * relative value.
	 * 
	 * @param percent
	 *            - the value between 0 - 100
	 * @return the RGB Color object
	 */
	public int adjustShade(final float percent) {
		final float multiplier = (100.0f - percent) / 100.0f;
		final float l = Math.max(0.0f, hsl[2] * multiplier);

		return HSLConverter.toRGB(hsl[0], hsl[1], l, alpha);
	}

	/**
	 * Create a RGB Color object based on this HSLColor with a different Tone.
	 * Changing the tone will return a lighter color. The percent specified is a
	 * relative value.
	 * 
	 * @param percent
	 *            - the value between 0 - 100
	 * @return the RGB Color object
	 */
	public int adjustTone(final float percent) {
		final float multiplier = (100.0f + percent) / 100.0f;
		final float l = Math.min(100.0f, hsl[2] * multiplier);
		return HSLConverter.toRGB(hsl[0], hsl[1], l, alpha);
	}

	/**
	 * Convert a RGB Color to it corresponding HSL values.
	 * 
	 * @return an array containing the 3 HSL values.
	 */
	public static float[] fromRGB(final int rgb) {
		// Get RGB values in the range 0 - 1

		final float r = RGBUtils.getRed(rgb);
		final float g = RGBUtils.getGreen(rgb);
		final float b = RGBUtils.getBlue(rgb);

		// Minimum and Maximum RGB values are used in the HSL calculations

		final float min = Math.min(r, Math.min(g, b));
		final float max = Math.max(r, Math.max(g, b));

		// Calculate the Hue

		float h = 0;

		if (max == min)
			h = 0;
		else if (max == r)
			h = ((60 * (g - b) / (max - min)) + 360) % 360;
		else if (max == g)
			h = (60 * (b - r) / (max - min)) + 120;
		else if (max == b)
			h = (60 * (r - g) / (max - min)) + 240;

		// Calculate the Luminance

		final float l = (max + min) / 2;
		System.out.println(max + " : " + min + " : " + l);

		// Calculate the Saturation

		float s = 0;

		if (max == min)
			s = 0;
		else if (l <= .5f)
			s = (max - min) / (max + min);
		else
			s = (max - min) / (2 - max - min);

		return new float[] { h, s * 100, l * 100 };
	}

	/**
	 * Convert HSL values to a RGB Color with a default alpha value of 1. H
	 * (Hue) is specified as degrees in the range 0 - 360. S (Saturation) is
	 * specified as a percentage in the range 1 - 100. L (Lumanance) is
	 * specified as a percentage in the range 1 - 100.
	 * 
	 * @param hsl
	 *            an array containing the 3 HSL values
	 * 
	 * @returns the RGB Color object
	 */
	public static int toRGB(final float[] hsl) {
		return toRGB(hsl, 1.0f);
	}

	/**
	 * Convert HSL values to a RGB Color. H (Hue) is specified as degrees in the
	 * range 0 - 360. S (Saturation) is specified as a percentage in the range 1
	 * - 100. L (Lumanance) is specified as a percentage in the range 1 - 100.
	 * 
	 * @param hsl
	 *            an array containing the 3 HSL values
	 * @param alpha
	 *            the alpha value between 0 - 1
	 * 
	 * @returns the RGB Color object
	 */
	public static int toRGB(final float[] hsl, final float alpha) {
		return HSLConverter.toRGB(hsl[0], hsl[1], hsl[2], alpha);
	}

	/**
	 * Convert HSL values to a RGB Color with a default alpha value of 1.
	 * 
	 * @param h
	 *            Hue is specified as degrees in the range 0 - 360.
	 * @param s
	 *            Saturation is specified as a percentage in the range 1 - 100.
	 * @param l
	 *            Lumanance is specified as a percentage in the range 1 - 100.
	 * 
	 * @returns the RGB Color object
	 */
	public static int toRGB(final float h, final float s, final float l) {
		return HSLConverter.toRGB(h, s, l, 1.0f);
	}

}
