package test.toto.graphics.color;

import test.toto.ToToTestCase;
import toto.graphics.color.convert.NV21Converter;
import toto.graphics.color.convert.YUVConverter;


public class NativeConverterTest extends ToToTestCase {

	public void testYUV() {
		byte[] test = toByteArray(1400000);
		int[] argb = YUVConverter.toRGBNative(test, 1, 1);
		assertEquals(argb[0], -10092544);
	}

	public void testNV21() {
		byte[] test = toByteArray(1400000);
		int[] argb = NV21Converter.toRGBNative(test, 1, 1);
		assertEquals(argb[0], -16751360);
	}
	private static byte[] toByteArray(final long data) {
		return new byte[] { (byte) (data >> 56 & 0xff),
				(byte) (data >> 48 & 0xff), (byte) (data >> 40 & 0xff),
				(byte) (data >> 32 & 0xff), (byte) (data >> 24 & 0xff),
				(byte) (data >> 16 & 0xff), (byte) (data >> 8 & 0xff),
				(byte) (data >> 0 & 0xff), };
	}

}
