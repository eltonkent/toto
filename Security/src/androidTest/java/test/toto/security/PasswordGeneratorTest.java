package test.toto.security;

import test.toto.ToToTestCase;
import toto.security.PasswordGenerator;

public class PasswordGeneratorTest extends ToToTestCase {

	public void testPasswordGenerator() {
		PasswordGenerator pwdGen = new PasswordGenerator(8);
		String pwd = pwdGen.generate();
		assertNotNull(pwd);
		assertEquals(8, pwd.length());
		logI(pwd);

	}
}
