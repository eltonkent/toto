/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.security;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.UUID;

import toto.io.codec.Base64;

public class SecurityUtils {

	/**
	 * Retrieves the private key from the specified keystore.
	 * 
	 * @param keystoreFile
	 *            the path to the keystore file
	 * @param keystorePass
	 *            the password that protects the keystore file
	 * @param keyAlias
	 *            the alias under which the private key is stored
	 * @param keyPass
	 *            the password protecting the private key
	 * @return the private key from the specified keystore
	 * @throws GeneralSecurityException
	 *             if the keystore cannot be loaded
	 * @throws IOException
	 *             if the file cannot be accessed
	 */
	public static PrivateKey getPrivateKeyFromKeystore(
			final String keystoreFile, final String keystorePass,
			final String keyAlias, final String keyPass) throws IOException,
			GeneralSecurityException {

		final KeyStore keyStore = KeyStore.getInstance(KeyStore
				.getDefaultType());
		FileInputStream keyStream = null;
		try {
			keyStream = new FileInputStream(keystoreFile);
			keyStore.load(keyStream, keystorePass.toCharArray());
			return (PrivateKey) keyStore
					.getKey(keyAlias, keyPass.toCharArray());
		} finally {
			if (keyStream != null) {
				keyStream.close();
			}
		}
	}

	/**
	 * Retrieves the public key from the specified keystore.
	 * 
	 * @param keystoreFile
	 *            the path to the keystore file
	 * @param keystorePass
	 *            the password that protects the keystore file
	 * @param keyAlias
	 *            the alias under which the keys is stored
	 * @return the public key from the specified keystore
	 * @throws GeneralSecurityException
	 *             if the keystore could not be loaded.
	 * @throws IOException
	 *             if the file cannot be accessed
	 */
	public static PublicKey getPublicKeyFromKeystore(final String keystoreFile,
			final String keystorePass, final String keyAlias)
			throws IOException, GeneralSecurityException {

		final KeyStore keyStore = KeyStore.getInstance(KeyStore
				.getDefaultType());
		FileInputStream keyStream = null;
		try {
			keyStream = new FileInputStream(keystoreFile);
			keyStore.load(keyStream, keystorePass.toCharArray());
			final Certificate cert = keyStore.getCertificate(keyAlias);
			return cert.getPublicKey();
		} finally {
			if (keyStream != null) {
				keyStream.close();
			}
		}
	}

	/**
	 * Treats the provided long as unsigned and converts it to a string.
	 * 
	 * @param
	 */
	public static String unsignedLongToString(final long value) {
		if (value >= 0) {
			return Long.toString(value);
		} else {
			// Split into two unsigned halves. As digits are printed out from
			// the bottom half, move data from the top half into the bottom
			// half
			final int max_dig = 20;
			final char[] cbuf = new char[max_dig];
			final int radix = 10;
			int dst = max_dig;
			long top = value >>> 32;
			long bot = value & 0xffffffffl;
			bot += (top % radix) << 32;
			top /= radix;
			while (bot > 0 || top > 0) {
				cbuf[--dst] = Character.forDigit((int) (bot % radix), radix);
				bot = (bot / radix) + ((top % radix) << 32);
				top /= radix;
			}
			return new String(cbuf, dst, max_dig - dst);
		}
	}

	/**
	 * Get a UUID as byte array
	 * 
	 * @param uuid
	 * @return
	 */
	public static byte[] asByteArray(final UUID uuid) {
		final long msb = uuid.getMostSignificantBits();
		final long lsb = uuid.getLeastSignificantBits();
		final byte[] buffer = new byte[16];
		for (int i = 0; i < 8; i++)
			buffer[i] = (byte) (msb >>> 8 * (7 - i));
		for (int i = 8; i < 16; i++)
			buffer[i] = (byte) (lsb >>> 8 * (7 - i));
		return buffer;
	}

	/**
	 * Get RSA public key from a PEM encoded string.
	 * 
	 * @param pemPublicKeyString
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 *             if there are problems decoding the public key string.
	 */
	public static PublicKey getPublicKey(final String pemPublicKeyString)
			throws GeneralSecurityException, IOException {
		final X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
				stripPublicKey(pemPublicKeyString));
		return KeyFactory.getInstance("RSA").generatePublic(publicKeySpec);
	}

	private static byte[] stripPublicKey(final String encodedPublicKey)
			throws IOException {
		final StringBuilder sb = new StringBuilder();
		for (final String s : encodedPublicKey.split("\n"))
			if (!s.startsWith("--"))
				sb.append(s);
		return Base64.decode(sb.toString());
	}

}
