/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.security;

public class HexUtils {
	private final static char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	private final static char[] HEX_DIGITS_LOWER = { '0', '1', '2', '3', '4',
			'5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	private static char[] encode(final byte[] data, final char[] toDigits) {
		final int l = data.length;
		final char[] out = new char[l << 1];
		// two characters form the hex value.
		for (int i = 0, j = 0; i < l; i++) {
			out[j++] = toDigits[(0xF0 & data[i]) >>> 4];
			out[j++] = toDigits[0x0F & data[i]];
		}
		return out;
	}

	/**
	 * Encode byte array to hex
	 * 
	 * @param data
	 * @param lowercase
	 *            use lower case hex digits for the encoding
	 * @return
	 */
	public static char[] encodeHex(final byte[] data, final boolean lowercase) {
		return encode(data, lowercase ? HEX_DIGITS_LOWER : HEX_DIGITS);
	}

	public static String dumpHexString(final byte[] array) {
		return dumpHexString(array, 0, array.length);
	}

	public static String dumpHexString(final byte[] array, final int offset,
			final int length) {
		final StringBuilder result = new StringBuilder();

		final byte[] line = new byte[16];
		int lineIndex = 0;

		result.append("\n0x");
		result.append(toHexString(offset));

		for (int i = offset; i < offset + length; i++) {
			if (lineIndex == 16) {
				result.append(" ");

				for (int j = 0; j < 16; j++) {
					if (line[j] > ' ' && line[j] < '~') {
						result.append(new String(line, j, 1));
					} else {
						result.append(".");
					}
				}

				result.append("\n0x");
				result.append(toHexString(i));
				lineIndex = 0;
			}

			final byte b = array[i];
			result.append(" ");
			result.append(HEX_DIGITS[(b >>> 4) & 0x0F]);
			result.append(HEX_DIGITS[b & 0x0F]);

			line[lineIndex++] = b;
		}

		if (lineIndex != 16) {
			int count = (16 - lineIndex) * 3;
			count++;
			for (int i = 0; i < count; i++) {
				result.append(" ");
			}

			for (int i = 0; i < lineIndex; i++) {
				if (line[i] > ' ' && line[i] < '~') {
					result.append(new String(line, i, 1));
				} else {
					result.append(".");
				}
			}
		}

		return result.toString();
	}

	public static String toHexString(final byte b) {
		return toHexString(toByteArray(b));
	}

	public static String toHexString(final byte[] array) {
		return toHexString(array, 0, array.length);
	}

	public static String toHexString(final byte[] array, final int offset,
			final int length) {
		final char[] buf = new char[length * 2];

		int bufIndex = 0;
		for (int i = offset; i < offset + length; i++) {
			final byte b = array[i];
			buf[bufIndex++] = HEX_DIGITS[(b >>> 4) & 0x0F];
			buf[bufIndex++] = HEX_DIGITS[b & 0x0F];
		}

		return new String(buf);
	}

	public static String toHexString(final int i) {
		return toHexString(toByteArray(i));
	}

	public static byte[] toByteArray(final byte b) {
		final byte[] array = new byte[1];
		array[0] = b;
		return array;
	}

	public static byte[] toByteArray(final int i) {
		final byte[] array = new byte[4];

		array[3] = (byte) (i & 0xFF);
		array[2] = (byte) ((i >> 8) & 0xFF);
		array[1] = (byte) ((i >> 16) & 0xFF);
		array[0] = (byte) ((i >> 24) & 0xFF);

		return array;
	}

	/**
	 * Convert a byte to hex
	 * 
	 * @param b
	 * @return
	 */
	public static int parseHex(final char b) {
		if (b >= '0' && b <= '9')
			return (b - '0');
		if (b >= 'A' && b <= 'F')
			return (b - 'A' + 10);
		if (b >= 'a' && b <= 'f')
			return (b - 'a' + 10);

		throw new IllegalArgumentException("Invalid hex char '" + b + "'");
	}

	public static byte[] toByteArray(final String hexString) {
		final int length = hexString.length();
		final byte[] buffer = new byte[length / 2];

		for (int i = 0; i < length; i += 2) {
			buffer[i / 2] = (byte) ((parseHex(hexString.charAt(i)) << 4) | parseHex(hexString
					.charAt(i + 1)));
		}

		return buffer;
	}
}
