package toto.security;

import java.util.ArrayList;
import java.util.Random;

/**
 * Highly customizable Random password generator.
 * <p>
 * </p>
 * 
 * @see toto.util.random.RandomString
 * @author Mobifluence Interactive
 * 
 */
public class PasswordGenerator {

	private int length;

	private boolean letters = true;
	private boolean capitalLetters = true;
	private boolean numbers = true;
	private boolean symbols = false;
	private boolean allowRepeating = true;

	/**
	 * Allow repeating characters.
	 * <p>
	 * During random selection of characters, there is a chance of characters to
	 * get repeated. This flags removes that possibility.
	 * </p>
	 * 
	 * @param allowRepeating
	 *            default is true.
	 */
	public final void setAllowCharacterRepeating(final boolean allowRepeating) {
		this.allowRepeating = allowRepeating;
	}

	private String allowedLetters = "abcdefghijklmnopqrstuvwxyz";

	public final String getAllowedLetters() {
		return allowedLetters;
	}

	/**
	 * Set the allowed lower case letters for the generated password
	 * <p>
	 * The default set is <code>abcdefghijklmnopqrstuvwxyz</code>
	 * </p>
	 * 
	 * @param allowedLetters
	 */
	public final void setAllowedLetters(final String allowedLetters) {
		this.allowedLetters = allowedLetters;
	}

	private final String allowedCapitalLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private final String allowedNumbers = "0123456789";
	private String allowedSymbols = "!#$%&/()=?,;.:-_}{*][*-+/";

	public final String getAllowedSymbols() {
		return allowedSymbols;
	}

	/**
	 * Set the allowed symbols that can be used for this password.
	 * <p>
	 * The default set is <code>!#$%&/()=?,;.:-_}{*][*-+/</code>
	 * </p>
	 * 
	 * @param allowedSymbols
	 */
	public final void setAllowedSymbols(final String allowedSymbols) {
		this.allowedSymbols = allowedSymbols;
	}

	/**
	 * Create a password generator with the given length
	 * 
	 * @param length
	 */
	public PasswordGenerator(final int length) {
		this.length = length;
	}

	public PasswordGenerator() {
		this(8);
	}

	/**
	 * Set the length of the password
	 * 
	 * @param l
	 */
	public void setLength(final int l) {
		length = l;
	}

	/**
	 * 
	 * @param value
	 */
	public void useLetters(final boolean value) {
		letters = value;
	}

	public void useCapitalLetters(final boolean value) {
		capitalLetters = value;
	}

	public void useNumbers(final boolean value) {
		numbers = value;
	}

	public void useSymbols(final boolean value) {
		symbols = value;
	}

	public String generate() {
		final Random rnd = new Random();
		final StringBuilder pwdString = new StringBuilder();
		final ArrayList<String> allowedCharsList = new ArrayList<String>();
		if (symbols) {
			allowedCharsList.add(allowedSymbols);
		}
		if (letters) {
			allowedCharsList.add(allowedLetters);
		}
		if (capitalLetters) {
			allowedCharsList.add(allowedCapitalLetters);
		}
		if (numbers) {
			allowedCharsList.add(allowedNumbers);
		}

		String currType, nextChar;
		String prevChar = "";
		int i, currIndx, nextCharIdx;
		for (i = 0; i < length; i++) {
			currIndx = rnd.nextInt(allowedCharsList.size());
			currType = allowedCharsList.get(currIndx);
			nextCharIdx = rnd.nextInt(currType.length());
			nextChar = currType.substring(nextCharIdx, nextCharIdx + 1);
			if (!allowRepeating) {
				while (prevChar.equals(nextChar)) {
					nextCharIdx = rnd.nextInt(currType.length());
					nextChar = currType.substring(nextCharIdx, nextCharIdx + 1);
				}
			}
			pwdString.append(nextChar);
			prevChar = nextChar;
		}
		return pwdString.toString();
	}
}
