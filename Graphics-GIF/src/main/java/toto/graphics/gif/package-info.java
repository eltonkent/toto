/**
 * Animated <a href="http://en.wikipedia.org/wiki/Graphics_Interchange_Format">GIF</a> file encoder and decoder.
 */
package toto.graphics.gif;