package test.toto.graphics.bitmap.filters;

import test.toto.ToToTestCase;
import toto.bitmap.ToToArrayBasedBitmap;
import toto.bitmap.ToToBitmap;
import toto.bitmap.filters.ArtisticFilters;
import toto.bitmap.filters.ColorFilters;
import toto.bitmap.filters.EnhancementFilters;
import toto.bitmap.filters.GlowFilters;
import toto.bitmap.filters.PixelateFilters;

/**
 * Test native filter implementations
 *
 */
public class NativeFiltersTest extends ToToTestCase {

	public void testGamma(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		//(getContext(), R.drawable.onepix,null);
		EnhancementFilters.adjustGammaNative(bitmap, 0.45);
		assertEquals(bitmap.getPixel(0, 0),-16776960);
	}
	
	public void testHDR(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		ArtisticFilters.HDRNative(bitmap);
		logI("after testHDR"+bitmap.getPixel(0, 0));
		assertEquals(bitmap.getPixel(0, 0),-16776192);
	}

	public void testPixellate(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		PixelateFilters.pixellateNative(bitmap,1);
		assertEquals(bitmap.getPixel(0, 0),-16181494);
	}
	
	public void testOilPaint(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		ArtisticFilters.oilPaintNative(bitmap);
		logI("after testOilPaint"+bitmap.getPixel(0, 0));
//		assertEquals(bitmap.getPixel(0, 0),-16181494);
	}

	public void testSketch(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		ArtisticFilters.sketchNative(bitmap);
		assertEquals(bitmap.getPixel(0, 0),-15658735);
	}
	
	public void testNeon(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		ArtisticFilters.sketchNative(bitmap);
		assertEquals(bitmap.getPixel(0, 0),-15658735);
	}
	
	public void testGrayscale(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		ColorFilters.grayScaleNative(bitmap,50);
		assertEquals(bitmap.getPixel(0, 0),-15724272);
	}
	
	public void testSoftGlow(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		GlowFilters.softGlowNative(bitmap,0.5);
		assertEquals(bitmap.getPixel(0, 0),-16048883);
	}
	
	public void testInvert(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		ColorFilters.invertNative(bitmap);
		assertEquals(bitmap.getPixel(0, 0),-595723);


	}
	
	public void testSepia(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		ColorFilters.sepiaNative(bitmap,50);
		assertEquals(bitmap.getPixel(0, 0),-9289714);
	}
	
	public void testBrightness(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		EnhancementFilters.adjustBrightnessNative(bitmap,50);
		assertEquals(bitmap.getPixel(0, 0),-12891844);

	}
	
	public void testContrast(){
		ToToBitmap bitmap=new ToToArrayBasedBitmap(new int[]{-16181494},1,1);
		EnhancementFilters.adjustContrastNative(bitmap,50);
		assertEquals(bitmap.getPixel(0, 0),-16777216);
	}
	

}

