/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;

import toto.bitmap.utils.BitmapUtils;
import toto.bitmap.ToToBitmap;
import toto.graphics.color.RGBUtils;
import android.graphics.Bitmap;

/**
 * Edge detection filters
 * <p>
 * <b>Edge Detection</b><br/>
 * <img src="../../../../resources/wholeimage_edge.png" ><br/>
 * 
 * <b>La place Edge Detection</b><br/>
 * <img src="../../../../resources/laplaceedge.png" ><br/>
 * 
 * <b>Simple Emboss</b><br/>
 * <img src="../../../../resources/simpleEmboss.png" ><br/>
 * <table>
 * <tr>
 * <th>Emboss with <code>emboss</code> setKey to false and
 * <code>bumpHeight</code> of 3</th>
 * <th>Emboss with <code>emboss</code> setKey to true and
 * <code>bumpHeight</code> of 3</th>
 * </tr>
 * <tr>
 * <td><img src="../../../../resources/emboss_false.png" ></td>
 * <td><img src="../../../../resources/emboss_true.png" ></td>
 * </tr>
 * </table>
 * </p>
 * 
 */
public class EdgeFilters {

	private final static float R2 = (float) Math.sqrt(2);
	public static float[] FREI_CHEN_H = { -1, -R2, -1, 0, 0, 0, 1, R2, 1, };

	public final static float[] FREI_CHEN_V = { -1, 0, 1, -R2, 0, R2, -1, 0, 1, };

	public final static float[] PREWITT_H = { -1, -1, -1, 0, 0, 0, 1, 1, 1, };

	public final static float[] PREWITT_V = { -1, 0, 1, -1, 0, 1, -1, 0, 1, };

	public final static float[] ROBERTS_H = { -1, 0, 0, 0, 1, 0, 0, 0, 0, };

	public final static float[] ROBERTS_V = { 0, 0, -1, 0, 1, 0, 0, 0, 0, };
	public final static float[] SOBEL_H = { -1, -2, -1, 0, 0, 0, 1, 2, 1, };
	public final static float[] SOBEL_V = { -1, 0, 1, -2, 0, 2, -1, 0, 1, };

	/**
	 * An edge-detection filter.
	 * 
	 * @param src
	 * @param vEdgeMatrix
	 *            can be {@link #ROBERTS_V}, {@link #PREWITT_V},{@link #SOBEL_V}
	 *            or {@link #FREI_CHEN_V}. recommended: {@link #SOBEL_V}
	 * @param hEdgeMatrix
	 *            can be {@link #ROBERTS_H}, {@link #PREWITT_H},{@link #SOBEL_H}
	 *            or {@link #FREI_CHEN_H}. recommended: {@link #SOBEL_H}
	 * @param outputConfig
	 * @return
	 */
	public static Bitmap detectEdge(final Bitmap src,
			final float[] vEdgeMatrix, final float[] hEdgeMatrix,
			final Bitmap.Config outputConfig) {
		int index = 0;
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = BitmapUtils.getPixels(src);
		final int[] outPixels = new int[inPixels.length];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int r = 0, g = 0, b = 0;
				int rh = 0, gh = 0, bh = 0;
				int rv = 0, gv = 0, bv = 0;
				final int a = inPixels[y * width + x] & 0xff000000;

				for (int row = -1; row <= 1; row++) {
					final int iy = y + row;
					int ioffset;
					if (0 <= iy && iy < height)
						ioffset = iy * width;
					else
						ioffset = y * width;
					final int moffset = 3 * (row + 1) + 1;
					for (int col = -1; col <= 1; col++) {
						int ix = x + col;
						if (!(0 <= ix && ix < width))
							ix = x;
						final int rgb = inPixels[ioffset + ix];
						final float h = hEdgeMatrix[moffset + col];
						final float v = vEdgeMatrix[moffset + col];

						r = (rgb & 0xff0000) >> 16;
						g = (rgb & 0x00ff00) >> 8;
						b = rgb & 0x0000ff;
						rh += (int) (h * r);
						gh += (int) (h * g);
						bh += (int) (h * b);
						rv += (int) (v * r);
						gv += (int) (v * g);
						bv += (int) (v * b);
					}
				}
				r = (int) (Math.sqrt(rh * rh + rv * rv) / 1.8);
				g = (int) (Math.sqrt(gh * gh + gv * gv) / 1.8);
				b = (int) (Math.sqrt(bh * bh + bv * bv) / 1.8);
				r = RGBUtils.clamp(r);
				g = RGBUtils.clamp(g);
				b = RGBUtils.clamp(b);
				outPixels[index++] = a | (r << 16) | (g << 8) | b;
			}

		}
		return Bitmap.createBitmap(outPixels, src.getWidth(), src.getHeight(),
				outputConfig);
	}

	/**
	 * Emboss the given bitmap
	 * 
	 * @param src
	 * @param lightDirection
	 *            Direction of the light. min:0 max:360
	 * @param lightElevation
	 *            brightness. min:0 max:90
	 * 
	 * @param bumpHeight
	 *            Height of the emobssed parts of the image. min:0 max:100
	 * @param emboss
	 *            if true, the bitmap is embossed retaining its color. else a
	 *            grayscale image is embossed.
	 * @param outputConfig
	 * @return
	 */
	public static void emboss(final ToToBitmap src, final float lightDirection,
			final float lightElevation, final float bumpHeight,
			final boolean emboss, final Bitmap.Config outputConfig) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		int index = 0;
		final int[] inPixels = src.getPixels();
		final int[] outPixels = new int[inPixels.length];
		int[] bumpPixels;
		int bumpMapWidth, bumpMapHeight;
		final float pixelScale = 255.9f;
		final float width45 = 3 * bumpHeight;

		bumpMapWidth = width;
		bumpMapHeight = height;
		bumpPixels = new int[bumpMapWidth * bumpMapHeight];
		for (int i = 0; i < inPixels.length; i++)
			bumpPixels[i] = RGBUtils.brightness(inPixels[i]);

		int Nx, Ny, Nz, Lx, Ly, Lz, Nz2, NzLz, NdotL;
		int shade, background;

		Lx = (int) (Math.cos(lightDirection) * Math.cos(lightElevation) * pixelScale);
		Ly = (int) (Math.sin(lightDirection) * Math.cos(lightElevation) * pixelScale);
		Lz = (int) (Math.sin(lightElevation) * pixelScale);

		Nz = (int) (6 * 255 / width45);
		Nz2 = Nz * Nz;
		NzLz = Nz * Lz;

		background = Lz;

		int bumpIndex = 0;

		for (int y = 0; y < height; y++, bumpIndex += bumpMapWidth) {
			int s1 = bumpIndex;
			int s2 = s1 + bumpMapWidth;
			int s3 = s2 + bumpMapWidth;

			for (int x = 0; x < width; x++, s1++, s2++, s3++) {
				if (y != 0 && y < height - 2 && x != 0 && x < width - 2) {
					Nx = bumpPixels[s1 - 1] + bumpPixels[s2 - 1]
							+ bumpPixels[s3 - 1] - bumpPixels[s1 + 1]
							- bumpPixels[s2 + 1] - bumpPixels[s3 + 1];
					Ny = bumpPixels[s3 - 1] + bumpPixels[s3]
							+ bumpPixels[s3 + 1] - bumpPixels[s1 - 1]
							- bumpPixels[s1] - bumpPixels[s1 + 1];

					if (Nx == 0 && Ny == 0)
						shade = background;
					else if ((NdotL = Nx * Lx + Ny * Ly + NzLz) < 0)
						shade = 0;
					else
						shade = (int) (NdotL / Math.sqrt(Nx * Nx + Ny * Ny
								+ Nz2));
				} else
					shade = background;

				if (emboss) {
					final int rgb = inPixels[index];
					final int a = rgb & 0xff000000;
					int r = (rgb >> 16) & 0xff;
					int g = (rgb >> 8) & 0xff;
					int b = rgb & 0xff;
					r = (r * shade) >> 8;
					g = (g * shade) >> 8;
					b = (b * shade) >> 8;
					outPixels[index++] = a | (r << 16) | (g << 8) | b;
				} else
					outPixels[index++] = 0xff000000 | (shade << 16)
							| (shade << 8) | shade;
			}
		}
		src.setPixels(outPixels, src.getWidth(), src.getHeight());// new
																	// RageJavaBitmap(outPixels,
																	// src.getWidth(),
																	// src.getHeight(),
		// outputConfig);
	}

	/**
	 * Apply emboss filter to the given image data
	 * 
	 * @param bitmap
	 *            of the image
	 * @param outputConfig
	 *            Bitmap configuration of the output bitmap
	 * @return
	 */
	public static void simpleEmboss(final ToToBitmap bitmap) {

		final byte[][] filter = { { -2, 0, 0 }, { 0, 1, 0 }, { 0, 0, 2 } };
		GenericFilters.applyFilter(bitmap, 100, filter);
	}

	// if (threshold < 0 || threshold > 255) {
	// throw new IllegalArgumentException("Threshold out of range.");
	// }
	//
	// if (widGaussianKernel < 3 || widGaussianKernel > 40) {
	// throw new IllegalArgumentException(
	// "widGaussianKernel out of its range.");
	// }

	/**
	 * Detect Edges using <a
	 * href="http://en.wikipedia.org/wiki/Canny_edge_detector">Canny Edge
	 * Detector</a>
	 * 
	 * @see #cannyEdge(toto.bitmap.ToToBitmap, int, int, int, int, float)
	 * 
	 * @param bitmap
	 */
	public static void cannyEdge(final ToToBitmap bitmap) {
		final CannyEdgeDetector detector = new CannyEdgeDetector(bitmap);
		detector.process();
	}

	/**
	 * Detect Edges using <a
	 * href="http://en.wikipedia.org/wiki/Canny_edge_detector">Canny Edge
	 * Detector</a>
	 * 
	 * @param bitmap
	 * @param highThreshold
	 *            detection high threshold. min 10.
	 * @param lowThreshold
	 *            detection high threshold. min 1.
	 * @param threshold
	 *            detection high threshold. Range of 0-255 .
	 * @param gaussianKernelSize
	 *            detection high threshold. Range of 3-40.
	 * @param sigma
	 *            canny filter sigma. > 1.0.
	 */
	public static void cannyEdge(final ToToBitmap bitmap, final int highThreshold,
			final int lowThreshold, final int threshold,
			final int gaussianKernelSize, final float sigma) {
		final CannyEdgeDetector detector = new CannyEdgeDetector(bitmap, highThreshold,
				lowThreshold, threshold, gaussianKernelSize, sigma);
		detector.process();
	}

	/**
	 * A simple embossing filter.
	 * 
	 * @param src
	 * @param edgeAction
	 *            use the EdgeAction constants defined in {@link GenericFilters}
	 *            . Recommended: {@link GenericFilters#CLAMP_EDGES}
	 * @param processAlpha
	 * @param premultiplyAlpha
	 * @return
	 */
	public static void bump(final ToToBitmap src, final int edgeAction,
			final boolean processAlpha, final boolean premultiplyAlpha) {
		final float[] embossMatrix = { -1.0f, -1.0f, 0.0f, -1.0f, 1.0f, 1.0f,
				0.0f, 1.0f, 1.0f };
		ConvolveUtils.doConvolve(embossMatrix, src, edgeAction, processAlpha,
				premultiplyAlpha);
	}

	private static void brightness(final int[] row) {
		for (int i = 0; i < row.length; i++) {
			final int rgb = row[i];
			final int r = rgb >> 16 & 0xff;
			final int g = rgb >> 8 & 0xff;
			final int b = rgb & 0xff;
			row[i] = (r + g + b) / 3;
		}
	}

	/**
	 * Edge detection using the Laplacian operator
	 * 
	 * @param src
	 * @param outputConfig
	 * @return
	 */
	public static Bitmap laPlaceEdge(final Bitmap src,
			final Bitmap.Config outputConfig) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = BitmapUtils.getPixels(src);
		int[] row1 = new int[width];
		int[] row2 = new int[width];
		int[] row3 = new int[width];
		final int[] pixels = new int[width];
		src.getPixels(row1, 0, width, 0, 0, width, 1);
		src.getPixels(row2, 0, width, 0, 1, width, 1);

		brightness(row1);
		brightness(row2);
		for (int y = 0; y < height; y++) {
			if (y < height - 1) {
				src.getPixels(row3, 0, width, 0, y + 1, width, 1);// getRGB(src,
																	// 0, y + 1,
																	// width, 1,
																	// row3);
				brightness(row3);
			}
			pixels[0] = pixels[width - 1] = 0xff000000;// FIXME
			for (int x = 1; x < width - 1; x++) {
				final int l1 = row2[x - 1];
				final int l2 = row1[x];
				final int l3 = row3[x];
				final int l4 = row2[x + 1];

				final int l = row2[x];
				final int max = Math.max(Math.max(l1, l2), Math.max(l3, l4));
				final int min = Math.min(Math.min(l1, l2), Math.min(l3, l4));

				final int gradient = (int) (0.5f * Math.max((max - l),
						(l - min)));

				final int r = ((row1[x - 1] + row1[x] + row1[x + 1]
						+ row2[x - 1] - (8 * row2[x]) + row2[x + 1]
						+ row3[x - 1] + row3[x] + row3[x + 1]) > 0) ? gradient
						: (128 + gradient);
				pixels[x] = r;
			}
			BitmapUtils.setPixelRow(pixels, y, width, inPixels);
			// setRGB(dst, 0, y, width, 1, pixels);
			final int[] t = row1;
			row1 = row2;
			row2 = row3;
			row3 = t;
		}

		BitmapUtils.getPixelRow(inPixels, row1, 0, 0, width);// getRGB(dst, 0,
																// 0, width, 1,
																// row1);
		BitmapUtils.getPixelRow(inPixels, row2, 0, 1, width);// getRGB(dst, 0,
																// 0, width, 1,
																// row2);
		for (int y = 0; y < height; y++) {
			if (y < height - 1) {
				BitmapUtils.getPixelRow(inPixels, row3, 0, y + 1, width);// getRGB(dst,
																			// 0,
																			// y
																			// +
																			// 1,
																			// width,
																			// 1,
																			// row3);
			}
			pixels[0] = pixels[width - 1] = 0xff000000;// FIXME
			for (int x = 1; x < width - 1; x++) {
				int r = row2[x];
				r = (((r <= 128) && ((row1[x - 1] > 128) || (row1[x] > 128)
						|| (row1[x + 1] > 128) || (row2[x - 1] > 128)
						|| (row2[x + 1] > 128) || (row3[x - 1] > 128)
						|| (row3[x] > 128) || (row3[x + 1] > 128))) ? ((r >= 128) ? (r - 128)
						: r)
						: 0);

				pixels[x] = 0xff000000 | (r << 16) | (r << 8) | r;
			}
			// setRGB(dst, 0, y, width, 1, pixels);
			BitmapUtils.setPixelRow(pixels, y, width, inPixels);
			final int[] t = row1;
			row1 = row2;
			row2 = row3;
			row3 = t;
		}

		return Bitmap.createBitmap(inPixels, width, height, outputConfig);
	}

	// public static Bitmap chrome(Bitmap src, Config outputConfig) {
	//
	// }
}
