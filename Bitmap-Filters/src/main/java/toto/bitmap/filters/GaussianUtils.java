/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;

import toto.graphics.color.RGBUtils;

/**
 * Gaussian theory utils
 * 
 * @author elton.stephen.kent
 * 
 */
class GaussianUtils {

	/**
	 * Blur and transpose a block of ARGB pixels.
	 * 
	 * @param kernel
	 *            the blur kernel
	 * @param inPixels
	 *            the input pixels
	 * @param outPixels
	 *            the output pixels
	 * @param width
	 *            the width of the pixel array
	 * @param height
	 *            the height of the pixel array
	 * @param alpha
	 *            whether to blur the alpha channel
	 * @param edgeAction
	 *            what to do at the edges
	 */
	static void convolveAndTranspose(final Kernel kernel, final int[] inPixels,
			final int[] outPixels, final int width, final int height,
			final boolean alpha, final boolean premultiply,
			final boolean unpremultiply, final int edgeAction) {
		final float[] matrix = kernel.getKernelData(null);
		final int cols = kernel.getWidth();
		final int cols2 = cols / 2;

		for (int y = 0; y < height; y++) {
			int index = y;
			final int ioffset = y * width;
			for (int x = 0; x < width; x++) {
				float r = 0, g = 0, b = 0, a = 0;
				final int moffset = cols2;
				for (int col = -cols2; col <= cols2; col++) {
					final float f = matrix[moffset + col];

					if (f != 0) {
						int ix = x + col;
						if (ix < 0) {
							if (edgeAction == GenericFilters.CLAMP_EDGES)
								ix = 0;
							else if (edgeAction == GenericFilters.WRAP_EDGES)
								ix = (x + width) % width;
						} else if (ix >= width) {
							if (edgeAction == GenericFilters.CLAMP_EDGES)
								ix = width - 1;
							else if (edgeAction == GenericFilters.WRAP_EDGES)
								ix = (x + width) % width;
						}
						final int rgb = inPixels[ioffset + ix];
						final int pa = (rgb >> 24) & 0xff;
						int pr = (rgb >> 16) & 0xff;
						int pg = (rgb >> 8) & 0xff;
						int pb = rgb & 0xff;
						if (premultiply) {
							final float a255 = pa * (1.0f / 255.0f);
							pr *= a255;
							pg *= a255;
							pb *= a255;
						}
						a += f * pa;
						r += f * pr;
						g += f * pg;
						b += f * pb;
					}
				}
				if (unpremultiply && a != 0 && a != 255) {
					final float f = 255.0f / a;
					r *= f;
					g *= f;
					b *= f;
				}
				final int ia = alpha ? RGBUtils.clamp((int) (a + 0.5)) : 0xff;
				final int ir = RGBUtils.clamp((int) (r + 0.5));
				final int ig = RGBUtils.clamp((int) (g + 0.5));
				final int ib = RGBUtils.clamp((int) (b + 0.5));
				outPixels[index] = (ia << 24) | (ir << 16) | (ig << 8) | ib;
				index += height;
			}
		}
	}

	/**
	 * Make a Gaussian blur kernel.
	 * 
	 * @param radius
	 *            the blur radius
	 * @return the kernel
	 */
	static Kernel makeKernel(final float radius) {
		final int r = (int) Math.ceil(radius);
		final int rows = r * 2 + 1;
		final float[] matrix = new float[rows];
		final float sigma = radius / 3;
		final float sigma22 = 2 * sigma * sigma;
		final float sigmaPi2 = 2 * ImageMath.PI * sigma;
		final float sqrtSigmaPi2 = (float) Math.sqrt(sigmaPi2);
		final float radius2 = radius * radius;
		float total = 0;
		int index = 0;
		for (int row = -r; row <= r; row++) {
			final float distance = row * row;
			if (distance > radius2)
				matrix[index] = 0;
			else
				matrix[index] = (float) Math.exp(-(distance) / sigma22)
						/ sqrtSigmaPi2;
			total += matrix[index];
			index++;
		}
		for (int i = 0; i < rows; i++)
			matrix[i] /= total;

		return new Kernel(rows, 1, matrix);
	}

}
