/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;

import com.mi.toto.ToTo;
import toto.bitmap.ToToBitmap;
import toto.bitmap.filters.nativ.NativeFilters;
import toto.graphics.color.RGBUtils;

/**
 * Basic bitmap enhancement operations.
 * <p>
 * <b>Exposure</b>
 * <table>
 * <tr>
 * <th>Normal Image</th>
 * <th>Exposure setKey to 3</th>
 * </tr>
 * <tr>
 * <td><img src="../../../../resources/src.png" ></td>
 * <td><img src="../../../../resources/exposure.png" ></td>
 * </tr>
 * </table>
 * <br/>
 * <b>Gain and Bias</b><br/>
 * Gain and bias with each setKey to 0.10 . Adjustment applied to one half of
 * the image. the remaining half is the normal image<br/>
 * <img src="../../../../resources/gainbias.png" ><br/>
 * 
 * <b>Gamma</b><br/>
 * Gamma with all three channels setKey to 0.75. Adjustment applied to one half
 * of the image. the remaining half is the normal image<br/>
 * <img src="../../../../resources/gamma.png" ><br/>
 * <b>DeSpeckle</b><br/>
 * <img src="../../../../resources/despeckle.png" ><br/>
 * <b>HSB adjust</b><br/>
 * HSB adjusted with <code>hFactor</code> setKey to 0.5 and <code>sFactor</code>
 * setKey to 0.5. . Adjustment applied to one half of the image. the remaining
 * half is the normal image<br/>
 * <img src="../../../../resources/hsbadjust.png" ><br/>
 * <b>RGB adjust</b><br/>
 * RGB adjusted with <code>rFactor</code> setKey to 0.5 and <code>gFactor</code>
 * setKey to 0.5. . Adjustment applied to one half of the image. the remaining
 * half is the normal image<br/>
 * <img src="../../../../resources/rgbadjust.png" ><br/>
 * </p>
 */
public class EnhancementFilters {

	/**
	 * A filter which performs a simple 3x3 sharpening operation.
	 * 
	 * @param src
	 * @param edgeAction
	 *            use the EdgeAction constants defined in {@link GenericFilters}
	 *            . Recommended: {@link GenericFilters#CLAMP_EDGES}
	 * @param processAlpha
	 * @param premultiplyAlpha
	 */
	public static void sharpen(final ToToBitmap src, final int edgeAction,
			final boolean processAlpha, final boolean premultiplyAlpha) {
		final float[] sharpenMatrix = { 0.0f, -0.2f, 0.0f, -0.2f, 1.8f, -0.2f,
				0.0f, -0.2f, 0.0f };
		ConvolveUtils.doConvolve(sharpenMatrix, src, edgeAction, processAlpha,
				premultiplyAlpha);
	}

	/**
	 * Sharpen filter. implemented in native code.
	 * 
	 * @param bitmap
	 */
	public static void sharpenNative(final ToToBitmap bitmap) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters
				.sharpenFilter(pixels, bitmap.getFilterX(),
						bitmap.getFilterY(), bitmap.getFilterWidth(),
						bitmap.getFilterHeight(), bitmap.getWidth(),
						bitmap.getHeight());
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * <a href="http://en.wikipedia.org/wiki/Gamma_correction">Gamma
	 * correction</a> implemented in native code.
	 * <p>
	 * Gamma (Y) has the following possible values - <b>2</b>, <b>1</b>
	 * (original), <b>1/2</b>(0.5), <b>1/3</b>(0.3333), <b>1/4</b>(0.25)
	 * </p>
	 * 
	 * @param bitmap
	 * @param gamma
	 */
	public static void adjustGammaNative(final ToToBitmap bitmap,
			final double gamma) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.gammaCorrection(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), gamma);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Relief filter. implemented in native code.
	 * 
	 * @param bitmap
	 */
	public static void reliefNative(final ToToBitmap bitmap) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters
				.reliefFilter(pixels, bitmap.getFilterX(), bitmap.getFilterY(),
						bitmap.getFilterWidth(), bitmap.getFilterHeight(),
						bitmap.getWidth(), bitmap.getHeight());
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	private static float transferBiasGainFunction(float f, final float gain,
			final float bias) {
		f = ImageMath.gain(f, gain);
		f = ImageMath.bias(f, bias);
		return f;
	}

	private static int[] makeGainBiasTable(final float gain, final float bias) {
		final int[] table = new int[256];
		for (int i = 0; i < 256; i++)
			table[i] = RGBUtils.clamp((int) (255 * transferBiasGainFunction(
					i / 255.0f, gain, bias)));
		return table;
	}

	/**
	 * Adjust the gain and bias for the given image
	 * 
	 * @param bitmap
	 * @param gain
	 *            min:0 max:1. recommended: 0.5.
	 * @param bias
	 *            min:0 max:1. recommended: 0.5.
	 */
	public static void setGainAndBias(final ToToBitmap bitmap, final float gain,
			final float bias) {
		int rgb, a, r, b, g;
		int[] rTable, gTable, bTable;
		rTable = gTable = bTable = makeGainBiasTable(gain, bias);

		int x, y;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				rgb = bitmap.getPixel(x, y);
				a = rgb & 0xff000000;
				r = (rgb >> 16) & 0xff;
				g = (rgb >> 8) & 0xff;
				b = rgb & 0xff;
				r = rTable[r];
				g = gTable[g];
				b = bTable[b];
				bitmap.setPixel(x, y, (a | (r << 16) | (g << 8) | b));

			}
		}
	}

	/**
	 * Correct Gamma for individual channels
	 * <p>
	 * For best results all three channels should have the same gamma value
	 * </p>
	 * 
	 * @param bitmap
	 * @param rGamma
	 *            min:0 max:1. recommended: 1
	 * @param gGamma
	 *            min:0 max:1. recommended: 1
	 * @param bGamma
	 *            min:0 max:1. recommended: 1
	 */
	public static void adjustGamma(final ToToBitmap bitmap, final float rGamma,
			final float gGamma, final float bGamma) {
		int[] rTable, gTable, bTable;
		rTable = makeGammaTable(rGamma);
		if (gGamma == rGamma)
			gTable = rTable;
		else
			gTable = makeGammaTable(gGamma);

		if (bGamma == rGamma)
			bTable = rTable;
		else if (bGamma == gGamma)
			bTable = gTable;
		else
			bTable = makeGammaTable(bGamma);

		int rgb, a, r, b, g;

		int x, y;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				rgb = bitmap.getPixel(x, y);
				a = rgb & 0xff000000;
				r = (rgb >> 16) & 0xff;
				g = (rgb >> 8) & 0xff;
				b = rgb & 0xff;
				r = rTable[r];
				g = gTable[g];
				b = bTable[b];
				bitmap.setPixel(x, y, (a | (r << 16) | (g << 8) | b));

			}
		}
	}

	private static int[] makeGammaTable(final float gamma) {
		final int[] table = new int[256];
		for (int i = 0; i < 256; i++) {
			int v = (int) ((255.0 * Math.pow(i / 255.0, 1.0 / gamma)) + 0.5);
			if (v > 255)
				v = 255;
			table[i] = v;
		}
		return table;
	}

	/**
	 * Set the exposure of the bitmap
	 * 
	 * @param bitmap
	 * @param exposure
	 *            min:0 max:5
	 * @param outputConfig
	 */
	public static void setExposure(final ToToBitmap bitmap, final float exposure) {

		int[] rTable, gTable, bTable;
		rTable = gTable = bTable = makeExposureTable(exposure);
		int rgb;
		int x, y;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				rgb = bitmap.getPixel(x, y);
				final int a = rgb & 0xff000000;
				int r = (rgb >> 16) & 0xff;
				int g = (rgb >> 8) & 0xff;
				int b = rgb & 0xff;
				r = rTable[r];
				g = gTable[g];
				b = bTable[b];
				bitmap.setPixel(x, y, (a | (r << 16) | (g << 8) | b));
			}

		}

	}

	private static int[] makeExposureTable(final float exposure) {
		final int[] table = new int[256];
		for (int i = 0; i < 256; i++)
			table[i] = toto.graphics.color.RGBUtils
					.clamp((int) (255 * exposureTransferFunction(i / 255.0f,
							exposure)));
		return table;
	}

	private static float exposureTransferFunction(final float f,
			final float exposure) {
		return 1 - (float) Math.exp(-f * exposure);
	}

	/**
	 * A filter which performs reduces noise by looking at each pixel's 8
	 * neighbours, and if it's a minimum or maximum, replacing it by the next
	 * minimum or maximum of the neighbours.
	 * <p>
	 * Filter bounds dont apply to this filter
	 * </p>
	 * 
	 * @param src
	 * @return
	 */
	public static void reduceNoise(final ToToBitmap src) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		int index = 0;
		final int[] r = new int[9];
		final int[] g = new int[9];
		final int[] b = new int[9];
		final int[] inPixels = src.getPixels();
		final int[] outPixels = new int[inPixels.length];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int k = 0;
				final int irgb = inPixels[index];
				final int ir = (irgb >> 16) & 0xff;
				final int ig = (irgb >> 8) & 0xff;
				final int ib = irgb & 0xff;
				for (int dy = -1; dy <= 1; dy++) {
					final int iy = y + dy;
					if (0 <= iy && iy < height) {
						final int ioffset = iy * width;
						for (int dx = -1; dx <= 1; dx++) {
							final int ix = x + dx;
							if (0 <= ix && ix < width) {
								final int rgb = inPixels[ioffset + ix];
								r[k] = (rgb >> 16) & 0xff;
								g[k] = (rgb >> 8) & 0xff;
								b[k] = rgb & 0xff;
							} else {
								r[k] = ir;
								g[k] = ig;
								b[k] = ib;
							}
							k++;
						}
					} else {
						for (int dx = -1; dx <= 1; dx++) {
							r[k] = ir;
							g[k] = ig;
							b[k] = ib;
							k++;
						}
					}
				}
				outPixels[index] = (inPixels[index] & 0xff000000)
						| (smooth(r) << 16) | (smooth(g) << 8) | smooth(b);
				index++;
			}
		}
		src.setPixels(outPixels, src.getWidth(), src.getHeight());
	}

	private static int smooth(final int[] v) {
		int minindex = 0, maxindex = 0, min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;

		for (int i = 0; i < 9; i++) {
			if (i != 4) {
				if (v[i] < min) {
					min = v[i];
					minindex = i;
				}
				if (v[i] > max) {
					max = v[i];
					maxindex = i;
				}
			}
		}
		if (v[4] < min)
			return v[minindex];
		if (v[4] > max)
			return v[maxindex];
		return v[4];
	}

	/**
	 * A filter which removes noise from an image using a "pepper and salt"
	 * algorithm.
	 * 
	 * @param src
	 */
	public static void deSpeckle(final ToToBitmap src) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = src.getPixels();
		final int[] outPixels = new int[inPixels.length];

		int index = 0;
		final short[][] r = new short[3][width];
		final short[][] g = new short[3][width];
		final short[][] b = new short[3][width];

		for (int x = 0; x < width; x++) {
			final int rgb = inPixels[x];
			r[1][x] = (short) ((rgb >> 16) & 0xff);
			g[1][x] = (short) ((rgb >> 8) & 0xff);
			b[1][x] = (short) (rgb & 0xff);
		}
		for (int y = 0; y < height; y++) {
			final boolean yIn = y > 0 && y < height - 1;
			int nextRowIndex = index + width;
			if (y < height - 1) {
				for (int x = 0; x < width; x++) {
					final int rgb = inPixels[nextRowIndex++];
					r[2][x] = (short) ((rgb >> 16) & 0xff);
					g[2][x] = (short) ((rgb >> 8) & 0xff);
					b[2][x] = (short) (rgb & 0xff);
				}
			}
			for (int x = 0; x < width; x++) {
				final boolean xIn = x > 0 && x < width - 1;
				short or = r[1][x];
				short og = g[1][x];
				short ob = b[1][x];
				final int w = x - 1;
				final int e = x + 1;

				if (yIn) {
					or = pepperAndSalt(or, r[0][x], r[2][x]);
					og = pepperAndSalt(og, g[0][x], g[2][x]);
					ob = pepperAndSalt(ob, b[0][x], b[2][x]);
				}

				if (xIn) {
					or = pepperAndSalt(or, r[1][w], r[1][e]);
					og = pepperAndSalt(og, g[1][w], g[1][e]);
					ob = pepperAndSalt(ob, b[1][w], b[1][e]);
				}

				if (yIn && xIn) {
					or = pepperAndSalt(or, r[0][w], r[2][e]);
					og = pepperAndSalt(og, g[0][w], g[2][e]);
					ob = pepperAndSalt(ob, b[0][w], b[2][e]);

					or = pepperAndSalt(or, r[2][w], r[0][e]);
					og = pepperAndSalt(og, g[2][w], g[0][e]);
					ob = pepperAndSalt(ob, b[2][w], b[0][e]);
				}

				outPixels[index] = (inPixels[index] & 0xff000000) | (or << 16)
						| (og << 8) | ob;
				index++;
			}
			short[] t;
			t = r[0];
			r[0] = r[1];
			r[1] = r[2];
			r[2] = t;
			t = g[0];
			g[0] = g[1];
			g[1] = g[2];
			g[2] = t;
			t = b[0];
			b[0] = b[1];
			b[1] = b[2];
			b[2] = t;
		}

		src.setPixels(outPixels, src.getWidth(), src.getHeight());
	}

	private static short pepperAndSalt(short c, final short v1, final short v2) {
		if (c < v1)
			c++;
		if (c < v2)
			c++;
		if (c > v1)
			c--;
		if (c > v2)
			c--;
		return c;
	}

	/**
	 * Set the brightness and contrast of the given bitmap
	 * 
	 * @param bitmap
	 * @param brightness
	 *            min:0 max:2.
	 * @param contrast
	 *            min:0 max:2.
	 */
	public static void setBrightnessAndContrast(final ToToBitmap bitmap,
			final float brightness, final float contrast) {
		int[] rTable, gTable, bTable;
		rTable = gTable = bTable = makeBrightnessContrastTable(brightness,
				contrast);
		int a, r, g, b;
		int y, x;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		int pixel;
		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {

				pixel = bitmap.getPixel(x, y);

				a = pixel & 0xff000000;
				r = (pixel >> 16) & 0xff;
				g = (pixel >> 8) & 0xff;
				b = pixel & 0xff;
				r = rTable[r];
				g = gTable[g];
				b = bTable[b];
				bitmap.setPixel(x, y, (a | (r << 16) | (g << 8) | b));
			}
		}
	}

	public static void setBrightnessAndContrastNative(final ToToBitmap bitmap,
			final float brightness, final float contrast) {
		final int[] pixels = bitmap.getPixels();
		setBrightnessAndContrast(pixels, bitmap.getWidth(),
				bitmap.getFilterX(), bitmap.getFilterY(),
				bitmap.getFilterWidth(), bitmap.getFilterHeight(), brightness,
				contrast);
	}

	public static native void setBrightnessAndContrast(final int[] bitmap,
			final int bitmapWidth, int x, int y, int width, int height,
			final float brightness, final float contrast);

	private static int[] makeBrightnessContrastTable(final float brightness,
			final float contrast) {
		final int[] table = new int[256];
		for (int i = 0; i < 256; i++)
			table[i] = RGBUtils
					.clamp((int) (255 * brightnessContrastTransferFunction(
							i / 255.0f, brightness, contrast)));
		return table;
	}

	private static float brightnessContrastTransferFunction(float f,
			final float brightness, final float contrast) {
		f = f * brightness;
		f = (f - 0.5f) * contrast + 0.5f;
		return f;
	}

	/**
	 * Adjust the HSB components
	 * 
	 * @param bitmap
	 * @param hFactor
	 *            scale from 0...1
	 * @param sFactor
	 *            scale from 0...1
	 * @param bFactor
	 *            scale from 0...1
	 */
	public static void adjustHSB(final ToToBitmap bitmap, final float hFactor,
			final float sFactor, final float bFactor) {
		int rgb;
		final float[] hsb = new float[3];

		int y, x;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {

				rgb = bitmap.getPixel(x, y);

				final int a = rgb & 0xff000000;
				final int r = (rgb >> 16) & 0xff;
				final int g = (rgb >> 8) & 0xff;
				final int b = rgb & 0xff;
				android.graphics.Color.RGBToHSV(r, g, b, hsb);// RGBtoHSB(r, g,
																// b, hsb);
				hsb[0] += hFactor;
				while (hsb[0] < 0)
					hsb[0] += Math.PI * 2;
				hsb[1] += sFactor;
				if (hsb[1] < 0)
					hsb[1] = 0;
				else if (hsb[1] > 1.0)
					hsb[1] = 1.0f;
				hsb[2] += bFactor;
				if (hsb[2] < 0)
					hsb[2] = 0;
				else if (hsb[2] > 1.0)
					hsb[2] = 1.0f;
				rgb = android.graphics.Color.HSVToColor(hsb);// HSBtoRGB(hsb[0],
																// hsb[1],
																// hsb[2]);
				bitmap.setPixel(x, y, (a | (rgb & 0xffffff)));
			}
		}
	}

	/**
	 * Adjust the brightness of a color.
	 * 
	 * @param bitmap
	 * @param brightness
	 *            to increase brightness. Negative values decrease brightness
	 */
	public static void adjustBrightnessNative(final ToToBitmap bitmap,
			final double brightness) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.brightFilter(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), brightness);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Adjust the contrast of the bitmap a color.
	 * 
	 * @param bitmap
	 * @param contrast
	 *            to increase brightness. Negative values decrease brightness
	 */
	public static void adjustContrastNative(final ToToBitmap bitmap,
			final double contrast) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.contrastFilter(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), contrast);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Adjust RGB components.
	 * 
	 * @param bitmap
	 * @param rFactor
	 * @param gFactor
	 * @param bFactor
	 */
	public static void adjustRGB(final ToToBitmap bitmap, float rFactor,
			float gFactor, float bFactor) {
		rFactor = 1 + rFactor;
		gFactor = 1 + gFactor;
		bFactor = 1 + bFactor;
		int rgb;
		int a, r, g, b;
		int y, x;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				rgb = bitmap.getPixel(x, y);
				a = rgb & 0xff000000;
				r = (rgb >> 16) & 0xff;
				g = (rgb >> 8) & 0xff;
				b = rgb & 0xff;
				r = RGBUtils.clamp((int) (r * rFactor));
				g = RGBUtils.clamp((int) (g * gFactor));
				b = RGBUtils.clamp((int) (b * bFactor));
				bitmap.setPixel(x, y, (a | (r << 16) | (g << 8) | b));
			}
		}
	}

	static {
		ToTo.loadLib("Bitmap_Filters");
	}
}
