/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;


import toto.bitmap.ToToBitmap;
import toto.graphics.color.RGBUtils;

class ConvolveUtils {
	static void doConvolve(final float[] matrix, final ToToBitmap src,
			final int edgeAction, final boolean processAlpha,
			final boolean premultiplyAlpha) {
		final Kernel kernel = new Kernel(3, 3, matrix);
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = src.getPixels();
		final int[] outPixels = new int[inPixels.length];
		if (premultiplyAlpha)
			ImageMath.premultiply(inPixels, 0, inPixels.length);
		convolve(kernel, inPixels, outPixels, width, height, processAlpha,
				edgeAction);
		if (premultiplyAlpha)
			ImageMath.unpremultiply(outPixels, 0, outPixels.length);

		src.setPixels(outPixels, src.getWidth(), src.getHeight());
	}

	/**
	 * Convolve a block of pixels.
	 * 
	 * @param kernel
	 *            the kernel
	 * @param inPixels
	 *            the input pixels
	 * @param outPixels
	 *            the output pixels
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @param alpha
	 *            include alpha channel
	 * @param edgeAction
	 *            what to do at the edges
	 */
	static void convolve(final Kernel kernel, final int[] inPixels,
			final int[] outPixels, final int width, final int height,
			final boolean alpha, final int edgeAction) {
		if (kernel.getHeight() == 1)
			convolveH(kernel, inPixels, outPixels, width, height, alpha,
					edgeAction);
		else if (kernel.getWidth() == 1)
			convolveV(kernel, inPixels, outPixels, width, height, alpha,
					edgeAction);
		else
			convolveHV(kernel, inPixels, outPixels, width, height, alpha,
					edgeAction);
	}

	/**
	 * Convolve with a 2D kernel.
	 * 
	 * @param kernel
	 *            the kernel
	 * @param inPixels
	 *            the input pixels
	 * @param outPixels
	 *            the output pixels
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @param alpha
	 *            include alpha channel
	 * @param edgeAction
	 *            what to do at the edges
	 */
	static void convolveHV(final Kernel kernel, final int[] inPixels,
			final int[] outPixels, final int width, final int height,
			final boolean alpha, final int edgeAction) {
		int index = 0;
		final float[] matrix = kernel.getKernelData(null);
		final int rows = kernel.getHeight();
		final int cols = kernel.getWidth();
		final int rows2 = rows / 2;
		final int cols2 = cols / 2;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				float r = 0, g = 0, b = 0, a = 0;

				for (int row = -rows2; row <= rows2; row++) {
					final int iy = y + row;
					int ioffset;
					if (0 <= iy && iy < height)
						ioffset = iy * width;
					else if (edgeAction == GenericFilters.CLAMP_EDGES)
						ioffset = y * width;
					else if (edgeAction == GenericFilters.WRAP_EDGES)
						ioffset = ((iy + height) % height) * width;
					else
						continue;
					final int moffset = cols * (row + rows2) + cols2;
					for (int col = -cols2; col <= cols2; col++) {
						final float f = matrix[moffset + col];

						if (f != 0) {
							int ix = x + col;
							if (!(0 <= ix && ix < width)) {
								if (edgeAction == GenericFilters.CLAMP_EDGES)
									ix = x;
								else if (edgeAction == GenericFilters.WRAP_EDGES)
									ix = (x + width) % width;
								else
									continue;
							}
							final int rgb = inPixels[ioffset + ix];
							a += f * ((rgb >> 24) & 0xff);
							r += f * ((rgb >> 16) & 0xff);
							g += f * ((rgb >> 8) & 0xff);
							b += f * (rgb & 0xff);
						}
					}
				}
				final int ia = alpha ? RGBUtils.clamp((int) (a + 0.5)) : 0xff;
				final int ir = RGBUtils.clamp((int) (r + 0.5));
				final int ig = RGBUtils.clamp((int) (g + 0.5));
				final int ib = RGBUtils.clamp((int) (b + 0.5));
				outPixels[index++] = (ia << 24) | (ir << 16) | (ig << 8) | ib;
			}
		}
	}

	/**
	 * Convolve with a kernel consisting of one row.
	 * 
	 * @param kernel
	 *            the kernel
	 * @param inPixels
	 *            the input pixels
	 * @param outPixels
	 *            the output pixels
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @param alpha
	 *            include alpha channel
	 * @param edgeAction
	 *            what to do at the edges
	 */
	static void convolveH(final Kernel kernel, final int[] inPixels,
			final int[] outPixels, final int width, final int height,
			final boolean alpha, final int edgeAction) {
		int index = 0;
		final float[] matrix = kernel.getKernelData(null);
		final int cols = kernel.getWidth();
		final int cols2 = cols / 2;

		for (int y = 0; y < height; y++) {
			final int ioffset = y * width;
			for (int x = 0; x < width; x++) {
				float r = 0, g = 0, b = 0, a = 0;
				final int moffset = cols2;
				for (int col = -cols2; col <= cols2; col++) {
					final float f = matrix[moffset + col];

					if (f != 0) {
						int ix = x + col;
						if (ix < 0) {
							if (edgeAction == GenericFilters.CLAMP_EDGES)
								ix = 0;
							else if (edgeAction == GenericFilters.WRAP_EDGES)
								ix = (x + width) % width;
						} else if (ix >= width) {
							if (edgeAction == GenericFilters.CLAMP_EDGES)
								ix = width - 1;
							else if (edgeAction == GenericFilters.WRAP_EDGES)
								ix = (x + width) % width;
						}
						final int rgb = inPixels[ioffset + ix];
						a += f * ((rgb >> 24) & 0xff);
						r += f * ((rgb >> 16) & 0xff);
						g += f * ((rgb >> 8) & 0xff);
						b += f * (rgb & 0xff);
					}
				}
				final int ia = alpha ? RGBUtils.clamp((int) (a + 0.5)) : 0xff;
				final int ir = RGBUtils.clamp((int) (r + 0.5));
				final int ig = RGBUtils.clamp((int) (g + 0.5));
				final int ib = RGBUtils.clamp((int) (b + 0.5));
				outPixels[index++] = (ia << 24) | (ir << 16) | (ig << 8) | ib;
			}
		}
	}

	/**
	 * Convolve with a kernel consisting of one column.
	 * 
	 * @param kernel
	 *            the kernel
	 * @param inPixels
	 *            the input pixels
	 * @param outPixels
	 *            the output pixels
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @param alpha
	 *            include alpha channel
	 * @param edgeAction
	 *            what to do at the edges
	 */
	static void convolveV(final Kernel kernel, final int[] inPixels,
			final int[] outPixels, final int width, final int height,
			final boolean alpha, final int edgeAction) {
		int index = 0;
		final float[] matrix = kernel.getKernelData(null);
		final int rows = kernel.getHeight();
		final int rows2 = rows / 2;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				float r = 0, g = 0, b = 0, a = 0;

				for (int row = -rows2; row <= rows2; row++) {
					final int iy = y + row;
					int ioffset;
					if (iy < 0) {
						if (edgeAction == GenericFilters.CLAMP_EDGES)
							ioffset = 0;
						else if (edgeAction == GenericFilters.WRAP_EDGES)
							ioffset = ((y + height) % height) * width;
						else
							ioffset = iy * width;
					} else if (iy >= height) {
						if (edgeAction == GenericFilters.CLAMP_EDGES)
							ioffset = (height - 1) * width;
						else if (edgeAction == GenericFilters.WRAP_EDGES)
							ioffset = ((y + height) % height) * width;
						else
							ioffset = iy * width;
					} else
						ioffset = iy * width;

					final float f = matrix[row + rows2];

					if (f != 0) {
						final int rgb = inPixels[ioffset + x];
						a += f * ((rgb >> 24) & 0xff);
						r += f * ((rgb >> 16) & 0xff);
						g += f * ((rgb >> 8) & 0xff);
						b += f * (rgb & 0xff);
					}
				}
				final int ia = alpha ? RGBUtils.clamp((int) (a + 0.5)) : 0xff;
				final int ir = RGBUtils.clamp((int) (r + 0.5));
				final int ig = RGBUtils.clamp((int) (g + 0.5));
				final int ib = RGBUtils.clamp((int) (b + 0.5));
				outPixels[index++] = (ia << 24) | (ir << 16) | (ig << 8) | ib;
			}
		}
	}
}
