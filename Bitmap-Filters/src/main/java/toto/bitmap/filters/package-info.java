/**
 * Over 200 Bitmap filters and Convolutions to help with any Bitmap manipulation task
 * @author Elton Kent
 *
 */
package toto.bitmap.filters;