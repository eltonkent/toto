package toto.bitmap.filters;


public class SizeTransformFilters {

	/**
	 * Scale using bilinear interpolation. Implemented in native code.
	 * 
	 * @param bitmap
	 * @param newWidth
	 * @param newHeight
	 */
	// public static void scaleBINative(final RBitmap bitmap, final int
	// newWidth,
	// final int newHeight) {
	// int[] pixels = bitmap.getPixels();
	// pixels = NativeFilters.scaleBI(pixels,
	// bitmap.getFilterX(),bitmap.getFilterY(), bitmap.getFilterWidth(),
	// bitmap.getFilterHeight(), newWidth, newHeight);
	// bitmap.setPixels(pixels, newWidth, newHeight);
	// }
}
