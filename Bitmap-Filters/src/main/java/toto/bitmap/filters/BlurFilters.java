/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;

import toto.bitmap.filters.nativ.NativeFilters;
import toto.geom2d.AffineTransform;
import toto.geom2d.Point;
import toto.bitmap.utils.BitmapUtils;
import toto.bitmap.ToToBitmap;
import toto.graphics.color.RGBUtils;
import toto.math.MathUtils;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

/**
 * Collection of various blur filters
 * <p>
 * <b>Native Library:</b>libBitmap_Filters <b>Gaussian Blur</b><br/>
 * Gaussian using the <code>radius</code> of <code>2</code><br/>
 * <img src="../../../../resources/gaussian.png"><br/>
 * 
 * <b>Motion Blur</b><br/>
 * Motion blur with <code>angle</code> 1.0f and <code>distance</code> 5.0f.<br/>
 * <img src="../../../../resources/motionBlur.png"><br/>
 * <b>Box Blur</b><br/>
 * Box blur with <code>hRadius</code> and <code>vRadius</code> of 5.0f. with 1
 * <code>iteration</code><br/>
 * <img src="../../../../resources/boxblur.png"><br/>
 * <b>Maximum</b><br/>
 * <img src="../../../../resources/maximum.png"><br/>
 * <b>Minimum</b><br/>
 * <img src="../../../../resources/minimum.png"><br/>
 * <b>Median</b><br/>
 * <img src="../../../../resources/median.png"><br/>
 * </p>
 * 
 * @author elton.stephen.kent
 * 
 */
public class BlurFilters {

	/**
	 * Apply Gaussian blur Filter to the given image data
	 * <p>
	 * <table border="0">
	 * <tr>
	 * <td><b>Before</b></td>
	 * <td><b>After</b></td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <img src="../../../resources/before.png"></td>
	 * <td><img src="../../../resources/gaussian.png"></td>
	 * </tr>
	 * </table>
	 * </p>
	 * 
	 * @param bitmap
	 * @param brightness
	 *            of the result. Optimum values are within 200
	 */
	public static final void fastGaussianBlur(final ToToBitmap bitmap,
			final int brightness) {
		final byte[][] filter = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
		GenericFilters.applyFilter(bitmap, brightness, filter);
	}

	/**
	 * Apply gaussian filter
	 * 
	 * @param src
	 * @param radius
	 *            filter radius. min:0 max:100
	 * @param convolveAlpha
	 * @param premultiplyAlpha
	 * @param outputConfig
	 */
	public static void gaussianBlur(final ToToBitmap src, final int radius,
			final boolean convolveAlpha, final boolean premultiplyAlpha) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = src.getPixels();
		final int[] outPixels = new int[inPixels.length];
		final Kernel kernel = GaussianUtils.makeKernel(radius);
		if (radius > 0) {
			GaussianUtils.convolveAndTranspose(kernel, inPixels, outPixels,
					width, height, convolveAlpha, convolveAlpha
							&& premultiplyAlpha, false,
					GenericFilters.CLAMP_EDGES);
			GaussianUtils.convolveAndTranspose(kernel, outPixels, inPixels,
					height, width, convolveAlpha, false, convolveAlpha
							&& premultiplyAlpha, GenericFilters.CLAMP_EDGES);
		}
		src.setPixels(inPixels, src.getWidth(), src.getHeight());
	}

	/**
	 * Produces motion blur the slow, but higher-quality way.
	 * 
	 * @param src
	 * @param angle
	 *            the angle of blur. min:0 max:360
	 * @param distance
	 *            the distance of blur. min:0 max:200
	 * @param rotation
	 *            the blur rotation. min:-180 max:180
	 * @param zoom
	 *            the blur zoom. min:0 max:100
	 * @param premultiplyAlpha
	 *            whether to premultiply the alpha channel.
	 * @param wrapEdges
	 *            Set whether to wrap at the image edges
	 * @param outputConfig
	 */
	public static Bitmap motionBlur(final Bitmap src, final float angle,
			final float distance, final float rotation, final float zoom,
			final boolean premultiplyAlpha, final boolean wrapEdges,
			final Bitmap.Config outputConfig) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = BitmapUtils.getPixels(src);
		final int[] outPixels = new int[inPixels.length];

		final int cx = width / 2;
		final int cy = height / 2;
		int index = 0;

		final float imageRadius = (float) Math.sqrt(cx * cx + cy * cy);
		final float translateX = (float) (distance * Math.cos(angle));
		final float translateY = (float) (distance * -Math.sin(angle));
		final float maxDistance = distance + Math.abs(rotation * imageRadius)
				+ zoom * imageRadius;
		final int repetitions = (int) maxDistance;
		final AffineTransform t = new AffineTransform();
		final Point p = new Point();

		if (premultiplyAlpha)
			ImageMath.premultiply(inPixels, 0, inPixels.length);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int a = 0, r = 0, g = 0, b = 0;
				int count = 0;
				for (int i = 0; i < repetitions; i++) {
					int newX = x, newY = y;
					final float f = (float) i / repetitions;

					p.setLocation(x, y);
					t.setToIdentity();
					t.translate(cx + f * translateX, cy + f * translateY);
					final float s = 1 - zoom * f;
					t.scale(s, s);
					if (rotation != 0)
						t.rotate(-rotation * f);
					t.translate(-cx, -cy);
					t.transform(p, p);
					newX = (int) p.getX();
					newY = (int) p.getY();

					if (newX < 0 || newX >= width) {
						if (wrapEdges)
							newX = ImageMath.mod(newX, width);
						else
							break;
					}
					if (newY < 0 || newY >= height) {
						if (wrapEdges)
							newY = ImageMath.mod(newY, height);
						else
							break;
					}

					count++;
					final int rgb = inPixels[newY * width + newX];
					a += (rgb >> 24) & 0xff;
					r += (rgb >> 16) & 0xff;
					g += (rgb >> 8) & 0xff;
					b += rgb & 0xff;
				}
				if (count == 0) {
					outPixels[index] = inPixels[index];
				} else {
					a = RGBUtils.clamp((a / count));
					r = RGBUtils.clamp((r / count));
					g = RGBUtils.clamp((g / count));
					b = RGBUtils.clamp((b / count));
					outPixels[index] = (a << 24) | (r << 16) | (g << 8) | b;
				}
				index++;
			}
		}
		if (premultiplyAlpha)
			ImageMath.unpremultiply(outPixels, 0, inPixels.length);

		return Bitmap.createBitmap(outPixels, src.getWidth(), src.getHeight(),
				outputConfig);
	}

	/**
	 * Apply image blur filter on the given image data
	 * 
	 * @param bitmap
	 *            image
	 * 
	 * @param outputConfig
	 *            of the image
	 */
	public static void simpleBlur(final ToToBitmap bitmap) {
		final byte[][] filter = { { -1, -1, -1 }, { -1, 0, -1 }, { -1, -1, -1 } };
		GenericFilters.applyFilter(bitmap, 100, filter);
	}

	/**
	 * performs a box blur on an image.
	 * <p>
	 * The horizontal and vertical blurs can be specified separately and a
	 * number of iterations can be given which allows an approximation to
	 * Gaussian blur.
	 * </p>
	 * 
	 * @param bitmap
	 * @param hRadius
	 *            min:0 max:100
	 * @param vRadius
	 *            min:0 max:100
	 * @param iterations
	 *            the number of iterations the blur is performed. min:0 max:10.
	 *            Recommended:1.
	 * @param premultiplyAlpha
	 *            whether to premultiply the alpha channel. Recommended:true
	 */
	public static void boxBlur(final ToToBitmap bitmap, final float hRadius,
			final float vRadius, final int iterations,
			final boolean premultiplyAlpha) {
		final int width = bitmap.getWidth();
		final int height = bitmap.getHeight();

		final int[] inPixels = bitmap.getPixels();
		final int[] outPixels = new int[width * height];

		if (premultiplyAlpha)
			ImageMath.premultiply(inPixels, 0, inPixels.length);
		for (int i = 0; i < iterations; i++) {
			blur(inPixels, outPixels, width, height, hRadius);
			blur(outPixels, inPixels, height, width, vRadius);
		}
		fractionalBLur(inPixels, outPixels, width, height, hRadius);
		fractionalBLur(outPixels, inPixels, height, width, vRadius);
		if (premultiplyAlpha)
			ImageMath.unpremultiply(inPixels, 0, inPixels.length);
		bitmap.setPixels(inPixels, width, height);
	}

	/**
	 * Blur and transpose a block of ARGB pixels.
	 * 
	 * @param in
	 *            the input pixels
	 * @param out
	 *            the output pixels
	 * @param width
	 *            the width of the pixel array
	 * @param height
	 *            the height of the pixel array
	 * @param radius
	 *            the radius of blur
	 */
	public static void blur(final int[] in, final int[] out, final int width,
			final int height, final float radius) {
		final int widthMinus1 = width - 1;
		final int r = (int) radius;
		final int tableSize = 2 * r + 1;
		final int divide[] = new int[256 * tableSize];

		for (int i = 0; i < 256 * tableSize; i++)
			divide[i] = i / tableSize;

		int inIndex = 0;

		for (int y = 0; y < height; y++) {
			int outIndex = y;
			int ta = 0, tr = 0, tg = 0, tb = 0;

			for (int i = -r; i <= r; i++) {
				final int rgb = in[(int) (inIndex + MathUtils.clamp(i, 0f,
						width - 1))];
				ta += (rgb >> 24) & 0xff;
				tr += (rgb >> 16) & 0xff;
				tg += (rgb >> 8) & 0xff;
				tb += rgb & 0xff;
			}

			for (int x = 0; x < width; x++) {
				out[outIndex] = (divide[ta] << 24) | (divide[tr] << 16)
						| (divide[tg] << 8) | divide[tb];

				int i1 = x + r + 1;
				if (i1 > widthMinus1)
					i1 = widthMinus1;
				int i2 = x - r;
				if (i2 < 0)
					i2 = 0;
				final int rgb1 = in[inIndex + i1];
				final int rgb2 = in[inIndex + i2];

				ta += ((rgb1 >> 24) & 0xff) - ((rgb2 >> 24) & 0xff);
				tr += ((rgb1 & 0xff0000) - (rgb2 & 0xff0000)) >> 16;
				tg += ((rgb1 & 0xff00) - (rgb2 & 0xff00)) >> 8;
				tb += (rgb1 & 0xff) - (rgb2 & 0xff);
				outIndex += height;
			}
			inIndex += width;
		}
	}

	/**
	 * Blur implementation similar to motion blur.
	 * 
	 * @param in
	 * @param out
	 * @param width
	 * @param height
	 * @param radius
	 */
	public static void fractionalBLur(final int[] in, final int[] out,
			final int width, final int height, float radius) {
		radius -= (int) radius;
		final float f = 1.0f / (1 + 2 * radius);
		int inIndex = 0;

		for (int y = 0; y < height; y++) {
			int outIndex = y;

			out[outIndex] = in[0];
			outIndex += height;
			for (int x = 1; x < width - 1; x++) {
				final int i = inIndex + x;
				final int rgb1 = in[i - 1];
				final int rgb2 = in[i];
				final int rgb3 = in[i + 1];

				int a1 = (rgb1 >> 24) & 0xff;
				int r1 = (rgb1 >> 16) & 0xff;
				int g1 = (rgb1 >> 8) & 0xff;
				int b1 = rgb1 & 0xff;
				final int a2 = (rgb2 >> 24) & 0xff;
				final int r2 = (rgb2 >> 16) & 0xff;
				final int g2 = (rgb2 >> 8) & 0xff;
				final int b2 = rgb2 & 0xff;
				final int a3 = (rgb3 >> 24) & 0xff;
				final int r3 = (rgb3 >> 16) & 0xff;
				final int g3 = (rgb3 >> 8) & 0xff;
				final int b3 = rgb3 & 0xff;
				a1 = a2 + (int) ((a1 + a3) * radius);
				r1 = r2 + (int) ((r1 + r3) * radius);
				g1 = g2 + (int) ((g1 + g3) * radius);
				b1 = b2 + (int) ((b1 + b3) * radius);
				a1 *= f;
				r1 *= f;
				g1 *= f;
				b1 *= f;
				out[outIndex] = (a1 << 24) | (r1 << 16) | (g1 << 8) | b1;
				outIndex += height;
			}
			out[outIndex] = in[width - 1];
			inIndex += width;
		}
	}

	/**
	 * A filter which replcaes each pixel by the maximum of itself and its eight
	 * neightbours.
	 * 
	 * @param src
	 */
	public static void maximum(final ToToBitmap src) {
		int[] inPixels = src.getPixels();
		final int[] outPixels = new int[inPixels.length];
		final int width = src.getWidth();
		final int height = src.getHeight();
		int index = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int pixel = 0xff000000;
				for (int dy = -1; dy <= 1; dy++) {
					final int iy = y + dy;
					int ioffset;
					if (0 <= iy && iy < height) {
						ioffset = iy * width;
						for (int dx = -1; dx <= 1; dx++) {
							final int ix = x + dx;
							if (0 <= ix && ix < width) {
								pixel = RGBUtils.combinePixels(pixel,
										inPixels[ioffset + ix],
										RGBUtils.Operation.MAX);
							}
						}
					}
				}
				outPixels[index++] = pixel;
			}
		}
		inPixels = null;
		src.setPixels(outPixels, width, height);
	}

	/**
	 * A filter which replcaes each pixel by the mimimum of itself and its eight
	 * neightbours.
	 * 
	 * @param src
	 */
	public static void minimum(final ToToBitmap src) {
		int[] inPixels = src.getPixels();
		final int[] outPixels = new int[inPixels.length];
		final int width = src.getWidth();
		final int height = src.getHeight();
		int index = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int pixel = 0xffffffff;
				for (int dy = -1; dy <= 1; dy++) {
					final int iy = y + dy;
					int ioffset;
					if (0 <= iy && iy < height) {
						ioffset = iy * width;
						for (int dx = -1; dx <= 1; dx++) {
							final int ix = x + dx;
							if (0 <= ix && ix < width) {
								pixel = RGBUtils.combinePixels(pixel,
										inPixels[ioffset + ix],
										RGBUtils.Operation.MIN);
							}
						}
					}
				}
				outPixels[index++] = pixel;
			}
		}
		inPixels = null;
		src.setPixels(outPixels, width, height);
	}

	/**
	 * A filter which performs a 3x3 median operation. Useful for removing dust
	 * and noise.
	 * 
	 * @param src
	 */
	public static void median(final ToToBitmap src) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = src.getPixels();
		int index = 0;
		final int[] argb = new int[9];
		final int[] r = new int[9];
		final int[] g = new int[9];
		final int[] b = new int[9];
		final int[] outPixels = new int[width * height];

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int k = 0;
				for (int dy = -1; dy <= 1; dy++) {
					final int iy = y + dy;
					if (0 <= iy && iy < height) {
						final int ioffset = iy * width;
						for (int dx = -1; dx <= 1; dx++) {
							final int ix = x + dx;
							if (0 <= ix && ix < width) {
								final int rgb = inPixels[ioffset + ix];
								argb[k] = rgb;
								r[k] = (rgb >> 16) & 0xff;
								g[k] = (rgb >> 8) & 0xff;
								b[k] = rgb & 0xff;
								k++;
							}
						}
					}
				}
				while (k < 9) {
					argb[k] = 0xff000000;
					r[k] = g[k] = b[k] = 0;
					k++;
				}
				outPixels[index++] = argb[rgbMedian(r, g, b)];
			}
		}
		src.setPixels(outPixels, width, height);
	}

	private static int rgbMedian(final int[] r, final int[] g, final int[] b) {
		int sum, index = 0, min = Integer.MAX_VALUE;

		for (int i = 0; i < 9; i++) {
			sum = 0;
			for (int j = 0; j < 9; j++) {
				sum += Math.abs(r[i] - r[j]);
				sum += Math.abs(g[i] - g[j]);
				sum += Math.abs(b[i] - b[j]);
			}
			if (sum < min) {
				min = sum;
				index = i;
			}
		}
		return index;
	}

	/**
	 * Perform blurring using renderscript.
	 * <p>
	 * Uses honeycomb as the target API.
	 * </p>
	 * 
	 * @param source
	 * @param radius
	 * @return
	 */
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	public static Bitmap stackBlurRS(final Context context,
			final Bitmap source, final int radius) {
		final Bitmap bitmap = source.copy(source.getConfig(), true);
		final RenderScript rs = RenderScript.create(context);
		final Allocation input = Allocation.createFromBitmap(rs, source,
				Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
		final Allocation output = Allocation.createTyped(rs, input.getType());
		final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs,
				Element.U8_4(rs));
		script.setRadius(radius);
		script.setInput(input);
		script.forEach(output);
		output.copyTo(bitmap);
		return bitmap;
	}

	/**
	 * Blur algorithm that is between gaussian blur and box blur.
	 * <p>
	 * Its 7 times faster than gaussian blur.
	 * </p>
	 * 
	 * @param original
	 * @param radius
	 */
	public static void stackBlur(final ToToBitmap original, int radius) {
		if (radius < 1)
			radius = 1;

		// final long time = System.currentTimeMillis();
		final int _width = original.getWidth();
		final int _height = original.getHeight();
		boolean alpha = false;

		final int[] currentPixels = original.getPixels();// new int[_width *
															// _height];
		// original.getPixels(currentPixels, 0, _width, 0, 0, _width, _height);
		final int wm = _width - 1;
		final int hm = _height - 1;
		final int wh = _width * _height;
		final int div = radius + radius + 1;

		final int r[] = new int[wh];
		final int g[] = new int[wh];
		final int b[] = new int[wh];
		int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
		final int vmin[] = new int[Math.max(_width, _height)];

		int divsum = (div + 1) >> 1;
		divsum *= divsum;
		final int dv[] = new int[256 * divsum];
		for (i = 0; i < 256 * divsum; i++) {
			dv[i] = (i / divsum);
		}

		yw = yi = 0;

		final int[][] stack = new int[div][3];
		int stackpointer;
		int stackstart;
		int[] sir;
		int rbs;
		final int r1 = radius + 1;
		int routsum, goutsum, boutsum;
		int rinsum, ginsum, binsum;

		for (y = 0; y < _height; y++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			for (i = -radius; i <= radius; i++) {
				p = currentPixels[yi + Math.min(wm, Math.max(i, 0))];
				sir = stack[i + radius];
				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);
				rbs = r1 - Math.abs(i);
				rsum += sir[0] * rbs;
				gsum += sir[1] * rbs;
				bsum += sir[2] * rbs;
				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				} else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}
			}
			stackpointer = radius;

			for (x = 0; x < _width; x++) {
				if (!alpha)
					alpha = Color.alpha(original.getPixel(x, y)) != 255;

				r[yi] = dv[rsum];
				g[yi] = dv[gsum];
				b[yi] = dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (y == 0) {
					vmin[x] = Math.min(x + radius + 1, wm);
				}
				p = currentPixels[yw + vmin[x]];

				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[(stackpointer) % div];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi++;
			}
			yw += _width;
		}
		for (x = 0; x < _width; x++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			yp = -radius * _width;
			for (i = -radius; i <= radius; i++) {
				yi = Math.max(0, yp) + x;

				sir = stack[i + radius];

				sir[0] = r[yi];
				sir[1] = g[yi];
				sir[2] = b[yi];

				rbs = r1 - Math.abs(i);

				rsum += r[yi] * rbs;
				gsum += g[yi] * rbs;
				bsum += b[yi] * rbs;

				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				} else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}

				if (i < hm) {
					yp += _width;
				}
			}
			yi = x;
			stackpointer = radius;
			for (y = 0; y < _height; y++) {
				// Preserve alpha channel: ( 0xff000000 & pix[yi] )
				if (alpha)
					currentPixels[yi] = (0xff000000 & currentPixels[yi])
							| (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];
				else
					currentPixels[yi] = 0xff000000 | (dv[rsum] << 16)
							| (dv[gsum] << 8) | dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (x == 0) {
					vmin[y] = Math.min(y + r1, hm) * _width;
				}
				p = x + vmin[y];

				sir[0] = r[p];
				sir[1] = g[p];
				sir[2] = b[p];

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[stackpointer];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi += _width;
			}
		}
		original.setPixels(currentPixels, _width, _height);
		// return Bitmap.createBitmap(currentPixels, ,
		// original.getConfig());
	}

	/**
	 * Average blur filter. Implemented in native code.
	 * 
	 * @param bitmap
	 * @throws IllegalArgumentException
	 *             if <code>maskSize</code> is even.
	 */
	public static void averageBlurNative(final ToToBitmap bitmap,
			final int maskSize) {
		if (maskSize % 2 == 0) {
			throw new IllegalArgumentException(String.format(
					"the maskSize must odd, but %d is an even", maskSize));
		}
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.averageSmooth(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), maskSize);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Gaussian blur filter. Implemented in native code.
	 * 
	 * @param bitmap
	 * @param sigma
	 * @throws IllegalArgumentException
	 *             if <code>sigma</code> is too small.
	 */
	public static void discreteGaussianBlurNative(final ToToBitmap bitmap,
			final double sigma) {
		final int ksize = (int) (sigma * 3 + 1);
		if (ksize == 1) {
			throw new IllegalArgumentException(String.format(
					"sigma %f is too small", sigma));
		}
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.discreteGaussianBlur(pixels,
				bitmap.getFilterX(), bitmap.getFilterY(),
				bitmap.getFilterWidth(), bitmap.getFilterHeight(),
				bitmap.getWidth(), bitmap.getHeight(), sigma);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Motion blur filter. Implemented in native code.
	 * 
	 * @param bitmap
	 * @param xSpeed
	 *            speed in x direction
	 * @param ySpeed
	 *            speed in y direction.
	 */
	public static void motionBlurNative(final ToToBitmap bitmap, final int xSpeed,
			final int ySpeed) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.motionBlurFilter(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), xSpeed, ySpeed);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Tests if a given image is blurred.
	 * 
	 * @param input
	 *            An array of input pixels in YUV420SP format.
	 * @param width
	 *            The width of the input image.
	 * 
	 * @param height
	 *            The height of the input image.
	 * @return true when input image is blurred.
	 */
	public static boolean isBlurredNative(final byte[] input, final int width,
			final int height) {
		return NativeFilters.isBlurred(input, width, height);
	}

}
