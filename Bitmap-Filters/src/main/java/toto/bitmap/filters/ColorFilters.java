/*******************************************************************************
 * mobifluence.com (c) 2013 
 * Mobifluence Interactive 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;


import toto.bitmap.ToToBitmap;
import toto.bitmap.filters.nativ.NativeFilters;
import toto.graphics.color.RGBUtils;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

/**
 * Bitmap texture filters
 * <p>
 * * <b>Invert</b><br/>
 * <img src="../../../../resources/invert.png" ><br/>
 * <b>Solarize</b><br/>
 * <img src="../../../../resources/solarize.png" ><br/>
 * <b>Sepia</b><br/>
 * Sepia with the <code>depth</code> of 10. </br> <img
 * src="../../../../resources/sepia.png" ><br/>
 * 
 * <b>Quantize</b><br/>
 * <table>
 * <tr>
 * <th>With 128 Colors</th>
 * <th>With 256 Colors</th>
 * </tr>
 * <tr>
 * <td><img src="../../../../resources/quantize128.png" ></td>
 * <td><img src="../../../../resources/quantize256.png" ></td>
 * </tr>
 * </table>
 * <br/>
 * <b>Glow</b><br/>
 * Glow using the <code>glowAmount</code> of <code>0.5</code> and
 * <code>glowRadius</code> of <code>2</code><br/>
 * <img src="../../../../resources/glow.png"><br/>
 * 
 * <b>Oil Paint</b><br/>
 * <img src="../../../../resources/wholeimage_oilpaint.png" ><br/>
 * <b>Plasma</b><br/>
 * <img src="../../../../resources/plasma.png" ><br/>
 * <b>Temperature</b><br/>
 * <code>temperature</code> setKey to 8500.<br/>
 * Note:The filter is applied to the first half of the image only.</br> <img
 * src="../../../../resources/temperature.png" ><br/>
 * <b>Tritone</b><br/>
 * Tritone with the <code>shadowColor</code> Color.GRAY, the
 * <code>midColor</code> Color.BLUE and the <code>highColor</code> of Color.RED.
 * <br/>
 * Note:The filter is applied to the first half of the image only.</br> <img
 * src="../../../../resources/tritone.png" ><br/>
 * <b>Mix Channels</b><br/>
 * Mix channels filter applied by neglecting the green channel and blending it
 * with red.<br/>
 * 
 * Note:The filter is applied to the first half of the image only.<br/>
 * <img src="../../../../resources/mixchannels.png" ><br/>
 * * <b>Stamp</b><br/>
 * Stamp filter with the <code>lowerColor</code> as white ,
 * <code>upperColor</code> as red,<code> threshold</code> and
 * <code>softness</code> are both 0.5.<br/>
 * 
 * Note:The filter is applied to the first half of the image only.<br/>
 * <img src="../../../../resources/stamp.png" ><br/>
 * </p>
 */
public class ColorFilters {

	/**
	 * Poseterize the given bitmap
	 * 
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @param bitmap
	 * @param depth
	 *            Posterization depth
	 * @return
	 */
	public static final void posterize(final ToToBitmap bitmap, final int depth) {
		int y = 0, x = 0;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		final int width = bitmap.getWidth();
		int a, r, g, b, argb, pos, redArea, greenArea, blue;
		final int noOfAreas = 256 / depth;
		final int noOfValues = 256 / (depth - 1);
		final int[] pixels = bitmap.getPixels();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				pos = (y * width) + x;
				argb = pixels[pos];
				a = (argb >> 24) & 0xff;
				r = (argb >> 16) & 0xff;
				g = (argb >> 8) & 0xff;
				b = argb & 0xff;

				redArea = r / noOfAreas;
				r = noOfValues * redArea;

				greenArea = g / noOfAreas;
				g = noOfValues * greenArea;

				blue = b / noOfAreas;
				b = noOfValues * blue;
				pixels[pos] = (a & 0xff) << 24 | (r & 0xff) << 16
						| (g & 0xff) << 8 | (b & 0xff) << 0;
			}
		}
		bitmap.setPixels(pixels);
	}

	/**
	 * Set the transparency of an image
	 * 
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @param bitmap
	 * @param level
	 *            between 0 and 256. 0 indicates fully transparent and 256
	 *            indicates its fully opaque
	 * @see ColorFilters#setTransparencyNative(toto.graphics.bitmap.ToToJavaBitmap, int)
	 */
	public static final void setTransparency(final ToToBitmap bitmap, int level) {

		level = (level << 24);
		Log.d("Level after shift", "Level->" + level);
		int y = 0, x = 0;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		int pos;
		final int[] pixels = bitmap.getPixels();
		final int width = bitmap.getWidth();
		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				pos = (y * width) + x;
				pixels[pos] = (pixels[pos] & 0x00ffffff) | level;
			}
		}
		bitmap.setPixels(pixels);
	}

	/**
	 * Sepia filter. implemented in native code.
	 * 
	 * @see #sepia(toto.graphics.bitmap.ToToBitmap, int)
	 * @param bitmap
	 * @param depth
	 */
	public static void sepiaNative(final ToToBitmap bitmap, final int depth) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.sepiaFilter(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), depth);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Invert the bitmap's colors.
	 * 
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @param bitmap
	 * @return
	 */
	public static final void invert(final ToToBitmap bitmap) {
		int y = 0, x = 0;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		final int width = bitmap.getWidth();
		int pos, a, r, g, b, color;
		final int[] pixels = bitmap.getPixels();
		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				pos = (y * width) + x;
				color = pixels[pos];
				a = (color >> 24) & 0xff;
				r = (color >> 16) & 0xff;
				g = (color >> 8) & 0xff;
				b = color & 0xff;

				r = 255 - r;
				g = 255 - g;
				b = 255 - b;

				pixels[pos] = (a & 0xff) << 24 | (r & 0xff) << 16
						| (g & 0xff) << 8 | (b & 0xff) << 0;
			}
		}

		bitmap.setPixels(pixels);

	}

	public static final void invertNative(final ToToBitmap bitmap) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters
				.invertFilter(pixels, bitmap.getFilterX(), bitmap.getFilterY(),
						bitmap.getFilterWidth(), bitmap.getFilterHeight(),
						bitmap.getWidth(), bitmap.getHeight());
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());

	}

	/**
	 * Apply sepia (brown) filters
	 * 
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @param bitmap
	 * @param depth
	 *            Sepia depth. values between 1-100 provide an optimal output.
	 * @return
	 */
	public static final void sepia(final ToToBitmap bitmap, final int depth) {
		int y = 0, x = 0;

		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		int pixel, pos;
		final int w = bitmap.getWidth();
		final int[] pixels = bitmap.getPixels();
		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				pos = (y * w) + x;
				pixel = pixels[pos];
				final int a = (pixel >> 24) & 0xff;
				int r = (pixel >> 16) & 0xff;
				int g = (pixel >> 8) & 0xff;
				int b = pixel & 0xff;

				final int gry = (r + g + b) / 3;
				r = g = b = gry;
				r = r + (depth * 2);
				g = g + depth;
				if (r > 255) {
					r = 255;
				}
				if (g > 255) {
					g = 255;
				}
				pixels[pos] = (a & 0xff) << 24 | (r & 0xff) << 16
						| (g & 0xff) << 8 | (b & 0xff) << 0;
			}
		}
		bitmap.setPixels(pixels);
	}

	/**
	 * Saturate the given bitmap
	 * 
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @param bitmap
	 * @param percent
	 * @return
	 */
	public static final void saturate(final ToToBitmap bitmap, final int percent) {
		int y = 0, x = 0;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		int pixel;
		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				pixel = bitmap.getPixel(x, y);
				bitmap.setPixel(x, y, RGBUtils.setSaturation(pixel, percent));
			}
		}
	}

	/**
	 * A filter which quantizes an image to a setKey number of colors
	 * <p>
	 * useful for producing images which are to be encoded using an index color
	 * model. The filter can perform Floyd-Steinberg error-diffusion dithering
	 * if required. At present, the quantization is done using an octtree
	 * algorithm but I eventually hope to add more quantization methods such as
	 * median cut. Note: at present, the filter produces an image which uses the
	 * RGB color model (because the application it was written for required it).
	 * </p>
	 * 
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @param src
	 * @param numColors
	 *            the number of colors to quantize to. Usually:256
	 * @param dither
	 *            Set whether to use dithering or not. If not, the image is
	 *            posterized.
	 * @param serpentine
	 *            Set whether to use a serpentine pattern for return or not.
	 *            This can reduce 'avalanche' artifacts in the output.
	 */
	public static void quantize(final ToToBitmap src, final int numColors,
			final boolean dither, final boolean serpentine) {
		final int width = src.getWidth();
		final int height = src.getHeight();

		final int[] inPixels = src.getPixels();
		final int[] outPixels = src.getPixels();// new int[inPixels.length];
		final int sum = 3 + 5 + 7 + 1;
		/**
		 * Floyd-Steinberg dithering matrix.
		 */
		final int[] matrix = { 0, 0, 0, 0, 0, 7, 3, 5, 1, };

		final int count = width * height;
		final OctTreeQuantizer quantizer = new OctTreeQuantizer();
		quantizer.setup(numColors);
		quantizer.addPixels(inPixels, 0, count);
		final int[] table = quantizer.buildColorTable();

		if (!dither) {
			for (int i = 0; i < count; i++)
				outPixels[i] = table[quantizer.getIndexForColor(inPixels[i])];
		} else {
			int index = 0;
			for (int y = 0; y < height; y++) {
				final boolean reverse = serpentine && (y & 1) == 1;
				int direction;
				if (reverse) {
					index = y * width + width - 1;
					direction = -1;
				} else {
					index = y * width;
					direction = 1;
				}
				for (int x = 0; x < width; x++) {
					int rgb1 = inPixels[index];
					final int rgb2 = table[quantizer.getIndexForColor(rgb1)];

					outPixels[index] = rgb2;

					int r1 = (rgb1 >> 16) & 0xff;
					int g1 = (rgb1 >> 8) & 0xff;
					int b1 = rgb1 & 0xff;

					final int r2 = (rgb2 >> 16) & 0xff;
					final int g2 = (rgb2 >> 8) & 0xff;
					final int b2 = rgb2 & 0xff;

					final int er = r1 - r2;
					final int eg = g1 - g2;
					final int eb = b1 - b2;

					for (int i = -1; i <= 1; i++) {
						final int iy = i + y;
						if (0 <= iy && iy < height) {
							for (int j = -1; j <= 1; j++) {
								final int jx = j + x;
								if (0 <= jx && jx < width) {
									int w;
									if (reverse)
										w = matrix[(i + 1) * 3 - j + 1];
									else
										w = matrix[(i + 1) * 3 + j + 1];
									if (w != 0) {
										final int k = reverse ? index - j
												: index + j;
										rgb1 = inPixels[k];
										r1 = (rgb1 >> 16) & 0xff;
										g1 = (rgb1 >> 8) & 0xff;
										b1 = rgb1 & 0xff;
										r1 += er * w / sum;
										g1 += eg * w / sum;
										b1 += eb * w / sum;
										inPixels[k] = (RGBUtils.clamp(r1) << 16)
												| (RGBUtils.clamp(g1) << 8)
												| RGBUtils.clamp(b1);
									}
								}
							}
						}
					}
					index += direction;
				}
			}
		}
	}

	/**
	 * Gaussian high pass filter.
	 * 
	 * @param src
	 * @param radius
	 *            recommended: 10
	 * @param processAlpha
	 * @param premultiplyAlpha
	 * @param outputConfig
	 */
	public static void applyHighPass(final ToToBitmap src, final float radius,
			final boolean processAlpha, final boolean premultiplyAlpha,
			final Bitmap.Config outputConfig) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = src.getPixels();
		final int[] outPixels = src.getPixels();
		final Kernel kernel = GaussianUtils.makeKernel(radius);
		if (radius > 0) {
			GaussianUtils.convolveAndTranspose(kernel, inPixels, outPixels,
					width, height, processAlpha, processAlpha
							&& premultiplyAlpha, false,
					GenericFilters.CLAMP_EDGES);
			GaussianUtils.convolveAndTranspose(kernel, outPixels, inPixels,
					height, width, processAlpha, false, processAlpha
							&& premultiplyAlpha, GenericFilters.CLAMP_EDGES);
		}

		int index = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				final int rgb1 = outPixels[index];
				int r1 = (rgb1 >> 16) & 0xff;
				int g1 = (rgb1 >> 8) & 0xff;
				int b1 = rgb1 & 0xff;

				final int rgb2 = inPixels[index];
				final int r2 = (rgb2 >> 16) & 0xff;
				final int g2 = (rgb2 >> 8) & 0xff;
				final int b2 = rgb2 & 0xff;

				r1 = (r1 + 255 - r2) / 2;
				g1 = (g1 + 255 - g2) / 2;
				b1 = (b1 + 255 - b2) / 2;

				inPixels[index] = (rgb1 & 0xff000000) | (r1 << 16) | (g1 << 8)
						| b1;
				index++;
			}
		}
		src.setPixels(inPixels, src.getWidth(), src.getHeight());
		// return Bitmap.createBitmap(inPixels, src.getWidth(), src.getHeight(),
		// outputConfig);
	}

	/**
	 * Tint the given bitmap
	 * 
	 * @param bitmap
	 * @param tintDegree
	 *            degree to tint the bitmap
	 * @return
	 */
	public static void tint(final ToToBitmap bitmap, final int tintDegree) {
		final int[] pixels = bitmap.getPixels();
		int RY, BY, RYY, GYY, BYY, R, G, B, Y;
		final double angle = (3.14159d * tintDegree) / 180.0d;
		final int S = (int) (256.0d * Math.sin(angle));
		final int C = (int) (256.0d * Math.cos(angle));

		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		final int w = bitmap.getWidth();
		int x, y, pos, pixel;

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				pos = (y * w) + x;
				pixel = pixels[pos];
				final int r = (pixel >> 16) & 0xff;
				final int g = (pixel >> 8) & 0xff;
				final int b = pixel & 0xff;
				RY = (70 * r - 59 * g - 11 * b) / 100;
				// GY = (-30 * r + 41 * g - 11 * b) / 100;
				BY = (-30 * r - 59 * g + 89 * b) / 100;
				Y = (30 * r + 59 * g + 11 * b) / 100;
				RYY = (S * BY + C * RY) / 256;
				BYY = (C * BY - S * RY) / 256;
				GYY = (-51 * RYY - 19 * BYY) / 100;
				R = Y + RYY;
				R = (R < 0) ? 0 : ((R > 255) ? 255 : R);
				G = Y + GYY;
				G = (G < 0) ? 0 : ((G > 255) ? 255 : G);
				B = Y + BYY;
				B = (B < 0) ? 0 : ((B > 255) ? 255 : B);
				pixels[pos] = 0xff000000 | (R << 16) | (G << 8) | B;
			}
		}
		bitmap.setPixels(pixels);
	}

	/**
	 * Converts given bitmap to grayscale with specified saturation level
	 * 
	 * @param bitmap
	 * @param saturation
	 *            Grayscale saturation level
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @return
	 */
	public static final void grayScale(final ToToBitmap bitmap,
			final int saturation) {

		int alpha, red, green, blue;
		int output_red, output_green, output_blue;

		// We will use the standard NTSC color quotiens, multiplied by 1024
		// in order to be able to use integer-only math throughout the code.
		final int RW = 306; // 0.299 * 1024
		final int RG = 601; // 0.587 * 1024
		final int RB = 117; // 0.114 * 1024

		// Define and calculate matrix quotients
		final int a, b, c, d, e, f, g, h, i;
		a = (1024 - saturation) * RW + saturation * 1024;
		b = (1024 - saturation) * RW;
		c = (1024 - saturation) * RW;
		d = (1024 - saturation) * RG;
		e = (1024 - saturation) * RG + saturation * 1024;
		f = (1024 - saturation) * RG;
		g = (1024 - saturation) * RB;
		h = (1024 - saturation) * RB;
		i = (1024 - saturation) * RB + saturation * 1024;

		int y, x;

		int pixel = 0;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {

				pixel = bitmap.getPixel(x, y);
				alpha = (0xFF000000 & pixel);
				red = (0x00FF & (pixel >> 16));
				green = (0x0000FF & (pixel >> 8));
				blue = pixel & (0x000000FF);

				// Matrix multiplication
				output_red = ((a * red + d * green + g * blue) >> 4) & 0x00FF0000;
				output_green = ((b * red + e * green + h * blue) >> 12) & 0x0000FF00;
				output_blue = (c * red + f * green + i * blue) >> 20;

				bitmap.setPixel(x, y,
						(alpha | output_red | output_green | output_blue));
			}
		}
	}

	public static void greyToRGB(final ToToBitmap bitmap) {
		final int[] des = grayScaleToRGB(bitmap.getPixels());
		bitmap.setPixels(des, bitmap.getWidth(), bitmap.getHeight());
	}

	private static final int[] grayScaleToRGB(final int[] pixels) {
		final int[] newPixels = new int[pixels.length];
		for (int i = 0; i < newPixels.length; i++) {
			final int pix = pixels[i] & 0xFF;
			newPixels[i] = (pix | pix << 8 | pix << 16 | 0xFF000000);
		}
		return newPixels;
	}

	/**
	 * Decrease the color depth of the given bitmap
	 * 
	 * @param bitmap
	 * @param bitOffset
	 * @return
	 */
	public static void decreaseColorDepth(final ToToBitmap bitmap,
			final int bitOffset) {
		int A, R, G, B;
		int y, x;

		int pixel = 0;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				// argb[position] = PixelUtils.setSaturation(argb[position],
				// percent);
				pixel = bitmap.getPixel(x, y);
				A = Color.alpha(pixel);
				R = Color.red(pixel);
				G = Color.green(pixel);
				B = Color.blue(pixel);
				// round-off color offset
				R = ((R + (bitOffset / 2))
						- ((R + (bitOffset / 2)) % bitOffset) - 1);
				if (R < 0) {
					R = 0;
				}
				G = ((G + (bitOffset / 2))
						- ((G + (bitOffset / 2)) % bitOffset) - 1);
				if (G < 0) {
					G = 0;
				}
				B = ((B + (bitOffset / 2))
						- ((B + (bitOffset / 2)) % bitOffset) - 1);
				if (B < 0) {
					B = 0;
				}

				bitmap.setPixel(x, y, Color.argb(A, R, G, B));
			}
		}
	}

	/**
	 * Gotham filter implemented in native code.
	 * <p>
	 * Dark effect like those in the batman movies.
	 * </p>
	 * 
	 * @param bitmap
	 */
	public static void gothamNative(final ToToBitmap bitmap) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters
				.gothamFilter(pixels, bitmap.getFilterX(), bitmap.getFilterY(),
						bitmap.getFilterWidth(), bitmap.getFilterHeight(),
						bitmap.getWidth(), bitmap.getHeight());
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Set the temperature of the image
	 * 
	 * @param bitmap
	 * @param temperature
	 *            of the image min:1000 max:10000
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @return
	 */
	public static void temperature(final ToToBitmap bitmap, float temperature) {
		int rgb, a, r, g, b, x, y;
		temperature = Math.max(1000F, Math.min(10000F, temperature));
		final int t = 3 * (int) ((temperature - 1000F) / 100F);
		float rFactor = 1.0F / RGBUtils.blackBodyRGB[t];
		float gFactor = 1.0F / RGBUtils.blackBodyRGB[t + 1];
		float bFactor = 1.0F / RGBUtils.blackBodyRGB[t + 2];
		final float m = Math.max(Math.max(rFactor, gFactor), bFactor);
		rFactor /= m;
		gFactor /= m;
		bFactor /= m;

		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				rgb = bitmap.getPixel(x, y);

				a = rgb & 0xff000000;
				r = rgb >> 16 & 0xff;
				g = rgb >> 8 & 0xff;
				b = rgb & 0xff;
				r = (int) (r * rFactor);
				g = (int) (g * gFactor);
				b = (int) (b * bFactor);
				bitmap.setPixel(x, y, (a | r << 16 | g << 8 | b));
			}
		}
	}

	/**
	 * A filter which performs a tritone conversion on an image.
	 * <p>
	 * Given three colors for shadows, midtones and highlights, it converts the
	 * image to grayscale and then applies a color mapping based on the colors.
	 * </p>
	 * 
	 * @param bitmap
	 * @param shadowColor
	 * @param midColor
	 * @param highColor
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @return
	 */
	public static void tritone(final ToToBitmap bitmap, final int shadowColor,
			final int midColor, final int highColor) {
		int rgb, y, x;
		final int[] lut = new int[256];
		for (int i = 0; i < 128; i++) {
			final float t = i / 127.0f;
			lut[i] = RGBUtils.mixColors(t, shadowColor, midColor);
		}
		for (int i = 128; i < 256; i++) {
			final float t = (i - 127) / 128.0f;
			lut[i] = RGBUtils.mixColors(t, midColor, highColor);
		}

		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				rgb = bitmap.getPixel(x, y);
				bitmap.setPixel(x, y, lut[RGBUtils.brightness(rgb)]);
			}
		}
	}

	/**
	 * A filter which allows the red, green and blue channels of an image to be
	 * mixed into each other.
	 * <p>
	 * Any particular color channel can be neglected by setting it to 0. and
	 * setting the <code>intoXX</code> value to 255.
	 * </p>
	 * 
	 * @param bitmap
	 * @param blueGreen
	 *            amount of blue to mix into green. min:0 max:255
	 * @param intoRed
	 *            corresponding level into red. min:0 max:255
	 * @param redBlue
	 *            amount of red to mix into blue. min:0 max:255
	 * @param intoGreen
	 *            corresponding level into green. min:0 max:255
	 * @param greenRed
	 *            amount of green to mix into red. min:0 max:255
	 * @param intoBlue
	 *            corresponding level into blue. min:0 max:255
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @return
	 */
	public static void mixChannels(final ToToBitmap bitmap, final int blueGreen,
			final int intoRed, final int redBlue, final int intoGreen,
			final int greenRed, final int intoBlue) {
		int rgb, a, r, g, b, nr, ng, nb;
		int y = 0, x = 0;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				rgb = bitmap.getPixel(x, y);
				a = rgb & 0xff000000;
				r = (rgb >> 16) & 0xff;
				g = (rgb >> 8) & 0xff;
				b = rgb & 0xff;
				nr = RGBUtils
						.clamp((intoRed
								* (blueGreen * g + (255 - blueGreen) * b) / 255 + (255 - intoRed)
								* r) / 255);
				ng = RGBUtils
						.clamp((intoGreen * (redBlue * b + (255 - redBlue) * r)
								/ 255 + (255 - intoGreen) * g) / 255);
				nb = RGBUtils
						.clamp((intoBlue
								* (greenRed * r + (255 - greenRed) * g) / 255 + (255 - intoBlue)
								* b) / 255);
				bitmap.setPixel(x, y, (a | (nr << 16) | (ng << 8) | nb));
			}
		}
	}

	public static void solarize(final ToToBitmap bitmap) {
		int rgb, a, r, g, b;
		int[] rTable, gTable, bTable;
		rTable = gTable = bTable = makeSolarizeTable();
		int y = 0, x = 0;

		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				rgb = bitmap.getPixel(x, y);
				a = rgb & 0xff000000;
				r = (rgb >> 16) & 0xff;
				g = (rgb >> 8) & 0xff;
				b = rgb & 0xff;
				r = rTable[r];
				g = gTable[g];
				b = bTable[b];
				bitmap.setPixel(x, y, (a | (r << 16) | (g << 8) | b));
			}
		}
	}

	private static float transferFunction(final float v) {
		return v > 0.5f ? 2 * (v - 0.5f) : 2 * (0.5f - v);
	}

	private static int[] makeSolarizeTable() {
		final int[] table = new int[256];
		for (int i = 0; i < 256; i++)
			table[i] = RGBUtils
					.clamp((int) (255 * transferFunction(i / 255.0f)));
		return table;
	}

	/**
	 * Grayscale implementation in native code.
	 * <p>
	 * A 300x300 bitmap takes just 7ms in this native implementation while the
	 * java implementation {@link #grayScale(toto.graphics.bitmap.ToToBitmap, int)}
	 * takes 80ms.
	 * </p>
	 * 
	 * @param bitmap
	 * @param saturation
	 */
	public static void grayScaleNative(final ToToBitmap bitmap,
			final int saturation) {
		int[] pixels = bitmap.getPixels();
		Log.d("Filter", "Before ->" + pixels.length);
		pixels = NativeFilters.grayScaleFilter(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), saturation);
		Log.d("Filter", "After ->" + pixels.length);
		bitmap.setPixels(pixels);

	}

	/**
	 * Set the transparency of an image using native code.
	 * <p>
	 * Guaranteed to be atleast 20 times faster than the java implementation
	 * {@link #setTransparency(toto.graphics.bitmap.ToToBitmap, int)}
	 * </p>
	 * 
	 * 
	 * @see toto.graphics.bitmap.ToToBitmap#setFilterBounds(int, int, int, int)
	 * @param bitmap
	 * @param level
	 *            between 0 and 256. 0 indicates fully transparent and 256
	 *            indicates its fully opaque
	 * @return
	 */
	public static final void transparencyNative(final ToToBitmap bitmap,
			final int level) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.transparencyFilter(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), level);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * A filter which produces a rubber-stamp type of effect.
	 * 
	 * @param bitmap
	 * @param lowerColor
	 * @param upperColor
	 *            Set the color to be used for pixels above the upper threshold.
	 * @param threshold
	 *            the color to be used for pixels below the lower threshold.
	 *            min:0 max:1
	 * @param softness
	 *            the softness of the effect. min:0 max:1
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @return
	 */
	public static void stamp(final ToToBitmap bitmap, final int lowerColor,
			final int upperColor, final float threshold, final float softness) {
		int rgb, r, g, b, l;
		final float lowerThreshold3 = 255 * 3 * (threshold - softness * 0.5f);
		final float upperThreshold3 = 255 * 3 * (threshold + softness * 0.5f);
		int y = 0, x = 0;

		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {

				rgb = bitmap.getPixel(x, y);// src.pixelArray[position];

				r = (rgb >> 16) & 0xff;
				g = (rgb >> 8) & 0xff;
				b = rgb & 0xff;
				l = r + g + b;
				final float f = ImageMath.smoothStep(lowerThreshold3,
						upperThreshold3, l);
				bitmap.setPixel(x, y,
						RGBUtils.mixColors(f, upperColor, lowerColor));
			}
		}
	}

}
