package toto.bitmap.filters;

import java.util.HashMap;

import toto.bitmap.ToToJavaBitmap;
import toto.bitmap.ToToBitmap;
import toto.bitmap.filters.nativ.NativeFilters;
import toto.graphics.color.RGBUtils;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;

public final class GlowFilters {

	private static HashMap<String, Bitmap> mGlowCache = new HashMap<String, Bitmap>();

	/**
	 * Returns a glowed image of the provided icon. If the provided name is
	 * already in the cache, the cached image will be returned. Otherwise, the
	 * bitmap will be glowed and cached under the provided name
	 * 
	 * @param name
	 *            The name of the bitmap
	 * @param src
	 *            The bitmap of the icon itself
	 * @return Glowed bitmap
	 */
	public Bitmap getGlow(final String name, final int glowColor,
			final Bitmap src) {
		if (mGlowCache.containsKey(name)) {
			return mGlowCache.get(name);
		} else {
			// An added margin to the initial image
			final int margin = 0;
			final int halfMargin = margin / 2;

			// The glow radius
			final int glowRadius = 4;

			// Extract the alpha from the source image
			final Bitmap alpha = src.extractAlpha();

			// The output bitmap (with the icon + glow)
			final Bitmap bmp = Bitmap.createBitmap(src.getWidth() + margin,
					src.getHeight() + margin, Bitmap.Config.ARGB_8888);

			// The canvas to paint on the image
			final Canvas canvas = new Canvas(bmp);

			final Paint paint = new Paint();
			paint.setColor(glowColor);

			// Outer glow
			final ColorFilter emphasize = new LightingColorFilter(glowColor, 1);
			paint.setColorFilter(emphasize);
			canvas.drawBitmap(src, halfMargin, halfMargin, paint);
			paint.setColorFilter(null);
			paint.setMaskFilter(new BlurMaskFilter(glowRadius, Blur.OUTER));
			canvas.drawBitmap(alpha, halfMargin, halfMargin, paint);

			// Cache icon
			mGlowCache.put(name, bmp);

			return bmp;
		}
	}

	/**
	 * Soft Glow filter implemented in native code.
	 * 
	 * @param bitmap
	 * @param blurSigma
	 */
	public static void softGlowNative(final ToToBitmap bitmap,
			final double blurSigma) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.softGlow(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), blurSigma);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Neon filter implemented in native code.
	 * 
	 * @param bitmap
	 * @param r
	 * @param g
	 * @param b
	 */
	public static void neonNative(final ToToBitmap bitmap, final int r,
			final int g, final int b) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.neonFilter(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), r, g, b);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * A filter which adds Gaussian blur to Bitmap, producing a glowing effect.
	 * 
	 * @see toto.graphics.bitmap.ToToJavaBitmap#setFilterBounds(int, int, int, int)
	 * @param bitmap
	 *            Source Bitmap
	 * @param glowAmount
	 *            Amount of glow. Should be from 0 to 1.Recommended:0.5f
	 * @param blurRadius
	 *            recommended :2
	 * @param processAlpha
	 *            process alpha for this image, recommended:true
	 * @param premultiplyAlpha
	 *            premulitply alpha. recommended:true
	 * @return
	 */
	public static void applyGlow(final ToToJavaBitmap bitmap,
			final float glowAmount, final float blurRadius,
			final boolean processAlpha, final boolean premultiplyAlpha) {

		final int width = bitmap.getWidth();
		final int height = bitmap.getHeight();
		final int[] argb = bitmap.getPixels();
		int[] dest = new int[argb.length];
		dest = bitmap.getPixels();
		final Kernel kernel = GaussianUtils.makeKernel(blurRadius);
		if (blurRadius > 0) {
			GaussianUtils.convolveAndTranspose(kernel, argb, dest, width,
					height, processAlpha, processAlpha && premultiplyAlpha,
					false, GenericFilters.CLAMP_EDGES);
			GaussianUtils.convolveAndTranspose(kernel, dest, argb, height,
					width, processAlpha, false, processAlpha
							&& premultiplyAlpha, GenericFilters.CLAMP_EDGES);
		}
		final float a = 4 * glowAmount;
		int index = 0;
		int y = 0, x = 0;
		final int filterY = bitmap.getFilterY();
		final int filterX = bitmap.getFilterX();
		final int filterWidth = bitmap.getFilterWidth();
		final int filterHeight = bitmap.getFilterHeight();
		final int[] pix = bitmap.getPixels();

		for (y = filterY; y < filterHeight; y++) {
			for (x = filterX; x < filterWidth; x++) {
				final int rgb1 = dest[index];
				int r1 = (rgb1 >> 16) & 0xff;
				int g1 = (rgb1 >> 8) & 0xff;
				int b1 = rgb1 & 0xff;

				final int rgb2 = argb[index];
				final int r2 = (rgb2 >> 16) & 0xff;
				final int g2 = (rgb2 >> 8) & 0xff;
				final int b2 = rgb2 & 0xff;

				r1 = RGBUtils.clamp((int) (r1 + a * r2));
				g1 = RGBUtils.clamp((int) (g1 + a * g2));
				b1 = RGBUtils.clamp((int) (b1 + a * b2));

				pix[index] = (rgb1 & 0xff000000) | (r1 << 16) | (g1 << 8) | b1;
				index++;
			}
		}

	}

}
