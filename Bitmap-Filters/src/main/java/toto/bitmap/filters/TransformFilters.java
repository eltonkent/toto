/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;

import toto.math.MathUtils;

public class TransformFilters {

	/**
	 * Treat pixels off the edge as zero.
	 */
	public final static int EDGE_ACTION_ZERO = 0;

	/**
	 * Clamp pixels to the image edges.
	 */
	public final static int EDGE_ACTION_CLAMP = 1;

	/**
	 * Wrap pixels off the edge onto the oppsoite edge.
	 */
	public final static int EDGE_ACTION_WRAP = 2;

	/**
	 * Clamp pixels RGB to the image edges, but zero the alpha. This prevents
	 * gray borders on your image.
	 */
	public final static int EDGE_ACTION_RGB_CLAMP = 3;

	/**
	 * Use nearest-neighbout interpolation.
	 */
	public final static int INTERPOLATION_NEAREST_NEIGHBOUR = 0;

	/**
	 * Use bilinear interpolation.
	 */
	public final static int INTERPOLATION_BILINEAR = 1;

	/**
	 * The action to take for pixels off the image edge.
	 */
	protected int edgeAction = EDGE_ACTION_RGB_CLAMP;

	static int getPixel(final int[] pixels, final int x, final int y,
			final int width, final int height, final int edgeAction) {
		if (x < 0 || x >= width || y < 0 || y >= height) {
			switch (edgeAction) {
			case EDGE_ACTION_ZERO:
			default:
				return 0;
			case EDGE_ACTION_WRAP:
				return pixels[(ImageMath.mod(y, height) * width)
						+ ImageMath.mod(x, width)];
			case EDGE_ACTION_CLAMP:
				return pixels[(MathUtils.clamp(y, 0, height - 1) * width)
						+ MathUtils.clamp(x, 0, width - 1)];
			case EDGE_ACTION_RGB_CLAMP:
				return pixels[(MathUtils.clamp(y, 0, height - 1) * width)
						+ MathUtils.clamp(x, 0, width - 1)] & 0x00ffffff;
			}
		}
		return pixels[y * width + x];
	}
}
