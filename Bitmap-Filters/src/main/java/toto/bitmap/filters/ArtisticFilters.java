package toto.bitmap.filters;

import java.util.Random;

import toto.bitmap.filters.nativ.NativeFilters;
import toto.geom2d.Rectangle;
import toto.bitmap.ToToBitmap;
import toto.graphics.color.RGBUtils;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

public class ArtisticFilters {

	/**
	 * Sketch filter implemented in native code.
	 * 
	 * @param bitmap
	 */
	public static void sketchNative(final ToToBitmap bitmap) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters
				.sketchFilter(pixels, bitmap.getFilterX(), bitmap.getFilterY(),
						bitmap.getFilterWidth(), bitmap.getFilterHeight(),
						bitmap.getWidth(), bitmap.getHeight());
		bitmap.setPixels(pixels);
	}

	/**
	 * Lomographic filter implemented in native code.
	 * <p>
	 * Lomo filters highlight the area in the circle with the given radius.
	 * </p>
	 * 
	 * @param bitmap
	 */
	public static void lomo(final ToToBitmap bitmap, final double roundRadius) {

		final int width = bitmap.getWidth();
		final int height = bitmap.getHeight();

		final Bitmap returnBitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);
		final Canvas canvas = new Canvas(returnBitmap);
		final Paint paint = new Paint();
		paint.setAntiAlias(true);

		final float scaleValue = 95 * 1.0F / 127;

		final ColorMatrix scaleMatrix = new ColorMatrix();
		scaleMatrix.reset();
		scaleMatrix.setScale((float) (scaleValue + 0.2),
				(float) (scaleValue + 0.4), (float) (scaleValue + 0.2), 1);

		final ColorMatrix satMatrix = new ColorMatrix();
		satMatrix.reset();
		satMatrix.setSaturation(0.85f);

		final ColorMatrix allMatrix = new ColorMatrix();
		allMatrix.reset();
		allMatrix.postConcat(scaleMatrix);
		allMatrix.postConcat(satMatrix);

		paint.setColorFilter(new ColorMatrixColorFilter(allMatrix));
		canvas.drawBitmap(bitmap.getBitmap(), 0, 0, paint);

		int[] pixels = new int[width * height];
		returnBitmap.getPixels(pixels, 0, width, 0, 0, width, height);

		pixels = NativeFilters.lomoAddBlckRound(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), roundRadius);
		bitmap.setPixels(pixels, width, height);

	}

	/**
	 * HDR filter. implemented in native code.
	 * 
	 * @param bitmap
	 */
	public static void HDRNative(final ToToBitmap bitmap) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters
				.hdrFilter(pixels, bitmap.getFilterX(), bitmap.getFilterY(),
						bitmap.getFilterWidth(), bitmap.getFilterHeight(),
						bitmap.getWidth(), bitmap.getHeight());
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	public static void oldLook(final ToToBitmap bitmap) {
		final int width = bitmap.getWidth();
		final int height = bitmap.getHeight();
		int pixColor = 0;
		int pixR = 0;
		int pixG = 0;
		int pixB = 0;
		int newR = 0;
		int newG = 0;
		int newB = 0;
		final int[] pixels = bitmap.getPixels();
		for (int i = 0; i < height; i++) {
			for (int k = 0; k < width; k++) {
				pixColor = pixels[width * i + k];
				pixR = Color.red(pixColor);
				pixG = Color.green(pixColor);
				pixB = Color.blue(pixColor);
				newR = (int) (0.393 * pixR + 0.769 * pixG + 0.189 * pixB);
				newG = (int) (0.349 * pixR + 0.686 * pixG + 0.168 * pixB);
				newB = (int) (0.272 * pixR + 0.534 * pixG + 0.131 * pixB);
				final int newColor = Color.argb(255, newR > 255 ? 255 : newR,
						newG > 255 ? 255 : newG, newB > 255 ? 255 : newB);
				pixels[width * i + k] = newColor;
			}
		}

		bitmap.setPixels(pixels, width, height);
	}

	/**
	 * A filter which produces a "oil-painting" effect.
	 * <p>
	 * Extremely CPU intensive. Run on dedicated thread.<br/>
	 * 
	 * </p>
	 * 
	 * @param src
	 * @param range
	 *            Range of effect in pixels. Recommended:3.
	 * @param levels
	 *            Set the number of levels for the effect. Recommended:256
	 * @return
	 */
	public static void oilPaint(final ToToBitmap src, final int range,
			final int levels) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = src.getPixels();
		final int[] outPixels = new int[inPixels.length];
		int index = 0;
		final int[] rHistogram = new int[levels];
		final int[] gHistogram = new int[levels];
		final int[] bHistogram = new int[levels];
		final int[] rTotal = new int[levels];
		final int[] gTotal = new int[levels];
		final int[] bTotal = new int[levels];

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				for (int i = 0; i < levels; i++)
					rHistogram[i] = gHistogram[i] = bHistogram[i] = rTotal[i] = gTotal[i] = bTotal[i] = 0;

				for (int row = -range; row <= range; row++) {
					final int iy = y + row;
					int ioffset;
					if (0 <= iy && iy < height) {
						ioffset = iy * width;
						for (int col = -range; col <= range; col++) {
							final int ix = x + col;
							if (0 <= ix && ix < width) {
								final int rgb = inPixels[ioffset + ix];
								final int r = (rgb >> 16) & 0xff;
								final int g = (rgb >> 8) & 0xff;
								final int b = rgb & 0xff;
								final int ri = r * levels / 256;
								final int gi = g * levels / 256;
								final int bi = b * levels / 256;
								rTotal[ri] += r;
								gTotal[gi] += g;
								bTotal[bi] += b;
								rHistogram[ri]++;
								gHistogram[gi]++;
								bHistogram[bi]++;
							}
						}
					}
				}

				int r = 0, g = 0, b = 0;
				for (int i = 1; i < levels; i++) {
					if (rHistogram[i] > rHistogram[r])
						r = i;
					if (gHistogram[i] > gHistogram[g])
						g = i;
					if (bHistogram[i] > bHistogram[b])
						b = i;
				}
				r = rTotal[r] / rHistogram[r];
				g = gTotal[g] / gHistogram[g];
				b = bTotal[b] / bHistogram[b];
				outPixels[index] = (inPixels[index] & 0xff000000) | (r << 16)
						| (g << 8) | b;
				index++;
			}
		}
		src.setPixels(outPixels, src.getWidth(), src.getHeight());
	}

	/**
	 * Oil paint filter implemented in native code.
	 * 
	 * @param bitmap
	 */
	public static void oilPaintNative(final ToToBitmap bitmap) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters
				.oilFilter(pixels, bitmap.getFilterX(), bitmap.getFilterY(),
						bitmap.getFilterWidth(), bitmap.getFilterHeight(),
						bitmap.getWidth(), bitmap.getHeight());
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	public static void tvNative(final ToToBitmap bitmap) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters
				.tvFilter(pixels, bitmap.getFilterX(), bitmap.getFilterY(),
						bitmap.getFilterWidth(), bitmap.getFilterHeight(),
						bitmap.getWidth(), bitmap.getHeight());
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}

	private static boolean doPlasmaPixel(final int x1, final int y1,
			final int x2, final int y2, final int[] pixels, final int stride,
			final int depth, final int scale, final float turbulence) {
		int mx, my;

		if (depth == 0) {
			int ml, mr, mt, mb, mm, t;

			final int tl = RGBUtils.getPixel(x1, y1, pixels, stride);
			final int bl = RGBUtils.getPixel(x1, y2, pixels, stride);
			final int tr = RGBUtils.getPixel(x2, y1, pixels, stride);
			final int br = RGBUtils.getPixel(x2, y2, pixels, stride);

			final float amount = (256.0f / (2.0f * scale)) * turbulence;

			mx = (x1 + x2) / 2;
			my = (y1 + y2) / 2;

			if (mx == x1 && mx == x2 && my == y1 && my == y2)
				return true;

			if (mx != x1 || mx != x2) {
				ml = RGBUtils.average(tl, bl);
				ml = RGBUtils.displace(ml, amount);
				RGBUtils.putPixel(x1, my, ml, pixels, stride);

				if (x1 != x2) {
					mr = RGBUtils.average(tr, br);
					mr = RGBUtils.displace(mr, amount);
					RGBUtils.putPixel(x2, my, mr, pixels, stride);
				}
			}

			if (my != y1 || my != y2) {
				if (x1 != mx || my != y2) {
					mb = RGBUtils.average(bl, br);
					mb = RGBUtils.displace(mb, amount);
					RGBUtils.putPixel(mx, y2, mb, pixels, stride);
				}

				if (y1 != y2) {
					mt = RGBUtils.average(tl, tr);
					mt = RGBUtils.displace(mt, amount);
					RGBUtils.putPixel(mx, y1, mt, pixels, stride);
				}
			}

			if (y1 != y2 || x1 != x2) {
				mm = RGBUtils.average(tl, br);
				t = RGBUtils.average(bl, tr);
				mm = RGBUtils.average(mm, t);
				mm = RGBUtils.displace(mm, amount);
				RGBUtils.putPixel(mx, my, mm, pixels, stride);
			}

			if (x2 - x1 < 3 && y2 - y1 < 3)
				return false;
			return true;
		}

		mx = (x1 + x2) / 2;
		my = (y1 + y2) / 2;
		doPlasmaPixel(x1, y1, mx, my, pixels, stride, depth - 1, scale + 1,
				turbulence);
		doPlasmaPixel(x1, my, mx, y2, pixels, stride, depth - 1, scale + 1,
				turbulence);
		doPlasmaPixel(mx, y1, x2, my, pixels, stride, depth - 1, scale + 1,
				turbulence);
		return doPlasmaPixel(mx, my, x2, y2, pixels, stride, depth - 1,
				scale + 1, turbulence);
	}

	/**
	 * 
	 * @param src
	 * @param turbulence
	 *            Specifies the turbulence of the texture. Min Value: 0,Max
	 *            value: 10. recommended:1.0
	 * @param useImageColors
	 *            recommended: false.
	 * @param useColormap
	 *            Use color map for this filter. recommended:false.
	 * @param outputConfig
	 */
	public static void plasma(final ToToBitmap src, final float turbulence,
			final boolean useImageColors, final boolean useColormap,
			final Bitmap.Config outputConfig) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		final int[] inPixels = src.getPixels();
		final int[] outPixels = src.getPixels();
		final Random randomGenerator = new Random();
		randomGenerator.setSeed(System.currentTimeMillis());
		final Colormap colormap = new LinearColormap();
		final Rectangle originalSpace = new Rectangle(0, 0, width, height);
		final int w1 = width - 1;
		final int h1 = height - 1;
		RGBUtils.putPixel(
				0,
				0,
				randomRGB(inPixels, 0, 0, useImageColors, originalSpace,
						randomGenerator), outPixels, width);
		RGBUtils.putPixel(
				w1,
				0,
				randomRGB(inPixels, w1, 0, useImageColors, originalSpace,
						randomGenerator), outPixels, width);
		RGBUtils.putPixel(
				0,
				h1,
				randomRGB(inPixels, 0, h1, useImageColors, originalSpace,
						randomGenerator), outPixels, width);
		RGBUtils.putPixel(
				w1,
				h1,
				randomRGB(inPixels, w1, h1, useImageColors, originalSpace,
						randomGenerator), outPixels, width);
		RGBUtils.putPixel(
				w1 / 2,
				h1 / 2,
				randomRGB(inPixels, w1 / 2, h1 / 2, useImageColors,
						originalSpace, randomGenerator), outPixels, width);
		RGBUtils.putPixel(
				0,
				h1 / 2,
				randomRGB(inPixels, 0, h1 / 2, useImageColors, originalSpace,
						randomGenerator), outPixels, width);
		RGBUtils.putPixel(
				w1,
				h1 / 2,
				randomRGB(inPixels, w1, h1 / 2, useImageColors, originalSpace,
						randomGenerator), outPixels, width);
		RGBUtils.putPixel(
				w1 / 2,
				0,
				randomRGB(inPixels, w1 / 2, 0, useImageColors, originalSpace,
						randomGenerator), outPixels, width);
		RGBUtils.putPixel(
				w1 / 2,
				h1,
				randomRGB(inPixels, w1 / 2, h1, useImageColors, originalSpace,
						randomGenerator), outPixels, width);
		int depth = 1;
		while (doPlasmaPixel(0, 0, width - 1, height - 1, outPixels, width,
				depth, 0, turbulence))
			depth++;

		if (useColormap && colormap != null) {
			int index = 0;
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					outPixels[index] = colormap
							.getColor((outPixels[index] & 0xff) / 255.0f);
					index++;
				}
			}
		}
		src.setPixels(outPixels, src.getWidth(), src.getHeight());
	}

	private static int randomRGB(final int[] inPixels, final int x,
			final int y, final boolean useImageColors,
			final Rectangle originalSpace, final Random randomGenerator) {
		if (useImageColors) {
			return inPixels[y * (int) originalSpace.getWidth() + x];
		} else {
			final int r = (int) (255 * randomGenerator.nextFloat());
			final int g = (int) (255 * randomGenerator.nextFloat());
			final int b = (int) (255 * randomGenerator.nextFloat());
			return 0xff000000 | (r << 16) | (g << 8) | b;
		}
	}

	/**
	 * Light effect in at a given point. Implemented in native code.
	 * 
	 * @param bitmap
	 * @param centerX
	 *            of light
	 * @param centerY
	 *            of light
	 * @param radius
	 *            radius of the light ray
	 */
	public static void lightNative(final ToToBitmap bitmap, final int centerX,
			final int centerY, final int radius) {
		int[] pixels = bitmap.getPixels();
		pixels = NativeFilters.lightFilter(pixels, bitmap.getFilterX(),
				bitmap.getFilterY(), bitmap.getFilterWidth(),
				bitmap.getFilterHeight(), bitmap.getWidth(),
				bitmap.getHeight(), centerX, centerY, radius);
		bitmap.setPixels(pixels, bitmap.getWidth(), bitmap.getHeight());
	}
}
