/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;

import toto.graphics.color.RGBUtils;
import toto.math.MathUtils;

/**
 * A colormap which interpolates linearly between two colors.
 */
class LinearColormap implements Colormap {

	private int color1;
	private int color2;

	/**
	 * Construct a color map with a grayscale ramp from black to white.
	 */
	LinearColormap() {
		this(0xff000000, 0xffffffff);
	}

	/**
	 * Construct a linear color map.
	 * 
	 * @param color1
	 *            the color corresponding to value 0 in the colormap
	 * @param color2
	 *            the color corresponding to value 1 in the colormap
	 */
	LinearColormap(final int color1, final int color2) {
		this.color1 = color1;
		this.color2 = color2;
	}

	/**
	 * Set the first color.
	 * 
	 * @param color1
	 *            the color corresponding to value 0 in the colormap
	 */
	void setColor1(final int color1) {
		this.color1 = color1;
	}

	/**
	 * Get the first color.
	 * 
	 * @return the color corresponding to value 0 in the colormap
	 */
	int getColor1() {
		return color1;
	}

	/**
	 * Set the second color.
	 * 
	 * @param color2
	 *            the color corresponding to value 1 in the colormap
	 */
	void setColor2(final int color2) {
		this.color2 = color2;
	}

	/**
	 * Get the second color.
	 * 
	 * @return the color corresponding to value 1 in the colormap
	 */
	int getColor2() {
		return color2;
	}

	/**
	 * Convert a value in the range 0..1 to an RGB color.
	 * 
	 * @param v
	 *            a value in the range 0..1
	 * @return an RGB color
	 */
	@Override
	public int getColor(final float v) {
		return RGBUtils.mixColors(MathUtils.clamp(v, 0, 1.0f), color1, color2);
	}

}
