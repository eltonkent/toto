package toto.bitmap.filters.crypto;

import android.graphics.Color;
import com.mi.toto.Conditions;
import toto.bitmap.ToToBitmap;
import toto.lang.ByteUtils;

import java.util.ArrayList;

/**
 * LSB(Least Significant Bit) <a
 * href="http://en.wikipedia.org/wiki/Steganography">Steganography</a>
 * implementation.
 */
public class Steganography {
	private static String END_MESSAGE_COSTANT = "#!@";
	private static String START_MESSAGE_COSTANT = "@!#";
	private static int[] binary = { 16, 8, 0 };
	private static byte[] andByte = { (byte) 0xC0, 0x30, 0x0C, 0x03 };
	private static int[] toShift = { 6, 4, 2, 0 };

	/**
	 * Stegnograpgh encoding
	 * 
	 * @param bitmap
	 * @param str
	 */
	public static void encodeMessage(final ToToBitmap bitmap, final String str) {
		encodeMessage(bitmap, str, START_MESSAGE_COSTANT, END_MESSAGE_COSTANT);
	}

	/**
	 * Stegnograph encoding
	 * 
	 * @param bitmap
	 * @param str
	 *            Message to encode.
	 * @param startDelimiter
	 *            String to represent the starting of the encoded text
	 * @param endDelimiter
	 *            String to represent the end of the encoded text
	 * @return Encoded message image pixels as byte[].
	 * @throws IllegalArgumentException
	 *             if <code>str</code> is too big for the given image
	 */
	public static void encodeMessage(final ToToBitmap bitmap, String str,
			final String startDelimiter, final String endDelimiter) {

		str += endDelimiter;
		str = startDelimiter + str;
		final byte[] msg = str.getBytes();
		final int channels = 3;
		int shiftIndex = 4;
		final int w = bitmap.getWidth();
		final int h = bitmap.getHeight();
		int msgIndex = 0;
		boolean msgEnded = false;
		Conditions.checkArgument(((w * h) >= (msg.length * 8)),
				"Provided text is too big for the given image");

		int x, y, pixel, channelIndex;
		final byte[] encPixel = new byte[3];

		for (y = 0; y < h; y++) {
			for (x = 0; x < w; x++) {
				pixel = bitmap.getPixel(x, y);
				for (channelIndex = 0; channelIndex < channels; channelIndex++) {
					if (!msgEnded) {
						encPixel[channelIndex] = (byte) ((((pixel >> binary[channelIndex]) & 0xFF) & 0xFC) | ((msg[msgIndex] >> toShift[(shiftIndex++)
								% toShift.length]) & 0x3));// 6
						if (shiftIndex % toShift.length == 0) {
							msgIndex++;
						}
						if (msgIndex == msg.length) {
							msgEnded = true;
						}
					} else {
						encPixel[channelIndex] = (byte) ((((pixel >> binary[channelIndex]) & 0xFF)));
					}
				}
				pixel = ByteUtils.toInt(encPixel, 0);
				pixel = Color.argb(0xFF, pixel >> 16 & 0xFF, pixel >> 8 & 0xFF,
						pixel & 0xFF);
				bitmap.setPixel(x, y, pixel);
			}
		}
	}

	public static String decodeMessage(final ToToBitmap bitmap) {
		return decodeMessage(bitmap, START_MESSAGE_COSTANT, END_MESSAGE_COSTANT);
	}

	public static String decodeMessage(final ToToBitmap bitmap,
			final String startDelimiter, final String endDelimiter) {
		final ArrayList<Byte> v = new ArrayList<Byte>();
		String builder = "";
		int shiftIndex = 4;
		byte tmp = 0x00;
		final int channels = 3;
		final int w = bitmap.getWidth();
		final int h = bitmap.getHeight();
		final byte[] decPixel = new byte[3];
		int x, y, pixel, channelIndex;
		boolean broken = false;
		out: if (!broken) {
			for (y = 0; y < h; y++) {
				for (x = 0; x < w; x++) {
					pixel = bitmap.getPixel(x, y);
					decPixel[0] = (byte) ((pixel >> 16) & 0xFF);
					decPixel[1] = (byte) ((pixel >> 8) & 0xFF);
					decPixel[2] = (byte) ((pixel) & 0xFF);

					for (channelIndex = 0; channelIndex < channels; channelIndex++) {
						tmp = (byte) (tmp | ((decPixel[channelIndex] << toShift[shiftIndex
								% toShift.length]) & andByte[shiftIndex++
								% toShift.length]));
						if (shiftIndex % toShift.length == 0) {
							v.add(new Byte(tmp));
							final byte[] nonso = { (v.get(v.size() - 1))
									.byteValue() };
							final String str = new String(nonso);
							// if (END_MESSAGE_COSTANT.equals(str)) {
							if (builder.endsWith(endDelimiter)) {
								break out;
							} else {
								builder = builder + str;
								if (builder.length() == startDelimiter.length()
										&& !startDelimiter.equals(builder)) {
									builder = null;
									broken = true;
									break out;
								}
							}
							tmp = 0x00;
						}
					}
				}
			}
		}

		if (builder != null)
			builder = builder.substring(startDelimiter.length(),
					builder.length() - endDelimiter.length());
		return builder;
	}

}
