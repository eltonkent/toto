/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;

import toto.geom2d.Point;
import toto.geom2d.Rectangle;
import toto.bitmap.utils.BitmapUtils;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

/**
 * Filters that transform the shape of the bitmap
 * <p>
 * <b>Rotate</b><br/>
 * Rotation with the <code>angle</code> of 2.<br/>
 * <img src="../../../../resources/rotate.png" ><br/>
 * </p>
 * 
 * @author elton.stephen.kent
 */
public class ShapeTransformFilters extends TransformFilters {

	/**
	 * Create a twirl effect .
	 * 
	 * @param bitmap
	 * @param centreX
	 *            the X coordinate of the twirl effect. the coordinates are
	 *            represented as a float. for instance 0.5 represents the centre
	 *            of the bitmap.
	 * @param centreY
	 *            the Y coordinate of the twirl effect. the coordinates are
	 *            represented as a float. for instance 0.5 represents the centre
	 *            of the bitmap
	 * @param angle
	 *            can be anything between -710 to 720. negative angles are used
	 *            for clockwise twirl rotation.
	 * @param radius
	 *            of the twirl
	 * @param edgeAction
	 *            for the effect. {@link TransformFilters#EDGE_ACTION_CLAMP},
	 *            {@link TransformFilters#EDGE_ACTION_RGB_CLAMP},
	 *            {@link TransformFilters#EDGE_ACTION_WRAP} and
	 *            {@link TransformFilters#EDGE_ACTION_ZERO}. recommended:
	 *            {@link TransformFilters#EDGE_ACTION_CLAMP}
	 * @param outputConfig
	 * @return
	 */
	public static Bitmap twirl(final Bitmap bitmap, final float centreX,
			final float centreY, final float angle, float radius,
			final int edgeAction, final Config outputConfig) {
		final int width = bitmap.getWidth();
		final int height = bitmap.getHeight();
		final float icentreX = width * centreX;
		final float icentreY = height * centreY;
		if (radius == 0)
			radius = Math.min(icentreX, icentreY);
		final float radius2 = radius * radius;

		final Rectangle transformedSpace = new Rectangle(0, 0, width, height);
		final int[] inPixels = BitmapUtils.getPixels(bitmap);
		final int srcWidth = width;
		final int srcHeight = height;
		final int srcWidth1 = width - 1;
		final int srcHeight1 = height - 1;
		final int outWidth = (int) transformedSpace.getWidth();
		final int outHeight = (int) transformedSpace.getHeight();
		final int[] outPixels = new int[outWidth];
		final int[] destPixels = new int[outHeight * outWidth];
		final float[] out = new float[2];

		float dx, dy, distance, a;
		for (int y = 0; y < outHeight; y++) {
			for (int x = 0; x < outWidth; x++) {

				/**
				 * transform inverse
				 */
				dx = x - icentreX;
				dy = y - icentreY;
				distance = dx * dx + dy * dy;
				if (distance > radius2) {
					out[0] = x;
					out[1] = y;
				} else {
					distance = (float) Math.sqrt(distance);
					a = (float) Math.atan2(dy, dx) + angle
							* (radius - distance) / radius;
					out[0] = icentreX + distance * (float) Math.cos(a);
					out[1] = icentreY + distance * (float) Math.sin(a);
				}

				final int srcX = (int) Math.floor(out[0]);
				final int srcY = (int) Math.floor(out[1]);
				final float xWeight = out[0] - srcX;
				final float yWeight = out[1] - srcY;
				int nw, ne, sw, se;

				if (srcX >= 0 && srcX < srcWidth1 && srcY >= 0
						&& srcY < srcHeight1) {
					// Easy case, all corners are in the image
					final int i = srcWidth * srcY + srcX;
					nw = inPixels[i];
					ne = inPixels[i + 1];
					sw = inPixels[i + srcWidth];
					se = inPixels[i + srcWidth + 1];
				} else {
					// Some of the corners are off the image
					nw = TransformFilters.getPixel(inPixels, srcX, srcY,
							srcWidth, srcHeight, edgeAction);
					ne = TransformFilters.getPixel(inPixels, srcX + 1, srcY,
							srcWidth, srcHeight, edgeAction);
					sw = TransformFilters.getPixel(inPixels, srcX, srcY + 1,
							srcWidth, srcHeight, edgeAction);
					se = TransformFilters.getPixel(inPixels, srcX + 1,
							srcY + 1, srcWidth, srcHeight, edgeAction);
				}
				outPixels[x] = ImageMath.bilinearInterpolate(xWeight, yWeight,
						nw, ne, sw, se);
			}
			// setRGB(dst, 0, y, transformedSpace.width, 1, outPixels);
			BitmapUtils.setPixelRow(outPixels, y, outWidth, destPixels);
		}
		return Bitmap.createBitmap(destPixels, outWidth, outHeight,
				outputConfig);
	}

	// private static void transformInverse(int x, int y, float[] out, float
	// sin, float cos) {
	// out[0] = (x * cos) - (y * sin);
	// out[1] = (y * cos) + (x * sin);
	// }

	private static void transformRotateSpace(final Rectangle rect,
			final boolean resize, final float sin, final float cos) {
		if (resize) {
			final Point out = new Point(0, 0);
			int minx = Integer.MAX_VALUE;
			int miny = Integer.MAX_VALUE;
			int maxx = Integer.MIN_VALUE;
			int maxy = Integer.MIN_VALUE;
			final int w = (int) rect.getWidth();
			final int h = (int) rect.getHeight();
			final int x = (int) rect.getX();
			final int y = (int) rect.getY();

			for (int i = 0; i < 4; i++) {
				switch (i) {
				case 0:
					transform(x, y, out, sin, cos);
					break;
				case 1:
					transform(x + w, y, out, sin, cos);
					break;
				case 2:
					transform(x, y + h, out, sin, cos);
					break;
				case 3:
					transform(x + w, y + h, out, sin, cos);
					break;
				}
				minx = Math.min(minx, (int) out.getX());
				miny = Math.min(miny, (int) out.getY());
				maxx = Math.max(maxx, (int) out.getX());
				maxy = Math.max(maxy, (int) out.getY());
			}

			rect.setX(minx);
			rect.setY(miny);
			rect.setWidth((float) (maxx - rect.getX()));
			rect.setHeight((float) (maxy - rect.getY()));
		}
	}

	/**
	 * Rotate the bitmap by the given angle
	 * 
	 * @param src
	 * @param angle
	 * @param resize
	 *            allow resizing of the bitmap
	 * @param edgeAction
	 *            recommended : {@link #EDGE_ACTION_RGB_CLAMP}
	 * @param outputConfig
	 * @return
	 */
	public static Bitmap rotate(final Bitmap src, final float angle,
			final boolean resize, final int edgeAction,
			final Bitmap.Config outputConfig) {
		final int width = src.getWidth();
		final int height = src.getHeight();
		float cos, sin;
		cos = (float) Math.cos(angle);
		sin = (float) Math.sin(angle);

		final Rectangle transformedSpace = new Rectangle(0, 0, width, height);
		transformRotateSpace(transformedSpace, resize, sin, cos);

		final int[] inPixels = BitmapUtils.getPixels(src);// getRGB(src, 0, 0,
		// width,
		// height, null);

		final int srcWidth = width;
		final int srcHeight = height;
		final int srcWidth1 = width - 1;
		final int srcHeight1 = height - 1;
		final int outWidth = (int) transformedSpace.getWidth();
		final int outHeight = (int) transformedSpace.getHeight();
		int outX, outY;
		final int[] outPixels = new int[outWidth];
		final int[] destPixels = new int[outWidth * outHeight];

		outX = (int) transformedSpace.getX();
		outY = (int) transformedSpace.getY();
		final float[] out = new float[2];

		for (int y = 0; y < outHeight; y++) {
			for (int x = 0; x < outWidth; x++) {
				// transformInverse(outX + x, outY + y, out, sin, cos);

				out[0] = (outX + x * cos) - (outY + y * sin);
				out[1] = (outY + y * cos) + (outX + x * sin);

				final int srcX = (int) Math.floor(out[0]);
				final int srcY = (int) Math.floor(out[1]);
				final float xWeight = out[0] - srcX;
				final float yWeight = out[1] - srcY;
				int nw, ne, sw, se;

				if (srcX >= 0 && srcX < srcWidth1 && srcY >= 0
						&& srcY < srcHeight1) {
					// Easy case, all corners are in the image
					final int i = srcWidth * srcY + srcX;
					nw = inPixels[i];
					ne = inPixels[i + 1];
					sw = inPixels[i + srcWidth];
					se = inPixels[i + srcWidth + 1];
				} else {
					// Some of the corners are off the image
					nw = getPixel(inPixels, srcX, srcY, srcWidth, srcHeight,
							edgeAction);
					ne = getPixel(inPixels, srcX + 1, srcY, srcWidth,
							srcHeight, edgeAction);
					sw = getPixel(inPixels, srcX, srcY + 1, srcWidth,
							srcHeight, edgeAction);
					se = getPixel(inPixels, srcX + 1, srcY + 1, srcWidth,
							srcHeight, edgeAction);
				}
				outPixels[x] = ImageMath.bilinearInterpolate(xWeight, yWeight,
						nw, ne, sw, se);
			}
			BitmapUtils.setPixelRow(outPixels, y,
					(int) transformedSpace.getWidth(), destPixels);
			// setRGB(dst, 0, y, transformedSpace.width, 1, outPixels);
		}
		return Bitmap.createBitmap(destPixels,
				(int) transformedSpace.getWidth(),
				(int) transformedSpace.getHeight(), outputConfig);
	}

	private static void transform(final int x, final int y, final Point out,
			final float sin, final float cos) {
		out.setLocation(((x * cos) + (y * sin)), ((y * cos) - (x * sin)));
	}

	protected static float[] transformShearSpace(final Rectangle r,
			final float xangle, final float yangle) {
		float tangent = (float) Math.tan(xangle);
		final float xoffset = (float) -r.getHeight() * tangent;
		if (tangent < 0.0)
			tangent = -tangent;
		r.setWidth((float) (r.getHeight() * tangent + r.getWidth() + 0.999999f));
		tangent = (float) Math.tan(yangle);
		final float yoffset = (float) -r.getWidth() * tangent;
		if (tangent < 0.0)
			tangent = -tangent;
		r.setHeight((float) (r.getWidth() * tangent + r.getHeight() + 0.999999f));
		return new float[] { xoffset, yoffset };
	}

	public static Bitmap shear(final Bitmap src, final float xangle,
			final float yangle, final int edgeAction, final Config outputConfig) {
		final float shx = (float) Math.sin(xangle);
		final float shy = (float) Math.sin(yangle);

		final int width = src.getWidth();
		final int height = src.getHeight();

		// Rectangle originalSpace = new Rectangle(0, 0, width, height);
		final Rectangle transformedSpace = new Rectangle(0, 0, width, height);
		final float[] offset = transformShearSpace(transformedSpace, xangle,
				yangle);
		final float xoffset = offset[0];
		final float yoffset = offset[1];
		final int[] inPixels = BitmapUtils.getPixels(src);

		final int srcWidth = width;
		final int srcHeight = height;
		final int srcWidth1 = width - 1;
		final int srcHeight1 = height - 1;
		final int outWidth = (int) transformedSpace.getWidth();
		final int outHeight = (int) transformedSpace.getHeight();
		int outX, outY, srcX, srcY;
		float xWeight, yWeight;
		// int index = 0;
		final int[] outPixels = new int[outWidth];
		final int[] destPixels = new int[outWidth * outHeight];
		outX = (int) transformedSpace.getX();
		outY = (int) transformedSpace.getY();
		final float[] out = new float[2];

		for (int y = 0; y < outHeight; y++) {
			for (int x = 0; x < outWidth; x++) {
				transformInverse(outX + x, outY + y, out, xoffset, yoffset,
						shx, shy);
				srcX = (int) Math.floor(out[0]);
				srcY = (int) Math.floor(out[1]);
				xWeight = out[0] - srcX;
				yWeight = out[1] - srcY;
				int nw, ne, sw, se;

				if (srcX >= 0 && srcX < srcWidth1 && srcY >= 0
						&& srcY < srcHeight1) {
					// Easy case, all corners are in the image
					final int i = srcWidth * srcY + srcX;
					nw = inPixels[i];
					ne = inPixels[i + 1];
					sw = inPixels[i + srcWidth];
					se = inPixels[i + srcWidth + 1];
				} else {
					// Some of the corners are off the image
					nw = getPixel(inPixels, srcX, srcY, srcWidth, srcHeight,
							edgeAction);
					ne = getPixel(inPixels, srcX + 1, srcY, srcWidth,
							srcHeight, edgeAction);
					sw = getPixel(inPixels, srcX, srcY + 1, srcWidth,
							srcHeight, edgeAction);
					se = getPixel(inPixels, srcX + 1, srcY + 1, srcWidth,
							srcHeight, edgeAction);
				}
				outPixels[x] = ImageMath.bilinearInterpolate(xWeight, yWeight,
						nw, ne, sw, se);
			}
			BitmapUtils.setPixelRow(outPixels, y,
					(int) transformedSpace.getWidth(), destPixels);
			// setRGB(dst, 0, y, transformedSpace.width, 1, outPixels);
		}
		return Bitmap.createBitmap(destPixels,
				(int) transformedSpace.getWidth(),
				(int) transformedSpace.getHeight(), outputConfig);
	}

	private static void transformInverse(final int x, final int y,
			final float[] out, final float xoffset, final float yoffset,
			final float shx, final float shy) {
		out[0] = x + xoffset + (y * shx);
		out[1] = y + yoffset + (x * shy);
	}

}
