/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.filters;

/**
 * An interface for color maps. These are passed to filters which convert gray
 * values to colors. This is similar to the ColorModel class but works with
 * floating point values.
 */
interface Colormap {
	/**
	 * Convert a value in the range 0..1 to an RGB color.
	 * 
	 * @param v
	 *            a value in the range 0..1
	 * @return an RGB color
	 */
	public int getColor(float v);
}
