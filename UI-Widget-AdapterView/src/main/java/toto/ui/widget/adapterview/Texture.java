/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.adapterview;

import static javax.microedition.khronos.opengles.GL10.GL_CLAMP_TO_EDGE;
import static javax.microedition.khronos.opengles.GL10.GL_LINEAR;
import static javax.microedition.khronos.opengles.GL10.GL_RGB;
import static javax.microedition.khronos.opengles.GL10.GL_RGBA;
import static javax.microedition.khronos.opengles.GL10.GL_TEXTURE_2D;
import static javax.microedition.khronos.opengles.GL10.GL_TEXTURE_MAG_FILTER;
import static javax.microedition.khronos.opengles.GL10.GL_TEXTURE_MIN_FILTER;
import static javax.microedition.khronos.opengles.GL10.GL_TEXTURE_WRAP_S;
import static javax.microedition.khronos.opengles.GL10.GL_TEXTURE_WRAP_T;
import static javax.microedition.khronos.opengles.GL10.GL_UNSIGNED_BYTE;
import static javax.microedition.khronos.opengles.GL10.GL_UNSIGNED_SHORT_4_4_4_4;
import static javax.microedition.khronos.opengles.GL10.GL_UNSIGNED_SHORT_5_6_5;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.opengl.GLUtils;

class Texture {

	static boolean isValidTexture(final Texture t) {
		return t != null && !t.isDestroyed();
	}

	private FlipRenderer renderer;

	private final int[] id = { 0 };

	private int width, height;
	private int contentWidth, contentHeight;

	private boolean destroyed = false;

	private Texture() {
	}

	public static Texture createTexture(final Bitmap bitmap,
			final FlipRenderer renderer, final GL10 gl) {
		final Texture t = new Texture();
		t.renderer = renderer;

		final int potW = Integer.highestOneBit(bitmap.getWidth() - 1) << 1;
		final int potH = Integer.highestOneBit(bitmap.getHeight() - 1) << 1;

		t.contentWidth = bitmap.getWidth();
		t.contentHeight = bitmap.getHeight();
		t.width = potW;
		t.height = potH;

		gl.glGenTextures(1, t.id, 0);
		gl.glBindTexture(GL_TEXTURE_2D, t.id[0]);

		gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		switch (bitmap.getConfig()) {
		case ARGB_8888:
			gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, potW, potH, 0, GL_RGBA,
					GL_UNSIGNED_BYTE, null);
			GLUtils.texSubImage2D(GL_TEXTURE_2D, 0, 0, 0, bitmap);
			break;
		case ARGB_4444:
			gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, potW, potH, 0, GL_RGBA,
					GL_UNSIGNED_SHORT_4_4_4_4, null);
			GLUtils.texSubImage2D(GL_TEXTURE_2D, 0, 0, 0, bitmap);
			break;
		case RGB_565:
			gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, potW, potH, 0, GL_RGB,
					GL_UNSIGNED_SHORT_5_6_5, null);
			GLUtils.texSubImage2D(GL_TEXTURE_2D, 0, 0, 0, bitmap);
			break;
		case ALPHA_8:
		default:
			throw new RuntimeException(
					"Unrecognized bitmap format for OpenGL texture: "
							+ bitmap.getConfig());
		}

		return t;
	}

	void postDestroy() {
		renderer.postDestroyTexture(this);
	}

	void destroy(final GL10 gl) {
		if (id[0] != 0) {
			gl.glDeleteTextures(1, id, 0);
		}

		id[0] = 0;
		destroyed = true;
	}

	boolean isDestroyed() {
		return destroyed;
	}

	int[] getId() {
		return id;
	}

	int getWidth() {
		return width;
	}

	int getHeight() {
		return height;
	}

	int getContentWidth() {
		return contentWidth;
	}

	int getContentHeight() {
		return contentHeight;
	}
}
