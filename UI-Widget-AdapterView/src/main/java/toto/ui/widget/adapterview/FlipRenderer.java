/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.adapterview;

import static javax.microedition.khronos.opengles.GL10.GL_AMBIENT;
import static javax.microedition.khronos.opengles.GL10.GL_COLOR_BUFFER_BIT;
import static javax.microedition.khronos.opengles.GL10.GL_DEPTH_BUFFER_BIT;
import static javax.microedition.khronos.opengles.GL10.GL_DEPTH_TEST;
import static javax.microedition.khronos.opengles.GL10.GL_LEQUAL;
import static javax.microedition.khronos.opengles.GL10.GL_LIGHT0;
import static javax.microedition.khronos.opengles.GL10.GL_LIGHTING;
import static javax.microedition.khronos.opengles.GL10.GL_MODELVIEW;
import static javax.microedition.khronos.opengles.GL10.GL_NICEST;
import static javax.microedition.khronos.opengles.GL10.GL_PERSPECTIVE_CORRECTION_HINT;
import static javax.microedition.khronos.opengles.GL10.GL_POSITION;
import static javax.microedition.khronos.opengles.GL10.GL_PROJECTION;
import static javax.microedition.khronos.opengles.GL10.GL_SMOOTH;

import java.util.LinkedList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import toto.geom2d.Angle;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.view.View;

/**
 * @see toto.ui.widget.adapterview.FlipView
 */
class FlipRenderer implements GLSurfaceView.Renderer {

	private final FlipView flipViewController;

	private final FlipCardHandler cards;

	private boolean created = false;

	private final LinkedList<Texture> postDestroyTextures = new LinkedList<Texture>();

	FlipRenderer(final FlipView flipViewController, final FlipCardHandler cards) {
		this.flipViewController = flipViewController;
		this.cards = cards;
	}

	@Override
	public void onSurfaceCreated(final GL10 gl, final EGLConfig config) {
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		gl.glShadeModel(GL_SMOOTH);
		gl.glClearDepthf(1.0f);
		gl.glEnable(GL_DEPTH_TEST);
		gl.glDepthFunc(GL_LEQUAL);
		gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		created = true;

		cards.invalidateTexture();
		flipViewController.reloadTexture();
	}

	static float[] light0Position = { 0, 0, 100f, 0f };

	@Override
	public void onSurfaceChanged(final GL10 gl, final int width,
			final int height) {
		gl.glViewport(0, 0, width, height);

		gl.glMatrixMode(GL_PROJECTION);
		gl.glLoadIdentity();

		final float fovy = 20f;
		final float eyeZ = height / 2f
				/ (float) Math.tan(Angle.convertToRadians(fovy / 2));

		GLU.gluPerspective(gl, fovy, (float) width / (float) height, 0.5f,
				Math.max(2500.0f, eyeZ));

		gl.glMatrixMode(GL_MODELVIEW);
		gl.glLoadIdentity();

		GLU.gluLookAt(gl, width / 2.0f, height / 2f, eyeZ, width / 2.0f,
				height / 2.0f, 0.0f, 0.0f, 1.0f, 0.0f);

		gl.glEnable(GL_LIGHTING);
		gl.glEnable(GL_LIGHT0);

		final float lightAmbient[] = new float[] { 3.5f, 3.5f, 3.5f, 1f };
		gl.glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient, 0);

		light0Position = new float[] { 0, 0, eyeZ, 0f };
		gl.glLightfv(GL_LIGHT0, GL_POSITION, light0Position, 0);

	}

	@Override
	public void onDrawFrame(final GL10 gl) {
		gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		synchronized (postDestroyTextures) {
			for (final Texture texture : postDestroyTextures)
				texture.destroy(gl);
			postDestroyTextures.clear();
		}

		cards.draw(this, gl);
	}

	void postDestroyTexture(final Texture texture) {
		synchronized (postDestroyTextures) {
			postDestroyTextures.add(texture);
		}
	}

	void updateTexture(final int frontIndex, final View frontView,
			final int backIndex, final View backView) {
		if (created) {
			cards.reloadTexture(frontIndex, frontView, backIndex, backView);
			flipViewController.getSurfaceView().requestRender();
		}
	}

}
