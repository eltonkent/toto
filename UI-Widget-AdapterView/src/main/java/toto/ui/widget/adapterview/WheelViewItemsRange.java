/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.adapterview;

/**
 * Range for visible items.
 */
class WheelViewItemsRange {
	// Items count
	private final int count;

	// First item number
	private final int first;

	/**
	 * Default constructor. Creates an empty range
	 */
	WheelViewItemsRange() {
		this(0, 0);
	}

	/**
	 * Constructor
	 * 
	 * @param first
	 *            the number of first item
	 * @param count
	 *            the count of items
	 */
	WheelViewItemsRange(final int first, final int count) {
		this.first = first;
		this.count = count;
	}

	/**
	 * Tests whether item is contained by range
	 * 
	 * @param index
	 *            the item number
	 * @return true if item is contained
	 */
	boolean contains(final int index) {
		return index >= getFirst() && index <= getLast();
	}

	/**
	 * Get items count
	 * 
	 * @return the count of items
	 */
	int getCount() {
		return count;
	}

	/**
	 * Gets number of first item
	 * 
	 * @return the number of the first item
	 */
	int getFirst() {
		return first;
	}

	/**
	 * Gets number of last item
	 * 
	 * @return the number of last item
	 */
	int getLast() {
		return getFirst() + getCount() - 1;
	}
}
