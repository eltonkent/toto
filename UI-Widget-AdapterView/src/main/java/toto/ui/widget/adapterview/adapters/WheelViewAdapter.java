/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.adapterview.adapters;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;

/**
 * Wheel items adapter interface
 */
public interface WheelViewAdapter {
	/**
	 * Get a View that displays an empty wheel item placed before the first or
	 * after the last wheel item.
	 * 
	 * @param convertView
	 *            the old view to reuse if possible
	 * @param parent
	 *            the parent that this view will eventually be attached to
	 * @return the empty item View
	 */
	public View getEmptyItem(View convertView, ViewGroup parent);

	/**
	 * Get a View that displays the data at the specified position in the data
	 * setKey
	 * 
	 * @param index
	 *            the item index
	 * @param convertView
	 *            the old view to reuse if possible
	 * @param parent
	 *            the parent that this view will eventually be attached to
	 * @return the wheel item View
	 */
	public View getItem(int index, View convertView, ViewGroup parent);

	/**
	 * Gets items count
	 * 
	 * @return the count of wheel items
	 */
	public int getItemsCount();

	/**
	 * Register an observer that is called when changes happen to the data used
	 * by this adapter.
	 * 
	 * @param observer
	 *            the observer to be registered
	 */
	public void registerDataSetObserver(DataSetObserver observer);

	/**
	 * Unregister an observer that has previously been registered
	 * 
	 * @param observer
	 *            the observer to be unregistered
	 */
	void unregisterDataSetObserver(DataSetObserver observer);
}
