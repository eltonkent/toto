/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.adapterview;

import javax.microedition.khronos.opengles.GL10;

import android.view.MotionEvent;
import android.view.View;

class FlipCardHandler {
	private final float acceleration;// = 0.15f;
	private final float movementRate;// = 1.5f;
	private final int maxTipAngle;// = 60;
	private final int maxTouchMoveAngle;// = 15;
	private final float minMovement;// = 4f;

	private static final int STATE_INIT = 0;
	private static final int STATE_TOUCH = 1;
	private static final int STATE_AUTO_ROTATE = 2;

	private ViewDualCards frontCards;
	private ViewDualCards backCards;

	private float accumulatedAngle = 0f;
	private boolean forward = true;
	private int animatedFrame = 0;
	private int state = STATE_INIT;

	private boolean orientationVertical = true;
	private float lastPosition = -1;

	private final FlipView controller;

	private boolean visible = false;

	private int maxIndex = 0;

	private int lastPageIndex;

	FlipCardHandler(final FlipView controller,
			final boolean orientationVertical, final float acceleration,
			final float movementRate, final int maxTipAngle,
			final float minMovement, final int touchMoveAngle) {
		this.controller = controller;

		frontCards = new ViewDualCards(orientationVertical);
		backCards = new ViewDualCards(orientationVertical);
		this.orientationVertical = orientationVertical;

		this.acceleration = acceleration;
		this.movementRate = movementRate;
		this.maxTipAngle = maxTipAngle;
		this.minMovement = minMovement;
		this.maxTouchMoveAngle = touchMoveAngle;
	}

	boolean isVisible() {
		return visible;
	}

	void setVisible(final boolean visible) {
		this.visible = visible;
	}

	boolean refreshPageView(final View view) {
		boolean match = false;
		if (frontCards.getView() == view) {
			frontCards.resetWithIndex(frontCards.getIndex());
			match = true;
		}
		if (backCards.getView() == view) {
			backCards.resetWithIndex(backCards.getIndex());
			match = true;
		}

		return match;
	}

	boolean refreshPage(final int pageIndex) {
		boolean match = false;
		if (frontCards.getIndex() == pageIndex) {
			frontCards.resetWithIndex(pageIndex);
			match = true;
		}
		if (backCards.getIndex() == pageIndex) {
			backCards.resetWithIndex(pageIndex);
			match = true;
		}

		return match;
	}

	void refreshAllPages() {
		frontCards.resetWithIndex(frontCards.getIndex());
		backCards.resetWithIndex(backCards.getIndex());
	}

	void reloadTexture(final int frontIndex, final View frontView,
			final int backIndex, final View backView) {
		synchronized (this) {
			frontCards.loadView(frontIndex, frontView,
					controller.getAnimationBitmapFormat());
			backCards.loadView(backIndex, backView,
					controller.getAnimationBitmapFormat());

		}
	}

	synchronized void resetSelection(final int selection, final int maxIndex) {

		// stop flip animation when selection is manually changed

		this.maxIndex = maxIndex;
		setVisible(false);
		setState(STATE_INIT);
		accumulatedAngle = selection * 180;
		frontCards.resetWithIndex(selection);
		backCards.resetWithIndex(selection + 1 < maxIndex ? selection + 1 : -1);
		controller.postHideFlipAnimation();
	}

	synchronized void draw(final FlipRenderer renderer, final GL10 gl) {
		frontCards.buildTexture(renderer, gl);
		backCards.buildTexture(renderer, gl);

		if (!Texture.isValidTexture(frontCards.getTexture())
				&& !Texture.isValidTexture(backCards.getTexture()))
			return;

		if (!visible)
			return;

		switch (state) {
		case STATE_INIT:
		case STATE_TOUCH:
			break;
		case STATE_AUTO_ROTATE: {
			animatedFrame++;
			final float delta = (forward ? acceleration : -acceleration)
					* animatedFrame % 180;

			final float oldAngle = accumulatedAngle;

			accumulatedAngle += delta;

			if (oldAngle < 0) { // bouncing back after flip backward and over
				// the first page
				if (accumulatedAngle >= 0) {
					accumulatedAngle = 0;
					setState(STATE_INIT);
				}
			} else {
				if (frontCards.getIndex() == maxIndex - 1
						&& oldAngle > frontCards.getIndex() * 180) { // bouncing
					// back
					// after
					// flip
					// forward
					// and
					// over
					// the
					// last
					// page
					if (accumulatedAngle <= frontCards.getIndex() * 180) {
						setState(STATE_INIT);
						accumulatedAngle = frontCards.getIndex() * 180;
					}
				} else {
					if (forward) {
						if (accumulatedAngle >= backCards.getIndex() * 180) { // moved
							// to
							// the
							// next
							// page
							accumulatedAngle = backCards.getIndex() * 180;
							setState(STATE_INIT);
							controller.postFlippedToView(backCards.getIndex());

							swapCards();
							backCards.resetWithIndex(frontCards.getIndex() + 1);
						}
					} else { // backward
						if (accumulatedAngle <= frontCards.getIndex() * 180) { // firstCards
							// restored
							accumulatedAngle = frontCards.getIndex() * 180;
							setState(STATE_INIT);
						}
					}
				}
			} // ends of `if (oldAngle < 0) {} else {}`

			if (state == STATE_INIT)
				controller.postHideFlipAnimation();
			else
				controller.getSurfaceView().requestRender();
		}
			break;
		default:
			break;
		}

		final float angle = getDisplayAngle();
		if (angle < 0) {
			frontCards.getTopCard().setAxis(FlipCard.AXIS_BOTTOM);
			frontCards.getTopCard().setAngle(-angle);
			frontCards.getTopCard().draw(gl);

			frontCards.getBottomCard().setAngle(0);
			frontCards.getBottomCard().draw(gl);

			// no need to draw backCards here
		} else {
			if (angle < 90) { // render front view over back view
				frontCards.getTopCard().setAngle(0);
				frontCards.getTopCard().draw(gl);

				backCards.getBottomCard().setAngle(0);
				backCards.getBottomCard().draw(gl);

				frontCards.getBottomCard().setAxis(FlipCard.AXIS_TOP);
				frontCards.getBottomCard().setAngle(angle);
				frontCards.getBottomCard().draw(gl);
			} else { // render back view first
				frontCards.getTopCard().setAngle(0);
				frontCards.getTopCard().draw(gl);

				backCards.getTopCard().setAxis(FlipCard.AXIS_BOTTOM);
				backCards.getTopCard().setAngle(180 - angle);
				backCards.getTopCard().draw(gl);

				backCards.getBottomCard().setAngle(0);
				backCards.getBottomCard().draw(gl);
			}
		}
	}

	void invalidateTexture() {
		frontCards.abandonTexture();
		backCards.abandonTexture();
	}

	synchronized boolean handleTouchEvent(final MotionEvent event,
			final boolean isOnTouchEvent) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			// remember page we started on...
			lastPageIndex = getPageIndexFromAngle(accumulatedAngle);
			lastPosition = orientationVertical ? event.getY() : event.getX();
			return isOnTouchEvent;
		case MotionEvent.ACTION_MOVE:
			final float delta = orientationVertical ? (lastPosition - event
					.getY()) : (lastPosition - event.getX());

			if (Math.abs(delta) > controller.getTouchSlop()) {
				setState(STATE_TOUCH);
				forward = delta > 0;
			}
			if (state == STATE_TOUCH) {
				if (Math.abs(delta) > minMovement) // ignore small movements
					forward = delta > 0;

				controller.showFlipAnimation();

				float angleDelta;
				if (orientationVertical)
					angleDelta = 180 * delta / controller.getContentHeight()
							* movementRate;
				else
					angleDelta = 180 * delta / controller.getContentWidth()
							* movementRate;

				if (Math.abs(angleDelta) > maxTouchMoveAngle) // prevent
					// large
					// delta
					// when
					// moving
					// too fast
					angleDelta = Math.signum(angleDelta) * maxTouchMoveAngle;

				// do not flip more than one page with one touch...
				if (Math.abs(getPageIndexFromAngle(accumulatedAngle
						+ angleDelta)
						- lastPageIndex) <= 1) {
					accumulatedAngle += angleDelta;
				}

				// Bounce the page for the first and the last page
				if (frontCards.getIndex() == maxIndex - 1) { // the last page
					if (accumulatedAngle > frontCards.getIndex() * 180
							+ maxTipAngle)
						accumulatedAngle = frontCards.getIndex() * 180
								+ maxTipAngle;
				} else if (accumulatedAngle < -maxTipAngle)
					accumulatedAngle = -maxTipAngle;

				final int anglePageIndex = getPageIndexFromAngle(accumulatedAngle);

				if (accumulatedAngle >= 0) {
					if (anglePageIndex != frontCards.getIndex()) {
						if (anglePageIndex == frontCards.getIndex() - 1) { // moved
							// to
							// previous
							// page
							swapCards(); // frontCards becomes the backCards
							frontCards.resetWithIndex(backCards.getIndex() - 1);
							controller.flippedToView(anglePageIndex, false);
						} else if (anglePageIndex == frontCards.getIndex() + 1) { // moved
							// to
							// next
							// page
							swapCards();
							backCards.resetWithIndex(frontCards.getIndex() + 1);
							controller.flippedToView(anglePageIndex, false);
						} else
							throw new RuntimeException(
									String.format(
											"Inconsistent states: anglePageIndex: %d, accumulatedAngle %.1f, frontCards %d, backCards %d",
											anglePageIndex, accumulatedAngle,
											frontCards.getIndex(),
											backCards.getIndex()));
					}
				}

				lastPosition = orientationVertical ? event.getY() : event
						.getX();

				controller.getSurfaceView().requestRender();
				return true;
			}

			return isOnTouchEvent;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL:
			if (state == STATE_TOUCH) {
				if (accumulatedAngle < 0)
					forward = true;
				else if (accumulatedAngle > frontCards.getIndex() * 180
						&& frontCards.getIndex() == maxIndex - 1)
					forward = false;

				setState(STATE_AUTO_ROTATE);
				controller.getSurfaceView().requestRender();
			}
			return isOnTouchEvent;
		}

		return false;
	}

	private void swapCards() {
		final ViewDualCards tmp = frontCards;
		frontCards = backCards;
		backCards = tmp;
	}

	private void setState(final int state) {
		if (this.state != state) {
			/*
			 * if (AphidLog.ENABLE_DEBUG)
			 * AphidLog.i("setState: from %d, to %d; angle %.1f", this.state,
			 * state, angle);
			 */
			this.state = state;
			animatedFrame = 0;
		}
	}

	private int getPageIndexFromAngle(final float angle) {
		return ((int) angle) / 180;
	}

	private float getDisplayAngle() {
		return accumulatedAngle % 180;
	}
}
