/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.adapterview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;


/**
 * Listview that attaches a custom view to the scrollbar as the list is
 * scrolling
 * <p>
 * <code>
 * <pre>
 * <img src="../../../../../resources/demo_capture.png"> <br/>
 * <b>Layout</b><br/>
 * &lt;rage.ui.widgets.listview.ExtendedListView<br/>
 * android:id="@android:id/list"<br/>
 * android:layout_height="fill_parent"<br/>
 * android:layout_width="fill_parent"<br/>
 * app:scrollBarPanel="@layout/&lt;YOUR_SCROLLBARPANEL_LAYOUT>"<br/>
 * app:scrollBarPanelInAnimation="@anim/YOUR_ANIMATION"<br/>
 * app:scrollBarPanelOutAnimation="@anim/YOUR_ANIMATION" /><br/>
 * </pre>
 * </code><br/>
 * <b>Code</b><br/>
 * <p/>
 * 
 * <pre>
 * <code>
 * // Set your scrollBarPanel
 * ScrollingInfoListView listView = (ScrollingInfoListView) findViewById(android.R.id.list);
 * // Attach a position changed listener on the listview and play with your
 * // scrollBarPanel
 * // when you need to update its content
 * mListView.setOnPositionChangedListener(new OnPositionChangedListener() {
 * 	&#064;Override
 * 	public void onPositionChanged(ExtendedListView listView, int firstVisiblePosition,View scrollBarPanel) {
 * 		((TextView) scrollBarPanel).setText(&quot;Position &quot; + firstVisiblePosition);
 *    }
 * });
 * </code>
 * </pre>
 * 
 * <b>Demo</b><br/>
 * <center><OBJECT CLASSID="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
 * WIDTH="725" HEIGHT="461" CODEBASE=
 * "http://active.macromedia.com/flash5/cabs/swflash.cab#version=7,0,0,0">
 * <PARAM NAME=movie VALUE="../../../../resources/scrollinginfolistview.swf">
 * <PARAM NAME=play VALUE=true> <PARAM NAME=loop VALUE=false> <PARAM NAME=wmode
 * VALUE=transparent> <PARAM NAME=quality VALUE=low> <EMBED
 * SRC="../../../../resources/scrollinginfolistview.swf" WIDTH=725 HEIGHT=461
 * quality=low loop=false wmode=transparent TYPE="application/x-shockwave-flash"
 * PLUGINSPAGE=
 * "http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"
 * > </EMBED> </OBJECT></center> <SCRIPT
 * src='../../../../resources/pagecurl.js'></script> <br/>
 * </p>
 * 
 * @author elton.kent
 */
public class ScrollingInfoListView extends ListView implements OnScrollListener {

	public static interface OnPositionChangedListener {

		public void onPositionChanged(ScrollingInfoListView listView,
				int position, View scrollBarPanel);

	}

	private OnScrollListener mOnScrollListener = null;

	private View mScrollBarPanel = null;
	private int mScrollBarPanelPosition = 0;

	private OnPositionChangedListener mPositionChangedListener;
	private int mLastPosition = -1;

	private Animation mInAnimation = null;
	private Animation mOutAnimation = null;

	private final Handler mHandler = new Handler();

	private final Runnable mScrollBarPanelFadeRunnable = new Runnable() {

		@Override
		public void run() {
			if (mOutAnimation != null) {
				mScrollBarPanel.startAnimation(mOutAnimation);
			}
		}
	};

	/*
	 * keep track of Measure Spec
	 */
	private int mWidthMeasureSpec;
	private int mHeightMeasureSpec;

	public ScrollingInfoListView(final Context context) {
		this(context, null);
	}

	public ScrollingInfoListView(final Context context, final AttributeSet attrs) {
		this(context, attrs, android.R.attr.listViewStyle);
	}

	public ScrollingInfoListView(final Context context,
			final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);

		super.setOnScrollListener(this);

		final TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.ScrollingInfoListView);
		final int scrollBarPanelLayoutId = a.getResourceId(
				R.styleable.ScrollingInfoListView_scrollBar_panel, -1);
		final int scrollBarPanelInAnimation = a.getResourceId(
				R.styleable.ScrollingInfoListView_scrollBar_panelInAnimation,
				-1);
		final int scrollBarPanelOutAnimation = a.getResourceId(
				R.styleable.ScrollingInfoListView_scrollBar_panelOutAnimation,
				-1);
		a.recycle();

		if (scrollBarPanelLayoutId != -1) {
			setScrollBarPanel(scrollBarPanelLayoutId);
		}

		final int scrollBarPanelFadeDuration = ViewConfiguration
				.getScrollBarFadeDuration();

		if (scrollBarPanelInAnimation > 0) {
			mInAnimation = AnimationUtils.loadAnimation(getContext(),
					scrollBarPanelInAnimation);
		} else {
			mInAnimation = toto.ui.animation.AnimationUtils.fadeInAnimation(
					1000, null);
		}
		if (scrollBarPanelOutAnimation > 0) {
			mOutAnimation = AnimationUtils.loadAnimation(getContext(),
					scrollBarPanelOutAnimation);
		} else {
			mOutAnimation = toto.ui.animation.AnimationUtils.fadeOutAnimation(
					2000, null);
		}

		if (scrollBarPanelOutAnimation > 0) {
			mOutAnimation.setDuration(scrollBarPanelFadeDuration);
			mOutAnimation.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(final Animation animation) {
				}

				@Override
				public void onAnimationRepeat(final Animation animation) {

				}

				@Override
				public void onAnimationEnd(final Animation animation) {
					if (mScrollBarPanel != null) {
						mScrollBarPanel.setVisibility(View.GONE);
					}
				}
			});
		}
	}

	@Override
	public void onScrollStateChanged(final AbsListView view,
			final int scrollState) {
		if (mOnScrollListener != null) {
			mOnScrollListener.onScrollStateChanged(view, scrollState);
		}
	}

	@Override
	public void onScroll(final AbsListView view, final int firstVisibleItem,
			final int visibleItemCount, final int totalItemCount) {
		if (null != mPositionChangedListener && null != mScrollBarPanel) {

			// Don't do anything if there is no itemviews
			if (totalItemCount > 0) {
				/*
				 * from android source code (ScrollBarDrawable.java)
				 */
				final int thickness = getVerticalScrollbarWidth();
				int height = Math.round((float) getMeasuredHeight()
						* computeVerticalScrollExtent()
						/ computeVerticalScrollRange());
				int thumbOffset = Math
						.round((float) (getMeasuredHeight() - height)
								* computeVerticalScrollOffset()
								/ (computeVerticalScrollRange() - computeVerticalScrollExtent()));
				final int minLength = thickness * 2;
				if (height < minLength) {
					height = minLength;
				}
				thumbOffset += height / 2;

				/*
				 * find out which itemviews the center of thumb is on
				 */
				final int count = getChildCount();
				for (int i = 0; i < count; ++i) {
					final View childView = getChildAt(i);
					if (childView != null) {
						if (thumbOffset > childView.getTop()
								&& thumbOffset < childView.getBottom()) {
							/*
							 * we have our candidate
							 */
							if (mLastPosition != firstVisibleItem + i) {
								mLastPosition = firstVisibleItem + i;

								/*
								 * inform the position of the panel has changed
								 */
								mPositionChangedListener.onPositionChanged(
										this, mLastPosition, mScrollBarPanel);

								/*
								 * measure panel right now since it has just
								 * changed
								 * 
								 * INFO: quick hack to handle TextView has
								 * ScrollBarPanel (to wrap text in case
								 * TextView's content has changed)
								 */
								measureChild(mScrollBarPanel,
										mWidthMeasureSpec, mHeightMeasureSpec);
							}
							break;
						}
					}
				}

				/*
				 * update panel position
				 */
				mScrollBarPanelPosition = thumbOffset
						- mScrollBarPanel.getMeasuredHeight() / 2;
				final int x = getMeasuredWidth()
						- mScrollBarPanel.getMeasuredWidth()
						- getVerticalScrollbarWidth();
				mScrollBarPanel.layout(
						x,
						mScrollBarPanelPosition,
						x + mScrollBarPanel.getMeasuredWidth(),
						mScrollBarPanelPosition
								+ mScrollBarPanel.getMeasuredHeight());
			}
		}

		if (mOnScrollListener != null) {
			mOnScrollListener.onScroll(view, firstVisibleItem,
					visibleItemCount, totalItemCount);
		}
	}

	public void setOnPositionChangedListener(
			final OnPositionChangedListener onPositionChangedListener) {
		mPositionChangedListener = onPositionChangedListener;
	}

	@Override
	public void setOnScrollListener(final OnScrollListener onScrollListener) {
		mOnScrollListener = onScrollListener;
	}

	public void setScrollBarPanel(final View scrollBarPanel) {
		mScrollBarPanel = scrollBarPanel;
		mScrollBarPanel.setVisibility(View.GONE);
		requestLayout();
	}

	public void setScrollBarPanel(final int resId) {
		setScrollBarPanel(LayoutInflater.from(getContext()).inflate(resId,
				this, false));
	}

	public View getScrollBarPanel() {
		return mScrollBarPanel;
	}

	@Override
	protected boolean awakenScrollBars(final int startDelay,
			final boolean invalidate) {
		final boolean isAnimationPlayed = super.awakenScrollBars(startDelay,
				invalidate);

		if (isAnimationPlayed == true && mScrollBarPanel != null) {
			if (mScrollBarPanel.getVisibility() == View.GONE) {
				mScrollBarPanel.setVisibility(View.VISIBLE);
				if (mInAnimation != null) {
					mScrollBarPanel.startAnimation(mInAnimation);
				}
			}

			mHandler.removeCallbacks(mScrollBarPanelFadeRunnable);
			mHandler.postAtTime(mScrollBarPanelFadeRunnable,
					AnimationUtils.currentAnimationTimeMillis() + startDelay);
		}

		return isAnimationPlayed;
	}

	@Override
	protected void onMeasure(final int widthMeasureSpec,
			final int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		if (mScrollBarPanel != null && getAdapter() != null) {
			mWidthMeasureSpec = widthMeasureSpec;
			mHeightMeasureSpec = heightMeasureSpec;
			measureChild(mScrollBarPanel, widthMeasureSpec, heightMeasureSpec);
		}
	}

	@Override
	protected void onLayout(final boolean changed, final int left,
			final int top, final int right, final int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		if (mScrollBarPanel != null) {
			final int x = getMeasuredWidth()
					- mScrollBarPanel.getMeasuredWidth()
					- getVerticalScrollbarWidth();
			mScrollBarPanel.layout(
					x,
					mScrollBarPanelPosition,
					x + mScrollBarPanel.getMeasuredWidth(),
					mScrollBarPanelPosition
							+ mScrollBarPanel.getMeasuredHeight());
		}
	}

	@Override
	protected void dispatchDraw(final Canvas canvas) {
		super.dispatchDraw(canvas);

		if (mScrollBarPanel != null
				&& mScrollBarPanel.getVisibility() == View.VISIBLE) {
			drawChild(canvas, mScrollBarPanel, getDrawingTime());
		}
	}

	@Override
	public void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		mHandler.removeCallbacks(mScrollBarPanelFadeRunnable);
	}
}