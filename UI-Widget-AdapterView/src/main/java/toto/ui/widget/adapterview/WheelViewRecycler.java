/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.adapterview;

import java.util.LinkedList;
import java.util.List;

import android.view.View;
import android.widget.LinearLayout;

/**
 * Recycle stores wheel items to reuse.
 */
class WheelViewRecycler {
	// Cached empty items
	private List<View> emptyItems;

	// Cached items
	private List<View> items;

	// Wheel view
	private final WheelView wheel;

	/**
	 * Constructor
	 * 
	 * @param wheel
	 *            the wheel view
	 */
	WheelViewRecycler(final WheelView wheel) {
		this.wheel = wheel;
	}

	/**
	 * Adds view to specified cache. Creates a cache list if it is null.
	 * 
	 * @param view
	 *            the view to be cached
	 * @param cache
	 *            the cache list
	 * @return the cache list
	 */
	private List<View> addView(final View view, List<View> cache) {
		if (cache == null) {
			cache = new LinkedList<View>();
		}

		cache.add(view);
		return cache;
	}

	/**
	 * Clears all views
	 */
	public void clearAll() {
		if (items != null) {
			items.clear();
		}
		if (emptyItems != null) {
			emptyItems.clear();
		}
	}

	/**
	 * Gets view from specified cache.
	 * 
	 * @param cache
	 *            the cache
	 * @return the first view from cache.
	 */
	private View getCachedView(final List<View> cache) {
		if (cache != null && cache.size() > 0) {
			final View view = cache.get(0);
			cache.remove(0);
			return view;
		}
		return null;
	}

	/**
	 * Gets empty item view
	 * 
	 * @return the cached empty view
	 */
	View getEmptyItem() {
		return getCachedView(emptyItems);
	}

	/**
	 * Gets item view
	 * 
	 * @return the cached view
	 */
	View getItem() {
		return getCachedView(items);
	}

	/**
	 * Recycles items from specified layout. There are saved only items not
	 * included to specified range. All the cached items are removed from
	 * original layout.
	 * 
	 * @param layout
	 *            the layout containing items to be cached
	 * @param firstItem
	 *            the number of first item in layout
	 * @param range
	 *            the range of current wheel items
	 * @return the new value of first item number
	 */
	int recycleItems(final LinearLayout layout, int firstItem,
			final WheelViewItemsRange range) {
		int index = firstItem;
		for (int i = 0; i < layout.getChildCount();) {
			if (!range.contains(index)) {
				recycleView(layout.getChildAt(i), index);
				layout.removeViewAt(i);
				if (i == 0) { // first item
					firstItem++;
				}
			} else {
				i++; // go to next item
			}
			index++;
		}
		return firstItem;
	}

	/**
	 * Adds view to cache. Determines view type (item view or empty one) by
	 * index.
	 * 
	 * @param view
	 *            the view to be cached
	 * @param index
	 *            the index of view
	 */
	private void recycleView(final View view, int index) {
		final int count = wheel.getViewAdapter().getItemsCount();

		if ((index < 0 || index >= count) && !wheel.isCyclic()) {
			// empty view
			emptyItems = addView(view, emptyItems);
		} else {
			while (index < 0) {
				index = count + index;
			}
			index %= count;
			items = addView(view, items);
		}
	}

}
