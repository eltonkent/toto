package toto.device.location;


interface ILocationService {
	Location getBestLocation();
}