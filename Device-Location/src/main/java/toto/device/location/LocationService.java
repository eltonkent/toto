/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device.location;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

/**
 * Location service that can be started dynamically.
 * <p>
 * 
 * </p>
 * 
 * @see toto.device.ServiceBinder
 * @author Mobifluence Interactive
 * 
 */
public class LocationService extends Service {

	private Location mBestLocation;

	private final LocationListener mLocationListener = new LocationListener() {

		@Override
		public void onLocationChanged(final Location location) {
			if (LocationUtils.isBetterLocation(location, mBestLocation)) {
				mBestLocation = location;
			}

		}

		@Override
		public void onProviderDisabled(final String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(final String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(final String provider, final int status,
				final Bundle extras) {
			// TODO Auto-generated method stub

		}
	};
	private LocationManager mLocationManager;

	ILocationService.Stub mService = new ILocationService.Stub() {

		@Override
		public Location getBestLocation() throws RemoteException {
			return mBestLocation;
		}

	};

	@Override
	public IBinder onBind(final Intent arg0) {
		return mService;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mLocationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		mBestLocation = null; // mLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
		if (mBestLocation == null) {
			mBestLocation = mLocationManager
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
		if (mBestLocation == null) {
			mBestLocation = mLocationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		}
		mLocationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				0, 0, mLocationListener);
		// mLocationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,
		// 0, 0, mLocationListener);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mLocationManager.removeUpdates(mLocationListener);
	}

}
