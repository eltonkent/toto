/**
 * Utilities that simplify Location management using the most efficient onboard providers. 
 */
package toto.device.location;