/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device.location;

import android.location.Location;

/**
 * Utilities for Android {@code Location} objects.
 * 
 * @author ekent4
 * 
 */
public final class LocationUtils {

	private static double curvature(final double e1f, final double e2f,
			final double e3f, final double e4f, final double p) {
		return e1f * p - e2f * Math.sin(2. * p) + e3f * Math.sin(4. * p) - e4f
				* Math.sin(6. * p);
	}

	private static double E1F(final double d) {
		return (1.0 - d / 4. - 3. * d * d / 64. - 5. * d * d * d / 256.);
	}

	private static double E2F(final double d) {
		return (3. * d / 8. + 3. * d * d / 32. + 45. * d * d * d / 1024.);
	}

	private static double E3F(final double d) {
		return (15. * d * d / 256. + 45. * d * d * d / 1024.);
	}

	private static double E4F(final double d) {
		return (35. * d * d * d * d / 3072.);
	}

	/**
	 * Converts degree representation to NW,N,S etc.
	 * 
	 * @param degrees
	 * @return
	 */
	public static String getDirection(final float degrees) {

		if ((degrees >= 11.25) && (degrees <= 33.75)) {
			return "NNE";
		} else if ((degrees > 33.75) && (degrees <= 56.25)) {
			return "NE";
		} else if ((degrees > 56.25) && (degrees <= 78.75)) {
			return "ENE";
		} else if ((degrees > 78.75) && (degrees <= 101.25)) {
			return "E";
		} else if ((degrees > 101.25) && (degrees <= 123.75)) {
			return "ESE";
		} else if ((degrees > 123.75) && (degrees <= 146.25)) {
			return "SE";
		} else if ((degrees > 146.25) && (degrees <= 168.75)) {
			return "SSE";
		} else if ((degrees > 168.75) && (degrees <= 191.25)) {
			return "S";
		} else if ((degrees > 191.25) && (degrees <= 213.75)) {
			return "SSW";
		} else if ((degrees > 213.75) && (degrees <= 236.25)) {
			return "SW";
		} else if ((degrees > 236.25) && (degrees <= 258.75)) {
			return "WSW";
		} else if ((degrees > 258.75) && (degrees <= 281.25)) {
			return "W";
		} else if ((degrees > 281.25) && (degrees <= 303.75)) {
			return "WNW";
		} else if ((degrees > 303.75) && (degrees <= 326.25)) {
			return "NW";
		} else if ((degrees > 326.2) && (degrees <= 348.75)) {
			return "NNW";
		} else if ((degrees > 348.75) && (degrees < 11.25)) {
			return "N";
		}
		return "U/K";
	}

	/**
	 * Determines whether one Location reading is better than the current
	 * Location fix
	 * 
	 * @param location
	 *            The new Location that you want to evaluate
	 * @param currentBestLocation
	 *            The Location fix, to which you want to compare the new one
	 * @return true of {@code location} is better than
	 *         {@code currentBestLocation}
	 */
	public static boolean isBetterLocation(final Location location,
			final Location currentBestLocation) {
		final int TWO_MINUTES = 1000 * 60 * 2;
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is newer or older
		final long timeDelta = location.getTime()
				- currentBestLocation.getTime();
		final boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		final boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		final boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use
		// the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be
			// worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		final int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
				.getAccuracy());
		final boolean isLessAccurate = accuracyDelta > 0;
		final boolean isMoreAccurate = accuracyDelta < 0;
		final boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		final boolean isFromSameProvider = isSameProvider(
				location.getProvider(), currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate
				&& isFromSameProvider) {
			return true;
		}
		return false;
	}

	/** Checks whether two providers are the same */
	private static boolean isSameProvider(final String provider1,
			final String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}

}
