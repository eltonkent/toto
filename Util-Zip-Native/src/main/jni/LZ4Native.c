#include "lz4/lz4.h"
#include <stdlib.h>
#include <LZ4Native.h>

static jclass OutOfMemoryError;


JNIEXPORT void JNICALL Java_toto_util_zip_nativ_LZ4Native_init
  (JNIEnv *env, jclass cls) {
  OutOfMemoryError = (*env)->FindClass(env, "java/lang/OutOfMemoryError");
}

static void throw_OOM(JNIEnv *env) {
  (*env)->ThrowNew(env, OutOfMemoryError, "Out of memory");
}


JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_compress_1limitedOutput
  (JNIEnv *env, jclass cls, jbyteArray src, jint srcOff, jint srcLen, jbyteArray dest, jint destOff, jint maxDestLen) {

  char* in;
  char* out;
  jint compressed;
  
  in = (char*) (*env)->GetPrimitiveArrayCritical(env, src, 0);
  if (in == NULL) {
    throw_OOM(env);
    return 0;
  }
  out = (char*) (*env)->GetPrimitiveArrayCritical(env, dest, 0);
  if (out == NULL) {
    throw_OOM(env);
    return 0;
  }

  compressed = LZ4_compress_limitedOutput(in + srcOff, out + destOff, srcLen, maxDestLen);

  (*env)->ReleasePrimitiveArrayCritical(env, src, in, 0);
  (*env)->ReleasePrimitiveArrayCritical(env, src, out, 0);

  return compressed;

}


JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_compressHC
  (JNIEnv *env, jclass cls, jbyteArray src, jint srcOff, jint srcLen, jbyteArray dest, jint destOff, jint maxDestLen) {

  char* in;
  char* out;
  jint compressed;
  
  in = (char*) (*env)->GetPrimitiveArrayCritical(env, src, 0);
  if (in == NULL) {
    throw_OOM(env);
    return 0;
  } 
  out = (char*) (*env)->GetPrimitiveArrayCritical(env, dest, 0);
  if (out == NULL) {
    throw_OOM(env);
    return 0;
  }

  compressed = LZ4_compressHC_limitedOutput(in + srcOff, out + destOff, srcLen, maxDestLen);

  (*env)->ReleasePrimitiveArrayCritical(env, src, in, 0);
  (*env)->ReleasePrimitiveArrayCritical(env, src, out, 0);

  return compressed;

}


JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_decompress_1fast
  (JNIEnv *env, jclass cls, jbyteArray src, jint srcOff, jbyteArray dest, jint destOff, jint destLen) {

  char* in;
  char* out;
  jint compressed;
  
  in = (char*) (*env)->GetPrimitiveArrayCritical(env, src, 0);
  if (in == NULL) {
    throw_OOM(env);
    return 0;
  }
  out = (char*) (*env)->GetPrimitiveArrayCritical(env, dest, 0);
  if (out == NULL) {
    throw_OOM(env);
    return 0;
  }

  compressed = LZ4_decompress_fast(in + srcOff, out + destOff, destLen);

  (*env)->ReleasePrimitiveArrayCritical(env, src, in, 0);
  (*env)->ReleasePrimitiveArrayCritical(env, src, out, 0);

  return compressed;

}


JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_decompress_1safe
  (JNIEnv *env, jclass cls, jbyteArray src, jint srcOff, jint srcLen, jbyteArray dest, jint destOff, jint maxDestLen) {

  char* in;
  char* out;
  jint decompressed;

  in = (char*) (*env)->GetPrimitiveArrayCritical(env, src, 0);
  if (in == NULL) {
    throw_OOM(env);
    return 0;
  }
  out = (char*) (*env)->GetPrimitiveArrayCritical(env, dest, 0);
  if (out == NULL) {
    throw_OOM(env);
    return 0;
  }

  decompressed = LZ4_decompress_safe(in + srcOff, out + destOff, srcLen, maxDestLen);

  (*env)->ReleasePrimitiveArrayCritical(env, src, in, 0);
  (*env)->ReleasePrimitiveArrayCritical(env, src, out, 0);

  return decompressed;

}


JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_decompress_1fast_1withPrefix64k
  (JNIEnv *env, jclass cls, jbyteArray src, jint srcOff, jbyteArray dest, jint destOff, jint destLen) {

  char* in;
  char* out;
  jint compressed;

  in = (char*) (*env)->GetPrimitiveArrayCritical(env, src, 0);
  if (in == NULL) {
    throw_OOM(env);
    return 0;
  }
  out = (char*) (*env)->GetPrimitiveArrayCritical(env, dest, 0);
  if (out == NULL) {
    throw_OOM(env);
    return 0;
  }

  compressed = LZ4_decompress_fast_withPrefix64k(in + srcOff, out + destOff, destLen);

  (*env)->ReleasePrimitiveArrayCritical(env, src, in, 0);
  (*env)->ReleasePrimitiveArrayCritical(env, src, out, 0);

  return compressed;

}


JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4JNI_decompress_1safe_1withPrefix64k
  (JNIEnv *env, jclass cls, jbyteArray src, jint srcOff, jint srcLen, jbyteArray dest, jint destOff, jint maxDestLen) {

  char* in;
  char* out;
  jint decompressed;

  in = (char*) (*env)->GetPrimitiveArrayCritical(env, src, 0);
  if (in == NULL) {
    throw_OOM(env);
    return 0;
  }
  out = (char*) (*env)->GetPrimitiveArrayCritical(env, dest, 0);
  if (out == NULL) {
    throw_OOM(env);
    return 0;
  }

  decompressed = LZ4_decompress_safe_withPrefix64k(in + srcOff, out + destOff, srcLen, maxDestLen);

  (*env)->ReleasePrimitiveArrayCritical(env, src, in, 0);
  (*env)->ReleasePrimitiveArrayCritical(env, src, out, 0);

  return decompressed;

}


JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4JNI_compressBound
  (JNIEnv *env, jclass cls, jint len) {

  return LZ4_compressBound(len);

}
