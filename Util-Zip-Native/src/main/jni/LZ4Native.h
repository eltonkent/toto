#include <jni.h>



/*
 * Class:     toto_util_zip_nativ_LZ4
 * Method:    init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_toto_util_zip_nativ_LZ4Native_init
  (JNIEnv *, jclass );

/*LZ4Native
 * Class:     toto_util_zip_nativ_LZ4Native
 * Method:    LZ4_compress_limitedOutput
 * Signature: ([BII[BII)I
 */
JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_compress_1limitedOutput
  (JNIEnv *, jclass , jbyteArray , jint , jint , jbyteArray , jint , jint );

/*
 * Class:     toto_util_zip_nativ_LZ4
 * Method:    LZ4_compressHC
 * Signature: ([BII[BI)I
 */
JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_compressHC
  (JNIEnv *, jclass , jbyteArray , jint , jint , jbyteArray , jint , jint );
/*
 * Class:     toto_util_zip_nativ_LZ4
 * Method:    LZ4_decompress
 * Signature: ([BI[BII)I
 */
JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_decompress_1fast
  (JNIEnv *, jclass , jbyteArray , jint , jbyteArray , jint , jint );

/*
 * Class:     toto_util_zip_nativ_LZ4
 * Method:    LZ4_decompress_unknownOutputSize
 * Signature: ([BII[BI)I
 */
JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_decompress_1safe
  (JNIEnv *, jclass , jbyteArray , jint , jint , jbyteArray , jint , jint );

/*
 * Class:     toto_util_zip_nativ_LZ4Native
 * Method:    LZ4_decompress_fast_withPrefix64k
 * Signature: ([BI[BII)I
 */
JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_decompress_1fast_1withPrefix64k
  (JNIEnv *, jclass , jbyteArray , jint , jbyteArray , jint , jint );

/*
 * Class:     toto_util_zip_nativ_LZ4JNI
 * Method:    LZ4_decompress_safe_withPrefix64k
 * Signature: ([BII[BII)I
 */
JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_decompress_1safe_1withPrefix64k
  (JNIEnv *, jclass , jbyteArray , jint , jint , jbyteArray , jint , jint ) ;

/*
 * Class:     toto_util_zip_nativ_LZ4JNI
 * Method:    LZ4_compressBound
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_toto_util_zip_nativ_LZ4Native_compressBound
  (JNIEnv *, jclass , jint );


