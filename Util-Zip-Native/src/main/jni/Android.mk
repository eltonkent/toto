LOCAL_PATH := $(call my-dir)
XXHASH_PATH := $(LOCAL_PATH)../../com.mobifluence.rage.security.hash/xxhash
include $(CLEAR_VARS)

LOCAL_MODULE := Zip_Snappy
LOCAL_CPP_EXTENSION := .cc .cpp
LOCAL_C_INCLUDES += snappy-1.1.0/
LOCAL_SRC_FILES := SnappyNative.cpp  snappy-1.1.0/snappy.cc snappy-1.1.0/snappy-sinksource.cc
include $(BUILD_SHARED_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE := Zip_LZ4
LOCAL_SRC_FILES := LZ4Native.c lz4/fuzzer.c lz4/lz4.c lz4/lz4hc.c lz4/xxhash.c
include $(BUILD_SHARED_LIBRARY)