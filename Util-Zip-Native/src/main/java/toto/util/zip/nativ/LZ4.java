package toto.util.zip.nativ;

import toto.lang.ArrayUtils;
import toto.util.zip.TLosslessCompression;

import java.io.IOException;

/**
 * Lossless <a
 * href="http://en.wikipedia.org/wiki/LZ4_(compression_algorithm)">LZ4</a>
 * compression that requires the size of the compressed data to be known
 * 
 * @author Mobifluence Interactive
 * 
 */
public final class LZ4 implements TLosslessCompression {

	/**
	 * Standard compression
	 * 
	 * @param src
	 * @param srcOff
	 * @param srcLen
	 * @param dest
	 * @param destOff
	 * @param maxDestLen
	 * @return
	 * @throws Exception
	 */
	public static int compress(final byte[] src, final int srcOff,
			final int srcLen, final byte[] dest, final int destOff,
			final int maxDestLen) throws IOException {
		ArrayUtils.checkRange(src, srcOff, srcLen);
		ArrayUtils.checkRange(dest, destOff, maxDestLen);
		final int result = LZ4Native.compress_limitedOutput(src, srcOff,
				srcLen, dest, destOff, maxDestLen);
		if (result <= 0) {
			throw new IOException("maxDestLen is too small");
		}
		return result;
	}

	/**
	 * High compression. A Bit slower
	 * 
	 * @param src
	 * @param srcOff
	 * @param srcLen
	 * @param dest
	 * @param destOff
	 * @param maxDestLen
	 * @return
	 * @throws Exception
	 */
	public static final int compressHC(final byte[] src, final int srcOff,
			final int srcLen, final byte[] dest, final int destOff,
			final int maxDestLen) throws IOException {
		ArrayUtils.checkRange(src, srcOff, srcLen);
		ArrayUtils.checkRange(dest, destOff, maxDestLen);
		final int result = LZ4Native.compressHC(src, srcOff, srcLen, dest,
				destOff, maxDestLen);
		if (result <= 0) {
			throw new IOException();
		}
		return result;
	}

	public static final byte[] compressHC(final byte[] src, final int srcOff,
			final int srcLen) throws IOException {
		final int maxCompressedLength = maxCompressedLength(srcLen);
		final byte[] compressed = new byte[maxCompressedLength];
		final int compressedLength = compressHC(src, srcOff, srcLen,
				compressed, 0);
		return ArrayUtils.copyOf(compressed, compressedLength);
	}

	public final int compressHC(final byte[] src, final byte[] dest)
			throws IOException {
		return compressHC(src, 0, src.length, dest, 0);
	}

	public static final int compressHC(final byte[] src, final int srcOff,
			final int srcLen, final byte[] dest, final int destOff)
			throws IOException {
		return compressHC(src, srcOff, srcLen, dest, destOff, dest.length
				- destOff);
	}

	/**
	 * Convenience method, equivalent to calling
	 * {@link #compress(byte[], int, int) compress(src, 0, src.length)}.
	 * 
	 * @throws Exception
	 */
	public static final byte[] compressHC(final byte[] src) throws IOException {
		return compressHC(src, 0, src.length);
	}

	//
	public static final byte[] compress(final byte[] src, final int srcOff,
			final int srcLen) throws IOException {
		final int maxCompressedLength = maxCompressedLength(srcLen);
		final byte[] compressed = new byte[maxCompressedLength];
		final int compressedLength = compressHC(src, srcOff, srcLen,
				compressed, 0);
		return ArrayUtils.copyOf(compressed, compressedLength);
	}

	public static final int compress(final byte[] src, final byte[] dest)
			throws IOException {
		return compressHC(src, 0, src.length, dest, 0);
	}

	public static final int compress(final byte[] src, final int srcOff,
			final int srcLen, final byte[] dest, final int destOff)
			throws IOException {
		return compressHC(src, srcOff, srcLen, dest, destOff, dest.length
				- destOff);
	}

	/**
	 * Convenience method, equivalent to calling
	 * {@link #compress(byte[], int, int) compress(src, 0, src.length)}.
	 * 
	 * @throws Exception
	 */
	public final byte[] compress(final byte[] src) throws IOException {
		return compressHC(src, 0, src.length);
	}

	public static final int maxCompressedLength(final int length) {
		if (length < 0) {
			throw new IllegalArgumentException("length must be >= 0, got "
					+ length);
		}
		return length + length / 255 + 16;
	}

	public static final int decompressFast(final byte[] src, final int srcOff,
			final byte[] dest, final int destOff, final int destLen)
			throws IOException {
		ArrayUtils.checkRange(src, srcOff);
		ArrayUtils.checkRange(dest, destOff, destLen);
		final int result = LZ4Native.decompress_fast(src, srcOff, dest,
				destOff, destLen);
		if (result < 0) {
			throw new IOException("Error decoding offset " + (srcOff - result)
					+ " of input buffer");
		}
		return result;
	}

	public static final int decompressSafe(final byte[] src, final int srcOff,
			final int srcLen, final byte[] dest, final int destOff,
			final int maxDestLen) throws IOException {
		ArrayUtils.checkRange(src, srcOff, srcLen);
		ArrayUtils.checkRange(dest, destOff, maxDestLen);
		final int result = LZ4Native.decompress_safe(src, srcOff, srcLen, dest,
				destOff, maxDestLen);
		if (result < 0) {
			throw new IOException("Error decoding offset " + (srcOff - result)
					+ " of input buffer");
		}
		return result;
	}

	public static final byte[] decompressSafe(final byte[] src,
			final int maxDestLen) throws IOException {
		return decompressSafe(src, 0, src.length, maxDestLen);
	}

	public static final byte[] decompressSafe(final byte[] src,
			final int srcOff, final int srcLen, final int maxDestLen)
			throws IOException {
		byte[] decompressed = new byte[maxDestLen];
		final int decompressedLength = decompressSafe(src, srcOff, srcLen,
				decompressed, 0, maxDestLen);
		if (decompressedLength != decompressed.length) {
			decompressed = ArrayUtils.copyOf(decompressed, decompressedLength);
		}
		return decompressed;
	}

	public static final int decompressSafe(final byte[] src, final int srcOff,
			final int srcLen, final byte[] dest, final int destOff)
			throws IOException {
		return decompressSafe(src, srcOff, srcLen, dest, destOff, dest.length
				- destOff);
	}

	// ==================

	/**
	 * 
	 * @param src
	 *            com
	 * @param maxDestLen
	 *            length of the uncompressed byte array.(array size before
	 *            compression)
	 * @return
	 * @throws IOException
	 */
	public static final byte[] decompressFast(final byte[] src,
			final int maxDestLen) throws IOException {
		return decompressFast(src, 0, src.length, maxDestLen);
	}

	public static final byte[] decompressFast(final byte[] src,
			final int srcOff, final int srcLen, final int maxDestLen)
			throws IOException {
		byte[] decompressed = new byte[maxDestLen];
		final int decompressedLength = decompressFast(src, srcOff,
				decompressed, 0, maxDestLen);
		if (decompressedLength != decompressed.length) {
			decompressed = ArrayUtils.copyOf(decompressed, decompressedLength);
		}
		return decompressed;
	}

	public static final int decompressFast(final byte[] src, final int srcOff,
			final int srcLen, final byte[] dest, final int destOff)
			throws IOException {
		return decompressFast(src, srcOff, dest, destOff, dest.length - destOff);
	}

	private int compressedSize;

	public void setCompressedSize(final int compressedSize) {
		this.compressedSize = compressedSize;
	}

	/**
	 * Since it requires the size of the compressed data to be known the
	 * {@link #setCompressedSize(int)} method should be called before calling
	 * decompress.
	 * 
	 * @throws IOException
	 */
	@Override
	public byte[] decompress(final byte[] source) throws IOException {
		return decompressSafe(source, compressedSize);
	}
}
