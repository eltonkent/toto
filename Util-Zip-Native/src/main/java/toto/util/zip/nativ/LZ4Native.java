package toto.util.zip.nativ;

import com.mi.toto.ToTo;

/**
 * JNI bindings to the original C implementation of LZ4.
 */
class LZ4Native {

	static {
		ToTo.loadLib("Zip_LZ4");
	}

	static native void init();

	static native int compress_limitedOutput(byte[] src, int srcOff,
			int srcLen, byte[] dest, int destOff, int maxDestLen);

	static native int compressHC(byte[] src, int srcOff, int srcLen,
			byte[] dest, int destOff, int maxDestLen);

	static native int decompress_fast(byte[] src, int srcOff, byte[] dest,
			int destOff, int destLen);

	static native int decompress_fast_withPrefix64k(byte[] src, int srcOff,
			byte[] dest, int destOff, int destLen);

	static native int decompress_safe(byte[] src, int srcOff, int srcLen,
			byte[] dest, int destOff, int maxDestLen);

	static native int decompress_safe_withPrefix64k(byte[] src, int srcOff,
			int srcLen, byte[] dest, int destOff, int maxDestLen);

	static native int compressBound(int len);

}
