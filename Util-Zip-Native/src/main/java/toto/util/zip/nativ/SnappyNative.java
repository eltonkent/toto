package toto.util.zip.nativ;

import com.mi.toto.ToTo;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Snappy native helper class.
 * 
 */
class SnappyNative {
	static {
		ToTo.loadLib("Zip_Snappy");
	}

	static native String nativeLibraryVersion();

	static native long rawCompress(long inputAddr, long inputSize, long destAddr)
			throws IOException;

	static native long rawUncompress(long inputAddr, long inputSize,
			long destAddr) throws IOException;

	static native int rawCompress(ByteBuffer input, int inputOffset,
			int inputLength, ByteBuffer compressed, int outputOffset)
			throws IOException;

	static native int rawCompress(Object input, int inputOffset,
			int inputByteLength, Object output, int outputOffset)
			throws IOException;

	static native int rawUncompress(ByteBuffer compressed, int inputOffset,
			int inputLength, ByteBuffer uncompressed, int outputOffset)
			throws IOException;

	static native int rawUncompress(Object input, int inputOffset,
			int inputLength, Object output, int outputOffset)
			throws IOException;

	// Returns the maximal size of the compressed representation of
	// input data that is "source_bytes" bytes in length;
	static native int maxCompressedLength(int source_bytes);

	// This operation takes O(1) time.
	static native int uncompressedLength(ByteBuffer compressed, int offset,
			int len) throws IOException;

	static native int uncompressedLength(Object input, int offset, int len)
			throws IOException;

	static native long uncompressedLength(long inputAddr, long len)
			throws IOException;

	static native boolean isValidCompressedBuffer(ByteBuffer compressed,
			int offset, int len) throws IOException;

	static native boolean isValidCompressedBuffer(Object input, int offset,
			int len) throws IOException;

	static native boolean isValidCompressedBuffer(long inputAddr, long offset,
			long len) throws IOException;

	static native void arrayCopy(Object src, int offset, int byteLength,
			Object dest, int dOffset) throws IOException;

	void throw_error(final int errorCode) throws IOException {
		throw new IOException(String.format("%s(%d)",
				SnappyErrorCode.getErrorMessage(errorCode), errorCode));
	}

}