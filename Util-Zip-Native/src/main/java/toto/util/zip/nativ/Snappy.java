package toto.util.zip.nativ;

import toto.lang.ArrayUtils;
import toto.util.zip.TLosslessCompression;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Snappy API for data compression/decompression.
 * <p>
 * The interface for the <a
 * href="https://code.google.com/p/snappy-java/">snappy</a>. The <a href=
 * "http://ning.github.io/jvm-compressor-benchmark/results/canterbury-uncompress-2011-07-28/index.html"
 * > fastest</a> compression library. <br/>
 * </p>
 * 
 * @see SnappyInputStream
 * @see SnappyOutputStream
 */
public class Snappy implements TLosslessCompression {

	/**
	 * Copy bytes from source to destination
	 * 
	 * @param src
	 *            pointer to the source array
	 * @param offset
	 *            byte offset in the source array
	 * @param byteLength
	 *            the number of bytes to copy
	 * @param dest
	 *            pointer to the destination array
	 * @param dest_offset
	 *            byte offset in the destination array
	 * @throws IOException
	 */
	public static void arrayCopy(final Object src, final int offset,
			final int byteLength, final Object dest, final int dest_offset)
			throws IOException {
		SnappyNative.arrayCopy(src, offset, byteLength, dest, dest_offset);
	}

	/**
	 * High-level API for compressing the input byte array. This method performs
	 * array copy to generate the result. If you want to reduce the memory copy
	 * cost, use {@link #compress(byte[], int, int, byte[], int)} or
	 * {@link #compress(ByteBuffer, ByteBuffer)}.
	 *
	 * @param input
	 *            the input data
	 * @return the compressed byte array
	 * @throws IOException
	 */
	public static byte[] doCompress(final byte[] input) throws IOException {
		return rawCompress(input, input.length);
	}

	/**
	 * Compress the input buffer content in [inputOffset,
	 * ...inputOffset+inputLength) then output to the specified output buffer.
	 *
	 * @param input
	 * @param inputOffset
	 * @param inputLength
	 * @param output
	 * @param outputOffset
	 * @return byte size of the compressed data
	 * @throws IOException
	 *             when failed to access the input/output buffer
	 */
	public static int compress(final byte[] input, final int inputOffset,
			final int inputLength, final byte[] output, final int outputOffset)
			throws IOException {
		ArrayUtils.checkRange(input, inputOffset, inputLength);
		ArrayUtils.checkRange(output, outputOffset, output.length);
		return rawCompress(input, inputOffset, inputLength, output,
				outputOffset);
	}

	/**
	 * Compress the content in the given input buffer. After the compression,
	 * you can retrieve the compressed data from the output buffer [pos() ...
	 * limit()) (compressed data size = limit() - pos() = remaining())
	 *
	 * @param uncompressed
	 *            buffer[pos() ... limit()) containing the input data
	 * @param compressed
	 *            output of the compressed data. Uses range [pos()..].
	 * @return byte size of the compressed data.
	 *
	 * @throws IOException
	 *             when the input is not a direct buffer
	 * @throws IOException
	 *             when the out is not a direct buffer
	 */
	public static int compress(final ByteBuffer uncompressed,
			final ByteBuffer compressed) throws IOException {

		if (!uncompressed.isDirect())
			throw new IOException("input is not a direct buffer");
		if (!compressed.isDirect())
			throw new IOException("destination is not a direct buffer");

		// input: uncompressed[pos(), limit())
		// output: compressed
		final int uPos = uncompressed.position();
		final int uLen = uncompressed.remaining();
		final int compressedSize = SnappyNative.rawCompress(uncompressed, uPos,
				uLen, compressed, compressed.position());

		// pos limit
		// [ ......BBBBBBB.........]
		compressed.limit(compressed.position() + compressedSize);

		return compressedSize;
	}

	/**
	 * Compress the input char array
	 *
	 * @param input
	 * @return the compressed data
	 */
	public static byte[] compress(final char[] input) throws IOException {
		return rawCompress(input, input.length * 2); // char uses 2 bytes
	}

	/**
	 * Compress the input double array
	 *
	 * @param input
	 * @return the compressed data
	 */
	public static byte[] compress(final double[] input) throws IOException {
		return rawCompress(input, input.length * 8); // double uses 8 bytes
	}

	/**
	 * Compress the input float array
	 *
	 * @param input
	 * @return the compressed data
	 */
	public static byte[] compress(final float[] input) throws IOException {
		return rawCompress(input, input.length * 4); // float uses 4 bytes
	}

	/**
	 * Compress the input int array
	 *
	 * @param input
	 * @return the compressed data
	 */
	public static byte[] compress(final int[] input) throws IOException {
		return rawCompress(input, input.length * 4); // int uses 4 bytes
	}

	/**
	 * Compress the input long array
	 *
	 * @param input
	 * @return the compressed data
	 */
	public static byte[] compress(final long[] input) throws IOException {
		return rawCompress(input, input.length * 8); // long uses 8 bytes
	}

	/**
	 * Compress the input short array
	 *
	 * @param input
	 * @return the compressed data
	 */
	public static byte[] compress(final short[] input) throws IOException {
		return rawCompress(input, input.length * 2); // short uses 2 bytes
	}

	/**
	 * Compress the input String
	 *
	 * @param s
	 * @return the compressed data
	 * @throws IOException
	 */
	public static byte[] compress(final String s) throws IOException {
		try {
			return compress(s, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			throw new IllegalStateException("UTF-8 encoder is not found");
		}
	}

	/**
	 * Compress the input string using the given encoding
	 *
	 * @param s
	 * @param encoding
	 * @return the compressed data
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static byte[] compress(final String s, final String encoding)
			throws UnsupportedEncodingException, IOException {
		final byte[] data = s.getBytes(encoding);
		return doCompress(data);
	}

	/**
	 * Returns true iff the contents of compressed buffer [offset,
	 * offset+length) can be uncompressed successfully. Does not return the
	 * uncompressed data. Takes time proportional to the input length, but is
	 * usually at least a factor of four faster than actual decompression.
	 */
	public static boolean isValidCompressedBuffer(final byte[] input,
			final int offset, final int length) throws IOException {
		if (input == null)
			throw new NullPointerException("input is null");
		return SnappyNative.isValidCompressedBuffer(input, offset, length);
	}

	/**
	 * Returns true iff the contents of compressed buffer [offset,
	 * offset+length) can be uncompressed successfully. Does not return the
	 * uncompressed data. Takes time proportional to the input length, but is
	 * usually at least a factor of four faster than actual decompression.
	 */
	public static boolean isValidCompressedBuffer(final byte[] input)
			throws IOException {
		return isValidCompressedBuffer(input, 0, input.length);
	}

	/**
	 * Returns true iff the contents of compressed buffer [pos() ... limit())
	 * can be uncompressed successfully. Does not return the uncompressed data.
	 * Takes time proportional to the input length, but is usually at least a
	 * factor of four faster than actual decompression.
	 */
	public static boolean isValidCompressedBuffer(final ByteBuffer compressed)
			throws IOException {
		return SnappyNative.isValidCompressedBuffer(compressed,
				compressed.position(), compressed.remaining());
	}

	/**
	 * Returns true iff the contents of compressed buffer [offset,
	 * offset+length) can be uncompressed successfully. Does not return the
	 * uncompressed data. Takes time proportional to the input length, but is
	 * usually at least a factor of four faster than actual decompression.
	 */
	public static boolean isValidCompressedBuffer(final long inputAddr,
			final long offset, final long length) throws IOException {
		return SnappyNative.isValidCompressedBuffer(inputAddr, offset, length);
	}

	/**
	 * Get the maximum byte size needed for compressing data of the given byte
	 * size.
	 *
	 * @param byteSize
	 *            byte size of the data to compress
	 * @return maximum byte size of the compressed data
	 */
	public static int maxCompressedLength(final int byteSize) {
		return SnappyNative.maxCompressedLength(byteSize);
	}

	/**
	 * Zero-copy compress using memory addresses.
	 *
	 * @param inputAddr
	 *            input memory address
	 * @param inputSize
	 *            input byte size
	 * @param destAddr
	 *            destination address of the compressed data
	 * @return the compressed data size
	 * @throws IOException
	 */
	public static long rawCompress(final long inputAddr, final long inputSize,
			final long destAddr) throws IOException {
		return SnappyNative.rawCompress(inputAddr, inputSize, destAddr);
	}

	/**
	 * Zero-copy decompress using memory addresses.
	 *
	 * @param inputAddr
	 *            input memory address
	 * @param inputSize
	 *            input byte size
	 * @param destAddr
	 *            destination address of the uncompressed data
	 * @return the uncompressed data size
	 * @throws IOException
	 */
	public static long rawUncompress(final long inputAddr,
			final long inputSize, final long destAddr) throws IOException {
		return SnappyNative.rawUncompress(inputAddr, inputSize, destAddr);
	}

	/**
	 * Compress the input data and produce a byte array of the uncompressed data
	 *
	 * @param data
	 *            input array. The input MUST be an array type
	 * @param byteSize
	 *            the input byte size
	 * @return compressed data
	 */
	public static byte[] rawCompress(final Object data, final int byteSize)
			throws IOException {
		final byte[] buf = new byte[Snappy.maxCompressedLength(byteSize)];
		final int compressedByteSize = SnappyNative.rawCompress(data, 0,
				byteSize, buf, 0);
		final byte[] result = new byte[compressedByteSize];
		System.arraycopy(buf, 0, result, 0, compressedByteSize);
		return result;
	}

	/**
	 * Compress the input buffer [offset,... ,offset+length) contents, then
	 * write the compressed data to the output buffer[offset, ...)
	 *
	 * @param input
	 *            input array. This MUST be a primitive array type
	 * @param inputOffset
	 *            byte offset at the output array
	 * @param inputLength
	 *            byte length of the input data
	 * @param output
	 *            output array. This MUST be a primitive array type
	 * @param outputOffset
	 *            byte offset at the output array
	 * @return byte size of the compressed data
	 * @throws IOException
	 */
	public static int rawCompress(final Object input, final int inputOffset,
			final int inputLength, final byte[] output, final int outputOffset)
			throws IOException {
		if (input == null || output == null)
			throw new NullPointerException("input or output is null");

		final int compressedSize = SnappyNative.rawCompress(input, inputOffset,
				inputLength, output, outputOffset);
		return compressedSize;
	}

	/**
	 * Uncompress the content in the input buffer. The uncompressed data is
	 * written to the output buffer.
	 *
	 * Note that if you pass the wrong data or the range [inputOffset,
	 * inputOffset + inputLength) that cannot be uncompressed, your JVM might
	 * crash due to the access violation exception issued in the native code
	 * written in C++. To avoid this type of crash, use
	 * {@link #isValidCompressedBuffer(byte[], int, int)} first.
	 *
	 * @param input
	 *            input byte array
	 * @param inputOffset
	 *            byte offset in the input byte array
	 * @param inputLength
	 *            byte length of the input data
	 * @param output
	 *            output buffer, MUST be a primitive type array
	 * @param outputOffset
	 *            byte offset in the output buffer
	 * @return the byte size of the uncompressed data
	 * @throws IOException
	 *             when failed to uncompress the input data
	 */
	public static int rawUncompress(final byte[] input, final int inputOffset,
			final int inputLength, final Object output, final int outputOffset)
			throws IOException {
		if (input == null || output == null)
			throw new NullPointerException("input or output is null");
		return SnappyNative.rawUncompress(input, inputOffset, inputLength,
				output, outputOffset);
	}

	/**
	 * High-level API for uncompressing the input byte array.
	 *
	 * @param input
	 * @return the uncompressed byte array
	 * @throws IOException
	 */
	public static byte[] uncompress(final byte[] input) throws IOException {
		final byte[] result = new byte[Snappy.uncompressedLength(input)];
		final int byteSize = Snappy.uncompress(input, 0, input.length, result,
				0);
		return result;
	}

	/**
	 * Uncompress the content in the input buffer. The uncompressed data is
	 * written to the output buffer.
	 *
	 * Note that if you pass the wrong data or the range [inputOffset,
	 * inputOffset + inputLength) that cannot be uncompressed, your JVM might
	 * crash due to the access violation exception issued in the native code
	 * written in C++. To avoid this type of crash, use
	 * {@link #isValidCompressedBuffer(byte[], int, int)} first.
	 *
	 * @param input
	 * @param inputOffset
	 * @param inputLength
	 * @param output
	 * @param outputOffset
	 * @return the byte size of the uncompressed data
	 * @throws IOException
	 */
	public static int uncompress(final byte[] input, final int inputOffset,
			final int inputLength, final byte[] output, final int outputOffset)
			throws IOException {
		return rawUncompress(input, inputOffset, inputLength, output,
				outputOffset);
	}

	/**
	 * Uncompress the content in the input buffer. The result is dumped to the
	 * specified output buffer.
	 *
	 * Note that if you pass the wrong data or the range [pos(), limit()) that
	 * cannot be uncompressed, your JVM might crash due to the access violation
	 * exception issued in the native code written in C++. To avoid this type of
	 * crash, use {@link #isValidCompressedBuffer(ByteBuffer)} first.
	 *
	 *
	 * @param compressed
	 *            buffer[pos() ... limit()) containing the input data
	 * @param uncompressed
	 *            output of the the uncompressed data. It uses buffer[pos()..]
	 * @return uncompressed data size
	 *
	 * @throws IOException
	 *             when failed to uncompress the given input
	 * @throws SnappyError
	 *             when the input is not a direct buffer
	 */
	public static int uncompress(final ByteBuffer compressed,
			final ByteBuffer uncompressed) throws IOException {

		if (!compressed.isDirect())
			throw new IOException("input is not a direct buffer");
		if (!uncompressed.isDirect())
			throw new IOException("destination is not a direct buffer");

		final int cPos = compressed.position();
		final int cLen = compressed.remaining();

		// pos limit
		// [ ......UUUUUU.........]
		final int decompressedSize = SnappyNative.rawUncompress(compressed,
				cPos, cLen, uncompressed, uncompressed.position());
		uncompressed.limit(uncompressed.position() + decompressedSize);

		return decompressedSize;
	}

	/**
	 * Uncompress the input data as char array
	 *
	 * @param input
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static char[] uncompressCharArray(final byte[] input)
			throws IOException {
		return n_uncompressCharArray(input, 0, input.length);
	}

	/**
	 * Uncompress the input[offset, .., offset+length) as a char array
	 *
	 * @param input
	 * @param offset
	 * @param length
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static char[] n_uncompressCharArray(final byte[] input,
			final int offset, final int length) throws IOException {
		final int uncompressedLength = Snappy.uncompressedLength(input, offset,
				length);
		final char[] result = new char[uncompressedLength / 2];
		final int byteSize = SnappyNative.rawUncompress(input, offset, length,
				result, 0);
		return result;
	}

	/**
	 * Uncompress the input as a double array
	 *
	 * @param input
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static double[] uncompressDoubleArray(final byte[] input)
			throws IOException {
		final int uncompressedLength = Snappy.uncompressedLength(input, 0,
				input.length);
		final double[] result = new double[uncompressedLength / 8];
		final int byteSize = SnappyNative.rawUncompress(input, 0, input.length,
				result, 0);
		return result;
	}

	/**
	 * Get the uncompressed byte size of the given compressed input. This
	 * operation takes O(1) time.
	 *
	 * @param input
	 * @return uncompressed byte size of the the given input data
	 * @throws IOException
	 *             when failed to uncompress the given input. The error code is
	 *             {@link SnappyErrorCode#PARSING_ERROR}
	 */
	public static int uncompressedLength(final byte[] input) throws IOException {
		return SnappyNative.uncompressedLength(input, 0, input.length);
	}

	/**
	 * Get the uncompressed byte size of the given compressed input. This
	 * operation takes O(1) time.
	 *
	 * @param input
	 * @param offset
	 * @param length
	 * @return uncompressed byte size of the the given input data
	 * @throws IOException
	 *             when failed to uncompress the given input. The error code is
	 *             {@link SnappyErrorCode#PARSING_ERROR}
	 */
	public static int uncompressedLength(final byte[] input, final int offset,
			final int length) throws IOException {
		if (input == null)
			throw new NullPointerException("input is null");

		return SnappyNative.uncompressedLength(input, offset, length);
	}

	/**
	 * Get the uncompressed byte size of the given compressed input. This
	 * operation takes O(1) time.
	 *
	 * @param compressed
	 *            input data [pos() ... limit())
	 * @return uncompressed byte length of the given input
	 * @throws IOException
	 *             when failed to uncompress the given input. The error code is
	 *             {@link SnappyErrorCode#PARSING_ERROR}
	 * @throws IOException
	 *             when the input is not a direct buffer
	 */
	public static int uncompressedLength(final ByteBuffer compressed)
			throws IOException {
		if (!compressed.isDirect())
			throw new IOException("input is not a direct buffer");

		return SnappyNative.uncompressedLength(compressed,
				compressed.position(), compressed.remaining());
	}

	/**
	 * Get the uncompressed byte size of the given compressed input. This
	 * operation takes O(1) time.
	 *
	 * @param inputAddr
	 *            compressed data address
	 * @param len
	 *            byte length of the input
	 * @return uncompressed byte length of the given input
	 * @throws IOException
	 *             when failed to uncompress the given input. The error code is
	 *             {@link SnappyErrorCode#PARSING_ERROR}
	 */
	public static long uncompressedLength(final long inputAddr, final long len)
			throws IOException {
		return SnappyNative.uncompressedLength(inputAddr, len);
	}

	/**
	 * Uncompress the input as a float array
	 *
	 * @param input
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static float[] uncompressFloatArray(final byte[] input)
			throws IOException {
		return uncompressFloatArray(input, 0, input.length);
	}

	/**
	 * Uncompress the input[offset, offset+length) as a float array
	 *
	 * @param input
	 * @param offset
	 * @param length
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static float[] uncompressFloatArray(final byte[] input,
			final int offset, final int length) throws IOException {
		final int uncompressedLength = Snappy.uncompressedLength(input, offset,
				length);
		final float[] result = new float[uncompressedLength / 4];
		final int byteSize = SnappyNative.rawUncompress(input, offset, length,
				result, 0);
		return result;
	}

	/**
	 * Uncompress the input data as an int array
	 *
	 * @param input
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static int[] uncompressIntArray(final byte[] input)
			throws IOException {
		return uncompressIntArray(input, 0, input.length);
	}

	/**
	 * Uncompress the input[offset, offset+length) as an int array
	 *
	 * @param input
	 * @param offset
	 * @param length
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static int[] uncompressIntArray(final byte[] input,
			final int offset, final int length) throws IOException {
		final int uncompressedLength = Snappy.uncompressedLength(input, offset,
				length);
		final int[] result = new int[uncompressedLength / 4];
		final int byteSize = SnappyNative.rawUncompress(input, offset, length,
				result, 0);
		return result;
	}

	/**
	 * Uncompress the input data as a long array
	 *
	 * @param input
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static long[] uncompressLongArray(final byte[] input)
			throws IOException {
		return uncompressLongArray(input, 0, input.length);
	}

	/**
	 * Uncompress the input[offset, offset+length) as a long array
	 *
	 * @param input
	 * @param offset
	 * @param length
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static long[] uncompressLongArray(final byte[] input,
			final int offset, final int length) throws IOException {
		final int uncompressedLength = Snappy.uncompressedLength(input, offset,
				length);
		final long[] result = new long[uncompressedLength / 8];
		final int byteSize = SnappyNative.rawUncompress(input, offset, length,
				result, 0);
		return result;
	}

	/**
	 * Uncompress the input as a short array
	 *
	 * @param input
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static short[] uncompressShortArray(final byte[] input)
			throws IOException {
		return uncompressShortArray(input, 0, input.length);
	}

	/**
	 * Uncompress the input[offset, offset+length) as a short array
	 *
	 * @param input
	 * @param offset
	 * @param length
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static short[] uncompressShortArray(final byte[] input,
			final int offset, final int length) throws IOException {
		final int uncompressedLength = Snappy.uncompressedLength(input, offset,
				length);
		final short[] result = new short[uncompressedLength / 2];
		final int byteSize = SnappyNative.rawUncompress(input, offset, length,
				result, 0);
		return result;
	}

	/**
	 * Uncompress the input as a String
	 * 
	 * @param input
	 * @return the uncompressed dasta
	 * @throws IOException
	 */
	public static String uncompressString(final byte[] input)
			throws IOException {
		try {
			return uncompressString(input, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			throw new IllegalStateException("UTF-8 decoder is not found");
		}
	}

	/**
	 * Uncompress the input[offset, offset+length) as a String
	 * 
	 * @param input
	 * @param offset
	 * @param length
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static String uncompressString(final byte[] input, final int offset,
			final int length) throws IOException {
		try {
			return uncompressString(input, offset, length, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			throw new IllegalStateException("UTF-8 decoder is not found");
		}
	}

	/**
	 * Uncompress the input[offset, offset+length) as a String of the given
	 * encoding
	 * 
	 * @param input
	 * @param offset
	 * @param length
	 * @param encoding
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static String uncompressString(final byte[] input, final int offset,
			final int length, final String encoding) throws IOException,
			UnsupportedEncodingException {
		final byte[] uncompressed = new byte[uncompressedLength(input, offset,
				length)];
		final int compressedSize = uncompress(input, offset, length,
				uncompressed, 0);
		return new String(uncompressed, encoding);
	}

	/**
	 * Uncompress the input[offset, offset+length) as a String of the given
	 * encoding
	 * 
	 * @param input
	 * @param offset
	 * @param length
	 * @param encoding
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static String uncompressString(final byte[] input, final int offset,
			final int length, final Charset encoding) throws IOException,
			UnsupportedEncodingException {
		final byte[] uncompressed = new byte[uncompressedLength(input, offset,
				length)];
		final int compressedSize = uncompress(input, offset, length,
				uncompressed, 0);
		return new String(uncompressed);
	}

	/**
	 * Uncompress the input as a String of the given encoding
	 * 
	 * @param input
	 * @param encoding
	 * @return the uncompressed data
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	public static String uncompressString(final byte[] input,
			final String encoding) throws IOException,
			UnsupportedEncodingException {
		final byte[] uncompressed = uncompress(input);
		return new String(uncompressed, encoding);
	}

	/**
	 * Uncompress the input as a String of the given encoding
	 * 
	 * @param input
	 * @param encoding
	 * @return the uncompressed data
	 * @throws IOException
	 */
	public static String uncompressString(final byte[] input,
			final Charset encoding) throws IOException,
			UnsupportedEncodingException {
		final byte[] uncompressed = uncompress(input);
		return new String(uncompressed);
	}

	@Override
	public byte[] decompress(final byte[] source) throws IOException {
		return uncompress(source);
	}

	@Override
	public byte[] compress(final byte[] source) throws IOException {
		return doCompress(source);
	}
}