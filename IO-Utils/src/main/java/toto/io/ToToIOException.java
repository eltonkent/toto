package toto.io;

/**
 * RAGE IO Exception
 * 
 * @author Mobifluence Interactive
 * 
 */
public class ToToIOException extends RuntimeException {

	public ToToIOException(final String string) {
		super(string);
	}

}
