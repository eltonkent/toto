/*******************************************************************************
\ * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StreamCorruptedException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.mi.toto.ToTo;

public class IOUtils {

	public static enum IOState {
		COMPLETE, CLIENT_ERROR, IN_PROGRESS, NOT_STARTED, PAUSED
	}

	/**
	 * Default buffer size for IOUtils class. (4KB)
	 */
	public static int DEFAULT_BUFFER_SIZE = 1024 * 4;

	/**
	 * This convenience method allows to read a InputStream into a string. The
	 * platform's default character encoding is used for converting bytes into
	 * characters.
	 * 
	 * @param pStream
	 *            The input stream to read.
	 * @see #asString(InputStream, String)
	 * @return The streams contents, as a string.
	 * @throws IOException
	 *             An I/O error occurred.
	 * 
	 */
	public static String asString(final InputStream pStream) throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		copy(pStream, baos, true);
		return baos.toString();
	}

	/**
	 * This convenience method allows to read a InputStream into a string, using
	 * the given character encoding.
	 * 
	 * @param pStream
	 *            The input stream to read.
	 * @param pEncoding
	 *            The character encoding, typically "UTF-8".
	 * @see #asString(InputStream)
	 * @return The streams contents, as a string.
	 * @throws IOException
	 *             An I/O error occurred.
	 */
	public static String asString(final InputStream pStream,
			final String pEncoding) throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		copy(pStream, baos, true);
		return baos.toString(pEncoding);
	}

	/**
	 * Compare the contents of two Streams to determine if they are equal or
	 * not.
	 * <p>
	 * This method buffers the input internally using
	 * <code>BufferedInputStream</code> if they are not already buffered.
	 * 
	 * @param input1
	 *            the first stream
	 * @param input2
	 *            the second stream
	 * @return true if the content of the streams are equal or they both don't
	 *         exist, false otherwise
	 * @throws NullPointerException
	 *             if either input is null
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	public static boolean contentEquals(InputStream input1, InputStream input2)
			throws IOException {
		if (!(input1 instanceof BufferedInputStream)) {
			input1 = new BufferedInputStream(input1);
		}
		if (!(input2 instanceof BufferedInputStream)) {
			input2 = new BufferedInputStream(input2);
		}

		int ch = input1.read();
		while (-1 != ch) {
			final int ch2 = input2.read();
			if (ch != ch2) {
				return false;
			}
			ch = input1.read();
		}

		final int ch2 = input2.read();
		return (ch2 == -1);
	}

	public static byte[] copy(final byte[] source, byte[] target) {
		final int len = source.length;
		if (len > target.length) {
			target = new byte[len];
		}
		System.arraycopy(source, 0, target, 0, len);
		return target;
	}

	/**
	 * 
	 * Copy bytes from an InputStream to an OutputStream.
	 * 
	 * This method buffers the input internally, so there is no need to use a
	 * BufferedInputStream.
	 * 
	 * Large streams (over 2GB) will return a bytes copied value of -1 after the
	 * copy has completed since the correct number of bytes cannot be returned
	 * as an int. For large streams use the copyLarge(InputStream, OutputStream)
	 * method.
	 * 
	 * @param input
	 *            the InputStream to read from
	 * @param output
	 *            the OutputStream to write to
	 * @return the number of bytes copied
	 * @throws IOException
	 *             if an I/O error occurs
	 * @see #DEFAULT_BUFFER_SIZE
	 */
	public static int copy(final InputStream input, final OutputStream output)
			throws IOException {
		final long count = copyLarge(input, output);
		if (count > Integer.MAX_VALUE) {
			return -1;
		}
		return (int) count;
	}

	/**
	 * 
	 * @param in
	 * @param out
	 * @throws IOException
	 * @see #DEFAULT_BUFFER_SIZE
	 * @see #copy(java.io.InputStream, java.io.OutputStream)
	 */
	public static void copyAndClose(final InputStream in, final OutputStream out)
			throws IOException {
		try {
			copy(in, out);
			in.close();
			out.close();
		} catch (final IOException ex) {
			closeSilently(in);
			closeSilently(out);
			// Propagate the original exception
			throw ex;
		}
	}

	static void closeSilently(final Closeable closable) {
		try {
			closable.close();
		} catch (final IOException ignore) {
		}
	}

	/**
	 * Copy the given input stream to a file
	 * 
	 * @param input
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @see #DEFAULT_BUFFER_SIZE
	 */
	public static int copy(final InputStream input, final File file)
			throws FileNotFoundException, IOException {
		return copy(input, new FileOutputStream(file));
	}

	/**
	 * Copies the contents of the given {@link InputStream} to the given
	 * {@link OutputStream}. Shortcut for
	 * 
	 * <pre>
	 * copy(pInputStream, pOutputStream, new byte[8192]);
	 * </pre>
	 * 
	 * @param pInputStream
	 *            The input stream, which is being read. It is guaranteed, that
	 *            {@link InputStream#close()} is called on the stream.
	 * @param pOutputStream
	 *            The output stream, to which data should be written. May be
	 *            null, in which case the input streams contents are simply
	 *            discarded.
	 * @param pClose
	 *            True guarantees, that {@link OutputStream#close()} is called
	 *            on the stream. False indicates, that only
	 *            {@link OutputStream#flush()} should be called finally.
	 * 
	 * @return Number of bytes, which have been copied.
	 * @throws IOException
	 *             An I/O error occurred.
	 * @see #DEFAULT_BUFFER_SIZE
	 */
	public static long copy(final InputStream pInputStream,
			final OutputStream pOutputStream, final boolean pClose)
			throws IOException {
		return copy(pInputStream, pOutputStream, pClose,
				new byte[DEFAULT_BUFFER_SIZE]);
	}

	/**
	 * Converts the input stream to a string by reading one line at a time.
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static String streamWithLinesToString(final InputStream is)
			throws IOException {
		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				is));
		final StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line + "\n");
		}
		is.close();
		return sb.toString();
	}

	/**
	 * Copies the contents of the given {@link InputStream} to the given
	 * {@link OutputStream}.
	 * 
	 * @param pIn
	 *            The input stream, which is being read. It is guaranteed, that
	 *            {@link InputStream#close()} is called on the stream.
	 * @param pOut
	 *            The output stream, to which data should be written. May be
	 *            null, in which case the input streams contents are simply
	 *            discarded.
	 * @param pClose
	 *            True guarantees, that {@link OutputStream#close()} is called
	 *            on the stream. False indicates, that only
	 *            {@link OutputStream#flush()} should be called finally.
	 * @param pBuffer
	 *            Temporary buffer, which is to be used for copying data.
	 * @return Number of bytes, which have been copied.
	 * @throws IOException
	 *             An I/O error occurred.
	 */
	public static long copy(final InputStream pIn, final OutputStream pOut,
			final boolean pClose, final byte[] pBuffer) throws IOException {
		OutputStream out = pOut;
		InputStream in = pIn;
		try {
			long total = 0;
			for (;;) {
				final int res = in.read(pBuffer);
				if (res == -1) {
					break;
				}
				if (res > 0) {
					total += res;
					if (out != null) {
						out.write(pBuffer, 0, res);
					}
				}
			}
			if (out != null) {
				if (pClose) {
					out.close();
				} else {
					out.flush();
				}
				out = null;
			}
			in.close();
			in = null;
			return total;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (final Throwable t) {
					/* Ignore me */
				}
			}
			if (pClose && out != null) {
				try {
					out.close();
				} catch (final Throwable t) {
					/* Ignore me */
				}
			}
		}
	}

	/**
	 * 
	 * @param in
	 * @param out
	 * @param callback
	 * @see #DEFAULT_BUFFER_SIZE
	 */
	public static void copy(final InputStream in, final OutputStream out,
			final IOProgressCallback callback) {
		try {
			final byte[] buff = new byte[DEFAULT_BUFFER_SIZE];
			int bytesRead = 0;
			while ((bytesRead = in.read(buff, 0, buff.length)) != -1) {
				out.write(buff, 0, bytesRead);
				if (callback != null) {
					callback.onProgress(bytesRead);
				}
			}
			out.close();
			in.close();
			if (callback != null)
				callback.onComplete();
		} catch (final IOException e) {
			ToTo.logException(e);
			callback.onError(e);
		}
	}

	/**
	 * Copy bytes from a large (over 2GB) InputStream to an OutputStream.
	 * 
	 * This method buffers the input internally, so there is no need to use a
	 * BufferedInputStream.
	 * 
	 * @param input
	 *            the InputStream to read from
	 * @param output
	 *            the OutputStream to write to
	 * @return the number of bytes copied
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	private static long copyLarge(final InputStream input,
			final OutputStream output) throws IOException {
		final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
		long count = 0;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		output.flush();
		output.close();
		input.close();
		return count;
	}

	/**
	 * De-serialize the byte array to an object.
	 * 
	 * @param data
	 *            the byte array
	 * @return the object
	 * @throws IOException
	 * @throws StreamCorruptedException
	 * @throws ClassNotFoundException
	 */
	public static Object deserialize(final byte[] data)
			throws StreamCorruptedException, IOException,
			ClassNotFoundException {
		final ByteArrayInputStream in = new ByteArrayInputStream(data);
		final ObjectInputStream is = new ObjectInputStream(in);
		return is.readObject();
	}

	/**
	 * Read input from reader and write it to writer until there is no more
	 * input from reader.
	 * 
	 * @param reader
	 *            the reader to read from.
	 * @param writer
	 *            the writer to write to.
	 * @param buf
	 *            the char array to use as a buffer
	 */
	public static void flow(final Reader reader, final Writer writer,
			final char[] buf) throws IOException {
		int numRead;
		while ((numRead = reader.read(buf)) >= 0) {
			writer.write(buf, 0, numRead);
		}
	}

	/**
	 * Compare two inputstreams
	 * 
	 * @param stream1
	 * @param stream2
	 * @return
	 * @throws IOException
	 */
	public static boolean isStreamsEqual(final InputStream stream1,
			final InputStream stream2) throws IOException {
		final byte[] buf1 = new byte[4096];
		final byte[] buf2 = new byte[4096];
		boolean done1 = false;
		boolean done2 = false;
		try {
			while (!done1) {
				int off1 = 0;
				int off2 = 0;

				while (off1 < buf1.length) {
					final int count = stream1.read(buf1, off1, buf1.length
							- off1);
					if (count < 0) {
						done1 = true;
						break;
					}
					off1 += count;
				}
				while (off2 < buf2.length) {
					final int count = stream2.read(buf2, off2, buf2.length
							- off2);
					if (count < 0) {
						done2 = true;
						break;
					}
					off2 += count;
				}
				if (off1 != off2 || done1 != done2)
					return false;
				for (int i = 0; i < off1; i++) {
					if (buf1[i] != buf2[i])
						return false;
				}
			}
			return true;
		} finally {
			stream1.close();
			stream2.close();
		}
	}

	/**
	 * Connects the given inputstream to the outputstream
	 * <p>
	 * The streams are closed after the operation is complete
	 * </p>
	 * 
	 * @param is
	 * @param os
	 * @throws IOException
	 * @see #DEFAULT_BUFFER_SIZE
	 */
	public static void performStreamPiping(final InputStream is,
			final OutputStream os) throws IOException {
		final byte[] buf = new byte[DEFAULT_BUFFER_SIZE];
		while (is.read(buf) != -1) {
			os.write(buf);
		}
		os.close();
		is.close();
	}

	/**
	 * Get the contents of an <code>InputStream</code> as a list of Strings, one
	 * entry per line, using the default character encoding of the platform.
	 * <p>
	 * This method buffers the input internally, so there is no need to use a
	 * <code>BufferedInputStream</code>.
	 * 
	 * @param input
	 *            the <code>InputStream</code> to read from, not null
	 * @return the list of Strings, never null
	 * @throws NullPointerException
	 *             if the input is null
	 * @throws IOException
	 *             if an I/O error occurs
	 * @since Commons IO 1.1
	 */
	public static List<String> readLines(final InputStream input)
			throws IOException {
		final InputStreamReader reader = new InputStreamReader(input);
		return readLines(reader);
	}

	/**
	 * Get the contents of an <code>InputStream</code> as a list of Strings, one
	 * entry per line, using the specified character encoding.
	 * <p>
	 * Character encoding names can be found at <a
	 * href="http://www.iana.org/assignments/character-sets">IANA</a>.
	 * <p>
	 * This method buffers the input internally, so there is no need to use a
	 * <code>BufferedInputStream</code>.
	 * 
	 * @param input
	 *            the <code>InputStream</code> to read from, not null
	 * @param encoding
	 *            the encoding to use, null means platform default
	 * @return the list of Strings, never null
	 * @throws NullPointerException
	 *             if the input is null
	 * @throws IOException
	 *             if an I/O error occurs
	 * @since Commons IO 1.1
	 */
	public static List<String> readLines(final InputStream input,
			final String encoding) throws IOException {
		if (encoding == null) {
			return readLines(input);
		} else {
			final InputStreamReader reader = new InputStreamReader(input,
					encoding);
			return readLines(reader);
		}
	}

	/**
	 * Get the contents of a <code>Reader</code> as a list of Strings, one entry
	 * per line.
	 * <p>
	 * This method buffers the input internally, so there is no need to use a
	 * <code>BufferedReader</code>.
	 * 
	 * @param input
	 *            the <code>Reader</code> to read from, not null
	 * @return the list of Strings, never null
	 * @throws NullPointerException
	 *             if the input is null
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	public static List<String> readLines(final Reader input) throws IOException {
		final BufferedReader reader = new BufferedReader(input);
		final List<String> list = new ArrayList<String>();
		String line = reader.readLine();
		while (line != null) {
			list.add(line);
			line = reader.readLine();
		}
		return list;
	}

	/**
	 * Serialize the object to a byte array.
	 * 
	 * @param obj
	 *            the object to serialize
	 * @return the byte array
	 * @throws IOException
	 */
	public static byte[] serialize(final Object obj) throws IOException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final ObjectOutputStream os = new ObjectOutputStream(out);
		os.writeObject(obj);
		return out.toByteArray();
	}

	/**
	 * Get the contents of an <code>InputStream</code> as a <code>byte[]</code>.
	 * <p>
	 * This method buffers the input internally, so there is no need to use a
	 * <code>BufferedInputStream</code>.
	 * 
	 * @param input
	 *            the <code>InputStream</code> to read from
	 * @return the requested byte array
	 * @throws NullPointerException
	 *             if the input is null
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	public static byte[] toByteArray(final InputStream input)
			throws IOException {
		final ByteArrayOutputStream output = new ByteArrayOutputStream();
		copy(input, output);
		return output.toByteArray();
	}

	/**
	 * Unconditionally close a <code>Closeable</code>.
	 * <p>
	 * Equivalent to {@link Closeable#close()}, except any exceptions will be
	 * ignored. This is typically used in finally blocks.
	 * <p>
	 * Example code:
	 * 
	 * <pre>
	 * Closeable closeable = null;
	 * try {
	 * 	closeable = new FileReader(&quot;foo.txt&quot;);
	 * 	// process closeable
	 * 	closeable.close();
	 * } catch (Exception e) {
	 * 	// error handling
	 * } finally {
	 * 	IOUtils.closeQuietly(closeable);
	 * }
	 * </pre>
	 * 
	 * @param closeable
	 *            the object to close, may be null or already closed
	 * @since 2.0
	 */
	public static void closeQuietly(final Closeable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (final IOException ioe) {
			// ignore
			ToTo.logException(ioe);
		}
	}

	/**
	 * <p>
	 * Read bytes from the <i>source</i> InputStream into the
	 * <code>buffer</code>.
	 * </p>
	 * <p>
	 * This utility ensures that either <code>len</code> bytes are read or the
	 * end of the stream has been reached.
	 * </p>
	 * 
	 * @see InputStream#read(byte[] buf,int off, int len)
	 * @param source
	 *            Stream from which the data is read.
	 * @param buffer
	 *            the buffer into which the data is read.
	 * @param offset
	 *            the start offset in array <code>buffer</code> at which the
	 *            data is written.
	 * @param len
	 *            maximum length of the bytes read.
	 * @return the total number of bytes read into the buffer, or
	 *         <code>-1</code> if there is no more data because the end of the
	 *         stream has been reached.
	 * @throws java.io.IOException
	 *             If the first byte cannot be read for any reason other than
	 *             end of file, or if the input stream has been closed, or if
	 *             some other I/O error occurs.
	 * @throws java.lang.NullPointerException
	 *             If <code>b</code> is <code>null</code>.
	 * @throws java.lang.IndexOutOfBoundsException
	 *             If <code>off</code> is negative, <code>len</code> is
	 *             negative, or <code>len</code> is greater than
	 *             <code>b.length - off</code>
	 */
	public static int tryReadFully(final InputStream source,
			final byte[] buffer, final int offset, final int len)
			throws IOException {
		if (len < 0) {
			throw new IndexOutOfBoundsException("len [" + len + "] < 0");
		}
		int n = 0;
		while (n < len) {
			final int count = source.read(buffer, offset + n, len - n);
			if (count < 0) {
				if (n == 0) {
					n = count;
				}
				break;
			}
			n += count;
		}
		return n;
	}

	/**
	 * Convert the specified string to an input stream, encoded as bytes using
	 * the specified character encoding.
	 * <p>
	 * Character encoding names can be found at <a
	 * href="http://www.iana.org/assignments/character-sets">IANA</a>.
	 * 
	 * @param input
	 *            the string to convert
	 * @param encoding
	 *            the encoding to use, null means platform default
	 * @throws IOException
	 *             if the encoding is invalid
	 * @return an input stream
	 * @since 1.1
	 */
	public static InputStream toInputStream(final String input,
			final String encoding) throws IOException {
		final byte[] bytes = encoding != null ? input.getBytes(encoding)
				: input.getBytes();
		return new ByteArrayInputStream(bytes);
	}

	public static double[] readStreamAsDoubleArray(final InputStream in,
			final long size) throws IOException {
		final int bufferSize = (int) (size / 2);
		final double[] result = new double[bufferSize];
		final DataInputStream is = new DataInputStream(in);
		for (int i = 0; i < bufferSize; i++) {
			result[i] = is.readShort() / 32768.0;
		}
		return result;
	}

	/**
	 * Get the transfer rate in bytes per second
	 * 
	 * @param bytesDownloaded
	 *            total bytes downloaded
	 * @param timeToDownload
	 *            time taken to download <code>byteDownloaded</code> in
	 *            milliseconds.
	 * @return transfer rate in bytes per second.
	 */
	public long getTransferRateInBPS(final long timeToDownload,
			final long bytesDownloaded) {
		return (bytesDownloaded / bytesDownloaded) * 1000;
	}

	/**
	 * Get the transfer rate in kilo bits per second.
	 * 
	 * @param timeToDownload
	 *            time taken to download <code>byteDownloaded</code> in
	 *            milliseconds.
	 * @param bytes
	 * @return transfer rate in kbps.
	 */
	public double getTransferRateInKBPS(final long timeToDownload,
			final long bytesDownloaded) {
		final double BYTE_TO_KILOBIT = 0.0078125;
		final double bytesD = getTransferRateInBPS(timeToDownload,
				bytesDownloaded);
		return bytesD * BYTE_TO_KILOBIT;
	}

	/**
	 * Get the transfer rate in mega bits per second
	 * 
	 * @param timeToDownload
	 *            time taken to download <code>byteDownloaded</code> in
	 *            milliseconds.
	 * @param bytesDownloaded
	 * @return
	 */
	public double getTransferRateInMBPS(final long timeToDownload,
			final long bytesDownloaded) {
		final double KILOBIT_TO_MEGABIT = 0.0009765625;
		return getTransferRateInKBPS(timeToDownload, bytesDownloaded)
				* KILOBIT_TO_MEGABIT;
	}

    public static	String determineEncoding(final InputStream in) throws IOException {
        String encoding = "UTF-8";

        in.mark(4);
        final byte[] check = new byte[4];
        final int size = in.read(check);
        if (size == 2) {
            if (((check[0] & 0xFF) == 0x00 && (check[1] & 0xFF) != 0x00)
                    || ((check[0] & 0xFF) == 0xFE && (check[1] & 0xFF) == 0xFF)) {
                encoding = "UTF-16BE";
            } else if (((check[0] & 0xFF) != 0x00 && (check[1] & 0xFF) == 0x00)
                    || ((check[0] & 0xFF) == 0xFF && (check[1] & 0xFF) == 0xFE)) {
                encoding = "UTF-16LE";
            }
        } else if (size == 4) {
            if (((check[0] & 0xFF) == 0x00 && (check[1] & 0xFF) == 0x00)) {
                encoding = "UTF-32BE";
            } else if (((check[2] & 0xFF) == 0x00 && (check[3] & 0xFF) == 0x00)) {
                encoding = "UTF-32LE";
            } else if (((check[0] & 0xFF) == 0x00 && (check[1] & 0xFF) != 0x00)
                    || ((check[0] & 0xFF) == 0xFE && (check[1] & 0xFF) == 0xFF)) {
                encoding = "UTF-16BE";
            } else if (((check[0] & 0xFF) != 0x00 && (check[1] & 0xFF) == 0x00)
                    || ((check[0] & 0xFF) == 0xFF && (check[1] & 0xFF) == 0xFE)) {
                encoding = "UTF-16LE";
            }
        }
        in.reset();

        return encoding;
    }
}
