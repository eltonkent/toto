/**
 * I/O Utilities to assist with the most complex of I/O tasks with simple yet powerful error handling and management.
 *
 */
package toto.io;