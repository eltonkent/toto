/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io;

import toto.io.IOUtils.IOState;

/**
 * Tracks the progress of an I/O operation
 * 
 * @author elton.stephen.kent
 * 
 */
public interface IOProgressCallback {

	/**
	 * Called when the I/O operation is completed.
	 */
	public void onComplete();

	/**
	 * Called when an I/O error has occurred.
	 * 
	 * @param t
	 */
	public void onError(Throwable t);

	/**
	 * Called during the I/O operation.
	 * 
	 * @param bytesWritten
	 *            number of bytes read/written
	 */
	public void onProgress(int bytes);

	public void progressPercent(double percent);

	public void setIOState(IOState state);
}
