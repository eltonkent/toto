package toto.io;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Parcel serialization utilities
 * 
 * @author elton.kent
 * 
 */
public final class ParcelUtils {

	public static Parcelable readObject(final DataInput datainputstream)
			throws IOException {
		Parcel parcel;
		final byte abyte0[] = new byte[datainputstream.readInt()];
		datainputstream.readFully(abyte0);
		parcel = Parcel.obtain();
		parcel.setDataPosition(0);
		parcel.unmarshall(abyte0, 0, abyte0.length);
		parcel.setDataPosition(0);
		final Parcelable parcelable = parcel.readParcelable(ParcelUtils.class
				.getClassLoader());
		parcel.recycle();
		return parcelable;

	}

	public static void writeObject(final DataOutput dataoutputstream,
			final Parcelable parcelable) throws IOException {
		final Parcel parcel = Parcel.obtain();
		parcel.setDataPosition(0);
		parcel.writeParcelable(parcelable, 0);
		final byte abyte0[] = parcel.marshall();
		dataoutputstream.writeInt(abyte0.length);
		dataoutputstream.write(abyte0);
		parcel.recycle();
	}

	public static void writeToDisk(final File file, final Parcelable parcelable)
			throws IOException {
		file.getParentFile().mkdirs();
		final DataOutputStream dataoutputstream1 = new DataOutputStream(
				new FileOutputStream(file));
		writeObject(dataoutputstream1, parcelable);
	}

	public static Parcelable readFromDisk(final File file) throws IOException {
		final DataInputStream datainputstream1 = new DataInputStream(
				new FileInputStream(file));
		final Parcelable parcelable1 = readObject(datainputstream1);
		return parcelable1;
	}
}
