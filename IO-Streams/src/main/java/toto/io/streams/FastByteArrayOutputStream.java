/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.streams;

import java.io.IOException;
import java.io.OutputStream;

/**
 * ByteArrayOutputStream thats NOT thread safe.
 */
public class FastByteArrayOutputStream extends OutputStream {
	private final byte bytes[];

	private int count = 0;

	public FastByteArrayOutputStream(final int length) {
		bytes = new byte[length];
	}

	public int getBytesWritten() {
		return count;
	}

	public byte[] toByteArray() {
		if (count < bytes.length) {
			final byte result[] = new byte[count];
			System.arraycopy(bytes, 0, result, 0, count);
			return result;
		}
		return bytes;
	}

	@Override
	public void write(final int value) throws IOException {
		if (count >= bytes.length) {
			throw new IOException("Write exceeded expected length (" + count
					+ ", " + bytes.length + ")");
		}

		bytes[count] = (byte) value;
		count++;
	}
}
