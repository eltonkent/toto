package toto.io.streams;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * An OutputStream that counts the number of bytes written.
 * 
 */
public final class CountingOutputStream extends FilterOutputStream {

	private long count;

	/**
	 * Wraps another output stream, counting the number of bytes written.
	 * 
	 * @param out
	 *            the output stream to be wrapped
	 */
	public CountingOutputStream(final OutputStream out) {
		super(out);
	}

	/** Returns the number of bytes written. */
	public long getCount() {
		return count;
	}

	@Override
	public void write(final byte[] b, final int off, final int len)
			throws IOException {
		out.write(b, off, len);
		count += len;
	}

	@Override
	public void write(final int b) throws IOException {
		out.write(b);
		count++;
	}
}
