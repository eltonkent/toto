/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.streams;

import java.io.IOException;
import java.io.OutputStream;

import toto.math.BinaryConstants;

public class BitOutputStream extends OutputStream implements BinaryConstants {
	private int bitCache = 0;
	private int bitsInCache = 0;

	private final int byteOrder;

	private int bytesWritten = 0;

	private final OutputStream os;

	public BitOutputStream(final OutputStream os, final int byteOrder) {
		this.byteOrder = byteOrder;
		this.os = os;
	}

	private void actualWrite(final int value) throws IOException {
		os.write(value);
		bytesWritten++;
	}

	public void flushCache() throws IOException {
		if (bitsInCache > 0) {
			final int bitMask = (1 << bitsInCache) - 1;
			int b = bitMask & bitCache;

			if (byteOrder == BinaryConstants.BYTE_ORDER_NETWORK) // MSB, so write from left
			{
				b <<= 8 - bitsInCache; // left align fragment.
				os.write(b);
			} else if (byteOrder == BinaryConstants.BYTE_ORDER_INTEL) // LSB, so write from
														// right
			{
				os.write(b);
			}
		}

		bitsInCache = 0;
		bitCache = 0;
	}

	public int getBytesWritten() {
		return bytesWritten + ((bitsInCache > 0) ? 1 : 0);
	}

	@Override
	public void write(final int value) throws IOException {
		writeBits(value, 8);
	}

	// TODO: in and out streams CANNOT accurately read/write 32bits at a time,
	// as int will overflow. should have used a long
	public void writeBits(int value, final int SampleBits) throws IOException {
		final int sampleMask = (1 << SampleBits) - 1;
		value &= sampleMask;

		if (byteOrder == BinaryConstants.BYTE_ORDER_NETWORK) // MSB, so add to right
		{
			bitCache = (bitCache << SampleBits) | value;
		} else if (byteOrder == BinaryConstants.BYTE_ORDER_INTEL) // LSB, so add to left
		{
			bitCache = bitCache | (value << bitsInCache);
		} else {
			throw new IOException("Unknown byte order: " + byteOrder);
		}
		bitsInCache += SampleBits;

		while (bitsInCache >= 8) {
			if (byteOrder == BinaryConstants.BYTE_ORDER_NETWORK) // MSB, so write from left
			{
				final int b = 0xff & (bitCache >> (bitsInCache - 8));
				actualWrite(b);

				bitsInCache -= 8;
			} else if (byteOrder == BinaryConstants.BYTE_ORDER_INTEL) // LSB, so write from
														// right
			{
				final int b = 0xff & bitCache;
				actualWrite(b);

				bitCache >>= 8;
				bitsInCache -= 8;
			}
			final int remainderMask = (1 << bitsInCache) - 1; // unneccesary
			bitCache &= remainderMask; // unneccesary
		}

	}

}
