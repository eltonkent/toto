/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.streams;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import toto.io.IOUtils;

/**
 * An input stream that allows you to make copies of any src i/p stream
 * 
 * @author elton.stephen.kent
 * 
 */
public class CopyInputStream {
	private ByteArrayOutputStream _copy;

	public CopyInputStream(final InputStream is) {
		try {
			copy(is);
		} catch (final IOException ex) {
			System.out.println("IOException in CopyInputStream");
			System.out.println(ex.toString());
		}
	}

	private void copy(final InputStream is) throws IOException {
		_copy = new ByteArrayOutputStream();
		IOUtils.copy(is, _copy);
	}

	public InputStream getCopy() {
		return new ByteArrayInputStream(_copy.toByteArray());
	}
}
