/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.streams;

import java.io.IOException;
import java.io.InputStream;

import toto.math.BinaryConstants;

public class BitInputStream extends InputStream implements BinaryConstants {
	private int bitCache = 0;
	private int bitsInCache = 0;
	private final int byteOrder;

	private long bytesRead = 0;

	private final InputStream is;

	private boolean tiffLZWMode = false;

	/**
	 * 
	 * @param is
	 * @param byteOrder
	 *            {@link BinaryConstants}
	 */
	public BitInputStream(final InputStream is, final int byteOrder) {
		this.byteOrder = byteOrder;
		this.is = is;
	}

	public void flushCache() {
		bitsInCache = 0;
		bitCache = 0;
	}

	public long getBytesRead() {
		return bytesRead;
	}

	@Override
	public int read() throws IOException {
		return readBits(8);
	}

	public int readBits(final int SampleBits) throws IOException {
		while (bitsInCache < SampleBits) {
			final int next = is.read();

			if (next < 0) {
				if (tiffLZWMode) {
					// pernicious special case!
					return 257;
				}
				return -1;
			}

			final int newByte = (0xff & next);

			if (byteOrder == BinaryConstants.BYTE_ORDER_NETWORK) {
				bitCache = (bitCache << 8) | newByte;
			} else if (byteOrder == BinaryConstants.BYTE_ORDER_INTEL) {
				bitCache = (newByte << bitsInCache) | bitCache;
			} else {
				throw new IOException("Unknown byte order: " + byteOrder);
			}

			bytesRead++;
			bitsInCache += 8;
		}
		final int sampleMask = (1 << SampleBits) - 1;

		int sample;

		if (byteOrder == BinaryConstants.BYTE_ORDER_NETWORK) // MSB, so read from left
		{
			sample = sampleMask & (bitCache >> (bitsInCache - SampleBits));
		} else if (byteOrder == BinaryConstants.BYTE_ORDER_INTEL) // LSB, so read from right
		{
			sample = sampleMask & bitCache;
			bitCache >>= SampleBits;
		} else {
			throw new IOException("Unknown byte order: " + byteOrder);
		}

		final int result = sample;

		bitsInCache -= SampleBits;
		final int remainderMask = (1 << bitsInCache) - 1;
		bitCache &= remainderMask;

		return result;
	}

	public void setTiffLZWMode() {
		tiffLZWMode = true;
	}

}
