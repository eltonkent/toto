/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.streams;

import java.io.IOException;
import java.io.Writer;

/**
 * Writer implementation that has a bigger buffer.
 * 
 * @author ekent4
 * 
 */
public class QuickWriter {

	private final char[] buffer;
	private int pointer;
	private final Writer writer;

	public QuickWriter(final Writer writer) {
		this(writer, 1024);
	}

	public QuickWriter(final Writer writer, final int bufferSize) {
		this.writer = writer;
		buffer = new char[bufferSize];
	}

	public void close() throws IOException {
		writer.write(buffer, 0, pointer);
		pointer = 0;
		writer.close();
	}

	public void flush() throws IOException {
		writer.write(buffer, 0, pointer);
		pointer = 0;
		writer.flush();

	}

	private void raw(final char c) throws IOException {
		writer.write(c);
		writer.flush();
	}

	private void raw(final char[] c) throws IOException {
		writer.write(c);
		writer.flush();
	}

	public void write(final char c) throws IOException {
		if (pointer + 1 >= buffer.length) {
			flush();
			if (buffer.length == 0) {
				raw(c);
				return;
			}
		}
		buffer[pointer++] = c;
	}

	public void write(final char[] c) throws IOException {
		final int len = c.length;
		if (pointer + len >= buffer.length) {
			flush();
			if (len > buffer.length) {
				raw(c);
				return;
			}
		}
		System.arraycopy(c, 0, buffer, pointer, len);
		pointer += len;
	}

	public void write(final String str) throws IOException {
		final int len = str.length();
		if (pointer + len >= buffer.length) {
			flush();
			if (len > buffer.length) {
				raw(str.toCharArray());
				return;
			}
		}
		str.getChars(0, len, buffer, pointer);
		pointer += len;
	}
}
