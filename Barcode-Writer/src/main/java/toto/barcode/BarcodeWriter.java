package toto.barcode;

import java.io.File;
import java.io.IOException;

import com.mi.toto.ToTo;
import android.util.Log;

/**
 * Barcode writer native interface.
 * 
 * @author Mobifluence Interactive
 * 
 */
class BarcodeWriter {

	private static final int WARN_INVALID_OPTION = 2;
	private static final int ERROR_TOO_LONG = 5;
	private static final int ERROR_INVALID_DATA = 6;
	private static final int ERROR_INVALID_CHECK = 7;
	private static final int ERROR_INVALID_OPTION = 8;
	private static final int ERROR_ENCODING_PROBLEM = 9;
	private static final int ERROR_FILE_ACCESS = 10;
	private static final int ERROR_MEMORY = 11;

	private int _pSymbol = 0;

	private final static String t = BarcodeWriter.class.getName();

	BarcodeWriter() {
		_pSymbol = create();
		if (_pSymbol == 0)
			throw new OutOfMemoryError();
	}

	void setSymbology(final int symbology) {
		setSymbology(_pSymbol, symbology);
	}

	void setHeight(final int height) {
		setHeight(_pSymbol, height);
	}

	void setWhitespace_width(final int whitespace_width) {
		setWhitespace_width(_pSymbol, whitespace_width);
	}

	void setBorder_width(final int border_width) {
		setBorder_width(_pSymbol, border_width);
	}

	void setOutput_options(final int output_options) {
		setOutput_options(_pSymbol, output_options);
	}

	void setFgcolour(final int fgcolour) {
		// Convert fgcolour to String
		final String sFgcolour = CorlorToString(fgcolour);
		setFgcolour(_pSymbol, sFgcolour);
	}

	void setBgcolour(final int bgcolour) {
		// Convert bgcolour to String
		final String sBgcolour = CorlorToString(bgcolour);
		setBgcolour(_pSymbol, sBgcolour);
	}

	void setScale(final float scale) {
		setScale(_pSymbol, scale);
	}

	void setOption_1(final int option_1) {
		setOption_1(_pSymbol, option_1);
	}

	void setOption_2(final int option_2) {
		setOption_2(_pSymbol, option_2);
	}

	void setOption_3(final int option_3) {
		setOption_3(_pSymbol, option_3);
	}

	void setShow_hrt(final int show_hrt) {
		setShow_hrt(_pSymbol, show_hrt);
	}

	void setInput_mode(final int input_mode) {
		setInput_mode(_pSymbol, input_mode);
	}

	void setText(final String text) {
		setText(_pSymbol, text);
	}

	void setPrimary(final String primary) {
		setPrimary(_pSymbol, primary);
	}

	void reset() {
		clear(_pSymbol);
	}

	void encode(final String input) throws RBarcodeException {
		final int RC = encode(_pSymbol, input);
		if (RC != 0 && RC != 2) {
			final String errtxt = getErrtxt(_pSymbol);
			Log.i(t, "encode() RC=" + RC + " errorText:" + errtxt);
			throw new RBarcodeException(RC, errtxt);
		}
		if (RC == 2) {
			Log.i(t, "Warning (RC==2) fro zint: " + getErrtxt(_pSymbol));
		}
	}

	void write(final File outputFile, final int rotation)
			throws RBarcodeException, IOException {
		final String outputFilePath = outputFile.getAbsolutePath();
		setOutfile(_pSymbol, outputFilePath);
		final int RC = print(_pSymbol, rotation);
		if (RC != 0) {
			final String errtxt = getErrtxt(_pSymbol);
			switch (RC) {
			case WARN_INVALID_OPTION:
			case ERROR_TOO_LONG:
			case ERROR_INVALID_DATA:
			case ERROR_INVALID_CHECK:
			case ERROR_INVALID_OPTION:
			case ERROR_ENCODING_PROBLEM:
			case ERROR_MEMORY:
				throw new RBarcodeException(RC, errtxt);
			case ERROR_FILE_ACCESS:
			default:
				throw new IOException(errtxt);
			}
		}
	}

	void delete() {
		delete(_pSymbol);
	}

	@Override
	protected void finalize() throws Throwable {
		delete(_pSymbol);
		super.finalize();
	}

	private static String CorlorToString(final int color) {
		Log.v("Zint", Integer.toString(color));
		String hex = Integer.toHexString(color);
		if (hex.length() > 6)
			hex = hex.substring(2);
		Log.v("RAGE Barcode", hex);
		return hex;
	}

	void setBitmapWidth(final int width) {
		setBitmapWidth(_pSymbol, width);
	}

	void setBitmapHeight(final int height) {
		setBitmapHeight(_pSymbol, height);
	}

	static native void setBitmapWidth(int ipSymbol, int width);

	static native void setBitmapHeight(int ipSymbol, int height);

	static native void setSymbology(int ipSymbol, int symbology);

	static native void setHeight(int ipSymbol, int height);

	static native void setWhitespace_width(int ipSymbol, int whitespace_width);

	static native void setBorder_width(int ipSymbol, int border_width);

	static native void setOutput_options(int ipSymbol, int output_options);

	static native void setFgcolour(int ipSymbol, String fgcolour);

	static native void setBgcolour(int ipSymbol, String bgcolour);

	static native void setOutfile(int ipSymbol, String outfile);

	static native void setScale(int ipSymbol, float scale);

	static native void setOption_1(int ipSymbol, int option_1);

	static native void setOption_2(int ipSymbol, int option_2);

	static native void setOption_3(int ipSymbol, int option_3);

	static native void setShow_hrt(int ipSymbol, int show_hrt);

	static native void setInput_mode(int ipSymbol, int input_mode);

	static native void setText(int ipSymbol, String text);

	static native void setPrimary(int ipSymbol, String primary);

	static native String getErrtxt(int ipSymbol);

	static native int create();

	static native void clear(int ipSymbol);

	static native void delete(int ipSymbol);

	static native int encode(int ipSymbol, String input);

	static native int print(int ipSymbol, int rotation);

	static {
		ToTo.loadLib("Bitmap_PNG");
		ToTo.loadLib("DI_BarcodeWriter");
	}
}
