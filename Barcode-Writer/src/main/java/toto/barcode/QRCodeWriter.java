package toto.barcode;

import java.util.Map;

import toto.util.collections.matrix.BitMatrix;
import toto.util.collections.matrix.ByteMatrix;

/**
 * This object renders a QR Code as a BitMatrix 2D array of greyscale values.
 * 
 */
final class QRCodeWriter implements Writer {

	private static final int QUIET_ZONE_SIZE = 4;

	@Override
	public BitMatrix encode(String contents, BarcodeType format, int width,
			int height) throws BarcodeWriterException {

		return encode(contents, format, width, height, null);
	}

	@Override
	public BitMatrix encode(String contents, BarcodeType format, int width,
			int height, Map<EncodeHintType, ?> hints)
			throws BarcodeWriterException {

		if (toto.text.StringUtils.isEmpty(contents)) {
			throw new InvalidBarcodeDataException("Found empty contents");
		}

		if (format != BarcodeType.QR_CODE) {
			throw new InvalidBarcodeDataException(
					"Can only encode QR_CODE, but got " + format);
		}

		if (width < 0 || height < 0) {
			throw new InvalidBarcodeDataException(
					"Requested dimensions are too small: " + width + 'x'
							+ height);
		}

		QRErrorCorrectionLevel errorCorrectionLevel = QRErrorCorrectionLevel.L;
		int quietZone = QUIET_ZONE_SIZE;
		if (hints != null) {
			QRErrorCorrectionLevel requestedECLevel = (QRErrorCorrectionLevel) hints
					.get(EncodeHintType.ERROR_CORRECTION);
			if (requestedECLevel != null) {
				errorCorrectionLevel = requestedECLevel;
			}
			Integer quietZoneInt = (Integer) hints.get(EncodeHintType.MARGIN);
			if (quietZoneInt != null) {
				quietZone = quietZoneInt;
			}
		}

		QRCode code = QREncoder.encode(contents, errorCorrectionLevel, hints);
		return renderResult(code, width, height, quietZone);
	}

	// Note that the input matrix uses 0 == white, 1 == black, while the output
	// matrix uses
	// 0 == black, 255 == white (i.e. an 8 bit greyscale bitmap).
	private static BitMatrix renderResult(QRCode code, int width, int height,
			int quietZone) {
		ByteMatrix input = code.getMatrix();
		if (input == null) {
			throw new IllegalStateException();
		}
		int inputWidth = input.getWidth();
		int inputHeight = input.getHeight();
		int qrWidth = inputWidth + (quietZone << 1);
		int qrHeight = inputHeight + (quietZone << 1);
		int outputWidth = Math.max(width, qrWidth);
		int outputHeight = Math.max(height, qrHeight);

		int multiple = Math.min(outputWidth / qrWidth, outputHeight / qrHeight);
		// Padding includes both the quiet zone and the extra white pixels to
		// accommodate the requested
		// dimensions. For example, if input is 25x25 the QR will be 33x33
		// including the quiet zone.
		// If the requested size is 200x160, the multiple will be 4, for a QR of
		// 132x132. These will
		// handle all the padding from 100x100 (the actual QR) up to 200x160.
		int leftPadding = (outputWidth - (inputWidth * multiple)) / 2;
		int topPadding = (outputHeight - (inputHeight * multiple)) / 2;

		BitMatrix output = new BitMatrix(outputWidth, outputHeight);

		for (int inputY = 0, outputY = topPadding; inputY < inputHeight; inputY++, outputY += multiple) {
			// Write the contents of this row of the barcode
			for (int inputX = 0, outputX = leftPadding; inputX < inputWidth; inputX++, outputX += multiple) {
				if (input.get(inputX, inputY) == 1) {
					output.setRegion(outputX, outputY, multiple, multiple);
				}
			}
		}

		return output;
	}

}
