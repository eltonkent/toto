/*
 * Copyright 2013 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package toto.barcode;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import toto.util.collections.list.BitArray;
import toto.util.collections.matrix.BitMatrix;

final class AztecWriter implements Writer {

	private static final Charset DEFAULT_CHARSET = Charset
			.forName("ISO-8859-1");

	@Override
	public BitMatrix encode(final String contents, final BarcodeType format,
			final int width, final int height) {
		return encode(contents, format, width, height, null);
	}

	@Override
	public BitMatrix encode(final String contents, final BarcodeType format,
			final int width, final int height,
			final Map<EncodeHintType, ?> hints) {
		final String charset = hints == null ? null : (String) hints
				.get(EncodeHintType.CHARACTER_SET);
		final Number eccPercent = hints == null ? null : (Number) hints
				.get(EncodeHintType.ERROR_CORRECTION);
		final Number layers = hints == null ? null : (Number) hints
				.get(EncodeHintType.AZTEC_LAYERS);
		return encode(
				contents,
				format,
				width,
				height,
				charset == null ? DEFAULT_CHARSET : Charset.forName(charset),
				eccPercent == null ? Encoder.DEFAULT_EC_PERCENT : eccPercent
						.intValue(),
				layers == null ? Encoder.DEFAULT_AZTEC_LAYERS : layers
						.intValue());
	}

	private static BitMatrix encode(final String contents,
			final BarcodeType format, final int width, final int height,
			final Charset charset, final int eccPercent, final int layers) {
		if (format != BarcodeType.AZTEC) {
			throw new InvalidBarcodeDataException(
					"Can only encode AZTEC, but got " + format);
		}
		// post 2.2
		final AztecCode aztec = Encoder.encode(contents.getBytes(charset),
				eccPercent, layers);

		// final AztecCode aztec = Encoder.encode(contents.getBytes(),
		// eccPercent,
		// layers);
		return renderResult(aztec, width, height);
	}

	private static BitMatrix renderResult(final AztecCode code,
			final int width, final int height) {
		final BitMatrix input = code.getMatrix();
		if (input == null) {
			throw new IllegalStateException();
		}
		final int inputWidth = input.getWidth();
		final int inputHeight = input.getHeight();
		final int outputWidth = Math.max(width, inputWidth);
		final int outputHeight = Math.max(height, inputHeight);

		final int multiple = Math.min(outputWidth / inputWidth, outputHeight
				/ inputHeight);
		final int leftPadding = (outputWidth - (inputWidth * multiple)) / 2;
		final int topPadding = (outputHeight - (inputHeight * multiple)) / 2;

		final BitMatrix output = new BitMatrix(outputWidth, outputHeight);

		for (int inputY = 0, outputY = topPadding; inputY < inputHeight; inputY++, outputY += multiple) {
			// Write the contents of this row of the barcode
			for (int inputX = 0, outputX = leftPadding; inputX < inputWidth; inputX++, outputX += multiple) {
				if (input.get(inputX, inputY)) {
					output.setRegion(outputX, outputY, multiple, multiple);
				}
			}
		}
		return output;
	}

	/**
	 * Aztec 2D code representation
	 * 
	 * @author Rustam Abdullaev
	 */
	static final class AztecCode {

		private boolean compact;
		private int size;
		private int layers;
		private int codeWords;
		private BitMatrix matrix;

		/**
		 * Compact or full symbol indicator
		 */
		boolean isCompact() {
			return compact;
		}

		void setCompact(final boolean compact) {
			this.compact = compact;
		}

		/**
		 * Size in pixels (width and height)
		 */
		int getSize() {
			return size;
		}

		void setSize(final int size) {
			this.size = size;
		}

		/**
		 * Number of levels
		 */
		int getLayers() {
			return layers;
		}

		void setLayers(final int layers) {
			this.layers = layers;
		}

		/**
		 * Number of data codewords
		 */
		int getCodeWords() {
			return codeWords;
		}

		void setCodeWords(final int codeWords) {
			this.codeWords = codeWords;
		}

		/**
		 * The symbol image
		 */
		BitMatrix getMatrix() {
			return matrix;
		}

		void setMatrix(final BitMatrix matrix) {
			this.matrix = matrix;
		}

	}

	/**
	 * Generates Aztec 2D barcodes.
	 * 
	 * @author Rustam Abdullaev
	 */
	private static final class Encoder {

		static final int DEFAULT_EC_PERCENT = 33; // default minimal percentage
													// of error check words
		static final int DEFAULT_AZTEC_LAYERS = 0;
		private static final int MAX_NB_BITS = 32;
		private static final int MAX_NB_BITS_COMPACT = 4;

		private static final int[] WORD_SIZE = { 4, 6, 6, 8, 8, 8, 8, 8, 8, 10,
				10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12,
				12, 12, 12, 12, 12, 12, 12 };

		private Encoder() {
		}

		/**
		 * Encodes the given binary content as an Aztec symbol
		 * 
		 * @param data
		 *            input data string
		 * @return Aztec symbol matrix with metadata
		 */
		static AztecCode encode(final byte[] data) {
			return encode(data, DEFAULT_EC_PERCENT, DEFAULT_AZTEC_LAYERS);
		}

		/**
		 * Encodes the given binary content as an Aztec symbol
		 * 
		 * @param data
		 *            input data string
		 * @param minECCPercent
		 *            minimal percentage of error check words (According to
		 *            ISO/IEC 24778:2008, a minimum of 23% + 3 words is
		 *            recommended)
		 * @param userSpecifiedLayers
		 *            if non-zero, a user-specified value for the number of
		 *            layers
		 * @return Aztec symbol matrix with metadata
		 */
		static AztecCode encode(final byte[] data, final int minECCPercent,
				final int userSpecifiedLayers) {
			// High-level encode
			final BitArray bits = new HighLevelEncoder(data).encode();

			// stuff bits and choose symbol size
			final int eccBits = bits.getSize() * minECCPercent / 100 + 11;
			final int totalSizeBits = bits.getSize() + eccBits;
			boolean compact;
			int layers;
			int totalBitsInLayer;
			int wordSize;
			BitArray stuffedBits;
			if (userSpecifiedLayers != DEFAULT_AZTEC_LAYERS) {
				compact = userSpecifiedLayers < 0;
				layers = Math.abs(userSpecifiedLayers);
				if (layers > (compact ? MAX_NB_BITS_COMPACT : MAX_NB_BITS)) {
					throw new InvalidBarcodeDataException(String.format(
							"Illegal value %s for layers", userSpecifiedLayers));
				}
				totalBitsInLayer = totalBitsInLayer(layers, compact);
				wordSize = WORD_SIZE[layers];
				final int usableBitsInLayers = totalBitsInLayer
						- (totalBitsInLayer % wordSize);
				stuffedBits = stuffBits(bits, wordSize);
				if (stuffedBits.getSize() + eccBits > usableBitsInLayers) {
					throw new InvalidBarcodeDataException(
							"Data to large for user specified layer");
				}
				if (compact && stuffedBits.getSize() > wordSize * 64) {
					// Compact format only allows 64 data words, though C4 can
					// hold more words than that
					throw new InvalidBarcodeDataException(
							"Data to large for user specified layer");
				}
			} else {
				wordSize = 0;
				stuffedBits = null;
				// We look at the possible table sizes in the order Compact1,
				// Compact2, Compact3,
				// Compact4, Normal4,... Normal(i) for i < 4 isn't typically
				// used since Compact(i+1)
				// is the same size, but has more data.
				for (int i = 0;; i++) {
					if (i > MAX_NB_BITS) {
						throw new InvalidBarcodeDataException(
								"Data too large for an Aztec code");
					}
					compact = i <= 3;
					layers = compact ? i + 1 : i;
					totalBitsInLayer = totalBitsInLayer(layers, compact);
					if (totalSizeBits > totalBitsInLayer) {
						continue;
					}
					// [Re]stuff the bits if this is the first opportunity, or
					// if the
					// wordSize has changed
					if (wordSize != WORD_SIZE[layers]) {
						wordSize = WORD_SIZE[layers];
						stuffedBits = stuffBits(bits, wordSize);
					}
					final int usableBitsInLayers = totalBitsInLayer
							- (totalBitsInLayer % wordSize);
					if (compact && stuffedBits.getSize() > wordSize * 64) {
						// Compact format only allows 64 data words, though C4
						// can hold more words than that
						continue;
					}
					if (stuffedBits.getSize() + eccBits <= usableBitsInLayers) {
						break;
					}
				}
			}
			final BitArray messageBits = generateCheckWords(stuffedBits,
					totalBitsInLayer, wordSize);

			// generate mode message
			final int messageSizeInWords = stuffedBits.getSize() / wordSize;
			final BitArray modeMessage = generateModeMessage(compact, layers,
					messageSizeInWords);

			// allocate symbol
			final int baseMatrixSize = compact ? 11 + layers * 4
					: 14 + layers * 4; // not including alignment lines
			final int[] alignmentMap = new int[baseMatrixSize];
			int matrixSize;
			if (compact) {
				// no alignment marks in compact mode, alignmentMap is a no-op
				matrixSize = baseMatrixSize;
				for (int i = 0; i < alignmentMap.length; i++) {
					alignmentMap[i] = i;
				}
			} else {
				matrixSize = baseMatrixSize + 1 + 2
						* ((baseMatrixSize / 2 - 1) / 15);
				final int origCenter = baseMatrixSize / 2;
				final int center = matrixSize / 2;
				for (int i = 0; i < origCenter; i++) {
					final int newOffset = i + i / 15;
					alignmentMap[origCenter - i - 1] = center - newOffset - 1;
					alignmentMap[origCenter + i] = center + newOffset + 1;
				}
			}
			final BitMatrix matrix = new BitMatrix(matrixSize);

			// draw data bits
			for (int i = 0, rowOffset = 0; i < layers; i++) {
				final int rowSize = compact ? (layers - i) * 4 + 9
						: (layers - i) * 4 + 12;
				for (int j = 0; j < rowSize; j++) {
					final int columnOffset = j * 2;
					for (int k = 0; k < 2; k++) {
						if (messageBits.get(rowOffset + columnOffset + k)) {
							matrix.set(alignmentMap[i * 2 + k], alignmentMap[i
									* 2 + j]);
						}
						if (messageBits.get(rowOffset + rowSize * 2
								+ columnOffset + k)) {
							matrix.set(
									alignmentMap[i * 2 + j],
									alignmentMap[baseMatrixSize - 1 - i * 2 - k]);
						}
						if (messageBits.get(rowOffset + rowSize * 4
								+ columnOffset + k)) {
							matrix.set(alignmentMap[baseMatrixSize - 1 - i * 2
									- k], alignmentMap[baseMatrixSize - 1 - i
									* 2 - j]);
						}
						if (messageBits.get(rowOffset + rowSize * 6
								+ columnOffset + k)) {
							matrix.set(alignmentMap[baseMatrixSize - 1 - i * 2
									- j], alignmentMap[i * 2 + k]);
						}
					}
				}
				rowOffset += rowSize * 8;
			}

			// draw mode message
			drawModeMessage(matrix, compact, matrixSize, modeMessage);

			// draw alignment marks
			if (compact) {
				drawBullsEye(matrix, matrixSize / 2, 5);
			} else {
				drawBullsEye(matrix, matrixSize / 2, 7);
				for (int i = 0, j = 0; i < baseMatrixSize / 2 - 1; i += 15, j += 16) {
					for (int k = (matrixSize / 2) & 1; k < matrixSize; k += 2) {
						matrix.set(matrixSize / 2 - j, k);
						matrix.set(matrixSize / 2 + j, k);
						matrix.set(k, matrixSize / 2 - j);
						matrix.set(k, matrixSize / 2 + j);
					}
				}
			}

			final AztecCode aztec = new AztecCode();
			aztec.setCompact(compact);
			aztec.setSize(matrixSize);
			aztec.setLayers(layers);
			aztec.setCodeWords(messageSizeInWords);
			aztec.setMatrix(matrix);
			return aztec;
		}

		private static void drawBullsEye(final BitMatrix matrix,
				final int center, final int size) {
			for (int i = 0; i < size; i += 2) {
				for (int j = center - i; j <= center + i; j++) {
					matrix.set(j, center - i);
					matrix.set(j, center + i);
					matrix.set(center - i, j);
					matrix.set(center + i, j);
				}
			}
			matrix.set(center - size, center - size);
			matrix.set(center - size + 1, center - size);
			matrix.set(center - size, center - size + 1);
			matrix.set(center + size, center - size);
			matrix.set(center + size, center - size + 1);
			matrix.set(center + size, center + size - 1);
		}

		static BitArray generateModeMessage(final boolean compact,
				final int layers, final int messageSizeInWords) {
			BitArray modeMessage = new BitArray();
			if (compact) {
				modeMessage.appendBits(layers - 1, 2);
				modeMessage.appendBits(messageSizeInWords - 1, 6);
				modeMessage = generateCheckWords(modeMessage, 28, 4);
			} else {
				modeMessage.appendBits(layers - 1, 5);
				modeMessage.appendBits(messageSizeInWords - 1, 11);
				modeMessage = generateCheckWords(modeMessage, 40, 4);
			}
			return modeMessage;
		}

		private static void drawModeMessage(final BitMatrix matrix,
				final boolean compact, final int matrixSize,
				final BitArray modeMessage) {
			final int center = matrixSize / 2;
			if (compact) {
				for (int i = 0; i < 7; i++) {
					final int offset = center - 3 + i;
					if (modeMessage.get(i)) {
						matrix.set(offset, center - 5);
					}
					if (modeMessage.get(i + 7)) {
						matrix.set(center + 5, offset);
					}
					if (modeMessage.get(20 - i)) {
						matrix.set(offset, center + 5);
					}
					if (modeMessage.get(27 - i)) {
						matrix.set(center - 5, offset);
					}
				}
			} else {
				for (int i = 0; i < 10; i++) {
					final int offset = center - 5 + i + i / 5;
					if (modeMessage.get(i)) {
						matrix.set(offset, center - 7);
					}
					if (modeMessage.get(i + 10)) {
						matrix.set(center + 7, offset);
					}
					if (modeMessage.get(29 - i)) {
						matrix.set(offset, center + 7);
					}
					if (modeMessage.get(39 - i)) {
						matrix.set(center - 7, offset);
					}
				}
			}
		}

		private static BitArray generateCheckWords(final BitArray bitArray,
				final int totalBits, final int wordSize) {
			// bitArray is guaranteed to be a multiple of the wordSize, so no
			// padding needed
			final int messageSizeInWords = bitArray.getSize() / wordSize;
			final ReedSolomonEncoder rs = new ReedSolomonEncoder(
					getGF(wordSize));
			final int totalWords = totalBits / wordSize;
			final int[] messageWords = bitsToWords(bitArray, wordSize,
					totalWords);
			rs.encode(messageWords, totalWords - messageSizeInWords);
			final int startPad = totalBits % wordSize;
			final BitArray messageBits = new BitArray();
			messageBits.appendBits(0, startPad);
			for (final int messageWord : messageWords) {
				messageBits.appendBits(messageWord, wordSize);
			}
			return messageBits;
		}

		private static int[] bitsToWords(final BitArray stuffedBits,
				final int wordSize, final int totalWords) {
			final int[] message = new int[totalWords];
			int i;
			int n;
			for (i = 0, n = stuffedBits.getSize() / wordSize; i < n; i++) {
				int value = 0;
				for (int j = 0; j < wordSize; j++) {
					value |= stuffedBits.get(i * wordSize + j) ? (1 << wordSize
							- j - 1) : 0;
				}
				message[i] = value;
			}
			return message;
		}

		private static GenericGF getGF(final int wordSize) {
			switch (wordSize) {
			case 4:
				return GenericGF.AZTEC_PARAM;
			case 6:
				return GenericGF.AZTEC_DATA_6;
			case 8:
				return GenericGF.AZTEC_DATA_8;
			case 10:
				return GenericGF.AZTEC_DATA_10;
			case 12:
				return GenericGF.AZTEC_DATA_12;
			default:
				return null;
			}
		}

		static BitArray stuffBits(final BitArray bits, final int wordSize) {
			final BitArray out = new BitArray();

			final int n = bits.getSize();
			final int mask = (1 << wordSize) - 2;
			for (int i = 0; i < n; i += wordSize) {
				int word = 0;
				for (int j = 0; j < wordSize; j++) {
					if (i + j >= n || bits.get(i + j)) {
						word |= 1 << (wordSize - 1 - j);
					}
				}
				if ((word & mask) == mask) {
					out.appendBits(word & mask, wordSize);
					i--;
				} else if ((word & mask) == 0) {
					out.appendBits(word | 1, wordSize);
					i--;
				} else {
					out.appendBits(word, wordSize);
				}
			}
			return out;
		}

		private static int totalBitsInLayer(final int layers,
				final boolean compact) {
			return ((compact ? 88 : 112) + 16 * layers) * layers;
		}
	}

	/**
	 * This produces nearly optimal encodings of text into the first-level of
	 * encoding used by Aztec code.
	 * 
	 * It uses a dynamic algorithm. For each prefix of the string, it determines
	 * a setKey of encodings that could lead to this prefix. We repeatedly add a
	 * character and generate a new setKey of optimal encodings until we have
	 * read through the entire input.
	 * 
	 * @author Frank Yellin
	 * @author Rustam Abdullaev
	 */
	static final class HighLevelEncoder {

		static final String[] MODE_NAMES = { "UPPER", "LOWER", "DIGIT",
				"MIXED", "PUNCT" };

		static final int MODE_UPPER = 0; // 5 bits
		static final int MODE_LOWER = 1; // 5 bits
		static final int MODE_DIGIT = 2; // 4 bits
		static final int MODE_MIXED = 3; // 5 bits
		static final int MODE_PUNCT = 4; // 5 bits

		// The Latch Table shows, for each pair of Modes, the optimal method for
		// getting from one mode to another. In the worst possible case, this
		// can
		// be up to 14 bits. In the best possible case, we are already there!
		// The high half-word of each entry gives the number of bits.
		// The low half-word of each entry are the actual bits necessary to
		// change
		static final int[][] LATCH_TABLE = { { 0, (5 << 16) + 28, // UPPER ->
																	// LOWER
				(5 << 16) + 30, // UPPER -> DIGIT
				(5 << 16) + 29, // UPPER -> MIXED
				(10 << 16) + (29 << 5) + 30, // UPPER -> MIXED -> PUNCT
		}, { (9 << 16) + (30 << 4) + 14, // LOWER -> DIGIT -> UPPER
				0, (5 << 16) + 30, // LOWER -> DIGIT
				(5 << 16) + 29, // LOWER -> MIXED
				(10 << 16) + (29 << 5) + 30, // LOWER -> MIXED -> PUNCT
		}, { (4 << 16) + 14, // DIGIT -> UPPER
				(9 << 16) + (14 << 5) + 28, // DIGIT -> UPPER -> LOWER
				0, (9 << 16) + (14 << 5) + 29, // DIGIT -> UPPER -> MIXED
				(14 << 16) + (14 << 10) + (29 << 5) + 30,
				// DIGIT -> UPPER -> MIXED -> PUNCT
				}, { (5 << 16) + 29, // MIXED -> UPPER
						(5 << 16) + 28, // MIXED -> LOWER
						(10 << 16) + (29 << 5) + 30, // MIXED -> UPPER -> DIGIT
						0, (5 << 16) + 30, // MIXED -> PUNCT
				}, { (5 << 16) + 31, // PUNCT -> UPPER
						(10 << 16) + (31 << 5) + 28, // PUNCT -> UPPER -> LOWER
						(10 << 16) + (31 << 5) + 30, // PUNCT -> UPPER -> DIGIT
						(10 << 16) + (31 << 5) + 29, // PUNCT -> UPPER -> MIXED
						0, }, };

		// A reverse mapping from [mode][char] to the encoding for that
		// character
		// in that mode. An entry of 0 indicates no mapping exists.
		private static final int[][] CHAR_MAP = new int[5][256];
		static {
			CHAR_MAP[MODE_UPPER][' '] = 1;
			for (int c = 'A'; c <= 'Z'; c++) {
				CHAR_MAP[MODE_UPPER][c] = c - 'A' + 2;
			}
			CHAR_MAP[MODE_LOWER][' '] = 1;
			for (int c = 'a'; c <= 'z'; c++) {
				CHAR_MAP[MODE_LOWER][c] = c - 'a' + 2;
			}
			CHAR_MAP[MODE_DIGIT][' '] = 1;
			for (int c = '0'; c <= '9'; c++) {
				CHAR_MAP[MODE_DIGIT][c] = c - '0' + 2;
			}
			CHAR_MAP[MODE_DIGIT][','] = 12;
			CHAR_MAP[MODE_DIGIT]['.'] = 13;
			final int[] mixedTable = { '\0', ' ', '\1', '\2', '\3', '\4', '\5',
					'\6', '\7', '\b', '\t', '\n', '\13', '\f', '\r', '\33',
					'\34', '\35', '\36', '\37', '@', '\\', '^', '_', '`', '|',
					'~', '\177' };
			for (int i = 0; i < mixedTable.length; i++) {
				CHAR_MAP[MODE_MIXED][mixedTable[i]] = i;
			}
			final int[] punctTable = { '\0', '\r', '\0', '\0', '\0', '\0', '!',
					'\'', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',',
					'-', '.', '/', ':', ';', '<', '=', '>', '?', '[', ']', '{',
					'}' };
			for (int i = 0; i < punctTable.length; i++) {
				if (punctTable[i] > 0) {
					CHAR_MAP[MODE_PUNCT][punctTable[i]] = i;
				}
			}
		}

		// A map showing the available shift codes. (The shifts to BINARY are
		// not
		// shown
		static final int[][] SHIFT_TABLE = new int[6][6]; // mode shift codes,
															// per table
		static {
			for (final int[] table : SHIFT_TABLE) {
				Arrays.fill(table, -1);
			}
			SHIFT_TABLE[MODE_UPPER][MODE_PUNCT] = 0;

			SHIFT_TABLE[MODE_LOWER][MODE_PUNCT] = 0;
			SHIFT_TABLE[MODE_LOWER][MODE_UPPER] = 28;

			SHIFT_TABLE[MODE_MIXED][MODE_PUNCT] = 0;

			SHIFT_TABLE[MODE_DIGIT][MODE_PUNCT] = 0;
			SHIFT_TABLE[MODE_DIGIT][MODE_UPPER] = 15;
		}

		private final byte[] text;

		HighLevelEncoder(final byte[] text) {
			this.text = text;
		}

		/**
		 * Convert the text represented by this High Level LameEncoder into a
		 * BitArray.
		 */
		BitArray encode() {
			Collection<State> states = Collections
					.singletonList(State.INITIAL_STATE);
			for (int index = 0; index < text.length; index++) {
				int pairCode;
				final int nextChar = index + 1 < text.length ? text[index + 1]
						: 0;
				switch (text[index]) {
				case '\r':
					pairCode = nextChar == '\n' ? 2 : 0;
					break;
				case '.':
					pairCode = nextChar == ' ' ? 3 : 0;
					break;
				case ',':
					pairCode = nextChar == ' ' ? 4 : 0;
					break;
				case ':':
					pairCode = nextChar == ' ' ? 5 : 0;
					break;
				default:
					pairCode = 0;
				}
				if (pairCode > 0) {
					// We have one of the four special PUNCT pairs. Treat them
					// specially.
					// Get a new setKey of states for the two new characters.
					states = updateStateListForPair(states, index, pairCode);
					index++;
				} else {
					// Get a new setKey of states for the new character.
					states = updateStateListForChar(states, index);
				}
			}
			// We are left with a setKey of states. Find the shortest one.
			final State minState = Collections.min(states,
					new Comparator<State>() {
						@Override
						public int compare(final State a, final State b) {
							return a.getBitCount() - b.getBitCount();
						}
					});
			// Convert it to a bit array, and return.
			return minState.toBitArray(text);
		}

		// We update a setKey of states for a new character by updating each
		// state
		// for the new character, merging the results, and then removing the
		// non-optimal states.
		private Collection<State> updateStateListForChar(
				final Iterable<State> states, final int index) {
			final Collection<State> result = new LinkedList<State>();
			for (final State state : states) {
				updateStateForChar(state, index, result);
			}
			return simplifyStates(result);
		}

		// Return a setKey of states that represent the possible ways of
		// updating this
		// state for the next character. The resulting setKey of states are
		// added to
		// the "result" list.
		private void updateStateForChar(final State state, final int index,
				final Collection<State> result) {
			final char ch = (char) (text[index] & 0xFF);
			final boolean charInCurrentTable = CHAR_MAP[state.getMode()][ch] > 0;
			State stateNoBinary = null;
			for (int mode = 0; mode <= MODE_PUNCT; mode++) {
				final int charInMode = CHAR_MAP[mode][ch];
				if (charInMode > 0) {
					if (stateNoBinary == null) {
						// Only create stateNoBinary the first time it's
						// required.
						stateNoBinary = state.endBinaryShift(index);
					}
					// Try generating the character by latching to its mode
					if (!charInCurrentTable || mode == state.getMode()
							|| mode == MODE_DIGIT) {
						// If the character is in the current table, we don't
						// want to latch to
						// any other mode except possibly digit (which uses only
						// 4 bits). Any
						// other latch would be equally successful *after* this
						// character, and
						// so wouldn't save any bits.
						final State latch_state = stateNoBinary.latchAndAppend(
								mode, charInMode);
						result.add(latch_state);
					}
					// Try generating the character by switching to its mode.
					if (!charInCurrentTable
							&& SHIFT_TABLE[state.getMode()][mode] >= 0) {
						// It never makes sense to temporarily shift to another
						// mode if the
						// character exists in the current mode. That can never
						// save bits.
						final State shift_state = stateNoBinary.shiftAndAppend(
								mode, charInMode);
						result.add(shift_state);
					}
				}
			}
			if (state.getBinaryShiftByteCount() > 0
					|| CHAR_MAP[state.getMode()][ch] == 0) {
				// It's never worthwhile to go into binary shift mode if you're
				// not already
				// in binary shift mode, and the character exists in your
				// current mode.
				// That can never save bits over just outputting the char in the
				// current mode.
				final State binaryState = state.addBinaryShiftChar(index);
				result.add(binaryState);
			}
		}

		private static Collection<State> updateStateListForPair(
				final Iterable<State> states, final int index,
				final int pairCode) {
			final Collection<State> result = new LinkedList<State>();
			for (final State state : states) {
				updateStateForPair(state, index, pairCode, result);
			}
			return simplifyStates(result);
		}

		private static void updateStateForPair(final State state,
				final int index, final int pairCode,
				final Collection<State> result) {
			final State stateNoBinary = state.endBinaryShift(index);
			// Possibility 1. Latch to MODE_PUNCT, and then append this code
			result.add(stateNoBinary.latchAndAppend(MODE_PUNCT, pairCode));
			if (state.getMode() != MODE_PUNCT) {
				// Possibility 2. Shift to MODE_PUNCT, and then append this
				// code.
				// Every state except MODE_PUNCT (handled above) can shift
				result.add(stateNoBinary.shiftAndAppend(MODE_PUNCT, pairCode));
			}
			if (pairCode == 3 || pairCode == 4) {
				// both characters are in DIGITS. Sometimes better to just add
				// two digits
				final State digit_state = stateNoBinary.latchAndAppend(
						MODE_DIGIT, 16 - pairCode) // period or comma in DIGIT
						.latchAndAppend(MODE_DIGIT, 1); // space in DIGIT
				result.add(digit_state);
			}
			if (state.getBinaryShiftByteCount() > 0) {
				// It only makes sense to do the characters as binary if we're
				// already
				// in binary mode.
				final State binaryState = state.addBinaryShiftChar(index)
						.addBinaryShiftChar(index + 1);
				result.add(binaryState);
			}
		}

		private static Collection<State> simplifyStates(
				final Iterable<State> states) {
			final List<State> result = new LinkedList<State>();
			for (final State newState : states) {
				boolean add = true;
				for (final Iterator<State> iterator = result.iterator(); iterator
						.hasNext();) {
					final State oldState = iterator.next();
					if (oldState.isBetterThanOrEqualTo(newState)) {
						add = false;
						break;
					}
					if (newState.isBetterThanOrEqualTo(oldState)) {
						iterator.remove();
					}
				}
				if (add) {
					result.add(newState);
				}
			}
			return result;
		}

	}

	/**
	 * State represents all information about a sequence necessary to generate
	 * the current output. Note that a state is immutable.
	 */
	static final class State {

		static final State INITIAL_STATE = new State(Token.EMPTY,
				HighLevelEncoder.MODE_UPPER, 0, 0);

		// The current mode of the encoding (or the mode to which we'll return
		// if
		// we're in Binary Shift mode.
		private final int mode;
		// The list of tokens that we output. If we are in Binary Shift mode,
		// this
		// token list does *not* yet included the token for those bytes
		private final Token token;
		// If non-zero, the number of most recent bytes that should be output
		// in Binary Shift mode.
		private final int binaryShiftByteCount;
		// The total number of bits generated (including Binary Shift).
		private final int bitCount;

		private State(final Token token, final int mode, final int binaryBytes,
				final int bitCount) {
			this.token = token;
			this.mode = mode;
			this.binaryShiftByteCount = binaryBytes;
			this.bitCount = bitCount;
			// Make sure we match the token
			// int binaryShiftBitCount = (binaryShiftByteCount * 8) +
			// (binaryShiftByteCount == 0 ? 0 :
			// binaryShiftByteCount <= 31 ? 10 :
			// binaryShiftByteCount <= 62 ? 20 : 21);
			// assert this.bitCount == token.getTotalBitCount() +
			// binaryShiftBitCount;
		}

		int getMode() {
			return mode;
		}

		Token getToken() {
			return token;
		}

		int getBinaryShiftByteCount() {
			return binaryShiftByteCount;
		}

		int getBitCount() {
			return bitCount;
		}

		// Create a new state representing this state with a latch to a (not
		// necessary different) mode, and then a code.
		State latchAndAppend(final int mode, final int value) {
			// assert binaryShiftByteCount == 0;
			int bitCount = this.bitCount;
			Token token = this.token;
			if (mode != this.mode) {
				final int latch = HighLevelEncoder.LATCH_TABLE[this.mode][mode];
				token = token.add(latch & 0xFFFF, latch >> 16);
				bitCount += latch >> 16;
			}
			final int latchModeBitCount = mode == HighLevelEncoder.MODE_DIGIT ? 4
					: 5;
			token = token.add(value, latchModeBitCount);
			return new State(token, mode, 0, bitCount + latchModeBitCount);
		}

		// Create a new state representing this state, with a temporary shift
		// to a different mode to output a single value.
		State shiftAndAppend(final int mode, final int value) {
			// assert binaryShiftByteCount == 0 && this.mode != mode;
			Token token = this.token;
			final int thisModeBitCount = this.mode == HighLevelEncoder.MODE_DIGIT ? 4
					: 5;
			// Shifts exist only to UPPER and PUNCT, both with tokens size 5.
			token = token.add(HighLevelEncoder.SHIFT_TABLE[this.mode][mode],
					thisModeBitCount);
			token = token.add(value, 5);
			return new State(token, this.mode, 0, this.bitCount
					+ thisModeBitCount + 5);
		}

		// Create a new state representing this state, but an additional
		// character
		// output in Binary Shift mode.
		State addBinaryShiftChar(final int index) {
			Token token = this.token;
			int mode = this.mode;
			int bitCount = this.bitCount;
			if (this.mode == HighLevelEncoder.MODE_PUNCT
					|| this.mode == HighLevelEncoder.MODE_DIGIT) {
				// assert binaryShiftByteCount == 0;
				final int latch = HighLevelEncoder.LATCH_TABLE[mode][HighLevelEncoder.MODE_UPPER];
				token = token.add(latch & 0xFFFF, latch >> 16);
				bitCount += latch >> 16;
				mode = HighLevelEncoder.MODE_UPPER;
			}
			final int deltaBitCount = (binaryShiftByteCount == 0 || binaryShiftByteCount == 31) ? 18
					: (binaryShiftByteCount == 62) ? 9 : 8;
			State result = new State(token, mode, binaryShiftByteCount + 1,
					bitCount + deltaBitCount);
			if (result.binaryShiftByteCount == 2047 + 31) {
				// The string is as long as it's allowed to be. We should end
				// it.
				result = result.endBinaryShift(index + 1);
			}
			return result;
		}

		// Create the state identical to this one, but we are no longer in
		// Binary Shift mode.
		State endBinaryShift(final int index) {
			if (binaryShiftByteCount == 0) {
				return this;
			}
			Token token = this.token;
			token = token.addBinaryShift(index - binaryShiftByteCount,
					binaryShiftByteCount);
			// assert token.getTotalBitCount() == this.bitCount;
			return new State(token, mode, 0, this.bitCount);
		}

		// Returns true if "this" state is better (or equal) to be in than
		// "that"
		// state under all possible circumstances.
		boolean isBetterThanOrEqualTo(final State other) {
			int mySize = this.bitCount
					+ (HighLevelEncoder.LATCH_TABLE[this.mode][other.mode] >> 16);
			if (other.binaryShiftByteCount > 0
					&& (this.binaryShiftByteCount == 0 || this.binaryShiftByteCount > other.binaryShiftByteCount)) {
				mySize += 10; // Cost of entering Binary Shift mode.
			}
			return mySize <= other.bitCount;
		}

		BitArray toBitArray(final byte[] text) {
			// Reverse the tokens, so that they are in the order that they
			// should
			// be output
			final Deque<Token> symbols = new LinkedList<Token>();
			for (Token token = endBinaryShift(text.length).token; token != null; token = token
					.getPrevious()) {
				symbols.addFirst(token);
			}
			final BitArray bitArray = new BitArray();
			// Add each token to the result.
			for (final Token symbol : symbols) {
				symbol.appendTo(bitArray, text);
			}
			// assert bitArray.getSize() == this.bitCount;
			return bitArray;
		}

		@Override
		public String toString() {
			return String.format("%s bits=%d bytes=%d",
					HighLevelEncoder.MODE_NAMES[mode], bitCount,
					binaryShiftByteCount);
		}

	}

	final static class SimpleToken extends Token {

		// For normal words, indicates value and bitCount
		private final short value;
		private final short bitCount;

		SimpleToken(final Token previous, final int value, final int bitCount) {
			super(previous);
			this.value = (short) value;
			this.bitCount = (short) bitCount;
		}

		@Override
		void appendTo(final BitArray bitArray, final byte[] text) {
			bitArray.appendBits(value, bitCount);
		}

		@Override
		public String toString() {
			int value = this.value & ((1 << bitCount) - 1);
			value |= 1 << bitCount;
			return '<' + Integer.toBinaryString(value | (1 << bitCount))
					.substring(1) + '>';
		}

	}

	static abstract class Token {

		static final Token EMPTY = new SimpleToken(null, 0, 0);

		private final Token previous;

		Token(final Token previous) {
			this.previous = previous;
		}

		final Token getPrevious() {
			return previous;
		}

		final Token add(final int value, final int bitCount) {
			return new SimpleToken(this, value, bitCount);
		}

		final Token addBinaryShift(final int start, final int byteCount) {
			// int bitCount = (byteCount * 8) + (byteCount <= 31 ? 10 :
			// byteCount <= 62 ? 20 : 21);
			return new BinaryShiftToken(this, start, byteCount);
		}

		abstract void appendTo(BitArray bitArray, byte[] text);

	}

	static final class BinaryShiftToken extends Token {

		private final short binaryShiftStart;
		private final short binaryShiftByteCount;

		BinaryShiftToken(final Token previous, final int binaryShiftStart,
				final int binaryShiftByteCount) {
			super(previous);
			this.binaryShiftStart = (short) binaryShiftStart;
			this.binaryShiftByteCount = (short) binaryShiftByteCount;
		}

		@Override
		void appendTo(final BitArray bitArray, final byte[] text) {
			for (int i = 0; i < binaryShiftByteCount; i++) {
				if (i == 0 || (i == 31 && binaryShiftByteCount <= 62)) {
					// We need a header before the first character, and before
					// character 31 when the total byte code is <= 62
					bitArray.appendBits(31, 5); // BINARY_SHIFT
					if (binaryShiftByteCount > 62) {
						bitArray.appendBits(binaryShiftByteCount - 31, 16);
					} else if (i == 0) {
						// 1 <= binaryShiftByteCode <= 62
						bitArray.appendBits(Math.min(binaryShiftByteCount, 31),
								5);
					} else {
						// 32 <= binaryShiftCount <= 62 and i == 31
						bitArray.appendBits(binaryShiftByteCount - 31, 5);
					}
				}
				bitArray.appendBits(text[binaryShiftStart + i], 8);
			}
		}

		@Override
		public String toString() {
			return "<" + binaryShiftStart + "::"
					+ (binaryShiftStart + binaryShiftByteCount - 1) + '>';
		}

	}

}
