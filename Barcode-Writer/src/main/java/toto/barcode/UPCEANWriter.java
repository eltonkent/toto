package toto.barcode;


/**
 * <p>
 * Encapsulates functionality and implementation that is common to UPC and EAN
 * families of one-dimensional barcodes.
 * </p>
 * 
 */
abstract class UPCEANWriter extends OneDimensionalCodeWriter {

	@Override
	int getDefaultMargin() {
		// Use a different default more appropriate for UPC/EAN
		return UPCEANReader.START_END_PATTERN.length;
	}

}
