package toto.util.collections.graph;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Dijkstras Graph for that allow you to calculate the shortest path between vertices.
 * <p>
 * <code>
 *     DijkstrasGraph<Character> g=new DijkstrasGraph<Character>();
 *     g.addVertex('A', Arrays.asList(new Vertex('B', 7), new DijkstrasVertex('C', 8)));
 *     g.addVertex('B', Arrays.asList(new Vertex('A', 7), new DijkstrasVertex('F', 2)));
 *     g.addVertex('C', Arrays.asList(new DijkstrasVertex('A', 8), new DijkstrasVertex('F', 6), new DijkstrasVertex('G', 4)));
 *     g.addVertex('D', Arrays.asList(new DijkstrasVertex('F', 8)));
 *     g.addVertex('E', Arrays.asList(new DijkstrasVertex('H', 1)));
 *     g.addVertex('F', Arrays.asList(new DijkstrasVertex('B', 2), new DijkstrasVertex('C', 6), new DijkstrasVertex('D', 8), new DijkstrasVertex('G', 9), new DijkstrasVertex('H', 3)));
 *     g.addVertex('G', Arrays.asList(new DijkstrasVertex('C', 4), new DijkstrasVertex('F', 9)));
 *     g.addVertex('H', Arrays.asList(new DijkstrasVertex('E', 1), new DijkstrasVertex('F', 3)));
 *     System.out.println(g.getShortestPath('A', 'H'));
 * </code>
 * </p>
 * @see DijkstrasVertex
 * @param  <T> Graph vertex type
 */
public class DijkstrasGraph<T> {

    private final Map<T, List<DijkstrasVertex<T>>> vertices;

    public DijkstrasGraph() {
        this.vertices = new LinkedHashMap<T, List<DijkstrasVertex<T>>>();
    }

    public void addVertex(T character, List<DijkstrasVertex<T>> vertex) {
        this.vertices.put(character, vertex);
    }

    public List<T> getShortestPath(T start, T finish) {
        Map<T, Float> distances = new LinkedHashMap<T, Float>();
        PriorityQueue<DijkstrasVertex<T>> nodes = new PriorityQueue<DijkstrasVertex<T>>();
        Map<T, DijkstrasVertex<T>> previous = new LinkedHashMap<T, DijkstrasVertex<T>>();
        List<T> path = new LinkedList<T>();

        for(T vertex : vertices.keySet()) {
            if (vertex.equals(start)) {
                distances.put(vertex, 0f);
                nodes.add(new DijkstrasVertex<T>(vertex, 0f));
            } else {
                distances.put(vertex, Float.MAX_VALUE);
                nodes.add(new DijkstrasVertex(vertex, Float.MAX_VALUE));
            }
            previous.put(vertex, null);
        }

        while (!nodes.isEmpty()) {
            DijkstrasVertex smallest = nodes.poll();
            if (smallest.getId().equals(finish)) {
                path = new LinkedList<T>();
                while (previous.get(smallest.getId()) != null) {
                    path.add((T) smallest.getId());
                    smallest = previous.get(smallest.getId());
                }
                return path;
            }

            if (distances.get(smallest.getId()) == Float.MAX_VALUE) {
                break;
            }

            for (DijkstrasVertex neighbor : vertices.get(smallest.getId())) {
                Float alt = distances.get(smallest.getId()) + neighbor.getDistance();
                if (alt < distances.get(neighbor.getId())) {
                    distances.put((T) neighbor.getId(), alt);
                    previous.put((T) neighbor.getId(), smallest);

                    forloop:
                    for(DijkstrasVertex n : nodes) {
                        if (n.getId() == neighbor.getId()) {
                            n.setDistance(alt);
                            break forloop;
                        }
                    }
                }
            }
        }

        return new ArrayList<T>(distances.keySet());
    }

}
