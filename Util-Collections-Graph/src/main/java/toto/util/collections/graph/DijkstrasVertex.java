package toto.util.collections.graph;

/**
 * Vertex definition of a Dijkstras Graph.
 * @see DijkstrasGraph
 */
public class DijkstrasVertex<T> implements Comparable<DijkstrasVertex>  {

    private T id;
    private Float distance;

    public DijkstrasVertex(T id, float distance) {
        super();
        this.id = id;
        this.distance = distance;
    }

    public T getId() {
        return id;
    }

    public Float getDistance() {
        return distance;
    }

    public void setId(T id) {
        this.id = id;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((distance == null) ? 0 : distance.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DijkstrasVertex other = (DijkstrasVertex) obj;
        if (distance == null) {
            if (other.distance != null)
                return false;
        } else if (!distance.equals(other.distance))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Vertex [id=" + id + ", distance=" + distance + "]";
    }

    @Override
    public int compareTo(DijkstrasVertex o) {
        return this.distance < o.distance ? -1 : this.distance == o.distance ? 0 : 1;
    }
}
