package toto.util.collections.graph;

import java.util.Arrays;
import java.util.List;

import test.toto.ToToTestCase;

/**
 */
public class TestDijkstrasGraph extends ToToTestCase {

    public void testDijkstrasGraph(){
        DijkstrasGraph<Character> g=new DijkstrasGraph<>();
        g.addVertex('A', Arrays.asList(new DijkstrasVertex<Character>('B', 7), new DijkstrasVertex<Character>('C', 8)));
             g.addVertex('B', Arrays.asList(new DijkstrasVertex<Character>('A', 7), new DijkstrasVertex<Character>('F', 2)));
             g.addVertex('C', Arrays.asList(new DijkstrasVertex<Character>('A', 8), new DijkstrasVertex<Character>('F', 6), new DijkstrasVertex<Character>('G', 4)));
             g.addVertex('D', Arrays.asList(new DijkstrasVertex<Character>('F', 8)));
             g.addVertex('E', Arrays.asList(new DijkstrasVertex<Character>('H', 1)));
             g.addVertex('F', Arrays.asList(new DijkstrasVertex<Character>('B', 2), new DijkstrasVertex<Character>('C', 6), new DijkstrasVertex<Character>('D', 8), new DijkstrasVertex<Character>('G', 9), new DijkstrasVertex<Character>('H', 3)));
             g.addVertex('G', Arrays.asList(new DijkstrasVertex<Character>('C', 4), new DijkstrasVertex<Character>('F', 9)));
             g.addVertex('H', Arrays.asList(new DijkstrasVertex<Character>('E', 1), new DijkstrasVertex<Character>('F', 3)));
        List<Character> shortestPath=g.getShortestPath('A','H');
        assertNotNull(shortestPath);
        assertEquals(shortestPath.size()>0,true);

    }
}
