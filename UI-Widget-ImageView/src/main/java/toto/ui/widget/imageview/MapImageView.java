package toto.ui.widget.imageview;

import java.util.ArrayList;
import java.util.List;

import toto.geom2d.Point;
import toto.geom2d.T2DShape;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

/**
 * ImageView that allows you to define regions based on any
 * {@link toto.geom2d.T2DShape}
 * <p/>
 * <p>
 * using the #addRegion methods The image should be non scalable
 * <b>drawable-nodpi</b>
 * </p>
 */
public class MapImageView extends ImageView {

	/**
	 * Callback setKey inform when a map region is clicked.
	 */
	public interface MapRegionCallback {
		/**
		 * The method to inform that a region is clicked.
		 * 
		 * @param x
		 *            of the click
		 * @param y
		 *            of the click
		 * @param shape
		 *            region clicked
		 */
		public void onRegionClicked(float x, float y, T2DShape shape);
	}

	private final List<T2DShape> regions = new ArrayList<T2DShape>();
	private Bitmap bitmap;
	private MapRegionCallback callback;

	public MapImageView(final Context context) {
		super(context);
		init();
	}

	public MapImageView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public MapImageView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		this.setOnTouchListener(touchListener);
	}

	public void addRegion(final T2DShape shape) {
		regions.add(shape);
	}

	public void removeRegion(final T2DShape shape) {
		regions.remove(shape);
	}

	public void clearRegions() {
		regions.clear();
	}

	private Point getProjectedPoint(final MotionEvent event) {
		final float x = event.getX();
		final float y = event.getY();

		if (x < 0 || y < 0 || x > getWidth() || y > getHeight()) {
			// outside ImageView
			return null;
		} else {
			final int projectedX = (int) (x * (bitmap.getWidth() / (double) getWidth()));
			final int projectedY = (int) (y * (bitmap.getHeight() / (double) getHeight()));
			return new Point(projectedX, projectedY);
		}
	}

	private final OnTouchListener touchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(final View v, final MotionEvent event) {
			final Point point = getProjectedPoint(event);
			if (point != null) {
				T2DShape shape;
				for (int i = 0; i < regions.size(); i++) {
					shape = regions.get(i);
					if (shape.contains(point)) {
						if (callback != null) {
							callback.onRegionClicked(event.getX(),
									event.getY(), shape);
							return true;
						}
					}
				}
			}
			return false;
		}
	};

	public void setImageBitmap(final Bitmap bm) {
		this.bitmap = bm;
	}

	public void setImageResource(final int resId) {
		setImageBitmap(BitmapFactory.decodeResource(getResources(), resId));
	}

	public void setImageDrawable(final Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			final Bitmap bm = ((BitmapDrawable) drawable).getBitmap();
			setImageBitmap(bm);
		} else {
			throw new IllegalArgumentException(
					"Only bitmap drawables are supported for MapImageView");
		}
	}

	public void setRegionCallback(final MapRegionCallback callback) {
		this.callback = callback;
	}

}
