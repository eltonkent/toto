/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.imageview;

import java.net.URI;

import toto.bitmap.utils.CroppingUtils;
import toto.jobs.DownloadBitmapJob;
import toto.jobs.TJobResponse;
import toto.jobs.TJob.JobNotifier;
import toto.pim.ContactUtils;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.ViewSwitcher;

import com.mi.toto.ui.imageview.R;

/**
 * An image view that fetches its image off the web using the supplied URL.
 * While the image is being downloaded, a progress indicator will be shown.
 * <p>
 * <font color="#FF001D"><b>Permissions</b></font><br/>
 * <b>android.permission.INTERNET</b><br/>
 * <br/>
 * <b>Styled Attributes</b><br/>
 * <table cellspacing="1" cellpadding="3">
 * <tr>
 * <th><b>Attribute</b></td>
 * <td><b>Type</b></td>
 * <td><b>Default</b></td>
 * <td><b>Description</b></td>
 * </tr>
 * <tr>
 * <td><code>imageUrl</code></td>
 * <td>String</td>
 * <td>null</td>
 * <td>URL for the image to be downloaded</td>
 * </tr>
 * <tr>
 * <td><code>autoLoad</code></td>
 * <td>boolean</td>
 * <td>true</td>
 * <td>Flag to indicate if the image should be downloaded automatically or
 * should be user initiated
 * {@link #loadImage(android.graphics.BitmapFactory.Options)}</td>
 * </tr>
 * <p/>
 * <tr>
 * <td><code>progressDrawable</code></td>
 * <td>drawable reference</td>
 * <td>0</td>
 * <td>Drawable to be used while the image is loaded. if the ProgressBar is not
 * desired. can be an instance of any drawable including AnimationDrawable</td>
 * </tr>
 * <p/>
 * <tr>
 * <td><code>errorDrawable</code></td>
 * <td>drawable reference</td>
 * <td>0</td>
 * <td>Drawable to be used if the image could not be loaded.</td>
 * </tr>
 * <p/>
 * <tr>
 * <td><code>progressWidth</code></td>
 * <td>dimension</td>
 * <td>wrap_content</td>
 * <td>Width of the progress drawable. Not setting this value makes the Progress
 * drawble take the <code>progressDrawable</code> width</td>
 * </tr>
 * <p/>
 * <tr>
 * <td><code>progressHeight</code></td>
 * <td>dimension</td>
 * <td>wrap_content</td>
 * <td>
 * <p>
 * Height of the progress drawable. Not setting this value makes the Progress
 * drawble take the <code>progressDrawable</code> height
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td><code>zoomable</code></td>
 * <td>boolean</td>
 * <td>false</td>
 * <td>
 * <p>
 * Allow zooming into the {@link TImageView} using pinch and double tap
 * guestures
 * </p>
 * </td>
 * </tr>
 * <p/>
 * <tr>
 * <td><code>allowReuse</code></td>
 * <td>boolean</td>
 * <td>false</td>
 * <td>
 * <p>
 * Allow the {@link TImageView} to be reused by assigning different URL's
 * multiple times. Its recommended to set this to false if the ImageView is not
 * going to be reused with multiple URL's. see
 * {@link #setImageUrl(String, android.graphics.BitmapFactory.Options)}
 * </p>
 * </td>
 * </tr>
 * </table>
 * <br/>
 * A transition animation from progress to the image view can be setKey using
 * the <code>android:outAnimation</code> attribute.<br/>
 * <p/>
 * <b>Demo</b><br/>
 * This demo uses the following images <br/>
 * http://rack.1.mshcdn.com/media/
 * ZgkyMDEyLzEyLzA2LzMxL2JyZWFrZGFuY2luLjR5eS5qcGc/0d9c16e9/892/breakdancing.jpg
 * <br/>
 * http://www.cuswirl.com/Websites/cuswirl/Images/logos/android-logo-white.png
 * <br/>
 * <p/>
 * <center><OBJECT CLASSID="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
 * WIDTH="725" HEIGHT="461" CODEBASE=
 * "http://active.macromedia.com/flash5/cabs/swflash.cab#version=7,0,0,0">
 * <PARAM NAME=movie VALUE="../../../../resources/demos/webimageview.swf">
 * <PARAM NAME=play VALUE=true> <PARAM NAME=loop VALUE=false> <PARAM NAME=wmode
 * VALUE=transparent> <PARAM NAME=quality VALUE=low> <EMBED
 * SRC="../../../../resources/demos/webimageview.swf" WIDTH=725 HEIGHT=461
 * quality=low loop=false wmode=transparent TYPE="application/x-shockwave-flash"
 * PLUGINSPAGE=
 * "http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"
 * > </EMBED> </OBJECT></center> <SCRIPT
 * src='../../../../resources/demos/pagecurl.js'></script> <br/>
 * <b>Using the WebImageView</b><br/>
 * <code>
 * &lt;com.mobifluence.rage.ui.widget.imageview.WebImageView<br/>
 * &emsp;android:id="@+id/image"<br/>
 * &emsp;android:layout_width="wrap_content"<br/>
 * &emsp;android:layout_height="wrap_content"<br/>
 * &emsp;app:imageUrl="http://test.com/image.png" <br/>
 * /&gt;
 * <p/>
 * </code> <br/>
 * <b>Setting a custom progress drawable</b><br/>
 * You can setKey a custom progress drawable by declaring a custom animate
 * drawable in XML. This example uses the following custom loader.<br/>
 * <i>loader_bg.png</i><br/>
 * <img src="../../../../resources/demos/loader.png"/><br/>
 * <i>loader.xml</i><br/>
 * <code>
 * &lt;?xml version="1.0" encoding="utf-8"?><br/>
 * &lt;animated-rotate<br/>
 * &emsp;xmlns:android="http://schemas.android.com/apk/res/android"<br/>
 * &emsp;<font color="green">//setKey the loader image</font><br/>
 * &emsp;android:drawable="@drawable/loader_bg"<br/>
 * &emsp;android:pivotX="50%"<br/>
 * &emsp;android:pivotY="50%"<br/>
 * /&gt;<br/>
 * </code> You can setKey the progress drawable using the
 * <code>progressDrawable</code> attribute<br/>
 * <code>progressDrawable="@drawable/loader"</code> <br/>
 * The following clip uses the <code>progressDrawable</code> feature.<br/>
 * <center><OBJECT CLASSID="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
 * WIDTH="725" HEIGHT="461" CODEBASE=
 * "http://active.macromedia.com/flash5/cabs/swflash.cab#version=7,0,0,0">
 * <PARAM NAME=movie
 * VALUE="../../../../resources/demos/webimageview_custom.swf"> <PARAM NAME=play
 * VALUE=true> <PARAM NAME=loop VALUE=false> <PARAM NAME=wmode
 * VALUE=transparent> <PARAM NAME=quality VALUE=low> <EMBED
 * SRC="../../../../resources/demos/webimageview_custom.swf" WIDTH=725
 * HEIGHT=461 quality=low loop=false wmode=transparent
 * TYPE="application/x-shockwave-flash" PLUGINSPAGE=
 * "http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"
 * > </EMBED> </OBJECT></center> <SCRIPT
 * src='../../../../resources/demos/pagecurl.js'></script>
 * <p/>
 * </p>
 * 
 * @see #getImageView()
 * @see #setImageUrl(String, android.graphics.BitmapFactory.Options)
 * @see #setImageResource(int)
 * @see #setImageBitmap(Bitmap)
 */
public class TImageView extends ViewSwitcher {

	/**
	 * Callback that allows you to fetch a bitmap locally (if present).
	 * <p>
	 * The {@linkplain BitmapFetchingCallback} is used to fetch the bitmap esp
	 * from a cache or other local storage system.
	 * </p>
	 * 
	 * @see toto.cache.TCache
	 * @author Mobifluence Interactive
	 */
	public interface BitmapFetchingCallback {

		/**
		 * Callback method to fetch the bitmap esp from a cache or other local
		 * storage system.
		 * 
		 * @param url
		 * @return the fetched bitmap or null if the bitmap is unavailable.
		 */
		public Bitmap fetchBitmap(String url);
	}

	/**
	 * Callback that allows you to perform some operations on the Bitmap right
	 * after its downloaded and before its assigned to the {@link TImageView}
	 * 
	 * @author Mobifluence Interactive
	 * @see CroppingUtils#cropRoundRect(Context, Bitmap, float, float, float,
	 *      float, int, int)
	 * @see CroppingUtils#cropCircular(Bitmap)
	 * @see CroppingUtils#crop(Bitmap, int, int, int, int,
	 *      android.graphics.Bitmap.Config)
	 */
	public interface BitmapProcessingCallback {

		/**
		 * Allows you to process the <code>bitmap</code> with operations like
		 * cropping and filters.
		 * <p>
		 * This method is called on a separate thread.
		 * </p>
		 * 
		 * @param bitmap
		 *            the download bitmap
		 * @return the processed bitmap
		 * @see TImageView#setImageBitmap(Bitmap)
		 */
		public Bitmap processBitmap(Bitmap bitmap);
	}

	private BitmapProcessingCallback bitmapCallback;
	private BitmapFetchingCallback bitmapFetchCallback;

	private boolean autoload;

	private String imageUrl;

	private ZoomableImageView imageView;

	private boolean isLoaded;

	private ProgressBar loadingSpinner;

	private Drawable progressDrawable, errorDrawable;

	private final ScaleType scaleType = ScaleType.CENTER_CROP;

	// private boolean allowReuse;

	private final JobNotifier<Bitmap> bitmapNotifier = new JobNotifier<Bitmap>() {

		@Override
		public void onError(final Throwable t) {

		}

		@Override
		public void onJobCompletedNonUI(final TJobResponse<Bitmap> response) {
			final Object data = response.getData();
			if (data != null) {
				Bitmap bmp = (Bitmap) data;
				if (bitmapCallback != null) {
					bmp = bitmapCallback.processBitmap(bmp);
				}
				response.setData(bmp);
			}
		}

		@Override
		public void onJobCompletedUI(final TJobResponse<Bitmap> response) {
			if (response.getResponseStatus() == TJobResponse.STATUS_SUCCESS) {
				final Bitmap data = response.getData();
				if (data != null) {
					setImageBitmap(data);
					if (imageViewCallback != null) {
						imageViewCallback.onImageViewLoaded();
					}
					isLoaded = true;
				}
			} else {
				// error occured
				if (errorDrawable != null)
					imageView.setImageDrawable(errorDrawable);
				if (imageViewCallback != null) {
					imageViewCallback.onLoadingFailed();
				}
				isLoaded = false;
			}
		}

		@Override
		public void onJobStarted() {

		}
	};

	private ImageViewLoadingCallback imageViewCallback;

	public TImageView(final Context context) {
		super(context);
		initialize(context, null, null, null, true, -2, -2, false);
	}

	public TImageView(final Context context, final AttributeSet attributes) {

		super(context, attributes);
		final TypedArray styledAttrs = context.obtainStyledAttributes(
				attributes, R.styleable.TImageView);

		final Drawable progressDrawable = styledAttrs
				.getDrawable(R.styleable.TImageView_progress_drawable);

		final Drawable errorDrawable = styledAttrs
				.getDrawable(R.styleable.TImageView_error_drawable);

		final int progressW = styledAttrs.getLayoutDimension(
				R.styleable.TImageView_progress_width, -2);
		final int progressH = styledAttrs.getLayoutDimension(
				R.styleable.TImageView_progress_height, -2);

		final boolean isZoomable = styledAttrs.getBoolean(
				R.styleable.TImageView_zoomable, true);
		initialize(context,
				styledAttrs.getString(R.styleable.TImageView_image_url),
				progressDrawable, errorDrawable,
				styledAttrs.getBoolean(R.styleable.TImageView_autoLoad, true),
				progressW, progressH, isZoomable);
		styledAttrs.recycle();
	}

	private void addImageView(final Context context, final boolean zoomable) {
		imageView = new ZoomableImageView(context);
		if (zoomable) {
			imageView.allowGestures(zoomable);
		}
		imageView.setScaleType(scaleType);
		final LayoutParams lp = new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.FILL_PARENT);
		lp.gravity = Gravity.CENTER;
		/* Make sure to use the parent's addview */
		super.addView(imageView, 1, lp);
	}

	private void addLoadingSpinnerView(final Context context, final int progW,
			final int progH) {
		loadingSpinner = new ProgressBar(context);
		loadingSpinner.setIndeterminate(true);
		if (this.progressDrawable == null) {
			this.progressDrawable = loadingSpinner.getIndeterminateDrawable();
		} else {
			loadingSpinner.setIndeterminateDrawable(progressDrawable);
			if (progressDrawable instanceof AnimationDrawable) {
				((AnimationDrawable) progressDrawable).start();
			}
		}
		final LayoutParams lp = new LayoutParams(progW, progH);
		lp.gravity = Gravity.CENTER;
		// make sure to use the parent's addview
		super.addView(loadingSpinner, 0, lp);
	}

	public void addView(final View child, final int index,
			final ViewGroup.LayoutParams params) {
		throw new IllegalStateException("Cannot add view to RImageView");
	}

	/**
	 * Allow zooming gestures . Default is true.
	 * 
	 * @param allowGesture
	 */
	public void allowZoomGestures(final boolean allowGesture) {
		imageView.allowGestures(allowGesture);
	}

	public final BitmapFetchingCallback getBitmapFetchCallback() {
		return bitmapFetchCallback;
	}

	public BitmapProcessingCallback getBitmapProcessingCallback() {
		return bitmapCallback;
	}

	/**
	 * Returns the URL of the image to show
	 * 
	 * @return
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * Get the ImageView associated with this WebImageView
	 * 
	 * @return
	 */
	public ImageView getImageView() {
		return imageView;
	}

	public ImageViewLoadingCallback getImageViewCallback() {
		return imageViewCallback;
	}

	public View getNextView() {
		throw new IllegalStateException("Cannot getNextView on RImageView");
	}

	private void initialize(final Context context, final String imageUrl,
			final Drawable progressDrawable, final Drawable errorDrawable,
			final boolean autoLoad, final int progressW, final int progressH,
			final boolean zoomable) {
		this.imageUrl = imageUrl;
		this.progressDrawable = progressDrawable;
		this.errorDrawable = errorDrawable;
		this.autoload = autoLoad;
		addLoadingSpinnerView(context, progressW, progressH);
		addImageView(context, zoomable);
		if (autoLoad && imageUrl != null) {
			loadImage(null);
		}
	}

	/**
	 * Check if the bitmap is loaded after the url is set.
	 * 
	 * @see #setImageUrl(String, android.graphics.BitmapFactory.Options)
	 * @see #loadImage(android.graphics.BitmapFactory.Options)
	 * @return
	 */
	public boolean isLoaded() {
		return isLoaded;
	}

	/**
	 * Use this method to trigger the image download if you had previously set
	 * autoLoad to false.
	 * <p>
	 * Every time this method is called, the image resource is fetched and
	 * assigned to the {@link TImageView}
	 * </p>
	 * 
	 * @param bitmapOptions
	 *            Bitmap options to fetch the image with
	 * @see TImageView#setImageUrl(String,
	 *      android.graphics.BitmapFactory.Options)
	 */
	public void loadImage(final BitmapFactory.Options bitmapOptions) {
		if (imageViewCallback != null) {
			imageViewCallback.onResourceAssigned();
		}
		if (imageUrl == null) {
			throw new IllegalStateException(
					"Image URL is not set. please set URL using setImageUrl");
		}
		resetWebImageView();
		if (bitmapFetchCallback != null) {
			final Bitmap bm = bitmapFetchCallback.fetchBitmap(imageUrl);
			if (bm != null) {
				processAndSetBitmap(bm);
			} else {
				startBitmapDownload(bitmapOptions);
			}
		} else {
			startBitmapDownload(bitmapOptions);
		}
	}

	private void processAndSetBitmap(Bitmap bmp) {
		if (bitmapCallback != null) {
			bmp = bitmapCallback.processBitmap(bmp);
		}
		setImageBitmap(bmp);
	}

	@Override
	public void reset() {
		super.reset();
		this.resetWebImageView();
	}

	private void resetWebImageView() {
		isLoaded = false;
		this.setDisplayedChild(0);
	}

	/**
	 * Set callback for fetching bitmaps.
	 * 
	 * @param bitmapFetchCallback
	 */
	public final void setBitmapFetchCallback(
			final BitmapFetchingCallback bitmapFetchCallback) {
		this.bitmapFetchCallback = bitmapFetchCallback;
	}

	/**
	 * Set callback for processing bitmaps.
	 * 
	 * @param bitmapCallback
	 */
	public void setBitmapProcessingCallback(
			final BitmapProcessingCallback bitmapCallback) {
		this.bitmapCallback = bitmapCallback;
	}

	/**
	 * Set the a contact's picture as the {@link TImageView} source
	 * 
	 * @param contactID
	 *            of a contact
	 */
	public void setContactImage(final int contactID) {
		final Bitmap bitmap = ContactUtils.getBitmapForContact(getContext(),
				contactID);
		if (bitmap != null) {
			processAndSetBitmap(bitmap);
		}
	}

	/**
	 * Allows you to set the bitmap directly.
	 * <p>
	 * Can also be called from a non-UI thread.
	 * </p>
	 * 
	 * @param bitmap
	 */
	public void setImageBitmap(final Bitmap bitmap) {
		final Message msg = new Message();
		msg.obj = bitmap;
		handler.sendMessage(msg);
		if (imageViewCallback != null) {
			imageViewCallback.onImageViewLoaded();
		}
		isLoaded = true;
	}

	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(final Message msg) {
			imageView.setImageBitmap((Bitmap) msg.obj);
			showNext();
			super.handleMessage(msg);
		}
	};

	/**
	 * Set the imageUrl
	 * 
	 * @param imageUrl
	 *            URL for the bitmap to download
	 * @param bitmapOptions
	 *            Options for the bitmap being loaded, If null, the default
	 *            options is used.
	 */
	public void setImageUrl(final String imageUrl,
			final BitmapFactory.Options bitmapOptions) {
		this.imageUrl = imageUrl;
		if (autoload)
			loadImage(bitmapOptions);
	}

	public void setImageViewCallback(
			final ImageViewLoadingCallback imageViewCallback) {
		this.imageViewCallback = imageViewCallback;
	}

	/**
	 * Set the image from a resource id.
	 * 
	 * @param resId
	 *            the resource of the placeholder image drawable
	 */
	public void setImageResource(final int resId) {
		imageView.setImageDrawable(getContext().getResources().getDrawable(
				resId));
		showNext();
	}

	/**
	 * Set the max zoom scale for if this image view is set to be zoomable.
	 * 
	 * @see #allowZoomGestures(boolean)
	 * @param maxScale
	 */
	public void setZoomMaxScale(final float maxScale) {
		imageView.setZoomMaxScale(maxScale);
	}

	/**
	 * Set the minimum scale for zooming if set.
	 * 
	 * @see #allowZoomGestures(boolean)
	 * @param minScale
	 */
	public void setZoomMinScale(final float minScale) {
		imageView.setZoomMinScale(minScale);
	}

	private void startBitmapDownload(final BitmapFactory.Options bitmapOptions) {
		final DownloadBitmapJob task = new DownloadBitmapJob(getContext(),
				bitmapNotifier);
		final Object[] param = new Object[2];
		param[0] = URI.create(imageUrl);
		param[1] = bitmapOptions == null ? new BitmapFactory.Options()
				: bitmapOptions;
		task.execute(param);
	}

	/**
	 * Set the scale at which double tap can be set.
	 * 
	 * @param doubleTapScale
	 */
	public void setDoubleTapScale(final float doubleTapScale) {
		imageView.setDoubleTapScale(doubleTapScale);
	}
}
