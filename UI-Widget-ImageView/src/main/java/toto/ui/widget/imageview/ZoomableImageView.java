/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.imageview;

import java.util.Arrays;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;

/**
 * ImageView that allows rotating and zooming with pinching and double taps.
 * <p>
 * <b>Known Issues:</b><br/>
 * Zooming does not work when the <code>scaleType</code> is setKey to
 * <code>fitXY</code> <br/>
 * <b>Demo</b><br/>
 * <center><OBJECT CLASSID="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
 * WIDTH="725" HEIGHT="461" CODEBASE=
 * "http://active.macromedia.com/flash5/cabs/swflash.cab#version=7,0,0,0">
 * <PARAM NAME=movie
 * VALUE="../../../../resources/demos/zoomableImageView.swf"> <PARAM
 * NAME=play VALUE=true> <PARAM NAME=loop VALUE=false> <PARAM NAME=wmode
 * VALUE=transparent> <PARAM NAME=quality VALUE=low> <EMBED
 * SRC="../../../../resources/demos/zoomableImageView.swf" WIDTH=725
 * HEIGHT=461 quality=low loop=false wmode=transparent
 * TYPE="application/x-shockwave-flash" PLUGINSPAGE=
 * "http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"
 * > </EMBED> </OBJECT></center> <SCRIPT
 * src='../../../../resources/demos/pagecurl.js'></script> <br/>
 * </p>
 *
 * @author ekent4
 */

/**
 * An extension of android's native {@link ImageView} supporting pinch-zoom,
 * double-tap-zoom and exploring the image via drag-gestures.
 */
class ZoomableImageView extends ImageView {
	public enum Axis {
		X, Y;
	}

	/**
	 * The maximum allowed scale factor.
	 */
	private float maxScale = 3.5f;

	/**
	 * The minimum allowed scale factor.
	 */
	private float minScale = 1f;

	/**
	 * Maximum scale factor for double taps.
	 */
	private float doubleTapScale = 2f;

	/**
	 * The {@link TouchListener} that manages drag, pinch and scale gestures.
	 */
	private final TouchListener touchListener;

	/**
	 * The image's initial (after {@link #onMeasure(int, int)} was performed)
	 * state array representation.
	 */
	private final float[] initialStateValues = new float[9];

	/**
	 * The array representation of the currently active image {@link Matrix}.
	 */
	private final float[] matrixValues = new float[9];

	/**
	 * An additional {@link Matrix} used for scaling.
	 */
	private final Matrix scaleMatrix = new Matrix();

	/**
	 * The {@link #scaleMatrix} in array representation.
	 */
	private final float[] scaleMatrixValues = new float[9];

	/**
	 * The position of the last pointer interaction.
	 */
	private final PointF lastPosition = new PointF(0, 0);

	/**
	 * The recently applied scaling.
	 */
	private float lastScale = 1;

	public ZoomableImageView(final Context context) {
		this(context, null, 0);
	}

	public ZoomableImageView(final Context context, final AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ZoomableImageView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);

		this.touchListener = new TouchListener(context);
		this.setOnTouchListener(this.touchListener);
	}

	float getZoomMaxScale() {
		return maxScale;
	}

	void setZoomMaxScale(final float maxScale) {
		this.maxScale = maxScale;
	}

	float getZoomMinScale() {
		return minScale;
	}

	void setZoomMinScale(final float minScale) {
		this.minScale = minScale;
	}

	float getDoubleTapScale() {
		return doubleTapScale;
	}

	void setDoubleTapScale(final float doubleTapScale) {
		this.doubleTapScale = doubleTapScale;
	}

	/**
	 * Get the image's current scale factor.
	 * 
	 * @return
	 */
	float getCurrentScale() {
		return matrixValues[Matrix.MSCALE_X];
	}

	PointF getLastPosition() {
		return lastPosition;
	}

	void setLastPosition(final MotionEvent event) {
		this.setLastPosition(event.getX(), event.getY());
	}

	void setLastPosition(final float x, final float y) {
		this.lastPosition.x = x;
		this.lastPosition.y = y;
	}

	float getLastScale() {
		return lastScale;
	}

	void setLastScale(final float lastScale) {
		this.lastScale = lastScale;
	}

	@Override
	protected void onMeasure(final int widthMeasureSpec,
			final int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		getImageMatrix().getValues(matrixValues);

		if (!Arrays.equals(matrixValues, initialStateValues)
				&& getDrawable() != null) {
			// Set dimensions to fill the viewport.
			setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec),
					MeasureSpec.getSize(heightMeasureSpec));

			// Use translate method to exploit its functionalities to center the
			// image.
			translate(0, 0);

			// Store this position as initial position.
			getImageMatrix().getValues(initialStateValues);
		}
	}

	/**
	 * Reset the drawable's position and size to its original condition.
	 * 
	 * @return <code>true</code> if the current image matrix' scale has been
	 *         adjusted to the initial matrix' scale. <code>false</code> if the
	 *         current image matrix has already been reset.
	 */
	boolean reset() {
		getImageMatrix().getValues(matrixValues);

		if (matrixValues[Matrix.MSCALE_X] == initialStateValues[Matrix.MSCALE_X]
				|| getImageMatrix().isIdentity()) {
			return false;
		} else {
			getImageMatrix().setValues(initialStateValues);
			invalidate();
			return true;
		}
	}

	/**
	 * Get the top or left offset to keep the image centered.
	 * 
	 * @param axis
	 * @param viewSize
	 * @param drawableSize
	 * @param scale
	 * @return
	 */
	protected float getOffset(final Axis axis, final int viewSize,
			final int drawableSize, final float scale) {
		if (axis == Axis.X) {
			return (viewSize - drawableSize * scale) / 2;
		} else {
			return drawableSize * scale < viewSize ? (viewSize - drawableSize
					* scale) / 2 : 0;
		}
	}

	/**
	 * Moves the image from its current position. This method does not accept
	 * absolute coordinates. The movement has to be specified via relative delta
	 * values.
	 * 
	 * @param deltaX
	 *            The amount of pixels to move the image on the X-axis.
	 * @param deltaY
	 *            The amount of pixels to move the image on the Y-axis.
	 */
	public void translate(final float deltaX, final float deltaY) {
		matrixValues[Matrix.MTRANS_X] = getTranslation(Axis.X, getDrawable()
				.getIntrinsicWidth(), getCurrentScale(), getMeasuredWidth(),
				matrixValues[Matrix.MTRANS_X], deltaX);

		matrixValues[Matrix.MTRANS_Y] = getTranslation(Axis.Y, getDrawable()
				.getIntrinsicHeight(), getCurrentScale(), getMeasuredHeight(),
				matrixValues[Matrix.MTRANS_Y], deltaY);

		setLastPosition(getLastPosition().x + deltaX, getLastPosition().y
				+ deltaY);
		getImageMatrix().setValues(matrixValues);
	}

	protected float getTranslation(final Axis axis, final int drawableSize,
			final float scale, final int viewSize, final float translation,
			final float delta) {
		final float currentDrawableSize = drawableSize * scale;

		if (currentDrawableSize <= viewSize) {
			return getOffset(axis, viewSize, drawableSize, scale);
		} else {
			return delta > 0 ? Math.min(0, translation + delta) : Math.max(-1
					* (currentDrawableSize - viewSize), translation + delta);
		}
	}

	void scale(float scale, final float x, final float y) {
		if (scale > 1) {
			scale = Math.min(scale, maxScale / getCurrentScale());
		} else if (scale < 1) {
			scale = Math.max(scale, initialStateValues[Matrix.MSCALE_X]
					/ getCurrentScale());
		}

		// Perform scaling on a separate matrix to prevent its translations from
		// leaving the view's bounds.
		scaleMatrix.set(getImageMatrix());
		scaleMatrix.postScale(scale, scale, x, y);
		scaleMatrix.getValues(scaleMatrixValues);

		setLastScale(scale);

		matrixValues[Matrix.MSCALE_X] = scaleMatrixValues[Matrix.MSCALE_X];
		matrixValues[Matrix.MSCALE_Y] = scaleMatrixValues[Matrix.MSCALE_Y];

		translate(scaleMatrixValues[Matrix.MTRANS_X]
				- matrixValues[Matrix.MTRANS_X],
				scaleMatrixValues[Matrix.MTRANS_Y]
						- matrixValues[Matrix.MTRANS_Y]);
	}

	private boolean allowGestures = true;

	void allowGestures(final boolean allowGestures) {
		reset();
		this.allowGestures = allowGestures;
	}

	protected class TouchListener implements OnTouchListener {
		private GestureDetector doubleTapDetector;

		private ScaleGestureDetector scaleDetector;

		public TouchListener(final Context context) {
			this.doubleTapDetector = new GestureDetector(context,
					new GestureDetector.SimpleOnGestureListener() {
						@Override
						public boolean onSingleTapConfirmed(
								final MotionEvent event) {
							View view = ZoomableImageView.this;

							if (!performClick()) {
								while (view.getParent() instanceof View) {
									view = (View) view.getParent();

									if (view.performClick()) {
										return true;
									}
								}

								return false;
							}

							return true;
						}

						@Override
						public void onLongPress(final MotionEvent event) {
							View view = ZoomableImageView.this;

							if (!performLongClick()) {
								while (view.getParent() instanceof View) {
									view = (View) view.getParent();

									if (view.performLongClick()) {
										return;
									}
								}
							}
						}

						@Override
						public boolean onDoubleTap(final MotionEvent event) {
							if (getCurrentScale() > initialStateValues[Matrix.MSCALE_X]) {
								// Zoom out.
								scale(initialStateValues[Matrix.MSCALE_X]
										/ getCurrentScale(), event.getX(),
										event.getY());
							} else {
								// Zoom in.
								scale(doubleTapScale / getCurrentScale(),
										event.getX(), event.getY());
							}

							return true;
						}
					});

			this.scaleDetector = new ScaleGestureDetector(context,
					new ScaleGestureDetector.SimpleOnScaleGestureListener() {
						@Override
						public boolean onScale(
								final ScaleGestureDetector detector) {
							scale(detector.getScaleFactor(),
									detector.getFocusX(), detector.getFocusY());
							return true;
						}
					});
		}

		public GestureDetector getDoubleTapDetector() {
			return doubleTapDetector;
		}

		public void setDoubleTapDetector(final GestureDetector doubleTapDetector) {
			this.doubleTapDetector = doubleTapDetector;
		}

		public ScaleGestureDetector getScaleDetector() {
			return scaleDetector;
		}

		public void setScaleDetector(final ScaleGestureDetector scaleDetector) {
			this.scaleDetector = scaleDetector;
		}

		@Override
		public boolean onTouch(final View view, final MotionEvent event) {
			if (allowGestures) {
				// Pass the touch event to the detectors to process further
				// events.
				getImageMatrix().getValues(matrixValues);
				doubleTapDetector.onTouchEvent(event);
				scaleDetector.onTouchEvent(event);

				// Manage the actual drag-event.
				if (!scaleDetector.isInProgress()) {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						// Store touch position as point on first interaction.
						setLastPosition(event);
						break;

					case MotionEvent.ACTION_MOVE:
						// Translate each movement event into the image matrix
						// and update the last touched position afterwards.
						translate(event.getX() - lastPosition.x, event.getY()
								- lastPosition.y);
						break;
					}
				}

				// Enforce the view to be redrawn.
				invalidate();

				return true;
			}
			return false;
		}
	}

	public void setImageBitmap(final Bitmap bitmap) {
		final Matrix matrix = getImageMatrix();
		super.setImageBitmap(bitmap);
		setImageMatrix(matrix);
	}
}