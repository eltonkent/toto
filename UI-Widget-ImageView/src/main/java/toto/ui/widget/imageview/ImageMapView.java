package toto.ui.widget.imageview;

//package com.mobifluence.rage.ui.widget.imageview;
//
//import android.content.Context;
//import android.content.res.TypedArray;
//import android.graphics.PointF;
//import android.util.AttributeSet;
//import android.util.Log;
//import android.util.SparseArray;
//
//import com.mobifluence.rage.R;
//import com.mobifluence.rage.geom.primitives.Circle;
//import com.mobifluence.rage.geom.primitives.Polygon;
//
//
//
//import org.xmlpull.v1.XmlPullParser;
//import org.xmlpull.v1.XmlPullParserException;
//import org.xmlpull.v1.XmlPullParserFactory;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//
///**
// * Image view with ImageMap features
// * <p>
// *
// * </p>
// *
// * @see #loadMap(InputStream)
// *
// */
//public class ImageMapView extends RageImageView implements
//        ImageMapView.OnClickListener {
//
//	private final ArrayList<Area> mAreaList = new ArrayList<Area>();
//	private final SparseArray<Area> mIdToArea = new SparseArray<Area>();
//	private boolean hasMap = false;
//
//	public ImageMapView(final Context context) {
//		super(context);
//	}
//
//	public ImageMapView(final Context context, final AttributeSet attrs) {
//		super(context, attrs);
//		final TypedArray a = context.obtainStyledAttributes(attrs,
//				R.styleable.ImageMapView);
//
//		final int resource = a.getResourceId(R.styleable.ImageMapView_mapFile,
//				-1);
//		if (resource > 0)
//			loadMap(resource);
//		a.recycle();
//		setOnClickListener(this);
////        allowDoubleTapZoom(false);
//	}
//
//
//	/**
//	 * Map area clicked callback
//	 */
//	public static interface OnAreaClickedListener {
//		/**
//		 * Area with 'id' has been tapped
//		 *
//		 * @param area
//		 */
//		void onAreaClicked(Area area);
//	}
//
//	private OnAreaClickedListener mClickHandler;
//
//	public void setAreaClickListener(final OnAreaClickedListener handler) {
//		mClickHandler = handler;
//	}
//
//	public void loadMap(final int res) {
//        loadMap(getContext().getResources().openRawResource(res));
//	}
//
//	public void click(final float relativeX, final float relativeY) {
//		for (final Area a : mAreaList) {
//			if (a.isInArea(relativeX, relativeY)) {
//				if (mClickHandler != null) {
//					mClickHandler.onAreaClicked(a);
//				}
//				// only fire click for one area
//				break;
//			}
//		}
//	}
//
//	public void onClick(final RageImageView view, final float eventX,
//			final float eventY) {
//		click(eventX, eventY);
//	}
//
//	/**
//	 * Parse map file from a stream
//	 * <p>
//	 * A sample map XML is as follows
//	 *
//	 * <pre>
//	 * &lt;maps xmlns:android="http://schemas.android.com/apk/res/android"&gt;
//	 *     &lt;map name="testmap">
//	 *          &lt;!-- the id attribute is returned on the click event --&gt;
//	 *          &lt;!-- x,y,x,y,x,y --&gt;
//	 *          &lt;area shape="poly" coords="409,298,409,298,411,292,409,279,409,233,408,233,437,231,446,263,449,267,448" name="polyshape" id="@+id/poly" /&gt;
//	 *          &lt;!--left,top,right,bottom --&gt;
//	 * 			&lt;area shape="rect" coords="114,348,163,381" name="rect" id="@+id/test_Rect" /&gt;
//	 * 			&lt;!-- x,y,radius --&gt;
//	 * 			&lt;area shape="circle" coords="114,348,163" name="circle" id="@+id/test_Circle" /&gt;
//	 *   	&lt;/map&gt;
//	 * &lt;/maps&gt;
//	 * </pre>
//	 *
//	 * Areas can also be added using the {@link #addArea(Area)} method.
//	 * </p>
//	 *
//	 * @see #addArea(Area)
//	 * @param is
//	 *            stream of the map file.
//	 */
//	public void loadMap(final InputStream is) {
//		boolean loading = false;
//		mAreaList.clear();
//		mIdToArea.clear();
//
//        try {
//			final XmlPullParserFactory factory = XmlPullParserFactory
//					.newInstance();
//			final XmlPullParser xpp = factory.newPullParser();
//			xpp.setInput(is, "UTF-8");
//			String tag, coords, shape, id, name;
//			int eventType = xpp.getEventType();
//			while (eventType != XmlPullParser.END_DOCUMENT) {
//				if (eventType == XmlPullParser.START_DOCUMENT) {
//					// Start document
//					// This is a useful branch for a debug log if
//					// parsing is not working
//				} else if (eventType == XmlPullParser.START_TAG) {
//					tag = xpp.getName();
//
//					if (tag.equalsIgnoreCase("map")) {
//						loading = true;
//					}
//					if (loading) {
//						if (tag.equalsIgnoreCase("area")) {
//							Area a = null;
//							shape = xpp.getAttributeValue(null, "shape");
//							coords = xpp.getAttributeValue(null, "coords");
//							id = xpp.getAttributeValue(null, "id");
//
//							// as a name for this area, try to find any of these
//							// attributes
//							// name attribute is custom to this impl (not
//							// standard in html area tag)
//							name = xpp.getAttributeValue(null, "name");
//							if (name == null) {
//								name = xpp.getAttributeValue(null, "title");
//							}
//							if (name == null) {
//								name = xpp.getAttributeValue(null, "alt");
//							}
//
//							if ((shape != null) && (coords != null)) {
//								a = addShape(shape, name, coords, id);
//								if (a != null) {
//									// add all of the area tag attributes
//									// so that they are available to the
//									// implementation if needed (see
//									// getAreaAttribute)
//									for (int i = 0; i < xpp.getAttributeCount(); i++) {
//										final String attrName = xpp
//												.getAttributeName(i);
//										final String attrVal = xpp
//												.getAttributeValue(null,
//														attrName);
//										a.addValue(attrName, attrVal);
//									}
//								}
//							}
//						}
//					}
//				} else if (eventType == XmlPullParser.END_TAG) {
//					tag = xpp.getName();
//					if (tag.equalsIgnoreCase("map")) {
//						loading = false;
//					}
//				}
//				eventType = xpp.next();
//			}
//
//			hasMap = true;
//		} catch (final XmlPullParserException xppe) {
//			Log.e("loadMap::XmlPullParserException", "", xppe);
//		} catch (final IOException ioe) {
//			Log.e("loadMap::IOException", "", ioe);
//		}
//        Log.d("Rage","Total Shapes"+mAreaList.size());
//	}
//
//	/**
//	 * Add a area in the {@link ImageMapView}
//	 *
//	 * @see RectArea
//	 * @see CircleArea
//	 * @see PolyArea
//	 * @param a
//	 */
//	public void addArea(final Area a) {
//		mAreaList.add(a);
//		mIdToArea.put(a.getId(), a);
//	}
//
//	/**
//	 * Add any shape using names like rect, circle, poly for the
//	 * <code>shape</code> parameter
//	 *
//	 * @param shape
//	 * @param name
//	 * @param coords
//	 * @param id
//	 * @return
//	 */
//	public Area addShape(final String shape, final String name,
//			final String coords, final String id) {
//		Area a = null;
//		final String rid = id.replace("@+id/", "");
//		int resID = 0;
//
//		try {
//			resID = getContext().getResources().getIdentifier(rid, "id",
//					getContext().getPackageName());
//		} catch (final Exception e) {
//			resID = 0;
//		}
//		if (resID != 0) {
//			if (shape.equalsIgnoreCase("rect")) {
//				final String[] v = coords.split(",");
//				if (v.length == 4) {
//					a = new RectArea(resID, name, Float.parseFloat(v[0]),
//							Float.parseFloat(v[1]), Float.parseFloat(v[2]),
//							Float.parseFloat(v[3]));
//				}
//			}
//			if (shape.equalsIgnoreCase("circle")) {
//				final String[] v = coords.split(",");
//				if (v.length == 3) {
//					a = new CircleArea(resID, name, Float.parseFloat(v[0]),
//							Float.parseFloat(v[1]), Float.parseFloat(v[2]));
//				}
//			}
//			if (shape.equalsIgnoreCase("poly")) {
//				a = new PolyArea(resID, name, coords);
//			}
//			if (a != null) {
//				addArea(a);
//			}
//		}
//		return a;
//	}
//
//	public static abstract class Area {
//		private final int mResId;
//		private String mName;
//		private HashMap<String, String> mValues;
//
//		public Area(final int id, final String name) {
//			mResId = id;
//			if (name != null) {
//				mName = name;
//			}
//		}
//
//		public int getId() {
//			return mResId;
//		}
//
//		public String getName() {
//			return mName;
//		}
//
//		// all xml values for the area are passed to the object
//		// the default impl just puts them into a hashmap for
//		// retrieval later
//		public void addValue(final String key, final String value) {
//			if (mValues == null) {
//				mValues = new HashMap<String, String>();
//			}
//			mValues.put(key, value);
//		}
//
//		public String getValue(final String key) {
//			String value = null;
//			if (mValues != null) {
//				value = mValues.get(key);
//			}
//			return value;
//		}
//
//		abstract boolean isInArea(float x, float y);
//
//		abstract float getOriginX();
//
//		abstract float getOriginY();
//	}
//
//	public final static  class CircleArea extends Area {
//		private final Circle circle;
//
//		public CircleArea(final int id, final String name, final float x,
//				final float y, final float radius) {
//			super(id, name);
//			circle = new Circle(x, y, radius);
//		}
//
//		public boolean isInArea(final float x, final float y) {
//			return circle.contains(x, y);
//		}
//
//		public float getOriginX() {
//			return circle.getX();
//		}
//
//		public float getOriginY() {
//			return circle.getY();
//		}
//	}
//
//	public static final class PolyArea extends Area {
//		private final float mPointX;
//		private final float mPointY;
//		private final Polygon polygon;
//
//		public PolyArea(final int id, final String name, final String coords) {
//			super(id, name);
//			polygon = new Polygon(coords);
//			final PointF poin = polygon.getCenterOfGravity();
//			mPointX = poin.x;
//			mPointY = poin.y;
//		}
//
//		@Override
//		public float getOriginX() {
//			return mPointX;
//		}
//
//		@Override
//		public float getOriginY() {
//			return mPointY;
//		}
//
//		@Override
//		public boolean isInArea(final float testx, final float testy) {
//			return polygon.contains(testx, testy);
//		}
//	}
//
//	public static class RectArea extends Area {
//		private final float mBoundLeft;
//		private final float mBoundTop;
//		private final float mBoundRight;
//		private final float mBoundBottom;
//
//		public RectArea(final int id, final String name, final float left,
//				final float top, final float right, final float bottom) {
//			super(id, name);
//			mBoundLeft = left;
//			mBoundTop = top;
//			mBoundRight = right;
//			mBoundBottom = bottom;
//		}
//
//		public boolean isInArea(final float x, final float y) {
////			boolean rValue = false;
////			if ((x > mBoundLeft) && (x < mBoundRight) && (y > mBoundTop)
////					&& (y < mBoundBottom)) {
////				rValue = true;
////			}
////			return rValue;
//
//
//            boolean rValue = false;
//            if ((x > mBoundLeft) && (x < mBoundRight) && (y > mBoundTop) && (y < mBoundBottom)) {
//                rValue = true;
//            }
//            return rValue;
//		}
//
//		public float getOriginX() {
//			return mBoundLeft;
//		}
//
//		public float getOriginY() {
//			return mBoundTop;
//		}
//	}
//
//	public boolean hasMap() {
//		return hasMap;
//	}
//
// }