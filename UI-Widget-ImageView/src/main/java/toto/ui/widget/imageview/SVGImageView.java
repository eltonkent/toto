/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.imageview;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import toto.graphics.svg.SVG;
import toto.graphics.svg.SVGFactory;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import com.mi.toto.ui.imageview.R;

/**
 * An ImageView that can also use SVG images as its source.
 * <p>
 * <b>Note:</b> setKey the layer type to software for devices >4.1 . This solves
 * rendering issues<br/>
 * The SVG image is loaded on its individual thread hence making the main thread
 * responsive.<br/>
 * <b>Attributes</b> <br/>
 * <table cellspacing="1" cellpadding="3">
 * <tr>
 * <th><b>Attribute</b></td>
 * <td><b>Type</b></td>
 * <td><b>Default</b></td>
 * <td><b>Description</b></td>
 * </tr>
 * <tr>
 * <td><code>svgResource</code></td>
 * <td>integer</td>
 * <td>-1</td>
 * <td>The SVG resource to be used. defined usually with
 * <code>@raw/svg_file</code></td>
 * </tr>
 * <td><code>svgZoomFactor</code></td>
 * <td>integer</td>
 * <td>100</td>
 * <td>The factor by which the SVG should be zoomed. <code>100</code> indicates
 * that the SVG will be rendered in its actual size, <code>200</code> means
 * twice its size,<code>50</code> means half its size and so on.</td> </tr>
 * </table>
 * <br/>
 * <b>Demo</b><br/>
 * This demo uses the <a
 * href="../../../../resources/giraffe.svg">Giraffe.svg</a> created by <a href=
 * "http://openclipart.org/detail/160609/new-cartoon-giraffe-by-ryanlerch"> ryan
 * lerch</a>. <center><OBJECT
 * CLASSID="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" WIDTH="725" HEIGHT="461"
 * CODEBASE=
 * "http://active.macromedia.com/flash5/cabs/swflash.cab#version=7,0,0,0">
 * <PARAM NAME=movie VALUE="../../../../resources/demos/svgimageview.swf">
 * <PARAM NAME=play VALUE=true> <PARAM NAME=loop VALUE=false> <PARAM NAME=wmode
 * VALUE=transparent> <PARAM NAME=quality VALUE=low> <EMBED
 * SRC="../../../../resources/demos/svgimageview.swf" WIDTH=725 HEIGHT=461
 * quality=low loop=false wmode=transparent TYPE="application/x-shockwave-flash"
 * PLUGINSPAGE=
 * "http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"
 * > </EMBED> </OBJECT></center> <SCRIPT
 * src='../../../../resources/demos/pagecurl.js'></script> <br/>
 * <b>Using the SVGImageView</b><br/>
 * <code>
 * 	&lt;com.mobifluence.rage.ui.widget.imageview.SVGImageView<br/>
 * 	&emsp;android:id="@+id/image"<br/>
 * 	&emsp;android:layout_width="wrap_content"<br/>
 *  &emsp;android:layout_height="wrap_content"<br/>
 *  &emsp;app:svgZoomFactor="100" <br/>
 *  &emsp;app:svgResource="@drawable/giraffe"<br/>
 *  /&gt;
 * </code> <br/>
 * </p>
 * 
 * @see SVGFactory
 * @see SVG
 * @author elton kent
 */
public class SVGImageView extends ImageView {

	private class LoadSVGTask extends AsyncTask<Void, Void, Drawable> {
		@Override
		protected Drawable doInBackground(final Void... params) {
			Drawable drawable = null;
			InputStream is = null;
			switch (currentSource) {
			case LOAD_ASSET:
				try {
					is = assetManager.open(assetPath);
				} catch (final IOException e) {
					e.printStackTrace();
				}
				break;
			case LOAD_FILE:
				try {
					is = new FileInputStream(file);
				} catch (final FileNotFoundException e) {
					e.printStackTrace();
				}
				break;
			case LOAD_IS:
				is = stream;
				break;
			case LOAD_RESOURCE:
				is = getResources().openRawResource(resId);
				break;
			}
			final SVG svg = SVGFactory.getSVGFromInputStream(is, zoomFactor);
			if (svg != null) {
				drawable = svg.createDrawable();
				if (imageViewCallback != null) {
					imageViewCallback.onImageViewLoaded();
				}
			}
			return drawable;
		}

		@Override
		protected void onPostExecute(final Drawable result) {
			if (result == null) {
				Log.d("SVGImageView", "Render Drawable is null");
				return;
			}
			Log.d("SVGImageView", "setting drawable->" + result);
			setImageDrawable(result);
		}
	}

	private int zoomFactor = 100;

	private int resId;
	private AssetManager assetManager;
	private String assetPath;
	private InputStream stream;
	private File file;

	private int currentSource = -1;

	private static final int LOAD_RESOURCE = 10;
	private static final int LOAD_ASSET = 20;
	private static final int LOAD_IS = 30;
	private static final int LOAD_FILE = 40;

	public SVGImageView(final Context context) {
		super(context);
	}

	public SVGImageView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		initAttributes(attrs);
	}

	public SVGImageView(final Context context, final AttributeSet attrs,
			final int intt) {
		super(context, attrs, intt);
		initAttributes(attrs);
	}

	private void initAttributes(final AttributeSet attrs) {
		final TypedArray styledAttrs = getContext().obtainStyledAttributes(
				attrs, R.styleable.SVGImageView);
		zoomFactor = styledAttrs.getInteger(
				R.styleable.SVGImageView_svg_zoomFactor, 100);
		final int resource = styledAttrs.getResourceId(
				R.styleable.SVGImageView_svg_resource, -1);
		if (resource > 0) {
			setSVGFromResource(resource);
		}
		styledAttrs.recycle();

	}

	public void setSVGFile(final File svg) {
		this.file = svg;
		currentSource = LOAD_FILE;
		load();
	}

	private void load() {
		new LoadSVGTask().execute();
	}

	public void setSVGFromResource(final int resId) {
		if (imageViewCallback != null) {
			imageViewCallback.onResourceAssigned();
		}
		this.resId = resId;
		currentSource = LOAD_RESOURCE;
		load();
	}

	public void setSVGFromAsset(final AssetManager assetMngr,
			final String svgPath) {
		this.assetManager = assetMngr;
		this.assetPath = svgPath;
		currentSource = LOAD_ASSET;
		load();
	}

	public void setSVGFromInputStream(final InputStream svgData) {
		this.stream = svgData;
		currentSource = LOAD_IS;
		load();
	}

	/**
	 * Set the Zoom level for the loaded SVG Image.
	 * <p>
	 * This is a Asynchronous call.
	 * </p>
	 * 
	 * @param zoomFactor
	 * @throws IllegalStateException
	 *             if the SVG image has not been setKey.
	 */
	public void setZoomFactor(final int zoomFactor) {
		this.zoomFactor = zoomFactor;
		if (currentSource == -1) {
			throw new IllegalStateException("SVG Image has not been setKey!");
		}
		new LoadSVGTask().execute();
	}

	public int getZoomFactor() {
		return zoomFactor;
	}

	private ImageViewLoadingCallback imageViewCallback;

	public ImageViewLoadingCallback getImageViewCallback() {
		return imageViewCallback;
	}

	public void setImageViewCallback(
			final ImageViewLoadingCallback imageViewCallback) {
		this.imageViewCallback = imageViewCallback;
	}
}
