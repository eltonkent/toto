package toto.ui.widget.imageview;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Scroller;

/**
 * ImageView that smoothly scrolls through an image.
 * 
 * @see #setFlowVelocity(float)
 * @see #setEdgeDelay(int)
 */
public class FlowingImageView extends ImageView {
	private static final String TAG = "ToTo";
	private static final boolean DEBUG = false;

	private static void DEBUG_LOG(final String msg) {
		if (DEBUG) {
			Log.v(TAG, msg);
		}
	}

	private static final float MIN_FLOW_VELOCITY = 0.3333f; // dips per second
	private static final float DEFAULT_FLOW_VELOCITY = 5.5f; // dips per second
	private static final int DEFAULT_EDGE_DELAY = 2000; // ms

	private float mMinFlowVelocity;
	private float mFlowVelocity;
	private int mEdgeDelay = DEFAULT_EDGE_DELAY;

	private Scroller mScroller;

	private boolean mIsFlowInited;
	private boolean mIsFlowPositive = true;
	private boolean mFlowStarted;

	private final Matrix mImageMatrix = new Matrix();
	private float mScale;
	private float mTranslateX;
	private float mTranslateXEnd;

	private final Runnable mReverseFlowRunnable = new Runnable() {
		public void run() {
			mIsFlowPositive = !mIsFlowPositive;
			DEBUG_LOG("mReverseFlowRunnable mIsFlowPositive=" + mIsFlowPositive);
			startFlow();
		}
	};

	public FlowingImageView(final Context context) {
		super(context);
		initFlowImageView();
	}

	public FlowingImageView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		initFlowImageView();
	}

	public FlowingImageView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
		initFlowImageView();
	}

	private void initFlowImageView() {
		setScaleType(ImageView.ScaleType.MATRIX);

		final Context context = getContext();
		final float density = context.getResources().getDisplayMetrics().density;

		mMinFlowVelocity = MIN_FLOW_VELOCITY * density;
		mFlowVelocity = DEFAULT_FLOW_VELOCITY * density;

		mScroller = new Scroller(context, new LinearInterpolator());
	}

	@Override
	public void setEnabled(final boolean enabled) {
		if (enabled == isEnabled()) {
			return;
		}
		super.setEnabled(enabled);
		if (enabled) {
			startFlow();
		} else {
			stopFlow();
		}
	}

	public float getFlowVelocity() {
		return mFlowVelocity;
	}

	/**
	 * Setting flow velocity.
	 * 
	 * @param flowVelocity
	 *            pixels per second
	 */
	public void setFlowVelocity(float flowVelocity) {
		if (flowVelocity < mMinFlowVelocity) {
			flowVelocity = mMinFlowVelocity;
		}
		mFlowVelocity = flowVelocity;
		restartFlow();
	}

	public int getEdgeDelay() {
		return mEdgeDelay;
	}

	public void setEdgeDelay(int edgeDelay) {
		if (edgeDelay < 0) {
			edgeDelay = 0;
		}
		mEdgeDelay = edgeDelay;
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		startFlow();
	}

	@Override
	protected void onDetachedFromWindow() {
		stopFlow();
		super.onDetachedFromWindow();
	}

	@Override
	public void onWindowFocusChanged(final boolean hasWindowFocus) {
		super.onWindowFocusChanged(hasWindowFocus);
		DEBUG_LOG("onWindowFocusChanged hasWindowFocus=" + hasWindowFocus);
		if (hasWindowFocus) {
			startFlow();
		} else {
			stopFlow();
		}
	}

	@Override
	protected void onLayout(final boolean changed, final int left,
			final int top, final int right, final int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		DEBUG_LOG("onLayout changed=" + changed + ", left=" + left + ", top="
				+ top + ", right=" + right + ", bottom=" + bottom);
		initFlow();
		startFlow();
	}

	private void initFlow() {
		final Drawable drawable = getDrawable();
		if (drawable != null) {
			final float viewWidth = getWidth();
			final float viewHeight = getHeight();
			final float imgWidth = drawable.getIntrinsicWidth();
			final float imgHeight = drawable.getIntrinsicHeight();
			mScale = viewHeight / imgHeight;
			mTranslateXEnd = viewWidth - imgWidth * mScale;
			mIsFlowInited = true;
			setImageMatrix();
		} else {
			mIsFlowInited = false;
		}
	}

	private void restartFlow() {
		if (mFlowStarted) {
			startFlow();
		}
	}

	private void startFlow() {
		if (!isEnabled() || !mIsFlowInited) {
			return;
		}
		final int sx = (int) mTranslateX;
		final int dx = (int) ((mIsFlowPositive ? mTranslateXEnd : 0) - mTranslateX);
		if (dx == 0) {
			getHandler().removeCallbacks(mReverseFlowRunnable);
			getHandler().post(mReverseFlowRunnable);
			return;
		}
		final int duration = (int) Math.abs(dx / mFlowVelocity * 1000);
		DEBUG_LOG("startFlow mIsFlowPositive=" + mIsFlowPositive
				+ ", mTranslateX=" + mTranslateX + ", mTranslateXEnd="
				+ mTranslateXEnd + ", sx=" + sx + ", dx=" + dx + ", duration="
				+ duration);
		mScroller.abortAnimation();
		mScroller.startScroll(sx * 100, 0, dx * 100, 0, duration);
		mFlowStarted = true;
		ViewCompat.postInvalidateOnAnimation(this);
		// postInvalidate();
	}

	private void stopFlow() {
		mScroller.abortAnimation();
		if (getHandler() != null) {
			getHandler().removeCallbacks(mReverseFlowRunnable);
		}
		mFlowStarted = false;
	}

	@Override
	public void computeScroll() {
		if (!isEnabled() || !mIsFlowInited || !mFlowStarted) {
			return;
		}
		if (!mScroller.isFinished() && mScroller.computeScrollOffset()) {
			mTranslateX = mScroller.getCurrX() / 100f;
			DEBUG_LOG("computeScroll mTranslateX=" + mTranslateX);
			setImageMatrix();

			// Keep on drawing until the animation has finished.
			ViewCompat.postInvalidateOnAnimation(this);
			// postInvalidate();;
			return;
		}

		// Done with scroll, clean up state.
		stopFlow();
		getHandler().postDelayed(mReverseFlowRunnable, mEdgeDelay);
	}

	private void setImageMatrix() {
		mImageMatrix.reset();
		mImageMatrix.postScale(mScale, mScale);
		mImageMatrix.postTranslate(mTranslateX, 0);
		setImageMatrix(mImageMatrix);
	}

}