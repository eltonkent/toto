/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget.imageview;

/**
 * Helps notifying the loading of SVG,GIF imageviews.
 * <p>
 * These callbacks methods are not necessarily initiated from the main thread.
 * </p>
 * 
 * @author elton.kent
 * 
 */
public interface ImageViewLoadingCallback {

	public void onResourceAssigned();

	/**
	 * Called when the assigned image resource is loaded
	 */
	public void onImageViewLoaded();

	public void onLoadingFailed();

}
