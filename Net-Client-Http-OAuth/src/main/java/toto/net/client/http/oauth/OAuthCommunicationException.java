/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;


@SuppressWarnings("serial")
public class OAuthCommunicationException extends OAuthException {

	private String responseBody;

	public OAuthCommunicationException(final Exception cause) {
		super("Communication with the service provider failed: "
				+ cause.getLocalizedMessage(), cause);
	}

	public OAuthCommunicationException(final String message,
			final String responseBody) {
		super(message);
		this.responseBody = responseBody;
	}

	public String getResponseBody() {
		return responseBody;
	}
}
