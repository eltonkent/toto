/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth.signature;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;

import toto.net.client.http.oauth.HttpParameters;
import toto.net.client.http.oauth.HttpRequest;
import toto.net.client.http.oauth.OAuth;
import toto.net.client.http.oauth.OAuthMessageSignerException;

class SignatureBaseString {

	private final HttpRequest request;

	private final HttpParameters requestParameters;

	/**
	 * Constructs a new SBS instance that will operate on the given request
	 * object and parameter setKey.
	 * 
	 * @param request
	 *            the HTTP request
	 * @param requestParameters
	 *            the setKey of request parameters from the Authorization
	 *            header, query string and form body
	 */
	SignatureBaseString(final HttpRequest request,
			final HttpParameters requestParameters) {
		this.request = request;
		this.requestParameters = requestParameters;
	}

	/**
	 * Builds the signature base string from the data this instance was
	 * configured with.
	 * 
	 * @return the signature base string
	 * @throws OAuthMessageSignerException
	 */
	String generate() throws OAuthMessageSignerException {

		try {
			final String normalizedUrl = normalizeRequestUrl();
			final String normalizedParams = normalizeRequestParameters();

			return request.getMethod() + '&'
					+ OAuth.percentEncode(normalizedUrl) + '&'
					+ OAuth.percentEncode(normalizedParams);
		} catch (final Exception e) {
			throw new OAuthMessageSignerException(e);
		}
	}

	/**
	 * Normalizes the setKey of request parameters this instance was configured
	 * with, as per OAuth spec section 9.1.1.
	 * 
	 * @param parameters
	 *            the setKey of request parameters
	 * @return the normalized params string
	 * @throws IOException
	 */
	String normalizeRequestParameters() throws IOException {
		if (requestParameters == null) {
			return "";
		}

		final StringBuilder sb = new StringBuilder();
		final Iterator<String> iter = requestParameters.keySet().iterator();

		for (int i = 0; iter.hasNext(); i++) {
			final String param = iter.next();

			if (OAuth.OAUTH_SIGNATURE.equals(param) || "realm".equals(param)) {
				continue;
			}

			if (i > 0) {
				sb.append("&");
			}

			sb.append(requestParameters.getAsQueryString(param));
		}
		return sb.toString();
	}

	String normalizeRequestUrl() throws URISyntaxException {
		final URI uri = new URI(request.getRequestUrl());
		final String scheme = uri.getScheme().toLowerCase();
		String authority = uri.getAuthority().toLowerCase();
		final boolean dropPort = (scheme.equals("http") && uri.getPort() == 80)
				|| (scheme.equals("https") && uri.getPort() == 443);
		if (dropPort) {
			// find the last : in the authority
			final int index = authority.lastIndexOf(":");
			if (index >= 0) {
				authority = authority.substring(0, index);
			}
		}
		String path = uri.getRawPath();
		if (path == null || path.length() <= 0) {
			path = "/"; // conforms to RFC 2616 section 3.2.2
		}
		// we know that there is no query and no fragment here.
		return scheme + "://" + authority + path;
	}
}
