/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;


class UrlStringRequestAdapter implements HttpRequest {

	private String url;

	public UrlStringRequestAdapter(final String url) {
		this.url = url;
	}

	@Override
	public Map<String, String> getAllHeaders() {
		return Collections.emptyMap();
	}

	@Override
	public String getContentType() {
		return null;
	}

	@Override
	public String getHeader(final String name) {
		return null;
	}

	@Override
	public InputStream getMessagePayload() throws IOException {
		return null;
	}

	@Override
	public String getMethod() {
		return "GET";
	}

	@Override
	public String getRequestUrl() {
		return url;
	}

	@Override
	public void setRequestUrl(final String url) {
		this.url = url;
	}

	@Override
	public void setHeader(final String name, final String value) {
	}

	@Override
	public Object unwrap() {
		return url;
	}
}
