/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.client.methods.HttpUriRequest;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


public class HttpRequestAdapter implements HttpRequest {

	private final HttpUriRequest request;
	private HttpEntity entity;

	public HttpRequestAdapter(final HttpUriRequest request) {
		this.request = request;
		if (request instanceof HttpEntityEnclosingRequest) {
			entity = ((HttpEntityEnclosingRequest) request).getEntity();
		}
	}

	@Override
	public Map<String, String> getAllHeaders() {
		final Header[] origHeaders = request.getAllHeaders();
		final HashMap<String, String> headers = new HashMap<String, String>();
		for (final Header h : origHeaders) {
			headers.put(h.getName(), h.getValue());
		}
		return headers;
	}

	@Override
	public String getContentType() {
		if (entity == null) {
			return null;
		}
		final Header header = entity.getContentType();
		if (header == null) {
			return null;
		}
		return header.getValue();
	}

	@Override
	public String getHeader(final String name) {
		final Header header = request.getFirstHeader(name);
		if (header == null) {
			return null;
		}
		return header.getValue();
	}

	@Override
	public InputStream getMessagePayload() throws IOException {
		if (entity == null) {
			return null;
		}
		return entity.getContent();
	}

	@Override
	public String getMethod() {
		return request.getRequestLine().getMethod();
	}

	@Override
	public String getRequestUrl() {
		return request.getURI().toString();
	}

	@Override
	public void setRequestUrl(final String url) {
		throw new RuntimeException(new UnsupportedOperationException());
	}

	@Override
	public void setHeader(final String name, final String value) {
		request.setHeader(name, value);
	}

	@Override
	public Object unwrap() {
		return request;
	}
}
