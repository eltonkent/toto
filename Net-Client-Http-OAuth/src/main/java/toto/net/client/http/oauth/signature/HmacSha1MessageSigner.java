/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth.signature;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import toto.net.client.http.oauth.HttpParameters;
import toto.net.client.http.oauth.HttpRequest;
import toto.net.client.http.oauth.OAuth;
import toto.net.client.http.oauth.OAuthMessageSignerException;

@SuppressWarnings("serial")
public class HmacSha1MessageSigner extends OAuthMessageSigner {

	private static final String MAC_NAME = "HmacSHA1";

	@Override
	public String getSignatureMethod() {
		return "HMAC-SHA1";
	}

	@Override
	public String sign(final HttpRequest request,
			final HttpParameters requestParams)
			throws OAuthMessageSignerException {
		try {
			final String keyString = OAuth.percentEncode(getConsumerSecret())
					+ '&' + OAuth.percentEncode(getTokenSecret());
			final byte[] keyBytes = keyString.getBytes(OAuth.ENCODING);

			final SecretKey key = new SecretKeySpec(keyBytes, MAC_NAME);
			final Mac mac = Mac.getInstance(MAC_NAME);
			mac.init(key);

			final String sbs = new SignatureBaseString(request, requestParams)
					.generate();
			OAuth.debugOut("SBS", sbs);
			final byte[] text = sbs.getBytes(OAuth.ENCODING);

			return base64Encode(mac.doFinal(text)).trim();
		} catch (final GeneralSecurityException e) {
			throw new OAuthMessageSignerException(e);
		} catch (final UnsupportedEncodingException e) {
			throw new OAuthMessageSignerException(e);
		}
	}
}
