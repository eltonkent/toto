/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;


/**
 * A concise description of an HTTP request. Contains methods to access all
 * those parts of an HTTP request which OAuth needs to sign a message. If you
 * want to extend OAuth to sign a different kind of HTTP request than those
 * currently supported, you'll have to write an adapter which implements this
 * interface and a custom {@link OAuthConsumer} which performs the wrapping.
 * 
 * @see HttpURLConnectionRequestAdapter
 */
public interface HttpRequest {

	Map<String, String> getAllHeaders();

	String getContentType();

	String getHeader(String name);

	InputStream getMessagePayload() throws IOException;

	String getMethod();

	String getRequestUrl();

	void setRequestUrl(String url);

	void setHeader(String name, String value);

	/**
	 * Returns the wrapped request object, in case you must work directly on it.
	 * 
	 * @return the wrapped request object
	 */
	Object unwrap();
}
