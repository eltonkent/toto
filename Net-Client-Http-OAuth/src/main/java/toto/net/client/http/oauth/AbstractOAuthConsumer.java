/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import toto.net.client.http.oauth.signature.AuthorizationHeaderSigningStrategy;
import toto.net.client.http.oauth.signature.HmacSha1MessageSigner;
import toto.net.client.http.oauth.signature.OAuthMessageSigner;
import toto.net.client.http.oauth.signature.QueryStringSigningStrategy;
import toto.net.client.http.oauth.signature.SigningStrategy;

/**
 * ABC for consumer implementations. If you're developing a custom consumer you
 * will probably inherit from this class to save you a lot of work.
 * 
 */
public abstract class AbstractOAuthConsumer implements toto.net.client.http.oauth.OAuthConsumer {

	private static final long serialVersionUID = 1L;
	private final String consumerKey, consumerSecret;
	// these are params that may be passed to the consumer directly (i.e.
	// without going through the request object)
	private toto.net.client.http.oauth.HttpParameters additionalParameters;
	private OAuthMessageSigner messageSigner;

	// these are the params which will be passed to the message signer
	private toto.net.client.http.oauth.HttpParameters requestParameters;

	private boolean sendEmptyTokens;

	private SigningStrategy signingStrategy;

	private String token;

	public AbstractOAuthConsumer(final String consumerKey,
			final String consumerSecret) {
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
		setMessageSigner(new HmacSha1MessageSigner());
		setSigningStrategy(new AuthorizationHeaderSigningStrategy());
	}

	/**
	 * Collects x-www-form-urlencoded body parameters as per OAuth Core 1.0 spec
	 * section 9.1.1
	 */
	protected void collectBodyParameters(final HttpRequest request,
			final toto.net.client.http.oauth.HttpParameters out) throws IOException {

		// collect x-www-form-urlencoded body params
		final String contentType = request.getContentType();
		if (contentType != null && contentType.startsWith(toto.net.client.http.oauth.OAuth.FORM_ENCODED)) {
			final InputStream payload = request.getMessagePayload();
			out.putAll(toto.net.client.http.oauth.OAuth.decodeForm(payload), true);
		}
	}

	/**
	 * Collects OAuth Authorization header parameters as per OAuth Core 1.0 spec
	 * section 9.1.1
	 */
	protected void collectHeaderParameters(final HttpRequest request,
			final toto.net.client.http.oauth.HttpParameters out) {
		final toto.net.client.http.oauth.HttpParameters headerParams = toto.net.client.http.oauth.OAuth
				.oauthHeaderToParamsMap(request
						.getHeader(toto.net.client.http.oauth.OAuth.HTTP_AUTHORIZATION_HEADER));
		out.putAll(headerParams, false);
	}

	/**
	 * Collects HTTP GET query string parameters as per OAuth Core 1.0 spec
	 * section 9.1.1
	 */
	protected void collectQueryParameters(final HttpRequest request,
			final toto.net.client.http.oauth.HttpParameters out) {

		final String url = request.getRequestUrl();
		final int q = url.indexOf('?');
		if (q >= 0) {
			// Combine the URL query string with the other parameters:
			out.putAll(toto.net.client.http.oauth.OAuth.decodeForm(url.substring(q + 1)), true);
		}
	}

	/**
	 * <p>
	 * Helper method that adds any OAuth parameters to the given request
	 * parameters which are missing from the current request but required for
	 * signing. A good example is the oauth_nonce parameter, which is typically
	 * not provided by the client in advance.
	 * </p>
	 * <p>
	 * It's probably not a very good idea to override this method. If you want
	 * to generate different nonces or timestamps, override
	 * {@link #generateNonce()} or {@link #generateTimestamp()} instead.
	 * </p>
	 *
	 * @param out
	 *            the request parameter which should be completed
	 */
	protected void completeOAuthParameters(final toto.net.client.http.oauth.HttpParameters out) {
		if (!out.containsKey(toto.net.client.http.oauth.OAuth.OAUTH_CONSUMER_KEY)) {
			out.put(toto.net.client.http.oauth.OAuth.OAUTH_CONSUMER_KEY, consumerKey, true);
		}
		if (!out.containsKey(toto.net.client.http.oauth.OAuth.OAUTH_SIGNATURE_METHOD)) {
			out.put(toto.net.client.http.oauth.OAuth.OAUTH_SIGNATURE_METHOD,
					messageSigner.getSignatureMethod(), true);
		}
		if (!out.containsKey(toto.net.client.http.oauth.OAuth.OAUTH_TIMESTAMP)) {
			out.put(toto.net.client.http.oauth.OAuth.OAUTH_TIMESTAMP, generateTimestamp(), true);
		}
		if (!out.containsKey(toto.net.client.http.oauth.OAuth.OAUTH_NONCE)) {
			out.put(toto.net.client.http.oauth.OAuth.OAUTH_NONCE, generateNonce(), true);
		}
		if (!out.containsKey(toto.net.client.http.oauth.OAuth.OAUTH_VERSION)) {
			out.put(toto.net.client.http.oauth.OAuth.OAUTH_VERSION, toto.net.client.http.oauth.OAuth.VERSION_1_0, true);
		}
		if (!out.containsKey(toto.net.client.http.oauth.OAuth.OAUTH_TOKEN)) {
			if (token != null && !token.equals("") || sendEmptyTokens) {
				out.put(toto.net.client.http.oauth.OAuth.OAUTH_TOKEN, token, true);
			}
		}
	}

	protected String generateNonce() {
		return Long.toString(new Random().nextLong());
	}

	protected String generateTimestamp() {
		return Long.toString(System.currentTimeMillis() / 1000L);
	}

	@Override
	public String getConsumerKey() {
		return this.consumerKey;
	}

	@Override
	public String getConsumerSecret() {
		return this.consumerSecret;
	}

	@Override
	public toto.net.client.http.oauth.HttpParameters getRequestParameters() {
		return requestParameters;
	}

	@Override
	public String getToken() {
		return token;
	}

	@Override
	public String getTokenSecret() {
		return messageSigner.getTokenSecret();
	}

	@Override
	public void setAdditionalParameters(
			final toto.net.client.http.oauth.HttpParameters additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	@Override
	public void setMessageSigner(final OAuthMessageSigner messageSigner) {
		this.messageSigner = messageSigner;
		messageSigner.setConsumerSecret(consumerSecret);
	}

	@Override
	public void setSendEmptyTokens(final boolean enable) {
		this.sendEmptyTokens = enable;
	}

	@Override
	public void setSigningStrategy(final SigningStrategy signingStrategy) {
		this.signingStrategy = signingStrategy;
	}

	@Override
	public void setTokenWithSecret(final String token, final String tokenSecret) {
		this.token = token;
		messageSigner.setTokenSecret(tokenSecret);
	}

	@Override
	public HttpRequest sign(final HttpRequest request)
			throws toto.net.client.http.oauth.OAuthMessageSignerException,
			OAuthExpectationFailedException, toto.net.client.http.oauth.OAuthCommunicationException {
		if (consumerKey == null) {
			throw new OAuthExpectationFailedException("consumer key not setKey");
		}
		if (consumerSecret == null) {
			throw new OAuthExpectationFailedException(
					"consumer secret not setKey");
		}

		requestParameters = new toto.net.client.http.oauth.HttpParameters();
		try {
			if (additionalParameters != null) {
				requestParameters.putAll(additionalParameters, false);
			}
			collectHeaderParameters(request, requestParameters);
			collectQueryParameters(request, requestParameters);
			collectBodyParameters(request, requestParameters);

			// add any OAuth params that haven't already been setKey
			completeOAuthParameters(requestParameters);

			requestParameters.remove(toto.net.client.http.oauth.OAuth.OAUTH_SIGNATURE);

		} catch (final IOException e) {
			throw new toto.net.client.http.oauth.OAuthCommunicationException(e);
		}

		final String signature = messageSigner.sign(request, requestParameters);
		toto.net.client.http.oauth.OAuth.debugOut("signature", signature);

		signingStrategy.writeSignature(signature, request, requestParameters);
		toto.net.client.http.oauth.OAuth.debugOut("Auth header", request.getHeader("Authorization"));
		toto.net.client.http.oauth.OAuth.debugOut("Request URL", request.getRequestUrl());

		return request;
	}

	@Override
	public HttpRequest sign(final Object request)
			throws toto.net.client.http.oauth.OAuthMessageSignerException,
			OAuthExpectationFailedException, toto.net.client.http.oauth.OAuthCommunicationException {
		return sign(wrap(request));
	}

	@Override
	public String sign(final String url) throws toto.net.client.http.oauth.OAuthMessageSignerException,
			OAuthExpectationFailedException, toto.net.client.http.oauth.OAuthCommunicationException {
		final HttpRequest request = new UrlStringRequestAdapter(url);

		// switch to URL signing
		final SigningStrategy oldStrategy = this.signingStrategy;
		this.signingStrategy = new QueryStringSigningStrategy();

		sign(request);

		// revert to old strategy
		this.signingStrategy = oldStrategy;

		return request.getRequestUrl();
	}

	/**
	 * Adapts the given request object to a Signpost {@link HttpRequest}. How
	 * this is done depends on the consumer implementation.
	 *
	 * @param request
	 *            the native HTTP request instance
	 * @return the adapted request
	 */
	protected abstract HttpRequest wrap(Object request);
}
