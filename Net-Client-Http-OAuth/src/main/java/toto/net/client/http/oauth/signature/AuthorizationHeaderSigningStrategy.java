/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth.signature;

import toto.net.client.http.oauth.HttpParameters;
import toto.net.client.http.oauth.HttpRequest;
import toto.net.client.http.oauth.OAuth;

/**
 * Writes to the HTTP Authorization header field.
 * 
 */
public class AuthorizationHeaderSigningStrategy implements SigningStrategy {

	private static final long serialVersionUID = 1L;

	@Override
	public String writeSignature(final String signature,
			final HttpRequest request, final HttpParameters requestParameters) {
		final StringBuilder sb = new StringBuilder();

		sb.append("OAuth ");
		if (requestParameters.containsKey("realm")) {
			sb.append(requestParameters.getAsHeaderElement("realm"));
			sb.append(", ");
		}
		if (requestParameters.containsKey(OAuth.OAUTH_TOKEN)) {
			sb.append(requestParameters.getAsHeaderElement(OAuth.OAUTH_TOKEN));
			sb.append(", ");
		}
		if (requestParameters.containsKey(OAuth.OAUTH_CALLBACK)) {
			sb.append(requestParameters
					.getAsHeaderElement(OAuth.OAUTH_CALLBACK));
			sb.append(", ");
		}
		if (requestParameters.containsKey(OAuth.OAUTH_VERIFIER)) {
			sb.append(requestParameters
					.getAsHeaderElement(OAuth.OAUTH_VERIFIER));
			sb.append(", ");
		}
		sb.append(requestParameters
				.getAsHeaderElement(OAuth.OAUTH_CONSUMER_KEY));
		sb.append(", ");
		sb.append(requestParameters.getAsHeaderElement(OAuth.OAUTH_VERSION));
		sb.append(", ");
		sb.append(requestParameters
				.getAsHeaderElement(OAuth.OAUTH_SIGNATURE_METHOD));
		sb.append(", ");
		sb.append(requestParameters.getAsHeaderElement(OAuth.OAUTH_TIMESTAMP));
		sb.append(", ");
		sb.append(requestParameters.getAsHeaderElement(OAuth.OAUTH_NONCE));
		sb.append(", ");
		sb.append(OAuth.toHeaderElement(OAuth.OAUTH_SIGNATURE, signature));

		final String header = sb.toString();
		request.setHeader(OAuth.HTTP_AUTHORIZATION_HEADER, header);

		return header;
	}

}
