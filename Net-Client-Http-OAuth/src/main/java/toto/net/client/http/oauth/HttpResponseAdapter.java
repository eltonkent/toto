/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;

import java.io.IOException;
import java.io.InputStream;


public class HttpResponseAdapter implements HttpResponse {

	private final org.apache.http.HttpResponse response;

	public HttpResponseAdapter(final org.apache.http.HttpResponse response) {
		this.response = response;
	}

	@Override
	public InputStream getContent() throws IOException {
		return response.getEntity().getContent();
	}

	@Override
	public String getReasonPhrase() throws Exception {
		return response.getStatusLine().getReasonPhrase();
	}

	@Override
	public int getStatusCode() throws IOException {
		return response.getStatusLine().getStatusCode();
	}

	@Override
	public Object unwrap() {
		return response;
	}
}
