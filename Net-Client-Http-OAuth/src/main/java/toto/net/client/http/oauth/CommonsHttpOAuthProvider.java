/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;


/**
 * This implementation uses the Apache Commons {@link HttpClient} 4.x HTTP
 * implementation to fetch OAuth tokens from a service provider. Android users
 * should use this provider implementation in favor of the default one, since
 * the latter is known to cause problems with Android's Apache Harmony
 * underpinnings.
 * 
 */
public class CommonsHttpOAuthProvider extends AbstractOAuthProvider {

	private static final long serialVersionUID = 1L;

	private transient HttpClient httpClient;

	public CommonsHttpOAuthProvider(final String requestTokenEndpointUrl,
			final String accessTokenEndpointUrl,
			final String authorizationWebsiteUrl) {
		super(requestTokenEndpointUrl, accessTokenEndpointUrl,
				authorizationWebsiteUrl);
		this.httpClient = new DefaultHttpClient();
	}

	public CommonsHttpOAuthProvider(final String requestTokenEndpointUrl,
			final String accessTokenEndpointUrl,
			final String authorizationWebsiteUrl, final HttpClient httpClient) {
		super(requestTokenEndpointUrl, accessTokenEndpointUrl,
				authorizationWebsiteUrl);
		this.httpClient = httpClient;
	}

	@Override
	protected void closeConnection(final HttpRequest request,
			final toto.net.client.http.oauth.HttpResponse response) throws Exception {
		if (response != null) {
			final HttpEntity entity = ((HttpResponse) response.unwrap())
					.getEntity();
			if (entity != null) {
				try {
					// free the connection
					entity.consumeContent();
				} catch (final IOException e) {
					// this means HTTP keep-alive is not possible
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	protected HttpRequest createRequest(final String endpointUrl)
			throws Exception {
		final HttpPost request = new HttpPost(endpointUrl);
		return new HttpRequestAdapter(request);
	}

	@Override
	protected toto.net.client.http.oauth.HttpResponse sendRequest(
			final HttpRequest request) throws Exception {
		final HttpResponse response = httpClient
				.execute((HttpUriRequest) request.unwrap());
		return new HttpResponseAdapter(response);
	}

	public void setHttpClient(final HttpClient httpClient) {
		this.httpClient = httpClient;
	}
}
