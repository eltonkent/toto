/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


/**
 * ABC for all provider implementations. If you're writing a custom provider,
 * you will probably inherit from this class, since it takes a lot of work from
 * you.
 * 
 */
public abstract class AbstractOAuthProvider implements OAuthProvider {

	private static final long serialVersionUID = 1L;

	private final String accessTokenEndpointUrl;

	private final String authorizationWebsiteUrl;

	private final Map<String, String> defaultHeaders;
	private final String requestTokenEndpointUrl;
	private boolean isOAuth10a;
	private transient OAuthProviderListener listener;
	private HttpParameters responseParameters;

	public AbstractOAuthProvider(final String requestTokenEndpointUrl,
			final String accessTokenEndpointUrl,
			final String authorizationWebsiteUrl) {
		this.requestTokenEndpointUrl = requestTokenEndpointUrl;
		this.accessTokenEndpointUrl = accessTokenEndpointUrl;
		this.authorizationWebsiteUrl = authorizationWebsiteUrl;
		this.responseParameters = new HttpParameters();
		this.defaultHeaders = new HashMap<String, String>();
	}

	/**
	 * Called when the connection is being finalized after receiving the
	 * response. Use this to do any cleanup / resource freeing.
	 *
	 * @param request
	 *            the request that has been sent
	 * @param response
	 *            the response that has been received
	 * @throws Exception
	 *             if something breaks
	 */
	protected void closeConnection(final HttpRequest request,
								   final HttpResponse response) throws Exception {
		// NOP
	}

	/**
	 * Overrride this method if you want to customize the logic for building a
	 * request object for the given endpoint URL.
	 *
	 * @param endpointUrl
	 *            the URL to which the request will go
	 * @return the request object
	 * @throws Exception
	 *             if something breaks
	 */
	protected abstract HttpRequest createRequest(String endpointUrl)
			throws Exception;

	@Override
	public String getAccessTokenEndpointUrl() {
		return this.accessTokenEndpointUrl;
	}

	@Override
	public String getAuthorizationWebsiteUrl() {
		return this.authorizationWebsiteUrl;
	}

	@Override
	public Map<String, String> getRequestHeaders() {
		return defaultHeaders;
	}

	@Override
	public String getRequestTokenEndpointUrl() {
		return this.requestTokenEndpointUrl;
	}

	/**
	 * Returns a single query parameter as served by the service provider in a
	 * token reply. You must call {@link #setResponseParameters} with the setKey
	 * of parameters before using this method.
	 *
	 * @param key
	 *            the parameter name
	 * @return the parameter value
	 */
	protected String getResponseParameter(final String key) {
		return responseParameters.getFirst(key);
	}

	@Override
	public HttpParameters getResponseParameters() {
		return responseParameters;
	}

	@Override
	public void setResponseParameters(final HttpParameters parameters) {
		this.responseParameters = parameters;
	}

	protected void handleUnexpectedResponse(final int statusCode,
											final HttpResponse response) throws Exception {
		if (response == null) {
			return;
		}
		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				response.getContent()));
		final StringBuilder responseBody = new StringBuilder();

		String line = reader.readLine();
		while (line != null) {
			responseBody.append(line);
			line = reader.readLine();
		}

		switch (statusCode) {
		case 401:
			throw new OAuthNotAuthorizedException(responseBody.toString());
		default:
			throw new toto.net.client.http.oauth.OAuthCommunicationException(
					"Service provider responded in error: " + statusCode + " ("
							+ response.getReasonPhrase() + ")",
					responseBody.toString());
		}
	}

	@Override
	public boolean isOAuth10a() {
		return isOAuth10a;
	}

	@Override
	public void setOAuth10a(final boolean isOAuth10aProvider) {
		this.isOAuth10a = isOAuth10aProvider;
	}

	@Override
	public void removeListener(final OAuthProviderListener listener) {
		this.listener = null;
	}

	@Override
	public void retrieveAccessToken(final OAuthConsumer consumer,
			final String oauthVerifier) throws toto.net.client.http.oauth.OAuthMessageSignerException,
			OAuthNotAuthorizedException, OAuthExpectationFailedException,
			toto.net.client.http.oauth.OAuthCommunicationException {

		if (consumer.getToken() == null || consumer.getTokenSecret() == null) {
			throw new OAuthExpectationFailedException(
					"Authorized request token or token secret not setKey. "
							+ "Did you retrieve an authorized request token before?");
		}

		if (isOAuth10a && oauthVerifier != null) {
			retrieveToken(consumer, accessTokenEndpointUrl,
					OAuth.OAUTH_VERIFIER, oauthVerifier);
		} else {
			retrieveToken(consumer, accessTokenEndpointUrl);
		}
	}

	@Override
	public String retrieveRequestToken(final OAuthConsumer consumer,
			final String callbackUrl) throws toto.net.client.http.oauth.OAuthMessageSignerException,
			OAuthNotAuthorizedException, OAuthExpectationFailedException,
			toto.net.client.http.oauth.OAuthCommunicationException {

		// invalidate current credentials, if any
		consumer.setTokenWithSecret(null, null);

		// 1.0a expects the callback to be sent while getting the request token.
		// 1.0 service providers would simply ignore this parameter.
		retrieveToken(consumer, requestTokenEndpointUrl, OAuth.OAUTH_CALLBACK,
				callbackUrl);

		final String callbackConfirmed = responseParameters
				.getFirst(OAuth.OAUTH_CALLBACK_CONFIRMED);
		responseParameters.remove(OAuth.OAUTH_CALLBACK_CONFIRMED);
		isOAuth10a = Boolean.TRUE.toString().equals(callbackConfirmed);

		// 1.0 service providers expect the callback as part of the auth URL,
		// Do not send when 1.0a.
		if (isOAuth10a) {
			return OAuth.addQueryParameters(authorizationWebsiteUrl,
					OAuth.OAUTH_TOKEN, consumer.getToken());
		} else {
			return OAuth.addQueryParameters(authorizationWebsiteUrl,
					OAuth.OAUTH_TOKEN, consumer.getToken(),
					OAuth.OAUTH_CALLBACK, callbackUrl);
		}
	}

	/**
	 * <p>
	 * Implemented by subclasses. The responsibility of this method is to
	 * contact the service provider at the given endpoint URL and fetch a
	 * request or access token. What kind of token is retrieved solely depends
	 * on the URL being used.
	 * </p>
	 * <p>
	 * Correct implementations of this method must guarantee the following
	 * post-conditions:
	 * <ul>
	 * <li>the {@link OAuthConsumer} passed to this method must have a valid
	 * {@link OAuth#OAUTH_TOKEN} and {@link OAuth#OAUTH_TOKEN_SECRET} setKey by
	 * calling {@link OAuthConsumer#setTokenWithSecret(String, String)}</li>
	 * <li>{@link #getResponseParameters()} must return the setKey of query
	 * parameters served by the service provider in the token response, with all
	 * OAuth specific parameters being removed</li>
	 * </ul>
	 * </p>
	 *
	 * @param consumer
	 *            the {@link OAuthConsumer} that should be used to sign the
	 *            request
	 * @param endpointUrl
	 *            the URL at which the service provider serves the OAuth token
	 *            that is to be fetched
	 * @param additionalParameters
	 *            you can pass parameters here (typically OAuth parameters such
	 *            as oauth_callback or oauth_verifier) which will go directly
	 *            into the signer, i.e. you don't have to put them into the
	 *            request first, just so the consumer pull them out again. Pass
	 *            them sequentially in key/value order.
	 * @throws toto.net.client.http.oauth.OAuthMessageSignerException
	 *             if signing the token request fails
	 * @throws toto.net.client.http.oauth.OAuthCommunicationException
	 *             if a network communication error occurs
	 * @throws OAuthNotAuthorizedException
	 *             if the server replies 401 - Unauthorized
	 * @throws OAuthExpectationFailedException
	 *             if an expectation has failed, e.g. because the server didn't
	 *             reply in the expected format
	 */
	protected void retrieveToken(final OAuthConsumer consumer,
			final String endpointUrl, final String... additionalParameters)
			throws toto.net.client.http.oauth.OAuthMessageSignerException, toto.net.client.http.oauth.OAuthCommunicationException,
			OAuthNotAuthorizedException, OAuthExpectationFailedException {
		final Map<String, String> defaultHeaders = getRequestHeaders();

		if (consumer.getConsumerKey() == null
				|| consumer.getConsumerSecret() == null) {
			throw new OAuthExpectationFailedException(
					"Consumer key or secret not setKey");
		}

		HttpRequest request = null;
		HttpResponse response = null;
		try {
			request = createRequest(endpointUrl);
			for (final String header : defaultHeaders.keySet()) {
				request.setHeader(header, defaultHeaders.get(header));
			}
			if (additionalParameters != null) {
				final HttpParameters httpParams = new HttpParameters();
				httpParams.putAll(additionalParameters, true);
				consumer.setAdditionalParameters(httpParams);
			}

			if (this.listener != null) {
				this.listener.prepareRequest(request);
			}

			consumer.sign(request);

			if (this.listener != null) {
				this.listener.prepareSubmission(request);
			}

			response = sendRequest(request);
			final int statusCode = response.getStatusCode();

			boolean requestHandled = false;
			if (this.listener != null) {
				requestHandled = this.listener.onResponseReceived(request,
						response);
			}
			if (requestHandled) {
				return;
			}

			if (statusCode >= 300) {
				handleUnexpectedResponse(statusCode, response);
			}

			final HttpParameters responseParams = OAuth.decodeForm(response
					.getContent());

			final String token = responseParams.getFirst(OAuth.OAUTH_TOKEN);
			final String secret = responseParams
					.getFirst(OAuth.OAUTH_TOKEN_SECRET);
			responseParams.remove(OAuth.OAUTH_TOKEN);
			responseParams.remove(OAuth.OAUTH_TOKEN_SECRET);

			setResponseParameters(responseParams);

			if (token == null || secret == null) {
				throw new OAuthExpectationFailedException(
						"Request token or token secret not setKey in server reply. "
								+ "The service provider you use is probably buggy.");
			}

			consumer.setTokenWithSecret(token, secret);

		} catch (final OAuthNotAuthorizedException e) {
			throw e;
		} catch (final OAuthExpectationFailedException e) {
			throw e;
		} catch (final Exception e) {
			throw new toto.net.client.http.oauth.OAuthCommunicationException(e);
		} finally {
			try {
				closeConnection(request, response);
			} catch (final Exception e) {
				throw new toto.net.client.http.oauth.OAuthCommunicationException(e);
			}
		}
	}

	/**
	 * Override this method if you want to customize the logic for how the given
	 * request is sent to the server.
	 *
	 * @param request
	 *            the request to send
	 * @return the response to the request
	 * @throws Exception
	 *             if something breaks
	 */
	protected abstract HttpResponse sendRequest(HttpRequest request)
			throws Exception;

	@Override
	public void setListener(final OAuthProviderListener listener) {
		this.listener = listener;
	}

	@Override
	public void setRequestHeader(final String header, final String value) {
		defaultHeaders.put(header, value);
	}
}
