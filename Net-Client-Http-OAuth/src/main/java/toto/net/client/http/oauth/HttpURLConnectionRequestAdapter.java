/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HttpURLConnectionRequestAdapter implements toto.net.client.http.oauth.HttpRequest {

	protected HttpURLConnection connection;

	public HttpURLConnectionRequestAdapter(final HttpURLConnection connection) {
		this.connection = connection;
	}

	@Override
	public Map<String, String> getAllHeaders() {
		final Map<String, List<String>> origHeaders = connection
				.getRequestProperties();
		final Map<String, String> headers = new HashMap<String, String>(
				origHeaders.size());
		for (final String name : origHeaders.keySet()) {
			final List<String> values = origHeaders.get(name);
			if (!values.isEmpty()) {
				headers.put(name, values.get(0));
			}
		}
		return headers;
	}

	@Override
	public String getContentType() {
		return connection.getRequestProperty("Content-Type");
	}

	@Override
	public String getHeader(final String name) {
		return connection.getRequestProperty(name);
	}

	@Override
	public InputStream getMessagePayload() throws IOException {
		return null;
	}

	@Override
	public String getMethod() {
		return connection.getRequestMethod();
	}

	@Override
	public String getRequestUrl() {
		return connection.getURL().toExternalForm();
	}

	@Override
	public void setRequestUrl(final String url) {
		// can't do
	}

	@Override
	public void setHeader(final String name, final String value) {
		connection.setRequestProperty(name, value);
	}

	@Override
	public HttpURLConnection unwrap() {
		return connection;
	}
}
