/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;

import java.io.IOException;
import java.io.InputStream;

public interface HttpResponse {

	InputStream getContent() throws IOException;

	String getReasonPhrase() throws Exception;

	int getStatusCode() throws IOException;

	/**
	 * Returns the underlying response object, in case you need to work on it
	 * directly.
	 * 
	 * @return the wrapped response object
	 */
	Object unwrap();
}
