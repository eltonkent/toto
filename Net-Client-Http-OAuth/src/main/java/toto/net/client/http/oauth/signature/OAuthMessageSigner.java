/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth.signature;

import android.util.Base64;

import java.io.IOException;
import java.io.Serializable;

import toto.net.client.http.oauth.HttpParameters;
import toto.net.client.http.oauth.HttpRequest;
import toto.net.client.http.oauth.OAuthMessageSignerException;

public abstract class OAuthMessageSigner implements Serializable {

	private static final long serialVersionUID = 4445779788786131202L;

	private String consumerSecret;

	private String tokenSecret;

	public OAuthMessageSigner() {
	}

	protected String base64Encode(final byte[] b) {
		return new String(Base64.encode(b, Base64.DEFAULT));
	}

	protected byte[] decodeBase64(final String s) {
		return Base64.decode(s.getBytes(), Base64.DEFAULT);
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public void setConsumerSecret(final String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}

	public abstract String getSignatureMethod();

	public String getTokenSecret() {
		return tokenSecret;
	}

	public void setTokenSecret(final String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}

	private void readObject(final java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
	}

	public abstract String sign(HttpRequest request,
			HttpParameters requestParameters)
			throws OAuthMessageSignerException;
}
