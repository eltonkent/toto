/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth;


/**
 * Provides hooks into the token request handling procedure executed by
 * {@link OAuthProvider}.
 * 
 */
public interface OAuthProviderListener {

	/**
	 * Called when the server response has been received. You can implement this
	 * to manually handle the response data.
	 * 
	 * @param request
	 *            the request that was sent
	 * @param response
	 *            the response that was received
	 * @return returning true means you have handled the response, and the
	 *         provider will return immediately. Return false to let the event
	 *         propagate and let the provider execute its default response
	 *         handling.
	 * @throws Exception
	 */
	boolean onResponseReceived(HttpRequest request, HttpResponse response)
			throws Exception;

	/**
	 * Called after the request has been created and default headers added, but
	 * before the request has been signed.
	 * 
	 * @param request
	 *            the request to be sent
	 * @throws Exception
	 */
	void prepareRequest(HttpRequest request) throws Exception;

	/**
	 * Called after the request has been signed, but before it's being sent.
	 * 
	 * @param request
	 *            the request to be sent
	 * @throws Exception
	 */
	void prepareSubmission(HttpRequest request) throws Exception;
}
