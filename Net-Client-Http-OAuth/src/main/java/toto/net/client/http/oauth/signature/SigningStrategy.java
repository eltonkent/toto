/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth.signature;

import java.io.Serializable;

import toto.net.client.http.oauth.HttpParameters;
import toto.net.client.http.oauth.HttpRequest;

/**
 * <p>
 * Defines how an OAuth signature string is written to a request.
 * </p>
 * <p>
 * Unlike {@link OAuthMessageSigner}, which is concerned with <i>how</i> to
 * generate a signature, this class is concered with <i>where</i> to write it
 * (e.g. HTTP header or query string).
 * </p>
 * 
 */
public interface SigningStrategy extends Serializable {

	/**
	 * Writes an OAuth signature and all remaining required parameters to an
	 * HTTP message.
	 * 
	 * @param signature
	 *            the signature to write
	 * @param request
	 *            the request to sign
	 * @param requestParameters
	 *            the request parameters
	 * @return whatever has been written to the request, e.g. an Authorization
	 *         header field
	 */
	String writeSignature(String signature, HttpRequest request,
						  HttpParameters requestParameters);

}
