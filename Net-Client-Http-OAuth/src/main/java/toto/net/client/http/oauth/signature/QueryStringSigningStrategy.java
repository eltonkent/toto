/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.oauth.signature;

import toto.net.client.http.oauth.HttpParameters;
import toto.net.client.http.oauth.HttpRequest;
import toto.net.client.http.oauth.OAuth;

/**
 * Writes to a URL query string. <strong>Note that this currently ONLY works
 * when signing a URL directly, not with HTTP request objects.</strong> That's
 * because most HTTP request implementations do not allow the client to change
 * the URL once the request has been instantiated, so there is no way to append
 * parameters to it.
 * 
 */
public class QueryStringSigningStrategy implements SigningStrategy {

	private static final long serialVersionUID = 1L;

	@Override
	public String writeSignature(final String signature,
			final HttpRequest request, final HttpParameters requestParameters) {

		// add the signature
		final StringBuilder sb = new StringBuilder(OAuth.addQueryParameters(
				request.getRequestUrl(), OAuth.OAUTH_SIGNATURE, signature));

		// add the optional OAuth parameters
		if (requestParameters.containsKey(OAuth.OAUTH_TOKEN)) {
			sb.append("&");
			sb.append(requestParameters.getAsQueryString(OAuth.OAUTH_TOKEN));
		}
		if (requestParameters.containsKey(OAuth.OAUTH_CALLBACK)) {
			sb.append("&");
			sb.append(requestParameters.getAsQueryString(OAuth.OAUTH_CALLBACK));
		}
		if (requestParameters.containsKey(OAuth.OAUTH_VERIFIER)) {
			sb.append("&");
			sb.append(requestParameters.getAsQueryString(OAuth.OAUTH_VERIFIER));
		}

		// add the remaining OAuth params
		sb.append("&");
		sb.append(requestParameters.getAsQueryString(OAuth.OAUTH_CONSUMER_KEY));
		sb.append("&");
		sb.append(requestParameters.getAsQueryString(OAuth.OAUTH_VERSION));
		sb.append("&");
		sb.append(requestParameters
				.getAsQueryString(OAuth.OAUTH_SIGNATURE_METHOD));
		sb.append("&");
		sb.append(requestParameters.getAsQueryString(OAuth.OAUTH_TIMESTAMP));
		sb.append("&");
		sb.append(requestParameters.getAsQueryString(OAuth.OAUTH_NONCE));

		final String signedUrl = sb.toString();

		request.setRequestUrl(signedUrl);

		return signedUrl;
	}

}
