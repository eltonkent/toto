/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.animation;

import android.view.animation.Interpolator;

public class BounceInterpolator implements Interpolator {

	@Override
	public float getInterpolation(final float t) {
		return 1.0F - doInterpolation(t);
	}

	private float doInterpolation(float f) {
		if (f < 0.36363636363636365D)
			return 7.5625F * (f = (float) (f - 0.18181818181818182D)) * f * 4F;
		if (f < 0.72727272727272729D)
			return 7.5625F * (f = (float) (f - 0.54545454545454541D)) * f
					+ 0.75F;
		if (f < 0.90909090909090917D)
			return 7.5625F * (f = (float) (f - 0.81818181818181823D)) * f
					+ 0.9375F;
		else
			return 7.5625F * (f = (float) (f - 0.95454545454545459D)) * f
					+ 0.984375F;
	}
}
