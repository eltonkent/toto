package toto.ui.animation;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Animate along a path
 * 
 * @author Elton Kent
 * 
 */
class PathAnimation extends Animation {
	private final PathMeasure measure;
	private final float[] pos = new float[2];

	PathAnimation(final Path path) {
		measure = new PathMeasure(path, false);
	}

	@Override
	protected void applyTransformation(final float interpolatedTime,
			final Transformation t) {
		measure.getPosTan(measure.getLength() * interpolatedTime, pos, null);
		t.getMatrix().setTranslate(pos[0], pos[1]);
	}
}