/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.animation;

import android.view.animation.TranslateAnimation;

class FloorBounceAnimation extends TranslateAnimation {

	FloorBounceAnimation(final float height) {
		this(height, 0);
	}

	FloorBounceAnimation(final float height, final int dimensionType) {
		super(0, 0.0F, 0, 0.0F, 0, 0.0F, dimensionType, -height);
		setInterpolator(new BounceInterpolator());
	}
}
