/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.animation;

import android.graphics.Path;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import toto.device.sensor.OrientationUtils;
import toto.device.sensor.Side;

public class AnimationUtils {
	public static Animation createHeightChangeAnimation(final int oldHeight,
			final int newHeight, final int duration,
			final AnimationListener listener) {
		final float targetScale = (float) newHeight / (float) oldHeight;
		final ScaleAnimation anim = new ScaleAnimation(1, 1, 1.0f, targetScale);
		anim.setDuration(duration);
		anim.setAnimationListener(listener);
		return anim;
	}

	public static Animation fadeInAnimation(final int duration,
			final AnimationListener listener) {
		return createFadeAnimation(false, duration, listener);
	}

	public static Animation fadeOutAnimation(final int duration,
			final AnimationListener listener) {
		return createFadeAnimation(true, duration, listener);
	}

	private static Animation createFadeAnimation(final boolean out,
			final int duration, final AnimationListener listener) {
		final AlphaAnimation anim = new AlphaAnimation((out ? 1.0f : 0.0f),
				(out ? 0.0f : 1.0f));
		anim.setDuration(duration);
		anim.setAnimationListener(listener);
		return anim;
	}

	public static Animation slideAnimation(final boolean relToParent,
			final boolean horizontal, final boolean fromLeftOrTop,
			final boolean exiting, final int duration) {
		final int rel = (relToParent ? Animation.RELATIVE_TO_PARENT
				: Animation.RELATIVE_TO_SELF);
		final float movingFrom = (exiting ? 0f : (fromLeftOrTop ? -1f : 1f));
		final float movingTo = (exiting ? (fromLeftOrTop ? 1f : -1f) : 0f);
		TranslateAnimation anim;
		if (horizontal) {
			anim = new TranslateAnimation(rel, movingFrom, rel, movingTo, rel,
					0, rel, 0);
		} else {
			anim = new TranslateAnimation(rel, 0, rel, 0, rel, movingFrom, rel,
					movingTo);
		}
		anim.setDuration(duration);
		return anim;
	}

	private static Animation moveAnimation(final float fromX, final float toX,
			final float fromY, final float toY) {
		final long moveDuration = 200, alphaDuration = 100;

		final AnimationSet main = new AnimationSet(false);

		Animation animation = new AlphaAnimation(0.0f, 1.0f);
		animation.setDuration(alphaDuration);
		main.addAnimation(animation);

		animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, fromX,
				Animation.RELATIVE_TO_SELF, toX, Animation.RELATIVE_TO_SELF,
				fromY, Animation.RELATIVE_TO_SELF, toY);
		animation.setDuration(moveDuration);
		main.addAnimation(animation);

		return main;
	}

	/**
	 * @return an animation controller
	 */
	public static Animation goDownAnimation() {
		return moveAnimation(0, 0, -1, 0);
	}

	/**
	 * @return an animation controller
	 */
	public static Animation goUpAnimation() {
		final Animation a = moveAnimation(0, 0, 0, -1);
		a.setFillBefore(true);
		return a;
	}

	public static Animation slideInDownAnimation() {
		final Animation slideInDownAnimation = new TranslateAnimation(0, 0,
				-50, 0);
		slideInDownAnimation.setDuration(400);
		return slideInDownAnimation;
	}

	public static Animation slideOutUpAnimation() {
		final Animation slideOutUpAnimation = new TranslateAnimation(0, 0, 0,
				-50);
		slideOutUpAnimation.setDuration(400);
		return slideOutUpAnimation;
	}

	/**
	 * Get a rotation animation based on the previous and current orientation
	 * 
	 * @param oldside
	 * @param newside
	 * @see toto.device.sensor.OrientationUtils
	 * 
	 * @return
	 */
	public static Animation rotationAnimation(
			final Side oldside,
			final Side newside) {
		RotateAnimation retval = null;
		if (oldside == null || newside == null) {
			retval = new RotateAnimation(0, 0, Animation.RELATIVE_TO_SELF,
					0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		} else {
			final int olddegree = OrientationUtils.toDegrees(oldside);
			int newdegree = OrientationUtils.toDegrees(newside);
			if (olddegree == 0 && newdegree == 270) {
				newdegree = -90;
			} else if (olddegree == 270 && newdegree == 0) {
				newdegree = 360;
			}
			retval = new RotateAnimation(olddegree, newdegree,
					Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
			retval.setDuration(500);
			retval.setFillEnabled(true);
			retval.setFillAfter(true);
			retval.setInterpolator(new DecelerateInterpolator());
		}

		return retval;
	}

	/**
	 * Bounce animation from ceiling to floor.
	 * 
	 * @param height
	 *            from floor to ceiling
	 * @return
	 */
	public static Animation floorBounceAnimation(final float height) {
		return new FloorBounceAnimation(height);
	}

	public static Animation flipAnimation(final View fromView,
			final View toView, final int centerX, final int centerY) {
		return new FlipAnimation(fromView, toView, centerX, centerY);
	}

	/**
	 * Animate along a path
	 * 
	 * @param path
	 * @return
	 */
	public static Animation pathAnimation(final Path path) {
		return new PathAnimation(path);
	}

	/**
	 * Creates a new 3D rotation on the Y axis. The rotation is defined by its
	 * start angle and its end angle. Both angles are in degrees. The rotation
	 * is performed around a center point on the 2D space, definied by a pair of
	 * X and Y coordinates, called centerX and centerY. When the animation
	 * starts, a translation on the Z axis (depth) is performed. The length of
	 * the translation can be specified, as well as whether the translation
	 * should be reversed in time.
	 * 
	 * @param fromDegrees
	 *            the start angle of the 3D rotation
	 * @param toDegrees
	 *            the end angle of the 3D rotation
	 * @param centerX
	 *            the X center of the 3D rotation
	 * @param centerY
	 *            the Y center of the 3D rotation
	 * @param reverse
	 *            true if the translation should be reversed, false otherwise
	 */
	public static Animation rotate3dAnimation(final float fromDegrees,
			final float toDegrees, final float centerX, final float centerY,
			final float depthZ, final boolean reverse) {
		return new Rotate3dAnimation(fromDegrees, toDegrees, centerX, centerY,
				depthZ, reverse);
	}
}
