package test.toto.device;

import java.io.IOException;

import test.toto.ToToTestCase;
import toto.device.DeviceUtils;
import android.util.Log;

public class DeviceUtilsTest extends ToToTestCase {

	public void testCPU() {
		float cs = DeviceUtils.getCPUClockSpeed();
		assertTrue(cs > 0);
	}

	public void testRAM() {
		int cs;
		try {
			cs = DeviceUtils.getRAMSize();
			assertTrue(cs > 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void testCPUName() {
		String hardwareName = DeviceUtils.getCPUHardwareName();
		assertNotNull(hardwareName);
		Log.d("RAGE", " testInternalStorage: " + hardwareName);
	}
}
