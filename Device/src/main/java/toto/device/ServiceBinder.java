/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device;

import java.lang.reflect.Method;

import com.mi.toto.ToTo;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.util.Log;

/**
 * Used to bind a service using code.
 * 
 * @author Mobifluence Interactive
 * 
 * @param <T>
 */
public class ServiceBinder<T extends IInterface> {

	private final Context mContext;
	private final Class<? extends IInterface> mInterfaceClass;
	private final Runnable mOnServiceReady;
	private final Class<? extends Service> mServiceClass;
	private final ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(final ComponentName name,
				final IBinder service) {
			try {
				Method asInterface = null;
				for (final Class c : mInterfaceClass.getClasses()) {
					final String className = c.getSimpleName();
					if (className.equals("Stub")) {
						asInterface = c.getMethod("asInterface", IBinder.class);
						break;
					}
				}

				mServiceInterface = (T) asInterface.invoke(null, service);
				if (mOnServiceReady != null) {
					mOnServiceReady.run();
				}

			} catch (final Exception e) {
				Log.e(ServiceBinder.class.getName(),
						"Unable to bind to service", e);
				ToTo.logException(e);
			}

		}

		@Override
		public void onServiceDisconnected(final ComponentName name) {
			// TODO Auto-generated method stub

		}

	};

	private T mServiceInterface;

	public ServiceBinder(final Context context,
			final Class<? extends Service> serviceClass,
			final Class<? extends IInterface> interfaceClass,
			final Runnable onServiceReady) {
		mContext = context;
		mServiceClass = serviceClass;
		mInterfaceClass = interfaceClass;
		mOnServiceReady = onServiceReady;
		final Intent serviceIntent = new Intent(context, mServiceClass);
		context.bindService(serviceIntent, mServiceConnection,
				Context.BIND_AUTO_CREATE);
	}

	public T getServiceInterface() {
		return mServiceInterface;
	}

	public void unBind() {
		mContext.unbindService(mServiceConnection);
	}
}
