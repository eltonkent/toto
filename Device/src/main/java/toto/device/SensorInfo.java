/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device;

/**
 * Contains sensor related information
 * <p>
 * {@link DeviceUtils#getSensorInfo(android.content.Context)}
 * </p>
 */
public class SensorInfo {
	private boolean hasAccelerometer;
	private boolean hasGyroscope;
	private boolean hasLightSensor;
	private boolean hasMagneticSensor;
	private boolean hasOrientationSensor;
	private boolean hasPressureSensor;
	private boolean hasProximitySensor;
	private boolean hasTemperatureSensor;

	SensorInfo() {
	}

	public boolean hasAccelerometer() {
		return hasAccelerometer;
	}

	public boolean hasGyroscope() {
		return hasGyroscope;
	}

	public boolean hasLightSensor() {
		return hasLightSensor;
	}

	public boolean hasMagneticSensor() {
		return hasMagneticSensor;
	}

	public boolean hasOrientationSensor() {
		return hasOrientationSensor;
	}

	public boolean hasPressureSensor() {
		return hasPressureSensor;
	}

	public boolean hasProximitySensor() {
		return hasProximitySensor;
	}

	public boolean hasTemperatureSensor() {
		return hasTemperatureSensor;
	}

	void setHasAccelerometer(final boolean hasAccelerometer) {
		this.hasAccelerometer = hasAccelerometer;
	}

	void setHasGyroscope(final boolean hasGyroscope) {
		this.hasGyroscope = hasGyroscope;
	}

	void setHasLightSensor(final boolean hasLightSensor) {
		this.hasLightSensor = hasLightSensor;
	}

	void setHasMagneticSensor(final boolean hasMagneticSensor) {
		this.hasMagneticSensor = hasMagneticSensor;
	}

	void setHasOrientationSensor(final boolean hasOrientationSensor) {
		this.hasOrientationSensor = hasOrientationSensor;
	}

	void setHasPressureSensor(final boolean hasPressureSensor) {
		this.hasPressureSensor = hasPressureSensor;
	}

	void setHasProximitySensor(final boolean hasProximitySensor) {
		this.hasProximitySensor = hasProximitySensor;
	}

	void setHasTemperatureSensor(final boolean hasTemperatureSensor) {
		this.hasTemperatureSensor = hasTemperatureSensor;
	}

}
