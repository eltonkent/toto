/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device;

/**
 * Contains network related information.
 * <p>
 * 
 * {@link DeviceUtils#getNetworkInfo(android.content.Context)}
 * </p>
 */
public final class NetworkInfo {

	private int dataState;

	private boolean hasTelephoneSupport;
	private boolean isRoaming;
	private boolean hasDataConnection;

	public boolean isHasDataConnection() {
		return hasDataConnection;
	}

	void setHasDataConnection(final boolean hasDataConnection) {
		this.hasDataConnection = hasDataConnection;
	}

	private int networkType;
	private String operatorName;
	private int phoneType;
	private String voicemailNumber;

	NetworkInfo() {
	}

	/**
	 * 
	 * 
	 * @return TelephonyManager.DATA_XXX
	 */
	public int getDataState() {
		return dataState;
	}

	/**
	 * Get the network type currently in use
	 * 
	 * @return TelephonyManager.NETWORKTYPE_XXX
	 */
	public int getNetworkType() {
		return networkType;
	}

	/**
	 * Returns the operator name
	 * 
	 * @return
	 */
	public String getOperatorName() {
		return operatorName;
	}

	public int getPhoneType() {
		return phoneType;
	}

	/**
	 * Returns the voice mail number
	 * 
	 * @return
	 */
	public String getVoicemailNumber() {
		return voicemailNumber;
	}

	/**
	 * Returns true if the device has telephony support
	 * 
	 * @return
	 */
	public boolean hasTelephoneSupport() {
		return hasTelephoneSupport;
	}

	/**
	 * 
	 * 
	 * @return True if the network is in roaming state
	 */
	public boolean isRoaming() {
		return isRoaming;
	}

	void setDataState(final int dataState) {
		this.dataState = dataState;
	}

	void setHasTelephoneSupport(final boolean hasTelephoneSupport) {
		this.hasTelephoneSupport = hasTelephoneSupport;
	}

	void setNetworkType(final int networkType) {
		this.networkType = networkType;
	}

	void setOperatorName(final String operatorName) {
		this.operatorName = operatorName;
	}

	/**
	 * Get the phone type
	 * 
	 * 
	 * @return TelephonyManager.PHONETYPE_XXX , 0 if phone type cannot be
	 *         established.
	 */
	void setPhoneType(final int phoneType) {
		this.phoneType = phoneType;
	}

	void setRoaming(final boolean isRoaming) {
		this.isRoaming = isRoaming;
	}

	void setVoicemailNumber(final String voicemailNumber) {
		this.voicemailNumber = voicemailNumber;
	}

}
