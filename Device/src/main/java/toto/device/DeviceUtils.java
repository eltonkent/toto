/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mi.toto.ToTo;
import toto.io.IOUtils;
import toto.net.CarrierHelper;
import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

/**
 * Utility to obtain all device related information like rooted check and many
 * more.
 * <p>
 * </p>
 * 
 */
public class DeviceUtils {
	private static final String DF_COMMAND = "df";

	private static final CharSequence FLASH_FILE_SYSTEM = "yaffs";
	private static final String KERNEL_FORMAT_REGEXP = "\\w+\\s+" + /*
																	 * ignore:
																	 * Linux
																	 */
	"\\w+\\s+" + /* ignore: version */
	"([^\\s]+)\\s+" + /* group 1: 2.6.22-omap1 */
	"\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+" + /*
												 * group 2:
												 * (xxxxxx@xxxxx.constant)
												 */
	"\\((?:[^(]*\\([^)]*\\))?[^)]*\\)\\s+" + /* ignore: (gcc ..) */
	"([^\\s]+)\\s+" + /* group 3: #26 */
	"(?:PREEMPT\\s+)?" + /* ignore: PREEMPT (optional) */
	"(.+)"; /* group 4: date */
	private static final String KERNEL_SOURCE = "/proc/version";
	private static final String MEM_INFO_FILE = "/proc/meminfo";
	private static final String MEMORY_METRIC = "K";
	private static final String MOUNT_COMMAND = "mount";
	public static final String TAG = "DeviceUtils";

	private static final CharSequence UBI_FILE_SYSTEM = "ubifs";

	private static String formatKernelVersion(final String raw) {
		if (raw != null && raw.length() > 0) {
			try {
				final Pattern p = Pattern.compile(KERNEL_FORMAT_REGEXP);
				final Matcher m = p.matcher(raw);
				if (!m.matches()) {
					Log.d(TAG, "Regex did not match on /proc/version: " + raw);
					return raw;
				} else if (m.groupCount() < 4) {
					Log.d(TAG, "Regex returned only " + m.groupCount()
							+ "groups");
					return raw;
				} else {
					return (new StringBuilder(m.group(1)).append("\n")
							.append(m.group(2)).append(" ").append(m.group(3))
							.append("\n").append(m.group(4))).toString();
				}
			} catch (final Throwable t) {
				Log.e(TAG,
						"Error formatting raw kernel version: "
								+ t.getMessage(), t);
			}
		}
		return raw;
	}

	public static Display[] getAllDisplays(final Context context) {
		final WindowManager winMgr = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		final Display[] displays = new Display[1];
		displays[0] = winMgr.getDefaultDisplay();
		return displays;
	}

	/**
	 * Get the clock speed(clock rate) of the CPU in Hertz
	 * 
	 * @return
	 */
	public static float getCPUClockSpeed() {
		float cpuclock = 0;
		try {
			final StringBuffer s = new StringBuffer();
			final Process p = Runtime.getRuntime().exec("cat /proc/cpuinfo");
			final BufferedReader input = new BufferedReader(
					new InputStreamReader(p.getInputStream()));
			String line = "";
			while ((line = input.readLine()) != null
					&& s.toString().length() == 0) {
				if (line.startsWith("BogoMIPS")) {
					s.append(line + "\n");
				}
			}
			final String cpuclockstr = s.substring(s.indexOf(":") + 2,
					s.length());
			cpuclock = Float.parseFloat(cpuclockstr);
		} catch (final Exception err) {
			// if ANYTHING goes wrong, just report 0 since this is only used for
			// performance appraisal.
			ToTo.logException(err);
		}
		return cpuclock;
	}

	public static DeviceInfo getDeviceInfo(final Context context) {
		final DeviceInfo info = new DeviceInfo();
		info.setFirmwareVersion(Build.VERSION.RELEASE);
		final String kernelRawVersion = readKernelVersionRaw();
		if (kernelRawVersion != null) {
			info.setKernelVersion(formatKernelVersion(kernelRawVersion));
		}
		info.setManufacturer(Build.MANUFACTURER);
		info.setDeviceModel(Build.MODEL);
		info.setDeviceBrand(Build.BRAND);
		return info;
	}

	/**
	 * Get the DisplayInfo for the given display
	 * 
	 * @see DisplayInfo
	 * @param display
	 * @param context
	 * @return
	 */
	public static DisplayInfo getDisplayInfo(final Display display,
			final Context context) {
		final DisplayMetrics outMetrics = new DisplayMetrics();
		display.getMetrics(outMetrics);
		final DisplayInfo info = new DisplayInfo();
		final double diagonalSizeInInch = Math.sqrt(Math.pow(
				outMetrics.widthPixels / outMetrics.xdpi, 2)
				+ Math.pow(outMetrics.heightPixels / outMetrics.ydpi, 2));
		info.setDiagonalSizeInInch(diagonalSizeInInch);
		info.setWidthInPixels(outMetrics.widthPixels);
		info.setHeightInPixels(outMetrics.heightPixels);
		info.setLogicalDensity(outMetrics.density);
		info.setLogicalDPI(outMetrics.densityDpi);
		info.setScaledDensity(outMetrics.scaledDensity);
		info.setHorizontalDensity(outMetrics.xdpi);
		info.setVerticalDensity(outMetrics.ydpi);
		final Configuration c = context.getResources().getConfiguration();
		final int touchMethod = c.touchscreen;
		if (touchMethod == Configuration.TOUCHSCREEN_UNDEFINED
				|| touchMethod == Configuration.TOUCHSCREEN_NOTOUCH)
			info.setTouchEnabled(false);
		else
			info.setTouchEnabled(true);
		info.setRefreshRate(display.getRefreshRate());

		final float aspectRatio = outMetrics.widthPixels
				/ outMetrics.heightPixels;
		if (aspectRatio == 0.60037524f) {
			info.setScreenType(DisplayInfo.SCREENTYPE_WIDE);
		}
		if (aspectRatio < 0.60037524f) {
			info.setScreenType(DisplayInfo.SCREENTYPE_NORMAL);
		}
		if (aspectRatio > 2) {
			info.setScreenType(DisplayInfo.SCREENTYPE_FULL_WIDTH);
		}

		return info;
	}

	/**
	 * Get the device's CPU hardware name.
	 * 
	 * @return
	 */
	public static String getCPUHardwareName() {
		String hardwarenamestr = "{}";
		try {
			final StringBuffer s = new StringBuffer();
			final Process p = Runtime.getRuntime().exec("cat /proc/cpuinfo");
			final BufferedReader input = new BufferedReader(
					new InputStreamReader(p.getInputStream()));
			String line = "";
			while ((line = input.readLine()) != null
					&& s.toString().length() == 0) {
				if (line.startsWith("Hardware")) {
					s.append(line + "\n");
				}
			}
			hardwarenamestr = s.substring(s.indexOf(":") + 2, s.length());
		} catch (final Exception err) {
			// if ANYTHING goes wrong, just report 0 since this is only used for
			// performance appraisal.
			ToTo.logException(err);
		}
		return hardwarenamestr;
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public static int getInternalStorageSize() {
		String cmd = MOUNT_COMMAND;
		final Runtime run = Runtime.getRuntime();
		Process pr = null;
		/* Parsing Mount Points */
		try {
			pr = run.exec(cmd);
		} catch (final IOException e) {
			return 0;
		}
		try {
			if (pr != null) {
				pr.waitFor();
			}
		} catch (final InterruptedException e) {
			return 0;
		}
		BufferedReader buf = null;

		String line = null;
		final ArrayList<String> mountPts = new ArrayList<String>(3);
		try {
			buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			while ((line = buf.readLine()) != null) {
				if (line.contains(FLASH_FILE_SYSTEM)
						|| line.contains(UBI_FILE_SYSTEM)) {
					final StringTokenizer token = new StringTokenizer(line, " ");
					while (token.hasMoreElements()) {
						final String mntPoint = token.nextToken();
						if (mntPoint != null && mntPoint.length() > 0
								&& !mntPoint.startsWith("/dev/block")
								&& mntPoint.startsWith("/")) {
							mountPts.add(mntPoint);
							break;
						}
					}
				}
			}
		} catch (final IOException e) {
			Log.e(TAG, "Could not parse mount points!", e);
		} finally {
			if (buf != null)
				try {
					buf.close();
				} catch (final IOException e) {
				}
		}

		/* Parsing Mount Point sizes */
		cmd = DF_COMMAND;
		try {
			pr = run.exec(cmd);
		} catch (final IOException e) {
			Log.e(TAG, "Could not execute command : " + cmd, e);
			return 0;
		}
		try {
			if (pr != null) {
				pr.waitFor();
			}
		} catch (final InterruptedException e) {
			Log.e(TAG, "Could not complete command : " + cmd, e);
			ToTo.logException(e);
			return 0;
		}
		final ArrayList<String> mountPtsSizes = new ArrayList<String>(
				mountPts.size());
		try {
			buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			while ((line = buf.readLine()) != null) {
				for (final String mntPnt : mountPts) {
					boolean found = false;
					if (line.contains(mntPnt)) {
						found = true;
						mountPts.remove(mntPnt);
						final StringTokenizer token = new StringTokenizer(line,
								" ");
						token.nextToken();

						while (token.hasMoreElements()) {
							String size = token.nextToken();

							if (size != null && size.length() > 0) {
								if (size.endsWith("K")) {
									size = size.substring(0, size.length() - 1);
								}
								mountPtsSizes.add(size);
								break;
							}
						}
						if (found)
							break;
					}
				}
				if (mountPts.size() == 0) {
					break;
				}
			}
		} catch (final IOException e) {
			ToTo.logException(e);
			Log.e(TAG, "Could not parse mount point sizes!", e);
		} finally {
			if (buf != null)
				try {
					buf.close();
				} catch (final IOException e) {
				}
		}

		/* Summing mount point sizes */
		if (mountPtsSizes != null && mountPtsSizes.size() > 0) {
			Integer fullSize = new Integer(0);
			for (final String size : mountPtsSizes) {
				fullSize += Integer.parseInt(size);
			}
			Log.d(TAG, "Fullsize: " + fullSize + MEMORY_METRIC);
			return fullSize;
		}
		return 0;

	}

	/**
	 * Get the logcat input stream
	 * 
	 * @return
	 * @throws IOException
	 */
	public static InputStream getLogcatLogs() throws IOException {
		final ProcessBuilder builder = new ProcessBuilder("logcat", "-d");
		builder.redirectErrorStream(true);
		final Process process = builder.start();
		// process.waitFor();
		return process.getInputStream();
	}

	/**
	 * 
	 * Get network related information. ACCESS_NETWORK_STATE permission needs to
	 * be setKey before using this method.
	 * 
	 * @param context
	 * @return
	 * @see NetworkInfo
	 */
	public static NetworkInfo getNetworkInfo(final Context context) {
		final TelephonyManager teleManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		if (teleManager != null) {
			final NetworkInfo info = new NetworkInfo();
			info.setDataState(teleManager.getDataState());
			info.setNetworkType(teleManager.getNetworkType());
			info.setOperatorName(teleManager.getNetworkOperatorName());
			info.setRoaming(teleManager.isNetworkRoaming());
			info.setVoicemailNumber(teleManager.getVoiceMailNumber());
			info.setPhoneType(teleManager.getPhoneType());
			info.setHasTelephoneSupport(teleManager.getDeviceId() != null);
			info.setHasDataConnection(CarrierHelper.getInstance(context)
					.getCurrentCarrier() != null);
		}
		return null;

	}

	/**
	 * Usses the <code>meminfo</code> command to get the exact available RAM in
	 * <code>KB</code>
	 * 
	 * @return Available RAM size. 0 if the command failed.
	 * @throws IOException
	 */
	public static int getRAMSize() throws IOException {
		String ram = null;
		BufferedReader in = null;
		in = new BufferedReader(new FileReader(MEM_INFO_FILE));
		String str;
		while ((str = in.readLine()) != null) {
			if (str.startsWith("MemTotal:")) {
				ram = str;
				break;
			}
		}
		in.close();
		if (ram != null && ram.length() > 0) {
			final StringTokenizer token = new StringTokenizer(ram, ":");
			while (token.hasMoreElements()) {
				ram = (String) token.nextElement();
			}
			ram = ram.trim();
		}
		Integer ramValue = null;
		try {
			ramValue = Integer.parseInt(ram.substring(0, ram.length() - 3));
		} catch (final Exception e) {
			return 0;
		}

		if (ramValue > 0) {
			return ramValue;
		}
		return 0;
	}

	public static SensorInfo getSensorInfo(final Context context) {
		final SensorManager sensorMgr = (SensorManager) context
				.getSystemService(Context.SENSOR_SERVICE);
		final SensorInfo info = new SensorInfo();
		info.setHasAccelerometer(!sensorMgr.getSensorList(
				Sensor.TYPE_ACCELEROMETER).isEmpty());
		info.setHasGyroscope(!sensorMgr.getSensorList(Sensor.TYPE_GYROSCOPE)
				.isEmpty());
		info.setHasLightSensor(!sensorMgr.getSensorList(Sensor.TYPE_LIGHT)
				.isEmpty());
		info.setHasMagneticSensor(!sensorMgr.getSensorList(
				Sensor.TYPE_MAGNETIC_FIELD).isEmpty());
		info.setHasOrientationSensor(!sensorMgr.getSensorList(
				Sensor.TYPE_ORIENTATION).isEmpty());
		info.setHasPressureSensor(!sensorMgr
				.getSensorList(Sensor.TYPE_PRESSURE).isEmpty());
		info.setHasProximitySensor(!sensorMgr.getSensorList(
				Sensor.TYPE_PROXIMITY).isEmpty());
		info.setHasTemperatureSensor(!sensorMgr.getSensorList(
				Sensor.TYPE_TEMPERATURE).isEmpty());
		return info;
	}

	private static String readKernelVersionRaw() {
		String kernelVersionRaw = null;
		try {
			final BufferedReader bReader = new BufferedReader(new FileReader(
					KERNEL_SOURCE), 256);
			try {
				kernelVersionRaw = bReader.readLine();
			} finally {
				bReader.close();
			}
		} catch (final Throwable t) {
			ToTo.logException(t);
		}
		return kernelVersionRaw;
	}

	private DeviceUtils() {

	}

	/**
	 * Get a system property by executing the <code>getprop</code> command.
	 * <p>
	 * This is a potentially blocking call and should be executed on its own
	 * thread.
	 * </p>
	 * 
	 * @param paramString
	 * @return
	 */
	public static String getSystemProperty(final String paramString) {
		Process localProcess = null;
		InputStream localInputStream = null;
		Scanner localScanner = null;
		try {
			localProcess = Runtime.getRuntime().exec(
					"/system/bin/getprop " + paramString);
			localInputStream = localProcess.getInputStream();
			localScanner = new Scanner(localInputStream).useDelimiter("\\n");
			final boolean bool = localScanner.hasNext();
			String localObject2 = null;
			if (bool) {
				final String str = localScanner.next();
				localObject2 = str;
			}
			IOUtils.closeQuietly(localInputStream);
			if (localScanner != null)
				localScanner.close();
			if (localProcess != null)
				localProcess.destroy();
			Log.d("RAGE[DeviceUtils]", "getSystemProperty: " + paramString
					+ "=" + localObject2);
			return localObject2;
		} catch (final Exception localException) {
			while (true) {
				Log.w("RAGE[DeviceUtils]", localException.getMessage(),
						localException);
				IOUtils.closeQuietly(localInputStream);
				if (localScanner != null)
					localScanner.close();
				Object localObject2 = null;
				if (localProcess != null) {
					localProcess.destroy();
					localObject2 = null;
				}
			}
		} finally {
			IOUtils.closeQuietly(localInputStream);
			if (localScanner != null)
				localScanner.close();
			if (localProcess != null)
				localProcess.destroy();
		}
	}

	/**
	 * Get the number of physical CPU's
	 * 
	 * @return
	 */
	public static int getNoOfCPUs() {
		return Runtime.getRuntime().availableProcessors();
	}
}
