/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device;

public final class DeviceInfo {
	private String deviceBrand;

	private String deviceModel;
	private String firmwareVersion;
	private String kernelVersion;
	private String manufacturer;

	DeviceInfo() {
	}

	public String getDeviceBrand() {
		return deviceBrand;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	public String getKernelVersion() {
		return kernelVersion;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	void setDeviceBrand(final String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}

	void setDeviceModel(final String deviceModel) {
		this.deviceModel = deviceModel;
	}

	void setFirmwareVersion(final String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	void setKernelVersion(final String kernelVersion) {
		this.kernelVersion = kernelVersion;
	}

	void setManufacturer(final String manufacturer) {
		this.manufacturer = manufacturer;
	}

}
