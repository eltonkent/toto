/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device;

/**
 * Contains information for a given display
 * <p>
 * {@link DeviceUtils#getDisplayInfo(android.view.Display, android.content.Context)}
 * </p>
 */
public final class DisplayInfo {

	public static final int SCREENTYPE_FULL_WIDTH = 30;
	public static final int SCREENTYPE_NORMAL = 10;
	public static final int SCREENTYPE_WIDE = 20;
	private double diagonalSizeInInch;
	private int heightInPixels;
	private float horizontalDensity;
	private boolean isTouchEnabled;
	private float logicalDensity;
	private float logicalDPI;
	private float refreshRate;

	private float scaledDensity;

	private int screenType;

	private float verticalDensity;

	private int widthInPixels;

	DisplayInfo() {
	}

	public double getDiagonalSizeInInch() {
		return diagonalSizeInInch;
	}

	public int getHeightInPixels() {
		return heightInPixels;
	}

	public float getHorizontalDensity() {
		return horizontalDensity;
	}

	public float getLogicalDensity() {
		return logicalDensity;
	}

	public float getLogicalDPI() {
		return logicalDPI;
	}

	public float getRefreshRate() {
		return refreshRate;
	}

	public float getScaledDensity() {
		return scaledDensity;
	}

	/**
	 * 
	 * 
	 * @return {@link DisplayInfo#SCREENTYPE_XXX}
	 */
	public int getScreenType() {
		return screenType;
	}

	public float getVerticalDensity() {
		return verticalDensity;
	}

	public int getWidthInPixels() {
		return widthInPixels;
	}

	public boolean isTouchEnabled() {
		return isTouchEnabled;
	}

	void setDiagonalSizeInInch(final double diagonalSizeInInch) {
		this.diagonalSizeInInch = diagonalSizeInInch;
	}

	void setHeightInPixels(final int heightInPixels) {
		this.heightInPixels = heightInPixels;
	}

	void setHorizontalDensity(final float horizontalDensity) {
		this.horizontalDensity = horizontalDensity;
	}

	void setLogicalDensity(final float logicalDensity) {
		this.logicalDensity = logicalDensity;
	}

	void setLogicalDPI(final float logicalDPI) {
		this.logicalDPI = logicalDPI;
	}

	void setRefreshRate(final float refreshRate) {
		this.refreshRate = refreshRate;
	}

	void setScaledDensity(final float scaledDensity) {
		this.scaledDensity = scaledDensity;
	}

	void setScreenType(final int screenType) {
		this.screenType = screenType;
	}

	void setTouchEnabled(final boolean isTouchEnabled) {
		this.isTouchEnabled = isTouchEnabled;
	}

	void setVerticalDensity(final float verticalDensity) {
		this.verticalDensity = verticalDensity;
	}

	void setWidthInPixels(final int widthInPixels) {
		this.widthInPixels = widthInPixels;
	}
}
