/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.security.crypto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import com.mi.toto.ToTo;
import toto.security.HexUtils;

public class CryptoUtils {

	public static String encryptAEStoHEX(final String input, final String key)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		final byte[] desKeyData = key.getBytes();
		final SecretKeySpec secretKey = new SecretKeySpec(desKeyData, "AES");
		final Cipher c = Cipher.getInstance("AES");
		c.init(Cipher.ENCRYPT_MODE, secretKey);
		final byte[] cipherText = c.doFinal(input.getBytes());
		final String retValue = new String(HexUtils.toHexString(cipherText));
		return retValue;
	}

	public static String decryptAESfromHEX(final String input, final String key)
			throws IllegalBlockSizeException, BadPaddingException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException {

		byte[] inputBytes;
		try {
			inputBytes = HexUtils.toByteArray(input);
		} catch (final Exception e) {
			ToTo.logException(e);
			throw new IllegalArgumentException("The input hex is not valid.");
		}

		final byte[] desKeyData = key.getBytes();
		final SecretKeySpec secretKey = new SecretKeySpec(desKeyData, "AES");
		// get cipher object for password-based encryption
		final Cipher c = Cipher.getInstance("AES");
		c.init(Cipher.DECRYPT_MODE, secretKey);
		// Decrypt the ciphertext
		final byte[] cleartext = c.doFinal(inputBytes);
		final String retValue = new String(cleartext);
		return retValue;
	}
}
