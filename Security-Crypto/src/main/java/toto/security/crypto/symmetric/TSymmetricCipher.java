package toto.security.crypto.symmetric;

/**
 * RAGE Symmetric Key Cipher interface.
 * 
 * @see AES
 * @see Fog
 * @see XTEA
 * @see ICE
 */
public interface TSymmetricCipher {
	public void decrypt(byte[] bytes, int off, int len);

	public void encrypt(byte[] bytes, int off, int len);
}
