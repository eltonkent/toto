package toto.security.crypto.io;

import com.mi.toto.Conditions;

import java.io.IOException;
import java.io.InputStream;

import toto.security.crypto.symmetric.TSymmetricCipher;

/**
 * Input stream with decryption while reading Created by ekent4 on 12/10/13.
 */
public class CipherInputStream extends InputStream {

	private final TSymmetricCipher cipher;

	public CipherInputStream(final TSymmetricCipher cipher) {
		Conditions
				.checkArgument(cipher != null, "Provider Rage Cipher is null");
		this.cipher = cipher;
	}

	public int read() throws IOException {
		final byte[] buf = new byte[1];
		return read(buf, 0, buf.length);
	}

	public int read(final byte[] b, final int off, final int len)
			throws IOException {
		final int read = super.read(b, off, len);
		cipher.decrypt(b, off, len);
		return read;
	}

	public int read(final byte[] b) throws IOException {
		return read(b, 0, b.length);
	}

}
