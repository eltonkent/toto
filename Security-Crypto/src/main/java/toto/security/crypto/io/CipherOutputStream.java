package toto.security.crypto.io;

import java.io.IOException;
import java.io.OutputStream;

import toto.lang.PrimitiveUtils;
import toto.security.crypto.symmetric.TSymmetricCipher;

/**
 * Cipher output stream
 */
public class CipherOutputStream extends OutputStream {

	private TSymmetricCipher cipher;

	public CipherOutputStream(TSymmetricCipher cipher) {
		this.cipher = cipher;
	}

	public void write(int i) throws IOException {
		byte[] byt = PrimitiveUtils.toByteArray(i);
		write(byt);
	}

	public void write(byte[] buffer) throws IOException {
		write(buffer, 0, buffer.length);
	}

	public void write(byte[] buffer, int offset, int count)
			throws IOException {
		cipher.encrypt(buffer, offset, count);
		super.write(buffer, offset, count);
	}

}
