package test.toto.security.crypto;

import java.io.IOException;

import test.toto.ToToTestCase;
import toto.io.codec.Base64;
import toto.security.crypto.KDFUtils;

public class NativeKDFUtilsTest extends ToToTestCase {


	public void testscrypt() throws IOException {
		int N = 16384;
		int r = 8;
		int p = 1;

		String hashed = KDFUtils.scryptNative(passwd, N, r, p);
		String[] parts = hashed.split("\\$");

		assertEquals(5, parts.length);
		assertEquals("s0", parts[1]);
		assertEquals(16, Base64.decode(parts[3]).length);
		assertEquals(32, Base64.decode(parts[4]).length);

		int params = Integer.valueOf(parts[2], 16);

		assertEquals(N, (int) Math.pow(2, params >> 16 & 0xffff));
		assertEquals(r, params >> 8 & 0xff);
		assertEquals(p, params >> 0 & 0xff);
	}

	public void testcheck() throws IOException {
		String hashed = KDFUtils.scryptNative(passwd, 16384, 8, 1);

		assertTrue(KDFUtils.scryptCheck(passwd, hashed));
		assertFalse(KDFUtils.scryptCheck("s3cr3t", hashed));
	}

	public void testformat_0_rp_max() throws Exception {
		int N = 2;
		int r = 255;
		int p = 255;

		String hashed = KDFUtils.scryptNative(passwd, N, r, p);
		assertTrue(KDFUtils.scryptCheck(passwd, hashed));

		String[] parts = hashed.split("\\$");
		int params = Integer.valueOf(parts[2], 16);

		assertEquals(N, (int) Math.pow(2, params >>> 16 & 0xffff));
		assertEquals(r, params >> 8 & 0xff);
		assertEquals(p, params >> 0 & 0xff);
	}

	String passwd = "secret";
}
