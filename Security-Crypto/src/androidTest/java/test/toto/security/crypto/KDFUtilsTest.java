package test.toto.security.crypto;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

import test.toto.ToToTestCase;
import toto.io.codec.Base64;
import toto.security.crypto.KDFUtils;

public class KDFUtilsTest extends ToToTestCase {
	public static byte[] decode(String str) {
		byte[] bytes = new byte[str.length() / 2];
		int index = 0;

		for (int i = 0; i < str.length(); i += 2) {
			int high = hexValue(str.charAt(i));
			int low = hexValue(str.charAt(i + 1));
			bytes[index++] = (byte) ((high << 4) + low);
		}

		return bytes;
	}

	public static int hexValue(char c) {
		return c >= 'a' ? c - 87 : c - 48;
	}

	// public void testpbkdf2_hmac_sha1_rfc6070() throws Exception {
	// String alg = "HmacSHA1";
	// byte[] P, S;
	// int c, dkLen;
	// String DK;
	//
	// P = "password".getBytes("UTF-8");
	// S = "salt".getBytes("UTF-8");
	// c = 1;
	// dkLen = 20;
	// DK = "0c60c80f961f0e71f3a9b524af6012062fe037a6";
	//
	// assertEquals(true,Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c,
	// dkLen)));
	//
	// P = "password".getBytes("UTF-8");
	// S = "salt".getBytes("UTF-8");
	// c = 2;
	// dkLen = 20;
	// DK = "ea6c014dc72d6f8ccd1ed92ace1d41f0d8de8957";
	//
	// assertEquals(true,Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c,
	// dkLen)));
	//
	// P = "password".getBytes("UTF-8");
	// S = "salt".getBytes("UTF-8");
	// c = 4096;
	// dkLen = 20;
	// DK = "4b007901b765489abead49d926f721d065a429c1";
	//
	// assertEquals(true,Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c,
	// dkLen)));
	//
	// P = "password".getBytes("UTF-8");
	// S = "salt".getBytes("UTF-8");
	// c = 16777216;
	// dkLen = 20;
	// DK = "eefe3d61cd4da4e4e9945b3d6ba2158c2634e984";
	//
	// assertEquals(true,Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c,
	// dkLen)));
	//
	// P = "passwordPASSWORDpassword".getBytes("UTF-8");
	// S = "saltSALTsaltSALTsaltSALTsaltSALTsalt".getBytes("UTF-8");
	// c = 4096;
	// dkLen = 25;
	// DK = "3d2eec4fe41c849b80c8d83662c0e44a8b291a964cf2f07038";
	//
	// assertEquals(true,Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c,
	// dkLen)));
	//
	// P = "pass\0word".getBytes("UTF-8");
	// S = "sa\0lt".getBytes("UTF-8");
	// c = 4096;
	// dkLen = 16;
	// DK = "56fa6aa75548099dcc37d7f03425e0c3";
	//
	// assertEquals(true,Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c,
	// dkLen)));
	// }

	public void testpbkdf2_hmac_sha1_rfc3962() throws Exception {
		String alg = "HmacSHA1";
		byte[] P, S;
		int c, dkLen;
		String DK;

		P = "password".getBytes("UTF-8");
		S = "ATHENA.MIT.EDUraeburn".getBytes("UTF-8");
		c = 1;

		dkLen = 16;
		DK = "cdedb5281bb2f801565a1122b2563515";
		assertEquals(true,
				Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c, dkLen)));

		dkLen = 32;
		DK = "cdedb5281bb2f801565a1122b25635150ad1f7a04bb9f3a333ecc0e2e1f70837";
		assertEquals(true,
				Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c, dkLen)));

		P = "password".getBytes("UTF-8");
		S = "ATHENA.MIT.EDUraeburn".getBytes("UTF-8");
		c = 2;

		dkLen = 16;
		DK = "01dbee7f4a9e243e988b62c73cda935d";
		assertEquals(true,
				Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c, dkLen)));

		dkLen = 32;
		DK = "01dbee7f4a9e243e988b62c73cda935da05378b93244ec8f48a99e61ad799d86";
		assertEquals(true,
				Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c, dkLen)));

		P = "password".getBytes("UTF-8");
		S = "ATHENA.MIT.EDUraeburn".getBytes("UTF-8");
		c = 1200;

		dkLen = 16;
		DK = "5c08eb61fdf71e4e4ec3cf6ba1f5512b";
		assertEquals(true,
				Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c, dkLen)));

		dkLen = 32;
		DK = "5c08eb61fdf71e4e4ec3cf6ba1f5512ba7e52ddbc5e5142f708a31e2e62b1e13";
		assertEquals(true,
				Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c, dkLen)));

		P = "password".getBytes("UTF-8");
		S = new BigInteger("1234567878563412", 16).toByteArray();
		c = 5;

		dkLen = 16;
		DK = "d1daa78615f287e6a1c8b120d7062a49";
		assertEquals(true,
				Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c, dkLen)));

		dkLen = 32;
		DK = "d1daa78615f287e6a1c8b120d7062a493f98d203e6be49a6adf4fa574b6e64ee";
		assertEquals(true,
				Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c, dkLen)));
	}

	public void testpbkdf2_hmac_sha256_scrypt() throws Exception {
		String alg = "HmacSHA256";
		byte[] P, S;
		int c, dkLen;
		String DK;

		P = "password".getBytes("UTF-8");
		S = "salt".getBytes("UTF-8");
		c = 4096;
		dkLen = 32;
		DK = "c5e478d59288c841aa530db6845c4c8d962893a001ce4e11a4963873aa98134a";

		assertEquals(true,
				Arrays.equals(decode(DK), KDFUtils.pbkdf2(alg, P, S, c, dkLen)));
	}

//	public void testscrypt_paper_appendix_b() throws Exception {
//		byte[] P, S;
//		int N, r, p, dkLen;
//		String DK;
//
//		// empty key & salt test missing because unsupported by JCE
//
//		P = "password".getBytes("UTF-8");
//		S = "NaCl".getBytes("UTF-8");
//		N = 1024;
//		r = 8;
//		p = 16;
//		dkLen = 64;
//		DK = "fdbabe1c9d3472007856e7190d01e9fe7c6ad7cbc8237830e77376634b3731622eaf30d92e22a3886ff109279d9830dac727afb94a83ee6d8360cbdfa2cc0640";
//
//		assertArrayEquals(decode(DK), KDFUtils.scrypt(P, S, N, r, p, dkLen));
//
//		P = "pleaseletmein".getBytes("UTF-8");
//		S = "SodiumChloride".getBytes("UTF-8");
//		N = 16384;
//		r = 8;
//		p = 1;
//		dkLen = 64;
//		DK = "7023bdcb3afd7348461c06cd81fd38ebfda8fbba904f8e3ea9b543f6545da1f2d5432955613f0fcf62d49705242a9af9e61e85dc0d651e40dfcf017b45575887";
//
//		assertArrayEquals(decode(DK), KDFUtils.scrypt(P, S, N, r, p, dkLen));
//
//		P = "pleaseletmein".getBytes("UTF-8");
//		S = "SodiumChloride".getBytes("UTF-8");
//		N = 1048576;
//		r = 8;
//		p = 1;
//		dkLen = 64;
//		DK = "2101cb9b6a511aaeaddbbe09cf70f881ec568d574a2ffd4dabe5ee9820adaa478e56fd8f4ba5d09ffa1c6d927c40f4c337304049e8a952fbcbf45c6fa77a41a4";
//
//		assertArrayEquals(decode(DK), KDFUtils.scrypt(P, S, N, r, p, dkLen));
//	}

	public void testscrypt() throws IOException {
		int N = 16384;
		int r = 8;
		int p = 1;

		String hashed = KDFUtils.scrypt(passwd, N, r, p);
		String[] parts = hashed.split("\\$");

		assertEquals(5, parts.length);
		assertEquals("s0", parts[1]);
		assertEquals(16, Base64.decode(parts[3]).length);
		assertEquals(32, Base64.decode(parts[4]).length);

		int params = Integer.valueOf(parts[2], 16);

		assertEquals(N, (int) Math.pow(2, params >> 16 & 0xffff));
		assertEquals(r, params >> 8 & 0xff);
		assertEquals(p, params >> 0 & 0xff);
	}

	public void testcheck() throws IOException {
		String hashed = KDFUtils.scrypt(passwd, 16384, 8, 1);

		assertTrue(KDFUtils.scryptCheck(passwd, hashed));
		assertFalse(KDFUtils.scryptCheck("s3cr3t", hashed));
	}

	public void testformat_0_rp_max() throws Exception {
		int N = 2;
		int r = 255;
		int p = 255;

		String hashed = KDFUtils.scrypt(passwd, N, r, p);
		assertTrue(KDFUtils.scryptCheck(passwd, hashed));

		String[] parts = hashed.split("\\$");
		int params = Integer.valueOf(parts[2], 16);

		assertEquals(N, (int) Math.pow(2, params >>> 16 & 0xffff));
		assertEquals(r, params >> 8 & 0xff);
		assertEquals(p, params >> 0 & 0xff);
	}

	String passwd = "secret";
}
