package test.toto.device.location.coor;

import java.util.ArrayList;
import java.util.List;

import test.toto.ToToTestCase;
import toto.device.location.coor.LatLongCoordinate;
import android.util.Log;

public class LatLongTest extends ToToTestCase {

	public void testEncode() {
		LatLongCoordinate latLong = new LatLongCoordinate(57.64911004342139,
				10.407439861446619);
		String hash = latLong.getGeoHash();
		assertEquals(hash, "u4pruydqqvj8");
		LatLongCoordinate latlonResult = LatLongCoordinate
				.fromGeoHash("u4pruydqqvj8");
		assertEquals(latLong, latlonResult);
	}

	public void testArea() {
		List<LatLongCoordinate> listArea = new ArrayList<LatLongCoordinate>();
		listArea.add(new LatLongCoordinate(40.710052, -74.014807));
		listArea.add(new LatLongCoordinate(40.803545, -73.967428));
		listArea.add(new LatLongCoordinate(40.78925, -73.938761));
		listArea.add(new LatLongCoordinate(40.713435, -73.980131));
		long area = (long) LatLongCoordinate.getArea(listArea);
		assertEquals(area, 27383661);
	}

	public void testHeading() {
		LatLongCoordinate coor1 = new LatLongCoordinate(40.710052, -74.014807);
		double heading = coor1.getHeading(new LatLongCoordinate(40.803545,
				-73.967428));
		assertEquals(heading, 20.9847518362843);

	}

	public void testLength() {
		List<LatLongCoordinate> listArea = new ArrayList<LatLongCoordinate>();
		listArea.add(new LatLongCoordinate(40.710052, -74.014807));
		listArea.add(new LatLongCoordinate(40.803545, -73.967428));
		double length = LatLongCoordinate.getLength(listArea);
		assertEquals(length, 11135.598068975441);

	}

	public void testContains() {
		LatLongCoordinate coor1 = new LatLongCoordinate(40.720331, -74.041071);
		List<LatLongCoordinate> listArea = new ArrayList<LatLongCoordinate>();
		listArea.add(new LatLongCoordinate(40.710052, -74.014807));
		listArea.add(new LatLongCoordinate(40.803545, -73.967428));
		listArea.add(new LatLongCoordinate(40.78925, -73.938761));
		listArea.add(new LatLongCoordinate(40.713435, -73.980131));
		boolean contains = coor1.containsLocation(listArea, true);
		assertFalse(contains);
		
	}
	
	public void testMidPoint() {
		LatLongCoordinate coor1 = new LatLongCoordinate(40.710052, -74.014807);
		LatLongCoordinate coor2=coor1.getMidPoint(new LatLongCoordinate(40.803545, -73.967428));
		assertNotNull(coor2);
	}
	
	public void testLatDegrees() {
		LatLongCoordinate coor1 = new LatLongCoordinate(40.710052, -74.014807);
		int degrees=coor1.getLatitudeDegrees();
		assertEquals(degrees, 40);
	}
	
	public void testLongDegrees() {
		LatLongCoordinate coor1 = new LatLongCoordinate(40.710052, -74.014807);
		int degrees=coor1.getLongitudeDegrees();
		assertEquals(degrees, -74);
		Log.d("RAGE", " testLongDegrees: " + degrees);
	}
}
