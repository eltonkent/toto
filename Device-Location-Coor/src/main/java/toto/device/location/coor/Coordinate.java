package toto.device.location.coor;

/**
 * Abstract implementation of location coordinate.
 */
abstract class Coordinate {
	private Datum datum;

	Coordinate(final Datum datum) {
		setDatum(datum);
	}

	/**
	 * Convert a co-ordinate in the co-ordinate system to a point represented by
	 * a latitude and longitude and a perpendicular height above (or below) a
	 * reference ellipsoid.
	 * 
	 * @return a LatLng representation of a point in a co-ordinate system.
	 * @since 1.1
	 */
	abstract LatLongCoordinate toLatLong();

	/**
	 * Set the datum.
	 * 
	 * @param datum
	 *            the datum.
	 * @since 1.1
	 */
	void setDatum(final Datum datum) {
		this.datum = datum;
	}

	/**
	 * Get the datum.
	 * 
	 * @return the datum.
	 * @since 1.1
	 */
	Datum getDatum() {
		return datum;
	}
}
