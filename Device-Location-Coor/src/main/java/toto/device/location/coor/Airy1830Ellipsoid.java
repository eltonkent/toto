package toto.device.location.coor;



class Airy1830Ellipsoid extends Ellipsoid {

	private static Airy1830Ellipsoid ref = null;

	private Airy1830Ellipsoid() {
		super(6377563.396, 6356256.909);
	}

	static Airy1830Ellipsoid getInstance() {
		if (ref == null) {
			ref = new Airy1830Ellipsoid();
		}
		return ref;
	}
}