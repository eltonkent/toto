package toto.device.location.coor;

/**
 *
 */
public class CoordinateException extends Exception {
	public CoordinateException(String message) {
		super(message);
	}
}
