package toto.device.location.coor;



class ModifiedAiryEllipsoid extends Ellipsoid {

	/**
	 * Static reference of this ellipsoid.
	 */
	private static ModifiedAiryEllipsoid ref = null;

	/**
	 * Create an object defining the Modified Airy reference ellipsoid.
	 * 
	 * @since 1.1
	 */
	private ModifiedAiryEllipsoid() {
		super(6377340.189, Double.NaN, 0.00667054015);
	}

	/**
	 * Get the static instance of this ellipsoid
	 * 
	 * @return a reference to the static instance of this ellipsoid
	 * @since 1.1
	 */
	static ModifiedAiryEllipsoid getInstance() {
		if (ref == null) {
			ref = new ModifiedAiryEllipsoid();
		}
		return ref;
	}
}