package toto.device.location.coor;

import toto.math.MathUtils;

/**
 * ECEF (earth-centred, earth-fixed) Cartesian co-ordinates
 * <p>
 * <div> Used to define a point in three-dimensional space. ECEF co-ordinates
 * are defined relative to an x-axis (the intersection of the equatorial plane
 * and the plane defined by the prime meridian), a y-axis (at 90&deg; to the
 * x-axis and its intersection with the equator) and a z-axis (intersecting the
 * North Pole). All the axes intersect at the point defined by the centre of
 * mass of the Earth. </div>
 * </p>
 */
public class ECEFCoordinate extends Coordinate {

	/**
	 * x co-ordinate in metres.
	 */
	private double x;

	/**
	 * y co-ordinate in metres.
	 */
	private double y;

	/**
	 * z co-ordinate in metres.
	 */
	private double z;

	/**
	 * Create a new earth-centred, earth-fixed (ECEF) reference from the given
	 * parameters using the WGS84 reference ellipsoid.
	 * 
	 * @param x
	 *            the x co-ordinate.
	 * @param y
	 *            the y co-ordinate.
	 * @param z
	 *            the z co-ordinate.
	 */
	public ECEFCoordinate(final double x, final double y, final double z) {

		this(x, y, z, new WGS84Datum());

	}

	/**
	 * Create a new earth-centred, earth-fixed (ECEF) reference from the given
	 * parameters and the given reference ellipsoid.
	 * 
	 * @param x
	 *            the x co-ordinate.
	 * @param y
	 *            the y co-ordinate.
	 * @param z
	 *            the z co-ordinate.
	 * @param datum
	 *            the datum.
	 * @since 1.1
	 */
	public ECEFCoordinate(final double x, final double y, final double z,
			final Datum datum) {

		super(datum);
		setX(x);
		setY(y);
		setZ(z);

	}

	/**
	 * Create a new earth-centred, earth-fixed reference from the given latitude
	 * and longitude.
	 * 
	 * @param ll
	 *            latitude and longitude.
	 * @since 1.1
	 */
	public ECEFCoordinate(final LatLongCoordinate ll) {

		super(ll.getDatum());

		final Ellipsoid ellipsoid = getDatum().getReferenceEllipsoid();

		final double phi = Math.toRadians(ll.getLatitude());
		final double lambda = Math.toRadians(ll.getLongitude());
		final double h = ll.getHeight();
		final double a = ellipsoid.getSemiMajorAxis();
		final double f = ellipsoid.getFlattening();
		final double eSquared = (2 * f) - (f * f);
		final double nphi = a
				/ Math.sqrt(1 - eSquared * MathUtils.sinSquared(phi));

		setX((nphi + h) * Math.cos(phi) * Math.cos(lambda));
		setY((nphi + h) * Math.cos(phi) * Math.sin(lambda));
		setZ((nphi * (1 - eSquared) + h) * Math.sin(phi));

	}

	/**
	 * Convert this ECEFRef object to a LatLng object.
	 * 
	 * @return the equivalent latitude and longitude.
	 * @since 1.1
	 */
	public LatLongCoordinate toLatLong() {

		final Ellipsoid ellipsoid = getDatum().getReferenceEllipsoid();

		final double a = ellipsoid.getSemiMajorAxis();
		final double b = ellipsoid.getSemiMinorAxis();
		final double e2Squared = ((a * a) - (b * b)) / (b * b);
		final double f = ellipsoid.getFlattening();
		final double eSquared = (2 * f) - (f * f);
		final double p = Math.sqrt((x * x) + (y * y));
		final double theta = Math.atan((z * a) / (p * b));

		final double phi = Math.atan((z + (e2Squared * b * MathUtils
				.sinCubed(theta)))
				/ (p - eSquared * a * MathUtils.cosCubed(theta)));
		final double lambda = Math.atan2(y, x);

		final double nphi = a
				/ Math.sqrt(1 - eSquared * MathUtils.sinSquared(phi));
		final double h = (p / Math.cos(phi)) - nphi;

		return new LatLongCoordinate(Math.toDegrees(phi),
				Math.toDegrees(lambda), h, new WGS84Datum());
	}

	/**
	 * Get the x co-ordinate.
	 * 
	 * @return the x co-ordinate.
	 * @since 1.1
	 */
	public double getX() {
		return x;
	}

	/**
	 * Get the y co-ordinate.
	 * 
	 * @return the y co-ordinate.
	 * @since 1.1
	 */
	public double getY() {
		return y;
	}

	/**
	 * Get the z co-ordinate.
	 * 
	 * @return the z co-ordinate.
	 * @since 1.1
	 */
	public double getZ() {
		return z;
	}

	/**
	 * Set the x co-ordinate.
	 * 
	 * @param x
	 *            the new x co-ordinate.
	 * @since 1.1
	 */
	public void setX(final double x) {
		this.x = x;
	}

	/**
	 * Set the y co-ordinate.
	 * 
	 * @param y
	 *            the y co-ordinate.
	 * @since 1.1
	 */
	public void setY(final double y) {
		this.y = y;
	}

	/**
	 * Set the z co-ordinate.
	 * 
	 * @param z
	 *            the z co-ordinate.
	 * @since 1.1
	 */
	public void setZ(final double z) {
		this.z = z;
	}

	/**
	 * Get a String representation of this ECEF reference.
	 * 
	 * @return a String representation of this ECEF reference.
	 * @since 1.1
	 */
	public String toString() {
		return "(" + x + "," + y + "," + z + ")";
	}

}
