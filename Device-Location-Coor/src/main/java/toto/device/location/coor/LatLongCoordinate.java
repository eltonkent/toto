package toto.device.location.coor;

import static java.lang.Math.PI;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.log;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.tan;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

import java.util.List;

import toto.math.MathUtils;

/**
 * Latitude Longitude coordinate system.
 */
public class LatLongCoordinate {

	/**
	 * Latitude in degrees.
	 */
	private double latitude;

	/**
	 * Longitude in degrees.
	 */
	private double longitude;

	/**
	 * Height.
	 */
	private double height;

	/**
	 * Datum of this reference.
	 */
	private final Datum datum;/* = new WGS84Datum(); */

	/**
	 * Latitude is north of the equator.
	 */
	public static final int NORTH = 1;

	/**
	 * Latitude is south of the equator.
	 */
	public static final int SOUTH = -1;

	/**
	 * Longitude is east of the prime meridian.
	 */
	public static final int EAST = 1;

	/**
	 * Longitude is west of the prime meridian.
	 */
	public static final int WEST = -1;

	/**
	 * The largest possible latitude value.
	 */
	public static final double LATITUDE_MAX = 90;

	/**
	 * The smallest possible latitude value.
	 */
	public static final double LATITUDE_MIN = -90;

	/**
	 * The largest possible longitude value.
	 */
	public static final double LONGITUDE_MAX = 180;

	/**
	 * The smallest possible longitude value.
	 */
	public static final double LONGITUDE_MIN = -180;

	/**
	 * Create a new LatLng object to represent a latitude/longitude pair using
	 * the WGS84 datum.
	 * 
	 * @param latitude
	 *            the latitude in degrees. Must be between -90.0 and 90.0
	 *            inclusive. -90.0 and 90.0 are effectively equivalent.
	 * @param longitude
	 *            the longitude in degrees. Must be between -180.0 and 180.0
	 *            inclusive. -180.0 and 180.0 are effectively equivalent.
	 * @throws IllegalArgumentException
	 *             if either the given latitude or the given longitude are
	 *             invalid.
	 * @since 1.0
	 */
	public LatLongCoordinate(final double latitude, final double longitude) {
		this(latitude, longitude, 0, new WGS84Datum());
	}

	/**
	 * Create a Geohash of the latlong coordinate.
	 * <p>
	 * <div> <a href="http://en.wikipedia.org/wiki/Geohash">Geohash</a> offers
	 * properties like arbitrary precision and the possibility of gradually
	 * removing characters from the end of the code to reduce its size (and
	 * gradually lose precision). As a consequence of the gradual precision
	 * degradation, nearby places will often (but not always) present similar
	 * prefixes. Conversely, the longer a shared prefix is, the closer the two
	 * places are.</br> </div>
	 * </p>
	 * 
	 * @return
	 */
	public String getGeoHash() {
		return GeoHash.encode(this);
	}

	/**
	 * Create a {@link LatLongCoordinate} instance from <a
	 * href="http://en.wikipedia.org/wiki/Geohash">Geohash</a>
	 * 
	 * @see #getGeoHash()
	 * @param hash
	 * @return
	 */
	public static LatLongCoordinate fromGeoHash(final String hash) {
		return GeoHash.decode(hash);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(latitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(longitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final LatLongCoordinate other = (LatLongCoordinate) obj;
		if (Double.doubleToLongBits(latitude) != Double
				.doubleToLongBits(other.latitude))
			return false;
		if (Double.doubleToLongBits(longitude) != Double
				.doubleToLongBits(other.longitude))
			return false;
		return true;
	}

	/**
	 * Create a new LatLng object to represent a latitude/longitude pair using
	 * the WGS84 datum.
	 * 
	 * @param latitude
	 *            the latitude in degrees. Must be between -90.0 and 90.0
	 *            inclusive. -90.0 and 90.0 are effectively equivalent.
	 * @param longitude
	 *            the longitude in degrees. Must be between -180.0 and 180.0
	 *            inclusive. -180.0 and 180.0 are effectively equivalent.
	 * @param height
	 *            the perpendicular height above the reference ellipsoid.
	 * @throws IllegalArgumentException
	 *             if either the given latitude or the given longitude are
	 *             invalid.
	 * @since 1.1
	 */
	public LatLongCoordinate(final double latitude, final double longitude,
			final double height) {
		this(latitude, longitude, height, new WGS84Datum());
	}

	/**
	 * Create a new LatLng object to represent a latitude/longitude pair using
	 * the WGS84 datum.
	 * 
	 * @param latitudeDegrees
	 *            the degrees part of the latitude. Must be 0 <= latitudeDegrees
	 *            <= 90.0.
	 * @param latitudeMinutes
	 *            the minutes part of the latitude. Must be 0 <= latitudeMinutes
	 *            < 60.0.
	 * @param latitudeSeconds
	 *            the seconds part of the latitude. Must be 0 <= latitudeSeconds
	 *            < 60.0.
	 * @param northSouth
	 *            whether the latitude is north or south of the equator. One of
	 *            LatLng.NORTH or LatLng.SOUTH.
	 * @param longitudeDegrees
	 *            the degrees part of the longitude. Must be 0 <=
	 *            longitudeDegrees <= 90.0.
	 * @param longitudeMinutes
	 *            the minutes part of the longitude. Must be 0 <=
	 *            longitudeMinutes < 60.0.
	 * @param longitudeSeconds
	 *            the seconds part of the longitude. Must be 0 <=
	 *            longitudeSeconds < 60.0.
	 * @param eastWest
	 *            whether the longitude is east or west of the prime meridian.
	 *            One of LatLng.EAST or LatLng.WEST.
	 * @throws IllegalArgumentException
	 *             if any of the parameters are invalid.
	 * @since 1.1
	 */
	public LatLongCoordinate(final int latitudeDegrees,
			final int latitudeMinutes, final double latitudeSeconds,
			final int northSouth, final int longitudeDegrees,
			final int longitudeMinutes, final double longitudeSeconds,
			final int eastWest) throws IllegalArgumentException {
		this(latitudeDegrees, latitudeMinutes, latitudeSeconds, northSouth,
				longitudeDegrees, longitudeMinutes, longitudeSeconds, eastWest,
				0.0, new WGS84Datum());
	}

	/**
	 * Create a new LatLng object to represent a latitude/longitude pair using
	 * the WGS84 datum.
	 * 
	 * @param latitudeDegrees
	 *            the degrees part of the latitude. Must be 0 <= latitudeDegrees
	 *            <= 90.0.
	 * @param latitudeMinutes
	 *            the minutes part of the latitude. Must be 0 <= latitudeMinutes
	 *            < 60.0.
	 * @param latitudeSeconds
	 *            the seconds part of the latitude. Must be 0 <= latitudeSeconds
	 *            < 60.0.
	 * @param northSouth
	 *            whether the latitude is north or south of the equator. One of
	 *            LatLng.NORTH or LatLng.SOUTH.
	 * @param longitudeDegrees
	 *            the degrees part of the longitude. Must be 0 <=
	 *            longitudeDegrees <= 90.0.
	 * @param longitudeMinutes
	 *            the minutes part of the longitude. Must be 0 <=
	 *            longitudeMinutes < 60.0.
	 * @param longitudeSeconds
	 *            the seconds part of the longitude. Must be 0 <=
	 *            longitudeSeconds < 60.0.
	 * @param eastWest
	 *            whether the longitude is east or west of the prime meridian.
	 *            One of LatLng.EAST or LatLng.WEST.
	 * @param height
	 *            the perpendicular height above the reference ellipsoid.
	 * @throws IllegalArgumentException
	 *             if any of the parameters are invalid.
	 * @since 1.1
	 */
	public LatLongCoordinate(final int latitudeDegrees,
			final int latitudeMinutes, final double latitudeSeconds,
			final int northSouth, final int longitudeDegrees,
			final int longitudeMinutes, final double longitudeSeconds,
			final int eastWest, final double height)
			throws IllegalArgumentException {
		this(latitudeDegrees, latitudeMinutes, latitudeSeconds, northSouth,
				longitudeDegrees, longitudeMinutes, longitudeSeconds, eastWest,
				height, new WGS84Datum());
	}

	/**
	 * Create a new LatLng object to represent a latitude/longitude pair using
	 * the specified datum.
	 * 
	 * @param latitudeDegrees
	 *            the degrees part of the latitude. Must be 0 <= latitudeDegrees
	 *            <= 90.0.
	 * @param latitudeMinutes
	 *            the minutes part of the latitude. Must be 0 <= latitudeMinutes
	 *            < 60.0.
	 * @param latitudeSeconds
	 *            the seconds part of the latitude. Must be 0 <= latitudeSeconds
	 *            < 60.0.
	 * @param northSouth
	 *            whether the latitude is north or south of the equator. One of
	 *            LatLng.NORTH or LatLng.SOUTH.
	 * @param longitudeDegrees
	 *            the degrees part of the longitude. Must be 0 <=
	 *            longitudeDegrees <= 90.0.
	 * @param longitudeMinutes
	 *            the minutes part of the longitude. Must be 0 <=
	 *            longitudeMinutes < 60.0.
	 * @param longitudeSeconds
	 *            the seconds part of the longitude. Must be 0 <=
	 *            longitudeSeconds < 60.0.
	 * @param eastWest
	 *            whether the longitude is east or west of the prime meridian.
	 *            One of LatLng.EAST or LatLng.WEST.
	 * @param height
	 *            the perpendicular height above the reference ellipsoid.
	 * @param datum
	 *            the datum that this reference is based on.
	 * @throws IllegalArgumentException
	 *             if any of the parameters are invalid.
	 * @since 1.1
	 */
	public LatLongCoordinate(final int latitudeDegrees,
			final int latitudeMinutes, final double latitudeSeconds,
			final int northSouth, final int longitudeDegrees,
			final int longitudeMinutes, final double longitudeSeconds,
			final int eastWest, final double height, final Datum datum)
			throws IllegalArgumentException {

		if (latitudeDegrees < 0.0 || latitudeDegrees > 90.0
				|| latitudeMinutes < 0.0 || latitudeMinutes >= 60.0
				|| latitudeSeconds < 0.0 || latitudeSeconds >= 60.0
				|| (northSouth != SOUTH && northSouth != NORTH)) {
			throw new IllegalArgumentException("Invalid latitude");
		}

		if (longitudeDegrees < 0.0 || longitudeDegrees > 90.0
				|| longitudeMinutes < 0.0 || longitudeMinutes >= 60.0
				|| longitudeSeconds < 0.0 || longitudeSeconds >= 60.0
				|| (eastWest != SOUTH && eastWest != NORTH)) {
			throw new IllegalArgumentException("Invalid longitude");
		}

		this.latitude = northSouth
				* (latitudeDegrees + (latitudeMinutes / 60.0) + (latitudeSeconds / 3600.0));
		this.longitude = eastWest
				* (longitudeDegrees + (longitudeMinutes / 60.0) + (longitudeSeconds / 3600.0));
		this.datum = datum;

	}

	/**
	 * Create a new LatLng object to represent a latitude/longitude pair using
	 * the specified datum.
	 * 
	 * @param latitude
	 *            the latitude in degrees. Must be between -90.0 and 90.0
	 *            inclusive. -90.0 and 90.0 are effectively equivalent.
	 * @param longitude
	 *            the longitude in degrees. Must be between -180.0 and 180.0
	 *            inclusive. -180.0 and 180.0 are effectively equivalent.
	 * @param height
	 *            the perpendicular height above the reference ellipsoid.
	 * @param datum
	 *            the datum that this reference is based on.
	 * @throws IllegalArgumentException
	 *             if either the given latitude or the given longitude are
	 *             invalid.
	 * @since 1.1
	 */
	public LatLongCoordinate(final double latitude, final double longitude,
			final double height, final Datum datum)
			throws IllegalArgumentException {

		if (latitude < -90.0 || latitude > 90.0) {
			throw new IllegalArgumentException("Latitude (" + latitude
					+ ") is invalid. Must be between -90.0 and 90.0 inclusive.");
		}

		if (longitude < -180.0 || longitude > 180.0) {
			throw new IllegalArgumentException(
					"Longitude ("
							+ longitude
							+ ") is invalid. Must be between -180.0 and 180.0 inclusive.");
		}

		this.latitude = latitude;
		this.longitude = longitude;
		this.height = height;
		this.datum = datum;
	}

	/**
	 * Get a String representation of this LatLng object.
	 * 
	 * @return a String representation of this LatLng object.
	 * @since 1.0
	 */
	public String toString() {
		return "(" + this.latitude + ", " + this.longitude + ")";
	}

	/**
	 * Return a String representation of this LatLng object in
	 * degrees-minutes-seconds format. The returned format will be like this: DD
	 * MM SS.SSS N DD MM SS.SSS E where DD is the number of degrees, MM is the
	 * number of minutes, SS.SSS is the number of seconds, N is either N or S to
	 * indicate north or south of the equator and E is either E or W to indicate
	 * east or west of the prime meridian.
	 * 
	 * @return a String representation of this LatLng object in DMS format.
	 */
	public String toDMSString() {
		final String ret = formatLatitude() + " " + formatLongitude();

		return ret;
	}

	/**
	 * Format the latitude into degrees-minutes-seconds format.
	 * 
	 * @return the formatted String
	 */
	private String formatLatitude() {
		final String ns = getLatitude() >= 0 ? "N" : "S";
		return Math.abs(getLatitudeDegrees()) + " " + getLatitudeMinutes()
				+ " " + getLatitudeSeconds() + " " + ns;
	}

	/**
	 * Format the longitude into degrees-minutes-seconds format.
	 * 
	 * @return the formatted String.
	 */
	private String formatLongitude() {
		final String ew = getLongitude() >= 0 ? "E" : "W";
		return Math.abs(getLongitudeDegrees()) + " " + getLongitudeMinutes()
				+ " " + getLongitudeSeconds() + " " + ew;
	}

	/**
	 * Convert this latitude and longitude into an OSGB (Ordnance Survey of
	 * Great Britain) grid reference.
	 * 
	 * @return the converted OSGB grid reference.
	 * @since 1.0
	 */
	public OSGBCoordinate toOSGB() {
		final Airy1830Ellipsoid airy1830 = Airy1830Ellipsoid.getInstance();
		final double OSGB_F0 = 0.9996012717;
		final double N0 = -100000.0;
		final double E0 = 400000.0;
		final double phi0 = Math.toRadians(49.0);
		final double lambda0 = Math.toRadians(-2.0);
		final double a = airy1830.getSemiMajorAxis();
		final double b = airy1830.getSemiMinorAxis();
		final double eSquared = airy1830.getEccentricitySquared();
		final double phi = Math.toRadians(getLatitude());
		final double lambda = Math.toRadians(getLongitude());
		double E = 0.0;
		double N = 0.0;
		final double n = (a - b) / (a + b);
		final double v = a * OSGB_F0
				* Math.pow(1.0 - eSquared * MathUtils.sinSquared(phi), -0.5);
		final double rho = a * OSGB_F0 * (1.0 - eSquared)
				* Math.pow(1.0 - eSquared * MathUtils.sinSquared(phi), -1.5);
		final double etaSquared = (v / rho) - 1.0;
		final double M = (b * OSGB_F0)
				* (((1 + n + ((5.0 / 4.0) * n * n) + ((5.0 / 4.0) * n * n * n)) * (phi - phi0))
						- (((3 * n) + (3 * n * n) + ((21.0 / 8.0) * n * n * n))
								* Math.sin(phi - phi0) * Math.cos(phi + phi0))
						+ ((((15.0 / 8.0) * n * n) + ((15.0 / 8.0) * n * n * n))
								* Math.sin(2.0 * (phi - phi0)) * Math
									.cos(2.0 * (phi + phi0))) - (((35.0 / 24.0)
						* n * n * n)
						* Math.sin(3.0 * (phi - phi0)) * Math
							.cos(3.0 * (phi + phi0))));
		final double I = M + N0;
		final double II = (v / 2.0) * Math.sin(phi) * Math.cos(phi);
		final double III = (v / 24.0) * Math.sin(phi)
				* Math.pow(Math.cos(phi), 3.0)
				* (5.0 - MathUtils.tanSquared(phi) + (9.0 * etaSquared));
		final double IIIA = (v / 720.0)
				* Math.sin(phi)
				* Math.pow(Math.cos(phi), 5.0)
				* (61.0 - (58.0 * MathUtils.tanSquared(phi)) + Math.pow(
						Math.tan(phi), 4.0));
		final double IV = v * Math.cos(phi);
		final double V = (v / 6.0) * Math.pow(Math.cos(phi), 3.0)
				* ((v / rho) - MathUtils.tanSquared(phi));
		final double VI = (v / 120.0)
				* Math.pow(Math.cos(phi), 5.0)
				* (5.0 - (18.0 * MathUtils.tanSquared(phi))
						+ (Math.pow(Math.tan(phi), 4.0)) + (14 * etaSquared) - (58 * MathUtils
						.tanSquared(phi) * etaSquared));

		N = I + (II * Math.pow(lambda - lambda0, 2.0))
				+ (III * Math.pow(lambda - lambda0, 4.0))
				+ (IIIA * Math.pow(lambda - lambda0, 6.0));
		E = E0 + (IV * (lambda - lambda0))
				+ (V * Math.pow(lambda - lambda0, 3.0))
				+ (VI * Math.pow(lambda - lambda0, 5.0));

		return new OSGBCoordinate(E, N);
	}

	/**
	 * Convert this latitude and longitude to a UTM reference.
	 * 
	 * @return the converted UTM reference.
	 * @throws CoordinateException
	 *             if an attempt is made to convert a LatLng that falls outside
	 *             the area covered by the UTM grid. The UTM grid is only
	 *             defined for latitudes south of 84&deg;N and north of
	 *             80&deg;S.
	 * @since 1.0
	 */
	public UTMCoordinate toUTM() throws CoordinateException {

		if (getLatitude() < -80 || getLatitude() > 84) {
			throw new CoordinateException("Latitude (" + getLatitude()
					+ ") falls outside the UTM grid.");
		}

		if (this.longitude == 180.0) {
			this.longitude = -180.0;
		}

		final double UTM_F0 = 0.9996;
		final double a = WGS84Ellipsoid.getInstance().getSemiMajorAxis();
		final double eSquared = WGS84Ellipsoid.getInstance()
				.getEccentricitySquared();
		final double longitude = this.longitude;
		final double latitude = this.latitude;

		final double latitudeRad = latitude * (Math.PI / 180.0);
		final double longitudeRad = longitude * (Math.PI / 180.0);
		int longitudeZone = (int) Math.floor((longitude + 180.0) / 6.0) + 1;

		// Special zone for Norway
		if (latitude >= 56.0 && latitude < 64.0 && longitude >= 3.0
				&& longitude < 12.0) {
			longitudeZone = 32;
		}

		// Special zones for Svalbard
		if (latitude >= 72.0 && latitude < 84.0) {
			if (longitude >= 0.0 && longitude < 9.0) {
				longitudeZone = 31;
			} else if (longitude >= 9.0 && longitude < 21.0) {
				longitudeZone = 33;
			} else if (longitude >= 21.0 && longitude < 33.0) {
				longitudeZone = 35;
			} else if (longitude >= 33.0 && longitude < 42.0) {
				longitudeZone = 37;
			}
		}

		final double longitudeOrigin = (longitudeZone - 1) * 6 - 180 + 3;
		final double longitudeOriginRad = longitudeOrigin * (Math.PI / 180.0);

		final char UTMZone = UTMCoordinate.getUTMLatitudeZoneLetter(latitude);

		final double ePrimeSquared = (eSquared) / (1 - eSquared);

		final double n = a
				/ Math.sqrt(1 - eSquared * Math.sin(latitudeRad)
						* Math.sin(latitudeRad));
		final double t = Math.tan(latitudeRad) * Math.tan(latitudeRad);
		final double c = ePrimeSquared * Math.cos(latitudeRad)
				* Math.cos(latitudeRad);
		final double A = Math.cos(latitudeRad)
				* (longitudeRad - longitudeOriginRad);

		final double M = a
				* ((1 - eSquared / 4 - 3 * eSquared * eSquared / 64 - 5
						* eSquared * eSquared * eSquared / 256)
						* latitudeRad
						- (3 * eSquared / 8 + 3 * eSquared * eSquared / 32 + 45
								* eSquared * eSquared * eSquared / 1024)
						* Math.sin(2 * latitudeRad)
						+ (15 * eSquared * eSquared / 256 + 45 * eSquared
								* eSquared * eSquared / 1024)
						* Math.sin(4 * latitudeRad) - (35 * eSquared * eSquared
						* eSquared / 3072)
						* Math.sin(6 * latitudeRad));

		final double UTMEasting = (UTM_F0
				* n
				* (A + (1 - t + c) * Math.pow(A, 3.0) / 6 + (5 - 18 * t + t * t
						+ 72 * c - 58 * ePrimeSquared)
						* Math.pow(A, 5.0) / 120) + 500000.0);

		double UTMNorthing = (UTM_F0 * (M + n
				* Math.tan(latitudeRad)
				* (A * A / 2 + (5 - t + (9 * c) + (4 * c * c))
						* Math.pow(A, 4.0) / 24 + (61 - (58 * t) + (t * t)
						+ (600 * c) - (330 * ePrimeSquared))
						* Math.pow(A, 6.0) / 720)));

		// Adjust for the southern hemisphere
		if (latitude < 0) {
			UTMNorthing += 10000000.0;
		}

		return new UTMCoordinate(longitudeZone, UTMZone, UTMEasting,
				UTMNorthing);
	}

	/**
	 * Convert this latitude and longitude to an MGRS reference.
	 * 
	 * @return the converted MGRS reference
	 * @since 1.1
	 */
	public MGRSCoordinate toMGRS() throws CoordinateException {
		final UTMCoordinate utm = toUTM();
		return new MGRSCoordinate(utm);
	}

	/**
	 * Convert this LatLng from the OSGB36 datum to the WGS84 datum using an
	 * approximate Helmert transformation.
	 * 
	 * @since 1.0
	 */
	public void toWGS84() {
		double a = Airy1830Ellipsoid.getInstance().getSemiMajorAxis();
		double eSquared = Airy1830Ellipsoid.getInstance()
				.getEccentricitySquared();
		final double phi = Math.toRadians(latitude);
		final double lambda = Math.toRadians(longitude);
		double v = a / (Math.sqrt(1 - eSquared * MathUtils.sinSquared(phi)));
		final double H = 0; // height
		final double x = (v + H) * Math.cos(phi) * Math.cos(lambda);
		final double y = (v + H) * Math.cos(phi) * Math.sin(lambda);
		final double z = ((1 - eSquared) * v + H) * Math.sin(phi);

		final double tx = 446.448;
		// ty : Incorrect value in v1.0 (-124.157). Corrected in v1.1.
		final double ty = -125.157;
		final double tz = 542.060;
		final double s = -0.0000204894;
		final double rx = Math.toRadians(0.00004172222);
		final double ry = Math.toRadians(0.00006861111);
		final double rz = Math.toRadians(0.00023391666);

		final double xB = tx + (x * (1 + s)) + (-rx * y) + (ry * z);
		final double yB = ty + (rz * x) + (y * (1 + s)) + (-rx * z);
		final double zB = tz + (-ry * x) + (rx * y) + (z * (1 + s));

		a = WGS84Ellipsoid.getInstance().getSemiMajorAxis();
		eSquared = WGS84Ellipsoid.getInstance().getEccentricitySquared();

		final double lambdaB = Math.toDegrees(Math.atan(yB / xB));
		final double p = Math.sqrt((xB * xB) + (yB * yB));
		double phiN = Math.atan(zB / (p * (1 - eSquared)));
		for (int i = 1; i < 10; i++) {
			v = a / (Math.sqrt(1 - eSquared * MathUtils.sinSquared(phiN)));
			final double phiN1 = Math
					.atan((zB + (eSquared * v * Math.sin(phiN))) / p);
			phiN = phiN1;
		}

		final double phiB = Math.toDegrees(phiN);

		latitude = phiB;
		longitude = lambdaB;
	}

	public void toDatum(final Datum d) {

		double invert = 1;

		if (!(datum instanceof WGS84Datum) && !(d instanceof WGS84Datum)) {
			toDatum(new WGS84Datum());
		} else {
			if (d instanceof WGS84Datum) {
				// Don't do anything if datum and d are both WGS84.
				return;
			}
			invert = -1;
		}

		double a = datum.getReferenceEllipsoid().getSemiMajorAxis();
		double eSquared = datum.getReferenceEllipsoid()
				.getEccentricitySquared();
		final double phi = Math.toRadians(latitude);
		final double lambda = Math.toRadians(longitude);
		double v = a / (Math.sqrt(1 - eSquared * MathUtils.sinSquared(phi)));
		final double H = height; // height
		final double x = (v + H) * Math.cos(phi) * Math.cos(lambda);
		final double y = (v + H) * Math.cos(phi) * Math.sin(lambda);
		final double z = ((1 - eSquared) * v + H) * Math.sin(phi);

		final double dx = invert * d.getDx();// 446.448;
		final double dy = invert * d.getDy();// -125.157;
		final double dz = invert * d.getDz();// 542.060;
		final double ds = invert * d.getDs() / 1000000.0;// -0.0000204894;
		final double rx = invert * Math.toRadians(d.getRx() / 3600.0);// Math.toRadians(0.00004172222);
		final double ry = invert * Math.toRadians(d.getRy() / 3600.0);// Math.toRadians(0.00006861111);
		final double rz = invert * Math.toRadians(d.getRz() / 3600.0);// Math.toRadians(0.00023391666);

		final double sc = 1 + ds;
		final double xB = dx + (x * sc) + ((-rx * y) * sc) + ((ry * z) * sc);
		final double yB = dy + ((rz * x) * sc) + (y * sc) + ((-rx * z) * sc);
		final double zB = dz + ((-ry * x) * sc) + ((rx * y) * sc) + (z * sc);

		a = d.getReferenceEllipsoid().getSemiMajorAxis();
		eSquared = d.getReferenceEllipsoid().getEccentricitySquared();

		final double lambdaB = Math.toDegrees(Math.atan(yB / xB));
		final double p = Math.sqrt((xB * xB) + (yB * yB));
		double phiN = Math.atan(zB / (p * (1 - eSquared)));
		for (int i = 1; i < 10; i++) {
			v = a / (Math.sqrt(1 - eSquared * MathUtils.sinSquared(phiN)));
			final double phiN1 = Math
					.atan((zB + (eSquared * v * Math.sin(phiN))) / p);
			phiN = phiN1;
		}

		final double phiB = Math.toDegrees(phiN);

		latitude = phiB;
		longitude = lambdaB;
	}

	/**
	 * Convert this LatLng from the WGS84 datum to the OSGB36 datum using an
	 * approximate Helmert transformation.
	 * 
	 * @since 1.0
	 */
	public void toOSGB36() {
		final WGS84Ellipsoid wgs84 = WGS84Ellipsoid.getInstance();
		double a = wgs84.getSemiMajorAxis();
		double eSquared = wgs84.getEccentricitySquared();
		final double phi = Math.toRadians(this.latitude);
		final double lambda = Math.toRadians(this.longitude);
		double v = a / (Math.sqrt(1 - eSquared * MathUtils.sinSquared(phi)));
		final double H = 0; // height
		final double x = (v + H) * Math.cos(phi) * Math.cos(lambda);
		final double y = (v + H) * Math.cos(phi) * Math.sin(lambda);
		final double z = ((1 - eSquared) * v + H) * Math.sin(phi);

		final double tx = -446.448;
		// ty : Incorrect value in v1.0 (124.157). Corrected in v1.1.
		final double ty = 125.157;
		final double tz = -542.060;
		final double s = 0.0000204894;
		final double rx = Math.toRadians(-0.00004172222);
		final double ry = Math.toRadians(-0.00006861111);
		final double rz = Math.toRadians(-0.00023391666);

		final double xB = tx + (x * (1 + s)) + (-rx * y) + (ry * z);
		final double yB = ty + (rz * x) + (y * (1 + s)) + (-rx * z);
		final double zB = tz + (-ry * x) + (rx * y) + (z * (1 + s));

		a = Airy1830Ellipsoid.getInstance().getSemiMajorAxis();
		eSquared = Airy1830Ellipsoid.getInstance().getEccentricitySquared();

		final double lambdaB = Math.toDegrees(Math.atan(yB / xB));
		final double p = Math.sqrt((xB * xB) + (yB * yB));
		double phiN = Math.atan(zB / (p * (1 - eSquared)));
		for (int i = 1; i < 10; i++) {
			v = a / (Math.sqrt(1 - eSquared * MathUtils.sinSquared(phiN)));
			final double phiN1 = Math
					.atan((zB + (eSquared * v * Math.sin(phiN))) / p);
			phiN = phiN1;
		}

		final double phiB = Math.toDegrees(phiN);

		latitude = phiB;
		longitude = lambdaB;
	}

	/**
	 * Calculate the surface distance in kilometres from this LatLng to the
	 * given LatLng.
	 * 
	 * @param ll
	 *            the LatLng object to measure the distance to.
	 * @return the surface distance in kilometres.
	 * @since 1.0
	 */
	public double getSurfaceDistance(final LatLongCoordinate ll) {
		final double er = 6366.707;

		final double latFrom = Math.toRadians(getLatitude());
		final double latTo = Math.toRadians(ll.getLatitude());
		final double lngFrom = Math.toRadians(getLongitude());
		final double lngTo = Math.toRadians(ll.getLongitude());

		final double d = Math.acos(Math.sin(latFrom) * Math.sin(latTo)
				+ Math.cos(latFrom) * Math.cos(latTo)
				* Math.cos(lngTo - lngFrom))
				* er;

		return d;
	}

	/**
	 * Calculate the surface distance in miles from this LatLng to the given
	 * LatLng.
	 * 
	 * @param ll
	 *            the LatLng object to measure the distance to.
	 * @return the surface distance in miles.
	 * @see #getSurfaceDistance(LatLongCoordinate)
	 */
	public double getSurfaceDistanceMiles(final LatLongCoordinate ll) {
		return getSurfaceDistance(ll) / 1.609344;
	}

	/**
	 * Return the latitude in degrees.
	 * 
	 * @return the latitude in degrees.
	 * @since 1.1
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @return
	 * @since 1.1
	 */
	public int getLatitudeDegrees() {
		final double ll = getLatitude();
		int deg = (int) Math.floor(ll);
		final double minx = ll - deg;
		if (ll < 0 && minx != 0.0) {
			deg++;
		}
		return deg;
	}

	/**
	 * Get latitude in minutes representation
	 * 
	 * @return
	 * @since 1.1
	 */
	public int getLatitudeMinutes() {
		final double ll = getLatitude();
		final int deg = (int) Math.floor(ll);
		double minx = ll - deg;
		if (ll < 0 && minx != 0.0) {
			minx = 1 - minx;
		}
		final int min = (int) Math.floor(minx * 60);
		return min;
	}

	/**
	 * Get latitude in seconds representation
	 * 
	 * @return
	 * @since 1.1
	 */
	public double getLatitudeSeconds() {
		final double ll = getLatitude();
		final int deg = (int) Math.floor(ll);
		double minx = ll - deg;
		if (ll < 0 && minx != 0.0) {
			minx = 1 - minx;
		}
		final int min = (int) Math.floor(minx * 60);
		final double sec = ((minx * 60) - min) * 60;
		return sec;
	}

	/**
	 * Return the longitude in degrees.
	 * 
	 * @return the longitude in degrees.
	 * @since 1.0
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @return
	 * @since 1.1
	 */
	public int getLongitudeDegrees() {
		final double ll = getLongitude();
		int deg = (int) Math.floor(ll);
		final double minx = ll - deg;
		if (ll < 0 && minx != 0.0) {
			deg++;
		}
		return deg;
	}

	/**
	 * Get longitude in minutes
	 * 
	 * @return
	 */
	public int getLongitudeMinutes() {
		final double ll = getLongitude();
		final int deg = (int) Math.floor(ll);
		double minx = ll - deg;
		if (ll < 0 && minx != 0.0) {
			minx = 1 - minx;
		}
		final int min = (int) Math.floor(minx * 60);
		return min;
	}

	/**
	 * @return
	 */
	public double getLongitudeSeconds() {
		final double ll = getLongitude();
		final int deg = (int) Math.floor(ll);
		double minx = ll - deg;
		if (ll < 0 && minx != 0.0) {
			minx = 1 - minx;
		}
		final int min = (int) Math.floor(minx * 60);
		final double sec = ((minx * 60) - min) * 60;
		return sec;
	}

	/**
	 * Get the height.
	 * 
	 * @return the height.
	 * @since 1.1
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * Get midpoint between this latlong coordinate and
	 * 
	 * @param ll2
	 * @return
	 */
	public LatLongCoordinate getMidPoint(final LatLongCoordinate ll2) {
		final double dLon = Math.toRadians((ll2.getLongitude() - longitude));
		final double Bx = Math.cos(ll2.getLatitude()) * Math.cos(dLon);
		final double By = Math.cos(ll2.getLatitude()) * Math.sin(dLon);
		final double lat3 = Math.atan2(
				Math.sin(latitude) + Math.sin(ll2.getLatitude()),
				Math.sqrt((Math.cos(latitude) + Bx) * (Math.cos(latitude) + Bx)
						+ By * By));
		final double lon3 = longitude + Math.atan2(By, Math.cos(latitude) + Bx);
		return new LatLongCoordinate(lat3, lon3);
	}

	/**
	 * Returns a coordinate on the great circle at the specified bearing.
	 * 
	 * @param d
	 *            the distance from the center
	 * @param bearing
	 *            the great circle bearing
	 * @return a coordinate at the specified bearing
	 */
	public LatLongCoordinate getPointOnGreatCircle(final double d,
			final double bearing) {
		final double angularDistance = d / LocationConstants.EARTH_RADIUS_KM;
		final double lon1 = Math.toRadians(longitude);
		final double lat1 = Math.toRadians(latitude);
		final double cosLat = Math.cos(lat1);
		final double sinLat = Math.sin(lat1);
		final double sinD = Math.sin(angularDistance);
		final double cosD = Math.cos(angularDistance);
		final double brng = Math.toRadians(bearing);
		final double lat2 = Math.asin((sinLat * cosD)
				+ (cosLat * sinD * Math.cos(brng)));
		final double lon2 = lon1
				+ Math.atan2(Math.sin(brng) * sinD * cosLat, cosD - sinLat
						* Math.sin(lat2));
		return new LatLongCoordinate(lat2, lon2);
	}

	/**
	 * Checks the given latitude value and throws an exception if the value is
	 * out of range.
	 * 
	 * @param lat
	 *            the latitude value that should be checked.
	 * @return the latitude value.
	 * @throws IllegalArgumentException
	 *             if the latitude value is < LATITUDE_MIN or > LATITUDE_MAX.
	 */
	public static double validateLatitude(final double lat) {
		if (lat < LATITUDE_MIN) {
			throw new IllegalArgumentException("invalid latitude value: " + lat);
		} else if (lat > LATITUDE_MAX) {
			throw new IllegalArgumentException("invalid latitude value: " + lat);
		} else {
			return lat;
		}
	}

	/**
	 * @param location
	 * @param currentBestLocation
	 */

	/**
	 * Checks the given longitude value and throws an exception if the value is
	 * out of range.
	 * 
	 * @param lon
	 *            the longitude value that should be checked.
	 * @return the longitude value.
	 * @throws IllegalArgumentException
	 *             if the longitude value is < LONGITUDE_MIN or > LONGITUDE_MAX.
	 */
	public static double validateLongitude(final double lon) {
		if (lon < LONGITUDE_MIN) {
			throw new IllegalArgumentException("invalid longitude value: "
					+ lon);
		} else if (lon > LONGITUDE_MAX) {
			throw new IllegalArgumentException("invalid longitude value: "
					+ lon);
		} else {
			return lon;
		}
	}

	/**
	 * Calculates geodetic distance between two GeoCoordinates using Vincenty
	 * inverse formula for ellipsoids. This is very accurate but consumes more
	 * resources and time than the sphericalDistance method
	 * <p/>
	 * Paper: Vincenty inverse formula - T Vincenty, "Direct and Inverse
	 * Solutions of Geodesics on the Ellipsoid with application of nested
	 * equations", Survey Review, vol XXII no 176, 1975
	 * (http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf)
	 * 
	 * @param ll2
	 *            second GeoCoordinate
	 * @return distance in meters between points as a double
	 * @see #getLawOfCosineDistance(LatLongCoordinate)
	 * @see #getHaversineDistance(LatLongCoordinate)
	 */
	public double getVincentyDistance(final LatLongCoordinate ll2) {
		final double f = 1 / LocationConstants.INVERSEFLATTENING;
		final double L = Math.toRadians(ll2.getLongitude() - longitude);
		final double U1 = Math.atan((1 - f)
				* Math.tan(Math.toRadians(latitude)));
		final double U2 = Math.atan((1 - f)
				* Math.tan(Math.toRadians(ll2.getLatitude())));
		final double sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
		final double sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);

		double lambda = L, lambdaP, iterLimit = 100;

		double cosSqAlpha = 0, sinSigma = 0, cosSigma = 0, cos2SigmaM = 0, sigma = 0, sinLambda = 0, sinAlpha = 0, cosLambda = 0;
		do {
			sinLambda = Math.sin(lambda);
			cosLambda = Math.cos(lambda);
			sinSigma = Math.sqrt((cosU2 * sinLambda) * (cosU2 * sinLambda)
					+ (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
					* (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
			if (sinSigma == 0) {
				return 0; // co-incident points
			}
			cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
			sigma = Math.atan2(sinSigma, cosSigma);
			sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
			cosSqAlpha = 1 - sinAlpha * sinAlpha;
			if (cosSqAlpha != 0) {
				cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
			} else {
				cos2SigmaM = 0;
			}
			final double C = f / 16 * cosSqAlpha
					* (4 + f * (4 - 3 * cosSqAlpha));
			lambdaP = lambda;
			lambda = L
					+ (1 - C)
					* f
					* sinAlpha
					* (sigma + C
							* sinSigma
							* (cos2SigmaM + C * cosSigma
									* (-1 + 2 * cos2SigmaM * cos2SigmaM)));
		} while ((Math.abs(lambda - lambdaP) > 1e-12) && (--iterLimit > 0));

		if (iterLimit == 0) {
			return 0; // formula failed to converge
		}

		final double uSq = cosSqAlpha
				* (Math.pow(LocationConstants.EQUATORIALRADIUS, 2) - Math.pow(
						LocationConstants.POLARRADIUS, 2))
				/ Math.pow(LocationConstants.POLARRADIUS, 2);
		final double A = 1 + uSq / 16384
				* (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
		final double B = uSq / 1024
				* (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
		final double deltaSigma = B
				* sinSigma
				* (cos2SigmaM + B
						/ 4
						* (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B
								/ 6 * cos2SigmaM
								* (-3 + 4 * sinSigma * sinSigma)
								* (-3 + 4 * cos2SigmaM * cos2SigmaM)));
		final double s = LocationConstants.POLARRADIUS * A
				* (sigma - deltaSigma);

		return s;
	}

	/**
	 * Convert the list of coordinates to GPX format.
	 * 
	 * @param g
	 *            coordinates
	 * @return Coordinates in GPX format
	 */
	public static String toGPX(final List<LatLongCoordinate> g) {
		final StringBuilder sb = new StringBuilder();
		sb.append(
				"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>")
				.append("\n");
		sb.append(
				"<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" creator=\"byHand\" "
						+ "version=\"1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
						+ "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 "
						+ "http://www.topografix.com/GPX/1/1/gpx.xsd\">")
				.append("\n");
		for (final LatLongCoordinate c : g) {
			sb.append("\t<wpt ").append("lat=\"").append(c.getLatitude())
					.append("\" ");
			sb.append("lon=\"").append(c.getLongitude()).append("\"/>");
			sb.append("\n");
		}
		sb.append("</gpx>");

		return sb.toString();
	}

	/**
	 * Computes whether the given point lies on or near a polyline, within a
	 * specified tolerance in meters. The polyline is composed of great circle
	 * segments if geodesic is true, and of Rhumb segments otherwise. The
	 * polyline is not closed -- the closing segment between the first point and
	 * the last point is not included.
	 */
	public boolean isLocationOnPath(final List<LatLongCoordinate> polyline,
			final boolean geodesic, final double tolerance) {
		return isLocationOnEdgeOrPath(polyline, false, geodesic, tolerance);
	}

	private static final double DEFAULT_TOLERANCE = 0.1; // meters.

	/**
	 * Same as {@link #isLocationOnPath(LatLng, List, boolean, double)}
	 * 
	 * with a default tolerance of 0.1 meters.
	 */
	public boolean isLocationOnPath(final List<LatLongCoordinate> polyline,
			final boolean geodesic) {
		return isLocationOnPath(polyline, geodesic, DEFAULT_TOLERANCE);
	}

	private boolean isLocationOnEdgeOrPath(final List<LatLongCoordinate> poly,
			final boolean closed, final boolean geodesic,
			final double toleranceEarth) {
		final int size = poly.size();
		if (size == 0) {
			return false;
		}
		final double tolerance = toleranceEarth
				/ LocationConstants.EARTH_RADIUS_METERS;
		final double havTolerance = hav(tolerance);
		final double lat3 = toRadians(latitude);
		final double lng3 = toRadians(longitude);
		final LatLongCoordinate prev = poly.get(closed ? size - 1 : 0);
		double lat1 = toRadians(prev.latitude);
		double lng1 = toRadians(prev.longitude);
		if (geodesic) {
			for (final LatLongCoordinate point2 : poly) {
				final double lat2 = toRadians(point2.latitude);
				final double lng2 = toRadians(point2.longitude);
				if (isOnSegmentGC(lat1, lng1, lat2, lng2, lat3, lng3,
						havTolerance)) {
					return true;
				}
				lat1 = lat2;
				lng1 = lng2;
			}
		} else {
			// We project the points to mercator space, where the Rhumb segment
			// is a straight line,
			// and compute the geodesic distance between point3 and the closest
			// point on the
			// segment. This method is an approximation, because it uses
			// "closest" in mercator
			// space which is not "closest" on the sphere -- but the error is
			// small because
			// "tolerance" is small.
			final double minAcceptable = lat3 - tolerance;
			final double maxAcceptable = lat3 + tolerance;
			double y1 = mercator(lat1);
			final double y3 = mercator(lat3);
			final double[] xTry = new double[3];
			for (final LatLongCoordinate point2 : poly) {
				final double lat2 = toRadians(point2.latitude);
				final double y2 = mercator(lat2);
				final double lng2 = toRadians(point2.longitude);
				if (Math.max(lat1, lat2) >= minAcceptable
						&& Math.min(lat1, lat2) <= maxAcceptable) {
					// We offset longitudes by -lng1; the implicit x1 is 0.
					final double x2 = MathUtils.wrap(lng2 - lng1, -PI, PI);
					final double x3Base = MathUtils.wrap(lng3 - lng1, -PI, PI);
					xTry[0] = x3Base;
					// Also explore wrapping of x3Base around the world in both
					// directions.
					xTry[1] = x3Base + 2 * PI;
					xTry[2] = x3Base - 2 * PI;
					for (final double x3 : xTry) {
						final double dy = y2 - y1;
						final double len2 = x2 * x2 + dy * dy;
						final double t = len2 <= 0 ? 0 : MathUtils.clamp((x3
								* x2 + (y3 - y1) * dy)
								/ len2, 0, 1);
						final double xClosest = t * x2;
						final double yClosest = y1 + t * dy;
						final double latClosest = inverseMercator(yClosest);
						final double havDist = havDistance(lat3, latClosest, x3
								- xClosest);
						if (havDist < havTolerance) {
							return true;
						}
					}
				}
				lat1 = lat2;
				lng1 = lng2;
				y1 = y2;
			}
		}
		return false;
	}

	/**
	 * Returns true if the line segment connecting the two specified longitudes
	 * crosses the international dateline.
	 * 
	 * @param ll2
	 *            second longitude
	 * @return true if the line segment crosses the international dateline,
	 *         false otherwise
	 */
	public boolean isDateLineCrossOver(final LatLongCoordinate ll2) {
		if (Math.abs(longitude - ll2.getLongitude()) > 180.0)
			return true;
		return false;
	}

	/**
	 * Get the <a
	 * href="http://en.wikipedia.org/wiki/Bearing_(navigation)">bearing</a> for
	 * the given points.
	 * 
	 * @param lon2
	 * @param lat2
	 * @return bearing in degrees
	 * @see #getHeading(LatLongCoordinate)
	 */
	public double getBearing(final LatLongCoordinate ll2) {
		final double dLon = Math.toRadians((ll2.longitude - longitude));
		final double y = Math.sin(dLon) * Math.cos(ll2.latitude);
		final double x = Math.cos(latitude) * Math.sin(ll2.latitude)
				- Math.sin(latitude) * Math.cos(ll2.latitude) * Math.cos(dLon);
		return Math.toDegrees(Math.atan2(y, x));
	}

	/**
	 * Calculate the spherical distance between two GeoCoordinates in meters
	 * using the Haversine formula.
	 * <p/>
	 * This calculation is done using the assumption, that the earth is a
	 * sphere, it is not though. If you need a higher precision and can afford a
	 * longer execution time you might want to use vincentyDistance
	 * 
	 * @return distance in meters as a double
	 * @throws IllegalArgumentException
	 *             if one of the arguments is null
	 * @see #getVincentyDistance(LatLongCoordinate)
	 * @see #getLawOfCosineDistance(LatLongCoordinate)
	 */
	public double getHaversineDistance(final LatLongCoordinate ll2) {
		final double dLat = Math.toRadians(ll2.getLatitude() - latitude);
		final double dLon = Math.toRadians(ll2.getLongitude() - longitude);
		final double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(Math.toRadians(latitude))
				* Math.cos(Math.toRadians(ll2.getLatitude()))
				* Math.sin(dLon / 2) * Math.sin(dLon / 2);
		final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return c * LocationConstants.EQUATORIALRADIUS;
	}

	/**
	 * Calculate the spherical distance between two points using the Spherical
	 * law of Cosines formula.
	 * <p>
	 * Its a more accurate than the haversine distance and gives an accuracy of
	 * upto 1 metre.
	 * </p>
	 * 
	 * @param ll2
	 * @return Distance in Km
	 * @see #getVincentyDistance(LatLongCoordinate)
	 * @see #getHaversineDistance(LatLongCoordinate)
	 */
	public double getLawOfCosineDistance(final LatLongCoordinate ll2) {
		return Math.acos(Math.sin(latitude) * Math.sin(ll2.getLatitude())
				+ Math.cos(latitude) * Math.cos(ll2.getLatitude())
				* Math.cos(ll2.getLongitude() - longitude)) * 6371;
	}

	/**
	 * Calculate the amount of degrees of latitude for a given distance in
	 * meters.
	 * 
	 * @param meters
	 *            distance in meters
	 * @return latitude degrees
	 */
	public static double getLatitudeDistance(final int meters) {
		return (meters * 360)
				/ (2 * Math.PI * LocationConstants.EQUATORIALRADIUS);
	}

	/**
	 * Calculate the amount of degrees of longitude for a given distance in
	 * meters.
	 * 
	 * @param meters
	 *            distance in meters
	 * @param latitude
	 *            the latitude at which the calculation should be performed
	 * @return longitude degrees
	 */
	public static double getLongitudeDistance(final int meters,
			final double latitude) {
		return (meters * 360)
				/ (2 * Math.PI * LocationConstants.EQUATORIALRADIUS * Math
						.cos(Math.toRadians(latitude)));
	}

	/**
	 * Calculate the amount of degrees of longitude for a given distance in
	 * meters.
	 * 
	 * @param meters
	 *            distance in meters
	 * @param latitude
	 *            the latitude at which the calculation should be performed
	 * @return longitude degrees
	 */
	public static double getLongitudeDistance(final int meters,
			final int latitude) {
		return (meters * 360)
				/ (2 * Math.PI * LocationConstants.EQUATORIALRADIUS * Math
						.cos(Math.toRadians(intToDouble(latitude))));
	}

	/**
	 * Gets the circular region approximation on the earth surface using
	 * haversine formula using the latitude and longitude as the center point.
	 * <p>
	 * Given a center point, this method returns a points around the given
	 * center.
	 * </p>
	 * 
	 * @param numberOfPoints
	 *            the number of points used to estimate the circular region
	 * @return an array of LatLon representing the points that estimate the
	 *         circular region
	 */
	public LatLongCoordinate[] getCircularRegion(final int numberOfPoints,
			final double radius) {
		if (radius >= LocationConstants.HALF_EARTH_CIRCUMFERENCE_KM) {
			final LatLongCoordinate[] points = new LatLongCoordinate[5];
			points[0] = new LatLongCoordinate(-90.0, -180.0);
			points[1] = new LatLongCoordinate(90.0, -180.0);
			points[2] = new LatLongCoordinate(90.0, 180.0);
			points[3] = new LatLongCoordinate(-90.0, 180.0);
			points[4] = points[0];
			return points;
		}
		// plus one to add closing point
		final LatLongCoordinate[] points = new LatLongCoordinate[numberOfPoints + 1];
		for (int i = 0; i < 360; i += (360 / numberOfPoints)) {
			points[i] = getPointOnGreatCircle(radius, i);
		}
		points[numberOfPoints] = points[0];
		return points;
	}

	/**
	 * Returns the LatLongCoordinate which lies the given fraction of the way
	 * between the origin LatLongCoordinate and the destination
	 * LatLongCoordinate.using <a
	 * href="http://en.wikipedia.org/wiki/Slerp">Slerp</a>
	 * <p>
	 * Interpolate from this point to the given point with the provided
	 * fraction.
	 * </p>
	 * 
	 * @param from
	 *            The LatLng from which to start.
	 * @param to
	 *            The LatLng toward which to travel.
	 * @param fraction
	 *            A fraction of the distance to travel.
	 * @return The interpolated LatLng.
	 */
	public LatLongCoordinate interpolate(final LatLongCoordinate to,
			final double fraction) {
		final double fromLat = toRadians(latitude);
		final double fromLng = toRadians(longitude);
		final double toLat = toRadians(to.latitude);
		final double toLng = toRadians(to.longitude);
		final double cosFromLat = cos(fromLat);
		final double cosToLat = cos(toLat);

		// Computes Spherical interpolation coefficients.
		final double angle = computeAngleBetween(this, to);
		final double sinAngle = sin(angle);
		if (sinAngle < 1E-6) {
			return this;
		}
		final double a = sin((1 - fraction) * angle) / sinAngle;
		final double b = sin(fraction * angle) / sinAngle;

		// Converts from polar to vector and interpolate.
		final double x = a * cosFromLat * cos(fromLng) + b * cosToLat
				* cos(toLng);
		final double y = a * cosFromLat * sin(fromLng) + b * cosToLat
				* sin(toLng);
		final double z = a * sin(fromLat) + b * sin(toLat);

		// Converts interpolated vector back to polar.
		final double lat = atan2(z, sqrt(x * x + y * y));
		final double lng = atan2(y, x);
		return new LatLongCoordinate(toDegrees(lat), toDegrees(lng));
	}

	/**
	 * Converts a coordinate from microdegrees to degrees.
	 * 
	 * @param coordinate
	 *            the coordinate in microdegrees.
	 * @return the coordinate in degrees.
	 */
	private static double intToDouble(final int coordinate) {
		return coordinate / 1000000;
	}

	/**
	 * Get the datum.
	 * 
	 * @return the datum.
	 * @since 1.1
	 */
	public Datum getDatum() {
		return datum;
	}

	/**
	 * Returns the heading from one LatLng to another LatLng. Headings are
	 * expressed in degrees clockwise from North within the range [-180,180).
	 * 
	 * @return The heading in degrees clockwise from north.
	 * @see toto.device.location.LocationUtils#getDirection(float)
	 * @see #getBearing(LatLongCoordinate)
	 */
	public double getHeading(final LatLongCoordinate to) {
		// http://williams.best.vwh.net/avform.htm#Crs
		final double fromLat = Math.toRadians(latitude);
		final double fromLng = Math.toRadians(longitude);
		final double toLat = Math.toRadians(to.latitude);
		final double toLng = Math.toRadians(to.longitude);
		final double dLng = toLng - fromLng;
		final double heading = Math.atan2(
				Math.sin(dLng) * Math.cos(toLat),
				Math.cos(fromLat) * Math.sin(toLat) - Math.sin(fromLat)
						* Math.cos(toLat) * Math.cos(dLng));
		return MathUtils.wrap(Math.toDegrees(heading), -180, 180);
	}

	/**
	 * Returns the length of the given path, in meters, on Earth.
	 * 
	 * @param path
	 * @return length in meters or 0 if the length of <code>path</code> is 0.
	 */
	public static double getLength(final List<LatLongCoordinate> path) {
		if (path.size() < 2) {
			return 0;
		}
		double length = 0;
		final LatLongCoordinate prev = path.get(0);
		double prevLat = Math.toRadians(prev.latitude);
		double prevLng = Math.toRadians(prev.longitude);
		for (final LatLongCoordinate point : path) {
			final double lat = Math.toRadians(point.latitude);
			final double lng = Math.toRadians(point.longitude);
			length += distanceRadians(prevLat, prevLng, lat, lng);
			prevLat = lat;
			prevLng = lng;
		}
		return length * LocationConstants.EARTH_RADIUS_METERS;
	}

	/**
	 * Returns the LatLongCoordinate resulting from moving a distance from an
	 * origin in the specified heading (expressed in degrees clockwise from
	 * north).
	 * 
	 * @param from
	 *            The LatLng from which to start.
	 * @param distance
	 *            The distance to travel.
	 * @param heading
	 *            The heading in degrees clockwise from north.
	 * @see #getHeading(LatLongCoordinate)
	 */
	public LatLongCoordinate getOffset(final LatLongCoordinate from,
			double distance, double heading) {
		distance /= LocationConstants.EARTH_RADIUS_METERS;
		heading = toRadians(heading);
		// http://williams.best.vwh.net/avform.htm#LL
		final double fromLat = toRadians(from.latitude);
		final double fromLng = toRadians(from.longitude);
		final double cosDistance = cos(distance);
		final double sinDistance = sin(distance);
		final double sinFromLat = sin(fromLat);
		final double cosFromLat = cos(fromLat);
		final double sinLat = cosDistance * sinFromLat + sinDistance
				* cosFromLat * cos(heading);
		final double dLng = atan2(sinDistance * cosFromLat * sin(heading),
				cosDistance - sinFromLat * sinLat);
		return new LatLongCoordinate(toDegrees(asin(sinLat)), toDegrees(fromLng
				+ dLng));
	}

	/**
	 * Returns distance on the unit sphere; the arguments are in radians.
	 */
	private static double distanceRadians(final double lat1, final double lng1,
			final double lat2, final double lng2) {
		return arcHav(havDistance(lat1, lat2, lng1 - lng2));
	}

	/**
	 * Returns hav() of distance from (lat1, lng1) to (lat2, lng2) on the unit
	 * sphere.
	 */
	private static double havDistance(final double lat1, final double lat2,
			final double dLng) {
		return hav(lat1 - lat2) + hav(dLng) * Math.cos(lat1) * Math.cos(lat2);
	}

	/**
	 * Returns haversine(angle-in-radians). hav(x) == (1 - cos(x)) / 2 == sin(x
	 * / 2)^2.
	 */
	private static double hav(final double x) {
		final double sinHalf = Math.sin(x * 0.5);
		return sinHalf * sinHalf;
	}

	private static double arcHav(final double x) {
		return 2 * Math.asin(Math.sqrt(x));
	}

	/**
	 * Returns the angle between two LatLngs, in radians. This is the same as
	 * the distance on the unit sphere.
	 */
	private static double computeAngleBetween(final LatLongCoordinate from,
			final LatLongCoordinate to) {
		return distanceRadians(toRadians(from.latitude),
				toRadians(from.longitude), toRadians(to.latitude),
				toRadians(to.longitude));
	}

	/**
	 * Returns the signed area of a triangle which has North Pole as a vertex.
	 * Formula derived from
	 * "Area of a spherical triangle given two edges and the included angle" as
	 * per "Spherical Trigonometry" by Todhunter, page 71, section 103, point 2.
	 * See http://books.google.com/books?id=3uBHAAAAIAAJ&pg=PA71 The arguments
	 * named "tan" are tan((pi/2 - latitude)/2).
	 */
	private static double polarTriangleArea(final double tan1,
			final double lng1, final double tan2, final double lng2) {
		final double deltaLng = lng1 - lng2;
		final double t = tan1 * tan2;
		return 2 * atan2(t * sin(deltaLng), 1 + t * cos(deltaLng));
	}

	/**
	 * Computes whether this point lies inside the specified polygon. The
	 * polygon is always considered closed, regardless of whether the last point
	 * equals the first or not. Inside is defined as not containing the South
	 * Pole -- the South Pole is always outside. The polygon is formed of great
	 * circle segments if geodesic is true, and of rhumb (loxodromic) segments
	 * otherwise.
	 * 
	 * @return true if this point is contained in <code>polygon</code>
	 */
	public boolean containsLocation(final List<LatLongCoordinate> polygon,
			final boolean geodesic) {
		final int size = polygon.size();
		if (size == 0) {
			return false;
		}
		final double lat3 = toRadians(latitude);
		final double lng3 = toRadians(longitude);
		final LatLongCoordinate prev = polygon.get(size - 1);
		double lat1 = toRadians(prev.latitude);
		double lng1 = toRadians(prev.longitude);
		int nIntersect = 0;
		for (final LatLongCoordinate point2 : polygon) {
			final double dLng3 = MathUtils.wrap(lng3 - lng1, -Math.PI, Math.PI);
			// Special case: point equal to vertex is inside.
			if (lat3 == lat1 && dLng3 == 0) {
				return true;
			}
			final double lat2 = toRadians(point2.latitude);
			final double lng2 = toRadians(point2.longitude);
			// Offset longitudes by -lng1.
			if (intersects(lat1, lat2,
					MathUtils.wrap(lng2 - lng1, -Math.PI, Math.PI), lat3,
					dLng3, geodesic)) {
				++nIntersect;
			}
			lat1 = lat2;
			lng1 = lng2;
		}
		return (nIntersect & 1) != 0;
	}

	/**
	 * Returns the signed area of a closed path on a sphere of given radius. The
	 * computed area uses the same units as the radius squared. Used by
	 * SphericalUtilTest.
	 * 
	 */
	private static double getSignedArea(final List<LatLongCoordinate> path,
			final double radius) {
		final int size = path.size();
		if (size < 3) {
			return 0;
		}
		double total = 0;
		final LatLongCoordinate prev = path.get(size - 1);
		double prevTanLat = Math
				.tan((Math.PI / 2 - toRadians(prev.latitude)) / 2);
		double prevLng = toRadians(prev.longitude);
		// For each edge, accumulate the signed area of the triangle formed by
		// the North Pole
		// and that edge ("polar triangle").
		for (final LatLongCoordinate point : path) {
			final double tanLat = Math
					.tan((Math.PI / 2 - toRadians(point.latitude)) / 2);
			final double lng = toRadians(point.longitude);
			total += polarTriangleArea(tanLat, lng, prevTanLat, prevLng);
			prevTanLat = tanLat;
			prevLng = lng;
		}
		return total * (radius * radius);
	}

	/**
	 * Returns the signed area of a closed path on Earth. The sign of the area
	 * may be used to determine the orientation of the path. "inside" is the
	 * surface that does not contain the South Pole.
	 * 
	 * @param path
	 *            A closed path.
	 * @return The loop's area in square meters.
	 */
	public static double getSignedArea(final List<LatLongCoordinate> path) {
		return getSignedArea(path, LocationConstants.EARTH_RADIUS_METERS);
	}

	/**
	 * Returns the area of a closed path on Earth.
	 * 
	 * @param path
	 *            A closed path.
	 * @return The path's area in square meters.
	 */
	public static double getArea(final List<LatLongCoordinate> path) {
		return Math.abs(getSignedArea(path));
	}

	/**
	 * Computes whether the vertical segment (lat3, lng3) to South Pole
	 * intersects the segment (lat1, lng1) to (lat2, lng2). Longitudes are
	 * offset by -lng1; the implicit lng1 becomes 0.
	 */
	private static boolean intersects(final double lat1, final double lat2,
			final double lng2, final double lat3, final double lng3,
			final boolean geodesic) {
		// Both ends on the same side of lng3.
		if ((lng3 >= 0 && lng3 >= lng2) || (lng3 < 0 && lng3 < lng2)) {
			return false;
		}
		// Point is South Pole.
		if (lat3 <= -PI / 2) {
			return false;
		}
		// Any segment end is a pole.
		if (lat1 <= -PI / 2 || lat2 <= -PI / 2 || lat1 >= PI / 2
				|| lat2 >= PI / 2) {
			return false;
		}
		if (lng2 <= -PI) {
			return false;
		}
		final double linearLat = (lat1 * (lng2 - lng3) + lat2 * lng3) / lng2;
		// Northern hemisphere and point under lat-lng line.
		if (lat1 >= 0 && lat2 >= 0 && lat3 < linearLat) {
			return false;
		}
		// Southern hemisphere and point above lat-lng line.
		if (lat1 <= 0 && lat2 <= 0 && lat3 >= linearLat) {
			return true;
		}
		// North Pole.
		if (lat3 >= PI / 2) {
			return true;
		}
		// Compare lat3 with latitude on the GC/Rhumb segment corresponding to
		// lng3.
		// Compare through a strictly-increasing function (tan() or mercator())
		// as convenient.
		return geodesic ? Math.tan(lat3) >= tanLatGC(lat1, lat2, lng2, lng3)
				: mercator(lat3) >= mercatorLatRhumb(lat1, lat2, lng2, lng3);
	}

	/**
	 * Returns tan(latitude-at-lng3) on the great circle (lat1, lng1) to (lat2,
	 * lng2). lng1==0. See http://williams.best.vwh.net/avform.htm .
	 */
	private static double tanLatGC(final double lat1, final double lat2,
			final double lng2, final double lng3) {
		return (Math.tan(lat1) * sin(lng2 - lng3) + Math.tan(lat2) * sin(lng3))
				/ sin(lng2);
	}

	/**
	 * Returns mercator(latitude-at-lng3) on the Rhumb line (lat1, lng1) to
	 * (lat2, lng2). lng1==0.
	 */
	private static double mercatorLatRhumb(final double lat1,
			final double lat2, final double lng2, final double lng3) {
		return (mercator(lat1) * (lng2 - lng3) + mercator(lat2) * lng3) / lng2;
	}

	private static double mercator(final double lat) {
		return log(tan(lat * 0.5 + PI / 4));
	}

	/**
	 * Returns latitude from mercator Y.
	 */
	private static double inverseMercator(final double y) {
		return 2 * Math.atan(Math.exp(y)) - PI / 2;
	}

	private static double sinSumFromHav(final double x, final double y) {
		final double a = sqrt(x * (1 - x));
		final double b = sqrt(y * (1 - y));
		return 2 * (a + b - 2 * (a * y + b * x));
	}

	// Returns hav(asin(x)).
	static double havFromSin(final double x) {
		final double x2 = x * x;
		return x2 / (1 + sqrt(1 - x2)) * .5;
	}

	// Given h==hav(x), returns sin(abs(x)).
	static double sinFromHav(final double h) {
		return 2 * sqrt(h * (1 - h));
	}

	/**
	 * Returns sin(initial bearing from (lat1,lng1) to (lat3,lng3) minus initial
	 * bearing from (lat1, lng1) to (lat2,lng2)).
	 */
	private static double sinDeltaBearing(final double lat1, final double lng1,
			final double lat2, final double lng2, final double lat3,
			final double lng3) {
		final double sinLat1 = sin(lat1);
		final double cosLat2 = cos(lat2);
		final double cosLat3 = cos(lat3);
		final double lat31 = lat3 - lat1;
		final double lng31 = lng3 - lng1;
		final double lat21 = lat2 - lat1;
		final double lng21 = lng2 - lng1;
		final double a = sin(lng31) * cosLat3;
		final double c = sin(lng21) * cosLat2;
		final double b = sin(lat31) + 2 * sinLat1 * cosLat3 * hav(lng31);
		final double d = sin(lat21) + 2 * sinLat1 * cosLat2 * hav(lng21);
		final double denom = (a * a + b * b) * (c * c + d * d);
		return denom <= 0 ? 1 : (a * d - b * c) / sqrt(denom);
	}

	private static boolean isOnSegmentGC(final double lat1, final double lng1,
			final double lat2, final double lng2, final double lat3,
			final double lng3, final double havTolerance) {
		final double havDist13 = havDistance(lat1, lat3, lng1 - lng3);
		if (havDist13 <= havTolerance) {
			return true;
		}
		final double havDist23 = havDistance(lat2, lat3, lng2 - lng3);
		if (havDist23 <= havTolerance) {
			return true;
		}
		final double sinBearing = sinDeltaBearing(lat1, lng1, lat2, lng2, lat3,
				lng3);
		final double sinDist13 = sinFromHav(havDist13);
		final double havCrossTrack = havFromSin(sinDist13 * sinBearing);
		if (havCrossTrack > havTolerance) {
			return false;
		}
		final double havDist12 = havDistance(lat1, lat2, lng1 - lng2);
		final double term = havDist12 + havCrossTrack * (1 - 2 * havDist12);
		if (havDist13 > term || havDist23 > term) {
			return false;
		}
		if (havDist12 < 0.74) {
			return true;
		}
		final double cosCrossTrack = 1 - 2 * havCrossTrack;
		final double havAlongTrack13 = (havDist13 - havCrossTrack)
				/ cosCrossTrack;
		final double havAlongTrack23 = (havDist23 - havCrossTrack)
				/ cosCrossTrack;
		final double sinSumAlongTrack = sinSumFromHav(havAlongTrack13,
				havAlongTrack23);
		return sinSumAlongTrack > 0; // Compare with half-circle == PI using
										// sign of sin().
	}

}
