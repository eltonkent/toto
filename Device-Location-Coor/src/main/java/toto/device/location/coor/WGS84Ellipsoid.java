package toto.device.location.coor;



class WGS84Ellipsoid extends Ellipsoid {

	/**
	 * Static reference of this ellipsoid.
	 */
	private static WGS84Ellipsoid ref = null;

	/**
	 * Create an object defining a WGS84 reference ellipsoid.
	 * 
	 * @since 1.1
	 */
	private WGS84Ellipsoid() {
		super(6378137, 6356752.3142);
	}

	static WGS84Ellipsoid getInstance() {
		if (ref == null) {
			ref = new WGS84Ellipsoid();
		}
		return ref;
	}
}