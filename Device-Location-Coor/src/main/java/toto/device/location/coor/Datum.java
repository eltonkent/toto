package toto.device.location.coor;


abstract class Datum {

	protected String name;

	protected Ellipsoid ellipsoid;

	/**
	 * Translation along the x-axis for use in 7-parameter Helmert
	 * transformations. This value should be used to convert a co-ordinate in a
	 * given datum to the WGS84 datum.
	 */
	protected double dx;

	/**
	 * Translation along the y-axis for use in 7-parameter Helmert
	 * transformations. This value should be used to convert a co-ordinate in a
	 * given datum to the WGS84 datum.
	 */
	protected double dy;

	/**
	 * Translation along the z-axis for use in 7-parameter Helmert
	 * transformations. This value should be used to convert a co-ordinate in a
	 * given datum to the WGS84 datum.
	 */
	protected double dz;

	/**
	 * Scale factor for use in 7-parameter Helmert transformations. This value
	 * should be used to convert a co-ordinate in a given datum to the WGS84
	 * datum.
	 */
	protected double ds;

	/**
	 * Rotation about the x-axis for use in 7-parameter Helmert transformations.
	 * This value should be used to convert a co-ordinate in a given datum to
	 * the WGS84 datum.
	 */
	protected double rx;

	/**
	 * Rotation about the y-axis for use in 7-parameter Helmert transformations.
	 * This value should be used to convert a co-ordinate in a given datum to
	 * the WGS84 datum.
	 */
	protected double ry;

	/**
	 * Rotation about the z-axis for use in 7-parameter Helmert transformations.
	 * This value should be used to convert a co-ordinate in a given datum to
	 * the WGS84 datum.
	 */
	protected double rz;

	/**
	 * Get the name of this Datum.
	 * 
	 * @return the name of this Datum.
	 * @since 1.1
	 */
	String getName() {
		return name;
	}

	/**
	 * Get the reference ellipsoid associated with this Datum.
	 * 
	 * @return the reference ellipsoid associated with this Datum.
	 * @since 1.1
	 */
	Ellipsoid getReferenceEllipsoid() {
		return ellipsoid;
	}

	/**
	 * 
	 * 
	 * @return the ds
	 */
	double getDs() {
		return ds;
	}

	/**
	 * 
	 * 
	 * @return the dx
	 */
	double getDx() {
		return dx;
	}

	/**
	 * 
	 * 
	 * @return the dy
	 */
	double getDy() {
		return dy;
	}

	/**
	 * 
	 * 
	 * @return the dz
	 */
	double getDz() {
		return dz;
	}

	/**
	 * 
	 * 
	 * @return the rx
	 */
	double getRx() {
		return rx;
	}

	/**
	 * 
	 * 
	 * @return the ry
	 */
	double getRy() {
		return ry;
	}

	/**
	 * 
	 * 
	 * @return the rz
	 * @since 1.1
	 */
	double getRz() {
		return rz;
	}

	/**
	 * Get a String representation of the parameters of a Datum object.
	 * 
	 * @return a String representation of the parameters of a Datum object.
	 */
	public String toString() {
		return getName() + " " + ellipsoid.toString() + " dx=" + dx + " dy="
				+ dy + " dz=" + dz + " ds=" + ds + " rx=" + rx + " ry=" + ry
				+ " rz=" + rz;
	}

}
