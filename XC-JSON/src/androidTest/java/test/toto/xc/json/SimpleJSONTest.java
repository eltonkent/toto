package test.toto.xc.json;

import test.toto.ToToTestCase;
import toto.xc.json.Json;

public class SimpleJSONTest extends ToToTestCase {

	public void testJSON(){
		String json="{\"name\":\"ToTo\",\"age\":3,\"address\":\"Mobifluence Interactive\"}";
		TestDAO dao=new Json().fromJson(json, TestDAO.class);
		assertNotNull(dao);
		assertEquals("ToTo", dao.getName());
		assertEquals(3, dao.getAge());
		assertEquals("Mobifluence Interactive", dao.getAddress());
	}
}
