package test.toto.xc.json;

import java.io.Serializable;

public class TestDAO implements Serializable{
	String name;
	int age;
	String address;
	
	public final String getName() {
		return name;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final int getAge() {
		return age;
	}

	public final void setAge(int age) {
		this.age = age;
	}

	public final String getAddress() {
		return address;
	}

	public final void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "TestDAO [name=" + name + ", age=" + age + ", address="
				+ address + "]";
	}

}
