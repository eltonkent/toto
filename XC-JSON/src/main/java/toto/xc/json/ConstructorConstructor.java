package toto.xc.json;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import toto.util.collections.map.LinkedTreeMap;

/**
 * Returns a function that can construct an instance of a requested type.
 */
final class ConstructorConstructor {
	private final Map<Type, InstanceCreator<?>> instanceCreators;

	ConstructorConstructor(final Map<Type, InstanceCreator<?>> instanceCreators) {
		this.instanceCreators = instanceCreators;
	}

	<T> ObjectConstructor<T> get(final TypeToken<T> typeToken) {
		final Type type = typeToken.getType();
		final Class<? super T> rawType = typeToken.getRawType();

		// first try an instance creator

		@SuppressWarnings("unchecked")
		// types must agree
		final InstanceCreator<T> typeCreator = (InstanceCreator<T>) instanceCreators
				.get(type);
		if (typeCreator != null) {
			return new ObjectConstructor<T>() {
				public T construct() {
					return typeCreator.createInstance(type);
				}
			};
		}

		// Next try raw type match for instance creators
		@SuppressWarnings("unchecked")
		// types must agree
		final InstanceCreator<T> rawTypeCreator = (InstanceCreator<T>) instanceCreators
				.get(rawType);
		if (rawTypeCreator != null) {
			return new ObjectConstructor<T>() {
				public T construct() {
					return rawTypeCreator.createInstance(type);
				}
			};
		}

		final ObjectConstructor<T> defaultConstructor = newDefaultConstructor(rawType);
		if (defaultConstructor != null) {
			return defaultConstructor;
		}

		final ObjectConstructor<T> defaultImplementation = newDefaultImplementationConstructor(
				type, rawType);
		if (defaultImplementation != null) {
			return defaultImplementation;
		}

		// finally try unsafe
		return newUnsafeAllocator(type, rawType);
	}

	private <T> ObjectConstructor<T> newDefaultConstructor(
			final Class<? super T> rawType) {
		try {
			final Constructor<? super T> constructor = rawType
					.getDeclaredConstructor();
			if (!constructor.isAccessible()) {
				constructor.setAccessible(true);
			}
			return new ObjectConstructor<T>() {
				@SuppressWarnings("unchecked")
				// T is the same raw type as is requested
				public T construct() {
					try {
						final Object[] args = null;
						return (T) constructor.newInstance(args);
					} catch (final InstantiationException e) {
						// TODO: JsonParseException ?
						throw new RuntimeException("Failed to invoke "
								+ constructor + " with no args", e);
					} catch (final InvocationTargetException e) {
						// TODO: don't wrap if cause is unchecked!
						// TODO: JsonParseException ?
						throw new RuntimeException("Failed to invoke "
								+ constructor + " with no args",
								e.getTargetException());
					} catch (final IllegalAccessException e) {
						throw new AssertionError(e);
					}
				}
			};
		} catch (final NoSuchMethodException e) {
			return null;
		}
	}

	/**
	 * Constructors for common interface types like Map and List and their
	 * subytpes.
	 */
	@SuppressWarnings("unchecked")
	// use runtime checks to guarantee that 'T' is what it is
	private <T> ObjectConstructor<T> newDefaultImplementationConstructor(
			final Type type, final Class<? super T> rawType) {
		if (Collection.class.isAssignableFrom(rawType)) {
			if (SortedSet.class.isAssignableFrom(rawType)) {
				return new ObjectConstructor<T>() {
					public T construct() {
						return (T) new TreeSet<Object>();
					}
				};
			} else if (EnumSet.class.isAssignableFrom(rawType)) {
				return new ObjectConstructor<T>() {
					@SuppressWarnings("rawtypes")
					public T construct() {
						if (type instanceof ParameterizedType) {
							final Type elementType = ((ParameterizedType) type)
									.getActualTypeArguments()[0];
							if (elementType instanceof Class) {
								return (T) EnumSet.noneOf((Class) elementType);
							} else {
								throw new JsonIOException(
										"Invalid EnumSet type: "
												+ type.toString());
							}
						} else {
							throw new JsonIOException("Invalid EnumSet type: "
									+ type.toString());
						}
					}
				};
			} else if (Set.class.isAssignableFrom(rawType)) {
				return new ObjectConstructor<T>() {
					public T construct() {
						return (T) new LinkedHashSet<Object>();
					}
				};
			} else if (Queue.class.isAssignableFrom(rawType)) {
				return new ObjectConstructor<T>() {
					public T construct() {
						return (T) new LinkedList<Object>();
					}
				};
			} else {
				return new ObjectConstructor<T>() {
					public T construct() {
						return (T) new ArrayList<Object>();
					}
				};
			}
		}

		if (Map.class.isAssignableFrom(rawType)) {
			if (SortedMap.class.isAssignableFrom(rawType)) {
				return new ObjectConstructor<T>() {
					public T construct() {
						return (T) new TreeMap<Object, Object>();
					}
				};
			} else if (type instanceof ParameterizedType
					&& !(String.class.isAssignableFrom(TypeToken
							.get(((ParameterizedType) type)
									.getActualTypeArguments()[0]).getRawType()))) {
				return new ObjectConstructor<T>() {
					public T construct() {
						return (T) new LinkedHashMap<Object, Object>();
					}
				};
			} else {
				return new ObjectConstructor<T>() {
					public T construct() {
						return (T) new LinkedTreeMap<String, Object>();
					}
				};
			}
		}

		return null;
	}

	private <T> ObjectConstructor<T> newUnsafeAllocator(final Type type,
			final Class<? super T> rawType) {
		return new ObjectConstructor<T>() {
			private final UnsafeAllocator unsafeAllocator = UnsafeAllocator
					.create();

			@SuppressWarnings("unchecked")
			public T construct() {
				try {
					final Object newInstance = unsafeAllocator
							.newInstance(rawType);
					return (T) newInstance;
				} catch (final Exception e) {
					throw new RuntimeException(
							("Unable to invoke no-args constructor for " + type
									+ ". " + "Register an InstanceCreator with Json for this type may fix this problem."),
							e);
				}
			}
		};
	}

	@Override
	public String toString() {
		return instanceCreators.toString();
	}
}
