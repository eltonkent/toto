package toto.xc.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import toto.util.collections.map.LinkedTreeMap;
import toto.xc.json.io.JsonReader;
import toto.xc.json.io.JsonToken;
import toto.xc.json.io.JsonWriter;

/**
 * Adapts types whose static type is only 'Object'. Uses getClass() on
 * serialization and a primitive/Map/List on deserialization.
 */
final class ObjectTypeAdapter extends TypeAdapter<Object> {
	public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
		@SuppressWarnings("unchecked")
		public <T> TypeAdapter<T> create(final Json gson,
				final TypeToken<T> type) {
			if (type.getRawType() == Object.class) {
				return (TypeAdapter<T>) new ObjectTypeAdapter(gson);
			}
			return null;
		}
	};

	private final Json gson;

	private ObjectTypeAdapter(final Json gson) {
		this.gson = gson;
	}

	@Override
	public Object read(final JsonReader in) throws IOException {
		final JsonToken token = in.peek();
		switch (token) {
		case BEGIN_ARRAY:
			final List<Object> list = new ArrayList<Object>();
			in.beginArray();
			while (in.hasNext()) {
				list.add(read(in));
			}
			in.endArray();
			return list;

		case BEGIN_OBJECT:
			final Map<String, Object> map = new LinkedTreeMap<String, Object>();
			in.beginObject();
			while (in.hasNext()) {
				map.put(in.nextName(), read(in));
			}
			in.endObject();
			return map;

		case STRING:
			return in.nextString();

		case NUMBER:
			return in.nextDouble();

		case BOOLEAN:
			return in.nextBoolean();

		case NULL:
			in.nextNull();
			return null;

		default:
			throw new IllegalStateException();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void write(final JsonWriter out, final Object value)
			throws IOException {
		if (value == null) {
			out.nullValue();
			return;
		}

		final TypeAdapter<Object> typeAdapter = (TypeAdapter<Object>) gson
				.getAdapter(value.getClass());
		if (typeAdapter instanceof ObjectTypeAdapter) {
			out.beginObject();
			out.endObject();
			return;
		}

		typeAdapter.write(out, value);
	}
}
