package toto.xc.json;

/**
 * This exception is raised when Json attempts to read (or write) a malformed
 * JSON element.
 * 
 * @author Inderjeet Singh
 * @author Joel Leitch
 */
public final class JsonSyntaxException extends JsonParseException {

	private static final long serialVersionUID = 1L;

	public JsonSyntaxException(final String msg) {
		super(msg);
	}

	public JsonSyntaxException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Creates exception with the specified cause. Consider using
	 * {@link #JsonSyntaxException(String, Throwable)} instead if you can
	 * describe what actually happened.
	 * 
	 * @param cause
	 *            root exception that caused this exception to be thrown.
	 */
	public JsonSyntaxException(final Throwable cause) {
		super(cause);
	}
}
