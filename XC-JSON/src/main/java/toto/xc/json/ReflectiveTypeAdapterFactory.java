package toto.xc.json;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

import toto.xc.annotations.SerializedName;
import toto.xc.json.io.JsonReader;
import toto.xc.json.io.JsonToken;
import toto.xc.json.io.JsonWriter;

/**
 * Type adapter that reflects over the fields and methods of a class.
 */
final class ReflectiveTypeAdapterFactory implements TypeAdapterFactory {
	private final ConstructorConstructor constructorConstructor;
	private final FieldNamingStrategy fieldNamingPolicy;
	private final Excluder excluder;

	ReflectiveTypeAdapterFactory(
			final ConstructorConstructor constructorConstructor,
			final FieldNamingStrategy fieldNamingPolicy, final Excluder excluder) {
		this.constructorConstructor = constructorConstructor;
		this.fieldNamingPolicy = fieldNamingPolicy;
		this.excluder = excluder;
	}

	boolean excludeField(final Field f, final boolean serialize) {
		return !excluder.excludeClass(f.getType(), serialize)
				&& !excluder.excludeField(f, serialize);
	}

	private String getFieldName(final Field f) {
		final SerializedName serializedName = f
				.getAnnotation(SerializedName.class);
		return serializedName == null ? fieldNamingPolicy.translateName(f)
				: serializedName.value();
	}

	public <T> TypeAdapter<T> create(final Json gson, final TypeToken<T> type) {
		final Class<? super T> raw = type.getRawType();

		if (!Object.class.isAssignableFrom(raw)) {
			return null; // it's a primitive!
		}

		final ObjectConstructor<T> constructor = constructorConstructor
				.get(type);
		return new Adapter<T>(constructor, getBoundFields(gson, type, raw));
	}

	private ReflectiveTypeAdapterFactory.BoundField createBoundField(
			final Json context, final Field field, final String name,
			final TypeToken<?> fieldType, final boolean serialize,
			final boolean deserialize) {
		final boolean isPrimitive = Primitives.isPrimitive(fieldType
				.getRawType());

		// special casing primitives here saves ~5% on Android...
		return new ReflectiveTypeAdapterFactory.BoundField(name, serialize,
				deserialize) {
			final TypeAdapter<?> typeAdapter = context.getAdapter(fieldType);

			@SuppressWarnings({ "unchecked", "rawtypes" })
			// the type adapter and field type always agree
			@Override
			void write(final JsonWriter writer, final Object value)
					throws IOException, IllegalAccessException {
				final Object fieldValue = field.get(value);
				final TypeAdapter t = new TypeAdapterRuntimeTypeWrapper(
						context, this.typeAdapter, fieldType.getType());
				t.write(writer, fieldValue);
			}

			@Override
			void read(final JsonReader reader, final Object value)
					throws IOException, IllegalAccessException {
				final Object fieldValue = typeAdapter.read(reader);
				if (fieldValue != null || !isPrimitive) {
					field.set(value, fieldValue);
				}
			}
		};
	}

	private Map<String, BoundField> getBoundFields(final Json context,
			TypeToken<?> type, Class<?> raw) {
		final Map<String, BoundField> result = new LinkedHashMap<String, BoundField>();
		if (raw.isInterface()) {
			return result;
		}

		final Type declaredType = type.getType();
		while (raw != Object.class) {
			final Field[] fields = raw.getDeclaredFields();
			for (final Field field : fields) {
				final boolean serialize = excludeField(field, true);
				final boolean deserialize = excludeField(field, false);
				if (!serialize && !deserialize) {
					continue;
				}
				field.setAccessible(true);
				final Type fieldType = $Json$Types.resolve(type.getType(), raw,
						field.getGenericType());
				final BoundField boundField = createBoundField(context, field,
						getFieldName(field), TypeToken.get(fieldType),
						serialize, deserialize);
				final BoundField previous = result.put(boundField.name,
						boundField);
				if (previous != null) {
					throw new IllegalArgumentException(declaredType
							+ " declares multiple JSON fields named "
							+ previous.name);
				}
			}
			type = TypeToken.get($Json$Types.resolve(type.getType(), raw,
					raw.getGenericSuperclass()));
			raw = type.getRawType();
		}
		return result;
	}

	static abstract class BoundField {
		final String name;
		final boolean serialized;
		final boolean deserialized;

		protected BoundField(final String name, final boolean serialized,
				final boolean deserialized) {
			this.name = name;
			this.serialized = serialized;
			this.deserialized = deserialized;
		}

		abstract void write(JsonWriter writer, Object value)
				throws IOException, IllegalAccessException;

		abstract void read(JsonReader reader, Object value) throws IOException,
				IllegalAccessException;
	}

	static final class Adapter<T> extends TypeAdapter<T> {
		private final ObjectConstructor<T> constructor;
		private final Map<String, BoundField> boundFields;

		private Adapter(final ObjectConstructor<T> constructor,
				final Map<String, BoundField> boundFields) {
			this.constructor = constructor;
			this.boundFields = boundFields;
		}

		@Override
		public T read(final JsonReader in) throws IOException {
			if (in.peek() == JsonToken.NULL) {
				in.nextNull();
				return null;
			}

			final T instance = constructor.construct();

			try {
				in.beginObject();
				while (in.hasNext()) {
					final String name = in.nextName();
					final BoundField field = boundFields.get(name);
					if (field == null || !field.deserialized) {
						in.skipValue();
					} else {
						field.read(in, instance);
					}
				}
			} catch (final IllegalStateException e) {
				throw new JsonSyntaxException(e);
			} catch (final IllegalAccessException e) {
				throw new AssertionError(e);
			}
			in.endObject();
			return instance;
		}

		@Override
		public void write(final JsonWriter out, final T value)
				throws IOException {
			if (value == null) {
				out.nullValue();
				return;
			}

			out.beginObject();
			try {
				for (final BoundField boundField : boundFields.values()) {
					if (boundField.serialized) {
						out.name(boundField.name);
						boundField.write(out, value);
					}
				}
			} catch (final IllegalAccessException e) {
				throw new AssertionError();
			}
			out.endObject();
		}
	}
}
