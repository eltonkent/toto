package toto.xc.json;

import java.io.IOException;

import toto.xc.json.io.JsonReader;

/**
 * Internal-only APIs of JsonReader available only to other classes in Json.
 */
public abstract class JsonReaderInternalAccess {
	public static JsonReaderInternalAccess INSTANCE;

	/**
	 * Changes the type of the current property name token to a string value.
	 */
	public abstract void promoteNameToValue(JsonReader reader)
			throws IOException;
}
