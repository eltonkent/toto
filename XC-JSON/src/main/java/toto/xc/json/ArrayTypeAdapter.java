package toto.xc.json;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import toto.xc.json.io.JsonReader;
import toto.xc.json.io.JsonToken;
import toto.xc.json.io.JsonWriter;

/**
 * Adapt an array of objects.
 */
final class ArrayTypeAdapter<E> extends TypeAdapter<Object> {

	public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public <T> TypeAdapter<T> create(final Json gson,
				final TypeToken<T> typeToken) {
			final Type type = typeToken.getType();
			if (!(type instanceof GenericArrayType || type instanceof Class
					&& ((Class<?>) type).isArray())) {
				return null;
			}

			final Type componentType = $Json$Types.getArrayComponentType(type);
			final TypeAdapter<?> componentTypeAdapter = gson
					.getAdapter(TypeToken.get(componentType));
			return new ArrayTypeAdapter(gson, componentTypeAdapter,
					$Json$Types.getRawType(componentType));
		}
	};

	private final Class<E> componentType;
	private final TypeAdapter<E> componentTypeAdapter;

	ArrayTypeAdapter(final Json context,
			final TypeAdapter<E> componentTypeAdapter,
			final Class<E> componentType) {
		this.componentTypeAdapter = new TypeAdapterRuntimeTypeWrapper<E>(
				context, componentTypeAdapter, componentType);
		this.componentType = componentType;
	}

	public Object read(final JsonReader in) throws IOException {
		if (in.peek() == JsonToken.NULL) {
			in.nextNull();
			return null;
		}

		final List<E> list = new ArrayList<E>();
		in.beginArray();
		while (in.hasNext()) {
			final E instance = componentTypeAdapter.read(in);
			list.add(instance);
		}
		in.endArray();
		final Object array = Array.newInstance(componentType, list.size());
		for (int i = 0; i < list.size(); i++) {
			Array.set(array, i, list.get(i));
		}
		return array;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void write(final JsonWriter out, final Object array)
			throws IOException {
		if (array == null) {
			out.nullValue();
			return;
		}

		out.beginArray();
		for (int i = 0, length = Array.getLength(array); i < length; i++) {
			final E value = (E) Array.get(array, i);
			componentTypeAdapter.write(out, value);
		}
		out.endArray();
	}
}
