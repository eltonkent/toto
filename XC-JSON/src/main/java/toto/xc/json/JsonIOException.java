package toto.xc.json;

/**
 * This exception is raised when Json was unable to read an input stream or
 * write to one.
 * 
 * @author Inderjeet Singh
 * @author Joel Leitch
 */
public final class JsonIOException extends JsonParseException {
	private static final long serialVersionUID = 1L;

	public JsonIOException(final String msg) {
		super(msg);
	}

	public JsonIOException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Creates exception with the specified cause. Consider using
	 * {@link #JsonIOException(String, Throwable)} instead if you can describe
	 * what happened.
	 * 
	 * @param cause
	 *            root exception that caused this exception to be thrown.
	 */
	public JsonIOException(final Throwable cause) {
		super(cause);
	}
}
