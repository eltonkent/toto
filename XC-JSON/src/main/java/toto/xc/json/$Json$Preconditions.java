package toto.xc.json;

/**
 * A simple utility class used to check method Preconditions.
 * 
 * <pre>
 * public long divideBy(long value) {
 * 	Preconditions.checkArgument(value != 0);
 * 	return this.value / value;
 * }
 * </pre>
 * 
 * @author Inderjeet Singh
 * @author Joel Leitch
 */
final class $Json$Preconditions {
	static <T> T checkNotNull(final T obj) {
		if (obj == null) {
			throw new NullPointerException();
		}
		return obj;
	}

	static void checkArgument(final boolean condition) {
		if (!condition) {
			throw new IllegalArgumentException();
		}
	}
}
