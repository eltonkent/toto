package toto.xc.json;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import toto.xc.json.io.JsonReader;
import toto.xc.json.io.JsonToken;
import toto.xc.json.io.JsonWriter;

/**
 * Adapter for Time. Although this class appears stateless, it is not.
 * DateFormat captures its time zone and locale when it is created, which gives
 * this class state. DateFormat isn't thread safe either, so this class has to
 * synchronize its read and write methods.
 */
final class TimeTypeAdapter extends TypeAdapter<Time> {
	public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
		@SuppressWarnings("unchecked")
		// we use a runtime check to make sure the 'T's equal
		public <T> TypeAdapter<T> create(final Json gson,
				final TypeToken<T> typeToken) {
			return typeToken.getRawType() == Time.class ? (TypeAdapter<T>) new TimeTypeAdapter()
					: null;
		}
	};

	private final DateFormat format = new SimpleDateFormat("hh:mm:ss a");

	@Override
	public synchronized Time read(final JsonReader in) throws IOException {
		if (in.peek() == JsonToken.NULL) {
			in.nextNull();
			return null;
		}
		try {
			final Date date = format.parse(in.nextString());
			return new Time(date.getTime());
		} catch (final ParseException e) {
			throw new JsonSyntaxException(e);
		}
	}

	@Override
	public synchronized void write(final JsonWriter out, final Time value)
			throws IOException {
		out.value(value == null ? null : format.format(value));
	}
}
