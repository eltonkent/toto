package toto.xc.json;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import toto.xc.json.io.JsonReader;
import toto.xc.json.io.JsonToken;
import toto.xc.json.io.MalformedJsonException;

/**
 * A parser to parse Json into a parse tree of {@link JsonElement}s
 * 
 * @author Inderjeet Singh
 * @author Joel Leitch
 * @since 1.3
 */
public final class JsonParser {

	/**
	 * Parses the specified JSON string into a parse tree
	 * 
	 * @param json
	 *            JSON text
	 * @return a parse tree of {@link JsonElement}s corresponding to the
	 *         specified JSON
	 * @throws JsonParseException
	 *             if the specified text is not valid JSON
	 * @since 1.3
	 */
	public JsonElement parse(final String json) throws JsonSyntaxException {
		return parse(new StringReader(json));
	}

	/**
	 * Parses the specified JSON string into a parse tree
	 * 
	 * @param json
	 *            JSON text
	 * @return a parse tree of {@link JsonElement}s corresponding to the
	 *         specified JSON
	 * @throws JsonParseException
	 *             if the specified text is not valid JSON
	 * @since 1.3
	 */
	public JsonElement parse(final Reader json) throws JsonIOException,
			JsonSyntaxException {
		try {
			final JsonReader jsonReader = new JsonReader(json);
			final JsonElement element = parse(jsonReader);
			if (!element.isJsonNull()
					&& jsonReader.peek() != JsonToken.END_DOCUMENT) {
				throw new JsonSyntaxException(
						"Did not consume the entire document.");
			}
			return element;
		} catch (final MalformedJsonException e) {
			throw new JsonSyntaxException(e);
		} catch (final IOException e) {
			throw new JsonIOException(e);
		} catch (final NumberFormatException e) {
			throw new JsonSyntaxException(e);
		}
	}

	/**
	 * Returns the next value from the JSON stream as a parse tree.
	 * 
	 * @throws JsonParseException
	 *             if there is an IOException or if the specified text is not
	 *             valid JSON
	 * @since 1.6
	 */
	public JsonElement parse(final JsonReader json) throws JsonIOException,
			JsonSyntaxException {
		final boolean lenient = json.isLenient();
		json.setLenient(true);
		try {
			return Streams.parse(json);
		} catch (final StackOverflowError e) {
			throw new JsonParseException("Failed parsing JSON source: " + json
					+ " to Json", e);
		} catch (final OutOfMemoryError e) {
			throw new JsonParseException("Failed parsing JSON source: " + json
					+ " to Json", e);
		} finally {
			json.setLenient(lenient);
		}
	}
}
