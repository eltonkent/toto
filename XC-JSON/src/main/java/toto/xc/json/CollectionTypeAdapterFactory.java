package toto.xc.json;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;

import toto.xc.json.io.JsonReader;
import toto.xc.json.io.JsonToken;
import toto.xc.json.io.JsonWriter;

/**
 * Adapt a homogeneous collection of objects.
 */
final class CollectionTypeAdapterFactory implements TypeAdapterFactory {
	private final ConstructorConstructor constructorConstructor;

	public CollectionTypeAdapterFactory(
			final ConstructorConstructor constructorConstructor) {
		this.constructorConstructor = constructorConstructor;
	}

	public <T> TypeAdapter<T> create(final Json gson,
			final TypeToken<T> typeToken) {
		final Type type = typeToken.getType();

		final Class<? super T> rawType = typeToken.getRawType();
		if (!Collection.class.isAssignableFrom(rawType)) {
			return null;
		}

		final Type elementType = $Json$Types.getCollectionElementType(type,
				rawType);
		final TypeAdapter<?> elementTypeAdapter = gson.getAdapter(TypeToken
				.get(elementType));
		final ObjectConstructor<T> constructor = constructorConstructor
				.get(typeToken);

		@SuppressWarnings({ "unchecked", "rawtypes" })
		// create() doesn't define a type parameter
		final TypeAdapter<T> result = new Adapter(gson, elementType,
				elementTypeAdapter, constructor);
		return result;
	}

	private static final class Adapter<E> extends TypeAdapter<Collection<E>> {
		private final TypeAdapter<E> elementTypeAdapter;
		private final ObjectConstructor<? extends Collection<E>> constructor;

		public Adapter(final Json context, final Type elementType,
				final TypeAdapter<E> elementTypeAdapter,
				final ObjectConstructor<? extends Collection<E>> constructor) {
			this.elementTypeAdapter = new TypeAdapterRuntimeTypeWrapper<E>(
					context, elementTypeAdapter, elementType);
			this.constructor = constructor;
		}

		public Collection<E> read(final JsonReader in) throws IOException {
			if (in.peek() == JsonToken.NULL) {
				in.nextNull();
				return null;
			}

			final Collection<E> collection = constructor.construct();
			in.beginArray();
			while (in.hasNext()) {
				final E instance = elementTypeAdapter.read(in);
				collection.add(instance);
			}
			in.endArray();
			return collection;
		}

		public void write(final JsonWriter out, final Collection<E> collection)
				throws IOException {
			if (collection == null) {
				out.nullValue();
				return;
			}

			out.beginArray();
			for (final E element : collection) {
				elementTypeAdapter.write(out, element);
			}
			out.endArray();
		}
	}
}
