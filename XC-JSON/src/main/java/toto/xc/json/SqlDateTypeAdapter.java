package toto.xc.json;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import toto.xc.json.io.JsonReader;
import toto.xc.json.io.JsonToken;
import toto.xc.json.io.JsonWriter;

/**
 * Adapter for java.sql.Date. Although this class appears stateless, it is not.
 * DateFormat captures its time zone and locale when it is created, which gives
 * this class state. DateFormat isn't thread safe either, so this class has to
 * synchronize its read and write methods.
 */
final class SqlDateTypeAdapter extends TypeAdapter<java.sql.Date> {
	static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
		@SuppressWarnings("unchecked")
		// we use a runtime check to make sure the 'T's equal
		public <T> TypeAdapter<T> create(final Json gson,
				final TypeToken<T> typeToken) {
			return typeToken.getRawType() == java.sql.Date.class ? (TypeAdapter<T>) new SqlDateTypeAdapter()
					: null;
		}
	};

	private final DateFormat format = new SimpleDateFormat("MMM d, yyyy");

	@Override
	public synchronized java.sql.Date read(final JsonReader in)
			throws IOException {
		if (in.peek() == JsonToken.NULL) {
			in.nextNull();
			return null;
		}
		try {
			final long utilDate = format.parse(in.nextString()).getTime();
			return new java.sql.Date(utilDate);
		} catch (final ParseException e) {
			throw new JsonSyntaxException(e);
		}
	}

	@Override
	public synchronized void write(final JsonWriter out,
			final java.sql.Date value) throws IOException {
		out.value(value == null ? null : format.format(value));
	}
}
