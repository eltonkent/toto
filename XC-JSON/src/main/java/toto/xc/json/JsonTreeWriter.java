package toto.xc.json;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import toto.xc.json.io.JsonWriter;

/**
 * This writer creates a JsonElement.
 */
final class JsonTreeWriter extends JsonWriter {
	private static final Writer UNWRITABLE_WRITER = new Writer() {
		@Override
		public void write(final char[] buffer, final int offset,
				final int counter) {
			throw new AssertionError();
		}

		@Override
		public void flush() throws IOException {
			throw new AssertionError();
		}

		@Override
		public void close() throws IOException {
			throw new AssertionError();
		}
	};
	/**
	 * Added to the top of the stack when this writer is closed to cause
	 * following ops to fail.
	 */
	private static final JsonPrimitive SENTINEL_CLOSED = new JsonPrimitive(
			"closed");

	/**
	 * The JsonElements and JsonArrays under modification, outermost to
	 * innermost.
	 */
	private final List<JsonElement> stack = new ArrayList<JsonElement>();

	/**
	 * The name for the next JSON object value. If non-null, the top of the
	 * stack is a JsonObject.
	 */
	private String pendingName;

	/** the JSON element constructed by this writer. */
	private JsonElement product = JsonNull.INSTANCE; // TODO: is this really
														// what we want?;

	public JsonTreeWriter() {
		super(UNWRITABLE_WRITER);
	}

	/**
	 * Returns the top level object produced by this writer.
	 */
	JsonElement get() {
		if (!stack.isEmpty()) {
			throw new IllegalStateException(
					"Expected one JSON element but was " + stack);
		}
		return product;
	}

	private JsonElement peek() {
		return stack.get(stack.size() - 1);
	}

	private void put(final JsonElement value) {
		if (pendingName != null) {
			if (!value.isJsonNull() || getSerializeNulls()) {
				final JsonObject object = (JsonObject) peek();
				object.add(pendingName, value);
			}
			pendingName = null;
		} else if (stack.isEmpty()) {
			product = value;
		} else {
			final JsonElement element = peek();
			if (element instanceof JsonArray) {
				((JsonArray) element).add(value);
			} else {
				throw new IllegalStateException();
			}
		}
	}

	@Override
	public JsonWriter beginArray() throws IOException {
		final JsonArray array = new JsonArray();
		put(array);
		stack.add(array);
		return this;
	}

	@Override
	public JsonWriter endArray() throws IOException {
		if (stack.isEmpty() || pendingName != null) {
			throw new IllegalStateException();
		}
		final JsonElement element = peek();
		if (element instanceof JsonArray) {
			stack.remove(stack.size() - 1);
			return this;
		}
		throw new IllegalStateException();
	}

	@Override
	public JsonWriter beginObject() throws IOException {
		final JsonObject object = new JsonObject();
		put(object);
		stack.add(object);
		return this;
	}

	@Override
	public JsonWriter endObject() throws IOException {
		if (stack.isEmpty() || pendingName != null) {
			throw new IllegalStateException();
		}
		final JsonElement element = peek();
		if (element instanceof JsonObject) {
			stack.remove(stack.size() - 1);
			return this;
		}
		throw new IllegalStateException();
	}

	@Override
	public JsonWriter name(final String name) throws IOException {
		if (stack.isEmpty() || pendingName != null) {
			throw new IllegalStateException();
		}
		final JsonElement element = peek();
		if (element instanceof JsonObject) {
			pendingName = name;
			return this;
		}
		throw new IllegalStateException();
	}

	@Override
	public JsonWriter value(final String value) throws IOException {
		if (value == null) {
			return nullValue();
		}
		put(new JsonPrimitive(value));
		return this;
	}

	@Override
	public JsonWriter nullValue() throws IOException {
		put(JsonNull.INSTANCE);
		return this;
	}

	@Override
	public JsonWriter value(final boolean value) throws IOException {
		put(new JsonPrimitive(value));
		return this;
	}

	@Override
	public JsonWriter value(final double value) throws IOException {
		if (!isLenient() && (Double.isNaN(value) || Double.isInfinite(value))) {
			throw new IllegalArgumentException(
					"JSON forbids NaN and infinities: " + value);
		}
		put(new JsonPrimitive(value));
		return this;
	}

	@Override
	public JsonWriter value(final long value) throws IOException {
		put(new JsonPrimitive(value));
		return this;
	}

	@Override
	public JsonWriter value(final Number value) throws IOException {
		if (value == null) {
			return nullValue();
		}

		if (!isLenient()) {
			final double d = value.doubleValue();
			if (Double.isNaN(d) || Double.isInfinite(d)) {
				throw new IllegalArgumentException(
						"JSON forbids NaN and infinities: " + value);
			}
		}

		put(new JsonPrimitive(value));
		return this;
	}

	@Override
	public void flush() throws IOException {
	}

	@Override
	public void close() throws IOException {
		if (!stack.isEmpty()) {
			throw new IOException("Incomplete document");
		}
		stack.add(SENTINEL_CLOSED);
	}
}
