package toto.xc.json;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import toto.xc.annotations.Expose;
import toto.xc.annotations.Since;
import toto.xc.annotations.Until;
import toto.xc.json.io.JsonReader;
import toto.xc.json.io.JsonWriter;

/**
 * This class selects which fields and types to omit. It is configurable,
 * supporting version attributes {@link Since} and {@link Until}, modifiers,
 * synthetic fields, anonymous and local classes, inner classes, and fields with
 * the {@link Expose} annotation.
 * 
 * <p>
 * This class is a type adapter factory; types that are excluded will be adapted
 * to null. It may delegate to another type adapter if only one direction is
 * excluded.
 * 
 * @author Joel Leitch
 * @author Jesse Wilson
 */
final class Excluder implements TypeAdapterFactory, Cloneable {
	private static final double IGNORE_VERSIONS = -1.0d;
	public static final Excluder DEFAULT = new Excluder();

	private double version = IGNORE_VERSIONS;
	private int modifiers = Modifier.TRANSIENT | Modifier.STATIC;
	private boolean serializeInnerClasses = true;
	private boolean requireExpose;
	private List<ExclusionStrategy> serializationStrategies = Collections
			.emptyList();
	private List<ExclusionStrategy> deserializationStrategies = Collections
			.emptyList();

	@Override
	protected Excluder clone() {
		try {
			return (Excluder) super.clone();
		} catch (final CloneNotSupportedException e) {
			throw new AssertionError();
		}
	}

	Excluder withVersion(final double ignoreVersionsAfter) {
		final Excluder result = clone();
		result.version = ignoreVersionsAfter;
		return result;
	}

	Excluder withModifiers(final int... modifiers) {
		final Excluder result = clone();
		result.modifiers = 0;
		for (final int modifier : modifiers) {
			result.modifiers |= modifier;
		}
		return result;
	}

	Excluder disableInnerClassSerialization() {
		final Excluder result = clone();
		result.serializeInnerClasses = false;
		return result;
	}

	Excluder excludeFieldsWithoutExposeAnnotation() {
		final Excluder result = clone();
		result.requireExpose = true;
		return result;
	}

	Excluder withExclusionStrategy(final ExclusionStrategy exclusionStrategy,
			final boolean serialization, final boolean deserialization) {
		final Excluder result = clone();
		if (serialization) {
			result.serializationStrategies = new ArrayList<ExclusionStrategy>(
					serializationStrategies);
			result.serializationStrategies.add(exclusionStrategy);
		}
		if (deserialization) {
			result.deserializationStrategies = new ArrayList<ExclusionStrategy>(
					deserializationStrategies);
			result.deserializationStrategies.add(exclusionStrategy);
		}
		return result;
	}

	public <T> TypeAdapter<T> create(final Json gson, final TypeToken<T> type) {
		final Class<?> rawType = type.getRawType();
		final boolean skipSerialize = excludeClass(rawType, true);
		final boolean skipDeserialize = excludeClass(rawType, false);

		if (!skipSerialize && !skipDeserialize) {
			return null;
		}

		return new TypeAdapter<T>() {
			/**
			 * The delegate is lazily created because it may not be needed, and
			 * creating it may fail.
			 */
			private TypeAdapter<T> delegate;

			@Override
			public T read(final JsonReader in) throws IOException {
				if (skipDeserialize) {
					in.skipValue();
					return null;
				}
				return delegate().read(in);
			}

			@Override
			public void write(final JsonWriter out, final T value)
					throws IOException {
				if (skipSerialize) {
					out.nullValue();
					return;
				}
				delegate().write(out, value);
			}

			private TypeAdapter<T> delegate() {
				final TypeAdapter<T> d = delegate;
				return d != null ? d : (delegate = gson.getDelegateAdapter(
						Excluder.this, type));
			}
		};
	}

	boolean excludeField(final Field field, final boolean serialize) {
		if ((modifiers & field.getModifiers()) != 0) {
			return true;
		}

		if (version != Excluder.IGNORE_VERSIONS
				&& !isValidVersion(field.getAnnotation(Since.class),
						field.getAnnotation(Until.class))) {
			return true;
		}

		if (field.isSynthetic()) {
			return true;
		}

		if (requireExpose) {
			final Expose annotation = field.getAnnotation(Expose.class);
			if (annotation == null
					|| (serialize ? !annotation.serialize() : !annotation
							.deserialize())) {
				return true;
			}
		}

		if (!serializeInnerClasses && isInnerClass(field.getType())) {
			return true;
		}

		if (isAnonymousOrLocal(field.getType())) {
			return true;
		}

		final List<ExclusionStrategy> list = serialize ? serializationStrategies
				: deserializationStrategies;
		if (!list.isEmpty()) {
			final FieldAttributes fieldAttributes = new FieldAttributes(field);
			for (final ExclusionStrategy exclusionStrategy : list) {
				if (exclusionStrategy.shouldSkipField(fieldAttributes)) {
					return true;
				}
			}
		}

		return false;
	}

	boolean excludeClass(final Class<?> clazz, final boolean serialize) {
		if (version != Excluder.IGNORE_VERSIONS
				&& !isValidVersion(clazz.getAnnotation(Since.class),
						clazz.getAnnotation(Until.class))) {
			return true;
		}

		if (!serializeInnerClasses && isInnerClass(clazz)) {
			return true;
		}

		if (isAnonymousOrLocal(clazz)) {
			return true;
		}

		final List<ExclusionStrategy> list = serialize ? serializationStrategies
				: deserializationStrategies;
		for (final ExclusionStrategy exclusionStrategy : list) {
			if (exclusionStrategy.shouldSkipClass(clazz)) {
				return true;
			}
		}

		return false;
	}

	private boolean isAnonymousOrLocal(final Class<?> clazz) {
		return !Enum.class.isAssignableFrom(clazz)
				&& (clazz.isAnonymousClass() || clazz.isLocalClass());
	}

	private boolean isInnerClass(final Class<?> clazz) {
		return clazz.isMemberClass() && !isStatic(clazz);
	}

	private boolean isStatic(final Class<?> clazz) {
		return (clazz.getModifiers() & Modifier.STATIC) != 0;
	}

	private boolean isValidVersion(final Since since, final Until until) {
		return isValidSince(since) && isValidUntil(until);
	}

	private boolean isValidSince(final Since annotation) {
		if (annotation != null) {
			final double annotationVersion = annotation.value();
			if (annotationVersion > version) {
				return false;
			}
		}
		return true;
	}

	private boolean isValidUntil(final Until annotation) {
		if (annotation != null) {
			final double annotationVersion = annotation.value();
			if (annotationVersion <= version) {
				return false;
			}
		}
		return true;
	}
}
