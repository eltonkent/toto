package toto.util.collections.cache;

/**
 * Listener to check if a entry is added/removed to the Cache.
 * 
 * @author Mobifluence Interactive
 * 
 * @param <K>
 * @param <V>
 */
public interface CacheListener<K, V> {
	public void onEntryRemoved(K k);

	public void onEntryAdded(K k, V value);
}
