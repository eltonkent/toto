package toto.util.collections.cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * single entry LRU cache
 * <p>
 * <div> A cache that holds strong references to a limited number of values.
 * Each time a value is accessed, it is moved to the head of a queue. When a
 * value is added to a full cache, the value at the end of that queue is evicted
 * and may become eligible for garbage collection. </div>
 * </p>
 * 
 * @param <T>
 */
public class LRUCache<K, V> implements Cache<K, V> {
	private final LinkedHashMap<K, V> map;
	private int size;
	private final int maxSize;
	private int putCount;
	private int createCount;
	private int evictionCount;
	private int hitCount;
	private int missCount;

	public LRUCache(final int maxSize) {
		if (maxSize <= 0) {
			throw new IllegalArgumentException("maxSize <= 0");
		}
		this.maxSize = maxSize;
		this.map = new LinkedHashMap(0, 0.75F, true);
	}

	public boolean isEmpty() {
		return map.isEmpty();
	}

	public boolean contains(final Object key) {
		return map.containsKey(key);
	}

	public final V fetch(final K key) {
		if (key == null)
			throw new NullPointerException("key == null");
		V mapValue;
		synchronized (this) {
			mapValue = this.map.get(key);
			if (mapValue != null) {
				this.hitCount += 1;
				return mapValue;
			}
			this.missCount += 1;
		}

		final V createdValue = create(key);
		if (createdValue == null) {
			return null;
		}

		synchronized (this) {
			this.createCount += 1;
			mapValue = this.map.put(key, createdValue);

			if (mapValue != null) {
				this.map.put(key, mapValue);
			} else
				this.size += safeSizeOf(key, createdValue);

		}

		if (mapValue != null) {
			entryRemoved(false, key, createdValue, mapValue);
			return mapValue;
		}
		trimToSize(this.maxSize);
		return createdValue;
	}

	public final boolean cache(final K key, final V value) {
		boolean ret = false;
		if ((key == null) || (value == null))
			throw new NullPointerException("key == null || value == null");
		V previous;
		synchronized (this) {
			this.putCount += 1;
			this.size += safeSizeOf(key, value);
			previous = this.map.put(key, value);

			if (previous != null) {
				this.size -= safeSizeOf(key, previous);
			}
		}

		if (previous != null) {
			ret = true;
			entryRemoved(false, key, previous, value);
		}
		trimToSize(this.maxSize);
		return ret;
	}

	public void trimToSize(final int maxSize) {
		while (true) {
			K key;
			V value;
			synchronized (this) {
				if ((this.size < 0)
						|| ((this.map.isEmpty()) && (this.size != 0))) {
					throw new IllegalStateException(getClass().getName()
							+ ".sizeOf() is reporting inconsistent results!");
				}

				if ((this.size > maxSize) && (this.map.isEmpty())) {
					break;
				}
				final Map.Entry<K, V> toEvict = this.map.entrySet().iterator()
						.next();
				key = toEvict.getKey();
				value = toEvict.getValue();
				this.map.remove(key);
				this.size -= safeSizeOf(key, value);
				this.evictionCount += 1;
			}

			entryRemoved(true, key, value, null);
		}
	}

	public final boolean remove(final K key) {
		if (key == null)
			throw new NullPointerException("key == null");
		V previous;
		synchronized (this) {
			previous = this.map.remove(key);
			if (previous != null) {
				this.size -= safeSizeOf(key, previous);
			}
		}

		if (previous != null) {
			entryRemoved(false, key, previous, null);
		}
		if (listener != null && previous != null) {
			listener.onEntryRemoved(key);
		}
		return true;
	}

	protected void entryRemoved(final boolean evicted, final Object key,
			final V oldValue, final V newValue) {
	}

	protected V create(final Object key) {
		return null;
	}

	private int safeSizeOf(final K key, final V value) {
		final int result = sizeOf(key, value);
		if (result < 0) {
			throw new IllegalStateException("Negative size: " + key + "="
					+ value);
		}
		return result;
	}

	protected int sizeOf(final K key, final V value) {
		return 1;
	}

	public final void evictAll() {
		trimToSize(-1);
	}

	public final synchronized int size() {
		return this.size;
	}

	public final synchronized int maxSize() {
		return this.maxSize;
	}

	public final synchronized int hitCount() {
		return this.hitCount;
	}

	public final synchronized int missCount() {
		return this.missCount;
	}

	public final synchronized int createCount() {
		return this.createCount;
	}

	public final synchronized int putCount() {
		return this.putCount;
	}

	public final synchronized int evictionCount() {
		return this.evictionCount;
	}

	public final synchronized Map<K, V> snapshot() {
		return new LinkedHashMap(this.map);
	}

	public final synchronized String toString() {
		final int accesses = this.hitCount + this.missCount;
		final int hitPercent = accesses != 0 ? 100 * this.hitCount / accesses
				: 0;
		return String.format(
				"LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]",
				new Object[] { Integer.valueOf(this.maxSize),
						Integer.valueOf(this.hitCount),
						Integer.valueOf(this.missCount),
						Integer.valueOf(hitPercent) });
	}

	private CacheListener<K, V> listener;

	@Override
	public CacheListener<K, V> getCacheListener() {
		return listener;
	}

	@Override
	public void setCacheListener(final CacheListener<K, V> cacheListener) {
		listener = cacheListener;
	}
}