package toto.util.collections.cache;


import toto.util.collections.Fetchable;

/**
 * The <code>Cache</code> interface is used to represent a cache that will store
 * key value pairs.
 * <p>
 * <div>The cache exposes only several methods to ensure that implementations
 * can focus on performance concerns rather than how to manage the cached
 * values. <br/>
 * For robust off-heap cache implementations, refer to the
 * {@link toto.cache.TCache} class. </div>
 * </p>
 * 
 */
public interface Cache<K, V> extends Fetchable<K, V> {

	/**
	 * This method is used to determine if the cache is empty. This is done by
	 * checking if there are any elements in the cache. If anything has been
	 * cached this will return false.
	 * 
	 * @return this returns true if the cache is empty
	 */
	boolean isEmpty();

	/**
	 * This method is used to insert a key value mapping in to the cache. The
	 * value can later be retrieved or removed from the cache if desired. If the
	 * value associated with the key is null then nothing is stored within the
	 * cache.
	 * 
	 * @param key
	 *            this is the key to cache the provided value to
	 * @param value
	 *            this is the value that is to be cached
	 * @return true if the caching operation was successful.
	 */
	boolean cache(K key, V value);

	/**
	 * This is used to exclusively take the value mapped to the specified key
	 * from the cache. Invoking this is effectively removing the value from the
	 * cache.
	 * 
	 * @param key
	 *            this is the key to acquire the cache value with
	 * 
	 * @return true if the removal was successful.
	 */
	boolean remove(K key);

	/**
	 * This method is used to get the value from the cache that is mapped to the
	 * specified key. If there is no value mapped to the specified key then this
	 * method will return a null.
	 * 
	 * @param key
	 *            this is the key to acquire the cache value with
	 * 
	 * @return this returns the value mapped to the specified key
	 */
	V fetch(K key);

	/**
	 * This is used to determine whether the specified key exists with in the
	 * cache. Typically this can be done using the fetch method, which will
	 * acquire the object.
	 * 
	 * @param key
	 *            this is the key to check within this segment
	 * 
	 * @return true if the specified key is within the cache
	 */
	boolean contains(K key);

	CacheListener<K, V> getCacheListener();

	void setCacheListener(final CacheListener<K, V> cacheListener);
}