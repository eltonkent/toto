package toto.util.collections.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * LFU cache implementation.
 * <ul>
 * <li>
 * Frequency list is stored as an array with no next/prev pointers between
 * nodes: looping over the array should be faster and more CPU-cache friendly
 * than using an ad-hoc linked-pointers structure.</li>
 * <li>
 * The max frequency is capped at the cache size to avoid creating more and more
 * frequency list entries, and all elements residing in the max frequency entry
 * are re-positioned in the frequency entry linked set in order to put most
 * recently accessed elements ahead of less recently ones, which will be
 * collected sooner.</li>
 * <li>
 * The eviction factor determines how many elements (more specifically, the
 * percentage of) will be evicted.</li>
 * </ul>
 * As a consequence, this cache runs in *amortized* O(1) time (considering the
 * worst case of having the lowest frequency at 0 and having to evict all
 * elements).
 * 
 */
public class LFUCache<K, V> implements Cache<K, V> {

	private final Map<K, CacheNode<K, V>> cache;
	private final LinkedHashSet[] frequencyList;
	private int lowestFrequency;
	private final int maxFrequency;
	//
	private final int maxCacheSize;
	private final float evictionFactor;

	public LFUCache(final int maxCacheSize, final float evictionFactor) {
		if (evictionFactor <= 0 || evictionFactor >= 1) {
			throw new IllegalArgumentException(
					"Eviction factor must be greater than 0 and lesser than or equal to 1");
		}
		this.cache = new HashMap<K, CacheNode<K, V>>(maxCacheSize);
		this.frequencyList = new LinkedHashSet[maxCacheSize];
		this.lowestFrequency = 0;
		this.maxFrequency = maxCacheSize - 1;
		this.maxCacheSize = maxCacheSize;
		this.evictionFactor = evictionFactor;
		initFrequencyList();
	}

	public boolean cache(final K k, final V v) {
		CacheNode<K, V> currentNode = cache.get(k);
		if (currentNode == null) {
			if (cache.size() == maxCacheSize) {
				doEviction();
			}
			final LinkedHashSet<CacheNode<K, V>> nodes = frequencyList[0];
			currentNode = new CacheNode(k, v, 0);
			nodes.add(currentNode);
			cache.put(k, currentNode);
			lowestFrequency = 0;
			return true;
		} else {
			currentNode.v = v;
		}
		return false;
	}

	public V fetch(final K k) {
		final CacheNode<K, V> currentNode = cache.get(k);
		if (currentNode != null) {
			final int currentFrequency = currentNode.frequency;
			if (currentFrequency < maxFrequency) {
				final int nextFrequency = currentFrequency + 1;
				final LinkedHashSet<CacheNode<K, V>> currentNodes = frequencyList[currentFrequency];
				final LinkedHashSet<CacheNode<K, V>> newNodes = frequencyList[nextFrequency];
				moveToNextFrequency(currentNode, nextFrequency, currentNodes,
						newNodes);
				cache.put(k, currentNode);
				if (lowestFrequency == currentFrequency
						&& currentNodes.isEmpty()) {
					lowestFrequency = nextFrequency;
				}
			} else {
				// Hybrid with LRU: put most recently accessed ahead of others:
				final LinkedHashSet<CacheNode<K, V>> nodes = frequencyList[currentFrequency];
				nodes.remove(currentNode);
				nodes.add(currentNode);
			}
			return currentNode.v;
		} else {
			return null;
		}
	}

	public boolean contains(final K key) {
		final V val = fetch(key);
		return val != null;
	}

	public boolean remove(final K k) {
		final CacheNode<K, V> currentNode = cache.remove(k);
		if (currentNode != null) {
			final LinkedHashSet<CacheNode<K, V>> nodes = frequencyList[currentNode.frequency];
			nodes.remove(currentNode);
			if (lowestFrequency == currentNode.frequency) {
				findNextLowestFrequency();
			}
			if (currentNode.v != null) {
				if (listener != null) {
					listener.onEntryRemoved(k);
				}
				return true;
			}
		}
		return false;
	}

	public int frequencyOf(final K k) {
		final CacheNode<K, V> node = cache.get(k);
		if (node != null) {
			return node.frequency + 1;
		} else {
			return 0;
		}
	}

	public void clear() {
		for (int i = 0; i <= maxFrequency; i++) {
			frequencyList[i].clear();
		}
		cache.clear();
		lowestFrequency = 0;
	}

	public int size() {
		return cache.size();
	}

	private void initFrequencyList() {
		for (int i = 0; i <= maxFrequency; i++) {
			frequencyList[i] = new LinkedHashSet<CacheNode<K, V>>();
		}
	}

	private void doEviction() {
		int currentlyDeleted = 0;
		final float target = maxCacheSize * evictionFactor;
		while (currentlyDeleted < target) {
			final LinkedHashSet<CacheNode<K, V>> nodes = frequencyList[lowestFrequency];
			if (nodes.isEmpty()) {
				throw new IllegalStateException(
						"Lowest frequency constraint violated!");
			} else {
				final Iterator<CacheNode<K, V>> it = nodes.iterator();
				while (it.hasNext() && currentlyDeleted++ < target) {
					final CacheNode<K, V> node = it.next();
					it.remove();
					cache.remove(node.k);
				}
				if (!it.hasNext()) {
					findNextLowestFrequency();
				}
			}
		}
	}

	private void moveToNextFrequency(final CacheNode<K, V> currentNode,
			final int nextFrequency,
			final LinkedHashSet<CacheNode<K, V>> currentNodes,
			final LinkedHashSet<CacheNode<K, V>> newNodes) {
		currentNodes.remove(currentNode);
		newNodes.add(currentNode);
		currentNode.frequency = nextFrequency;
	}

	private void findNextLowestFrequency() {
		while (lowestFrequency <= maxFrequency
				&& frequencyList[lowestFrequency].isEmpty()) {
			lowestFrequency++;
		}
		if (lowestFrequency > maxFrequency) {
			lowestFrequency = 0;
		}
	}

	private static class CacheNode<Key, Value> {

		public final Key k;
		public Value v;
		public int frequency;

		public CacheNode(final Key k, final Value v, final int frequency) {
			this.k = k;
			this.v = v;
			this.frequency = frequency;
		}

	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	private CacheListener<K, V> listener;

	@Override
	public CacheListener<K, V> getCacheListener() {
		return listener;
	}

	@Override
	public void setCacheListener(final CacheListener<K, V> cacheListener) {
		listener = cacheListener;
	}
}