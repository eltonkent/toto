package toto.util.collections.cache;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;

/**
 * The <code>WeakCache</code> object is an implementation of a cache that holds
 * on to cached items only if the key remains in memory. This is effectively
 * like a concurrent hash map with weak keys, it ensures that multiple threads
 * can concurrently access weak hash maps in a way that lowers contention for
 * the locks used.
 * 
 */
public class WeakCache<K, V> implements Cache<K, V> {

	/**
	 * This is used to store a list of segments for the cache.
	 */
	private SegmentList list;

	/**
	 * Constructor for the <code>WeakCache</code> object. This is used to create
	 * a cache that stores values in such a way that when the key is garbage
	 * collected the value is removed from the map. This is similar to the
	 * concurrent hash map.
	 */
	public WeakCache() {
		this(10);
	}

	/**
	 * Constructor for the <code>WeakCache</code> object. This is used to create
	 * a cache that stores values in such a way that when the key is garbage
	 * collected the value is removed from the map. This is similar to the
	 * concurrent hash map.
	 * 
	 * @param size
	 *            this is the number of segments within the cache
	 */
	public WeakCache(final int size) {
		this.list = new SegmentList(size);
	}

	public boolean isEmpty() {
		for (final Segment segment : list) {
			if (!segment.isEmpty()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This method is used to insert a key value mapping in to the cache. The
	 * value can later be retrieved or removed from the cache if desired. If the
	 * value associated with the key is null then nothing is stored within the
	 * cache.
	 * 
	 * @param key
	 *            this is the key to cache the provided value to
	 * @param value
	 *            this is the value that is to be cached
	 */
	public boolean cache(final K key, final V value) {
		map(key).cache(key, value);
		return true;
	}

	/**
	 * This is used to exclusively take the value mapped to the specified key
	 * from the cache. Invoking this is effectively removing the value from the
	 * cache.
	 * 
	 * @param key
	 *            this is the key to acquire the cache value with
	 * 
	 * @return this returns the value mapped to the specified key
	 */
	public boolean remove(final K key) {
		final V v = map(key).take(key);
		if (v != null) {

			if (listener != null)
				listener.onEntryRemoved(key);
			return true;
		}
		return false;
	}

	/**
	 * This method is used to get the value from the cache that is mapped to the
	 * specified key. If there is no value mapped to the specified key then this
	 * method will return a null.
	 * 
	 * @param key
	 *            this is the key to acquire the cache value with
	 * 
	 * @return this returns the value mapped to the specified key
	 */
	public V fetch(final K key) {
		return map(key).fetch(key);
	}

	/**
	 * This is used to determine whether the specified key exists with in the
	 * cache. Typically this can be done using the fetch method, which will
	 * acquire the object.
	 * 
	 * @param key
	 *            this is the key to check within this segment
	 * 
	 * @return true if the specified key is within the cache
	 */
	public boolean contains(final K key) {
		return map(key).contains(key);
	}

	/**
	 * This method is used to acquire a <code>Segment</code> using the keys has
	 * code. This method effectively uses the hash to find a specific segment
	 * within the fixed list of segments.
	 * 
	 * @param key
	 *            this is the key used to acquire the segment
	 * 
	 * @return this returns the segment used to get acquire value
	 */
	private Segment map(final Object key) {
		return list.get(key);
	}

	/**
	 * This is used to maintain a list of segments. All segments that are stored
	 * by this object can be acquired using a given key. The keys hash is used
	 * to select the segment, this ensures that all read and write operations
	 * with the same key result in the same segment object within this list.
	 * 
	 */
	private class SegmentList implements Iterable<Segment> {

		/**
		 * The list of segment objects maintained by this object.
		 */
		private final List<Segment> list;

		/**
		 * Represents the number of segments this object maintains.
		 */
		private final int size;

		/**
		 * Constructor for the <code>SegmentList</code> object. This is used to
		 * create a list of weak hash maps that can be acquired using the hash
		 * code of a given key.
		 * 
		 * @param size
		 *            this is the number of hash maps to maintain
		 */
		public SegmentList(final int size) {
			this.list = new ArrayList<Segment>();
			this.size = size;
			this.create(size);
		}

		public Iterator<Segment> iterator() {
			return list.iterator();
		}

		/**
		 * This is used to acquire the segment using the given key. The keys
		 * hash is used to determine the index within the list to acquire the
		 * segment, which is a synchronized weak hash map storing the key value
		 * pairs for a given hash.
		 * 
		 * @param key
		 *            this is the key used to determine the segment
		 * 
		 * @return the segment that is stored at the resolved hash
		 */
		public Segment get(final Object key) {
			final int segment = segment(key);

			if (segment < size) {
				return list.get(segment);
			}
			return null;
		}

		/**
		 * Upon initialization the segment list is populated in such a way that
		 * synchronization is not needed. Each segment is created and stored in
		 * an increasing index within the list.
		 * 
		 * @param size
		 *            this is the number of segments to be used
		 */
		private void create(final int size) {
			int count = size;

			while (count-- > 0) {
				list.add(new Segment());
			}
		}

		/**
		 * This method performs the translation of the key hash code to the
		 * segment index within the list. Translation is done by acquiring the
		 * modulus of the hash and the list size.
		 * 
		 * @param key
		 *            this is the key used to resolve the index
		 * 
		 * @return the index of the segment within the list
		 */
		private int segment(final Object key) {
			return Math.abs(key.hashCode() % size);
		}
	}

	/**
	 * The segment is effectively a synchronized weak hash map. If is used to
	 * store the key value pairs in such a way that they are kept only as long
	 * as the garbage collector does not collect the key. This ensures the cache
	 * does not cause memory issues.
	 * 
	 */
	private class Segment extends WeakHashMap<K, V> {

		/**
		 * This method is used to insert a key value mapping in to the cache.
		 * The value can later be retrieved or removed from the cache if
		 * desired. If the value associated with the key is null then nothing is
		 * stored within the cache.
		 * 
		 * @param key
		 *            this is the key to cache the provided value to
		 * @param value
		 *            this is the value that is to be cached
		 */
		public synchronized void cache(final K key, final V value) {
			put(key, value);
		}

		/**
		 * This method is used to get the value from the cache that is mapped to
		 * the specified key. If there is no value mapped to the specified key
		 * then this method will return a null.
		 * 
		 * @param key
		 *            this is the key to acquire the cache value with
		 * 
		 * @return this returns the value mapped to the specified key
		 */
		public synchronized V fetch(final K key) {
			return get(key);
		}

		/**
		 * This is used to exclusively take the value mapped to the specified
		 * key from the cache. Invoking this is effectively removing the value
		 * from the cache.
		 * 
		 * @param key
		 *            this is the key to acquire the cache value with
		 * 
		 * @return this returns the value mapped to the specified key
		 */
		public synchronized V take(final K key) {
			return remove(key);
		}

		/**
		 * This is used to determine whether the specified key exists with in
		 * the cache. Typically this can be done using the fetch method, which
		 * will acquire the object.
		 * 
		 * @param key
		 *            this is the key to check within this segment
		 * 
		 * @return true if the specified key is within the cache
		 */
		public synchronized boolean contains(final K key) {
			return containsKey(key);
		}
	}

	private CacheListener<K, V> listener;

	@Override
	public CacheListener<K, V> getCacheListener() {
		return listener;
	}

	@Override
	public void setCacheListener(final CacheListener<K, V> cacheListener) {
		listener = cacheListener;
	}
}
