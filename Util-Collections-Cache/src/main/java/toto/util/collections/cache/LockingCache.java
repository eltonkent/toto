package toto.util.collections.cache;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Read/Write locking cache implementation.
 * <p>
 * Every read/write operation is guranteed to be synchronized
 * </p>
 * 
 * @param <T>
 *            DataCache type
 */
public class LockingCache<K, V> implements Cache<K, V> {
	private int mCapacity;
	private final Map<K, V> mCache;
	private final ReadWriteLock mReadWriteLock = new ReentrantReadWriteLock();

	public LockingCache(final int capacity) {
		mCapacity = capacity;
		mCache = new LinkedHashMap<K, V>(mCapacity) {
			private static final long serialVersionUID = -9165777183357349715L;

			@Override
			protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
				if (size() > mCapacity) {
					mReadWriteLock.writeLock().lock();
					remove(eldest.getKey());
					mReadWriteLock.writeLock().unlock();
				}

				return false;
			}
		};
	}

	public V fetch(final Object key) {
		mReadWriteLock.readLock().lock();
		final V result = mCache.get(key);
		mReadWriteLock.readLock().unlock();
		return result;
	}

	public boolean remove(final K key) {
		mReadWriteLock.readLock().lock();
		final V result = mCache.get(key);
		mCache.remove(key);
		mReadWriteLock.readLock().unlock();
		if (listener != null && result != null) {
			listener.onEntryRemoved(key);
		}
		return true;
	}

	public boolean cache(final K key, final V value) {
		boolean ret = false;
		if (key != null && value != null) {
			mReadWriteLock.writeLock().lock();
			ret = (mCache.put(key, value) != null);
			mReadWriteLock.writeLock().unlock();
		}
		return ret;
	}

	public boolean contains(final Object key) {
		mReadWriteLock.readLock().lock();
		final boolean result = mCache.containsKey(key);
		mReadWriteLock.readLock().unlock();
		return result;
	}

	public void clear() {
		mReadWriteLock.writeLock().lock();
		mCache.clear();
		mReadWriteLock.writeLock().unlock();
	}

	public void setCapacity(final int capacity) {
		mCapacity = capacity;
	}

	@Override
	public boolean isEmpty() {
		// mReadWriteLock.writeLock().lock();
		return mCache.isEmpty();
		// mReadWriteLock.writeLock().unlock();
	}

	private CacheListener<K, V> listener;

	@Override
	public CacheListener<K, V> getCacheListener() {
		return listener;
	}

	@Override
	public void setCacheListener(final CacheListener<K, V> cacheListener) {
		listener = cacheListener;
	}
}