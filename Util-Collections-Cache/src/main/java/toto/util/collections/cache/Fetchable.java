package toto.util.collections;

public interface Fetchable<K, V> {

	public V fetch(K key);
}
