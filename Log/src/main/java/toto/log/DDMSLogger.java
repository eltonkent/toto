/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.log;

import android.util.Log;

public class DDMSLogger extends Logger {

	public DDMSLogger(final String parentTag) {
		super(parentTag);
	}

	@Override
	void d(final String tag, final String message) {
		Log.d(tag, message);
	}

	@Override
	void e(final String tag, final String message) {
		Log.e(tag, message);
	}

	@Override
	void i(final String tag, final String message) {
		Log.i(tag, message);
	}

	@Override
	public void shutdown() {

	}

	@Override
	void v(final String tag, final String message) {
		Log.v(tag, message);
	}

	@Override
	void w(final String tag, final String message) {
		Log.w(tag, message);
	}

	@Override
	void write(final int level, final String tag, final String message) {
		Log.println(level, tag, message);
	}

}
