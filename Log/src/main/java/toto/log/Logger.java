/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.log;

public abstract class Logger {

    private final String tag;

    Logger(final String tag) {
        this.tag = tag;
    }

    public void d(final String message) {
        d(tag, message);
    }

    abstract void d(String tag, String message);

    /**
     * debug level log with formatting arguments
     *
     * @param tag
     * @param message
     * @param args
     * @see String#format(String, Object...)
     */
    public void d(final String message, final Object... args) {
        d(tag, String.format(message, args));
    }

    public void e(final String message) {
        e(tag, message);
    }

    abstract void e(String tag, String message);

    /**
     * error level log with formatting arguments
     *
     * @param tag
     * @param subtag
     * @param args
     * @see String#format(String, Object...)
     */
    public void e(final String subtag, final Object... args) {
        e(tag, String.format(subtag, args));
    }

    public void i(final String message) {
        i(tag, message);
    }

    abstract void i(String tag, String message);

    /**
     * info level log with formatting arguments
     *
     * @param tag
     * @param message
     * @param args
     * @see String#format(String, Object...)
     */
    public void i(final String message, final Object... args) {
        i(tag, String.format(message, args));
    }

    public abstract void shutdown();

    public void v(final String message) {
        v(tag, message);
    }

    abstract void v(String tag, String message);

    /**
     * verbose level log with formatting arguments
     *
     * @param tag
     * @param message
     * @param args
     * @see String#format(String, Object...)
     */
    public void v(final String message, final Object... args) {
        v(tag, String.format(message, args));
    }

    public void w(final String message) {
        w(tag, message);
    }

    abstract void w(String tag, String message);

    /**
     * warning level log with formatting arguments
     *
     * @param tag
     * @param message
     * @param args
     * @see String#format(String, Object...)
     */
    public void w(final String message, final Object... args) {
        w(tag, String.format(message, args));
    }

    abstract void write(int level, String tag, String message);

    protected String formatMessage(final String subtag, final String message) {
        return new StringBuilder().append(subtag).append(" : ").append(message)
                .toString();
    }

    static Logger defLogger = new DDMSLogger("ToTo");

    public static Logger getDefault() {
        return defLogger;
    }
}
