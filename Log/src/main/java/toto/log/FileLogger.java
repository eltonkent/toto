/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.log;

import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;

/**
 * Simple log file strategy.
 * <p>
 * Creates a simple log at the path mentioned in the initializer.<br/>
 * The SDCard write permission needs to be setKey for this logging strategy.
 * <br/>
 * </p>
 */
public class FileLogger extends Logger {
    private File currentLogFile;
    private final String[] LEVELS = {"Verbose", "Debug", "Info", "Warn",
            "Error"};
    private final File logDirectory;
    protected FileWriter writer;

    /**
     * Creates a new Log file strategy instance
     * <p>
     *     All previous logs file are removed from the log directory.
     *
     * </p>
     *
     * @param logDirectory of the log file.
     * @param logFileName     of the log file
     * @throws java.lang.IllegalArgumentException if logDirectory is not a directory
     */
    public FileLogger(final String tag, final File logDirectory,
                      final String logFileName) {
        super(tag);
        if (!logDirectory.isDirectory()) {
            throw new IllegalArgumentException("Log directory is not a directory");
        }
        this.logDirectory=logDirectory;
        try {
            final String fname = logFileName;
            if (logDirectory.isFile()) {
                logDirectory.delete();
            }
            if (!logDirectory.exists()) {
                logDirectory.mkdir();
            }
            currentLogFile = new File(logDirectory, fname);
            writer = new FileWriter(currentLogFile, true);
        } catch (final IOException e) {
            e.printStackTrace();
            Log.e("Logger", e.getMessage());
        }
    }

    @Override
    void d(final String tag, final String message) {
        write(Log.DEBUG, tag, message);
    }

    @Override
    void e(final String tag, final String message) {
        write(Log.ERROR, tag, message);
    }

    protected String formatDate(final Date date) {
        return MessageFormat.format("{0,date} {0,time}", date);
    }

    @Override
    void i(final String tag, final String message) {
        write(Log.INFO, tag, message);
    }

    protected String levelToString(final int level) {
        return LEVELS[level - 2];
    }

    /**
     * Removes all other files at the given log directory except the current
     * one.
     * <p>
     * Note: all files even if not a log file will be deleted from the given
     * directory.
     * </p>
     */
    public void removeOldLogFiles() {
        // remove all files but the current file
        final File[] allfiles = logDirectory.listFiles();
        for (int i = 0; i < allfiles.length; i++) {
            if (!currentLogFile.equals(allfiles[i])) {
                allfiles[i].delete();
            }
        }
    }

    @Override
    public void shutdown() {
        Log.d("Logger", "Shutting down logger");
        if (writer != null) {
            try {
                writer.close();
                writer = null;
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    void v(final String tag, final String message) {
        write(Log.VERBOSE, tag, message);
    }

    @Override
    void w(final String tag, final String message) {
        write(Log.WARN, tag, message);
    }

    @Override
    void write(final int level, final String tag, final String message) {
        writeImpl(level, tag, message);
    }

    private void writeImpl(final int level, final String tag,
                           final String message) {
        if (writer != null) {
            try {
                writer.write("[" + formatDate(new Date()) + "] "
                        + levelToString(level) + " [" + tag + "] " + message
                        + "\r\n");
                writer.flush();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }/* else { */
        Log.println(level, tag, message);
        // }
    }
}
