package toto.log;

/**
 * Rage Library logger
 * 
 * @author Mobifluence Interactive
 * 
 */
public final class RLogger {

	private final Logger logger;
	private static final String TAG = "Rage";

	public RLogger(final Logger logger) {
		this.logger = logger;
	}

	public void d(final String message) {
		logger.d(TAG, message);
	}

	public void e(final String message) {
		logger.e(TAG, message);
	}

	public void i(final String message) {
		logger.i(TAG, message);
	}

	public void shutdown() {
		logger.shutdown();
	}

	public void v(final String message) {
		logger.v(TAG, message);
	}

	public void w(final String message) {
		logger.w(TAG, message);
	}

}
