/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LoggerUtils {

	/**
	 * 
	 * @return Buffered reader instance, null if the operation failed.
	 * @throws IOException
	 */
	public static BufferedReader getLogcatOutput() throws IOException {
		final Process process = getLogcatProcess();
		if (process != null)
			return new BufferedReader(new InputStreamReader(
					process.getInputStream()), 1024);
		else
			return null;
	}

	/**
	 * Get the handle to the logcat process
	 * 
	 * @return
	 * @throws IOException
	 */

	public static Process getLogcatProcess() throws IOException {
		return Runtime.getRuntime().exec(new String[] { "logcat" });

	}

}
