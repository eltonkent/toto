/**
 * Logging framework that allows logging to DDMS, simple log files and HTML files.
 * <p>
 * <b>Dependent packages/classes</b></br>
 * <ul>
 * <li>com.ek.android.io.file.SDCardUtils</li>
 * </ul>
 * </p>
 *
 */
package toto.log;