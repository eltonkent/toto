package toto.log;

/**
 * Logger for RAGE library. Internal to the library
 */
public class Log {
	// for development
	private static final Logger logger = new DDMSLogger("RAGE");

	// for production
	// private static final Logger logger=new DummyLogger("RAGE");

	public static void d(String message) {
		logger.d(message);
	}

	public static void e(final String message) {
		logger.e(message);
	}

	public static void i(final String message) {
		logger.i(message);
	}

	public static void shutdown() {
		logger.shutdown();
	}

	public static void v(final String message) {
		logger.v(message);
	}

	public static void w(final String message) {
		logger.w(message);
	}

}
