/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.log;

public class DummyLogger extends Logger {

	public DummyLogger(final String tag) {
		super(tag);
	}

	void d(final String tag, final String message) {
	}

	void e(final String tag, final String message) {
	}

	void i(final String tag, final String message) {
	}

	public void shutdown() {
		// TODO Auto-generated method stub

	}

	void v(final String tag, final String message) {
	}

	void w(final String tag, final String message) {
	}

	void write(final int level, final String tag, final String message) {
	}

}
