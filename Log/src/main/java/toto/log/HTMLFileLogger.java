/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.log;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import android.util.Log;

/**
 * Logger that logs data in HTML format.
 * <p> This enables quick search filter functionality.</p>
 * 
 * @author ekent4
 * 
 */
public final class HTMLFileLogger extends FileLogger {

	HTMLFileLogger(final String tag, final File directory,
			final String fileName) {
		super(tag, directory, fileName);
		initHTMLLogger();
	}

	private void initHTMLLogger() {
		if (writer != null) {
			try {
				writer.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n");
				writer.write("<html>\n");
				writer.write("<head>");
				writer.write("<title></title>");
				writer.write("<script type=\"text/javascript\">");
				writer.flush();
				writer.write("function filter (phrase, _id){\n");
				writer.write("var words = phrase.value.toLowerCase().split(\" \");\n");
				writer.write("var table = document.getElementById(_id);\n");
				writer.write("var ele;\n");
				writer.write("for (var r = 1; r < table.rows.length; r++){\n");
				writer.write("ele = table.rows[r].innerHTML.replace(/<[^>]+>/g,\"\");\n");
				writer.write("var displayStyle = 'none';\n");
				writer.write("for (var i = 0; i < words.length; i++) {\n");
				writer.write("if (ele.toLowerCase().indexOf(words[i])>=0)\n");
				writer.write("displayStyle = '';\n");
				writer.write("else {\n");
				writer.write("displayStyle = 'none';\n");
				writer.write("break;\n");
				writer.write("}\n");
				writer.write("}\n");
				writer.write("table.rows[r].style.display = displayStyle;\n");
				writer.write("}\n");
				writer.write("}\n");
				writer.flush();
				writer.write("</script>\n");
				writer.write("<form><b>Search:</b><input name=\"filt\"onkeyup=\"filter(this, 'sf')\" type=\"text\"></form>");
				writer.write("<table id=\"sf\" WIDTH=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"5\"");
				writer.write("<tr><th><b>Date</b></th><th><b>Level</b></th><th><b>Tag</b></th><th><b>Message</b></th></tr>");
				writer.flush();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void shutdown() {
		if (writer != null) {
			try {
				writer.write("</table></body></html>");
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
		super.shutdown();
	}

	private static String endFont = "</font></td>";
	private static String pStart = "<p>";
	private static String pEnd = "</p>";
	private static String trStart = "<tr>";
	private static String trEnd = "</tr>";
	private static String trFontColor = "<td><font color=\"";
	private static String color_info = "#007F00";
	private static String color_debug = "#00007F";
	private static String color_verbose = "#000000";
	private static String color_error = "#ff0000";
	private static String color_warn = "#FF7F00";
	private static String color_assert = "#000000";

	@Override
	public void write(final int level, final String tag, final String message) {

		if (writer != null) {
			String color = null;
			switch (level) {
			case Log.INFO:
				color = color_info;
				break;
			case Log.DEBUG:
				color = color_debug;
				break;
			case Log.VERBOSE:
				color = color_verbose;
				break;
			case Log.ERROR:
				color = color_error;
				break;
			case Log.WARN:
				color = color_warn;
				break;
			case Log.ASSERT:
				color = color_assert;
				break;
			}
			final StringBuffer buf = new StringBuffer();

			final String rowFont = trFontColor + color + "\">";
			buf.append(trStart);
			buf.append(rowFont);
			buf.append(formatDate(new Date()));
			buf.append(endFont);
			buf.append(rowFont);
			buf.append(levelToString(level));
			buf.append(endFont);

			buf.append(rowFont);
			buf.append(tag);
			buf.append(endFont);
			buf.append(rowFont);
			buf.append(pStart);
			buf.append(message);
			buf.append(pEnd);
			buf.append(endFont);
			buf.append(trEnd);
			try {
				writer.append(buf);
				writer.flush();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
		Log.println(level, tag, message);
	}
}
