package toto.barcode;


/**
 * The Version object encapsulates attributes about a particular size Data
 * Matrix Code.
 * 
 */
final class DataMatrixVersion {

	private static final DataMatrixVersion[] VERSIONS = buildVersions();

	private final int versionNumber;
	private final int symbolSizeRows;
	private final int symbolSizeColumns;
	private final int dataRegionSizeRows;
	private final int dataRegionSizeColumns;
	private final ECBlocks ecBlocks;
	private final int totalCodewords;

	private DataMatrixVersion(int versionNumber, int symbolSizeRows,
			int symbolSizeColumns, int dataRegionSizeRows,
			int dataRegionSizeColumns, ECBlocks ecBlocks) {
		this.versionNumber = versionNumber;
		this.symbolSizeRows = symbolSizeRows;
		this.symbolSizeColumns = symbolSizeColumns;
		this.dataRegionSizeRows = dataRegionSizeRows;
		this.dataRegionSizeColumns = dataRegionSizeColumns;
		this.ecBlocks = ecBlocks;

		// Calculate the total number of codewords
		int total = 0;
		int ecCodewords = ecBlocks.getECCodewords();
		ECB[] ecbArray = ecBlocks.getECBlocks();
		for (ECB ecBlock : ecbArray) {
			total += ecBlock.getCount()
					* (ecBlock.getDataCodewords() + ecCodewords);
		}
		this.totalCodewords = total;
	}

	int getVersionNumber() {
		return versionNumber;
	}

	int getSymbolSizeRows() {
		return symbolSizeRows;
	}

	int getSymbolSizeColumns() {
		return symbolSizeColumns;
	}

	int getDataRegionSizeRows() {
		return dataRegionSizeRows;
	}

	int getDataRegionSizeColumns() {
		return dataRegionSizeColumns;
	}

	int getTotalCodewords() {
		return totalCodewords;
	}

	ECBlocks getECBlocks() {
		return ecBlocks;
	}

	/**
	 * <p>
	 * Deduces version information from Data Matrix dimensions.
	 * </p>
	 * 
	 * @param numRows
	 *            Number of rows in modules
	 * @param numColumns
	 *            Number of columns in modules
	 * @return Version for a Data Matrix Code of those dimensions
	 * @throws FormatException
	 *             if dimensions do correspond to a valid Data Matrix size
	 */
	static DataMatrixVersion getVersionForDimensions(int numRows, int numColumns)
			throws FormatException {
		if ((numRows & 0x01) != 0 || (numColumns & 0x01) != 0) {
			throw FormatException.getFormatInstance();
		}

		for (DataMatrixVersion version : VERSIONS) {
			if (version.symbolSizeRows == numRows
					&& version.symbolSizeColumns == numColumns) {
				return version;
			}
		}

		throw FormatException.getFormatInstance();
	}

	/**
	 * <p>
	 * Encapsulates a setKey of error-correction blocks in one symbol version.
	 * Most versions will use blocks of differing sizes within one version, so,
	 * this encapsulates the parameters for each setKey of blocks. It also holds
	 * the number of error-correction codewords per block since it will be the
	 * same across all blocks within one version.
	 * </p>
	 */
	static final class ECBlocks {
		private final int ecCodewords;
		private final ECB[] ecBlocks;

		private ECBlocks(int ecCodewords, ECB ecBlocks) {
			this.ecCodewords = ecCodewords;
			this.ecBlocks = new ECB[] { ecBlocks };
		}

		private ECBlocks(int ecCodewords, ECB ecBlocks1, ECB ecBlocks2) {
			this.ecCodewords = ecCodewords;
			this.ecBlocks = new ECB[] { ecBlocks1, ecBlocks2 };
		}

		int getECCodewords() {
			return ecCodewords;
		}

		ECB[] getECBlocks() {
			return ecBlocks;
		}
	}

	/**
	 * <p>
	 * Encapsualtes the parameters for one error-correction block in one symbol
	 * version. This includes the number of data codewords, and the number of
	 * times a block with these parameters is used consecutively in the Data
	 * Matrix code version's format.
	 * </p>
	 */
	static final class ECB {
		private final int count;
		private final int dataCodewords;

		private ECB(int count, int dataCodewords) {
			this.count = count;
			this.dataCodewords = dataCodewords;
		}

		int getCount() {
			return count;
		}

		int getDataCodewords() {
			return dataCodewords;
		}
	}

	@Override
	public String toString() {
		return String.valueOf(versionNumber);
	}

	/**
	 * See ISO 16022:2006 5.5.1 Table 7
	 */
	private static DataMatrixVersion[] buildVersions() {
		return new DataMatrixVersion[] {
				new DataMatrixVersion(1, 10, 10, 8, 8, new ECBlocks(5, new ECB(
						1, 3))),
				new DataMatrixVersion(2, 12, 12, 10, 10, new ECBlocks(7,
						new ECB(1, 5))),
				new DataMatrixVersion(3, 14, 14, 12, 12, new ECBlocks(10,
						new ECB(1, 8))),
				new DataMatrixVersion(4, 16, 16, 14, 14, new ECBlocks(12,
						new ECB(1, 12))),
				new DataMatrixVersion(5, 18, 18, 16, 16, new ECBlocks(14,
						new ECB(1, 18))),
				new DataMatrixVersion(6, 20, 20, 18, 18, new ECBlocks(18,
						new ECB(1, 22))),
				new DataMatrixVersion(7, 22, 22, 20, 20, new ECBlocks(20,
						new ECB(1, 30))),
				new DataMatrixVersion(8, 24, 24, 22, 22, new ECBlocks(24,
						new ECB(1, 36))),
				new DataMatrixVersion(9, 26, 26, 24, 24, new ECBlocks(28,
						new ECB(1, 44))),
				new DataMatrixVersion(10, 32, 32, 14, 14, new ECBlocks(36,
						new ECB(1, 62))),
				new DataMatrixVersion(11, 36, 36, 16, 16, new ECBlocks(42,
						new ECB(1, 86))),
				new DataMatrixVersion(12, 40, 40, 18, 18, new ECBlocks(48,
						new ECB(1, 114))),
				new DataMatrixVersion(13, 44, 44, 20, 20, new ECBlocks(56,
						new ECB(1, 144))),
				new DataMatrixVersion(14, 48, 48, 22, 22, new ECBlocks(68,
						new ECB(1, 174))),
				new DataMatrixVersion(15, 52, 52, 24, 24, new ECBlocks(42,
						new ECB(2, 102))),
				new DataMatrixVersion(16, 64, 64, 14, 14, new ECBlocks(56,
						new ECB(2, 140))),
				new DataMatrixVersion(17, 72, 72, 16, 16, new ECBlocks(36,
						new ECB(4, 92))),
				new DataMatrixVersion(18, 80, 80, 18, 18, new ECBlocks(48,
						new ECB(4, 114))),
				new DataMatrixVersion(19, 88, 88, 20, 20, new ECBlocks(56,
						new ECB(4, 144))),
				new DataMatrixVersion(20, 96, 96, 22, 22, new ECBlocks(68,
						new ECB(4, 174))),
				new DataMatrixVersion(21, 104, 104, 24, 24, new ECBlocks(56,
						new ECB(6, 136))),
				new DataMatrixVersion(22, 120, 120, 18, 18, new ECBlocks(68,
						new ECB(6, 175))),
				new DataMatrixVersion(23, 132, 132, 20, 20, new ECBlocks(62,
						new ECB(8, 163))),
				new DataMatrixVersion(24, 144, 144, 22, 22, new ECBlocks(62,
						new ECB(8, 156), new ECB(2, 155))),
				new DataMatrixVersion(25, 8, 18, 6, 16, new ECBlocks(7,
						new ECB(1, 5))),
				new DataMatrixVersion(26, 8, 32, 6, 14, new ECBlocks(11,
						new ECB(1, 10))),
				new DataMatrixVersion(27, 12, 26, 10, 24, new ECBlocks(14,
						new ECB(1, 16))),
				new DataMatrixVersion(28, 12, 36, 10, 16, new ECBlocks(18,
						new ECB(1, 22))),
				new DataMatrixVersion(29, 16, 36, 14, 16, new ECBlocks(24,
						new ECB(1, 32))),
				new DataMatrixVersion(30, 16, 48, 14, 22, new ECBlocks(28,
						new ECB(1, 49))) };
	}

}
