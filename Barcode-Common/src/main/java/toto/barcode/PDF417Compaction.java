package toto.barcode;

enum PDF417Compaction {

	AUTO, TEXT, BYTE, NUMERIC

}