package toto.barcode;


/**
 * Created by Elton Kent on 12/20/13.
 */
class QRFinderPatternInfo {
	private final QRFinderPattern bottomLeft;
	private final QRFinderPattern topLeft;
	private final QRFinderPattern topRight;

	QRFinderPatternInfo(QRFinderPattern[] patternCenters) {
		this.bottomLeft = patternCenters[0];
		this.topLeft = patternCenters[1];
		this.topRight = patternCenters[2];
	}

	QRFinderPattern getBottomLeft() {
		return bottomLeft;
	}

	QRFinderPattern getTopLeft() {
		return topLeft;
	}

	QRFinderPattern getTopRight() {
		return topRight;
	}

}
