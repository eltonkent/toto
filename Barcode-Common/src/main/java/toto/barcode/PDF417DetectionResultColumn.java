package toto.barcode;

import java.util.Formatter;

class PDF417DetectionResultColumn {

	private static final int MAX_NEARBY_DISTANCE = 5;

	private final PDF417BoundingBox boundingBox;
	private final PDF417Codeword[] codewords;

	PDF417DetectionResultColumn(PDF417BoundingBox boundingBox) {
		this.boundingBox = new PDF417BoundingBox(boundingBox);
		codewords = new PDF417Codeword[boundingBox.getMaxY()
				- boundingBox.getMinY() + 1];
	}

	final PDF417Codeword getCodewordNearby(int imageRow) {
		PDF417Codeword codeword = getCodeword(imageRow);
		if (codeword != null) {
			return codeword;
		}
		for (int i = 1; i < MAX_NEARBY_DISTANCE; i++) {
			int nearImageRow = imageRowToCodewordIndex(imageRow) - i;
			if (nearImageRow >= 0) {
				codeword = codewords[nearImageRow];
				if (codeword != null) {
					return codeword;
				}
			}
			nearImageRow = imageRowToCodewordIndex(imageRow) + i;
			if (nearImageRow < codewords.length) {
				codeword = codewords[nearImageRow];
				if (codeword != null) {
					return codeword;
				}
			}
		}
		return null;
	}

	final int imageRowToCodewordIndex(int imageRow) {
		return imageRow - boundingBox.getMinY();
	}

	/*
	 * final int codewordIndexToImageRow(int codewordIndex) { return
	 * boundingBox.getMinY() + codewordIndex; }
	 */

	final void setCodeword(int imageRow, PDF417Codeword codeword) {
		codewords[imageRowToCodewordIndex(imageRow)] = codeword;
	}

	final PDF417Codeword getCodeword(int imageRow) {
		return codewords[imageRowToCodewordIndex(imageRow)];
	}

	final PDF417BoundingBox getBoundingBox() {
		return boundingBox;
	}

	final PDF417Codeword[] getCodewords() {
		return codewords;
	}

	@Override
	public String toString() {
		Formatter formatter = new Formatter();
		int row = 0;
		for (PDF417Codeword codeword : codewords) {
			if (codeword == null) {
				formatter.format("%3d:    |   \n", row++);
				continue;
			}
			formatter.format("%3d: %3d|%3d\n", row++, codeword.getRowNumber(),
					codeword.getValue());
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

}
