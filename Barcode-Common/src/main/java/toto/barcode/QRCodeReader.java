package toto.barcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import toto.geom2d.PerspectiveTransform;
import toto.geom2d.Point;
import toto.bitmap.BinaryBitmap;
import toto.bitmap.ToToBitmapException;
import toto.math.MathUtils;
import toto.util.collections.matrix.BitMatrix;

/**
 * This implementation can detect and decode QR Codes in an image.
 * 
 */
class QRCodeReader implements Reader {

	private static final ResultPoint[] NO_POINTS = new ResultPoint[0];

	private final Decoder decoder = new Decoder();

	protected final Decoder getDecoder() {
		return decoder;
	}

	/**
	 * Locates and decodes a QR code in an image.
	 * 
	 * @return a String representing the content encoded by the QR code
	 * @throws BarcodeNotFoundException
	 *             if a QR code cannot be found
	 * @throws FormatException
	 *             if a QR code cannot be decoded
	 * @throws BarCodeChecksumException
	 *             if error correction fails
	 */
	@Override
	public Result decode(BinaryBitmap image) throws BarcodeNotFoundException,
			BarCodeChecksumException, FormatException {
		return decode(image, null);
	}

	@Override
	public final Result decode(BinaryBitmap image, Map<DecodeHintType, ?> hints)
			throws BarcodeNotFoundException, BarCodeChecksumException,
			FormatException {
		DecoderResult decoderResult;
		ResultPoint[] points;
		try {
			if (hints != null && hints.containsKey(DecodeHintType.PURE_BARCODE)) {
				BitMatrix bits = extractPureBits(image.getBlackMatrix());
				decoderResult = decoder.decode(bits, hints);
				points = NO_POINTS;
			} else {
				DetectorResult detectorResult = new Detector(image.getBlackMatrix())
						.detect(hints);
				decoderResult = decoder.decode(detectorResult.getBits(), hints);
				points = detectorResult.getPoints();
			}
		}catch (ToToBitmapException e){
			throw new BarcodeNotFoundException();
		}

		// If the code was mirrored: swap the bottom-left and the top-right
		// points.
		if (decoderResult.getOther() instanceof QRCodeDecoderMetaData) {
			((QRCodeDecoderMetaData) decoderResult.getOther())
					.applyMirroredCorrection(points);
		}

		Result result = new Result(decoderResult.getText(),
				decoderResult.getRawBytes(), points, BarcodeType.QR_CODE);
		List<byte[]> byteSegments = decoderResult.getByteSegments();
		if (byteSegments != null) {
			result.putMetadata(ResultMetadataType.BYTE_SEGMENTS, byteSegments);
		}
		String ecLevel = decoderResult.getECLevel();
		if (ecLevel != null) {
			result.putMetadata(ResultMetadataType.ERROR_CORRECTION_LEVEL,
					ecLevel);
		}
		return result;
	}

	@Override
	public void reset() {
		// do nothing
	}

	/**
	 * This method detects a code in a "pure" image -- that is, pure monochrome
	 * image which contains only an unrotated, unskewed, image of a code, with
	 * some white border around it. This is a specialized method that works
	 * exceptionally fast in this special case.
	 * 
	 * @see DataMatrixReader#extractPureBits(BitMatrix)
	 */
	private static BitMatrix extractPureBits(BitMatrix image)
			throws BarcodeNotFoundException {

		int[] leftTopBlack = image.getTopLeftOnBit();
		int[] rightBottomBlack = image.getBottomRightOnBit();
		if (leftTopBlack == null || rightBottomBlack == null) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		float moduleSize = moduleSize(leftTopBlack, image);

		int top = leftTopBlack[1];
		int bottom = rightBottomBlack[1];
		int left = leftTopBlack[0];
		int right = rightBottomBlack[0];

		// Sanity check!
		if (left >= right || top >= bottom) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		if (bottom - top != right - left) {
			// Special case, where bottom-right module wasn't black so we found
			// something else in the last row
			// Assume it's a square, so use height as the width
			right = left + (bottom - top);
		}

		int matrixWidth = Math.round((right - left + 1) / moduleSize);
		int matrixHeight = Math.round((bottom - top + 1) / moduleSize);
		if (matrixWidth <= 0 || matrixHeight <= 0) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}
		if (matrixHeight != matrixWidth) {
			// Only possibly decode square regions
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		// Push in the "border" by half the module width so that we start
		// sampling in the middle of the module. Just in case the image is a
		// little off, this will help recover.
		int nudge = (int) (moduleSize / 2.0f);
		top += nudge;
		left += nudge;

		// But careful that this does not sample off the edge
		int nudgedTooFarRight = left + (int) ((matrixWidth - 1) * moduleSize)
				- (right - 1);
		if (nudgedTooFarRight > 0) {
			if (nudgedTooFarRight > nudge) {
				// Neither way fits; abort
				throw BarcodeNotFoundException.getNotFoundInstance();
			}
			left -= nudgedTooFarRight;
		}
		int nudgedTooFarDown = top + (int) ((matrixHeight - 1) * moduleSize)
				- (bottom - 1);
		if (nudgedTooFarDown > 0) {
			if (nudgedTooFarDown > nudge) {
				// Neither way fits; abort
				throw BarcodeNotFoundException.getNotFoundInstance();
			}
			top -= nudgedTooFarDown;
		}

		// Now just read off the bits
		BitMatrix bits = new BitMatrix(matrixWidth, matrixHeight);
		for (int y = 0; y < matrixHeight; y++) {
			int iOffset = top + (int) (y * moduleSize);
			for (int x = 0; x < matrixWidth; x++) {
				if (image.get(left + (int) (x * moduleSize), iOffset)) {
					bits.set(x, y);
				}
			}
		}
		return bits;
	}

	private static float moduleSize(int[] leftTopBlack, BitMatrix image)
			throws BarcodeNotFoundException {
		int height = image.getHeight();
		int width = image.getWidth();
		int x = leftTopBlack[0];
		int y = leftTopBlack[1];
		boolean inBlack = true;
		int transitions = 0;
		while (x < width && y < height) {
			if (inBlack != image.get(x, y)) {
				if (++transitions == 5) {
					break;
				}
				inBlack = !inBlack;
			}
			x++;
			y++;
		}
		if (x == width || y == height) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}
		return (x - leftTopBlack[0]) / 7.0f;
	}

	private static final class Decoder {

		private final ReedSolomonDecoder rsDecoder;

		Decoder() {
			rsDecoder = new ReedSolomonDecoder(GenericGF.QR_CODE_FIELD_256);
		}

		DecoderResult decode(boolean[][] image)
				throws BarCodeChecksumException, FormatException {
			return decode(image, null);
		}

		/**
		 * <p>
		 * Convenience method that can decode a QR Code represented as a 2D
		 * array of booleans. "true" is taken to mean a black module.
		 * </p>
		 * 
		 * @param image
		 *            booleans representing white/black QR Code modules
		 * @return text and bytes encoded within the QR Code
		 * @throws FormatException
		 *             if the QR Code cannot be decoded
		 * @throws BarCodeChecksumException
		 *             if error correction fails
		 */
		DecoderResult decode(boolean[][] image, Map<DecodeHintType, ?> hints)
				throws BarCodeChecksumException, FormatException {
			int dimension = image.length;
			BitMatrix bits = new BitMatrix(dimension);
			for (int i = 0; i < dimension; i++) {
				for (int j = 0; j < dimension; j++) {
					if (image[i][j]) {
						bits.set(j, i);
					}
				}
			}
			return decode(bits, hints);
		}

		DecoderResult decode(BitMatrix bits) throws BarCodeChecksumException,
				FormatException {
			return decode(bits, null);
		}

		/**
		 * <p>
		 * Decodes a QR Code represented as a {@link BitMatrix}. A 1 or "true"
		 * is taken to mean a black module.
		 * </p>
		 * 
		 * @param bits
		 *            booleans representing white/black QR Code modules
		 * @return text and bytes encoded within the QR Code
		 * @throws FormatException
		 *             if the QR Code cannot be decoded
		 * @throws BarCodeChecksumException
		 *             if error correction fails
		 */
		DecoderResult decode(BitMatrix bits, Map<DecodeHintType, ?> hints)
				throws FormatException, BarCodeChecksumException {

			// Construct a parser and read version, error-correction level
			QRBitMatrixParser parser = new QRBitMatrixParser(bits);
			FormatException fe = null;
			BarCodeChecksumException ce = null;
			try {
				return decode(parser, hints);
			} catch (FormatException e) {
				fe = e;
			} catch (BarCodeChecksumException e) {
				ce = e;
			}

			try {

				// Revert the bit matrix
				parser.remask();

				// Will be attempting a mirrored reading of the version and
				// format info.
				parser.setMirror(true);

				// Preemptively read the version.
				parser.readVersion();

				// Preemptively read the format information.
				parser.readFormatInformation();

				/*
				 * Since we're here, this means we have successfully detected
				 * some kind of version and format information when mirrored.
				 * This is a good sign, that the QR code may be mirrored, and we
				 * should try once more with a mirrored content.
				 */
				// Prepare for a mirrored reading.
				parser.mirror();

				DecoderResult result = decode(parser, hints);

				// Success! Notify the caller that the code was mirrored.
				result.setOther(new QRCodeDecoderMetaData(true));

				return result;

			} catch (FormatException e) {
				// Throw the exception from the original reading
				if (fe != null) {
					throw fe;
				}
				if (ce != null) {
					throw ce;
				}
				throw e;

			} catch (BarCodeChecksumException e) {
				// Throw the exception from the original reading
				if (fe != null) {
					throw fe;
				}
				if (ce != null) {
					throw ce;
				}
				throw e;

			}
		}

		private DecoderResult decode(QRBitMatrixParser parser,
				Map<DecodeHintType, ?> hints) throws FormatException,
				BarCodeChecksumException {
			QRVersion version = parser.readVersion();
			QRErrorCorrectionLevel ecLevel = parser.readFormatInformation()
					.getErrorCorrectionLevel();

			// Read codewords
			byte[] codewords = parser.readCodewords();
			// Separate into data blocks
			QRDataBlock[] dataBlocks = QRDataBlock.getDataBlocks(codewords,
					version, ecLevel);

			// Count total number of data bytes
			int totalBytes = 0;
			for (QRDataBlock dataBlock : dataBlocks) {
				totalBytes += dataBlock.getNumDataCodewords();
			}
			byte[] resultBytes = new byte[totalBytes];
			int resultOffset = 0;

			// Error-correct and copy data blocks together into a stream of
			// bytes
			for (QRDataBlock dataBlock : dataBlocks) {
				byte[] codewordBytes = dataBlock.getCodewords();
				int numDataCodewords = dataBlock.getNumDataCodewords();
				correctErrors(codewordBytes, numDataCodewords);
				for (int i = 0; i < numDataCodewords; i++) {
					resultBytes[resultOffset++] = codewordBytes[i];
				}
			}

			// Decode the contents of that stream of bytes
			return QRDecodedBitStreamParser.decode(resultBytes, version,
					ecLevel, hints);
		}

		/**
		 * <p>
		 * Given data and error-correction codewords received, possibly
		 * corrupted by errors, attempts to correct the errors in-place using
		 * Reed-Solomon error correction.
		 * </p>
		 * 
		 * @param codewordBytes
		 *            data and error correction codewords
		 * @param numDataCodewords
		 *            number of codewords that are data bytes
		 * @throws BarCodeChecksumException
		 *             if error correction fails
		 */
		private void correctErrors(byte[] codewordBytes, int numDataCodewords)
				throws BarCodeChecksumException {
			int numCodewords = codewordBytes.length;
			// First read into an array of ints
			int[] codewordsInts = new int[numCodewords];
			for (int i = 0; i < numCodewords; i++) {
				codewordsInts[i] = codewordBytes[i] & 0xFF;
			}
			int numECCodewords = codewordBytes.length - numDataCodewords;
			try {
				rsDecoder.decode(codewordsInts, numECCodewords);
			} catch (ReedSolomonException ignored) {
				throw BarCodeChecksumException.getChecksumInstance();
			}
			// Copy back into array of bytes -- only need to worry about the
			// bytes that were data
			// We don't care about errors in the error-correction codewords
			for (int i = 0; i < numDataCodewords; i++) {
				codewordBytes[i] = (byte) codewordsInts[i];
			}
		}

	}

	private static class Detector {

		private final BitMatrix image;
		private ResultPointCallback resultPointCallback;

		Detector(BitMatrix image) {
			this.image = image;
		}

		protected final BitMatrix getImage() {
			return image;
		}

		protected final ResultPointCallback getResultPointCallback() {
			return resultPointCallback;
		}

		/**
		 * <p>
		 * Detects a QR Code in an image, simply.
		 * </p>
		 * 
		 * @return {@link DetectorResult} encapsulating results of detecting a
		 *         QR Code
		 * @throws BarcodeNotFoundException
		 *             if no QR Code can be found
		 */
		DetectorResult detect() throws BarcodeNotFoundException,
				FormatException {
			return detect(null);
		}

		/**
		 * <p>
		 * Detects a QR Code in an image, simply.
		 * </p>
		 * 
		 * @param hints
		 *            optional hints to detector
		 * @return {@link BarcodeNotFoundException} encapsulating results of
		 *         detecting a QR Code
		 * @throws BarcodeNotFoundException
		 *             if QR Code cannot be found
		 * @throws FormatException
		 *             if a QR Code cannot be decoded
		 */
		final DetectorResult detect(Map<DecodeHintType, ?> hints)
				throws BarcodeNotFoundException, FormatException {

			resultPointCallback = hints == null ? null
					: (ResultPointCallback) hints
							.get(DecodeHintType.NEED_RESULT_POINT_CALLBACK);

			QRFinderPatternFinder finder = new QRFinderPatternFinder(image,
					resultPointCallback);
			QRFinderPatternInfo info = finder.find(hints);

			return processFinderPatternInfo(info);
		}

		protected final DetectorResult processFinderPatternInfo(
				QRFinderPatternInfo info) throws BarcodeNotFoundException,
				FormatException {

			QRFinderPattern topLeft = info.getTopLeft();
			QRFinderPattern topRight = info.getTopRight();
			QRFinderPattern bottomLeft = info.getBottomLeft();

			float moduleSize = calculateModuleSize(topLeft, topRight,
					bottomLeft);
			if (moduleSize < 1.0f) {
				throw BarcodeNotFoundException.getNotFoundInstance();
			}
			int dimension = computeDimension(topLeft, topRight, bottomLeft,
					moduleSize);
			QRVersion provisionalVersion = QRVersion
					.getProvisionalVersionForDimension(dimension);
			int modulesBetweenFPCenters = provisionalVersion
					.getDimensionForVersion() - 7;

			AlignmentPattern alignmentPattern = null;
			// Anything above version 1 has an alignment pattern
			if (provisionalVersion.getAlignmentPatternCenters().length > 0) {

				// Guess where a "bottom right" finder pattern would have been
				float bottomRightX = topRight.getX() - topLeft.getX()
						+ bottomLeft.getX();
				float bottomRightY = topRight.getY() - topLeft.getY()
						+ bottomLeft.getY();

				// Estimate that alignment pattern is closer by 3 modules
				// from "bottom right" to known top left location
				float correctionToTopLeft = 1.0f - 3.0f / (float) modulesBetweenFPCenters;
				int estAlignmentX = (int) (topLeft.getX() + correctionToTopLeft
						* (bottomRightX - topLeft.getX()));
				int estAlignmentY = (int) (topLeft.getY() + correctionToTopLeft
						* (bottomRightY - topLeft.getY()));

				// Kind of arbitrary -- expand search radius before giving up
				for (int i = 4; i <= 16; i <<= 1) {
					try {
						alignmentPattern = findAlignmentInRegion(moduleSize,
								estAlignmentX, estAlignmentY, (float) i);
						break;
					} catch (BarcodeNotFoundException re) {
						// try next round
					}
				}
				// If we didn't find alignment pattern... well try anyway
				// without it
			}

			PerspectiveTransform transform = createTransform(topLeft, topRight,
					bottomLeft, alignmentPattern, dimension);

			BitMatrix bits = sampleGrid(image, transform, dimension);

			ResultPoint[] points;
			if (alignmentPattern == null) {
				points = new ResultPoint[] { bottomLeft, topLeft, topRight };
			} else {
				points = new ResultPoint[] { bottomLeft, topLeft, topRight,
						alignmentPattern };
			}
			return new DetectorResult(bits, points);
		}

		private static PerspectiveTransform createTransform(
				ResultPoint topLeft, ResultPoint topRight,
				ResultPoint bottomLeft, ResultPoint alignmentPattern,
				int dimension) {
			float dimMinusThree = (float) dimension - 3.5f;
			float bottomRightX;
			float bottomRightY;
			float sourceBottomRightX;
			float sourceBottomRightY;
			if (alignmentPattern != null) {
				bottomRightX = alignmentPattern.getX();
				bottomRightY = alignmentPattern.getY();
				sourceBottomRightX = dimMinusThree - 3.0f;
				sourceBottomRightY = sourceBottomRightX;
			} else {
				// Don't have an alignment pattern, just make up the
				// bottom-right point
				bottomRightX = (topRight.getX() - topLeft.getX())
						+ bottomLeft.getX();
				bottomRightY = (topRight.getY() - topLeft.getY())
						+ bottomLeft.getY();
				sourceBottomRightX = dimMinusThree;
				sourceBottomRightY = dimMinusThree;
			}

			return PerspectiveTransform.quadrilateralToQuadrilateral(3.5f,
					3.5f, dimMinusThree, 3.5f, sourceBottomRightX,
					sourceBottomRightY, 3.5f, dimMinusThree, topLeft.getX(),
					topLeft.getY(), topRight.getX(), topRight.getY(),
					bottomRightX, bottomRightY, bottomLeft.getX(),
					bottomLeft.getY());
		}

		private static BitMatrix sampleGrid(BitMatrix image,
				PerspectiveTransform transform, int dimension)
				throws BarcodeNotFoundException {

			GridSampler sampler = GridSampler.getInstance();
			return sampler.sampleGrid(image, dimension, dimension, transform);
		}

		/**
		 * <p>
		 * Computes the dimension (number of modules on a size) of the QR Code
		 * based on the position of the finder patterns and estimated module
		 * size.
		 * </p>
		 */
		private static int computeDimension(ResultPoint topLeft,
				ResultPoint topRight, ResultPoint bottomLeft, float moduleSize)
				throws BarcodeNotFoundException {
			int tltrCentersDimension = MathUtils.round(ResultPoint.distance(
					topLeft, topRight) / moduleSize);
			int tlblCentersDimension = MathUtils.round(ResultPoint.distance(
					topLeft, bottomLeft) / moduleSize);
			int dimension = ((tltrCentersDimension + tlblCentersDimension) >> 1) + 7;
			switch (dimension & 0x03) { // mod 4
			case 0:
				dimension++;
				break;
			// 1? do nothing
			case 2:
				dimension--;
				break;
			case 3:
				throw BarcodeNotFoundException.getNotFoundInstance();
			}
			return dimension;
		}

		/**
		 * <p>
		 * Computes an average estimated module size based on estimated derived
		 * from the positions of the three finder patterns.
		 * </p>
		 */
		protected final float calculateModuleSize(ResultPoint topLeft,
				ResultPoint topRight, ResultPoint bottomLeft) {
			// Take the average
			return (calculateModuleSizeOneWay(topLeft, topRight) + calculateModuleSizeOneWay(
					topLeft, bottomLeft)) / 2.0f;
		}

		/**
		 * <p>
		 * Estimates module size based on two finder patterns -- it uses
		 * {@link #sizeOfBlackWhiteBlackRunBothWays(int, int, int, int)} to
		 * figure the width of each, measuring along the axis between their
		 * centers.
		 * </p>
		 */
		private float calculateModuleSizeOneWay(ResultPoint pattern,
				ResultPoint otherPattern) {
			float moduleSizeEst1 = sizeOfBlackWhiteBlackRunBothWays(
					(int) pattern.getX(), (int) pattern.getY(),
					(int) otherPattern.getX(), (int) otherPattern.getY());
			float moduleSizeEst2 = sizeOfBlackWhiteBlackRunBothWays(
					(int) otherPattern.getX(), (int) otherPattern.getY(),
					(int) pattern.getX(), (int) pattern.getY());
			if (Float.isNaN(moduleSizeEst1)) {
				return moduleSizeEst2 / 7.0f;
			}
			if (Float.isNaN(moduleSizeEst2)) {
				return moduleSizeEst1 / 7.0f;
			}
			// Average them, and divide by 7 since we've counted the width of 3
			// black modules,
			// and 1 white and 1 black module on either side. Ergo, divide sum
			// by 14.
			return (moduleSizeEst1 + moduleSizeEst2) / 14.0f;
		}

		/**
		 * See {@link #sizeOfBlackWhiteBlackRun(int, int, int, int)}; computes
		 * the total width of a finder pattern by looking for a
		 * black-white-black run from the center in the direction of another
		 * point (another finder pattern center), and in the opposite direction
		 * too.</p>
		 */
		private float sizeOfBlackWhiteBlackRunBothWays(int fromX, int fromY,
				int toX, int toY) {

			float result = sizeOfBlackWhiteBlackRun(fromX, fromY, toX, toY);

			// Now count other way -- don't run off image though of course
			float scale = 1.0f;
			int otherToX = fromX - (toX - fromX);
			if (otherToX < 0) {
				scale = (float) fromX / (float) (fromX - otherToX);
				otherToX = 0;
			} else if (otherToX >= image.getWidth()) {
				scale = (float) (image.getWidth() - 1 - fromX)
						/ (float) (otherToX - fromX);
				otherToX = image.getWidth() - 1;
			}
			int otherToY = (int) (fromY - (toY - fromY) * scale);

			scale = 1.0f;
			if (otherToY < 0) {
				scale = (float) fromY / (float) (fromY - otherToY);
				otherToY = 0;
			} else if (otherToY >= image.getHeight()) {
				scale = (float) (image.getHeight() - 1 - fromY)
						/ (float) (otherToY - fromY);
				otherToY = image.getHeight() - 1;
			}
			otherToX = (int) (fromX + (otherToX - fromX) * scale);

			result += sizeOfBlackWhiteBlackRun(fromX, fromY, otherToX, otherToY);

			// Middle pixel is double-counted this way; subtract 1
			return result - 1.0f;
		}

		/**
		 * <p>
		 * This method traces a line from a point in the image, in the direction
		 * towards another point. It begins in a black region, and keeps going
		 * until it finds white, then black, then white again. It reports the
		 * distance from the start to this point.
		 * </p>
		 * 
		 * <p>
		 * This is used when figuring out how wide a finder pattern is, when the
		 * finder pattern may be skewed or rotated.
		 * </p>
		 */
		private float sizeOfBlackWhiteBlackRun(int fromX, int fromY, int toX,
				int toY) {
			// Mild variant of Bresenham's algorithm;
			// see http://en.wikipedia.org/wiki/Bresenham's_line_algorithm
			boolean steep = Math.abs(toY - fromY) > Math.abs(toX - fromX);
			if (steep) {
				int temp = fromX;
				fromX = fromY;
				fromY = temp;
				temp = toX;
				toX = toY;
				toY = temp;
			}

			int dx = Math.abs(toX - fromX);
			int dy = Math.abs(toY - fromY);
			int error = -dx >> 1;
			int xstep = fromX < toX ? 1 : -1;
			int ystep = fromY < toY ? 1 : -1;

			// In black pixels, looking for white, first or second time.
			int state = 0;
			// Loop up until x == toX, but not beyond
			int xLimit = toX + xstep;
			for (int x = fromX, y = fromY; x != xLimit; x += xstep) {
				int realX = steep ? y : x;
				int realY = steep ? x : y;

				// Does current pixel mean we have moved white to black or vice
				// versa?
				// Scanning black in state 0,2 and white in state 1, so if we
				// find the wrong
				// color, advance to next state or end if we are in state 2
				// already
				if ((state == 1) == image.get(realX, realY)) {
					if (state == 2) {
						return Point.distance(x, y, fromX, fromY);
					}
					state++;
				}

				error += dy;
				if (error > 0) {
					if (y == toY) {
						break;
					}
					y += ystep;
					error -= dx;
				}
			}
			// Found black-white-black; give the benefit of the doubt that the
			// next pixel outside the image
			// is "white" so this last point at (toX+xStep,toY) is the right
			// ending. This is really a
			// small approximation; (toX+xStep,toY+yStep) might be really
			// correct. Ignore this.
			if (state == 2) {
				return Point.distance(toX + xstep, toY, fromX, fromY);
			}
			// else we didn't find even black-white-black; no estimate is really
			// possible
			return Float.NaN;
		}

		/**
		 * <p>
		 * Attempts to locate an alignment pattern in a limited region of the
		 * image, which is guessed to contain it. This method uses
		 * {@link AlignmentPattern}.
		 * </p>
		 * 
		 * @param overallEstModuleSize
		 *            estimated module size so far
		 * @param estAlignmentX
		 *            x coordinate of center of area probably containing
		 *            alignment pattern
		 * @param estAlignmentY
		 *            y coordinate of above
		 * @param allowanceFactor
		 *            number of pixels in all directions to search from the
		 *            center
		 * @return {@link AlignmentPattern} if found, or null otherwise
		 * @throws BarcodeNotFoundException
		 *             if an unexpected error occurs during detection
		 */
		protected final AlignmentPattern findAlignmentInRegion(
				float overallEstModuleSize, int estAlignmentX,
				int estAlignmentY, float allowanceFactor)
				throws BarcodeNotFoundException {
			// Look for an alignment pattern (3 modules in size) around where it
			// should be
			int allowance = (int) (allowanceFactor * overallEstModuleSize);
			int alignmentAreaLeftX = Math.max(0, estAlignmentX - allowance);
			int alignmentAreaRightX = Math.min(image.getWidth() - 1,
					estAlignmentX + allowance);
			if (alignmentAreaRightX - alignmentAreaLeftX < overallEstModuleSize * 3) {
				throw BarcodeNotFoundException.getNotFoundInstance();
			}

			int alignmentAreaTopY = Math.max(0, estAlignmentY - allowance);
			int alignmentAreaBottomY = Math.min(image.getHeight() - 1,
					estAlignmentY + allowance);
			if (alignmentAreaBottomY - alignmentAreaTopY < overallEstModuleSize * 3) {
				throw BarcodeNotFoundException.getNotFoundInstance();
			}

			AlignmentPatternFinder alignmentFinder = new AlignmentPatternFinder(
					image, alignmentAreaLeftX, alignmentAreaTopY,
					alignmentAreaRightX - alignmentAreaLeftX,
					alignmentAreaBottomY - alignmentAreaTopY,
					overallEstModuleSize, resultPointCallback);
			return alignmentFinder.find();
		}

	}

	static final class AlignmentPatternFinder {

		private final BitMatrix image;
		private final List<AlignmentPattern> possibleCenters;
		private final int startX;
		private final int startY;
		private final int width;
		private final int height;
		private final float moduleSize;
		private final int[] crossCheckStateCount;
		private final ResultPointCallback resultPointCallback;

		/**
		 * <p>
		 * Creates a finder that will look in a portion of the whole image.
		 * </p>
		 * 
		 * @param image
		 *            image to search
		 * @param startX
		 *            left column from which to start searching
		 * @param startY
		 *            top row from which to start searching
		 * @param width
		 *            width of region to search
		 * @param height
		 *            height of region to search
		 * @param moduleSize
		 *            estimated module size so far
		 */
		AlignmentPatternFinder(BitMatrix image, int startX, int startY,
				int width, int height, float moduleSize,
				ResultPointCallback resultPointCallback) {
			this.image = image;
			this.possibleCenters = new ArrayList<AlignmentPattern>(5);
			this.startX = startX;
			this.startY = startY;
			this.width = width;
			this.height = height;
			this.moduleSize = moduleSize;
			this.crossCheckStateCount = new int[3];
			this.resultPointCallback = resultPointCallback;
		}

		/**
		 * <p>
		 * This method attempts to find the bottom-right alignment pattern in
		 * the image. It is a bit messy since it's pretty performance-critical
		 * and so is written to be fast foremost.
		 * </p>
		 * 
		 * @return {@link AlignmentPattern} if found
		 * @throws BarcodeNotFoundException
		 *             if not found
		 */
		AlignmentPattern find() throws BarcodeNotFoundException {
			int startX = this.startX;
			int height = this.height;
			int maxJ = startX + width;
			int middleI = startY + (height >> 1);
			// We are looking for black/white/black modules in 1:1:1 ratio;
			// this tracks the number of black/white/black modules seen so far
			int[] stateCount = new int[3];
			for (int iGen = 0; iGen < height; iGen++) {
				// Search from middle outwards
				int i = middleI
						+ ((iGen & 0x01) == 0 ? (iGen + 1) >> 1
								: -((iGen + 1) >> 1));
				stateCount[0] = 0;
				stateCount[1] = 0;
				stateCount[2] = 0;
				int j = startX;
				// Burn off leading white pixels before anything else; if we
				// start in the middle of
				// a white run, it doesn't make sense to count its length, since
				// we don't know if the
				// white run continued to the left of the start point
				while (j < maxJ && !image.get(j, i)) {
					j++;
				}
				int currentState = 0;
				while (j < maxJ) {
					if (image.get(j, i)) {
						// Black pixel
						if (currentState == 1) { // Counting black pixels
							stateCount[currentState]++;
						} else { // Counting white pixels
							if (currentState == 2) { // A winner?
								if (foundPatternCross(stateCount)) { // Yes
									AlignmentPattern confirmed = handlePossibleCenter(
											stateCount, i, j);
									if (confirmed != null) {
										return confirmed;
									}
								}
								stateCount[0] = stateCount[2];
								stateCount[1] = 1;
								stateCount[2] = 0;
								currentState = 1;
							} else {
								stateCount[++currentState]++;
							}
						}
					} else { // White pixel
						if (currentState == 1) { // Counting black pixels
							currentState++;
						}
						stateCount[currentState]++;
					}
					j++;
				}
				if (foundPatternCross(stateCount)) {
					AlignmentPattern confirmed = handlePossibleCenter(
							stateCount, i, maxJ);
					if (confirmed != null) {
						return confirmed;
					}
				}

			}

			// Hmm, nothing we saw was observed and confirmed twice. If we had
			// any guess at all, return it.
			if (!possibleCenters.isEmpty()) {
				return possibleCenters.get(0);
			}

			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		/**
		 * Given a count of black/white/black pixels just seen and an end
		 * position, figures the location of the center of this
		 * black/white/black run.
		 */
		private static float centerFromEnd(int[] stateCount, int end) {
			return (float) (end - stateCount[2]) - stateCount[1] / 2.0f;
		}

		/**
		 * @param stateCount
		 *            count of black/white/black pixels just read
		 * @return true iff the proportions of the counts is close enough to the
		 *         1/1/1 ratios used by alignment patterns to be considered a
		 *         match
		 */
		private boolean foundPatternCross(int[] stateCount) {
			float moduleSize = this.moduleSize;
			float maxVariance = moduleSize / 2.0f;
			for (int i = 0; i < 3; i++) {
				if (Math.abs(moduleSize - stateCount[i]) >= maxVariance) {
					return false;
				}
			}
			return true;
		}

		/**
		 * <p>
		 * After a horizontal scan finds a potential alignment pattern, this
		 * method "cross-checks" by scanning down vertically through the center
		 * of the possible alignment pattern to see if the same proportion is
		 * detected.
		 * </p>
		 * 
		 * @param startI
		 *            row where an alignment pattern was detected
		 * @param centerJ
		 *            center of the section that appears to cross an alignment
		 *            pattern
		 * @param maxCount
		 *            maximum reasonable number of modules that should be
		 *            observed in any reading state, based on the results of the
		 *            horizontal scan
		 * @return vertical center of alignment pattern, or {@link Float#NaN} if
		 *         not found
		 */
		private float crossCheckVertical(int startI, int centerJ, int maxCount,
				int originalStateCountTotal) {
			BitMatrix image = this.image;

			int maxI = image.getHeight();
			int[] stateCount = crossCheckStateCount;
			stateCount[0] = 0;
			stateCount[1] = 0;
			stateCount[2] = 0;

			// Start counting up from center
			int i = startI;
			while (i >= 0 && image.get(centerJ, i) && stateCount[1] <= maxCount) {
				stateCount[1]++;
				i--;
			}
			// If already too many modules in this state or ran off the edge:
			if (i < 0 || stateCount[1] > maxCount) {
				return Float.NaN;
			}
			while (i >= 0 && !image.get(centerJ, i)
					&& stateCount[0] <= maxCount) {
				stateCount[0]++;
				i--;
			}
			if (stateCount[0] > maxCount) {
				return Float.NaN;
			}

			// Now also count down from center
			i = startI + 1;
			while (i < maxI && image.get(centerJ, i)
					&& stateCount[1] <= maxCount) {
				stateCount[1]++;
				i++;
			}
			if (i == maxI || stateCount[1] > maxCount) {
				return Float.NaN;
			}
			while (i < maxI && !image.get(centerJ, i)
					&& stateCount[2] <= maxCount) {
				stateCount[2]++;
				i++;
			}
			if (stateCount[2] > maxCount) {
				return Float.NaN;
			}

			int stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2];
			if (5 * Math.abs(stateCountTotal - originalStateCountTotal) >= 2 * originalStateCountTotal) {
				return Float.NaN;
			}

			return foundPatternCross(stateCount) ? centerFromEnd(stateCount, i)
					: Float.NaN;
		}

		/**
		 * <p>
		 * This is called when a horizontal scan finds a possible alignment
		 * pattern. It will cross check with a vertical scan, and if successful,
		 * will see if this pattern had been found on a previous horizontal
		 * scan. If so, we consider it confirmed and conclude we have found the
		 * alignment pattern.
		 * </p>
		 * 
		 * @param stateCount
		 *            reading state module counts from horizontal scan
		 * @param i
		 *            row where alignment pattern may be found
		 * @param j
		 *            end of possible alignment pattern in row
		 * @return {@link AlignmentPattern} if we have found the same pattern
		 *         twice, or null if not
		 */
		private AlignmentPattern handlePossibleCenter(int[] stateCount, int i,
				int j) {
			int stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2];
			float centerJ = centerFromEnd(stateCount, j);
			float centerI = crossCheckVertical(i, (int) centerJ,
					2 * stateCount[1], stateCountTotal);
			if (!Float.isNaN(centerI)) {
				float estimatedModuleSize = (float) (stateCount[0]
						+ stateCount[1] + stateCount[2]) / 3.0f;
				for (AlignmentPattern center : possibleCenters) {
					// Look for about the same center and module size:
					if (center.aboutEquals(estimatedModuleSize, centerI,
							centerJ)) {
						return center.combineEstimate(centerI, centerJ,
								estimatedModuleSize);
					}
				}
				// Hadn't found this before; save it
				AlignmentPattern point = new AlignmentPattern(centerJ, centerI,
						estimatedModuleSize);
				possibleCenters.add(point);
				if (resultPointCallback != null) {
					resultPointCallback.foundPossibleResultPoint(point);
				}
			}
			return null;
		}

	}

	private static final class AlignmentPattern extends ResultPoint {

		private final float estimatedModuleSize;

		AlignmentPattern(float posX, float posY, float estimatedModuleSize) {
			super(posX, posY);
			this.estimatedModuleSize = estimatedModuleSize;
		}

		/**
		 * <p>
		 * Determines if this alignment pattern "about equals" an alignment
		 * pattern at the stated position and size -- meaning, it is at nearly
		 * the same center with nearly the same size.
		 * </p>
		 */
		boolean aboutEquals(float moduleSize, float i, float j) {
			if (Math.abs(i - getY()) <= moduleSize
					&& Math.abs(j - getX()) <= moduleSize) {
				float moduleSizeDiff = Math.abs(moduleSize
						- estimatedModuleSize);
				return moduleSizeDiff <= 1.0f
						|| moduleSizeDiff <= estimatedModuleSize;
			}
			return false;
		}

		/**
		 * Combines this object's current estimate of a finder pattern position
		 * and module size with a new estimate. It returns a new
		 * {@code FinderPattern} containing an average of the two.
		 */
		AlignmentPattern combineEstimate(float i, float j, float newModuleSize) {
			float combinedX = (getX() + j) / 2.0f;
			float combinedY = (getY() + i) / 2.0f;
			float combinedModuleSize = (estimatedModuleSize + newModuleSize) / 2.0f;
			return new AlignmentPattern(combinedX, combinedY,
					combinedModuleSize);
		}

	}

}
