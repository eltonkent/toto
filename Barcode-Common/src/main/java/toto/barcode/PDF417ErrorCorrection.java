package toto.barcode;


final class PDF417ErrorCorrection {

	private final PDF417ModulusGF field;

	public PDF417ErrorCorrection() {
		this.field = PDF417ModulusGF.PDF417_GF;
	}

	/**
	 * @return number of errors
	 */
	public int decode(int[] received, int numECCodewords, int[] erasures)
			throws BarCodeChecksumException {

		PDF417ModulusPoly poly = new PDF417ModulusPoly(field, received);
		int[] S = new int[numECCodewords];
		boolean error = false;
		for (int i = numECCodewords; i > 0; i--) {
			int eval = poly.evaluateAt(field.exp(i));
			S[numECCodewords - i] = eval;
			if (eval != 0) {
				error = true;
			}
		}

		if (!error) {
			return 0;
		}

		PDF417ModulusPoly knownErrors = field.getOne();
		for (int erasure : erasures) {
			int b = field.exp(received.length - 1 - erasure);
			// Add (1 - bx) term:
			PDF417ModulusPoly term = new PDF417ModulusPoly(field, new int[] {
					field.subtract(0, b), 1 });
			knownErrors = knownErrors.multiply(term);
		}

		PDF417ModulusPoly syndrome = new PDF417ModulusPoly(field, S);
		// syndrome = syndrome.multiply(knownErrors);

		PDF417ModulusPoly[] sigmaOmega = runEuclideanAlgorithm(
				field.buildMonomial(numECCodewords, 1), syndrome,
				numECCodewords);
		PDF417ModulusPoly sigma = sigmaOmega[0];
		PDF417ModulusPoly omega = sigmaOmega[1];

		// sigma = sigma.multiply(knownErrors);

		int[] errorLocations = findErrorLocations(sigma);
		int[] errorMagnitudes = findErrorMagnitudes(omega, sigma,
				errorLocations);

		for (int i = 0; i < errorLocations.length; i++) {
			int position = received.length - 1 - field.log(errorLocations[i]);
			if (position < 0) {
				throw BarCodeChecksumException.getChecksumInstance();
			}
			received[position] = field.subtract(received[position],
					errorMagnitudes[i]);
		}
		return errorLocations.length;
	}

	private PDF417ModulusPoly[] runEuclideanAlgorithm(PDF417ModulusPoly a,
			PDF417ModulusPoly b, int R) throws BarCodeChecksumException {
		// Assume a's degree is >= b's
		if (a.getDegree() < b.getDegree()) {
			PDF417ModulusPoly temp = a;
			a = b;
			b = temp;
		}

		PDF417ModulusPoly rLast = a;
		PDF417ModulusPoly r = b;
		PDF417ModulusPoly tLast = field.getZero();
		PDF417ModulusPoly t = field.getOne();

		// Run Euclidean algorithm until r's degree is less than R/2
		while (r.getDegree() >= R / 2) {
			PDF417ModulusPoly rLastLast = rLast;
			PDF417ModulusPoly tLastLast = tLast;
			rLast = r;
			tLast = t;

			// Divide rLastLast by rLast, with quotient in q and remainder in r
			if (rLast.isZero()) {
				// Oops, Euclidean algorithm already terminated?
				throw BarCodeChecksumException.getChecksumInstance();
			}
			r = rLastLast;
			PDF417ModulusPoly q = field.getZero();
			int denominatorLeadingTerm = rLast
					.getCoefficient(rLast.getDegree());
			int dltInverse = field.inverse(denominatorLeadingTerm);
			while (r.getDegree() >= rLast.getDegree() && !r.isZero()) {
				int degreeDiff = r.getDegree() - rLast.getDegree();
				int scale = field.multiply(r.getCoefficient(r.getDegree()),
						dltInverse);
				q = q.add(field.buildMonomial(degreeDiff, scale));
				r = r.subtract(rLast.multiplyByMonomial(degreeDiff, scale));
			}

			t = q.multiply(tLast).subtract(tLastLast).negative();
		}

		int sigmaTildeAtZero = t.getCoefficient(0);
		if (sigmaTildeAtZero == 0) {
			throw BarCodeChecksumException.getChecksumInstance();
		}

		int inverse = field.inverse(sigmaTildeAtZero);
		PDF417ModulusPoly sigma = t.multiply(inverse);
		PDF417ModulusPoly omega = r.multiply(inverse);
		return new PDF417ModulusPoly[] { sigma, omega };
	}

	private int[] findErrorLocations(PDF417ModulusPoly errorLocator)
			throws BarCodeChecksumException {
		// This is a direct application of Chien's search
		int numErrors = errorLocator.getDegree();
		int[] result = new int[numErrors];
		int e = 0;
		for (int i = 1; i < field.getSize() && e < numErrors; i++) {
			if (errorLocator.evaluateAt(i) == 0) {
				result[e] = field.inverse(i);
				e++;
			}
		}
		if (e != numErrors) {
			throw BarCodeChecksumException.getChecksumInstance();
		}
		return result;
	}

	private int[] findErrorMagnitudes(PDF417ModulusPoly errorEvaluator,
			PDF417ModulusPoly errorLocator, int[] errorLocations) {
		int errorLocatorDegree = errorLocator.getDegree();
		int[] formalDerivativeCoefficients = new int[errorLocatorDegree];
		for (int i = 1; i <= errorLocatorDegree; i++) {
			formalDerivativeCoefficients[errorLocatorDegree - i] = field
					.multiply(i, errorLocator.getCoefficient(i));
		}
		PDF417ModulusPoly formalDerivative = new PDF417ModulusPoly(field,
				formalDerivativeCoefficients);

		// This is directly applying Forney's Formula
		int s = errorLocations.length;
		int[] result = new int[s];
		for (int i = 0; i < s; i++) {
			int xiInverse = field.inverse(errorLocations[i]);
			int numerator = field.subtract(0,
					errorEvaluator.evaluateAt(xiInverse));
			int denominator = field.inverse(formalDerivative
					.evaluateAt(xiInverse));
			result[i] = field.multiply(numerator, denominator);
		}
		return result;
	}
}
