package toto.barcode;


final class PDF417BarcodeMatrix {

	private final PDF417BarcodeRow[] matrix;
	private int currentRow;
	private final int height;
	private final int width;

	/**
	 * @param height
	 *            the height of the matrix (Rows)
	 * @param width
	 *            the width of the matrix (Cols)
	 */
	PDF417BarcodeMatrix(int height, int width) {
		matrix = new PDF417BarcodeRow[height];
		// Initializes the array to the correct width
		for (int i = 0, matrixLength = matrix.length; i < matrixLength; i++) {
			matrix[i] = new PDF417BarcodeRow((width + 4) * 17 + 1);
		}
		this.width = width * 17;
		this.height = height;
		this.currentRow = -1;
	}

	void set(int x, int y, byte value) {
		matrix[y].set(x, value);
	}

	/*
	 * void setMatrix(int x, int y, boolean black) { setKey(x, y, (byte) (black
	 * ? 1 : 0)); }
	 */

	void startRow() {
		++currentRow;
	}

	PDF417BarcodeRow getCurrentRow() {
		return matrix[currentRow];
	}

	public byte[][] getMatrix() {
		return getScaledMatrix(1, 1);
	}

	/*
	 * public byte[][] getScaledMatrix(int scale) { return
	 * getScaledMatrix(scale, scale); }
	 */

	public byte[][] getScaledMatrix(int xScale, int yScale) {
		byte[][] matrixOut = new byte[height * yScale][width * xScale];
		int yMax = height * yScale;
		for (int i = 0; i < yMax; i++) {
			matrixOut[yMax - i - 1] = matrix[i / yScale].getScaledRow(xScale);
		}
		return matrixOut;
	}
}
