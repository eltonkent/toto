package toto.barcode;

public final class FinderPattern {

	private final int value;
	private final int[] startEnd;
	private final ResultPoint[] resultPoints;

	public FinderPattern(final int value, final int[] startEnd,
			final int start, final int end, final int rowNumber) {
		this.value = value;
		this.startEnd = startEnd;
		this.resultPoints = new ResultPoint[] {
				new ResultPoint(start, rowNumber),
				new ResultPoint(end, rowNumber), };
	}

	public int getValue() {
		return value;
	}

	public int[] getStartEnd() {
		return startEnd;
	}

	public ResultPoint[] getResultPoints() {
		return resultPoints;
	}

	@Override
	public boolean equals(final Object o) {
		if (!(o instanceof FinderPattern)) {
			return false;
		}
		final FinderPattern that = (FinderPattern) o;
		return value == that.value;
	}

	@Override
	public int hashCode() {
		return value;
	}

}