package toto.barcode;

/**
 * Runtime exception when generating/reading RAGE barcodes.
 */
public class RBarcodeException extends RuntimeException {
	public RBarcodeException(final String message) {
		super(message);

	}

	private static final long serialVersionUID = 1L;

	private int type;

	public RBarcodeException(final int type) {
		super();
		this.type = type;
	}

	public RBarcodeException(final int type, final String detailMessage,
			final Throwable throwable) {
		super(detailMessage, throwable);
		this.type = type;
	}

	public RBarcodeException(final int type, final String detailMessage) {
		super(detailMessage);
		this.type = type;
	}

	public RBarcodeException(final int type, final Throwable throwable) {
		super(throwable);
		this.type = type;
	}

	public int getType() {
		return type;
	}
}
