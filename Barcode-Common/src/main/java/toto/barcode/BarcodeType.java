package toto.barcode;

/**
 * Represents the various Barcode types supported.
 */
public enum BarcodeType {
	/**
	 * Intermec <a href="http://en.wikipedia.org/wiki/Code_11">Code11</a>
	 * implementation
	 * <p>
	 * Validation Regex - "^[-0-9]+$"
	 * </p>
	 */
	CODE11(1),
	/**
	 * Code 2 of 5 Matrix. Also known as Standard code 2 of 5.
	 */
	C25MATRIX(2), C25INTER(3), C25IATA(4), C25LOGIC(6), C25IND(7),
	/**
	 * Code 39 extended. Also known as Code 39+, Code 39e.
	 */
	EXCODE39(9),
	/**
	 * Also known as UCC/EAN-128
	 */
	EAN128(16), DPLEIT(21), DPIDENT(22),
	/**
	 * Code 16K (EN 12323)
	 * <p>
	 * Validation Regex - "^[\u0000-\u00FF]+$"
	 * </p>
	 */
	CODE16K(23), CODE49(24), FLAT(28), RSS_LTD(30), TELEPEN(32), POSTNET(40), MSI_PLESSEY(
			47), FIM(49), LOGMARS(50), PHARMA(51),
	/**
	 * <p>
	 * Validation Regex - "^[0-9]+$"
	 * </p>
	 */
	PZN(52), PHARMA_TWO(53), PDF417TRUNC(56),
	/**
	 * Code 128 symbology 60
	 * <p>
	 * Validation Regex - "^[\u0000-\u00FF]+$"
	 * </p>
	 */
	CODE128B(60), AUSPOST(63), AUS_REPLY(66), AUS_ROUTE(67), AUS_REDIRECT(68),
	/**
	 * ISBN barcode. also known as Bookland EAN-13
	 */
	ISBNX(69),
	/**
	 * Royal Mail 4 State.
	 * <p>
	 * Validation Regex - "^[0-9A-Z]*"
	 * </p>
	 */
	RM4SCC(70), CODABLOCKF(74), NVE18(75),
	/**
	 * <p>
	 * Validation Regex - "^[0-9A-Z]*"
	 * </p>
	 */
	JAPANPOST(76), KOREAPOST(77), RSS14STACK(79), RSS14STACK_OMNI(80), RSS_EXPSTACK(
			81), PLANET(82), MICROPDF417(84), ONECODE(85), PLESSEY(86), TELEPEN_NUM(
			87),
	/**
	 * Dutch Post KIX Code
	 * <p>
	 * Validation Regex - "^[0-9A-Z]*"
	 * </p>
	 */
	KIX(90), DAFT(93),
	/**
	 * Micro QR Code (ISO 18004)
	 */
	MICRO_QRCODE(97), HIBC_128(98), HIBC_39(99), HIBC_DM(102), HIBC_QR(104), HIBC_PDF(
			106), HIBC_MICPDF(108), HIBC_BLOCKF(110), HIBC_AZTEC(112), AZRUNE(
			128),
	/**
	 * Code 32 barcode. also known as Italian ministry of health barcode.
	 * <p>
	 * Validation Regex - "^[0-9]+$"
	 * </p>
	 */
	CODE32(129), EANX_CC(130), EAN128_CC(131), RSS14_CC(132), RSS_LTD_CC(133), RSS_EXP_CC(
			134), UPCA_CC(135), UPCE_CC(136), RSS14STACK_CC(137), RSS14_OMNI_CC(
			138), RSS_EXPSTACK_CC(139), CHANNEL(140), CODEONE(141),
	/**
	 * Grid Matrix
	 * <p>
	 * Validation Regex - "^[\u0000-\u00FF]+$"
	 * </p>
	 */
	GRIDMATRIX(142), DATAMATRIX_SQUARE(100),
	/**
	 * Aztec 2D barcode format. *
	 * <p>
	 * Validation Regex - "^[\u0000-\u00FF]+$"
	 * </p>
	 */
	AZTEC(92),

	/**
	 * CODABAR 1D format. Also known as Monarch, ABC Codabar, USD-4, Ames.
	 */
	CODABAR(18),

	/**
	 * Code 39 1D format. Also known as Code 3 of 9.
	 */
	CODE_39(8),

	/**
	 * Code 93 1D format.
	 */
	CODE_93(25),

	/**
	 * Code 128 1D format.
	 * <p>
	 * Validation Regex - "^[\u0000-\u00FF]+$"
	 * </p>
	 */
	CODE_128(20),

	/**
	 * Data Matrix 2D barcode format. Also known as Semacode. *
	 * <p>
	 * Validation Regex - "^[\u0000-\u00FF]+$"
	 * </p>
	 */
	DATA_MATRIX(71),

	/**
	 * Data bar format.
	 */
	DATA_BAR(-1),

	/**
	 * EAN-8 1D format.
	 */
	EAN_8(-1),

	/**
	 * EAN-13 1D format.
	 */
	EAN_13(72),

	/**
	 * ITF (Interleaved Two of Five) 1D format. Also known as Case Code.
	 */
	ITF(89),

	/**
	 * MaxiCode 2D barcode format.
	 */
	MAXICODE(57),

	/**
	 * PDF417 format.
	 * <p>
	 * Validation Regex - "^[\u0000-\u00FF]+$"
	 * </p>
	 */
	PDF_417(55),

	/**
	 * QR Code 2D barcode format.
	 * <p>
	 * <b>Data Capacity:</b><br/>
	 * Numeric Only:<b>7,089 characters</b><br/>
	 * Alphanumeric:<b>4,296 characters</b><br/>
	 * <p/>
	 */
	QR_CODE(58),

	/**
	 * RSS 14. Also known as GS1 DataBar-14
	 */
	RSS_14(29),

	/**
	 * RSS EXPANDED
	 */
	RSS_EXPANDED(31),

	/**
	 * UPC-A 1D format.
	 */
	UPC_A(34),

	/**
	 * UPC-E 1D format.
	 */
	UPC_E(37),

	/**
	 * UPC/EAN extension format. Not a stand-alone format.
	 */
	UPC_EAN_EXTENSION(13);

	private int symbol;

	private BarcodeType(final int symbol) {
		this.symbol = symbol;
	}

	public int getSymbol() {
		return symbol;
	}
}
