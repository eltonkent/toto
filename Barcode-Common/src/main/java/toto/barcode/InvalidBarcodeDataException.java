package toto.barcode;


/**
 * Created by Elton Kent on 12/27/13.
 */
public class InvalidBarcodeDataException extends RBarcodeException {
	public InvalidBarcodeDataException(String message) {
		super(message);
	}
}
