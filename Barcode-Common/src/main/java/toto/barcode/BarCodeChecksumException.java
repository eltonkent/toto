package toto.barcode;


/**
 * Thrown when a barcode was successfully detected and decoded, but was not
 * returned because its checksum feature failed.
 * 
 */
final class BarCodeChecksumException extends ReaderException {

	private static final BarCodeChecksumException instance = new BarCodeChecksumException();

	private BarCodeChecksumException() {
		// do nothing
	}

	static BarCodeChecksumException getChecksumInstance() {
		return instance;
	}

}