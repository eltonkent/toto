package toto.barcode;

import toto.geom2d.Point;

/**
 * <p>
 * Encapsulates a point of interest in an image containing a barcode. Typically,
 * this would be the location of a finder pattern or the corner of the barcode,
 * for example.
 * </p>
 * 
 */
class ResultPoint {

	private final float x;
	private final float y;

	ResultPoint(final float x, final float y) {
		this.x = x;
		this.y = y;
	}

	final float getX() {
		return x;
	}

	final float getY() {
		return y;
	}

	@Override
	public final boolean equals(final Object other) {
		if (other instanceof ResultPoint) {
			final ResultPoint otherPoint = (ResultPoint) other;
			return x == otherPoint.x && y == otherPoint.y;
		}
		return false;
	}

	@Override
	public final int hashCode() {
		return 31 * Float.floatToIntBits(x) + Float.floatToIntBits(y);
	}

	@Override
	public final String toString() {
		final StringBuilder result = new StringBuilder(25);
		result.append('(');
		result.append(x);
		result.append(',');
		result.append(y);
		result.append(')');
		return result.toString();
	}

	/**
	 * <p>
	 * Orders an array of three ResultPoints in an order [A,B,C] such that AB <
	 * AC and BC < AC and the angle between BC and BA is less than 180 degrees.
	 */
	public static void orderBestPatterns(final ResultPoint[] patterns) {

		// Find distances between pattern centers
		final float zeroOneDistance = distance(patterns[0], patterns[1]);
		final float oneTwoDistance = distance(patterns[1], patterns[2]);
		final float zeroTwoDistance = distance(patterns[0], patterns[2]);

		ResultPoint pointA;
		ResultPoint pointB;
		ResultPoint pointC;
		// Assume one closest to other two is B; A and C will just be guesses at
		// first
		if (oneTwoDistance >= zeroOneDistance
				&& oneTwoDistance >= zeroTwoDistance) {
			pointB = patterns[0];
			pointA = patterns[1];
			pointC = patterns[2];
		} else if (zeroTwoDistance >= oneTwoDistance
				&& zeroTwoDistance >= zeroOneDistance) {
			pointB = patterns[1];
			pointA = patterns[0];
			pointC = patterns[2];
		} else {
			pointB = patterns[2];
			pointA = patterns[0];
			pointC = patterns[1];
		}

		// Use cross product to figure out whether A and C are correct or
		// flipped.
		// This asks whether BC x BA has a positive z component, which is the
		// arrangement
		// we want for A, B, C. If it's negative, then we've got it flipped
		// around and
		// should swap A and C.
		if (crossProductZ(pointA, pointB, pointC) < 0.0f) {
			final ResultPoint temp = pointA;
			pointA = pointC;
			pointC = temp;
		}

		patterns[0] = pointA;
		patterns[1] = pointB;
		patterns[2] = pointC;
	}

	/**
	 * @return distance between two points
	 */
	public static float distance(final ResultPoint pattern1,
			final ResultPoint pattern2) {
		return Point.distance(pattern1.x, pattern1.y, pattern2.x, pattern2.y);
	}

	/**
	 * Returns the z component of the cross product between vectors BC and BA.
	 */
	private static float crossProductZ(final ResultPoint pointA,
			final ResultPoint pointB, final ResultPoint pointC) {
		final float bX = pointB.x;
		final float bY = pointB.y;
		return ((pointC.x - bX) * (pointA.y - bY))
				- ((pointC.y - bY) * (pointA.x - bX));
	}

}
