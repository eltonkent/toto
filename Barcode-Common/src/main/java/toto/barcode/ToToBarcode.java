package toto.barcode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import toto.async.AsyncCallback;
import toto.async.AsyncClient;
import toto.async.AsyncOperation;
import toto.bitmap.BinaryBitmap;
import toto.bitmap.HybridBinarizer;
import toto.bitmap.PlanarYUVLuminanceSource;
import toto.bitmap.ToToBitmapException;
import toto.bitmap.nativ.ToToNativeBitmap;
import toto.bitmap.utils.BitmapUtils;
import toto.graphics.color.convert.RGBConverter;
import toto.io.file.FileUtils;
import toto.util.collections.matrix.BitMatrix;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;

/**
 * Barcode encoding/decoding utility.
 * <p>
 * Contains both native and java based implementations.
 * </p>
 */
public final class ToToBarcode {

	/**
	 * Hints that help with encoding a barcode.
	 * <p>
	 * Not all provided hints are honored by all barcode types.
	 * </p>
	 * 
	 * @author Mobifluence Interactive
	 * 
	 */
	public enum EncodeHints {
		/**
		 * Width of the encoded bitmap. This hint is mandatory
		 */
		WIDTH,
		/**
		 * Height of the encoded bitmap. This hint is mandatory
		 */
		HEIGHT,
		/**
		 * Background color of the barcode.
		 * <p>
		 * The default is WHITE.
		 * </p>
		 */
		BG_COLOR,
		/**
		 * Foreground color of the barcode.
		 * <p>
		 * The default is BLACK.
		 * </p>
		 */
		FG_COLOR,
		/**
		 * Barcode border width
		 */
		BORDER_WIDTH,
		/**
		 * Barcode bitmap scale.
		 * <p>
		 * <ul>
		 * <li>0 - Tiny</li>
		 * <li>1 - Small</li>
		 * <li>2 - Medium</li>
		 * <li>3 - Large</li>
		 * <li>4 - Very Large</li>
		 * <li>5 - Huge</li>
		 * </ul>
		 * </p>
		 */
		SCALE,
		/**
		 * Set the android bitmap config. Default is {@link Config#ARGB_8888}
		 * <p>
		 * <ul>
		 * <li>0 - {@link Config#ARGB_8888}</li>
		 * <li>1 - {@link Config#ALPHA_8}</li>
		 * <li>2 - {@link Config#ARGB_4444}</li>
		 * <li>3 - {@link Config#RGB_565}</li>
		 * </ul>
		 * </p>
		 */
		BITMAP_CONFIG,
		/**
		 * Size of individual barcode symbols.
		 * <p>
		 * <ul>
		 * <li>0 - Automatic</li>
		 * <li>1 - 10x10</li>
		 * <li>2 - 12x12</li>
		 * <li>3 - 14x14</li>
		 * <li>4 - 16x16</li>
		 * <li>5 - 18x18</li>
		 * <li>6 - 20x20</li>
		 * <li>7 - 22x22</li>
		 * <li>8 - 24x24</li>
		 * <li>9 - 32x32</li>
		 * <li>10 - 36x36</li>
		 * <li>11 - 40x40</li>
		 * <li>12 - 44x44</li>
		 * <li>13 - 48x48</li>
		 * <li>14 - 52x52</li>
		 * <li>15 - 72x72</li>
		 * <li>16 - 64x64</li>
		 * <li>17 - 88x88</li>
		 * <li>18 - 80x80</li>
		 * <li>19 - 104x104</li>
		 * <li>20 - 96x96</li>
		 * <li>21 - 132x132</li>
		 * <li>22 - 120x120</li>
		 * <li>23 - 8x18</li>
		 * <li>24 - 144x144</li>
		 * <li>25 - 12x26</li>
		 * <li>26 - 8x32</li>
		 * <li>27 - 16x36</li>
		 * <li>28 - 12x36</li>
		 * <li>29 - 16x48</li>
		 * </ul>
		 * </p>
		 * 
		 */
		SYMBOL_SIZE,
		/**
		 * Barcode input mode.
		 * <ul>
		 * <li>0 - DATA_MODE</li>
		 * <li>1 - UNICODE_MODE</li>
		 * <li>2 - GS1_MODE</li>
		 * <li>3 - KANJI_MODE</li>
		 * <li>4 - SJIS_MODE</li>
		 * </ul>
		 */
		INPUT_MODE,
		/**
		 * Hide human readable text that is usually below the generated barcode.
		 * <ul>
		 * <li>0 - Hide</li>
		 * <li>1 - Show</li>
		 * </ul>
		 */
		HIDE_HRT
	}

	/**
	 * 
	 * @param context
	 *            application context
	 * @param type
	 *            barcode type
	 * @param data
	 *            to encode
	 * @param width
	 *            bitmap width
	 * @param height
	 *            bitmap height
	 * @return Bitmap if the encoding was successful or null. throws
	 *         RBarcodeException
	 */
	public static Bitmap encodeNative(final Context context,
			final BarcodeType type, final String data, final int width,
			final int height) {
		final Map<EncodeHints, Integer> hints = new HashMap<ToToBarcode.EncodeHints, Integer>();
		hints.put(EncodeHints.WIDTH, width);
		hints.put(EncodeHints.HEIGHT, height);
		return encodeNative(context, type, data, hints);
	}

	/**
	 * <p>
	 * The {@link EncodeHints#SCALE} is used to determine barcode size.
	 * </p>
	 * 
	 * @param context
	 * @param type
	 * @param data
	 * @param encodingHints
	 * @return
	 * @throws RBarcodeException
	 */
	public static Bitmap encodeNative(final Context context,
			final BarcodeType type, final String data,
			final Map<EncodeHints, Integer> encodingHints)
			throws RBarcodeException {
		Bitmap bitmap = null;
		final File cacheDir = context.getCacheDir();
		final File tmpFile = new File(cacheDir, System.currentTimeMillis()
				+ ".png");
		try {
			encodeToFileNative(tmpFile, type, data, encodingHints);
			bitmap = BitmapFactory.decodeFile(tmpFile.getAbsolutePath());
		} catch (final IOException e) {
			e.printStackTrace();
		}
		FileUtils.deleteQuietly(tmpFile);
		return bitmap;
	}

	/**
	 * Encode barcode to a supported image file. Uses native code.
	 * <p>
	 * The supported output formats are PNG, SVG, EPS, TXT. The
	 * {@link EncodeHints#SCALE} is used to determine barcode size.
	 * </p>
	 * 
	 * @param file
	 * @param type
	 * @param data
	 * @param encodingHints
	 * @throws RBarcodeException
	 */
	public static void encodeToFileNative(final File file,
			final BarcodeType type, final String data,
			final Map<EncodeHints, Integer> encodingHints)
			throws RBarcodeException, IOException {
		analyseHints(encodingHints);
		final BarcodeWriter builder = new BarcodeWriter();
		builder.setSymbology(type.getSymbol());
		final int bgColor = encodingHints.containsKey(EncodeHints.BG_COLOR) ? encodingHints
				.get(EncodeHints.BG_COLOR) : 0xFFFFFFFF;
		final int fgColor = encodingHints.containsKey(EncodeHints.BG_COLOR) ? encodingHints
				.get(EncodeHints.FG_COLOR) : 0xFF000000;
		final int w = encodingHints.get(EncodeHints.WIDTH);
		final int h = encodingHints.get(EncodeHints.HEIGHT);
		builder.setFgcolour(fgColor);
		builder.setBgcolour(bgColor);
		builder.setBitmapWidth(w);
		builder.setBitmapHeight(h);
		int scale = 8;
		if (encodingHints.containsKey(EncodeHints.SCALE)) {
			scale = encodingHints.get(EncodeHints.SCALE);
			if (scale == 0) {
				scale = 2;
			}
		}
		if (encodingHints.containsKey(EncodeHints.HIDE_HRT)) {
			builder.setShow_hrt(encodingHints.get(EncodeHints.HIDE_HRT));
		}
		if (encodingHints.containsKey(EncodeHints.SYMBOL_SIZE)) {
			builder.setOption_2(encodingHints.get(EncodeHints.SYMBOL_SIZE));
		}
		builder.setScale(scale);
		builder.encode(data);
		builder.write(file, 0);
		builder.delete();
	}

	private static void analyseHints(
			final Map<EncodeHints, Integer> encodingHints) {
		if (encodingHints == null
				|| !encodingHints.containsKey(EncodeHints.WIDTH)
				|| !encodingHints.containsKey(EncodeHints.HEIGHT)) {
			throw new IllegalArgumentException(
					" encodingHints is NULL or Barcode width and height should be specified in the encoding hints.");
		}
	}

	/**
	 * Creates the requested {@link BarcodeType} on a bitmap of specified size.
	 * <p>
	 * The default background of white and foreground of black is used. To
	 * change the color, use {@link #encode(BarcodeType, String, Map)}.<br/>
	 * </p>
	 * 
	 * @param type
	 *            To generate
	 * @param data
	 *            to embed in barcode
	 * @param width
	 *            of the barcode bitmap
	 * @param height
	 *            of the barcode bitmap
	 * @return Encoded bitmap or null if the {@link BarcodeType} is'nt supported
	 *         or encoding failed.
	 * @throws BarcodeWriterException
	 */
	public static Bitmap encode(final BarcodeType type, final String data,
			final int width, final int height) throws BarcodeWriterException {
		final Map<EncodeHints, Integer> hints = new HashMap<ToToBarcode.EncodeHints, Integer>();
		hints.put(EncodeHints.WIDTH, width);
		hints.put(EncodeHints.HEIGHT, height);
		return encode(type, data, hints);
	}

	/**
	 * Create a barcode
	 * <p>
	 * 
	 * The formats currently supported for native decode are
	 * <ul>
	 * 
	 * <li>{@link BarcodeType#EAN_8}</li>
	 * <li>{@link BarcodeType#EAN_13}</li>
	 * <li>{@link BarcodeType#UPC_A}</li>
	 * <li>{@link BarcodeType#QR_CODE}</li>
	 * <li>{@link BarcodeType#CODE_39}</li>
	 * <li>{@link BarcodeType#CODE_128}</li>
	 * <li>{@link BarcodeType#ITF}</li>
	 * <li>{@link BarcodeType#PDF_417}</li>
	 * <li>{@link BarcodeType#CODABAR}</li>
	 * <li>{@link BarcodeType#DATA_MATRIX}</li>
	 * <li>{@link BarcodeType#AZTEC}</li>
	 * 
	 * </ul>
	 * </p>
	 * 
	 * @param type
	 * @param data
	 *            to embed in barcode
	 * @param encodingHints
	 *            Information to help with encoding.
	 * @throws IllegalArgumentException
	 *             if <code>encodingHints</code> is null.
	 * @throws IllegalArgumentException
	 *             if <code>encodingHints</code> doesnt contain
	 *             {@link EncodeHints#WIDTH} or {@link EncodeHints#HEIGHT}
	 * @throws IllegalArgumentException
	 *             If the {@link BarcodeType} isnt supported
	 * @return
	 * @throws BarcodeWriterException
	 */
	public static Bitmap encode(final BarcodeType type, final String data,
			final Map<EncodeHints, Integer> encodingHints)
			throws BarcodeWriterException {
		analyseHints(encodingHints);
		Writer writer = null;
		switch (type) {
		case EAN_8:
			writer = new EAN8Writer();
			break;
		case EAN_13:
			writer = new EAN13Writer();
			break;
		case UPC_A:
			writer = new UPCAWriter();
			break;
		case QR_CODE:
			writer = new QRCodeWriter();
			break;
		case CODE_39:
			writer = new Code39Writer();
			break;
		case CODE_128:
			writer = new Code128Writer();
			break;
		case ITF:
			writer = new ITFWriter();
			break;
		case PDF_417:
			writer = new PDF417Writer();
			break;
		case CODABAR:
			writer = new CodaBarWriter();
			break;
		case DATA_MATRIX:
			writer = new DataMatrixWriter();
			break;
		case AZTEC:
			writer = new AztecWriter();
			break;
		default:
			throw new IllegalArgumentException(
					"No encoder available for format " + type);
		}
		if (writer != null) {
			int w = encodingHints.get(EncodeHints.WIDTH);
			int h = encodingHints.get(EncodeHints.HEIGHT);
			if (encodingHints.containsKey(EncodeHints.SCALE)
					&& encodingHints.get(EncodeHints.SCALE) > 0) {
				final int scale = encodingHints.get(EncodeHints.SCALE);
				w = w * scale;
				h = h * scale;
			}
			Config config = Config.ARGB_8888;
			final int bgColor = encodingHints.containsKey(EncodeHints.BG_COLOR) ? encodingHints
					.get(EncodeHints.BG_COLOR) : 0xFFFFFFFF;
			final int fgColor = encodingHints.containsKey(EncodeHints.BG_COLOR) ? encodingHints
					.get(EncodeHints.FG_COLOR) : 0xFF000000;
			if (encodingHints.containsKey(EncodeHints.BITMAP_CONFIG)) {
				switch (encodingHints.get(EncodeHints.BITMAP_CONFIG)) {
				case 0:
					config = Config.ARGB_8888;
					break;
				case 1:
					config = Config.ALPHA_8;
					break;
				case 2:
					config = Config.ARGB_4444;
					break;
				case 3:
					config = Config.RGB_565;
					break;
				}
			}
			BitMatrix matrix;
			matrix = writer.encode(data, type, w, h);
			return BitmapUtils.toBitmap(matrix, config, bgColor, fgColor);
		}
		return null;
	}

	private static Collection<Reader> readers;

	private static void populateReaders() {
		readers = new ArrayList<Reader>();
		readers.add(new QRCodeReader());
		readers.add(new DataMatrixReader());
		readers.add(new AztecReader());
		readers.add(new PDF417Reader());
		readers.add(new MaxiCodeReader());
		readers.add(new MultiFormatOneDReader(null));

	}

	/**
	 * Decode using
	 * 
	 * @param width
	 * @param height
	 * @param data
	 * @return
	 */
	public static String decode(final int width, final int height,
			final byte[] data) {
		if (readers == null) {
			populateReaders();
		}
		final PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(
				data, width, height, 0, 0, width, height, false);
		final BinaryBitmap image;
		try {
			image = new BinaryBitmap(new HybridBinarizer(source));
		} catch (ToToBitmapException e) {
			e.printStackTrace();
			return null;
		}
		for (final Reader reader : readers) {
			try {
				return reader.decode(image, null).getText();
			} catch (final ReaderException re) {
			}
		}
		return null;
	}

	/**
	 * Decode a YUV barcode image using native code.
	 * <p>
	 * <div> The average time to read a barcode using
	 * {@link #decodeNative(int, int, byte[])} is <code>0.038 Seconds</code> and
	 * is probably the only barcode reader that can decode within the first two
	 * frames of capture. <br/>
	 * <b>Note: </b>YUV formats are the formats returned by a Camera Preview
	 * Callback. <br/>
	 * The formats currently supported for native decode are,
	 * <ul>
	 * 
	 * <li>{@link BarcodeType#QR_CODE}</li>
	 * <li>{@link BarcodeType#CODABAR}</li>
	 * <li>{@link BarcodeType#CODE_128}</li>
	 * <li>{@link BarcodeType#CODE_39}</li>
	 * <li>{@link BarcodeType#CODE_93}</li>
	 * <li>{@link BarcodeType#DATA_BAR}</li>
	 * <li>{@link BarcodeType#EAN_13}</li>
	 * <li>{@link BarcodeType#EAN_8}</li>
	 * <li>{@link BarcodeType#PDF_417}</li>
	 * 
	 * </ul>
	 * </div>
	 * </p>
	 * 
	 * @param width
	 *            of the yuv image.
	 * @param height
	 *            of the yuv image
	 * @param imgData
	 *            yuv image data
	 * @return text decoded from the barcode.
	 */
	public static String decodeNative(final int width, final int height,
			final byte[] imgData) {
		return BarcodeReader.process(width, height, imgData);
	}

	/**
	 * Decode barcode asynchronously.
	 * <p>
	 * This {@link AsyncCallback}'s {@link AsyncCallback#onSuccess(Object)}
	 * method is called when the barcode is successfully decoded.
	 * </p>
	 * 
	 * @param width
	 *            image height
	 * @param height
	 *            image width
	 * @param imgData
	 *            image yuv data
	 * @param callback
	 *            Async callback
	 */
	public static void decodeAsyncNative(final int width, final int height,
			final byte[] imgData, final AsyncCallback<String> callback) {
		AsyncClient.getReusableInstance().addTask(new AsyncOperation<String>() {

			@Override
			public boolean isResponseValid(final String t) {

				return t != null ? true : false;
			}

			@Override
			public String doOperation() throws Exception {
				return BarcodeReader.process(width, height, imgData);
			}
		}, callback);
	}

	/**
	 * Decode a rage bitmap using native code.
	 * 
	 * @param bitmap
	 * @return
	 * @see ToToJavaBitmap
	 * @see ToToNativeBitmap
	 */
	public static String decodeNative(final Bitmap bitmap) {
		final byte[] yuv = RGBConverter.toYUV(BitmapUtils.getPixels(bitmap),
				null, bitmap.getWidth(), bitmap.getHeight());
		return BarcodeReader
				.process(bitmap.getWidth(), bitmap.getHeight(), yuv);
	}

}
