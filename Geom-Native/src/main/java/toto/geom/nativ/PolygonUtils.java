package toto.geom.nativ;

import com.mi.toto.ToTo;

public class PolygonUtils{
    /**
     *
     * Convert a polygon into a series of triangles. (<a
     * href="http://en.wikipedia.org/wiki/Polygon_triangulation"
     * >Triangulation</a>)
     * <p>
     * Written in native code.
     * </p>
     *
     * @param polygon
     * @return an array of integers containing the triangles.
     */
    public static int[] triangulatePolygonNative(final int[] polygon) {
        return triangulatePolygon(polygon);
    }

    private static native int[] triangulatePolygon(int[] polygon);

    static {
        ToTo.loadLib("Geom");
    }
}