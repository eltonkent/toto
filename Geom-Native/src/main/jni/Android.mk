LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CPPFLAGS += -ffunction-sections -fdata-sections -fvisibility=hidden
LOCAL_CFLAGS += -ffunction-sections -fdata-sections
LOCAL_LDFLAGS += -Wl,--gc-sections
APP_STL := gnustl_static

LOCAL_LDLIBS +=  -llog 
#LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog
LOCAL_MODULE    := Geom
LOCAL_SRC_FILES := PolygonUtils.cpp \
	
include $(BUILD_SHARED_LIBRARY)
