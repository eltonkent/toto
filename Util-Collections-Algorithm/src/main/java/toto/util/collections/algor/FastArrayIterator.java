/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections.algor;

import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * Iterator of an Array of any type
 */

public class FastArrayIterator<T> implements Iterator<T> {
	private final FastArray<T> array;
	int index;

	public FastArrayIterator(final FastArray<T> array) {
		this.array = array;
	}

	public boolean hasNext() {
		return index < array.size;
	}

	public T next() {
		if (index >= array.size)
			throw new NoSuchElementException(String.valueOf(index));
		return array.items[index++];
	}

	public void remove() {
		index--;
		array.removeIndex(index);
	}

	public void reset() {
		index = 0;
	}
}