package test.toto.util.collections;

import test.toto.ToToTestCase;
import toto.util.collections.stack.FastStack;

public class FastStackTest extends ToToTestCase {
	public void testFastStack() {
		FastStack<String> fs = new FastStack<String>(10);
		fs.push("ToTo");
		fs.push("is");
		fs.push("awesome");
		String value = fs.pop();
		assertEquals(value, "awesome");
		value = fs.peek();
		assertEquals(value, "is");
		assertEquals(fs.size(), 2);
		assertFalse(fs.isEmpty());
		fs.pop();
		fs.pop();
		assertTrue(fs.isEmpty());
	}
}
