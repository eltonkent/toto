package toto.util.collections;

import com.mi.toto.Conditions;

/**
 * Precondition checks useful in collection implementations.
 */
public final class CollectionConditions {

	public static void checkEntryNotNull(final Object key, final Object value) {
		if (key == null) {
			throw new NullPointerException("null key in entry: null=" + value);
		} else if (value == null) {
			throw new NullPointerException("null value in entry: " + key
					+ "=null");
		}
	}

	public static int checkNonnegative(final int value, final String name) {
		if (value < 0) {
			throw new IllegalArgumentException(name
					+ " cannot be negative but was: " + value);
		}
		return value;
	}

	/**
	 * Precondition tester for {@code Iterator.remove()} that throws an
	 * exception with a consistent error message.
	 */
	public static void checkRemove(final boolean canRemove) {
		Conditions.checkState(canRemove,
				"no calls to next() since the last call to remove()");
	}
}
