/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections.queue;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * A unbounded circular queue based on array.
 * 
 */
public class CircularQueue<E> extends AbstractList<E> implements Queue<E>,
		Serializable {
	/** Minimal size of the underlying array */
	private static final int DEFAULT_CAPACITY = 4;

	/** The serialVersionUID : mandatory for serializable classes */
	private static final long serialVersionUID = 3993421269224511264L;

	/**
	 * The capacity must be a power of 2.
	 */
	private static int normalizeCapacity(final int initialCapacity) {
		int actualCapacity = 1;

		while (actualCapacity < initialCapacity) {
			actualCapacity <<= 1;
			if (actualCapacity < 0) {
				actualCapacity = 1 << 30;
				break;
			}
		}
		return actualCapacity;
	}

	private int first = 0;
	private boolean full;
	/** The initial capacity of the list */
	private final int initialCapacity;
	// XXX: This volatile keyword here is a workaround for SUN Java Compiler
	// bug,
	// which produces buggy byte code. I don't event know why adding a volatile
	// fixes the problem. Eclipse Java Compiler seems to produce correct byte
	// code.
	private volatile Object[] items;
	private int last = 0;
	private int mask;

	private int shrinkThreshold;

	/**
	 * Construct a new, empty queue.
	 */
	public CircularQueue() {
		this(DEFAULT_CAPACITY);
	}

	public CircularQueue(final int initialCapacity) {
		final int actualCapacity = normalizeCapacity(initialCapacity);
		items = new Object[actualCapacity];
		mask = actualCapacity - 1;
		this.initialCapacity = actualCapacity;
		this.shrinkThreshold = 0;
	}

	@Override
	public boolean add(final E o) {
		return offer(o);
	}

	@Override
	public void add(final int idx, final E o) {
		if (idx == size()) {
			offer(o);
			return;
		}

		checkIndex(idx);
		expandIfNeeded();

		final int realIdx = getRealIndex(idx);

		// Make a room for a new element.
		if (first < last) {
			System.arraycopy(items, realIdx, items, realIdx + 1, last - realIdx);
		} else {
			if (realIdx >= first) {
				System.arraycopy(items, 0, items, 1, last);
				items[0] = items[items.length - 1];
				System.arraycopy(items, realIdx, items, realIdx + 1,
						items.length - realIdx - 1);
			} else {
				System.arraycopy(items, realIdx, items, realIdx + 1, last
						- realIdx);
			}
		}

		items[realIdx] = o;
		increaseSize();
	}

	/**
	 * Returns the capacity of this queue.
	 */
	public int capacity() {
		return items.length;
	}

	private void checkIndex(final int idx) {
		if (idx < 0 || idx >= size()) {
			throw new IndexOutOfBoundsException(String.valueOf(idx));
		}
	}

	@Override
	public void clear() {
		if (!isEmpty()) {
			Arrays.fill(items, null);
			first = 0;
			last = 0;
			full = false;
			shrinkIfNeeded();
		}
	}

	private void decreaseSize() {
		first = (first + 1) & mask;
		full = false;
	}

	@Override
	public E element() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		return peek();
	}

	private void expandIfNeeded() {
		if (full) {
			// expand queue
			final int oldLen = items.length;
			final int newLen = oldLen << 1;
			final Object[] tmp = new Object[newLen];

			if (first < last) {
				System.arraycopy(items, first, tmp, 0, last - first);
			} else {
				System.arraycopy(items, first, tmp, 0, oldLen - first);
				System.arraycopy(items, 0, tmp, oldLen - first, last);
			}

			first = 0;
			last = oldLen;
			items = tmp;
			mask = tmp.length - 1;
			if (newLen >>> 3 > initialCapacity) {
				shrinkThreshold = newLen >>> 3;
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public E get(final int idx) {
		checkIndex(idx);
		return (E) items[getRealIndex(idx)];
	}

	private int getRealIndex(final int idx) {
		return (first + idx) & mask;
	}

	private void increaseSize() {
		last = (last + 1) & mask;
		full = first == last;
	}

	@Override
	public boolean isEmpty() {
		return (first == last) && !full;
	}

	@Override
	public boolean offer(final E item) {
		if (item == null) {
			throw new IllegalArgumentException("item");
		}

		expandIfNeeded();
		items[last] = item;
		increaseSize();
		return true;
	}

	@Override
	@SuppressWarnings("unchecked")
	public E peek() {
		if (isEmpty()) {
			return null;
		}

		return (E) items[first];
	}

	@Override
	@SuppressWarnings("unchecked")
	public E poll() {
		if (isEmpty()) {
			return null;
		}

		final Object ret = items[first];
		items[first] = null;
		decreaseSize();

		if (first == last) {
			first = last = 0;
		}

		shrinkIfNeeded();
		return (E) ret;
	}

	@Override
	public E remove() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		return poll();
	}

	@SuppressWarnings("unchecked")
	@Override
	public E remove(final int idx) {
		if (idx == 0) {
			return poll();
		}

		checkIndex(idx);

		final int realIdx = getRealIndex(idx);
		final Object removed = items[realIdx];

		// Remove a room for the removed element.
		if (first < last) {
			System.arraycopy(items, first, items, first + 1, realIdx - first);
		} else {
			if (realIdx >= first) {
				System.arraycopy(items, first, items, first + 1, realIdx
						- first);
			} else {
				System.arraycopy(items, 0, items, 1, realIdx);
				items[0] = items[items.length - 1];
				System.arraycopy(items, first, items, first + 1, items.length
						- first - 1);
			}
		}

		items[first] = null;
		decreaseSize();

		shrinkIfNeeded();
		return (E) removed;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E set(final int idx, final E o) {
		checkIndex(idx);

		final int realIdx = getRealIndex(idx);
		final Object old = items[realIdx];
		items[realIdx] = o;
		return (E) old;
	}

	private void shrinkIfNeeded() {
		final int size = size();
		if (size <= shrinkThreshold) {
			// shrink queue
			final int oldLen = items.length;
			int newLen = normalizeCapacity(size);
			if (size == newLen) {
				newLen <<= 1;
			}

			if (newLen >= oldLen) {
				return;
			}

			if (newLen < initialCapacity) {
				if (oldLen == initialCapacity) {
					return;
				}

				newLen = initialCapacity;
			}

			final Object[] tmp = new Object[newLen];

			// Copy only when there's something to copy.
			if (size > 0) {
				if (first < last) {
					System.arraycopy(items, first, tmp, 0, last - first);
				} else {
					System.arraycopy(items, first, tmp, 0, oldLen - first);
					System.arraycopy(items, 0, tmp, oldLen - first, last);
				}
			}

			first = 0;
			last = size;
			items = tmp;
			mask = tmp.length - 1;
			shrinkThreshold = 0;
		}
	}

	@Override
	public int size() {
		if (full) {
			return capacity();
		}

		if (last >= first) {
			return last - first;
		}

		return last - first + capacity();
	}

	@Override
	public String toString() {
		return "first=" + first + ", last=" + last + ", size=" + size()
				+ ", mask = " + mask;
	}
}
