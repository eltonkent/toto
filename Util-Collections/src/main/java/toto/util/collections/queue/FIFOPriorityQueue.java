package toto.util.collections.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

import toto.util.collections.queue.FIFOPriorityQueue.Prioritizable;

/**
 * Priority queue that allows you to set the priority of objects saved.
 * 
 * @see Prioritizable
 */
public class FIFOPriorityQueue<T extends Prioritizable> {

	/**
	 * Tasks with minimum priority will only be executed after all other tasks
	 * have been executed.
	 */
	public static int MIN_PRIORITY = -100;

	/**
	 * Tasks with default priority can be run in any arbitrary order w.r.t to
	 * other tasks of default priority.
	 */
	public static int DEFAULT_PRIORITY = 0;

	/**
	 * Tasks with maximum priority will be executed before all other tasks.
	 */
	public static int MAX_PRIORITY = 100;

	/**
	 * Interface for objects that should define a priority.
	 * 
	 * @see FIFOPriorityQueue#MAX_PRIORITY
	 * @see FIFOPriorityQueue#MIN_PRIORITY
	 * @see FIFOPriorityQueue#DEFAULT_PRIORITY
	 */
	public interface Prioritizable {
		int getPriority();
	}

	private final BlockingQueue<Entry<T>> _queue = new PriorityBlockingQueue<Entry<T>>();
	private final AtomicLong _sequenceNumber = new AtomicLong();

	private final int _defaultPriority;

	public FIFOPriorityQueue() {
		this(DEFAULT_PRIORITY);
	}

	public FIFOPriorityQueue(final int defaultPriority) {
		_defaultPriority = defaultPriority;
	}

	public void add(final T value) {
		final int priority = value.getPriority(); /*
												 * instanceof Prioritizable ?
												 * ((Prioritizable) value)
												 * .getPriority() :
												 * _defaultPriority;
												 */
		_queue.add(new Entry<T>(_sequenceNumber.getAndIncrement(), priority,
				value));
	}

	public T poll() {
		final Entry<T> entry = _queue.poll();
		return entry == null ? null : entry._value;
	}

	private static class Entry<T> implements Comparable<Entry<T>> {
		private final long _sequenceNumber;
		private final int _priority;
		private final T _value;

		private Entry(final long sequenceNumber, final int priority,
				final T value) {
			_sequenceNumber = sequenceNumber;
			_priority = priority;
			_value = value;
		}

		@Override
		public int compareTo(final Entry<T> o) {
			final int comp = compare(o._priority, _priority);
			return comp == 0 ? compare(_sequenceNumber, o._sequenceNumber)
					: comp;
		}

		private int compare(final long lhs, final long rhs) {
			if (lhs < rhs) {
				return -1;
			} else if (lhs > rhs) {
				return 1;
			}
			return 0;
		}
	}
}