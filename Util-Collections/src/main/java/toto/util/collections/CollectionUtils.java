/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import toto.util.collections.list.ByteArrayAsList;

import static com.mi.toto.Conditions.checkNotNull;

public class CollectionUtils {
	public static <E> List<E> asList(final E... elements) {
		if (elements == null || elements.length == 0) {
			return Collections.emptyList();
		}
		// Avoid integer overflow when a large array is passed in
		final int capacity = computeListCapacity(elements.length);
		final ArrayList<E> list = new ArrayList<E>(capacity);
		Collections.addAll(list, elements);
		return list;
	}

	private static int computeListCapacity(final int arraySize) {
		return (int) Math.min(5L + arraySize + (arraySize / 10),
				Integer.MAX_VALUE);
	}

	public static <E> Set<E> asSet(final E... elements) {
		if (elements == null || elements.length == 0) {
			return Collections.emptySet();
		}
		final LinkedHashSet<E> set = new LinkedHashSet<E>(
				elements.length * 4 / 3 + 1);
		Collections.addAll(set, elements);
		return set;
	}

    /**
     * Returns an array containing each value of {@code collection}, converted
     * to a {@code byte} value in the manner of {@link Number#byteValue}.
     *
     * <p>
     * Elements are copied from the argument collection as if by
     * {@code collection.toArray()}. Calling this method is as thread-safe as
     * calling that method.
     *
     * @param collection
     *            a collection of {@code Number} instances
     * @return an array containing the same values as {@code collection}, in the
     *         same order, converted to primitives
     * @throws NullPointerException
     *             if {@code collection} or any of its elements is null
     * @since 1.0 (parameter was {@code Collection<Byte>} before 12.0)
     */
    public static byte[] toArray(final Collection<? extends Number> collection) {
        if (collection instanceof ByteArrayAsList) {
            return ((ByteArrayAsList) collection).toByteArray();
        }

        final Object[] boxedArray = collection.toArray();
        final int len = boxedArray.length;
        final byte[] array = new byte[len];
        for (int i = 0; i < len; i++) {
            // checkNotNull for GWT (do not optimize)
            array[i] = ((Number) checkNotNull(boxedArray[i])).byteValue();
        }
        return array;
    }

    /**
     * Returns a fixed-size list backed by the specified array, similar to
     * {@link java.util.Arrays#asList(Object[])}. The list supports
     * {@link List#set(int, Object)}, but any attempt to setKey a value to
     * {@code null} will result in a {@link NullPointerException}.
     *
     * <p>
     * The returned list maintains the values, but not the identities, of
     * {@code Byte} objects written to or read from it. For example, whether
     * {@code list.get(0) == list.get(0)} is true for the returned list is
     * unspecified.
     *
     * @param backingArray
     *            the array to back the list
     * @return a list view of the array
     */
    public static List<Byte> asList(final byte... backingArray) {
        if (backingArray.length == 0) {
            return Collections.emptyList();
        }
        return new ByteArrayAsList(backingArray);
    }
}
