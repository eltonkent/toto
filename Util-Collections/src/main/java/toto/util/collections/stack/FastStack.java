/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections.stack;

import java.lang.reflect.Array;

/**
 * An array-based stack implementation.
 * <p>
 * Not thread-safe.
 * </p>
 * 
 * @see ArrayStack
 */
public final class FastStack<I> {

	private int pointer;
	private I[] stack;

	@SuppressWarnings("unchecked")
	public FastStack(final int initialCapacity) {
		stack = (I[]) new Object[initialCapacity];
	}

	public I get(final int i) {
		return stack[i];
	}

	public boolean isEmpty() {
		return !(pointer > 0);
	}

	public I peek() {
		return pointer == 0 ? null : stack[pointer - 1];
	}

	public I pop() {
		final I result = stack[--pointer];
		stack[pointer] = null;
		return result;
	}

	public void popSilently() {
		stack[--pointer] = null;
	}

	public I push(final I value) {
		if (pointer + 1 >= stack.length) {
			resizeStack(stack.length * 2);
		}
		stack[pointer++] = value;
		return value;
	}

	public I replace(final I value) {
		final I result = stack[pointer - 1];
		stack[pointer - 1] = value;
		return result;
	}

	public void replaceSilently(final I value) {
		stack[pointer - 1] = value;
	}

	private void resizeStack(final int newCapacity) {
		@SuppressWarnings("unchecked")
		final I[] newStack = (I[]) Array.newInstance(stack.getClass(),
				newCapacity);
		System.arraycopy(stack, 0, newStack, 0, Math.min(pointer, newCapacity));
		stack = newStack;
	}

	public int size() {
		return pointer;
	}

	@Override
	public String toString() {
		final StringBuffer result = new StringBuffer("[");
		for (int i = 0; i < pointer; i++) {
			if (i > 0) {
				result.append(", ");
			}
			result.append(stack[i]);
		}
		result.append(']');
		return result.toString();
	}
}
