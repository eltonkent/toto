package toto.math;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;

/**
 * Matrix class.
 * <P>
 * <div> The Matrix Class provides the fundamental operations of numerical
 * linear algebra. Various constructors create Matrices from two dimensional
 * arrays of double precision floating point numbers. Various "gets" and "sets"
 * provide access to submatrices and matrix elements. Several methods implement
 * basic matrix arithmetic, including matrix addition and multiplication, matrix
 * norms, and element-by-element array operations. Methods for reading and
 * printing matrices are also included. All the operations in this version of
 * the Matrix Class involve real matrices. Complex matrices may be handled in a
 * future version.
 * <P>
 * Five fundamental matrix decompositions, which consist of pairs or triples of
 * matrices, permutation vectors, and the like, produce results in five
 * decomposition classes. These decompositions are accessed by the Matrix class
 * to compute solutions of simultaneous linear equations, determinants, inverses
 * and other matrix functions. The five decompositions are:
 * <P>
 * <UL>
 * <LI>Cholesky Decomposition of symmetric, positive definite matrices.
 * <LI>LU Decomposition of rectangular matrices.
 * <LI>QR Decomposition of rectangular matrices.
 * <LI>Singular Value Decomposition of rectangular matrices.
 * <LI>Eigenvalue Decomposition of both symmetric and nonsymmetric square
 * matrices.
 * </UL>
 * <DL>
 * <DT><B>Example of use:</B></DT>
 * <P>
 * <DD>Solve a linear system A x = b and compute the residual norm, ||b - A x||.
 * <P>
 * 
 * <pre>
 * double[][] vals = { { 1., 2., 3 }, { 4., 5., 6. }, { 7., 8., 10. } };
 * Matrix A = new Matrix(vals);
 * Matrix b = Matrix.random(3, 1);
 * Matrix x = A.solve(b);
 * Matrix r = A.times(x).minus(b);
 * double rnorm = r.normInf();
 * </pre>
 * 
 * </DD>
 * </DL>
 * </div>
 */

public class Matrix implements Cloneable, Serializable {
	/*
	 * ------------------------ Class variables ------------------------
	 */

	/**
	 * Array for internal storage of elements.
	 * 
	 * @serial internal array storage.
	 */
	private final double[][] A;

	/** Number of rows. @serial number of rows. */
	private final int m;
	/** Number of columns. @serial number of columns. */
	private int n;

	/*
	 * ------------------------ Constructors ------------------------
	 */

	/**
	 * Construct an m-by-n matrix of zeros.
	 * 
	 * @param m
	 *            Number of rows.
	 * @param n
	 *            Number of colums.
	 */
	public Matrix(final int m, final int n) {
		this.m = m;
		this.n = n;
		A = new double[m][n];
	}

	/**
	 * Construct an m-by-n constant matrix.
	 * 
	 * @param m
	 *            Number of rows.
	 * @param n
	 *            Number of colums.
	 * @param s
	 *            Fill the matrix with this scalar value.
	 */
	public Matrix(final int m, final int n, final double s) {
		this.m = m;
		this.n = n;
		A = new double[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = s;
			}
		}
	}

	/**
	 * Construct a matrix from a 2-D array.
	 * 
	 * @param A
	 *            Two-dimensional array of doubles.
	 * @exception IllegalArgumentException
	 *                All rows must have the same length
	 * @see #constructWithCopy
	 */
	public Matrix(final double[][] A) {
		m = A.length;
		n = A[0].length;
		for (int i = 0; i < m; i++) {
			if (A[i].length != n) {
				throw new IllegalArgumentException(
						"All rows must have the same length.");
			}
		}
		this.A = A;
	}

	/**
	 * Construct a matrix quickly without checking arguments.
	 * 
	 * @param A
	 *            Two-dimensional array of doubles.
	 * @param m
	 *            Number of rows.
	 * @param n
	 *            Number of colums.
	 */
	public Matrix(final double[][] A, final int m, final int n) {
		this.A = A;
		this.m = m;
		this.n = n;
	}

	/**
	 * Construct a matrix from a one-dimensional packed array
	 * 
	 * @param vals
	 *            One-dimensional array of doubles, packed by columns (ala
	 *            Fortran).
	 * @param m
	 *            Number of rows.
	 * @exception IllegalArgumentException
	 *                Array length must be a multiple of m.
	 */
	public Matrix(final double vals[], final int m) {
		this.m = m;
		n = (m != 0 ? vals.length / m : 0);
		if (m * n != vals.length) {
			throw new IllegalArgumentException(
					"Array length must be a multiple of m.");
		}
		A = new double[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = vals[i + j * m];
			}
		}
	}

	/**
	 * Construct a matrix from a Vector of double[]
	 * 
	 * @param rows
	 *            A Vector<double[]> containing double[] items. Each double[]
	 *            item is a row of the matrix to create. All double[] items must
	 *            be of the same length.
	 */
	public Matrix(final Vector<double[]> rows, final boolean clone) {
		m = rows.size();
		n = rows.get(0).length;
		A = new double[m][n];
		if (clone) {
			for (int i = 0; i < m; i++)
				A[i] = rows.get(i).clone();
		} else {
			for (int i = 0; i < m; i++)
				A[i] = rows.get(i);
		}

		// sanity check:
		for (int i = 0; i < m; i++)
			if (A[i].length != n)
				(new IllegalArgumentException("Length of row " + i + " is "
						+ A[i].length + ". Should be " + n)).printStackTrace();
	}

	/*
	 * ------------------------ Public Methods ------------------------
	 */

	/**
	 * Construct a matrix from a copy of a 2-D array.
	 * 
	 * @param A
	 *            Two-dimensional array of doubles.
	 * @exception IllegalArgumentException
	 *                All rows must have the same length
	 */
	public static Matrix constructWithCopy(final double[][] A) {
		final int m = A.length;
		final int n = A[0].length;
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			if (A[i].length != n) {
				throw new IllegalArgumentException(
						"All rows must have the same length.");
			}
			for (int j = 0; j < n; j++) {
				C[i][j] = A[i][j];
			}
		}
		return X;
	}

	/**
	 * Make a deep copy of a matrix
	 */
	public Matrix copy() {
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[i][j] = A[i][j];
			}
		}
		return X;
	}

	/**
	 * Clone the Matrix object.
	 */
	public Object clone() {
		return this.copy();
	}

	/**
	 * Access the internal two-dimensional array.
	 * 
	 * @return Pointer to the two-dimensional array of matrix elements.
	 */
	public double[][] getArray() {
		return A;
	}

	/**
	 * Copy the internal two-dimensional array.
	 * 
	 * @return Two-dimensional array copy of matrix elements.
	 */
	public double[][] getArrayCopy() {
		final double[][] C = new double[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[i][j] = A[i][j];
			}
		}
		return C;
	}

	/**
	 * Make a one-dimensional column packed copy of the internal array.
	 * 
	 * @return Matrix elements packed in a one-dimensional array by columns.
	 */
	public double[] getColumnPackedCopy() {
		final double[] vals = new double[m * n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				vals[i + j * m] = A[i][j];
			}
		}
		return vals;
	}

	/**
	 * Make a one-dimensional row packed copy of the internal array.
	 * 
	 * @return Matrix elements packed in a one-dimensional array by rows.
	 */
	public double[] getRowPackedCopy() {
		final double[] vals = new double[m * n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				vals[i * n + j] = A[i][j];
			}
		}
		return vals;
	}

	/**
	 * Get row dimension.
	 * 
	 * @return m, the number of rows.
	 */
	public int getRowDimension() {
		return m;
	}

	/**
	 * Get column dimension.
	 * 
	 * @return n, the number of columns.
	 */
	public int getColumnDimension() {
		return n;
	}

	/**
	 * Get a single element.
	 * 
	 * @param i
	 *            Row index.
	 * @param j
	 *            Column index.
	 * @return A(i,j)
	 * @exception ArrayIndexOutOfBoundsException
	 */
	public double get(final int i, final int j) {
		return A[i][j];
	}

	/**
	 * Get a submatrix.
	 * 
	 * @param i0
	 *            Initial row index
	 * @param i1
	 *            Final row index
	 * @param j0
	 *            Initial column index
	 * @param j1
	 *            Final column index
	 * @return A(i0:i1,j0:j1)
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public Matrix getMatrix(final int i0, final int i1, final int j0,
			final int j1) {
		final Matrix X = new Matrix(i1 - i0 + 1, j1 - j0 + 1);
		final double[][] B = X.getArray();
		try {
			for (int i = i0; i <= i1; i++) {
				for (int j = j0; j <= j1; j++) {
					B[i - i0][j - j0] = A[i][j];
				}
			}
		} catch (final ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
		return X;
	}

	/**
	 * Get a submatrix.
	 * 
	 * @param r
	 *            Array of row indices.
	 * @param c
	 *            Array of column indices.
	 * @return A(r(:),c(:))
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public Matrix getMatrix(final int[] r, final int[] c) {
		final Matrix X = new Matrix(r.length, c.length);
		final double[][] B = X.getArray();
		try {
			for (int i = 0; i < r.length; i++) {
				for (int j = 0; j < c.length; j++) {
					B[i][j] = A[r[i]][c[j]];
				}
			}
		} catch (final ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
		return X;
	}

	/**
	 * Get a submatrix.
	 * 
	 * @param i0
	 *            Initial row index
	 * @param i1
	 *            Final row index
	 * @param c
	 *            Array of column indices.
	 * @return A(i0:i1,c(:))
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public Matrix getMatrix(final int i0, final int i1, final int[] c) {
		final Matrix X = new Matrix(i1 - i0 + 1, c.length);
		final double[][] B = X.getArray();
		try {
			for (int i = i0; i <= i1; i++) {
				for (int j = 0; j < c.length; j++) {
					B[i - i0][j] = A[i][c[j]];
				}
			}
		} catch (final ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
		return X;
	}

	/**
	 * Get a submatrix.
	 * 
	 * @param r
	 *            Array of row indices.
	 * @param j0
	 *            Initial column index
	 * @param j1
	 *            Final column index
	 * @return A(r(:),j0:j1)
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public Matrix getMatrix(final int[] r, final int j0, final int j1) {
		final Matrix X = new Matrix(r.length, j1 - j0 + 1);
		final double[][] B = X.getArray();
		try {
			for (int i = 0; i < r.length; i++) {
				for (int j = j0; j <= j1; j++) {
					B[i][j - j0] = A[r[i]][j];
				}
			}
		} catch (final ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
		return X;
	}

	/**
	 * Set a single element.
	 * 
	 * @param i
	 *            Row index.
	 * @param j
	 *            Column index.
	 * @param s
	 *            A(i,j).
	 * @exception ArrayIndexOutOfBoundsException
	 */
	public void set(final int i, final int j, final double s) {
		A[i][j] = s;
	}

	/**
	 * Set a submatrix.
	 * 
	 * @param i0
	 *            Initial row index
	 * @param i1
	 *            Final row index
	 * @param j0
	 *            Initial column index
	 * @param j1
	 *            Final column index
	 * @param X
	 *            A(i0:i1,j0:j1)
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public void setMatrix(final int i0, final int i1, final int j0,
			final int j1, final Matrix X) {
		try {
			for (int i = i0; i <= i1; i++) {
				for (int j = j0; j <= j1; j++) {
					A[i][j] = X.get(i - i0, j - j0);
				}
			}
		} catch (final ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
	}

	/**
	 * Set a submatrix.
	 * 
	 * @param r
	 *            Array of row indices.
	 * @param c
	 *            Array of column indices.
	 * @param X
	 *            A(r(:),c(:))
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public void setMatrix(final int[] r, final int[] c, final Matrix X) {
		try {
			for (int i = 0; i < r.length; i++) {
				for (int j = 0; j < c.length; j++) {
					A[r[i]][c[j]] = X.get(i, j);
				}
			}
		} catch (final ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
	}

	/**
	 * Set a submatrix.
	 * 
	 * @param r
	 *            Array of row indices.
	 * @param j0
	 *            Initial column index
	 * @param j1
	 *            Final column index
	 * @param X
	 *            A(r(:),j0:j1)
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public void setMatrix(final int[] r, final int j0, final int j1,
			final Matrix X) {
		try {
			for (int i = 0; i < r.length; i++) {
				for (int j = j0; j <= j1; j++) {
					A[r[i]][j] = X.get(i, j - j0);
				}
			}
		} catch (final ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
	}

	/**
	 * Set a submatrix.
	 * 
	 * @param i0
	 *            Initial row index
	 * @param i1
	 *            Final row index
	 * @param c
	 *            Array of column indices.
	 * @param X
	 *            A(i0:i1,c(:))
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public void setMatrix(final int i0, final int i1, final int[] c,
			final Matrix X) {
		try {
			for (int i = i0; i <= i1; i++) {
				for (int j = 0; j < c.length; j++) {
					A[i][c[j]] = X.get(i - i0, j);
				}
			}
		} catch (final ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
	}

	/**
	 * Matrix transpose.
	 * 
	 * @return A'
	 */
	public Matrix transpose() {
		final Matrix X = new Matrix(n, m);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[j][i] = A[i][j];
			}
		}
		return X;
	}

	/**
	 * One norm
	 * 
	 * @return maximum column sum.
	 */
	public double norm1() {
		double f = 0;
		for (int j = 0; j < n; j++) {
			double s = 0;
			for (int i = 0; i < m; i++) {
				s += Math.abs(A[i][j]);
			}
			f = Math.max(f, s);
		}
		return f;
	}

	/**
	 * Infinity norm
	 * 
	 * @return maximum row sum.
	 */
	public double normInf() {
		double f = 0;
		for (int i = 0; i < m; i++) {
			double s = 0;
			for (int j = 0; j < n; j++) {
				s += Math.abs(A[i][j]);
			}
			f = Math.max(f, s);
		}
		return f;
	}

	/**
	 * Unary minus
	 * 
	 * @return -A
	 */
	public Matrix uminus() {
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[i][j] = -A[i][j];
			}
		}
		return X;
	}

	/**
	 * C = A + B
	 * 
	 * @param B
	 *            another matrix
	 * @return A + B
	 */
	public Matrix plus(final Matrix B) {
		checkMatrixDimensions(B);
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[i][j] = A[i][j] + B.A[i][j];
			}
		}
		return X;
	}

	/**
	 * A = A + B
	 * 
	 * @param B
	 *            another matrix
	 * @return A + B
	 */
	public Matrix plusEquals(final Matrix B) {
		checkMatrixDimensions(B);
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = A[i][j] + B.A[i][j];
			}
		}
		return this;
	}

	/**
	 * C = A - B
	 * 
	 * @param B
	 *            another matrix
	 * @return A - B
	 */
	public Matrix minus(final Matrix B) {
		checkMatrixDimensions(B);
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[i][j] = A[i][j] - B.A[i][j];
			}
		}
		return X;
	}

	/**
	 * A = A - B
	 * 
	 * @param B
	 *            another matrix
	 * @return A - B
	 */
	public Matrix minusEquals(final Matrix B) {
		checkMatrixDimensions(B);
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = A[i][j] - B.A[i][j];
			}
		}
		return this;
	}

	/**
	 * Element-by-element multiplication, C = A.*B
	 * 
	 * @param B
	 *            another matrix
	 * @return A.*B
	 */
	public Matrix arrayTimes(final Matrix B) {
		checkMatrixDimensions(B);
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[i][j] = A[i][j] * B.A[i][j];
			}
		}
		return X;
	}

	/**
	 * Element-by-element multiplication in place, A = A.*B
	 * 
	 * @param B
	 *            another matrix
	 * @return A.*B
	 */
	public Matrix arrayTimesEquals(final Matrix B) {
		checkMatrixDimensions(B);
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = A[i][j] * B.A[i][j];
			}
		}
		return this;
	}

	/**
	 * Element-by-element right division, C = A./B
	 * 
	 * @param B
	 *            another matrix
	 * @return A./B
	 */
	public Matrix arrayRightDivide(final Matrix B) {
		checkMatrixDimensions(B);
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[i][j] = A[i][j] / B.A[i][j];
			}
		}
		return X;
	}

	/**
	 * Element-by-element right division in place, A = A./B
	 * 
	 * @param B
	 *            another matrix
	 * @return A./B
	 */
	public Matrix arrayRightDivideEquals(final Matrix B) {
		checkMatrixDimensions(B);
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = A[i][j] / B.A[i][j];
			}
		}
		return this;
	}

	/**
	 * Element-by-element left division, C = A.\B
	 * 
	 * @param B
	 *            another matrix
	 * @return A.\B
	 */
	public Matrix arrayLeftDivide(final Matrix B) {
		checkMatrixDimensions(B);
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[i][j] = B.A[i][j] / A[i][j];
			}
		}
		return X;
	}

	/**
	 * Element-by-element left division in place, A = A.\B
	 * 
	 * @param B
	 *            another matrix
	 * @return A.\B
	 */
	public Matrix arrayLeftDivideEquals(final Matrix B) {
		checkMatrixDimensions(B);
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = B.A[i][j] / A[i][j];
			}
		}
		return this;
	}

	/**
	 * Multiply a matrix by a scalar, C = s*A
	 * 
	 * @param s
	 *            scalar
	 * @return s*A
	 */
	public Matrix times(final double s) {
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				C[i][j] = s * A[i][j];
			}
		}
		return X;
	}

	/**
	 * Multiply a matrix by a scalar in place, A = s*A
	 * 
	 * @param s
	 *            scalar
	 * @return replace A by s*A
	 */
	public Matrix timesEquals(final double s) {
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = s * A[i][j];
			}
		}
		return this;
	}

	/**
	 * Linear algebraic matrix multiplication, A * B
	 * 
	 * @param B
	 *            another matrix
	 * @return Matrix product, A * B
	 * @exception IllegalArgumentException
	 *                Matrix inner dimensions must agree.
	 */
	public Matrix times(final Matrix B) {
		if (B.m != n) {
			throw new IllegalArgumentException(
					"Matrix inner dimensions must agree.");
		}
		final Matrix X = new Matrix(m, B.n);
		final double[][] C = X.getArray();
		final double[] Bcolj = new double[n];
		for (int j = 0; j < B.n; j++) {
			for (int k = 0; k < n; k++) {
				Bcolj[k] = B.A[k][j];
			}
			for (int i = 0; i < m; i++) {
				final double[] Arowi = A[i];
				double s = 0;
				for (int k = 0; k < n; k++) {
					s += Arowi[k] * Bcolj[k];
				}
				C[i][j] = s;
			}
		}
		return X;
	}

	/**
	 * Linear algebraic matrix multiplication, A * B B being a triangular matrix
	 * <b>Note:</b> Actually the matrix should be a <b>column orienten, upper
	 * triangular matrix</b> but use the <b>row oriented, lower triangular
	 * matrix</b> instead (transposed), because this is faster due to the easyer
	 * array access.
	 * 
	 * @param B
	 *            another matrix
	 * @return Matrix product, A * B
	 * 
	 * @exception IllegalArgumentException
	 *                Matrix inner dimensions must agree.
	 */
	public Matrix timesTriangular(final Matrix B) {
		if (B.m != n)
			throw new IllegalArgumentException(
					"Matrix inner dimensions must agree.");

		final Matrix X = new Matrix(m, B.n);
		final double[][] c = X.getArray();
		double[][] b;
		double s = 0;
		double[] Arowi;
		double[] Browj;

		b = B.getArray();
		// multiply with each row of A
		for (int i = 0; i < m; i++) {
			Arowi = A[i];

			// for all columns of B
			for (int j = 0; j < B.n; j++) {
				s = 0;
				Browj = b[j];
				// since B being triangular, this loop uses k <= j
				for (int k = 0; k <= j; k++) {
					s += Arowi[k] * Browj[k];
				}
				c[i][j] = s;
			}
		}
		return X;
	}

	/**
	 * X.diffEquals() calculates differences between adjacent columns of this
	 * matrix. Consequently the size of the matrix is reduced by one. The result
	 * is stored in this matrix object again.
	 */
	public void diffEquals() {
		double[] col = null;
		for (int i = 0; i < A.length; i++) {
			col = new double[A[i].length - 1];

			for (int j = 1; j < A[i].length; j++)
				col[j - 1] = Math.abs(A[i][j] - A[i][j - 1]);

			A[i] = col;
		}
		n--;
	}

	/**
	 * X.logEquals() calculates the natural logarithem of each element of the
	 * matrix. The result is stored in this matrix object again.
	 */
	public void logEquals() {
		for (int i = 0; i < A.length; i++)
			for (int j = 0; j < A[i].length; j++)
				A[i][j] = Math.log(A[i][j]);
	}

	/**
	 * X.powEquals() calculates the power of each element of the matrix. The
	 * result is stored in this matrix object again.
	 */
	public void powEquals(final double exp) {
		for (int i = 0; i < A.length; i++)
			for (int j = 0; j < A[i].length; j++)
				A[i][j] = Math.pow(A[i][j], exp);
	}

	/**
	 * X.powEquals() calculates the power of each element of the matrix.
	 * 
	 * @return Matrix
	 */
	public Matrix pow(final double exp) {
		final Matrix X = new Matrix(m, n);
		final double[][] C = X.getArray();

		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				C[i][j] = Math.pow(A[i][j], exp);

		return X;
	}

	/**
	 * X.thrunkAtLowerBoundariy(). All values smaller than the given one are set
	 * to this lower boundary.
	 */
	public void thrunkAtLowerBoundary(final double value) {
		for (int i = 0; i < A.length; i++)
			for (int j = 0; j < A[i].length; j++) {
				if (A[i][j] < value)
					A[i][j] = value;
			}
	}

	/**
	 * Matrix trace.
	 * 
	 * @return sum of the diagonal elements.
	 */
	public double trace() {
		double t = 0;
		for (int i = 0; i < Math.min(m, n); i++) {
			t += A[i][i];
		}
		return t;
	}

	/**
	 * Solve A*X = B
	 * 
	 * @param B
	 *            right hand side
	 * @return solution if A is square, least squares solution otherwise
	 */
	public Matrix solve(final Matrix B) {
		return (m == n ? (new LUDecomposition(this)).solve(B)
				: (new QRDecomposition(this)).solve(B));
	}

	public CholeskyDecomposition chol() {
		return new CholeskyDecomposition(this);
	}

	/**
	 * Effective numerical <a
	 * href="http://en.wikipedia.org/wiki/Rank_(linear_algebra)"> matrix
	 * rank</a>
	 * 
	 * @return Number of nonnegligible singular values.
	 */
	public int rank() {
		return new SingularValueDecomposition(this).rank();
	}

	/**
	 * Two norm <a
	 * href="http://en.wikipedia.org/wiki/Condition_number">condition number</a>
	 * 
	 * @return max(S)/min(S)
	 */
	public double cond() {
		return new SingularValueDecomposition(this).cond();
	}

	/**
	 * Return the left singular vectors
	 * <p>
	 * <a href=
	 * "http://en.wikipedia.org/wiki/Singular_value_decomposition#Singular_values.2C_singular_vectors.2C_and_their_relation_to_the_SVD"
	 * >http://en.wikipedia.org/wiki/Singular_value_decomposition#
	 * Singular_values.2C_singular_vectors.2C_and_their_relation_to_the_SVD</a>
	 * </p>
	 * 
	 * @return U
	 */
	public Matrix getU() {
		return new SingularValueDecomposition(this).getU();
	}

	/**
	 * Return the right singular vectors
	 * <p>
	 * <a href=
	 * "http://en.wikipedia.org/wiki/Singular_value_decomposition#Singular_values.2C_singular_vectors.2C_and_their_relation_to_the_SVD"
	 * >http://en.wikipedia.org/wiki/Singular_value_decomposition#
	 * Singular_values.2C_singular_vectors.2C_and_their_relation_to_the_SVD</a>
	 * </p>
	 * 
	 * @return V
	 */
	public Matrix getV() {
		return new SingularValueDecomposition(this).getV();
	}

	/**
	 * Return the diagonal matrix of singular values
	 * <p>
	 * <a href=
	 * "http://en.wikipedia.org/wiki/Singular_value_decomposition#Singular_values.2C_singular_vectors.2C_and_their_relation_to_the_SVD"
	 * >http://en.wikipedia.org/wiki/Singular_value_decomposition#
	 * Singular_values.2C_singular_vectors.2C_and_their_relation_to_the_SVD</a>
	 * </p>
	 * 
	 * @return S
	 */
	public Matrix getS() {
		return new SingularValueDecomposition(this).getS();
	}

	/**
	 * Two norm
	 * 
	 * @return max(S)
	 */
	public double norm2() {
		return new SingularValueDecomposition(this).norm2();
	}

	/**
	 * Return pivot permutation vector as a one-dimensional double array
	 * 
	 * @return (double) piv
	 */
	public double[] getDoublePivot() {
		return new LUDecomposition(this).getDoublePivot();
	}

	/**
	 * Return the one-dimensional array of singular values
	 * 
	 * @return diagonal of S.
	 */
	public double[] getSingularValues() {
		return new SingularValueDecomposition(this).getSingularValues();
	}

	/**
	 * Solve X*A = B, which is also A'*X' = B'
	 * 
	 * @param B
	 *            right hand side
	 * @return solution if A is square, least squares solution otherwise.
	 */
	public Matrix solveTranspose(final Matrix B) {
		return transpose().solve(B.transpose());
	}

	/**
	 * Matrix inverse or pseudoinverse
	 * 
	 * @return inverse(A) if A is square, pseudoinverse otherwise.
	 */
	public Matrix inverse() {
		return solve(identity(m, m));
	}

	/**
	 * Generate matrix with random elements
	 * 
	 * @param rows
	 *            Number of rows.
	 * @param cols
	 *            Number of colums.
	 * @return An m-by-n matrix with uniformly distributed random elements.
	 */
	public static Matrix random(final int rows, final int cols) {
		final Matrix A = new Matrix(rows, cols);
		final double[][] X = A.getArray();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				X[i][j] = Math.random();
			}
		}
		return A;
	}

	/**
	 * Generate identity matrix
	 * 
	 * @param m
	 *            Number of rows.
	 * @param n
	 *            Number of colums.
	 * @return An m-by-n matrix with ones on the diagonal and zeros elsewhere.
	 */
	public static Matrix identity(final int m, final int n) {
		final Matrix A = new Matrix(m, n);
		final double[][] X = A.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				X[i][j] = (i == j ? 1.0 : 0.0);
			}
		}
		return A;
	}

	/**
	 * Print the matrix to stdout. Line the elements up in columns with a
	 * Fortran-like 'Fw.d' style format.
	 * 
	 * @param w
	 *            Column width.
	 * @param d
	 *            Number of digits after the decimal.
	 */
	public void print(final int w, final int d) {
		print(new PrintWriter(System.out, true), w, d);
	}

	/**
	 * Print the matrix to the output stream. Line the elements up in columns
	 * with a Fortran-like 'Fw.d' style format.
	 * 
	 * @param output
	 *            Output stream.
	 * @param w
	 *            Column width.
	 * @param d
	 *            Number of digits after the decimal.
	 */
	public void print(final PrintWriter output, final int w, final int d) {
		final DecimalFormat format = new DecimalFormat();
		format.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		format.setMinimumIntegerDigits(1);
		format.setMaximumFractionDigits(d);
		format.setMinimumFractionDigits(d);
		format.setGroupingUsed(false);
		print(output, format, w + 2);
	}

	/**
	 * Print the matrix to stdout. Line the elements up in columns. Use the
	 * format object, and right justify within columns of width characters. Note
	 * that is the matrix is to be read back in, you probably will want to use a
	 * NumberFormat that is set to US Locale.
	 * 
	 * @param format
	 *            A Formatting object for individual elements.
	 * @param width
	 *            Field width for each column.
	 * @see java.text.DecimalFormat#setDecimalFormatSymbols
	 */
	public void print(final NumberFormat format, final int width) {
		print(new PrintWriter(System.out, true), format, width);
	}

	// DecimalFormat is a little disappointing coming from Fortran or C's
	// printf.
	// Since it doesn't pad on the left, the elements will come out different
	// widths. Consequently, we'll pass the desired column width in as an
	// argument and do the extra padding ourselves.

	/**
	 * Print the matrix to the output stream. Line the elements up in columns.
	 * Use the format object, and right justify within columns of width
	 * characters. Note that is the matrix is to be read back in, you probably
	 * will want to use a NumberFormat that is set to US Locale.
	 * 
	 * @param output
	 *            the output stream.
	 * @param format
	 *            A formatting object to format the matrix elements
	 * @param width
	 *            Column width.
	 * @see java.text.DecimalFormat#setDecimalFormatSymbols
	 */
	public void print(final PrintWriter output, final NumberFormat format,
			final int width) {
		output.println(); // start on new line.
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				final String s = format.format(A[i][j]); // format the number
				final int padding = Math.max(1, width - s.length()); // At
																		// _least_
																		// 1
				// space
				for (int k = 0; k < padding; k++)
					output.print(' ');
				output.print(s);
			}
			output.println();
		}
		output.println(); // end with blank line.
	}

	/*
	 * ------------------------ Private Methods ------------------------
	 */

	/** Check if size(A) == size(B) **/
	private void checkMatrixDimensions(final Matrix B) {
		if (B.m != m || B.n != n) {
			throw new IllegalArgumentException("Matrix dimensions must agree.");
		}
	}

	/**
	 * Returns the mean values along the specified dimension.
	 * 
	 * @param dim
	 *            If 1, then the mean of each column is returned in a row
	 *            vector. If 2, then the mean of each row is returned in a
	 *            column vector.
	 * @return A vector containing the mean values along the specified
	 *         dimension.
	 */
	public Matrix mean(final int dim) {
		Matrix result;
		switch (dim) {
		case 1:
			result = new Matrix(1, n);
			for (int currN = 0; currN < n; currN++) {
				for (int currM = 0; currM < m; currM++)
					result.A[0][currN] += A[currM][currN];
				result.A[0][currN] /= m;
			}
			return result;
		case 2:
			result = new Matrix(m, 1);
			for (int currM = 0; currM < m; currM++) {
				for (int currN = 0; currN < n; currN++) {
					result.A[currM][0] += A[currM][currN];
				}
				result.A[currM][0] /= n;
			}
			return result;
		default:
			(new IllegalArgumentException(
					"dim must be either 1 or 2, and not: " + dim))
					.printStackTrace();
			return null;
		}
	}

	/**
	 * Calculate the full covariance matrix.
	 * 
	 * @return the covariance matrix
	 */
	public Matrix cov() {
		final Matrix transe = this.transpose();
		final Matrix result = new Matrix(transe.m, transe.m);
		for (int currM = 0; currM < transe.m; currM++) {
			for (int currN = currM; currN < transe.m; currN++) {
				final double covMN = cov(transe.A[currM], transe.A[currN]);
				result.A[currM][currN] = covMN;
				result.A[currN][currM] = covMN;
			}
		}
		return result;
	}

	/**
	 * Calculate the covariance between the two vectors.
	 * 
	 * @param vec1
	 * @param vec2
	 * @return the covariance between the two vectors.
	 */
	private double cov(final double[] vec1, final double[] vec2) {
		double result = 0;
		final int dim = vec1.length;
		if (vec2.length != dim)
			(new IllegalArgumentException("vectors are not of same length"))
					.printStackTrace();
		final double meanVec1 = mean(vec1), meanVec2 = mean(vec2);
		for (int i = 0; i < dim; i++) {
			result += (vec1[i] - meanVec1) * (vec2[i] - meanVec2);
		}
		return result / Math.max(1, dim - 1);
		// int dim = vec1.length;
		// if(vec2.length != dim)
		// (new
		// IllegalArgumentException("vectors are not of same length")).printStackTrace();
		// double[] times = new double[dim];
		// for(int i=0; i<dim; i++)
		// times[i] += vec1[i]*vec2[i];
		// return mean(times) - mean(vec1)*mean(vec2);
	}

	/**
	 * the mean of the values in the double array
	 * 
	 * @param vec
	 *            double values
	 * @return the mean of the values in vec
	 */
	private double mean(final double[] vec) {
		double result = 0;
		for (int i = 0; i < vec.length; i++)
			result += vec[i];
		return result / vec.length;
	}

	/**
	 * Returns the sum of the component of the matrix.
	 * 
	 * @return the sum
	 */
	public double sum() {
		double result = 0;
		for (final double[] dArr : A)
			for (final double d : dArr)
				result += d;
		return result;
	}

	/**
	 * returns a new Matrix object, where each value is set to the absolute
	 * value
	 * 
	 * @return a new Matrix with all values being positive
	 */
	public Matrix abs() {
		final Matrix result = new Matrix(m, n); // don't use clone(), as the
												// values
		// are assigned in the loop.
		for (int i = 0; i < result.A.length; i++) {
			for (int j = 0; j < result.A[i].length; j++)
				result.A[i][j] = Math.abs(A[i][j]);
		}
		return result;
	}

	/**
	 * Writes the Matrix to an ascii-textfile that can be read by Matlab. Usage
	 * in Matlab: load('filename', '-ascii');
	 * 
	 * @param filename
	 *            the name of the ascii file to create, e.g.
	 *            "C:\\temp\\matrix.ascii"
	 * @throws IllegalArgumentException
	 *             if there is a problem with the filename
	 */
	public void writeAscii(final String filename)
			throws IllegalArgumentException {
		PrintWriter pw;
		try {
			pw = new PrintWriter(new File(filename));
		} catch (final FileNotFoundException e) {
			throw new IllegalArgumentException(
					"there was a problem with the file " + filename);
		}
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				pw.printf("%1$1.7e ", A[i][j]);
			}
			pw.printf("\r");
		}
		pw.close();
	}
}
