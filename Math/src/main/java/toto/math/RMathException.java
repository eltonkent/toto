package toto.math;

/**
 * RAGE math exception
 * 
 * @author Mobifluence Interactive
 * 
 */
public class RMathException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6672060091965365391L;

	public RMathException() {
		super();
	}

	public RMathException(final String message) {
		super(message);
	}
}
