package toto.math;

import java.math.BigInteger;

/**
 * A collection of preconditions for math functions.
 * 
 */
public final class MathConditions {
	public static int checkPositive(final String role, final int x) {
		if (x <= 0) {
			throw new IllegalArgumentException(role + " (" + x
					+ ") must be > 0");
		}
		return x;
	}

	public static long checkPositive(final String role, final long x) {
		if (x <= 0) {
			throw new IllegalArgumentException(role + " (" + x
					+ ") must be > 0");
		}
		return x;
	}

	public static BigInteger checkPositive(final String role, final BigInteger x) {
		if (x.signum() <= 0) {
			throw new IllegalArgumentException(role + " (" + x
					+ ") must be > 0");
		}
		return x;
	}

	public static int checkNonNegative(final String role, final int x) {
		if (x < 0) {
			throw new IllegalArgumentException(role + " (" + x
					+ ") must be >= 0");
		}
		return x;
	}

	public static long checkNonNegative(final String role, final long x) {
		if (x < 0) {
			throw new IllegalArgumentException(role + " (" + x
					+ ") must be >= 0");
		}
		return x;
	}

	public static BigInteger checkNonNegative(final String role,
			final BigInteger x) {
		if (x.signum() < 0) {
			throw new IllegalArgumentException(role + " (" + x
					+ ") must be >= 0");
		}
		return x;
	}

	public static double checkNonNegative(final String role, final double x) {
		if (!(x >= 0)) { // not x < 0, to work with NaN.
			throw new IllegalArgumentException(role + " (" + x
					+ ") must be >= 0");
		}
		return x;
	}

	public static void checkRoundingUnnecessary(final boolean condition) {
		if (!condition) {
			throw new ArithmeticException(
					"mode was UNNECESSARY, but rounding was necessary");
		}
	}

	public static void checkInRange(final boolean condition) {
		if (!condition) {
			throw new ArithmeticException("not in range");
		}
	}

	public static void checkNoOverflow(final boolean condition) {
		if (!condition) {
			throw new ArithmeticException("overflow");
		}
	}

	private MathConditions() {
	}
}
