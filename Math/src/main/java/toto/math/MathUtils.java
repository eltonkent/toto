/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.math;

import static java.lang.Math.abs;
import static java.lang.Math.min;
import static java.math.RoundingMode.HALF_EVEN;
import static java.math.RoundingMode.HALF_UP;
import static com.mi.toto.Conditions.checkArgument;
import static com.mi.toto.Conditions.checkNotNull;
import static toto.math.MathConditions.checkNoOverflow;
import static toto.math.MathConditions.checkNonNegative;
import static toto.math.MathConditions.checkPositive;
import static toto.math.MathConditions.checkRoundingUnnecessary;

import java.math.RoundingMode;

import com.mi.toto.Conditions;

public class MathUtils {

	public static double hypot(final double a, final double b) {
		double r;
		if (Math.abs(a) > Math.abs(b)) {
			r = b / a;
			r = Math.abs(a) * Math.sqrt(1 + r * r);
		} else if (b != 0) {
			r = a / b;
			r = Math.abs(b) * Math.sqrt(1 + r * r);
		} else {
			r = 0.0;
		}
		return r;
	}

	/**
	 * Wraps the given value into the inclusive-exclusive interval between min
	 * and max.
	 * 
	 * @param n
	 *            The value to wrap.
	 * @param min
	 *            The minimum.
	 * @param max
	 *            The maximum.
	 */
	public static double wrap(final double n, final double min, final double max) {
		return (n >= min && n < max) ? n : (mod(n - min, max - min) + min);
	}

	/**
	 * returns the next highest power of two, or the current value if it's
	 * already a power of two or zero
	 */
	public static int nextHighestPowerOfTwo(int v) {
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v++;
		return v;
	}

	/**
	 * Returns {@code true} if {@code x} represents a power of two.
	 * 
	 * <p>
	 * This differs from {@code Integer.bitCount(x) == 1}, because
	 * {@code Integer.bitCount(Integer.MIN_VALUE) == 1}, but
	 * {@link Integer#MIN_VALUE} is not a power of two.
	 */
	public static boolean isPowerOfTwo(final int x) {
		return x > 0 & (x & (x - 1)) == 0;
	}

	/**
	 * Returns the base-2 logarithm of {@code x}, rounded according to the
	 * specified rounding mode.
	 * 
	 * @throws IllegalArgumentException
	 *             if {@code x <= 0}
	 * @throws ArithmeticException
	 *             if {@code mode} is {@link RoundingMode#UNNECESSARY} and
	 *             {@code x} is not a power of two
	 */
	public static int log2(final int x, final RoundingMode mode) {
		checkPositive("x", x);
		switch (mode) {
		case UNNECESSARY:
			checkRoundingUnnecessary(isPowerOfTwo(x));
			// fall through
		case DOWN:
		case FLOOR:
			return (Integer.SIZE - 1) - Integer.numberOfLeadingZeros(x);

		case UP:
		case CEILING:
			return Integer.SIZE - Integer.numberOfLeadingZeros(x - 1);

		case HALF_DOWN:
		case HALF_UP:
		case HALF_EVEN:
			// Since sqrt(2) is irrational, log2(x) - logFloor cannot be exactly
			// 0.5
			final int leadingZeros = Integer.numberOfLeadingZeros(x);
			final int cmp = MAX_POWER_OF_SQRT2_UNSIGNED >>> leadingZeros;
			// floor(2^(logFloor + 0.5))
			final int logFloor = (Integer.SIZE - 1) - leadingZeros;
			return logFloor + lessThanBranchFree(cmp, x);

		default:
			throw new AssertionError();
		}
	}

	public static int log2(int n) {
		int log = 0;
		if ((n & 0xffff0000) != 0) {
			n >>>= 16;
			log = 16;
		}
		if (n >= 256) {
			n >>>= 8;
			log += 8;
		}
		if (n >= 16) {
			n >>>= 4;
			log += 4;
		}
		if (n >= 4) {
			n >>>= 2;
			log += 2;
		}
		return log + (n >>> 1);
	}

	/** The biggest half power of two that can fit in an unsigned int. */
	static final int MAX_POWER_OF_SQRT2_UNSIGNED = 0xB504F333;

	/**
	 * Returns 1 if {@code x < y} as unsigned integers, and 0 otherwise. Assumes
	 * that x - y fits into a signed int. The implementation is branch-free, and
	 * benchmarks suggest it is measurably (if narrowly) faster than the
	 * straightforward ternary expression.
	 */
	static int lessThanBranchFree(final int x, final int y) {
		// The double negation is optimized away by normal Java, but is
		// necessary for GWT
		// to make sure bit twiddling works as expected.
		return ~~(x - y) >>> (Integer.SIZE - 1);
	}

	/**
	 * returns the next highest power of two, or the current value if it's
	 * already a power of two or zero
	 */
	public static long nextHighestPowerOfTwo(long v) {
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v |= v >> 32;
		v++;
		return v;
	}

	/**
	 * Equivalent to Math.max(low, Math.min(high, amount));
	 */
	public static float constrain(final float amount, final float low,
			final float high) {
		return amount < low ? low : amount > high ? high : amount;
	}

	/**
	 * Equivalent to Math.max(low, Math.min(high, amount));
	 */
	public static int constrain(final int amount, final int low, final int high) {
		return amount < low ? low : amount > high ? high : amount;
	}

	/**
	 * Return logarithm to base 10.
	 * 
	 * @param x
	 *            Argument to take logarithm from (x>0)
	 */

	public static final double log10(final double x)
			throws IllegalArgumentException {
		if (x <= 0)
			throw new IllegalArgumentException();
		else {
			final double LN10 = Math.log(10.0);
			return Math.log(x) / LN10;
		}
	}

	/**
	 * Calculate sin^2(x).
	 * 
	 * @param x
	 *            x
	 * @return sin^2(x)
	 * @since 1.0
	 */
	public static double sinSquared(final double x) {
		return Math.sin(x) * Math.sin(x);
	}

	/**
	 * Calculate sin^3(x).
	 * 
	 * @param x
	 *            x
	 * @return sin^3(x)
	 * @since 1.1
	 */
	public static double sinCubed(final double x) {
		return sinSquared(x) * Math.sin(x);
	}

	/**
	 * Calculate cos^2(x).
	 * 
	 * @param x
	 *            x
	 * @return cos^2(x)
	 * @since 1.0
	 */
	public static double cosSquared(final double x) {
		return Math.cos(x) * Math.cos(x);
	}

	/**
	 * Calculate cos^3(x).
	 * 
	 * @param x
	 *            x
	 * @return cos^3(x)
	 * @since 1.1
	 */
	public static double cosCubed(final double x) {
		return cosSquared(x) * Math.cos(x);
	}

	/**
	 * Calculate tan^2(x).
	 * 
	 * @param x
	 *            x
	 * @return tan^2(x)
	 * @since 1.0
	 */
	public static double tanSquared(final double x) {
		return Math.tan(x) * Math.tan(x);
	}

	/**
	 * Calculate sec(x).
	 * 
	 * @param x
	 *            x
	 * @return sec(x)
	 * @since 1.0
	 */
	public static double sec(final double x) {
		return 1.0 / Math.cos(x);
	}

	/**
	 * Rounds the given double value
	 * 
	 * @param value
	 *            the value
	 * @return the rounded value, x.5 and higher is rounded to x + 1.
	 */
	public static long round(final double value) {
		if (value < 0) {
			return (long) (value - 0.5);
		} else {
			return (long) (value + 0.5);
		}
	}

	/**
	 * Ends up being a bit faster than {@link Math#round(float)}. This merely
	 * rounds its argument to the nearest int, where x.5 rounds up to x+1.
	 */
	public static int round(final float d) {
		return (int) (d + 0.5f);
	}

	/**
	 * Round value to the number of places sepcified
	 * 
	 * @param value
	 * @param places
	 * @return
	 */
	public static double round(double value, final int places) {
		if (places < 0) {
			throw new IllegalArgumentException();
		}

		final long factor = (long) Math.pow(10, places);
		value = value * factor;
		final long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	/**
	 * Computes a float from mantissa and exponent.
	 */
	public static float buildFloat(int mant, final int exp) {
		if (exp < -125 || mant == 0) {
			return 0.0f;
		}

		if (exp >= 128) {
			return (mant > 0) ? Float.POSITIVE_INFINITY
					: Float.NEGATIVE_INFINITY;
		}

		if (exp == 0) {
			return mant;
		}

		if (mant >= (1 << 26)) {
			mant++; // round up trailing bits if they will be dropped.
		}

		return (float) ((exp > 0) ? mant * pow10[exp] : mant / pow10[-exp]);
	}

	/**
	 * Array of powers of ten. Using double instead of float gives a tiny bit
	 * more precision.
	 */
	private static final double[] pow10 = new double[128];
	static {
		for (int i = 0; i < pow10.length; i++) {
			pow10[i] = Math.pow(10, i);
		}
	}

	/**
	 * Returns the base-10 logarithm of {@code x}, rounded according to the
	 * specified rounding mode.
	 * 
	 * @throws IllegalArgumentException
	 *             if {@code x <= 0}
	 * @throws ArithmeticException
	 *             if {@code mode} is {@link RoundingMode#UNNECESSARY} and
	 *             {@code x} is not a power of ten
	 */
	public static int log10(final int x, final RoundingMode mode) {
		checkPositive("x", x);
		final int logFloor = log10Floor(x);
		final int floorPow = powersOf10[logFloor];
		switch (mode) {
		case UNNECESSARY:
			checkRoundingUnnecessary(x == floorPow);
			// fall through
		case FLOOR:
		case DOWN:
			return logFloor;
		case CEILING:
		case UP:
			return logFloor + lessThanBranchFree(floorPow, x);
		case HALF_DOWN:
		case HALF_UP:
		case HALF_EVEN:
			// sqrt(10) is irrational, so log10(x) - logFloor is never exactly
			// 0.5
			return logFloor + lessThanBranchFree(halfPowersOf10[logFloor], x);
		default:
			throw new AssertionError();
		}
	}

	private static int log10Floor(final int x) {
		/*
		 * Based on Hacker's Delight Fig. 11-5, the two-table-lookup,
		 * branch-free implementation.
		 * 
		 * The key idea is that based on the number of leading zeros
		 * (equivalently, floor(log2(x))), we can narrow the possible
		 * floor(log10(x)) values to two. For example, if floor(log2(x)) is 6,
		 * then 64 <= x < 128, so floor(log10(x)) is either 1 or 2.
		 */
		final int y = maxLog10ForLeadingZeros[Integer.numberOfLeadingZeros(x)];
		/*
		 * y is the higher of the two possible values of floor(log10(x)). If x <
		 * 10^y, then we want the lower of the two possible values, or y - 1,
		 * otherwise, we want y.
		 */
		return y - lessThanBranchFree(x, powersOf10[y]);
	}

	// maxLog10ForLeadingZeros[i] == floor(log10(2^(Long.SIZE - i)))

	static final byte[] maxLog10ForLeadingZeros = { 9, 9, 9, 8, 8, 8, 7, 7, 7,
			6, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0, 0,
			0, 0 };

	static final int[] powersOf10 = { 1, 10, 100, 1000, 10000, 100000, 1000000,
			10000000, 100000000, 1000000000 };

	// halfPowersOf10[i] = largest int less than 10^(i + 0.5)

	static final int[] halfPowersOf10 = { 3, 31, 316, 3162, 31622, 316227,
			3162277, 31622776, 316227766, Integer.MAX_VALUE };

	/**
	 * Returns {@code b} to the {@code k}th power. Even if the result overflows,
	 * it will be equal to {@code BigInteger.valueOf(b).pow(k).intValue()}. This
	 * implementation runs in {@code O(log k)} time.
	 * 
	 * <p>
	 * Compare {@link #checkedPow}, which throws an {@link ArithmeticException}
	 * upon overflow.
	 * 
	 * @throws IllegalArgumentException
	 *             if {@code k < 0}
	 */
	public static int pow(int b, int k) {
		checkNonNegative("exponent", k);
		switch (b) {
		case 0:
			return (k == 0) ? 1 : 0;
		case 1:
			return 1;
		case (-1):
			return ((k & 1) == 0) ? 1 : -1;
		case 2:
			return (k < Integer.SIZE) ? (1 << k) : 0;
		case (-2):
			if (k < Integer.SIZE) {
				return ((k & 1) == 0) ? (1 << k) : -(1 << k);
			} else {
				return 0;
			}
		default:
			// continue below to handle the general case
		}
		for (int accum = 1;; k >>= 1) {
			switch (k) {
			case 0:
				return accum;
			case 1:
				return b * accum;
			default:
				accum *= ((k & 1) == 0) ? 1 : b;
				b *= b;
			}
		}
	}

	/**
	 * Returns the square root of {@code x}, rounded with the specified rounding
	 * mode.
	 * 
	 * @throws IllegalArgumentException
	 *             if {@code x < 0}
	 * @throws ArithmeticException
	 *             if {@code mode} is {@link RoundingMode#UNNECESSARY} and
	 *             {@code sqrt(x)} is not an integer
	 */
	@SuppressWarnings("fallthrough")
	public static int sqrt(final int x, final RoundingMode mode) {
		checkNonNegative("x", x);
		final int sqrtFloor = sqrtFloor(x);
		switch (mode) {
		case UNNECESSARY:
			checkRoundingUnnecessary(sqrtFloor * sqrtFloor == x); // fall
																	// through
		case FLOOR:
		case DOWN:
			return sqrtFloor;
		case CEILING:
		case UP:
			return sqrtFloor + lessThanBranchFree(sqrtFloor * sqrtFloor, x);
		case HALF_DOWN:
		case HALF_UP:
		case HALF_EVEN:
			final int halfSquare = sqrtFloor * sqrtFloor + sqrtFloor;
			/*
			 * We wish to test whether or not x <= (sqrtFloor + 0.5)^2 =
			 * halfSquare + 0.25. Since both x and halfSquare are integers, this
			 * is equivalent to testing whether or not x <= halfSquare. (We have
			 * to deal with overflow, though.)
			 * 
			 * If we treat halfSquare as an unsigned int, we know that
			 * sqrtFloor^2 <= x < (sqrtFloor + 1)^2 halfSquare - sqrtFloor <= x
			 * < halfSquare + sqrtFloor + 1 so |x - halfSquare| <= sqrtFloor.
			 * Therefore, it's safe to treat x - halfSquare as a signed int, so
			 * lessThanBranchFree is safe for use.
			 */
			return sqrtFloor + lessThanBranchFree(halfSquare, x);
		default:
			throw new AssertionError();
		}
	}

	private static int sqrtFloor(final int x) {
		// There is no loss of precision in converting an int to a double,
		// according to
		// http://java.sun.com/docs/books/jls/third_edition/html/conversions.html#5.1.2
		return (int) Math.sqrt(x);
	}

	/**
	 * Returns the result of dividing {@code p} by {@code q}, rounding using the
	 * specified {@code RoundingMode}.
	 * 
	 * @throws ArithmeticException
	 *             if {@code q == 0}, or if {@code mode == UNNECESSARY} and
	 *             {@code a} is not an integer multiple of {@code b}
	 */
	@SuppressWarnings("fallthrough")
	public static int divide(final int p, final int q, final RoundingMode mode) {
		checkNotNull(mode);
		if (q == 0) {
			throw new ArithmeticException("/ by zero"); // for GWT
		}
		final int div = p / q;
		final int rem = p - q * div; // equal to p % q

		if (rem == 0) {
			return div;
		}

		/*
		 * Normal Java division rounds towards 0, consistently with
		 * RoundingMode.DOWN. We just have to deal with the cases where rounding
		 * towards 0 is wrong, which typically depends on the sign of p / q.
		 * 
		 * signum is 1 if p and q are both nonnegative or both negative, and -1
		 * otherwise.
		 */
		final int signum = 1 | ((p ^ q) >> (Integer.SIZE - 1));
		boolean increment;
		switch (mode) {
		case UNNECESSARY:
			checkRoundingUnnecessary(rem == 0);
			// fall through
		case DOWN:
			increment = false;
			break;
		case UP:
			increment = true;
			break;
		case CEILING:
			increment = signum > 0;
			break;
		case FLOOR:
			increment = signum < 0;
			break;
		case HALF_EVEN:
		case HALF_DOWN:
		case HALF_UP:
			final int absRem = abs(rem);
			final int cmpRemToHalfDivisor = absRem - (abs(q) - absRem);
			// subtracting two nonnegative ints can't overflow
			// cmpRemToHalfDivisor has the same sign as compare(abs(rem), abs(q)
			// / 2).
			if (cmpRemToHalfDivisor == 0) { // exactly on the half mark
				increment = (mode == HALF_UP || (mode == HALF_EVEN & (div & 1) != 0));
			} else {
				increment = cmpRemToHalfDivisor > 0; // closer to the UP value
			}
			break;
		default:
			throw new AssertionError();
		}
		return increment ? div + signum : div;
	}

	/**
	 * Returns {@code x mod m}, a non-negative value less than {@code m}. This
	 * differs from {@code x % m}, which might be negative.
	 * 
	 * <p>
	 * For example:
	 * 
	 * <pre>
	 * {@code
	 * 
	 * mod(7, 4) == 3
	 * mod(-7, 4) == 1
	 * mod(-1, 4) == 3
	 * mod(-8, 4) == 0
	 * mod(8, 4) == 0}
	 * </pre>
	 * 
	 * @throws ArithmeticException
	 *             if {@code m <= 0}
	 * @see <a
	 *      href="http://docs.oracle.com/javase/specs/jls/se7/html/jls-15.html#jls-15.17.3">
	 *      Remainder Operator</a>
	 */
	public static int mod(final int x, final int m) {
		if (m <= 0) {
			throw new ArithmeticException("Modulus " + m + " must be > 0");
		}
		final int result = x % m;
		return (result >= 0) ? result : result + m;
	}

	/**
	 * Returns the greatest common divisor of {@code a, b}. Returns {@code 0} if
	 * {@code a == 0 && b == 0}.
	 * 
	 * @throws IllegalArgumentException
	 *             if {@code a < 0} or {@code b < 0}
	 */
	public static int gcd(int a, int b) {
		/*
		 * The reason we require both arguments to be >= 0 is because otherwise,
		 * what do you return on gcd(0, Integer.MIN_VALUE)? BigInteger.gcd would
		 * return positive 2^31, but positive 2^31 isn't an int.
		 */
		checkNonNegative("a", a);
		checkNonNegative("b", b);
		if (a == 0) {
			// 0 % b == 0, so b divides a, but the converse doesn't hold.
			// BigInteger.gcd is consistent with this decision.
			return b;
		} else if (b == 0) {
			return a; // similar logic
		}
		/*
		 * Uses the binary GCD algorithm; see
		 * http://en.wikipedia.org/wiki/Binary_GCD_algorithm. This is >40%
		 * faster than the Euclidean algorithm in benchmarks.
		 */
		final int aTwos = Integer.numberOfTrailingZeros(a);
		a >>= aTwos; // divide out all 2s
		final int bTwos = Integer.numberOfTrailingZeros(b);
		b >>= bTwos; // divide out all 2s
		while (a != b) { // both a, b are odd
			// The key to the binary GCD algorithm is as follows:
			// Both a and b are odd. Assume a > b; then gcd(a - b, b) = gcd(a,
			// b).
			// But in gcd(a - b, b), a - b is even and b is odd, so we can
			// divide out powers of two.

			// We bend over backwards to avoid branching, adapting a technique
			// from
			// http://graphics.stanford.edu/~seander/bithacks.html#IntegerMinOrMax

			final int delta = a - b; // can't overflow, since a and b are
										// nonnegative

			final int minDeltaOrZero = delta & (delta >> (Integer.SIZE - 1));
			// equivalent to Math.min(delta, 0)

			a = delta - minDeltaOrZero - minDeltaOrZero; // sets a to Math.abs(a
															// - b)
			// a is now nonnegative and even

			b += minDeltaOrZero; // sets b to min(old a, b)
			a >>= Integer.numberOfTrailingZeros(a); // divide out all 2s, since
													// 2 doesn't divide b
		}
		return a << min(aTwos, bTwos);
	}

	/**
	 * Returns the sum of {@code a} and {@code b}, provided it does not
	 * overflow.
	 * 
	 * @throws ArithmeticException
	 *             if {@code a + b} overflows in signed {@code int} arithmetic
	 */
	public static int checkedAdd(final int a, final int b) {
		final long result = (long) a + b;
		checkNoOverflow(result == (int) result);
		return (int) result;
	}

	/**
	 * Returns the difference of {@code a} and {@code b}, provided it does not
	 * overflow.
	 * 
	 * @throws ArithmeticException
	 *             if {@code a - b} overflows in signed {@code int} arithmetic
	 */
	public static int checkedSubtract(final int a, final int b) {
		final long result = (long) a - b;
		checkNoOverflow(result == (int) result);
		return (int) result;
	}

	/**
	 * Returns the product of {@code a} and {@code b}, provided it does not
	 * overflow.
	 * 
	 * @throws ArithmeticException
	 *             if {@code a * b} overflows in signed {@code int} arithmetic
	 */
	public static int checkedMultiply(final int a, final int b) {
		final long result = (long) a * b;
		checkNoOverflow(result == (int) result);
		return (int) result;
	}

	/**
	 * Returns the {@code b} to the {@code k}th power, provided it does not
	 * overflow.
	 * 
	 * <p>
	 * {@link #pow} may be faster, but does not check for overflow.
	 * 
	 * @throws ArithmeticException
	 *             if {@code b} to the {@code k}th power overflows in signed
	 *             {@code int} arithmetic
	 */
	public static int checkedPow(int b, int k) {
		checkNonNegative("exponent", k);
		switch (b) {
		case 0:
			return (k == 0) ? 1 : 0;
		case 1:
			return 1;
		case (-1):
			return ((k & 1) == 0) ? 1 : -1;
		case 2:
			checkNoOverflow(k < Integer.SIZE - 1);
			return 1 << k;
		case (-2):
			checkNoOverflow(k < Integer.SIZE);
			return ((k & 1) == 0) ? 1 << k : -1 << k;
		default:
			// continue below to handle the general case
		}
		int accum = 1;
		while (true) {
			switch (k) {
			case 0:
				return accum;
			case 1:
				return checkedMultiply(accum, b);
			default:
				if ((k & 1) != 0) {
					accum = checkedMultiply(accum, b);
				}
				k >>= 1;
				if (k > 0) {
					checkNoOverflow(-FLOOR_SQRT_MAX_INT <= b
							& b <= FLOOR_SQRT_MAX_INT);
					b *= b;
				}
			}
		}
	}

	static final int FLOOR_SQRT_MAX_INT = 46340;

	/**
	 * Returns {@code n} choose {@code k}, also known as the binomial
	 * coefficient of {@code n} and {@code k}, or {@link Integer#MAX_VALUE} if
	 * the result does not fit in an {@code int}.
	 * 
	 * @throws IllegalArgumentException
	 *             if {@code n < 0}, {@code k < 0} or {@code k > n}
	 */
	public static int binomial(final int n, int k) {
		checkNonNegative("n", n);
		checkNonNegative("k", k);
		checkArgument(k <= n, "k (%s) > n (%s)", k, n);
		if (k > (n >> 1)) {
			k = n - k;
		}
		if (k >= biggestBinomials.length || n > biggestBinomials[k]) {
			return Integer.MAX_VALUE;
		}
		switch (k) {
		case 0:
			return 1;
		case 1:
			return n;
		default:
			long result = 1;
			for (int i = 0; i < k; i++) {
				result *= n - i;
				result /= i + 1;
			}
			return (int) result;
		}
	}

	// binomial(biggestBinomials[k], k) fits in an int, but not
	// binomial(biggestBinomials[k]+1,k).

	static int[] biggestBinomials = { Integer.MAX_VALUE, Integer.MAX_VALUE,
			65536, 2345, 477, 193, 110, 75, 58, 49, 43, 39, 37, 35, 34, 34, 33 };

	/**
	 * Returns the arithmetic mean of {@code x} and {@code y}, rounded towards
	 * negative infinity. This method is overflow resilient.
	 * 
	 * @since 14.0
	 */
	public static int mean(final int x, final int y) {
		// Efficient method for computing the arithmetic mean.
		// The alternative (x + y) / 2 fails for large values.
		// The alternative (x + y) >>> 1 fails for negative values.
		return (x & y) + ((x ^ y) >> 1);
	}

	/**
	 * Reverse the bits in a 32 bit integer. This code is also available in Java
	 * 5 using Integer.reverse, however not available yet in Retrotranslator.
	 * The code was taken from http://www.hackersdelight.org - reverse.c
	 * 
	 * @param x
	 *            the original value
	 * @return the value with reversed bits
	 */
	public static int reverseInt(int x) {
		x = (x & 0x55555555) << 1 | (x >>> 1) & 0x55555555;
		x = (x & 0x33333333) << 2 | (x >>> 2) & 0x33333333;
		x = (x & 0x0f0f0f0f) << 4 | (x >>> 4) & 0x0f0f0f0f;
		x = (x << 24) | ((x & 0xff00) << 8) | ((x >>> 8) & 0xff00) | (x >>> 24);
		return x;
	}

	/**
	 * Reverse the bits in a 64 bit long. This code is also available in Java 5
	 * using Long.reverse, however not available yet in Retrotranslator.
	 * 
	 * @param x
	 *            the original value
	 * @return the value with reversed bits
	 */
	public static long reverseLong(final long x) {
		return (reverseInt((int) (x >>> 32L)) & 0xffffffffL)
				^ (((long) reverseInt((int) x)) << 32L);
	}

	public static int computePercent(final double total, final double current) {
		return computePercent(0, total, current);
	}

	/**
	 * Get the percentage of the given value in the interval between
	 * <code>start</code> and <code>end</code>
	 * <p>
	 * eg: start=0 end=10 current=5, percent=50%
	 * </p>
	 * 
	 * @param start
	 * @param end
	 * @param current
	 * @return
	 */
	public static int computePercent(final double start, final double end,
			final double current) {
		Conditions.checkArgument(start < end && current <= end,
				"start>end or current>end");
		return (int) Math.abs(100 * (current - start) / (end - start));
	}

	/**
	 * Calculates a number between two numbers at a specific increment.
	 * 
	 * @param t
	 *            the interpolation parameter
	 * @param a
	 *            the lower interpolation range
	 * @param b
	 *            the upper interpolation range
	 * @return
	 */
	public static float lerp(final float t, final float a, final float b) {
		return a + t * (b - a);
	}

	/**
	 * Linear interpolation.
	 * 
	 * @param t
	 *            the interpolation parameter
	 * @param a
	 *            the lower interpolation range
	 * @param b
	 *            the upper interpolation range
	 * @return the interpolated value
	 */
	public static int lerp(final float t, final int a, final int b) {
		return (int) (a + t * (b - a));
	}

	/**
	 * Return a mod b. This differs from the % operator with respect to negative
	 * numbers.
	 * 
	 * @param a
	 *            the dividend
	 * @param b
	 *            the divisor
	 * @return a mod b
	 */
	public static float mod(float a, final float b) {
		final int n = (int) (a / b);

		a -= n * b;
		if (a < 0)
			return a + b;
		return a;
	}

	/**
	 * Clamp a value to an interval.
	 * 
	 * @param low
	 *            the lower clamp threshold
	 * @param high
	 *            the upper clamp threshold
	 * @param x
	 *            the input parameter
	 * @return the clamped value
	 */
	public static float clamp(final float x, final float low, final float high) {
		return x < low ? low : (x > high ? high : x);
	}

	/**
	 * Clamp a value to an interval.
	 * 
	 * @param low
	 *            the lower clamp threshold
	 * @param high
	 *            the upper clamp threshold
	 * @param x
	 *            the input parameter
	 * @return the clamped value
	 */
	public static int clamp(final int x, final int low, final int high) {
		return x < low ? low : (x > high ? high : x);
	}

	/**
	 * Clamp a value to an interval.
	 * 
	 * @param a
	 *            the lower clamp threshold
	 * @param b
	 *            the upper clamp threshold
	 * @param x
	 *            the input parameter
	 * @return the clamped value
	 */
	public static double clamp(final double x, final int a, final int b) {
		return (x < a) ? a : (x > b) ? b : x;
	}

	/**
	 * Return a mod b. This differs from the % operator with respect to negative
	 * numbers.
	 * 
	 * @param a
	 *            the dividend
	 * @param b
	 *            the divisor
	 * @return a mod b
	 */
	public static double mod(double a, final double b) {
		final int n = (int) (a / b);

		a -= n * b;
		if (a < 0)
			return a + b;
		return a;
	}

	/**
	 * Add two integers, checking for overflow.
	 * 
	 * @param x
	 *            an addend
	 * @param y
	 *            an addend
	 * @return the sum {@code x+y}
	 * @throws RMathException
	 *             if the result can not be represented as an {@code int}.
	 */
	public static int addAndCheck(final int x, final int y)
			throws RMathException {
		final long s = (long) x + (long) y;
		if (s < Integer.MIN_VALUE || s > Integer.MAX_VALUE) {
			throw new RMathException("Overflow for integer addition");
		}
		return (int) s;
	}

	/**
	 * Multiply two long integers, checking for overflow.
	 * 
	 * @param a
	 *            Factor.
	 * @param b
	 *            Factor.
	 * @return the product {@code a * b}.
	 * @throws RMathException
	 *             if the result can not be represented as a {@code long}.
	 * @since 1.2
	 */
	public static long mulAndCheck(final long a, final long b)
			throws RMathException {
		long ret;
		if (a > b) {
			// use symmetry to reduce boundary cases
			ret = mulAndCheck(b, a);
		} else {
			if (a < 0) {
				if (b < 0) {
					// check for positive overflow with negative a, negative b
					if (a >= Long.MAX_VALUE / b) {
						ret = a * b;
					} else {
						throw new RMathException();
					}
				} else if (b > 0) {
					// check for negative overflow with negative a, positive b
					if (Long.MIN_VALUE / b <= a) {
						ret = a * b;
					} else {
						throw new RMathException();

					}
				} else {
					// assert b == 0
					ret = 0;
				}
			} else if (a > 0) {
				// assert a > 0
				// assert b > 0

				// check for positive overflow with positive a, positive b
				if (a <= Long.MAX_VALUE / b) {
					ret = a * b;
				} else {
					throw new RMathException();
				}
			} else {
				// assert a == 0
				ret = 0;
			}
		}
		return ret;
	}

	/**
	 * <p>
	 * Gets the greatest common divisor of the absolute value of two numbers,
	 * using the "binary gcd" method which avoids division and modulo
	 * operations. See Knuth 4.5.2 algorithm B. This algorithm is due to Josef
	 * Stein (1961).
	 * </p>
	 * Special cases:
	 * <ul>
	 * <li>The invocations {@code gcd(Long.MIN_VALUE, Long.MIN_VALUE)},
	 * {@code gcd(Long.MIN_VALUE, 0L)} and {@code gcd(0L, Long.MIN_VALUE)} throw
	 * an {@code RageMathException}, because the result would be 2^63, which is
	 * too large for a long value.</li>
	 * <li>The result of {@code gcd(x, x)}, {@code gcd(0L, x)} and
	 * {@code gcd(x, 0L)} is the absolute value of {@code x}, except for the
	 * special cases above.
	 * <li>The invocation {@code gcd(0L, 0L)} is the only one which returns
	 * {@code 0L}.</li>
	 * </ul>
	 * 
	 * @param p
	 *            Number.
	 * @param q
	 *            Number.
	 * @return the greatest common divisor, never negative.
	 * @throws RMathException
	 *             if the result cannot be represented as a non-negative
	 *             {@code long} value.
	 * @since 2.1
	 */
	public static long gcd(final long p, final long q) throws RMathException {
		long u = p;
		long v = q;
		if ((u == 0) || (v == 0)) {
			if ((u == Long.MIN_VALUE) || (v == Long.MIN_VALUE)) {
				throw new RMathException(
						"Arithmetic Exception: GCD Overflow 64 bits");
			}
			return Math.abs(u) + Math.abs(v);
		}
		// keep u and v negative, as negative integers range down to
		// -2^63, while positive numbers can only be as large as 2^63-1
		// (i.e. we can't necessarily negate a negative number without
		// overflow)
		/* assert u!=0 && v!=0; */
		if (u > 0) {
			u = -u;
		} // make u negative
		if (v > 0) {
			v = -v;
		} // make v negative
			// B1. [Find power of 2]
		int k = 0;
		while ((u & 1) == 0 && (v & 1) == 0 && k < 63) { // while u and v are
															// both even...
			u /= 2;
			v /= 2;
			k++; // cast out twos.
		}
		if (k == 63) {
			throw new RMathException(
					"Arithmetic Exception: GCD Overflow 64 bits");
		}
		// B2. Initialize: u and v have been divided by 2^k and at least
		// one is odd.
		long t = ((u & 1) == 1) ? v : -(u / 2)/* B3 */;
		// t negative: u was odd, v may be even (t replaces v)
		// t positive: u was even, v is odd (t replaces u)
		do {
			/* assert u<0 && v<0; */
			// B4/B3: cast out twos from t.
			while ((t & 1) == 0) { // while t is even..
				t /= 2; // cast out twos
			}
			// B5 [reset max(u,v)]
			if (t > 0) {
				u = -t;
			} else {
				v = t;
			}
			// B6/B3. at this point both u and v should be odd.
			t = (v - u) / 2;
			// |u| larger: t positive (replace u)
			// |v| larger: t negative (replace v)
		} while (t != 0);
		return -u * (1L << k); // gcd is u*2^k
	}

	/**
	 * Subtract two integers, checking for overflow.
	 * 
	 * @param x
	 *            Minuend.
	 * @param y
	 *            Subtrahend.
	 * @return the difference {@code x - y}.
	 * @throws RMathException
	 *             if the result can not be represented as an {@code int}.
	 * @since 1.1
	 */
	public static int subAndCheck(final int x, final int y)
			throws RMathException {
		final long s = (long) x - (long) y;
		if (s < Integer.MIN_VALUE || s > Integer.MAX_VALUE) {
			throw new RMathException("Subtraction overflow");
		}
		return (int) s;
	}

	/**
	 * Compute the natural logarithm of the factorial of {@code n}.
	 * 
	 * @param n
	 *            Argument.
	 * @return {@code n!}
	 * @throws RMathException
	 *             if {@code n < 0}.
	 */
	public static double factorialLog(final int n) throws RMathException {
		if (n < 0) {
			throw new RMathException("N is not a positive number");
		}
		if (n < 21) {
			return Math.log(FACTORIALS[n]);
		}
		double logSum = 0;
		for (int i = 2; i <= n; i++) {
			logSum += Math.log(i);
		}
		return logSum;
	}

	/**
	 * Returns n!. Shorthand for {@code n} <a
	 * href="http://mathworld.wolfram.com/Factorial.html"> Factorial</a>, the
	 * product of the numbers {@code 1,...,n}.
	 * <p>
	 * <Strong>Preconditions</strong>:
	 * <ul>
	 * <li> {@code n >= 0} (otherwise {@code IllegalArgumentException} is thrown)
	 * </li>
	 * <li>The result is small enough to fit into a {@code long}. The largest
	 * value of {@code n} for which {@code n!} < Long.MAX_VALUE} is 20. If the
	 * computed value exceeds {@code Long.MAX_VALUE} an
	 * {@code RageMathException } is thrown.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param n
	 *            argument
	 * @return {@code n!}
	 * @throws RMathException
	 *             if the result is too large to be represented by a
	 *             {@code long}.
	 * @throws RMathException
	 *             if {@code n < 0}.
	 * @throws RMathException
	 *             if {@code n > 20}: The factorial value is too large to fit in
	 *             a {@code long}.
	 */
	public static long factorial(final int n) throws RMathException {
		if (n < 0) {
			throw new RMathException("N is not positive");
		}
		if (n > 20) {
			throw new RMathException();
		}
		return FACTORIALS[n];
	}

	/**
	 * <p>
	 * Returns the least common multiple of the absolute value of two numbers,
	 * using the formula {@code lcm(a,b) = (a / gcd(a,b)) * b}.
	 * </p>
	 * Special cases:
	 * <ul>
	 * <li>The invocations {@code lcm(Integer.MIN_VALUE, n)} and
	 * {@code lcm(n, Integer.MIN_VALUE)}, where {@code abs(n)} is a power of 2,
	 * throw an {@code RageMathException}, because the result would be 2^31,
	 * which is too large for an int value.</li>
	 * <li>The result of {@code lcm(0, x)} and {@code lcm(x, 0)} is {@code 0}
	 * for any {@code x}.
	 * </ul>
	 * 
	 * @param a
	 *            Number.
	 * @param b
	 *            Number.
	 * @return the least common multiple, never negative.
	 * @throws RMathException
	 *             if the result cannot be represented as a non-negative
	 *             {@code int} value.
	 * @since 1.1
	 */
	public static int lcm(final int a, final int b) throws RMathException {
		if (a == 0 || b == 0) {
			return 0;
		}
		final int lcm = (int) Math.abs(mulAndCheck(a / gcd(a, b), b));
		if (lcm == Integer.MIN_VALUE) {
			throw new RMathException(
					"result cannot be represented as a non-negative");
		}
		return lcm;
	}

	/**
	 * Fast way to compute if the argument is a power of two.
	 * 
	 * @param n
	 *            the number to test
	 * @return true if the argument is a power of two
	 */
	public static boolean isPowerOfTwo(final long n) {
		return (n > 0) && ((n & (n - 1)) == 0);
	}

	public static float symmetry(float x, final float b) {
		x = MathUtils.mod(x, 2 * b);
		if (x > b)
			return 2 * b - x;
		return x;
	}

	/**
	 * Returns an exact representation of the <a
	 * href="http://mathworld.wolfram.com/BinomialCoefficient.html"> Binomial
	 * Coefficient</a>, "{@code n choose k}", the number of {@code k}-element
	 * subsets that can be selected from an {@code n}-element setKey.
	 * <p>
	 * <Strong>Preconditions</strong>:
	 * <ul>
	 * <li> {@code 0 <= k <= n } (otherwise {@code IllegalArgumentException} is
	 * thrown)</li>
	 * <li>The result is small enough to fit into a {@code long}. The largest
	 * value of {@code n} for which all coefficients are
	 * {@code  < Long.MAX_VALUE} is 66. If the computed value exceeds
	 * {@code Long.MAX_VALUE} an {@code ArithMeticException} is thrown.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param n
	 *            the size of the setKey
	 * @param k
	 *            the size of the subsets to be counted
	 * @return {@code n choose k}
	 * @throws NotPositiveException
	 *             if {@code n < 0}.
	 * @throws NumberIsTooLargeException
	 *             if {@code k > n}.
	 * @throws MathArithmeticException
	 *             if the result is too large to be represented by a long
	 *             integer.
	 */
	public static long binomialCoefficient(final int n, final int k)
			throws RMathException {
		checkBinomial(n, k);
		if ((n == k) || (k == 0)) {
			return 1;
		}
		if ((k == 1) || (k == n - 1)) {
			return n;
		}
		// Use symmetry for large k
		if (k > n / 2) {
			return binomialCoefficient(n, n - k);
		}

		// We use the formula
		// (n choose k) = n! / (n-k)! / k!
		// (n choose k) == ((n-k+1)*...*n) / (1*...*k)
		// which could be written
		// (n choose k) == (n-1 choose k-1) * n / k
		long result = 1;
		if (n <= 61) {
			// For n <= 61, the naive implementation cannot overflow.
			int i = n - k + 1;
			for (int j = 1; j <= k; j++) {
				result = result * i / j;
				i++;
			}
		} else if (n <= 66) {
			// For n > 61 but n <= 66, the result cannot overflow,
			// but we must take care not to overflow intermediate values.
			int i = n - k + 1;
			for (int j = 1; j <= k; j++) {
				// We know that (result * i) is divisible by j,
				// but (result * i) may overflow, so we split j:
				// Filter out the gcd, d, so j/d and i/d are integer.
				// result is divisible by (j/d) because (j/d)
				// is relative prime to (i/d) and is a divisor of
				// result * (i/d).
				final long d = MathUtils.gcd(i, j);
				result = (result / (j / d)) * (i / d);
				i++;
			}
		} else {
			// For n > 66, a result overflow might occur, so we check
			// the multiplication, taking care to not overflow
			// unnecessary.
			int i = n - k + 1;
			for (int j = 1; j <= k; j++) {
				final long d = MathUtils.gcd(i, j);
				result = MathUtils.mulAndCheck(result / (j / d), i / d);
				i++;
			}
		}
		return result;
	}

	/**
	 * Check binomial preconditions.
	 * 
	 * @param n
	 *            Size of the setKey.
	 * @param k
	 *            Size of the subsets to be counted.
	 * @throws NotPositiveException
	 *             if {@code n < 0}.
	 * @throws NumberIsTooLargeException
	 *             if {@code k > n}.
	 */
	private static void checkBinomial(final int n, final int k)
			throws RMathException {
		if (n < k) {
			throw new RMathException("Number is too large");
		}
		if (n < 0) {
			throw new RMathException("Not a positive value");
		}
	}

	/**
	 * Returns a {@code double} representation of the <a
	 * href="http://mathworld.wolfram.com/BinomialCoefficient.html"> Binomial
	 * Coefficient</a>, "{@code n choose k}", the number of {@code k}-element
	 * subsets that can be selected from an {@code n}-element setKey.
	 * <p>
	 * <Strong>Preconditions</strong>:
	 * <ul>
	 * <li> {@code 0 <= k <= n } (otherwise {@code IllegalArgumentException} is
	 * thrown)</li>
	 * <li>The result is small enough to fit into a {@code double}. The largest
	 * value of {@code n} for which all coefficients are < Double.MAX_VALUE is
	 * 1029. If the computed value exceeds Double.MAX_VALUE,
	 * Double.POSITIVE_INFINITY is returned</li>
	 * </ul>
	 * </p>
	 * 
	 * @param n
	 *            the size of the setKey
	 * @param k
	 *            the size of the subsets to be counted
	 * @return {@code n choose k}
	 * @throws NotPositiveException
	 *             if {@code n < 0}.
	 * @throws NumberIsTooLargeException
	 *             if {@code k > n}.
	 * @throws MathArithmeticException
	 *             if the result is too large to be represented by a long
	 *             integer.
	 */
	public static double binomialCoefficientDouble(final int n, final int k)
			throws RMathException {
		checkBinomial(n, k);
		if ((n == k) || (k == 0)) {
			return 1d;
		}
		if ((k == 1) || (k == n - 1)) {
			return n;
		}
		if (k > n / 2) {
			return binomialCoefficientDouble(n, n - k);
		}
		if (n < 67) {
			return binomialCoefficient(n, k);
		}

		double result = 1d;
		for (int i = 1; i <= k; i++) {
			result *= (double) (n - k + i) / (double) i;
		}

		return Math.floor(result + 0.5);
	}

	/**
	 * Returns the <a
	 * href="http://mathworld.wolfram.com/StirlingNumberoftheSecondKind.html">
	 * Stirling number of the second kind</a>, "{@code S(n,k)}", the number of
	 * ways of partitioning an {@code n}-element setKey into {@code k} non-empty
	 * subsets.
	 * <p>
	 * The preconditions are {@code 0 <= k <= n } (otherwise
	 * {@code NotPositiveException} is thrown)
	 * </p>
	 * 
	 * @param n
	 *            the size of the setKey
	 * @param k
	 *            the number of non-empty subsets
	 * @return {@code S(n,k)}
	 * @throws NotPositiveException
	 *             if {@code k < 0}.
	 * @throws NumberIsTooLargeException
	 *             if {@code k > n}.
	 * @throws MathArithmeticException
	 *             if some overflow happens, typically for n exceeding 25 and k
	 *             between 20 and n-2 (S(n,n-1) is handled specifically and does
	 *             not overflow)
	 * @since 3.1
	 */
	public static long stirlingS2(final int n, final int k)
			throws RMathException {
		if (k < 0) {
			throw new RMathException("K is not a positive number");
		}
		if (k > n) {
			throw new RMathException("Number is too large");
		}

		// the cache has never been initialized, compute the first numbers
		// by direct recurrence relation

		// as S(26,9) = 11201516780955125625 is larger than Long.MAX_VALUE
		// we must stop computation at row 26
		final int maxIndex = 26;
		final long[][] stirlingS2 = new long[maxIndex][];
		stirlingS2[0] = new long[] { 1l };
		for (int i = 1; i < stirlingS2.length; ++i) {
			stirlingS2[i] = new long[i + 1];
			stirlingS2[i][0] = 0;
			stirlingS2[i][1] = 1;
			stirlingS2[i][i] = 1;
			for (int j = 2; j < i; ++j) {
				stirlingS2[i][j] = j * stirlingS2[i - 1][j]
						+ stirlingS2[i - 1][j - 1];
			}
		}

		if (n < stirlingS2.length) {
			// the number is in the small cache
			return stirlingS2[n][k];
		} else {
			// use explicit formula to compute the number without caching it
			if (k == 0) {
				return 0;
			} else if (k == 1 || k == n) {
				return 1;
			} else if (k == 2) {
				return (1l << (n - 1)) - 1l;
			} else if (k == n - 1) {
				return binomialCoefficient(n, 2);
			} else {
				// definition formula: note that this may trigger some overflow
				long sum = 0;
				long sign = ((k & 0x1) == 0) ? 1 : -1;
				for (int j = 1; j <= k; ++j) {
					sign = -sign;
					sum += sign * binomialCoefficient(k, j) * Math.pow(j, n);
					if (sum < 0) {
						// there was an overflow somewhere
						throw new RMathException("Overflow");
					}
				}

				return sum / MathUtils.factorial(k);
			}
		}

	}

	/** All long-representable factorials */
	static final long[] FACTORIALS = new long[] { 1l, 1l, 2l, 6l, 24l, 120l,
			720l, 5040l, 40320l, 362880l, 3628800l, 39916800l, 479001600l,
			6227020800l, 87178291200l, 1307674368000l, 20922789888000l,
			355687428096000l, 6402373705728000l, 121645100408832000l,
			2432902008176640000l };
}
