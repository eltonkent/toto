/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.math;

/**
 * Contains all types of numeric & date conversion functions
 * 
 * 
 */
public final class NumericConverter {

	/**
	 * Convert two characters representing hex 00 to ff (or FF) to a byte.
	 */
	public static int hexToByte(final char c1, final char c2) {
		int b;
		if ((c1 >= 'a')) {
			b = c1 - 'a' + 10;
		} else if (c1 >= 'A') {
			b = c1 - 'A' + 10;
		} else {
			b = c1 - '0';
		}
		b <<= 4;
		if ((c2 >= 'a')) {
			b |= c2 - 'a' + 10;
		} else if (c2 >= 'A') {
			b |= c2 - 'A' + 10;
		} else {
			b |= c2 - '0';
		}
		return b;
	}

	private static void putBytes(final byte[] buf, int pos, final int val) {
		buf[pos++] = (byte) (val >>> 24);
		buf[pos++] = (byte) (val >>> 16);
		buf[pos++] = (byte) (val >>> 8);
		buf[pos++] = (byte) (val >>> 0);
	}

	private static void putBytes(final byte[] buf, int pos, final long val) {
		buf[pos++] = (byte) (val >>> 56);
		buf[pos++] = (byte) (val >>> 48);
		buf[pos++] = (byte) (val >>> 40);
		buf[pos++] = (byte) (val >>> 32);
		buf[pos++] = (byte) (val >>> 24);
		buf[pos++] = (byte) (val >>> 16);
		buf[pos++] = (byte) (val >>> 8);
		buf[pos++] = (byte) (val >>> 0);
	}

	/**
	 * Convert a String to byte
	 * 
	 * @param str
	 * @return Byte warpper for the primitive
	 */
	public static Byte toByte(final String str) {
		try {
			return (str != null) ? new Byte(Byte.parseByte(str.trim())) : null;
		} catch (final NumberFormatException ex) {
			return null;
		}
	}

	/**
	 * Convert a int to a byte[]
	 * 
	 * @param val
	 *            integer
	 * @return byte arrya
	 */
	public static byte[] toBytes(final int val) {
		final byte[] result = new byte[4];
		putBytes(result, 0, val);
		return result;
	}

	/**
	 * convert a long to byte[]
	 * 
	 * @param value
	 * @return Array of byte representing <code> value</code>
	 */
	public static byte[] toBytes(final long value) {
		final byte[] result = new byte[8];
		putBytes(result, 0, value);
		return result;
	}

	/**
	 * Convert a short to a byte array and setKey it into <code>buffer</code> at
	 * specified <code>offset</code>
	 * 
	 * @param value
	 * @param buffer
	 * @param offset
	 * @return Array of byte representing <code> value</code>
	 */
	public static void toBytes(final short value, final byte[] buffer,
			final int offset) {
		if (buffer.length - offset < 2) {
			throw new ArrayIndexOutOfBoundsException();
		}

		buffer[offset] = (byte) (value >> 8);
		buffer[offset + 1] = (byte) value;
	}

	/**
	 * Convert a byte array to an int value
	 * 
	 * @param bytes
	 * @param offset
	 * @return the int value corresponding to the 4 first bytes of the byte
	 *         array
	 */
	public static int toInt(final byte[] bytes, final int offset) {
		int value = 0;
		if ((bytes != null) && (bytes.length >= 4)) {
			for (int i = 0; i < 4; ++i) {
				value += (0x000000FF & bytes[offset + i]) << ((3 - i) * 8);
			}
		}
		return value;
	}

	/**
	 * Convert a byte array to a long value
	 * 
	 * @param bytes
	 * @param offset
	 * @return the long value corresponding to the 8 first bytes of the byte
	 *         array
	 */
	public static long toLong(final byte[] bytes, final int offset) {
		int value = 0;
		if ((bytes != null) && (bytes.length >= 8)) {
			for (int i = 0; i < 8; ++i) {
				value += (0x000000FF & bytes[offset + i]) << ((7 - i) * 8);
			}
		}
		return value;
	}

	/**
	 * Convert a byte array to a short value
	 * 
	 * @param bytes
	 * @param offset
	 * @return the short value corresponding to the 2 first bytes of the byte
	 *         array
	 */
	public static short toShort(final byte[] bytes, final int offset) {
		short value = 0;
		if ((bytes != null) && (bytes.length >= 2)) {
			value += (0x000000FF & bytes[offset]) << 8;
			value += (0x000000FF & bytes[offset + 1]);
		}
		return value;
	}

	public static byte toSignedByte(final byte b) {
		return (byte) (b + Byte.MIN_VALUE);
	}

	public static int toUnsignedInt(final byte b) {
		return (b - Byte.MIN_VALUE);
	}

}
