/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.math;

import java.text.NumberFormat;

public class RationalNumber extends Number {
	private static final NumberFormat nf = NumberFormat.getInstance();

	private static final long serialVersionUID = -1;

	public static final RationalNumber factoryMethod(long n, long d) {
		// safer than constructor - handles values outside min/max range.
		// also does some simple finding of common denominators.

		if ((n > Integer.MAX_VALUE) || (n < Integer.MIN_VALUE)
				|| (d > Integer.MAX_VALUE) || (d < Integer.MIN_VALUE)) {
			while (((n > Integer.MAX_VALUE) || (n < Integer.MIN_VALUE)
					|| (d > Integer.MAX_VALUE) || (d < Integer.MIN_VALUE))
					&& (Math.abs(n) > 1) && (Math.abs(d) > 1)) {
				// brutal, inprecise truncation =(
				// use the sign-preserving right shift operator.
				n >>= 1;
				d >>= 1;
			}

			if (d == 0) {
				throw new NumberFormatException("Invalid value, numerator: "
						+ n + ", divisor: " + d);
			}
		}

		final long gcd = gcd(n, d);
		d = d / gcd;
		n = n / gcd;

		return new RationalNumber((int) n, (int) d);
	}

	/**
	 * Return the greatest common divisor
	 */
	private static long gcd(final long a, final long b) {

		if (b == 0) {
			return a;
		} else {
			return gcd(b, a % b);
		}
	}

	public final int divisor;

	public final int numerator;

	public RationalNumber(final int numerator, final int divisor) {
		this.numerator = numerator;
		this.divisor = divisor;
	}

	@Override
	public double doubleValue() {
		return (double) numerator / (double) divisor;
	}

	@Override
	public float floatValue() {
		return (float) numerator / (float) divisor;
	}

	@Override
	public int intValue() {
		return numerator / divisor;
	}

	public boolean isValid() {
		return divisor != 0;
	}

	@Override
	public long longValue() {
		return (long) numerator / (long) divisor;
	}

	public RationalNumber negate() {
		return new RationalNumber(-numerator, divisor);
	}

	public String toDisplayString() {
		if ((numerator % divisor) == 0) {
			return "" + (numerator / divisor);
		}
		final NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(3);
		return nf.format((double) numerator / (double) divisor);
	}

	@Override
	public String toString() {
		if (divisor == 0) {
			return "Invalid rational (" + numerator + "/" + divisor + ")";
		}
		if ((numerator % divisor) == 0) {
			return nf.format(numerator / divisor);
		}
		return numerator + "/" + divisor + " ("
				+ nf.format((double) numerator / divisor) + ")";
	}
}
