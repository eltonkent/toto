package toto.bitmap;

/**
 * A wrapper implementation of {@link LuminanceSource} which inverts the
 * luminances it returns -- black becomes white and vice versa, and each value
 * becomes (255-value).
 * 
 */
public final class InvertedLuminanceSource extends LuminanceSource {

	private final LuminanceSource delegate;

	public InvertedLuminanceSource(final LuminanceSource delegate) {
		super(delegate.getWidth(), delegate.getHeight());
		this.delegate = delegate;
	}

	@Override
	public byte[] getRow(final int y, byte[] row) {
		row = delegate.getRow(y, row);
		final int width = getWidth();
		for (int i = 0; i < width; i++) {
			row[i] = (byte) (255 - (row[i] & 0xFF));
		}
		return row;
	}

	@Override
	public byte[] getMatrix() {
		final byte[] matrix = delegate.getMatrix();
		final int length = getWidth() * getHeight();
		final byte[] invertedMatrix = new byte[length];
		for (int i = 0; i < length; i++) {
			invertedMatrix[i] = (byte) (255 - (matrix[i] & 0xFF));
		}
		return invertedMatrix;
	}

	@Override
	public boolean isCropSupported() {
		return delegate.isCropSupported();
	}

	public @Override
	LuminanceSource crop(final int left, final int top, final int width,
			final int height) {
		return new InvertedLuminanceSource(delegate.crop(left, top, width,
				height));
	}

	@Override
	public boolean isRotateSupported() {
		return delegate.isRotateSupported();
	}

	/**
	 * @return original delegate {@link LuminanceSource} since invert undoes
	 *         itself
	 */
	@Override
	public LuminanceSource invert() {
		return delegate;
	}

	@Override
	public LuminanceSource rotateCounterClockwise() {
		return new InvertedLuminanceSource(delegate.rotateCounterClockwise());
	}

	@Override
	public LuminanceSource rotateCounterClockwise45() {
		return new InvertedLuminanceSource(delegate.rotateCounterClockwise45());
	}

}
