package toto.bitmap;

/**
 * Created by Elton Kent on 4/15/2015.
 */
public class ToToBitmapException extends Exception {
    public ToToBitmapException(String su) {
        super(su);
    }

    public ToToBitmapException() {
        super();
    }
}
