package toto.bitmap;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * @see ToToJavaBitmap
 * @see toto.bitmap.ToToNativeBitmap
 * @see ToToArrayBasedBitmap
 */
public interface ToToBitmap {

	public void setPixel(final int x, final int y, final int color);

	public int getPixel(final int x, final int y);

	public int getDepth();

	public int getWidth();

	public int getHeight();

	public Rect getRect();

	public void recycle();

	public ToToBitmap copy();

	public Rect getFilterBounds();

	public void setFilterBounds(final int x, final int y, final int width,
			final int height);

	public int getFilterHeight();

	public int getFilterWidth();

	public int getFilterX();

	public int getFilterY();

	public Bitmap getBitmap();

	public int[] getPixels();

	public void setPixels(int[] pixels, int w, int h);

	public void setPixels(int[] pixels);
}
