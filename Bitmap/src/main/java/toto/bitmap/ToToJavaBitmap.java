package toto.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.zip.InflaterInputStream;

import toto.graphics.color.convert.YUVConverter;
import toto.lang.reflect.FieldUtils;

/**
 * 
 * An Extension to the android bitmap allowing you to manipulate and extract
 * parts of the bitmap.
 * 
 * @see ToToNativeBitmap Native bitmap
 * @see ToToBitmap
 */
public class ToToJavaBitmap implements ToToBitmap {

	private static final class PixMeta {
		private int[] pixl;
		private int w;
		private int h;
	}

	/**
	 * Construct {@link ToToJavaBitmap} from pixel data.
	 * <p>
	 * This method is used in conjunction with
	 * {@link #writePixels(OutputStream)}
	 * </p>
	 * 
	 * @see ToToJavaBitmap#readPixels(FileInputStream, boolean)
	 * @param is
	 * @param bufferSize
	 *            The buffer size to read the pixel file. A good buffer size is
	 *            equal to the size of the file being read(if its know). Since
	 *            the implementation uses NIO, the entire file is taken in one
	 *            read operation(atleast from a java perspective).
	 * @return
	 * @throws IOException
	 */
	public static ToToJavaBitmap readPixels(final InputStream is,
			final boolean isCompressed, final int bufferSize,
			final Config config) throws IOException {
		final PixMeta pix = isCompressed ? readPixelsCompressed(is)
				: readPixels(is, bufferSize);
		return new ToToJavaBitmap(pix.pixl, pix.w, pix.h, config);
	}

	/**
	 * Dedicated method for file based streams as its optimized and a bit
	 * faster.
	 * 
	 * @see #readPixels(InputStream, boolean, int)
	 * @param is
	 *            file stream
	 * @return
	 * @throws IOException
	 */
	public static ToToJavaBitmap readPixels(final FileInputStream is,
			final boolean isCompressed, final Config config) throws IOException {
		final PixMeta pix = isCompressed ? readPixelsCompressed(is)
				: readPixels(is);
		return new ToToJavaBitmap(pix.pixl, pix.w, pix.h, config);
	}

	private static PixMeta readPixelsCompressed(final InputStream is)
			throws IOException {
		long t = System.currentTimeMillis();
		final InflaterInputStream iis = new InflaterInputStream(is);
		// final GZIPInputStream iis = new GZIPInputStream(is);
		final DataInputStream dos = new DataInputStream(iis);
		final PixMeta meta = new PixMeta();
		meta.w = dos.readInt();
		meta.h = dos.readInt();
		final int len = dos.readInt();

		final int[] pix = new int[len];

		for (int i = 0; i < len; i++) {
			pix[i] = dos.readInt();
		}
		meta.pixl = pix;
		dos.close();
		t = System.currentTimeMillis() - t;
		Log.d("RAGE", len + " Pixels read with compression on in " + t + "ms.");
		return meta;
	}

	private static PixMeta readPixels(final FileInputStream is)
			throws IOException {
		long t = System.currentTimeMillis();
		final FileChannel fs = is.getChannel();
		final ByteBuffer buffer = ByteBuffer.allocateDirect((int) fs.size());
		fs.read(buffer);
		buffer.rewind();
		final PixMeta meta = getPixInfo(buffer);
		fs.close();
		t = System.currentTimeMillis() - t;
		Log.d("RAGE", meta.pixl.length
				+ " Pixels written with NO compression on in " + t + "ms.");
		return meta;
	}

	private static PixMeta getPixInfo(final ByteBuffer buff) {
		final PixMeta meta = new PixMeta();
		meta.w = buff.getInt();
		meta.h = buff.getInt();
		final int len = buff.getInt();

		final int[] pix = new int[len];

		for (int i = 0; i < len; i++) {
			pix[i] = buff.getInt();
		}
		meta.pixl = pix;
		return meta;
	}

	private static PixMeta readPixels(final InputStream is, final int bufferSize)
			throws IOException {
		long t = System.currentTimeMillis();
		final ReadableByteChannel channel = Channels.newChannel(is);// is.getChannel();
		final ByteBuffer buffer = ByteBuffer.allocateDirect(bufferSize);
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		while (channel.read(buffer) != -1 || buffer.position() > 0) {
			buffer.rewind();
			bos.write(buffer.array());
		}
		buffer.clear();
		channel.close();
		final ByteBuffer dis = ByteBuffer.wrap(bos.toByteArray());
		final PixMeta meta = getPixInfo(dis);
		t = System.currentTimeMillis() - t;
		Log.d("RAGE", meta.pixl.length
				+ " Pixels written with NO compression  in " + t + "ms.");
		return meta;
	}

	@Override
	public String toString() {
		return "RageBitmap [width=" + width + ", height=" + height
				+ ", aspectRatio=" + aspectRatio + "]";
	}

	int width;

	int height;

	private float aspectRatio;

	// int[] pixelArray;

	int filterX;

	int filterY;

	int filterWidth;

	int filterHeight;

	private Bitmap bitmap;

	public ToToJavaBitmap(final Bitmap bitmap) {
		this(bitmap, false);
	}

	/**
	 * Creates a rage bitmap.
	 * 
	 * @param bitmap
	 * @param recycleOriginal
	 *            make the provided bitmap available for GC
	 */
	public ToToJavaBitmap(final Bitmap bitmap, final boolean recycleOriginal) {
		if (!bitmap.isMutable()) {
			makeMutable(bitmap, recycleOriginal);
		} else {
			this.bitmap = bitmap;
		}
		init();
	}

	private void makeMutable(final Bitmap bitmap, final boolean recycleOriginal) {
		this.bitmap = bitmap.copy(bitmap.getConfig(), true);
		if (recycleOriginal)
			bitmap.recycle();
	}

	public ToToJavaBitmap(final byte[] bitmapBytes,
                          final BitmapFactory.Options options) {
		this(BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length,
				options));
	}

	/**
	 * Construct a ToTo Bitmap from camera yuv420sp encoding
	 * 
	 * @param yuv420sp
	 */
	public ToToJavaBitmap(final byte[] yuv420sp, final int width,
                          final int height, final Config config) {
		this(YUVConverter.toRGB(yuv420sp, null, width, height), width, height,
				config);
	}

	/**
	 * Create bitmap using an inputstream.
	 * 
	 * @param inputStream
	 * @param options
	 *            decoding options. can be null.
	 */
	public ToToJavaBitmap(final InputStream inputStream,
                          BitmapFactory.Options options) {
		if (options == null) {
			options = new BitmapFactory.Options();
		}
		setMutableField(options);
		this.bitmap = BitmapFactory.decodeStream(inputStream, null, options);
		init();
	}

	/**
	 * Decode bitmap using resource id.
	 * 
	 * @param context
	 *            application context.
	 * @param resId
	 *            resource id of the bitmap drawable
	 * @param options
	 *            decoding options. can be null.
	 */
	public ToToJavaBitmap(final Context context, final int resId,
                          BitmapFactory.Options options) {
		if (options == null) {
			options = new BitmapFactory.Options();
		}
		setMutableField(options);
		this.bitmap = BitmapFactory.decodeResource(context.getResources(),
				resId, null);
		init();
	}

	/**
	 * work around for pre API 10
	 * 
	 * @param options
	 */
	private void setMutableField(final BitmapFactory.Options options) {
		try {
			FieldUtils
					.writeField(options, "inMutable", new Boolean(true), true);
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	private void init() {
		/* in case the mutable field isnt */
		if (!bitmap.isMutable()) {
			makeMutable(bitmap, true);
		}
		this.width = this.bitmap.getWidth();
		this.height = this.bitmap.getHeight();
		this.aspectRatio = (this.width / this.height);
		setFilterBounds(0, 0, this.width, this.height);
	}

	/**
	 * Create a Rage Bitmap from RGB pixels.
	 * 
	 * @param pixels
	 * @param width
	 * @param height
	 */
	public ToToJavaBitmap(final int[] pixels, final int width, final int height,
                          final Config config) {
		this(Bitmap.createBitmap(pixels, width, height, config));
	}

	public float getAspectRatio() {
		return aspectRatio;
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public Bitmap getBitmap() {
		return bitmap;
	}

	/**
	 * returns the indices of the given column of the bitmap populated in
	 * <code>columnIdx</code>
	 * <p>
	 * this prev
	 * </p>
	 * 
	 * @param columnIdx
	 * @param colNumber
	 */
	public void getColumnIndices(final int[] columnIdx, final int colNumber,
			final int columnWidth) {

	}

	public Rect getFilterBounds() {
		return new Rect(filterX, filterY, filterWidth, filterHeight);
	}

	public int getHeight() {
		return this.height;
	}

	/**
	 * Get the pixel at the given <code>x</code>,<code>y</code> coordinate
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public int getPixel(final int x, final int y) {
		return bitmap.getPixel(x, y);
	}

	public void setPixel(final int x, final int y, final int color) {
		bitmap.setPixel(x, y, color);
	}

	public int[] getPixels() {
		return getPixels(bitmap);
	}

	public void getRowIndices(final short[] rowIdx, final int rowNumber) {

	}

	/**
	 * 
	 * @see #getSubBitmap(int[], int, int, int, int)
	 * @see #setSubBitmap(int[], int, int, int, int)
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @return
	 */
	public ToToJavaBitmap getSubBitmap(final int x, final int y, final int width,
			final int height) {
		return new ToToJavaBitmap(bitmap.createBitmap(bitmap, x, y, width, height));
	}

	public void setSubBitmap(final int[] subBitmap, final int x, final int y,
			final int width, final int height) {
		final int spanY = y + height;
		final int spanX = x + width;
		int i, wIdx, idx = 0;
		for (i = y; i < spanY; i++) {
			for (wIdx = x; wIdx < spanX; wIdx++) {
				bitmap.setPixel(wIdx, i, subBitmap[idx]);
				idx++;
			}
		}
	}

	/**
	 * Extract a portion of the bitmap using the coordinates and specified width
	 * and height.
	 * 
	 * @param subBitmap
	 *            array to contain the sub bitmap. Its length is
	 *            <code>width</code>X<code>height</code>
	 * @param x
	 *            starting horizontal point on the bitmap
	 * @param y
	 *            starting vertical point on the bitmap
	 * @param width
	 *            width of the bitmap starting from <code>x</code>
	 * @param height
	 *            height of the bitmap starting from <code>y</code>
	 * @throws IllegalArgumentException
	 *             if the sum of the <code>x</code> coordinate and
	 *             <code>width</code> is greater than this bitmap's width.
	 * @throws IllegalArgumentException
	 *             if the sum of the <code>Y</code> coordinate and
	 *             <code>Height</code> is greater than this bitmap's height.
	 */
	public void getSubBitmap(final int[] subBitmap, final int x, final int y,
			final int width, final int height) {
		final int spanY = y + height;
		final int spanX = x + width;
		if (spanX > this.width) {
			throw new IllegalArgumentException(
					"X + width is greater than the width of the bitmap");
		}
		if (spanY > this.height) {
			throw new IllegalArgumentException(
					"Y + height is greater than the height of the bitmap");
		}
		int wIdx = 0;
		int idx = 0;
		int i;
		for (i = y; i < spanY; i++) {
			for (wIdx = x; wIdx < spanX; wIdx++) {
				subBitmap[idx] = bitmap.getPixel(wIdx, i);
				idx++;
			}
		}
	}

	public int getWidth() {
		return this.width;

	}

	/**
	 * Get columns of pixels at the given column number and grabWidth
	 * 
	 * @param colPix
	 *            array to populate the column pixels. its length is
	 *            <code>bitmap height</code> X <code>grabWidth</code>)
	 * @param colNumber
	 * @param grabWidth
	 */
	public void grabColumn(final int[] colPix, final int colNumber,
			final int grabWidth) {
		int wIdx = 0;
		int idx = 0;
		int pxIdx = 0;
		for (int i = 0; i < height; i++) {
			for (wIdx = 0; wIdx < grabWidth; wIdx++) {
				pxIdx = colNumber + wIdx;
				colPix[idx] = bitmap.getPixel(i, pxIdx);
				idx++;
			}
		}
	}

	/**
	 * Get columns of pixels at the given column number and grabWidth
	 * <p>
	 * This method does a series of checks to ensure that the method arguments
	 * are correct.
	 * </p>
	 * 
	 * @param columnPix
	 *            array to populate the column pixels. its length is
	 *            <code>bitmap height</code> X <code>grabWidth</code>)
	 * @param columnNumber
	 * @param grabWidth
	 */
	public void grabColumnSafely(final int[] columnPix, final int columnNumber,
			final int grabWidth) {
		if (columnNumber > width) {
			throw new IllegalArgumentException(
					"column number requested is greater than the bitmap width");
		}
		if (((columnNumber - 1) + grabWidth) > width) {
			throw new IllegalArgumentException(
					"column number + column width is greater than the bitmap width");
		}
		if (columnNumber < 0) {
			throw new IllegalArgumentException("column number is less than 0");
		}
		if (grabWidth < 0) {
			throw new IllegalArgumentException("column width is less than 0");
		}
		grabColumn(columnPix, columnNumber, grabWidth);
	}

	/**
	 * Get rows of pixels at the given row number and grabHeight.
	 * 
	 * @param rowPix
	 *            array to populate the row pixels. its length is (
	 *            <code>bitmap width</code> X <code>grabHeight</code>)
	 * @param rowNumber
	 *            row from which to start grabbing pixels
	 * @param grabHeight
	 *            number of rows from <code>rowNumber</code> to grab pixels . if
	 *            the <code>rowNumber</code>
	 * @return pixels at <code>rowNumber</code>X<code>grabHeight</code>
	 * @see #grabRowSafely(int[], int, int)
	 */
	// public void grabRow(final int[] rowPix, final int rowNumber,
	// final int grabHeight) {
	//
	// int hIdx = 0;
	// int currIdx = 0;
	//
	// for (hIdx = 0; hIdx < grabHeight; hIdx++) {
	// currIdx = rowNumber + hIdx;
	// System.arraycopy(pixelArray, currIdx * this.width, rowPix, hIdx
	// * width, width);
	// }
	// }

	/**
	 * Get a row of pixels at the given row number and row height.
	 * <p>
	 * This method does a series of checks to ensure that the method arguments
	 * are correct. t
	 * </p>
	 * 
	 * @param rowPix
	 *            array to populate the row pixels. its length is (
	 *            <code>bitmap width</code><b> X</b> <code>grabHeight</code>)
	 * @param rowNumber
	 *            row from which to start grabbing pixels
	 * @param grabHeight
	 *            number of rows from <code>rowNumber</code> to grab pixels
	 *            until. if the <code>rowNumber</code>
	 * @throws IllegalArgumentException
	 *             if the <code>rowNumber</code> + <code>grabHeight</code> is
	 *             greater than the bitmap height.
	 * @throws IllegalArgumentException
	 *             if the <code>rowNumber</code> requested is greater than the
	 *             bitmap height.
	 * @throws IllegalArgumentException
	 *             if <code>rowNumber</code> is less than 0.
	 * @throws IllegalArgumentException
	 *             <code>grabHeight</code> is less than 0.
	 * @see RageJavaBitmap#grabRow(int[], int, int)
	 */
	// public void grabRowSafely(final int[] rowPix, final int rowNumber,
	// final int grabHeight) {
	// if (rowNumber > height) {
	// throw new IllegalArgumentException(
	// "row number requested is greater than the bitmap height");
	// }
	// if ((rowNumber + grabHeight) > height) {
	// throw new IllegalArgumentException(
	// "row number + row height is greater than the bitmap height");
	// }
	// if (rowNumber < 0) {
	// throw new IllegalArgumentException("row number is less than 0");
	// }
	// if (grabHeight < 0) {
	// throw new IllegalArgumentException("row height is less than 0");
	// }
	// grabRow(rowPix, rowNumber, grabHeight);
	// }

	/**
	 * 
	 * @param columnPix
	 * @param columnNumber
	 * @param columnWidth
	 */
	public void setColumn(final int[] columnPix, final int columnNumber,
			final int columnWidth) {

	}

	/**
	 * Set the bounds for any filter operations. the default bounds are the
	 * width and height of the bitmap
	 * 
	 * @param x
	 * @param y
	 * @param width
	 *            width of the filter bounds. if the width is greater than the
	 *            bitmap width, then the bitmap width is used.
	 * @param height
	 *            height of the filter bounds. if the height is greater than the
	 *            bitmap height, then the bitmap width is height.
	 */
	public void setFilterBounds(final int x, final int y, final int width,
			final int height) {
		if (x < 0 || y < 0 || width < 0 || height < 0) {
			throw new IllegalArgumentException("Negative filter bounds");
		}
		if (x > width || y > height) {
			throw new IllegalArgumentException(
					"X/Y corrdinate is greater than Width/Height");
		}
		this.filterX = x;
		this.filterY = y;
		this.filterHeight = ((filterY + height) > this.height) ? this.height
				: filterY + height;
		this.filterWidth = ((filterX + width) > this.width) ? this.width
				: filterX + width;
	}

	public void setRow(final int[] rowPix, final int rowNumber,
			final int width, final int height) {
	}

	/**
	 * Write the entire pixel data to the output stream.
	 * <p>
	 * Its worth noting that the raw pixel data is almost twice the size of the
	 * image itself.
	 * </p>
	 * 
	 * @param os
	 * @param compress
	 *            compress pixels as its being written to the given stream.
	 *            Compression makes writing pixels slower but drastically
	 *            reduces the size of the final file.
	 * @param compressionStrategy
	 *            compression strategy to use if <code>compress</code> is
	 *            <code>true</code>
	 * @see #COMPRESSION_BEST
	 * @see #COMPRESSION_DEFLATE
	 * @see #COMPRESSION_HUFFMAN
	 * @see #COMPRESSION_NO
	 * @see #COMPRESSION_SPEED
	 * @throws IOException
	 * 
	 * 
	 */
	// public void writePixels(final OutputStream os, final boolean compress,
	// final int compressionStrategy) throws IOException {
	// writePixelsToStream(os, compress, compressionStrategy, 0, 0,
	// this.width, this.height);
	// }

	/**
	 * Write the pixel data specified by the given bounds
	 * 
	 * @param os
	 * @param compress
	 *            compress pixels as its being written to the given stream.
	 *            Compression makes writing pixels slower but drastically
	 *            reduces the size of the final file.
	 * @param compressionStrategy
	 *            compression strategy to use if <code>compress</code> is
	 *            <code>true</code>.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * 
	 * @see #COMPRESSION_BEST
	 * @see #COMPRESSION_DEFLATE
	 * @see #COMPRESSION_HUFFMAN
	 * @see #COMPRESSION_NO
	 * @see #COMPRESSION_SPEED
	 * @throws IOException
	 */
	// public void writePixels(final OutputStream os, final boolean compress,
	// final int compressionStrategy, final int x, final int y,
	// final int width, final int height) throws IOException {
	// writePixelsToStream(os, compress, compressionStrategy, x, y, width,
	// height);
	// }

	/**
	 * Write raw pixels to the given file stream
	 * 
	 * @param os
	 *            file stream. pixels are written using NIO classes and hence
	 *            only streams that contain channels can be used
	 * @param compress
	 *            compress the pixels before writing to stream.
	 * @param compressionStrategy
	 *            compression strategy to use if <code>compress</code> is
	 *            <code>true</code>.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @see #COMPRESSION_BEST
	 * @see #COMPRESSION_DEFLATE
	 * @see #COMPRESSION_HUFFMAN
	 * @see #COMPRESSION_NO
	 * @see #COMPRESSION_SPEED
	 * @throws IOException
	 */
	// private void writePixelsToStream(final OutputStream os,
	// final boolean compress, final int compressionStrategy, final int x,
	// final int y, final int width, final int height) throws IOException {
	// if (x < 0 || y < 0 || width < 0 || height < 0) {
	// throw new IllegalArgumentException("Negative bounding values");
	// }
	// if (x > width || y > height) {
	// throw new IllegalArgumentException(
	// "X/Y coordinate is > than width/height");
	// }
	// final int spanY = ((y + height) > this.height) ? this.height : y
	// + height;
	// final int spanX = ((x + width) > this.width) ? this.width : x + width;
	// if (compress) {
	// writePixelsCompressed(os, compressionStrategy, x, y, width, height);
	// } else {
	// long t = System.currentTimeMillis();
	// final WritableByteChannel channel = Channels.newChannel(os);
	// final int w = x + width > this.width ? this.width - x : width;
	// final int h = y + height > this.height ? this.height - y : height;
	// final int len = w * h;
	// final ByteBuffer buffer = ByteBuffer
	// .allocateDirect((len + 1/* w */+ 1/* h */+ 1/* arraylength */) * 4);
	// /* write width */
	// buffer.putInt(w);
	// /* write height */
	// buffer.putInt(h);
	// buffer.putInt(len/* pixelArray.length */);
	//
	// int wIdx = 0;
	// int i = 0;
	// for (i = y; i < spanY; i++) {
	// for (wIdx = x; wIdx < spanX; wIdx++) {
	// buffer.putInt(pixelArray[(i * this.width + wIdx)]);
	// }
	// }
	// buffer.flip();
	// while (buffer.hasRemaining()) {
	// channel.write(buffer);
	// }
	// buffer.clear();
	// channel.close();
	// os.close();
	// t = System.currentTimeMillis() - t;
	// Log.d("RAGE", len + " Pixels written with NO compression  in " + t
	// + "ms.");
	// }
	// }

	/**
	 * Compression strategy for speed
	 */
	public static final int COMPRESSION_SPEED = 1;
	/**
	 * Huffman compression strategy
	 */
	public static final int COMPRESSION_HUFFMAN = 2;

	/**
	 * Best compression strategy
	 */
	public static final int COMPRESSION_BEST = 9;
	/**
	 * Deflate compression strategy
	 */
	public static final int COMPRESSION_DEFLATE = 8;
	/**
	 * No compression. for the compression strategy
	 */
	public static final int COMPRESSION_NO = 0;

	/**
	 * Write raw pixels to the given file stream
	 * 
	 * @param os
	 *            file stream. pixels are written using NIO classes and hence
	 *            only streams that contain channels can be used
	 * 
	 * @param top
	 * @param top
	 * @param wdth
	 * @param ht
	 * @throws IOException
	 */
	// private void writePixelsCompressed(final OutputStream os,
	// final int compressionStrategy, final int x, final int y,
	// final int wdth, final int ht) throws IOException {
	// long t = System.currentTimeMillis();
	// final int spanY = ((y + ht) > this.height) ? this.height : y + ht;
	// final int spanX = ((x + wdth) > this.width) ? this.width : x + wdth;
	//
	// final Deflater def = new Deflater(compressionStrategy);
	// final DeflaterOutputStream defos = new DeflaterOutputStream(os, def);
	// // final GZIPOutputStream defos = new GZIPOutputStream(os);
	// final DataOutputStream dos = new DataOutputStream(defos);
	// final int w = x + wdth > this.width ? this.width - x : wdth;
	// final int h = y + ht > this.height ? this.height - y : ht;
	// final int len = w * h;
	// dos.writeInt(w);
	// dos.writeInt(h);
	// dos.writeInt(len);
	//
	// int wIdx = 0;
	// int i = 0;
	// for (i = y; i < spanY; i++) {
	// for (wIdx = x; wIdx < spanX; wIdx++) {
	// dos.writeInt(pixelArray[(i * this.width + wIdx)]);
	// }
	// }
	// t = System.currentTimeMillis() - t;
	// Log.d("RAGE", len + " Pixels written with compression on in " + t
	// + "ms.");
	// dos.close();
	// }

	public ToToJavaBitmap[] slice() {
		return null;
	}

	@Override
	public ToToBitmap copy() {
		return new ToToJavaBitmap(bitmap.copy(bitmap.getConfig(), true), false);
	}

//	/**
//	 * Get the checksum of the bitmap's pixel array.
//	 * <p>
//	 * Can be used as a fast way to compare two bitmaps of equal dimensions
//	 * </p>
//	 *
//	 * @return Fletcher32 checksum of the bitmap's pixel array
//	 */
//	public long getChecksum() {
//		return new Fletcher32().digest(BitmapUtils.getPixels(bitmap));
//	}

	/**
	 * Release memory allocated by this bitmap.
	 */
	public void recycle() {
		bitmap.recycle();
		this.filterHeight = this.filterWidth = this.filterX = this.filterY = this.width = this.height = 0;
	}

	private boolean isSaved;
	private Bitmap savedPix;

	/**
	 * Can be used to save bitmap state before its pixels are manipulated using
	 * operations like filters.
	 * <p>
	 * This method is thread safe
	 * </p>
	 * 
	 * @see ToToJavaBitmap#restore()
	 */
	public synchronized void save() {
		isSaved = true;
		savedPix = bitmap.copy(bitmap.getConfig(), true);
	}

	/**
	 * Restore pixel state.
	 * <p>
	 * This method is thread safe
	 * </p>
	 * 
	 * @see #save()
	 */
	public synchronized void restore() {
		if (!isSaved) {
			throw new IllegalStateException(
					"RAGE bitmap state is not saved using save() or has been restored() already");
		}
		bitmap = savedPix;
		isSaved = false;
		savedPix = null;
	}

	//
	// /**
	// * uses JNI to write pixels to the given stream
	// */
	// public void fastWritePixels(final OutputStream os, final int x, final int
	// y,
	// final int width, final int height) throws IOException {
	// if (x < 0 || y < 0 || width < 0 || height < 0) {
	// throw new IllegalArgumentException("Negative bounding values");
	// }
	// if (x > width || y > height) {
	// throw new
	// IllegalArgumentException("X/Y coordinate is > than width/height");
	// }
	// final int spanY = ((y + height) > this.height) ? this.height : y +
	// height;
	// final int spanX = ((x + width) > this.width) ? this.width : x + width;
	// long t = System.currentTimeMillis();
	// final WritableByteChannel channel = Channels.newChannel(os);
	// final int w = x + width > this.width ? this.width - x : width;
	// final int h = y + height > this.height ? this.height - y : height;
	// final int len = w * h;
	// final ByteBuffer buffer = NativeNIOUtils.newByteBuffer((pixelArray.length
	// / 3) * 4);
	// // NativeNIOUtils.newByteBuffer((len + 1/* w */+ 1/* h */+ 1/*
	// // arraylength */) * 4);
	//
	// // ByteBuffer
	// // .allocateDirect((len + 1/* w */+ 1/* h */+ 1/* arraylength */) * 4);
	// /* write width */
	// buffer.putInt(w);
	// /* write height */
	// buffer.putInt(h);
	// buffer.putInt(len/* pixelArray.length */);
	//
	// // final int wIdx = 0;
	// // int i = 0;
	// // for (i = y; i < spanY; i++) {
	// NativeNIOUtils.N_copy(pixelArray, 0, buffer, pixelArray.length / 3);
	// //
	// // // for (wIdx = x; wIdx < spanX; wIdx++) {
	// // // buffer.putInt(pixelArray[(i * this.width + wIdx)]);
	// // // }
	// // }
	//
	// buffer.flip();
	// while (buffer.hasRemaining()) {
	// channel.write(buffer);
	// }
	// buffer.clear();
	// channel.close();
	// os.close();
	// t = System.currentTimeMillis() - t;
	// Log.d("RAGE", len + " Pixels written with NO compression  in " + t +
	// "ms.");
	//
	// }

	@Override
	public int getDepth() {
		return 0;
	}

	@Override
	public Rect getRect() {
		return new Rect(0, 0, width, height);
	}

	@Override
	public int getFilterHeight() {
		return filterHeight;
	}

	@Override
	public int getFilterWidth() {
		return filterWidth;
	}

	@Override
	public int getFilterX() {
		return filterX;
	}

	@Override
	public int getFilterY() {
		return filterY;
	}

	@Override
	public void setPixels(final int[] pixels, final int w, final int h) {
		bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
	}

	@Override
	public void setPixels(final int[] pixels) {
		setPixels(pixels, width, height);

	}
	 static int[] getPixels(final Bitmap bitmap) {
		final int w = bitmap.getWidth();
		final int h = bitmap.getHeight();
		final int[] pixels = new int[w * h];
		bitmap.getPixels(pixels, 0, w, 0, 0, w, h);
		return pixels;
	}

}
