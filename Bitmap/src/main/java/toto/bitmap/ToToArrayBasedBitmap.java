package toto.bitmap;

import toto.lang.ArrayUtils;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Rect;

/**
 * {@link ToToBitmap} that uses an integer array as a back-end.
 * 
 * @author Mobifluence Interactive
 * 
 */
public class ToToArrayBasedBitmap implements ToToBitmap {

	int[] pixels;
	int width;
	int height, filterX, filterY, filterHeight, filterWidth;

	public ToToArrayBasedBitmap(final int[] pixels, final int width,
                                final int height) {
		this.pixels = pixels;
		this.width = width;
		this.height = height;
		this.filterHeight = height;
		this.filterWidth = width;
	}

	@Override
	public void setPixel(final int x, final int y, final int color) {
		pixels[y * width + x] = color;
	}

	@Override
	public int getPixel(final int x, final int y) {
		return pixels[y * width + x];
	}

	@Override
	public int getDepth() {
		return 32;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public Rect getRect() {
		return new Rect(0, 0, width, height);
	}

	@Override
	public void recycle() {
		pixels = null;
	}

	@Override
	public ToToBitmap copy() {
		final int[] pixls = ArrayUtils.copyOf(pixels, pixels.length);
		return new ToToArrayBasedBitmap(pixls, width, height);
	}

	@Override
	public Rect getFilterBounds() {
		return new Rect(filterX, filterY, filterWidth, filterHeight);
	}

	/**
	 * Set the bounds for any filter operations. the default bounds are the
	 * width and height of the bitmap
	 * 
	 * @param x
	 * @param y
	 * @param width
	 *            width of the filter bounds. if the width is greater than the
	 *            bitmap width, then the bitmap width is used.
	 * @param height
	 *            height of the filter bounds. if the height is greater than the
	 *            bitmap height, then the bitmap width is height.
	 */
	public void setFilterBounds(final int x, final int y, final int width,
			final int height) {
		if (x < 0 || y < 0 || width < 0 || height < 0) {
			throw new IllegalArgumentException("Negative filter bounds");
		}
		if (x > width || y > height) {
			throw new IllegalArgumentException(
					"X/Y corrdinate is greater than Width/Height");
		}
		this.filterX = x;
		this.filterY = y;
		this.filterHeight = ((filterY + height) > this.height) ? this.height
				: filterY + height;
		this.filterWidth = ((filterX + width) > this.width) ? this.width
				: filterX + width;
	}

	@Override
	public int getFilterHeight() {
		return filterHeight;
	}

	@Override
	public int getFilterWidth() {
		return filterWidth;
	}

	@Override
	public int getFilterX() {
		return filterX;
	}

	@Override
	public int getFilterY() {
		return filterY;
	}

	@Override
	public Bitmap getBitmap() {
		return Bitmap.createBitmap(pixels, width, height, Config.ARGB_8888);
	}

	@Override
	public int[] getPixels() {
		return pixels;
	}

	@Override
	public void setPixels(final int[] pixels, final int w, final int h) {
		this.pixels = pixels;
		this.width = w;
		this.height = h;
	}

	@Override
	public void setPixels(final int[] pixels) {
		this.pixels = pixels;
	}

}
