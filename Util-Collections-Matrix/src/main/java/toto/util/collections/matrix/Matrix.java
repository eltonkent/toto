package toto.util.collections.matrix;

/**
 * Created by Elton Kent on 12/20/13.
 */
public interface Matrix {

	public int getHeight();

	public int getWidth();
	// public Number get(int x, int y);
}
