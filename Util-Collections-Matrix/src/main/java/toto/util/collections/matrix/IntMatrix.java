package toto.util.collections.matrix;

public class IntMatrix implements Matrix {

	private final int[][] ints;
	private final int width;
	private final int height;

	public IntMatrix(int width, int height) {
		ints = new int[height][width];
		this.width = width;
		this.height = height;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getWidth() {
		return width;
	}

	public int get(int x, int y) {
		return ints[y][x];
	}

	public void set(int x, int y, int value) {
		ints[y][x] = value;
	}

	public void clear(int value) {
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				ints[y][x] = value;
			}
		}
	}
}
