LOCAL_PATH := $(call my-dir)
CRYPT_PP_PATH := $(LOCAL_PATH)/cryptopp-SVN
include $(CLEAR_VARS)

LOCAL_CPPFLAGS += -ffunction-sections -fdata-sections -fvisibility=hidden
LOCAL_CFLAGS += -ffunction-sections -fdata-sections

#remove duplicate code.
ifeq ($(TARGET_ARCH),mips)
  LOCAL_LDFLAGS += -Wl,--gc-sections
else
  LOCAL_LDFLAGS += -Wl,--gc-sections,--icf=safe
endif

LOCAL_LDLIBS +=  -llog
LOCAL_C_INCLUDES :=  $(LOCAL_PATH)/cryptopp-SVN 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/scrypt/include
LOCAL_MODULE    := Security_Crypto
LOCAL_SRC_FILES := CryptoUtilsNative.c \
	scrypt/sha256.c \
	scrypt/crypto_scrypt-nosse.c \
	cryptopp-SVN/3way.cpp \
	cryptopp-SVN/adler32.cpp \
	cryptopp-SVN/algebra.cpp \
	cryptopp-SVN/algparam.cpp \
	cryptopp-SVN/arc4.cpp \
	cryptopp-SVN/asn.cpp \
	cryptopp-SVN/authenc.cpp \
	cryptopp-SVN/base32.cpp \
	cryptopp-SVN/base64.cpp \
	cryptopp-SVN/basecode.cpp \
	cryptopp-SVN/bench.cpp \
	cryptopp-SVN/bench2.cpp \
	cryptopp-SVN/bfinit.cpp \
	cryptopp-SVN/blowfish.cpp \
	cryptopp-SVN/blumshub.cpp \
	cryptopp-SVN/camellia.cpp \
	cryptopp-SVN/cast.cpp \
	cryptopp-SVN/casts.cpp \
	cryptopp-SVN/cbcmac.cpp \
	cryptopp-SVN/ccm.cpp \
	cryptopp-SVN/channels.cpp \
	cryptopp-SVN/cmac.cpp \
	cryptopp-SVN/cpu.cpp \
	cryptopp-SVN/crc.cpp \
	cryptopp-SVN/cryptlib.cpp \
	cryptopp-SVN/cryptlib_bds.cpp \
	cryptopp-SVN/datatest.cpp \
	cryptopp-SVN/default.cpp \
	cryptopp-SVN/des.cpp \
	cryptopp-SVN/dessp.cpp \
	cryptopp-SVN/dh.cpp \
	cryptopp-SVN/dh2.cpp \
	cryptopp-SVN/dll.cpp \
	cryptopp-SVN/dlltest.cpp \
	cryptopp-SVN/dsa.cpp \
	cryptopp-SVN/eax.cpp \
	cryptopp-SVN/ec2n.cpp \
	cryptopp-SVN/eccrypto.cpp \
	cryptopp-SVN/ecp.cpp \
	cryptopp-SVN/elgamal.cpp \
	cryptopp-SVN/emsa2.cpp \
	cryptopp-SVN/eprecomp.cpp \
	cryptopp-SVN/esign.cpp \
	cryptopp-SVN/files.cpp \
	cryptopp-SVN/filters.cpp \
	cryptopp-SVN/fips140.cpp \
	cryptopp-SVN/fipsalgt.cpp \
	cryptopp-SVN/fipstest.cpp \
	cryptopp-SVN/gcm.cpp \
	cryptopp-SVN/gf256.cpp \
	cryptopp-SVN/gf2n.cpp \
	cryptopp-SVN/gf2_32.cpp \
	cryptopp-SVN/gfpcrypt.cpp \
	cryptopp-SVN/gost.cpp \
	cryptopp-SVN/gzip.cpp \
	cryptopp-SVN/hex.cpp \
	cryptopp-SVN/hmac.cpp \
	cryptopp-SVN/hrtimer.cpp \
	cryptopp-SVN/ida.cpp \
	cryptopp-SVN/idea.cpp \
	cryptopp-SVN/integer.cpp \
	cryptopp-SVN/iterhash.cpp \
	cryptopp-SVN/luc.cpp \
	cryptopp-SVN/mars.cpp \
	cryptopp-SVN/marss.cpp \
	cryptopp-SVN/md2.cpp \
	cryptopp-SVN/md4.cpp \
	cryptopp-SVN/md5.cpp \
	cryptopp-SVN/misc.cpp \
	cryptopp-SVN/modes.cpp \
	cryptopp-SVN/mqueue.cpp \
	cryptopp-SVN/mqv.cpp \
	cryptopp-SVN/nbtheory.cpp \
	cryptopp-SVN/network.cpp \
	cryptopp-SVN/oaep.cpp \
	cryptopp-SVN/osrng.cpp \
	cryptopp-SVN/panama.cpp \
	cryptopp-SVN/pch.cpp \
	cryptopp-SVN/pkcspad.cpp \
	cryptopp-SVN/polynomi.cpp \
	cryptopp-SVN/pssr.cpp \
	cryptopp-SVN/pubkey.cpp \
	cryptopp-SVN/queue.cpp \
	cryptopp-SVN/rabin.cpp \
	cryptopp-SVN/randpool.cpp \
	cryptopp-SVN/rc2.cpp \
	cryptopp-SVN/rc5.cpp \
	cryptopp-SVN/rc6.cpp \
	cryptopp-SVN/rdtables.cpp \
	cryptopp-SVN/regtest.cpp \
	cryptopp-SVN/rijndael.cpp \
	cryptopp-SVN/ripemd.cpp \
	cryptopp-SVN/rng.cpp \
	cryptopp-SVN/rsa.cpp \
	cryptopp-SVN/rw.cpp \
	cryptopp-SVN/safer.cpp \
	cryptopp-SVN/salsa.cpp \
	cryptopp-SVN/seal.cpp \
	cryptopp-SVN/seed.cpp \
	cryptopp-SVN/serpent.cpp \
	cryptopp-SVN/sha.cpp \
	cryptopp-SVN/sha3.cpp \
	cryptopp-SVN/shacal2.cpp \
	cryptopp-SVN/shark.cpp \
	cryptopp-SVN/sharkbox.cpp \
	cryptopp-SVN/simple.cpp \
	cryptopp-SVN/skipjack.cpp \
	cryptopp-SVN/socketft.cpp \
	cryptopp-SVN/sosemanuk.cpp \
	cryptopp-SVN/square.cpp \
	cryptopp-SVN/squaretb.cpp \
	cryptopp-SVN/strciphr.cpp \
	cryptopp-SVN/tea.cpp \
	cryptopp-SVN/test.cpp \
	cryptopp-SVN/tftables.cpp \
	cryptopp-SVN/tiger.cpp \
	cryptopp-SVN/tigertab.cpp \
	cryptopp-SVN/trdlocal.cpp \
	cryptopp-SVN/ttmac.cpp \
	cryptopp-SVN/twofish.cpp \
	cryptopp-SVN/validat1.cpp \
	cryptopp-SVN/validat2.cpp \
	cryptopp-SVN/validat3.cpp \
	cryptopp-SVN/vmac.cpp \
	cryptopp-SVN/wait.cpp \
	cryptopp-SVN/wake.cpp \
	cryptopp-SVN/whrlpool.cpp \
	cryptopp-SVN/winpipes.cpp \
	cryptopp-SVN/xtr.cpp \
	cryptopp-SVN/xtrcrypt.cpp \
	cryptopp-SVN/zdeflate.cpp \
	cryptopp-SVN/zinflate.cpp \
	cryptopp-SVN/zlib.cpp
		
include $(BUILD_SHARED_LIBRARY)