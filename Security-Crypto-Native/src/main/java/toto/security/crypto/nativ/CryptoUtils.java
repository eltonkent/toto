package toto.security.crypto.nativ;

import com.mi.toto.ToTo;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;

import static toto.io.codec.Base64.encode;
import static toto.math.MathUtils.log2;

/**
 * Cryptographic utils implemented in c++
 * Created by esk on 12/3/15.
 */
public class CryptoUtils {
    static {
        ToTo.loadLib("Security_Crypto");
    }


    /**
     * Native C implementation of the <a
     * href="http://www.tarsnap.com/scrypt/scrypt.pdf"/>scrypt KDF</a> using the
     * code from <a
     * href="http://www.tarsnap.com/scrypt.html">http://www.tarsnap.
     * com/scrypt.html<a>.
     *
     * @param passwd Password.
     * @param salt   Salt.
     * @param N      CPU cost parameter.
     * @param r      Memory cost parameter. Max:255
     * @param p      Parallelization parameter. Max:255
     * @param dkLen  Intended length of the derived key.
     * @return The derived key.
     */
    public static byte[] scryptNative(final byte[] passwd, final byte[] salt,
                                      final int N, final int r, final int p, final int dkLen) {
        return scryptNative(passwd, salt, N, r, p, dkLen);
    }

    /**
     * Hash the supplied plaintext password and generate output in the format.
     *
     * @param passwd Password.
     * @param N      CPU cost parameter. Always >= 2
     * @param r      Memory cost parameter. Max:255
     * @param p      Parallelization parameter. Max 255
     * @return The hashed password.
     */
    public static String scryptNative(final String passwd, final int N,
                                      final int r, final int p) {
        try {
            final byte[] salt = new byte[16];
            SecureRandom.getInstance("SHA1PRNG").nextBytes(salt);

            final byte[] derived = scryptNative(passwd.getBytes("UTF-8"), salt,
                    N, r, p, 32);

            final String params = Long
                    .toString(log2(N) << 16L | r << 8 | p, 16);

            final StringBuilder sb = new StringBuilder(
                    (salt.length + derived.length) * 2);
            sb.append("$s0$").append(params).append('$');
            sb.append(encode(salt)).append('$');
            sb.append(encode(derived));

            return sb.toString();
        } catch (final UnsupportedEncodingException e) {
            ToTo.logException(e);
            throw new IllegalStateException("JVM doesn't support UTF-8?");
        } catch (final GeneralSecurityException e) {
            ToTo.logException(e);
            throw new IllegalStateException(
                    "JVM doesn't support SHA1PRNG or HMAC_SHA256?");
        }
    }

}
