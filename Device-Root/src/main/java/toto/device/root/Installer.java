package toto.device.root;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.mi.toto.ToTo;
import toto.io.IOUtils;
import toto.log.Logger;

import android.content.Context;

class Installer {

	static final String BOGUS_FILE_NAME = "bogus";

	Context context;
	String filesPath;

	public Installer(final Context context) throws IOException {

		this.context = context;
		this.filesPath = context.getFilesDir().getCanonicalPath();
	}

	/**
	 * This method can be used to unpack a binary from the raw resources folder
	 * and store it in /data/data/app.package/files/ This is typically useful if
	 * you provide your own C- or C++-based binary. This binary can then be
	 * executed using sendShell() and its full path.
	 * 
	 * @param sourceId
	 *            resource id; typically <code>R.raw.id</code>
	 * @param destName
	 *            destination file name; appended to
	 *            /data/data/app.package/files/
	 * @param mode
	 *            chmod value for this file
	 * @return a <code>boolean</code> which indicates whether or not we were
	 *         able to create the new file.
	 */
	protected boolean installBinary(final int sourceId, final String destName,
			final String mode) {
		final File mf = new File(filesPath + File.separator + destName);
		if (!mf.exists()
				|| !getFileSignature(mf).equals(
						getStreamSignature(context.getResources()
								.openRawResource(sourceId)))) {
            Logger.getDefault().d("Installing a new version of binary: " + destName);
			// First, does our files/ directory even exist?
			// We cannot wait for android to lazily create it as we will soon
			// need it.
			try {
				final FileInputStream fis = context
						.openFileInput(BOGUS_FILE_NAME);
				fis.close();
			} catch (final FileNotFoundException e) {
				FileOutputStream fos = null;
				try {
					fos = context.openFileOutput("bogus", Context.MODE_PRIVATE);
					fos.write("justcreatedfilesdirectory".getBytes());
				} catch (final Exception ex) {
					ToTo.logException(ex);
					return false;
				} finally {
					IOUtils.closeQuietly(fos);
					context.deleteFile(BOGUS_FILE_NAME);
				}
			} catch (final IOException ex) {
				ToTo.logException(ex);
				return false;
			}

			// Only now can we start creating our actual file
			final InputStream iss = context.getResources().openRawResource(
					sourceId);
			final ReadableByteChannel rfc = Channels.newChannel(iss);
			FileOutputStream oss = null;
			try {
				oss = new FileOutputStream(mf);
				final FileChannel ofc = oss.getChannel();
				long pos = 0;
				try {
					final long size = iss.available();
					while ((pos += ofc.transferFrom(rfc, pos, size - pos)) < size)
						;
				} catch (final IOException ex) {
					ToTo.logException(ex);
					return false;
				}
			} catch (final FileNotFoundException ex) {
				ToTo.logException(ex);
				return false;
			} finally {
				if (oss != null) {
					try {
						oss.flush();
						oss.getFD().sync();
						oss.close();
					} catch (final Exception e) {
					}
				}
			}
			try {
				iss.close();
			} catch (final IOException ex) {
				ToTo.logException(ex);
				return false;
			}

			try {
				final CommandCapture command = new CommandCapture(0, false,
						"chmod " + mode + " " + filesPath + File.separator
								+ destName);
				Shell.startRootShell().add(command);
				commandWait(command);

			} catch (final Exception e) {
			}
		}
		return true;
	}

	protected boolean isBinaryInstalled(final String destName) {
		boolean installed = false;
		final File mf = new File(filesPath + File.separator + destName);
		if (mf.exists()) {
			installed = true;
			// TODO: pass mode as argument and check it matches
		}
		return installed;
	}

	protected String getFileSignature(final File f) {
		String signature = "";
		try {
			signature = getStreamSignature(new FileInputStream(f));
		} catch (final FileNotFoundException ex) {
			ToTo.logException(ex);
		}
		return signature;
	}

	/*
	 * Note: this method will close any string passed to it
	 */
	protected String getStreamSignature(final InputStream is) {
		String signature = "";
		try {
			final MessageDigest md = MessageDigest.getInstance("MD5");
			final DigestInputStream dis = new DigestInputStream(is, md);
			final byte[] buffer = new byte[4096];
			while (-1 != dis.read(buffer))
				;
			final byte[] digest = md.digest();
			final StringBuffer sb = new StringBuffer();

			for (int i = 0; i < digest.length; i++)
				sb.append(Integer.toHexString(digest[i] & 0xFF));

			signature = sb.toString();
		} catch (final IOException ex) {
			ToTo.logException(ex);
		} catch (final NoSuchAlgorithmException ex) {
			ToTo.logException(ex);
		} finally {
			try {
				is.close();
			} catch (final IOException e) {
			}
		}
		return signature;
	}

	private void commandWait(final Command cmd) {
		synchronized (cmd) {
			try {
				if (!cmd.isFinished()) {
					cmd.wait(2000);
				}
			} catch (final InterruptedException ex) {
				ToTo.logException(ex);
			}
		}
	}
}