package toto.device.root;

import java.io.IOException;

import com.mi.toto.ToTo;
import android.content.Context;

class Runner extends Thread {

	private static final String LOG_TAG = "RootTools::Runner";

	Context context;
	String binaryName;
	String parameter;

	Runner(final Context context, final String binaryName,
			final String parameter) {
		this.context = context;
		this.binaryName = binaryName;
		this.parameter = parameter;
	}

	public void run() {
		String privateFilesPath = null;
		try {
			privateFilesPath = context.getFilesDir().getCanonicalPath();
		} catch (final IOException e) {
			ToTo.logException(e);
		}
		if (privateFilesPath != null) {
			try {
				final CommandCapture command = new CommandCapture(0, false,
						privateFilesPath + "/" + binaryName + " " + parameter);
				Shell.startRootShell().add(command);
				commandWait(command);

			} catch (final Exception e) {
				ToTo.logException(e);
			}
		}
	}

	private void commandWait(final Command cmd) {
		synchronized (cmd) {
			try {
				if (!cmd.isFinished()) {
					cmd.wait(2000);
				}
			} catch (final InterruptedException e) {
				ToTo.logException(e);
			}
		}
	}

}