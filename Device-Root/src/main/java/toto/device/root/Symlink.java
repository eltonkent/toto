package toto.device.root;

import java.io.File;

/**
 * Created by ekent4 on 1/28/14.
 */
class Symlink {
	protected final File file;
	protected final File symlinkPath;

	Symlink(File file, File path) {
		this.file = file;
		symlinkPath = path;
	}

	File getFile() {
		return this.file;
	}

	File getSymlinkPath() {
		return symlinkPath;
	}
}
