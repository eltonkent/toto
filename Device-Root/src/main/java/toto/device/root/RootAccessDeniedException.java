package toto.device.root;

public class RootAccessDeniedException extends Exception {

	public RootAccessDeniedException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 561449027640594195L;

}
