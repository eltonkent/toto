package toto.device.root;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.mi.toto.ToTo;

import toto.log.Logger;

class Remounter {

	// -------------
	// # Remounter #
	// -------------

	/**
	 * This will take a path, which can contain the file name as well, and
	 * attempt to remount the underlying partition.
	 * <p/>
	 * For example, passing in the following string:
	 * "/system/bin/some/directory/that/really/would/never/exist" will result in
	 * /system ultimately being remounted. However, keep in mind that the longer
	 * the path you supply, the more work this has to do, and the slower it will
	 * run.
	 * 
	 * @param file
	 *            file path
	 * @param mountType
	 *            mount type: pass in RO (Read only) or RW (Read Write)
	 * @return a <code>boolean</code> which indicates whether or not the
	 *         partition has been remounted as specified.
	 */

	public boolean remount(String file, final String mountType) {

		// if the path has a trailing slash get rid of it.
		if (file.endsWith("/") && !file.equals("/")) {
			file = file.substring(0, file.lastIndexOf("/"));
		}
		// Make sure that what we are trying to remount is in the mount list.
		boolean foundMount = false;

		while (!foundMount) {
			try {
				for (final Mount mount : RootTools.getMounts()) {
					Logger.getDefault().d(mount.getMountPoint().toString());

					if (file.equals(mount.getMountPoint().toString())) {
						foundMount = true;
						break;
					}
				}
			} catch (final Exception e) {
				if (RootTools.debugMode) {
					e.printStackTrace();
				}
				return false;
			}
			if (!foundMount) {
				try {
					file = (new File(file).getParent());
				} catch (final Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		}

		Mount mountPoint = findMountPointRecursive(file);

		if (mountPoint != null) {

			Logger.getDefault().d("Remounting "
					+ mountPoint.getMountPoint().getAbsolutePath() + " as "
					+ mountType.toLowerCase());
			final boolean isMountMode = mountPoint.getFlags().contains(
					mountType.toLowerCase());

			if (!isMountMode) {
				// grab an instance of the internal class
				try {
					final CommandCapture command = new CommandCapture(0, true,
							"busybox mount -o remount,"
									+ mountType.toLowerCase()
									+ " "
									+ mountPoint.getDevice().getAbsolutePath()
									+ " "
									+ mountPoint.getMountPoint()
											.getAbsolutePath(),
							"toolbox mount -o remount,"
									+ mountType.toLowerCase()
									+ " "
									+ mountPoint.getDevice().getAbsolutePath()
									+ " "
									+ mountPoint.getMountPoint()
											.getAbsolutePath(),
							"mount -o remount,"
									+ mountType.toLowerCase()
									+ " "
									+ mountPoint.getDevice().getAbsolutePath()
									+ " "
									+ mountPoint.getMountPoint()
											.getAbsolutePath(),
							"/system/bin/toolbox mount -o remount,"
									+ mountType.toLowerCase()
									+ " "
									+ mountPoint.getDevice().getAbsolutePath()
									+ " "
									+ mountPoint.getMountPoint()
											.getAbsolutePath());
					Shell.startRootShell().add(command);
					commandWait(command);

				} catch (final Exception e) {
				}

				mountPoint = findMountPointRecursive(file);
			}

			if (mountPoint != null) {
				Logger.getDefault().d(mountPoint.getFlags() + " AND "
						+ mountType.toLowerCase());
				if (mountPoint.getFlags().contains(mountType.toLowerCase())) {
					Logger.getDefault().d(mountPoint.getFlags().toString());
					return true;
				} else {
					Logger.getDefault().d(mountPoint.getFlags().toString());
					return false;
				}
			} else {
				Logger.getDefault().d("mount is null, file was: " + file + " mountType was: "
						+ mountType);
			}
		} else {
			Logger.getDefault().d("mount is null, file was: " + file + " mountType was: "
					+ mountType);
		}

		return false;
	}

	private Mount findMountPointRecursive(final String file) {
		try {
			final ArrayList<Mount> mounts = RootTools.getMounts();

			for (final File path = new File(file); path != null;) {
				for (final Mount mount : mounts) {
					if (mount.getMountPoint().equals(path)) {
						return mount;
					}
				}
			}

			return null;

		} catch (final IOException e) {
			ToTo.logException(e);
		} catch (final Exception e) {
			ToTo.logException(e);
		}

		return null;
	}

	private void commandWait(final Command cmd) {
		synchronized (cmd) {
			try {
				if (!cmd.isFinished()) {
					cmd.wait(2000);
				}
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}