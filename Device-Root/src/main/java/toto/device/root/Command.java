package toto.device.root;

import java.io.IOException;

import com.mi.toto.ToTo;
import toto.log.Log;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * Created by ekent4 on 1/28/14.
 */
abstract class Command {
	ExecutionMonitor executionMonitor = null;
	Handler mHandler = null;
	boolean executing = false;

	String[] command = {};
	boolean javaCommand = false;
	Context context = null;
	boolean finished = false;
	boolean terminated = false;
	boolean handlerEnabled = true;
	int exitCode = -1;
	int id = 0;
	int timeout = RootTools.default_Command_Timeout;

	abstract void commandOutput(int id, String line);

	abstract void commandTerminated(int id, String reason);

	abstract void commandCompleted(int id, int exitCode);

	/**
	 * Constructor for executing a normal shell command
	 * 
	 * @param id
	 *            the id of the command being executed
	 * @param command
	 *            the command, or commands, to be executed.
	 */
	Command(final int id, final String... command) {
		this.command = command;
		this.id = id;

		createHandler(RootTools.handlerEnabled);
	}

	/**
	 * Constructor for executing a normal shell command
	 * 
	 * @param id
	 *            the id of the command being executed
	 * @param handlerEnabled
	 *            when true the handler will be used to call the callback
	 *            methods if possible.
	 * @param command
	 *            the command, or commands, to be executed.
	 */
	Command(final int id, final boolean handlerEnabled, final String... command) {
		this.command = command;
		this.id = id;

		createHandler(handlerEnabled);
	}

	/**
	 * Constructor for executing a normal shell command
	 * 
	 * @param id
	 *            the id of the command being executed
	 * @param timeout
	 *            the time allowed before the shell will give up executing the
	 *            command and throw a TimeoutException.
	 * @param command
	 *            the command, or commands, to be executed.
	 */
	Command(final int id, final int timeout, final String... command) {
		this.command = command;
		this.id = id;
		this.timeout = timeout;

		createHandler(RootTools.handlerEnabled);
	}

	/**
	 * Constructor for executing Java commands rather than binaries
	 * 
	 * @param javaCommand
	 *            when True, it is a java command.
	 * @param context
	 *            needed to execute java command.
	 */
	public Command(final int id, final boolean javaCommand,
			final Context context, final String... command) {
		this(id, command);
		this.javaCommand = javaCommand;
		this.context = context;
	}

	/**
	 * Constructor for executing Java commands rather than binaries
	 * 
	 * @param javaCommand
	 *            when True, it is a java command.
	 * @param context
	 *            needed to execute java command.
	 */
	Command(final int id, final boolean handlerEnabled,
			final boolean javaCommand, final Context context,
			final String... command) {
		this(id, handlerEnabled, command);
		this.javaCommand = javaCommand;
		this.context = context;
	}

	/**
	 * Constructor for executing Java commands rather than binaries
	 * 
	 * @param javaCommand
	 *            when True, it is a java command.
	 * @param context
	 *            needed to execute java command.
	 */
	Command(final int id, final int timeout, final boolean javaCommand,
			final Context context, final String... command) {
		this(id, timeout, command);
		this.javaCommand = javaCommand;
		this.context = context;
	}

	protected void finishCommand() {
		executing = false;
		finished = true;
		this.notifyAll();
	}

	protected void commandFinished() {
		if (!terminated) {
			synchronized (this) {
				if (mHandler != null && handlerEnabled) {
					final Message msg = mHandler.obtainMessage();
					final Bundle bundle = new Bundle();
					bundle.putInt(CommandHandler.ACTION,
							CommandHandler.COMMAND_COMPLETED);
					msg.setData(bundle);
					mHandler.sendMessage(msg);
				} else {
					commandCompleted(id, exitCode);
				}

				Log.d("Command " + id + " finished.");
				finishCommand();
			}
		}
	}

	private void createHandler(final boolean handlerEnabled) {

		this.handlerEnabled = handlerEnabled;

		if (Looper.myLooper() != null && handlerEnabled) {
			Log.d("CommandHandler created");
			mHandler = new CommandHandler();
		} else {
			Log.d("CommandHandler not created");
		}
	}

	String getCommand() {
		final StringBuilder sb = new StringBuilder();

		if (javaCommand) {
			final String filePath = context.getFilesDir().getPath();
			for (int i = 0; i < command.length; i++) {
				/*
				 * TODO Make withFramework optional for applications that do not
				 * require access to the fw. -CFR
				 */
				sb.append("dalvikvm -cp " + filePath + "/anbuild.dex"
						+ " com.android.internal.util.WithFramework"
						+ " com.stericson.RootTools.containers.RootClass "
						+ command[i]);
				sb.append('\n');
			}
		} else {
			for (int i = 0; i < command.length; i++) {
				sb.append(command[i]);
				sb.append('\n');
			}
		}
		return sb.toString();
	}

	boolean isExecuting() {
		return executing;
	}

	boolean isHandlerEnabled() {
		return handlerEnabled;
	}

	boolean isFinished() {
		return finished;
	}

	int getExitCode() {
		return this.exitCode;
	}

	protected void setExitCode(final int code) {
		synchronized (this) {
			exitCode = code;
		}
	}

	protected void startExecution() {
		executionMonitor = new ExecutionMonitor();
		executionMonitor.setPriority(Thread.MIN_PRIORITY);
		executionMonitor.start();
		executing = true;
	}

	void terminate(final String reason) {
		try {
			Shell.closeAll();
			Log.d("Terminating all shells.");
			terminated(reason);
		} catch (final IOException e) {
			ToTo.logException(e);
		}
	}

	protected void terminated(final String reason) {
		synchronized (Command.this) {

			if (mHandler != null && handlerEnabled) {
				final Message msg = mHandler.obtainMessage();
				final Bundle bundle = new Bundle();
				bundle.putInt(CommandHandler.ACTION,
						CommandHandler.COMMAND_TERMINATED);
				bundle.putString(CommandHandler.TEXT, reason);
				msg.setData(bundle);
				mHandler.sendMessage(msg);
			} else {
				commandTerminated(id, reason);
			}

			Log.d("Command "
					+ id
					+ " did not finish because it was terminated. Termination reason: "
					+ reason);
			setExitCode(-1);
			terminated = true;
			finishCommand();
		}
	}

	protected void output(final int id, final String line) {
		if (mHandler != null && handlerEnabled) {
			final Message msg = mHandler.obtainMessage();
			final Bundle bundle = new Bundle();
			bundle.putInt(CommandHandler.ACTION, CommandHandler.COMMAND_OUTPUT);
			bundle.putString(CommandHandler.TEXT, line);
			msg.setData(bundle);
			mHandler.sendMessage(msg);
		} else {
			commandOutput(id, line);
		}
	}

	private class ExecutionMonitor extends Thread {
		public void run() {
			while (!finished) {

				synchronized (Command.this) {
					try {
						Command.this.wait(timeout);
					} catch (final InterruptedException e) {
						ToTo.logException(e);
					}
				}

				if (!finished) {
					Log.d("Timeout Exception has occurred.");
					terminate("Timeout Exception");
				}
			}
		}
	}

	private class CommandHandler extends Handler {
		static final public String ACTION = "action";
		static final public String TEXT = "text";

		static final public int COMMAND_OUTPUT = 0x01;
		static final public int COMMAND_COMPLETED = 0x02;
		static final public int COMMAND_TERMINATED = 0x03;

		public void handleMessage(final Message msg) {
			final int action = msg.getData().getInt(ACTION);
			final String text = msg.getData().getString(TEXT);

			switch (action) {
			case COMMAND_OUTPUT:
				commandOutput(id, text);
				break;
			case COMMAND_COMPLETED:
				commandCompleted(id, exitCode);
				break;
			case COMMAND_TERMINATED:
				commandTerminated(id, text);
				break;
			}
		}
	}
}
