package toto.device.root;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

class Mount {
	final File mDevice;
	final File mMountPoint;
	final String mType;
	final Set<String> mFlags;

	public Mount(File device, File path, String type, String flagsStr) {
		mDevice = device;
		mMountPoint = path;
		mType = type;
		mFlags = new LinkedHashSet<String>(Arrays.asList(flagsStr.split(",")));
	}

	File getDevice() {
		return mDevice;
	}

	File getMountPoint() {
		return mMountPoint;
	}

	String getType() {
		return mType;
	}

	Set<String> getFlags() {
		return mFlags;
	}

	@Override
	public String toString() {
		return String.format("%s on %s type %s %s", mDevice, mMountPoint,
				mType, mFlags);
	}
}