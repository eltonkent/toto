package toto.device.root;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;

import com.mi.toto.ToTo;
import android.content.Context;

import toto.log.Logger;

/**
 * Tools for rooted devices
 * 
 * @author ekent4
 */
public class RootTools {

	/**
	 * Setting this to false will disable the handler that is used by default
	 * for the 3 callback methods for Command.
	 * 
	 * By disabling this all callbacks will be called from a thread other than
	 * the main UI thread.
	 */
	public static boolean handlerEnabled = true;

	/**
	 * Setting this will change the default command timeout.
	 * 
	 * The default is 20000ms
	 */
	public static int default_Command_Timeout = 20000;

	public static boolean debugMode = false;

	public static List<String> lastFoundBinaryPaths = new ArrayList<String>();
	public static String utilPath;

	/**
	 * @param binaryName
	 *            String that represent the binary to find.
	 * @return <code>true</code> if the specified binary was found. Also, the
	 *         path the binary was found at can be retrieved via the variable
	 *         lastFoundBinaryPath, if the binary was found in more than one
	 *         location this will contain all of these locations.
	 */
	public static boolean findBinary(final String binaryName) {
		boolean found = false;

		final List<String> list = new ArrayList<String>();
		final String[] places = { "/sbin/", "/system/bin/", "/system/xbin/",
				"/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/",
				"/system/bin/failsafe/", "/data/local/" };
        Logger.getDefault().d("Checking for " + binaryName);

		// Try to use stat first
		try {
			for (final String path : places) {
				final CommandCapture cc = new CommandCapture(0, false, "stat "
						+ path + binaryName) {
					@Override
					public void commandOutput(final int id, final String line) {
						if (line.contains("File: ")
								&& line.contains(binaryName)) {
							list.add(path);

							Logger.getDefault().d(binaryName + " was found here: " + path);
						}

						Logger.getDefault().d(line);
					}
				};

				getShell(false).add(cc);
				commandWait(cc);

			}

			found = !list.isEmpty();
		} catch (final Exception e) {
			ToTo.logException(e);
			Logger.getDefault().d(binaryName
					+ " was not found, more information MAY be available with Debugging on.");
		}

		if (!found) {
			Logger.getDefault().d("Trying second method");

			for (final String where : places) {
				if (exists(where + binaryName)) {
					Logger.getDefault().d(binaryName + " was found here: " + where);
					list.add(where);
					found = true;
				} else {
					Logger.getDefault().d(binaryName + " was NOT found here: " + where);
				}
			}
		}

		if (!found) {
			Logger.getDefault().d("Trying third method");

			try {
				final List<String> paths = getPath();

				if (paths != null) {
					for (final String path : paths) {
						if (exists(path + "/" + binaryName)) {
							Logger.getDefault().d(binaryName + " was found here: " + path);
							list.add(path);
							found = true;
						} else {
							Logger.getDefault().d(binaryName + " was NOT found here: " + path);
						}
					}
				}
			} catch (final Exception e) {
				ToTo.logException(e);
				Logger.getDefault().d(binaryName
						+ " was not found, more information MAY be available with Debugging on.");
			}
		}

		Collections.reverse(list);

		lastFoundBinaryPaths.addAll(list);

		return found;
	}

	private static void commandWait(final Command cmd) throws Exception {

		while (!cmd.isFinished()) {

			Logger.getDefault().d(Shell.getOpenShell().getCommandQueuePositionString(cmd));

			synchronized (cmd) {
				try {
					if (!cmd.isFinished()) {
						cmd.wait(2000);
					}
				} catch (final InterruptedException e) {
					e.printStackTrace();
				}
			}

			if (!cmd.isExecuting() && !cmd.isFinished()) {
				if (!Shell.isExecuting && !Shell.isReading) {
					Logger.getDefault().e("Waiting for a command to be executed in a shell that is not executing and not reading! \n\n Command: "
							+ cmd.getCommand());
					final Exception e = new Exception();
					e.setStackTrace(Thread.currentThread().getStackTrace());
					e.printStackTrace();
				} else if (Shell.isExecuting && !Shell.isReading) {
					Logger.getDefault().e("Waiting for a command to be executed in a shell that is executing but not reading! \n\n Command: "
							+ cmd.getCommand());
					final Exception e = new Exception();
					e.setStackTrace(Thread.currentThread().getStackTrace());
					e.printStackTrace();
				} else {
					Logger.getDefault().e("Waiting for a command to be executed in a shell that is not reading! \n\n Command: "
							+ cmd.getCommand());
					final Exception e = new Exception();
					e.setStackTrace(Thread.currentThread().getStackTrace());
					e.printStackTrace();
				}
			}

		}
	}

	/**
	 * Use this to check whether or not a file exists on the filesystem.
	 * 
	 * @param file
	 *            String that represent the file, including the full path to the
	 *            file and its name.
	 * @return a boolean that will indicate whether or not the file exists.
	 */
	public static boolean exists(final String file) {
		final List<String> result = new ArrayList<String>();

		final CommandCapture command = new CommandCapture(0, false, "ls "
				+ file) {
			@Override
			public void output(final int arg0, final String arg1) {
				Logger.getDefault().d(arg1);
				result.add(arg1);
			}
		};

		try {
			// Try not to open a new shell if one is open.
			if (!Shell.isAnyShellOpen()) {
				Shell.startShell().add(command);
				commandWait(command);

			} else {
				Shell.getOpenShell().add(command);
				commandWait(command);
			}
		} catch (final Exception e) {
			return false;
		}

		for (final String line : result) {
			if (line.trim().equals(file)) {
				return true;
			}
		}

		try {
			closeShell(false);
		} catch (final Exception e) {
		}

		result.clear();
		try {
			Shell.startRootShell().add(command);
			commandWait(command);

		} catch (final Exception e) {
			return false;
		}

		// Avoid concurrent modification...
		final List<String> final_result = new ArrayList<String>();
		final_result.addAll(result);

		for (final String line : final_result) {
			if (line.trim().equals(file)) {
				return true;
			}
		}

		return false;

	}

	/**
	 * This will close either the root shell or the standard shell depending on
	 * what you specify.
	 * 
	 * @param root
	 *            a <code>boolean</code> to specify whether to close the root
	 *            shell or the standard shell.
	 * @throws IOException
	 */
	public static void closeShell(final boolean root) throws IOException {
		if (root)
			Shell.closeRootShell();
		else
			Shell.closeShell();
	}

	/**
	 * This will open or return, if one is already open, a shell, you are
	 * responsible for managing the shell, reading the output and for closing
	 * the shell when you are done using it.
	 * 
	 * @param retry
	 *            a <code>int</code> to indicate how many times the ROOT shell
	 *            should try to open with root priviliges...
	 * @throws TimeoutException
	 * @throws RootAccessDeniedException
	 * @param root
	 *            a <code>boolean</code> to Indicate whether or not you want to
	 *            open a root shell or a standard shell
	 * @param timeout
	 *            an <code>int</code> to Indicate the length of time to wait
	 *            before giving up on opening a shell.
	 * @throws IOException
	 */
	public static Shell getShell(final boolean root, final int timeout,
			final int retry) throws IOException, TimeoutException,
			RootAccessDeniedException {
		if (root)
			return Shell.startRootShell(timeout);
		else
			return Shell.startShell(timeout);
	}

	/**
	 * This will open or return, if one is already open, a shell, you are
	 * responsible for managing the shell, reading the output and for closing
	 * the shell when you are done using it.
	 * 
	 * @throws TimeoutException
	 * @throws RootAccessDeniedException
	 * @param root
	 *            a <code>boolean</code> to Indicate whether or not you want to
	 *            open a root shell or a standard shell
	 * @param timeout
	 *            an <code>int</code> to Indicate the length of time to wait
	 *            before giving up on opening a shell.
	 * @throws IOException
	 */
	public static Shell getShell(final boolean root, final int timeout)
			throws IOException, TimeoutException, RootAccessDeniedException {
		return getShell(root, timeout, 3);
	}

	/**
	 * This will open or return, if one is already open, a shell, you are
	 * responsible for managing the shell, reading the output and for closing
	 * the shell when you are done using it.
	 * 
	 * @throws TimeoutException
	 * @throws RootAccessDeniedException
	 * @param root
	 *            a <code>boolean</code> to Indicate whether or not you want to
	 *            open a root shell or a standard shell
	 * @throws IOException
	 */
	public static Shell getShell(final boolean root) throws IOException,
			TimeoutException, RootAccessDeniedException {
		return RootTools.getShell(root, 25000);
	}

	/**
	 * This method can be used to unpack a binary from the raw resources folder
	 * and store it in /data/data/app.package/files/ This is typically useful if
	 * you provide your own C- or C++-based binary. This binary can then be
	 * executed using sendShell() and its full path.
	 * 
	 * @param context
	 *            the current activity's <code>Context</code>
	 * @param sourceId
	 *            resource id; typically <code>R.raw.id</code>
	 * @param binaryName
	 *            destination file name; appended to
	 *            /data/data/app.package/files/
	 * @return a <code>boolean</code> which indicates whether or not we were
	 *         able to create the new file.
	 */
	public static boolean installBinary(final Context context,
			final int sourceId, final String binaryName) {
		return installBinary(context, sourceId, binaryName, "700");
	}

	/**
	 * This method can be used to unpack a binary from the raw resources folder
	 * and store it in /data/data/app.package/files/ This is typically useful if
	 * you provide your own C- or C++-based binary. This binary can then be
	 * executed using sendShell() and its full path.
	 * 
	 * @param context
	 *            the current activity's <code>Context</code>
	 * @param sourceId
	 *            resource id; typically <code>R.raw.id</code>
	 * @param destName
	 *            destination file name; appended to
	 *            /data/data/app.package/files/
	 * @param mode
	 *            chmod value for this file
	 * @return a <code>boolean</code> which indicates whether or not we were
	 *         able to create the new file.
	 */
	public static boolean installBinary(final Context context,
			final int sourceId, final String destName, final String mode) {
		Installer installer;

		try {
			installer = new Installer(context);
		} catch (final IOException ex) {
			if (RootTools.debugMode) {
				ex.printStackTrace();
			}
			return false;
		}

		return (installer.installBinary(sourceId, destName, mode));
	}

	/**
	 * This will return the environment variable PATH
	 * 
	 * @return <code>List<String></code> A List of Strings representing the
	 *         environment variable $PATH
	 */
	public static List<String> getPath() {
		return Arrays.asList(System.getenv("PATH").split(":"));
	}

	/**
	 * This will tell you how the specified mount is mounted. rw, ro, etc...
	 * <p/>
	 * 
	 * @param path
	 *            mount you want to check
	 * @return <code>String</code> What the mount is mounted as.
	 * @throws Exception
	 *             if we cannot determine how the mount is mounted.
	 */
	public static String getMountedAs(final String path) throws Exception {
		InternalVariables.mounts = getMounts();
		String mp;
		if (InternalVariables.mounts != null) {
			for (final Mount mount : InternalVariables.mounts) {

				mp = mount.getMountPoint().getAbsolutePath();

				if (mp.equals("/")) {
					if (path.equals("/")) {
						return (String) mount.getFlags().toArray()[0];
					} else {
						continue;
					}
				}

				if (path.equals(mp) || path.startsWith(mp + "/")) {
					Logger.getDefault().d((String) mount.getFlags().toArray()[0]);
					return (String) mount.getFlags().toArray()[0];
				}
			}

			throw new Exception();
		} else {
			throw new Exception();
		}
	}

	/**
	 * This will return an ArrayList of the class Mount. The class mount
	 * contains the following property's: device mountPoint type flags
	 * <p/>
	 * These will provide you with any information you need to work with the
	 * mount points.
	 * 
	 * @return <code>ArrayList<Mount></code> an ArrayList of the class Mount.
	 * @throws Exception
	 *             if we cannot return the mount points.
	 */
	static ArrayList<Mount> getMounts() throws Exception {

		final Shell shell = RootTools.getShell(true);

		final CommandCapture cmd = new CommandCapture(0, false,
				"cat /proc/mounts > /data/local/RootToolsMounts",
				"chmod 0777 /data/local/RootToolsMounts");
		shell.add(cmd);
		commandWait(cmd);

		LineNumberReader lnr = null;
		FileReader fr = null;

		try {
			fr = new FileReader("/data/local/RootToolsMounts");
			lnr = new LineNumberReader(fr);
			String line;
			final ArrayList<Mount> mounts = new ArrayList<Mount>();
			while ((line = lnr.readLine()) != null) {

				Logger.getDefault().d(line);

				final String[] fields = line.split(" ");
				mounts.add(new Mount(new File(fields[0]), // device
						new File(fields[1]), // mountPoint
						fields[2], // fstype
						fields[3] // flags
				));
			}
			InternalVariables.mounts = mounts;

			if (InternalVariables.mounts != null) {
				return InternalVariables.mounts;
			} else {
				throw new Exception();
			}
		} finally {
			try {
				fr.close();
				fr = null;
			} catch (final Exception e) {
			}

			try {
				lnr.close();
				lnr = null;
			} catch (final Exception e) {
			}
		}
	}

	/**
	 * Deletes a file or directory
	 * 
	 * @param target
	 *            example: /data/data/org.adaway/files/hosts
	 * @param remountAsRw
	 *            remounts the destination as read/write before writing to it
	 * @return true if it was successfully deleted
	 */
	public boolean deleteFileOrDirectory(final String target,
			final boolean remountAsRw) {
		boolean result = true;

		try {
			// mount destination as rw before writing to it
			if (remountAsRw) {
				remount(target, "RW");
			}

			if (hasUtil("rm", "toolbox")) {
				Logger.getDefault().d("rm command is available!");

				final CommandCapture command = new CommandCapture(0, false,
						"rm -r " + target);
				Shell.startRootShell().add(command);
				commandWait(command);

				if (command.getExitCode() != 0) {
					Logger.getDefault().d("target not exist or unable to delete file");
					result = false;
				}
			} else {
				if (checkUtil("busybox") && hasUtil("rm", "busybox")) {
					Logger.getDefault().d("busybox cp command is available!");

					final CommandCapture command = new CommandCapture(0, false,
							"busybox rm -rf " + target);
					Shell.startRootShell().add(command);
					commandWait(command);

					if (command.getExitCode() != 0) {
						Logger.getDefault().d("target not exist or unable to delete file");
						result = false;
					}
				}
			}

			// mount destination back to ro
			if (remountAsRw) {
				remount(target, "RO");
			}
		} catch (final Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	/**
	 * This will check a given binary, determine if it exists and determine that
	 * it has either the permissions 755, 775, or 777.
	 * 
	 * @param util
	 *            Name of the utility to check.
	 * @return boolean to indicate whether the binary is installed and has
	 *         appropriate permissions.
	 */
	public static boolean checkUtil(final String util) {
		if (RootTools.findBinary(util)) {

			final List<String> binaryPaths = new ArrayList<String>();
			binaryPaths.addAll(RootTools.lastFoundBinaryPaths);

			for (final String path : binaryPaths) {
				final Permissions permissions = getFilePermissionsSymlinks(path
						+ "/" + util);

				if (permissions != null) {
					String permission;

					if (Integer.toString(permissions.getPermissions()).length() > 3)
						permission = Integer.toString(
								permissions.getPermissions()).substring(1);
					else
						permission = Integer.toString(permissions
								.getPermissions());

					if (permission.equals("755") || permission.equals("777")
							|| permission.equals("775")) {
						RootTools.utilPath = path + "/" + util;
						return true;
					}
				}
			}
		}

		return false;

	}

	static final int FPS = 1;

	/**
	 * @param file
	 *            String that represent the file, including the full path to the
	 *            file and its name.
	 * @return An instance of the class permissions from which you can get the
	 *         permissions of the file or if the file could not be found or
	 *         permissions couldn't be determined then permissions will be null.
	 */
	static Permissions getFilePermissionsSymlinks(final String file) {
		Logger.getDefault().d("Checking permissions for " + file);
		if (RootTools.exists(file)) {
			Logger.getDefault().d(file + " was found.");
			try {

				final CommandCapture command = new CommandCapture(FPS, false,
						"ls -l " + file, "busybox ls -l " + file,
						"/system/bin/failsafe/toolbox ls -l " + file,
						"toolbox ls -l " + file) {
					@Override
					public void output(final int id, final String line) {
						if (id == FPS) {
							String symlink_final = "";

							final String[] lineArray = line.split(" ");
							if (lineArray[0].length() != 10) {
								return;
							}

							Logger.getDefault().d("Line " + line);

							try {
								final String[] symlink = line.split(" ");
								if (symlink[symlink.length - 2].equals("->")) {
									Logger.getDefault().d("Symlink found.");
									symlink_final = symlink[symlink.length - 1];
								}
							} catch (final Exception e) {
								ToTo.logException(e);
							}

							try {
								InternalVariables.permissions = getPermissions(line);
								if (InternalVariables.permissions != null) {
									InternalVariables.permissions
											.setSymlink(symlink_final);
								}
							} catch (final Exception e) {
								Logger.getDefault().d(e.getMessage());
							}
						}
					}
				};
				Shell.startRootShell().add(command);
				commandWait(command);

				return InternalVariables.permissions;

			} catch (final Exception e) {
				Logger.getDefault().d(e.getMessage());
				return null;
			}
		}

		return null;
	}

	public static Permissions getPermissions(final String line) {

		final String[] lineArray = line.split(" ");
		final String rawPermissions = lineArray[0];

		if (rawPermissions.length() == 10
				&& (rawPermissions.charAt(0) == '-'
						|| rawPermissions.charAt(0) == 'd' || rawPermissions
						.charAt(0) == 'l')
				&& (rawPermissions.charAt(1) == '-' || rawPermissions.charAt(1) == 'r')
				&& (rawPermissions.charAt(2) == '-' || rawPermissions.charAt(2) == 'w')) {
			Logger.getDefault().d(rawPermissions);

			final Permissions permissions = new Permissions();

			permissions.setType(rawPermissions.substring(0, 1));

			Logger.getDefault().d(permissions.getType());

			permissions.setUserPermissions(rawPermissions.substring(1, 4));

			Logger.getDefault().d(permissions.getUserPermissions());

			permissions.setGroupPermissions(rawPermissions.substring(4, 7));

			Logger.getDefault().d(permissions.getGroupPermissions());

			permissions.setOtherPermissions(rawPermissions.substring(7, 10));

			Logger.getDefault().d(permissions.getOtherPermissions());

			final StringBuilder finalPermissions = new StringBuilder();
			finalPermissions.append(parseSpecialPermissions(rawPermissions));
			finalPermissions.append(parsePermissions(permissions
					.getUserPermissions()));
			finalPermissions.append(parsePermissions(permissions
					.getGroupPermissions()));
			finalPermissions.append(parsePermissions(permissions
					.getOtherPermissions()));

			permissions.setPermissions(Integer.parseInt(finalPermissions
					.toString()));

			return permissions;
		}

		return null;
	}

	static int parsePermissions(final String permission) {
		int tmp;
		if (permission.charAt(0) == 'r')
			tmp = 4;
		else
			tmp = 0;

		Logger.getDefault().d("permission " + tmp);
		Logger.getDefault().d("character " + permission.charAt(0));

		if (permission.charAt(1) == 'w')
			tmp += 2;
		else
			tmp += 0;

		Logger.getDefault().d("permission " + tmp);
		Logger.getDefault().d("character " + permission.charAt(1));

		if (permission.charAt(2) == 'x')
			tmp += 1;
		else
			tmp += 0;

		Logger.getDefault().d("permission " + tmp);
		Logger.getDefault().d("character " + permission.charAt(2));

		return tmp;
	}

	static int parseSpecialPermissions(final String permission) {
		int tmp = 0;
		if (permission.charAt(2) == 's')
			tmp += 4;

		if (permission.charAt(5) == 's')
			tmp += 2;

		if (permission.charAt(8) == 't')
			tmp += 1;

		Logger.getDefault().d("special permissions " + tmp);

		return tmp;
	}

	/**
	 * This will take a path, which can contain the file name as well, and
	 * attempt to remount the underlying partition.
	 * <p/>
	 * For example, passing in the following string:
	 * "/system/bin/some/directory/that/really/would/never/exist" will result in
	 * /system ultimately being remounted. However, keep in mind that the longer
	 * the path you supply, the more work this has to do, and the slower it will
	 * run.
	 * 
	 * @param file
	 *            file path
	 * @param mountType
	 *            mount type: pass in RO (Read only) or RW (Read Write)
	 * @return a <code>boolean</code> which indicates whether or not the
	 *         partition has been remounted as specified.
	 */
	public static boolean remount(final String file, final String mountType) {
		// Recieved a request, get an instance of Remounter
		final Remounter remounter = new Remounter();
		// send the request.
		return (remounter.remount(file, mountType));
	}

	/**
	 * This restarts only Android OS without rebooting the whole device. This
	 * does NOT work on all devices. This is done by killing the main init
	 * process named zygote. Zygote is restarted automatically by Android after
	 * killing it.
	 * 
	 * @throws TimeoutException
	 */
	public static void restartAndroid() {
		Logger.getDefault().d("Restart Android");
		killProcess("zygote");
	}

	/**
	 * This method can be used to kill a running process
	 * 
	 * @param processName
	 *            name of process to kill
	 * @return <code>true</code> if process was found and killed successfully
	 */
	public static boolean killProcess(final String processName) {
		Logger.getDefault().d("Killing process " + processName);

		InternalVariables.pid_list = "";

		// Assume that the process is running
		InternalVariables.processRunning = true;

		try {

			CommandCapture command = new CommandCapture(0, false, "ps") {
				@Override
				public void output(final int id, final String line) {
					if (line.contains(processName)) {
						final Matcher psMatcher = InternalVariables.psPattern
								.matcher(line);

						try {
							if (psMatcher.find()) {
								final String pid = psMatcher.group(1);

								InternalVariables.pid_list += " " + pid;
								InternalVariables.pid_list = InternalVariables.pid_list
										.trim();

								Logger.getDefault().d("Found pid: " + pid);
							} else {
								Logger.getDefault().d("Matching in ps command failed!");
							}
						} catch (final Exception e) {
							Logger.getDefault().d("Error with regex!");
							e.printStackTrace();
						}
					}
				}
			};
			RootTools.getShell(true).add(command);
			commandWait(command);

			// get all pids in one string, created in process method
			final String pids = InternalVariables.pid_list;

			// kill processes
			if (!pids.equals("")) {
				try {
					// example: kill -9 1234 1222 5343
					command = new CommandCapture(0, false, "kill -9 " + pids);
					RootTools.getShell(true).add(command);
					commandWait(command);

					return true;
				} catch (final Exception e) {
					Logger.getDefault().d(e.getMessage());
				}
			} else {
				// no pids match, must be dead
				return true;
			}
		} catch (final Exception e) {
			Logger.getDefault().d(e.getMessage());
		}

		return false;
	}

	/**
	 * Checks whether the toolbox or busybox binary contains a specific util
	 * 
	 * @param util
	 * @param box
	 *            Should contain "toolbox" or "busybox"
	 * @return true if it contains this util
	 */
	public static boolean hasUtil(final String util, final String box) {

		InternalVariables.found = false;

		// only for busybox and toolbox
		if (!(box.endsWith("toolbox") || box.endsWith("busybox"))) {
			return false;
		}

		try {

			final CommandCapture command = new CommandCapture(0, false,
					box.endsWith("toolbox") ? box + " " + util : box
							+ " --list") {

				@Override
				public void output(final int id, final String line) {
					if (box.endsWith("toolbox")) {
						if (!line.contains("no such tool")) {
							InternalVariables.found = true;
						}
					} else if (box.endsWith("busybox")) {
						// go through all lines of busybox --list
						if (line.contains(util)) {
							Logger.getDefault().d("Found util!");
							InternalVariables.found = true;
						}
					}
				}
			};
			RootTools.getShell(true).add(command);
			commandWait(command);

			if (InternalVariables.found) {
				Logger.getDefault().d("Box contains " + util + " util!");
				return true;
			} else {
				Logger.getDefault().d("Box does not contain " + util + " util!");
				return false;
			}
		} catch (final Exception e) {
			Logger.getDefault().d(e.getMessage());
			return false;
		}
	}

	/**
	 * @return <code>true</code> if su was found.
	 */
	public static boolean isRootAvailable() {
		return findBinary("su");
	}

	/**
	 * Executes binary in a separated process. Before using this method, the
	 * binary has to be installed in /data/data/app.package/files/ using the
	 * installBinary method.
	 * 
	 * @param context
	 *            the current activity's <code>Context</code>
	 * @param binaryName
	 *            name of installed binary
	 * @param parameter
	 *            parameter to append to binary like "-vxf"
	 */
	public static void runBinary(final Context context,
			final String binaryName, final String parameter) {
		final Runner runner = new Runner(context, binaryName, parameter);
		runner.start();
	}

	/**
	 * Copys a file to a destination. Because cp is not available on all android
	 * devices, we have a fallback on the cat command
	 * 
	 * @param source
	 *            example: /data/data/org.adaway/files/hosts
	 * @param destination
	 *            example: /system/etc/hosts
	 * @param remountAsRw
	 *            remounts the destination as read/write before writing to it
	 * @param preserveFileAttributes
	 *            tries to copy file attributes from source to destination, if
	 *            only cat is available only permissions are preserved
	 * @return true if it was successfully copied
	 */
	public static boolean copyFile(final String source,
			final String destination, final boolean remountAsRw,
			final boolean preserveFileAttributes) {
		CommandCapture command = null;
		boolean result = true;

		try {
			// mount destination as rw before writing to it
			if (remountAsRw) {
				RootTools.remount(destination, "RW");
			}

			// if cp is available and has appropriate permissions
			if (checkUtil("cp")) {
				Logger.getDefault().d("cp command is available!");

				if (preserveFileAttributes) {
					command = new CommandCapture(0, false, "cp -fp " + source
							+ " " + destination);
					Shell.startRootShell().add(command);
					commandWait(command);

					// ensure that the file was copied, an exitcode of zero
					// means success
					result = command.getExitCode() == 0;

				} else {
					command = new CommandCapture(0, false, "cp -f " + source
							+ " " + destination);
					Shell.startRootShell().add(command);
					commandWait(command);

					// ensure that the file was copied, an exitcode of zero
					// means success
					result = command.getExitCode() == 0;

				}
			} else {
				if (checkUtil("busybox") && hasUtil("cp", "busybox")) {
					Logger.getDefault().d("busybox cp command is available!");

					if (preserveFileAttributes) {
						command = new CommandCapture(0, false,
								"busybox cp -fp " + source + " " + destination);
						Shell.startRootShell().add(command);
						commandWait(command);

					} else {
						command = new CommandCapture(0, false, "busybox cp -f "
								+ source + " " + destination);
						Shell.startRootShell().add(command);
						commandWait(command);

					}
				} else { // if cp is not available use cat
					// if cat is available and has appropriate permissions
					if (checkUtil("cat")) {
						Logger.getDefault().d("cp is not available, use cat!");

						int filePermission = -1;
						if (preserveFileAttributes) {
							// get permissions of source before overwriting
							final Permissions permissions = getFilePermissionsSymlinks(source);
							filePermission = permissions.getPermissions();
						}

						// copy with cat
						command = new CommandCapture(0, false, "cat " + source
								+ " > " + destination);
						Shell.startRootShell().add(command);
						commandWait(command);

						if (preserveFileAttributes) {
							// setKey premissions of source to destination
							command = new CommandCapture(0, false, "chmod "
									+ filePermission + " " + destination);
							Shell.startRootShell().add(command);
							commandWait(command);
						}
					} else {
						result = false;
					}
				}
			}

			// mount destination back to ro
			if (remountAsRw) {
				RootTools.remount(destination, "RO");
			}
		} catch (final Exception e) {
			e.printStackTrace();
			result = false;
		}

		if (command != null) {
			// ensure that the file was copied, an exitcode of zero means
			// success
			result = command.getExitCode() == 0;
		}

		return result;
	}

	static final int IAG = 2;

	/**
	 * @return <code>true</code> if your app has been given root access.
	 * @throws TimeoutException
	 *             if this operation times out. (cannot determine if access is
	 *             given)
	 */
	public static boolean isRootAccessGiven() {
		try {
			Logger.getDefault().d("Checking for Root access");
			InternalVariables.accessGiven = false;

			final CommandCapture command = new CommandCapture(IAG, false, "id") {
				@Override
				public void output(final int id, final String line) {
					if (id == IAG) {
						final Set<String> ID = new HashSet<String>(
								Arrays.asList(line.split(" ")));
						for (final String userid : ID) {
							Logger.getDefault().d(userid);

							if (userid.toLowerCase().contains("uid=0")) {
								InternalVariables.accessGiven = true;
								Logger.getDefault().d("Access Given");
								break;
							}
						}
						if (!InternalVariables.accessGiven) {
							Logger.getDefault().d("Access Denied?");
						}
					}
				}
			};
			Shell.startRootShell().add(command);
			commandWait(command);

			if (InternalVariables.accessGiven) {
				return true;
			} else {
				return false;
			}

		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	static final int GS = 6;

	/**
	 * Get the space for a desired partition.
	 * 
	 * @param path
	 *            The partition to find the space for.
	 * @return the amount if space found within the desired partition. If the
	 *         space was not found then the value is -1
	 * @throws TimeoutException
	 */
	public static long getSpace(final String path) {
		InternalVariables.getSpaceFor = path;
		boolean found = false;
		Logger.getDefault().d("Looking for Space");
		try {
			final CommandCapture command = new CommandCapture(GS, false, "df "
					+ path) {

				@Override
				public void output(final int id, final String line) {
					if (id == GS) {
						if (line.contains(InternalVariables.getSpaceFor.trim())) {
							InternalVariables.space = line.split(" ");
						}
					}
				}
			};
			Shell.startRootShell().add(command);
			commandWait(command);

		} catch (final Exception e) {
		}

		if (InternalVariables.space != null) {
			Logger.getDefault().d("First Method");

			for (final String spaceSearch : InternalVariables.space) {

				Logger.getDefault().d(spaceSearch);

				if (found) {
					return getConvertedSpace(spaceSearch);
				} else if (spaceSearch.equals("used,")) {
					found = true;
				}
			}

			// Try this way
			int count = 0, targetCount = 3;

			Logger.getDefault().d("Second Method");

			if (InternalVariables.space[0].length() <= 5) {
				targetCount = 2;
			}

			for (final String spaceSearch : InternalVariables.space) {

				Logger.getDefault().d(spaceSearch);
				if (spaceSearch.length() > 0) {
					Logger.getDefault().d(spaceSearch + ("Valid"));
					if (count == targetCount) {
						return getConvertedSpace(spaceSearch);
					}
					count++;
				}
			}
		}
		Logger.getDefault().d("Returning -1, space could not be determined.");
		return -1;
	}

	/**
	 * @return long Size, converted to kilobytes (from xxx or xxxm or xxxk etc.)
	 */
	static long getConvertedSpace(final String spaceStr) {
		try {
			double multiplier = 1.0;
			char c;
			final StringBuffer sb = new StringBuffer();
			for (int i = 0; i < spaceStr.length(); i++) {
				c = spaceStr.charAt(i);
				if (!Character.isDigit(c) && c != '.') {
					if (c == 'm' || c == 'M') {
						multiplier = 1024.0;
					} else if (c == 'g' || c == 'G') {
						multiplier = 1024.0 * 1024.0;
					}
					break;
				}
				sb.append(spaceStr.charAt(i));
			}
			return (long) Math.ceil(Double.valueOf(sb.toString()) * multiplier);
		} catch (final Exception e) {
			return -1;
		}
	}

}
