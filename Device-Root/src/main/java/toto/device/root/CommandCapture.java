package toto.device.root;

import toto.log.Log;

class CommandCapture extends Command {
	private StringBuilder sb = new StringBuilder();

	CommandCapture(int id, String... command) {
		super(id, command);
	}

	CommandCapture(int id, boolean handlerEnabled, String... command) {
		super(id, handlerEnabled, command);
	}

	CommandCapture(int id, int timeout, String... command) {
		super(id, timeout, command);
	}

	@Override
	public void commandOutput(int id, String line) {
		sb.append(line).append('\n');
		Log.d("ID: " + id + ", " + line);
	}

	@Override
	public void commandTerminated(int id, String reason) {
		// pass
	}

	@Override
	public void commandCompleted(int id, int exitcode) {
		// pass
	}

	@Override
	public String toString() {
		return sb.toString();
	}
}