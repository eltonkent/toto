/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections.list;

/**
 * Circular list using a simple object array
 * 
 * @author elton.stephen.kent
 * 
 */
public class SimpleCircularList {
	int currentIndex;
	Object[] list;
	private int pollIndex = -1;

	int size;

	public SimpleCircularList(final int size) {
		this.size = size;
		this.list = new Object[size];
	}

	public void add(final Object o) {
		list[currentIndex] = o;
		currentIndex++;
	}

	public Object getObjectAt(final int index)
			throws ArrayIndexOutOfBoundsException {
		return list[index];
	}

	public Object poll() {
		final int temp = pollIndex + 1;
		if (temp > (size - 1)) {
			pollIndex = 0;
		} else {
			pollIndex++;
		}
		return list[pollIndex];
	}
}
