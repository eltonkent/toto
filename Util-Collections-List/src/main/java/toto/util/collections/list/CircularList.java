/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections.list;

import java.util.ArrayList;

/**
 * Cyclic list implementation. Based on a ArrayList.
 * 
 */
public class CircularList extends ArrayList {

	private int iCurrentIndex, iMaxIndex;

	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor. Arguments are just passed on to the superclass.
	 */

	public CircularList() {
		super();
		iCurrentIndex = -1;
		iMaxIndex = -1;
	}

	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor. Arguments are just passed on to the superclass.
	 * 
	 * @param initialCapacity
	 */

	public CircularList(final int initialCapacity) {
		super(initialCapacity);
		iCurrentIndex = -1;
		iMaxIndex = -1;
	}

	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor. Arguments are just passed on to the superclass.
	 * 
	 * @param initialCapacity
	 * @param capacityIncrement
	 */

	public CircularList(final int initialCapacity, final int capacityIncrement) {
		super(initialCapacity);
		iCurrentIndex = -1;
		iMaxIndex = -1;
	}

	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * Add an element to the list. Calls Vector.addElement().
	 * 
	 * @param obj
	 *            Object to add to the list.
	 */

	public final synchronized void addElem(final Object obj) {
		iMaxIndex++;
		iCurrentIndex = iMaxIndex;
		add(obj);
	}

	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * Return current element. If no element exists, return null.
	 */

	public final synchronized Object currElem() {
		if (iMaxIndex == -1)
			return null;
		else
			return get(iCurrentIndex);
	}

	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * Return next element. If no element exists, return null. If at end of the
	 * list, return the first element.
	 */

	public final synchronized Object nextElem() {
		try {
			if (iMaxIndex == -1)
				return null;
			else if (iCurrentIndex < iMaxIndex)
				iCurrentIndex++;
			else
				iCurrentIndex = 0;
			return get(iCurrentIndex);
		} catch (final Exception e) { // ArrayIndexOutOfBoundsException
										// impossible
			return null;
		}
	}

	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * Return previous element. If no element exists, return null. If at
	 * beginning of the list, return the last element.
	 */

	public final synchronized Object prevElem() {
		try {
			if (iMaxIndex == -1)
				return null;
			else if (iCurrentIndex > 0)
				iCurrentIndex--;
			else
				iCurrentIndex = iMaxIndex;
			return get(iCurrentIndex);
		} catch (final Exception e) { // ArrayIndexOutOfBoundsException
										// impossible
			return null;
		}
	}
}
