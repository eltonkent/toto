package toto.util.collections.list;

import static com.mi.toto.Conditions.checkElementIndex;
import static com.mi.toto.Conditions.checkNotNull;
import static com.mi.toto.Conditions.checkPositionIndexes;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

import com.mi.toto.Conditions;
import toto.lang.ByteUtils;

/**
 * Byte array that can be represented as a list
 * 
 * @author ekent4
 * 
 */
public class ByteArrayAsList extends AbstractList<Byte> implements
		RandomAccess, Serializable {
	final byte[] array;
	final int start;
	final int end;

	public ByteArrayAsList(final byte[] array) {
		this(array, 0, array.length);
	}

	ByteArrayAsList(final byte[] array, final int start, final int end) {
		this.array = array;
		this.start = start;
		this.end = end;
	}

	public int size() {
		return end - start;
	}

	public boolean isEmpty() {
		return false;
	}

	public Byte get(final int index) {
		Conditions.checkElementIndex(index, size());
		return array[start + index];
	}

	public boolean contains(final Object target) {
		// Overridden to prevent a ton of boxing
		return (target instanceof Byte)
				&& ByteUtils.indexOf(array, (Byte) target, start, end) != -1;
	}

	public int indexOf(final Object target) {
		// Overridden to prevent a ton of boxing
		if (target instanceof Byte) {
			final int i = ByteUtils.indexOf(array, (Byte) target, start, end);
			if (i >= 0) {
				return i - start;
			}
		}
		return -1;
	}

	@Override
	public int lastIndexOf(final Object target) {
		// Overridden to prevent a ton of boxing
		if (target instanceof Byte) {
			final int i = ByteUtils.lastIndexOf(array, (Byte) target, start,
					end);
			if (i >= 0) {
				return i - start;
			}
		}
		return -1;
	}

	@Override
	public Byte set(final int index, final Byte element) {
		checkElementIndex(index, size());
		final byte oldValue = array[start + index];
		// checkNotNull for GWT (do not optimize)
		array[start + index] = checkNotNull(element);
		return oldValue;
	}

	@Override
	public List<Byte> subList(final int fromIndex, final int toIndex) {
		final int size = size();
		checkPositionIndexes(fromIndex, toIndex, size);
		if (fromIndex == toIndex) {
			return Collections.emptyList();
		}
		return new ByteArrayAsList(array, start + fromIndex, start + toIndex);
	}

	@Override
	public boolean equals(final Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof ByteArrayAsList) {
			final ByteArrayAsList that = (ByteArrayAsList) object;
			final int size = size();
			if (that.size() != size) {
				return false;
			}
			for (int i = 0; i < size; i++) {
				if (array[start + i] != that.array[that.start + i]) {
					return false;
				}
			}
			return true;
		}
		return super.equals(object);
	}

	public int hashCode() {
		int result = 1;
		for (int i = start; i < end; i++) {
			result = 31 * result + ByteUtils.hashCode(array[i]);
		}
		return result;
	}

	public String toString() {
		final StringBuilder builder = new StringBuilder(size() * 5);
		builder.append('[').append(array[start]);
		for (int i = start + 1; i < end; i++) {
			builder.append(", ").append(array[i]);
		}
		return builder.append(']').toString();
	}

	public byte[] toByteArray() {
		// Arrays.copyOfRange() is not available under GWT
		final int size = size();
		final byte[] result = new byte[size];
		System.arraycopy(array, start, result, 0, size);
		return result;
	}

	private static final long serialVersionUID = 0;
}