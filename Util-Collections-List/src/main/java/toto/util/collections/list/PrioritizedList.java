/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections.list;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * List that allows items to be added with a priority that will affect the order
 * in which they are later iterated over. Objects with a high priority will
 * appear before objects with a low priority in the list. If two objects of the
 * same priority are added to the list, the most recently added one will be
 * iterated over first. Implementation uses a TreeSet, which has a guaranteed
 * add time of O(log(n)).
 * 
 */
public class PrioritizedList {

	private static class PrioritizedItem implements Comparable {

		final int id;
		final int priority;
		final Object value;

		public PrioritizedItem(final Object value, final int priority,
				final int id) {
			this.value = value;
			this.priority = priority;
			this.id = id;
		}

		@Override
		public int compareTo(final Object o) {
			final PrioritizedItem other = (PrioritizedItem) o;
			if (this.priority != other.priority) {
				return (other.priority - this.priority);
			}
			return (other.id - this.id);
		}

		@Override
		public boolean equals(final Object obj) {
			if (obj == null)
				return false;
			return this.id == ((PrioritizedItem) obj).id;
		}

	}

	private static class PrioritizedItemIterator implements Iterator {

		private final Iterator iterator;

		public PrioritizedItemIterator(final Iterator iterator) {
			this.iterator = iterator;
		}

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public Object next() {
			return ((PrioritizedItem) iterator.next()).value;
		}

		@Override
		public void remove() {
			// call iterator.remove()?
			throw new UnsupportedOperationException();
		}

	}

	private int lastId = 0;

	private int lowestPriority = Integer.MAX_VALUE;

	private final Set set = new TreeSet();

	public void add(final Object item, final int priority) {
		if (this.lowestPriority > priority) {
			this.lowestPriority = priority;
		}
		this.set.add(new PrioritizedItem(item, priority, ++lastId));
	}

	public Iterator iterator() {
		return new PrioritizedItemIterator(this.set.iterator());
	}

}
