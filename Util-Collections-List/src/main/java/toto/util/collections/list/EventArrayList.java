package toto.util.collections.list;

import java.util.ArrayList;
import java.util.Collection;

/**
 * ArrayList with callback events for add and remove operations.
 * 
 */
public class EventArrayList<E> extends ArrayList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1529768190064375609L;

	/**
	 * EventArray list callback
	 * 
	 * 
	 * @param <E>
	 */
	public static interface Callback<E> {
		public void itemAdded(E item);

		public void itemChanged(E item);

		public void itemRemoved(E... item);

		public void multipleItemsRemoved();
	}

	private Callback<E> callback;

	public void setCallback(final Callback<E> callback) {
		this.callback = callback;
	}

	public boolean add(final E e) {
		if (callback != null) {
			callback.itemAdded(e);
		}
		return super.add(e);
	}

	public E remove(final int index) {
		final E e = super.remove(index);
		if (callback != null) {
			callback.itemRemoved(e);
		}
		return e;
	}

	public boolean removeAll(final Collection<?> c) {
		final boolean e = super.removeAll(c);
		if (callback != null) {
			callback.multipleItemsRemoved();
		}
		return e;
	}

	public void clear() {
		if (!isEmpty()) {
			super.clear();
			if (callback != null) {
				callback.multipleItemsRemoved();
			}
		}
	}

	public E set(final int index, final E element) {
		final E e = super.set(index, element);
		if (e != null)
			if (callback != null) {
				callback.itemChanged(e);
			}
		return e;
	}

}
