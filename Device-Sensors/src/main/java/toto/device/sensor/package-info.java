/**
 * Device hardware(Sensors,Accessories) querying and reporting utilities.
 * <p>
 *Queries device paramters like
 *<ul>
 *<li>Software related information like Kernel/Firmware version.</li>
 *<li>Display Information like Density,Dimension, Refresh rate etc.</li>
 *<li>Sensor information</li>
 *<li>Network information</li>
 *</ul>
 *</p>
 */
package toto.device.sensor;