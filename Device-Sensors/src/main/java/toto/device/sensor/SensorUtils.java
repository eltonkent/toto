/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device.sensor;

import android.hardware.SensorManager;

public class SensorUtils {

	/**
	 * Get the force from a ACCELEROMETER sensor when the device is swung or any
	 * similar movement
	 * 
	 * @param values
	 *            the values given by the accelerometer sensor in the
	 *            SensorEvent object.
	 * @return
	 * @see android.hardware.SensorEvent#values
	 */
	public static double getForce(final float[] values) {
		double netForce = values[0] * values[0];
		netForce += values[1] * values[1]; // Y axis
		netForce += (values[2]) * (values[2]); // Z axis (upwards)
		netForce = Math.sqrt(netForce) - SensorManager.GRAVITY_EARTH;
		return netForce;
	}
}
