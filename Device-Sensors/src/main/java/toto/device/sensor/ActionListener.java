/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Determine the movement of the sensors using this class. The callbacks are
 * registered using the {@link toto.device.sensor.ActionListener.ActionCallback}
 * listener.
 * <p>
 * The Accelerometer sensor is used to determine the flip and shake and the
 * Orientation sensor is used to determine the tilt. </br/> <b>Implement a
 * SensorActionListener</b><br/>
 * <p/>
 * 
 * <pre>
 * <code>
 *  ActionCallback actionListener=new ActionCallback() {
 * 
 *        @Override
 * 		public boolean onTilt(SensorEvent event, int direction) {
 * 		//return true if TILT was consumed
 * 			return false;
 * 		}
 * 
 * 		@Override
 * 		public boolean onShake(SensorEvent event, float speed) {
 * 			//return true if SHAKE was consumed
 * 			return false;
 * 		}
 * 
 * 		@Override
 * 		public boolean onLongFlip(SensorEvent event) {
 * 			//return true if LONG FLIP was consumed
 * 			return false;
 * 		}
 * 
 * 		@Override
 * 		public boolean onFlip(SensorEvent event) {
 * 			//return true if FLIP was consumed
 * 			return false;
 * 		}
 * 
 * 		@Override
 * 		public boolean onFaceUp(SensorEvent event) {
 * 			return false;
 * 		}
 * 
 * 		@Override
 * 		public boolean onFaceDown(SensorEvent event) {
 * 			return false;
 * 		}
 * 	};
 * </code>
 * </pre>
 * <p/>
 * <b>Register the listener with the ActionListener instance</b><br/>
 * <p/>
 * 
 * <pre>
 * <code>
 * ActionListener action=new ActionListener(actionListener);
 * </code>
 * </pre>
 * <p/>
 * <b>Determine the sensor action by the values returned by the sensor
 * action.</b>
 * <p/>
 * 
 * <pre>
 *  <code>
 * 	SensorManager mgr=(SensorManager)ctxt.getSystemService(Context.SENSOR_SERVICE);
 *  mgr.registerListener(action,
 *  mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
 *  SensorManager.SENSOR_DELAY_UI);
 * 
 *  private SensorEventListener listener=new SensorEventListener() {
 *         public void onSensorChanged(SensorEvent e) {
 *          </code>
 * </pre>
 * <p/>
 * <b>action.determineActionListener(e);</b> <code>
 * <pre>
 *  		..
 *  		..
 *  		 }
 *   		}
 * 			</code> </pre>
 * 
 * </p>
 * 
 * @author Mobifluence Interactive
 */
public class ActionListener {

	public static interface ActionCallback {
		public int DIRECTION_LEFT = 1;
		public int DIRECTION_RIGHT = 2;
		public int DIRECTION_UP = 3;
		public int DIRECTION_DOWN = 4;

		public boolean onTilt(SensorEvent event, int direction);

		public boolean onFlip(SensorEvent event);

		public boolean onLongFlip(SensorEvent event);

		public boolean onShake(SensorEvent event, float speed);

		public boolean onFaceUp(SensorEvent event);

		public boolean onFaceDown(SensorEvent event);

		public void sensorUnavailable();
	}

	private static final int TIME_THRESHOLD = 100;

	private static final int FLIP_HOLD_THRESHOLD = 800;

	private static final int SPEED_THRESHOLD = 800; // 350

	private static final int SHAKE_TIMEOUT = 500;
	private static final int SHAKE_DURATION = 1000;
	private static final int SHAKE_COUNT = 3;

	private float lastX = -0.0f;

	private float lastY = -0.0f;

	private float lastZ = -0.0f;

	private long lastTime;

	private int shakeCount = 0;

	private long lastShake;

	private long lastShakeUpdate;

	private final ActionCallback sal;

	private float lastRoll;

	private float lastPitch;

	private long lastFaceUpUpdate;

	private boolean faceUp = true;

	private boolean flipped;

	private final SensorManager mSensorManager;

	public ActionListener(final Context context, final ActionCallback sal) {
		this.sal = sal;
		mSensorManager = (SensorManager) context
				.getSystemService(Context.SENSOR_SERVICE);
		final Sensor lightSensor = mSensorManager
				.getDefaultSensor(Sensor.TYPE_LIGHT);
		if (lightSensor != null) {
			mSensorManager.registerListener(listener, lightSensor,
					SensorManager.SENSOR_DELAY_NORMAL);
		} else {
			sal.sensorUnavailable();
		}
	}

	private void detectFlip(final long now, final long diff, final float x,
			final float y, final float z, final SensorEvent event) {

		if (diff < TIME_THRESHOLD) {
			return;
		}

		// flip
		if (z >= 0) {
			sal.onFaceUp(event);

			if (flipped) {
				sal.onFlip(event);
				flipped = false;
			}

			//
			faceUp = true;
			lastFaceUpUpdate = now;
		} else if (z < 0) {
			sal.onFaceDown(event);

			flipped = flipped || faceUp;

			if (flipped && now - lastFaceUpUpdate > FLIP_HOLD_THRESHOLD) {
				sal.onLongFlip(event);
				flipped = false;
			}

			faceUp = false;
		}

		lastTime = now;
	}

	private final SensorEventListener listener = new SensorEventListener() {
		public void onSensorChanged(final SensorEvent event) {
			determineSensorAction(event);
		}

		@Override
		public void onAccuracyChanged(final Sensor sensor, final int i) {

		}
	};

	private void determineSensorAction(final SensorEvent event) {
		final long now = System.currentTimeMillis();
		final long diff = (now - lastTime);

		final float[] values = event.values;

		final float x = values[SensorManager.DATA_X];
		final float y = values[SensorManager.DATA_Y];
		final float z = values[SensorManager.DATA_Z];

		final int type = event.sensor.getType();

		switch (type) {
		case Sensor.TYPE_ACCELEROMETER:
			detectFlip(now, diff, x, y, z, event);
			detectShake(now, diff, x, y, z, event);
			break;
		case Sensor.TYPE_ORIENTATION:
			detectTilt(now, diff, x, y, z, event);
			break;
		}
	}

	private void detectShake(final long now, final long diff, final float x,
			final float y, final float z, final SensorEvent event) {

		if (diff < TIME_THRESHOLD) {
			return;
		}

		// if (Debug.DEBUG) {
		// Log.d(TAG, "*** detectShake  elapse: " + diff + " lastTime: "
		// + lastTime + " now: " + now + "  x: " + x + " y: " + y
		// + " z: " + z);
		// }

		if ((now - lastShakeUpdate) > SHAKE_TIMEOUT) {
			shakeCount = 0;
		}

		final float speed = Math.abs(x + y + z - lastX - lastY - lastZ) / diff
				* 10000;
		//
		if (speed > SPEED_THRESHOLD) {
			if ((++shakeCount >= SHAKE_COUNT)
					&& (now - lastShake > SHAKE_DURATION)) {
				lastShake = now;
				shakeCount = 0;
				//
				sal.onShake(event, speed);
			}

			lastShakeUpdate = now;
		}

		//
		lastX = x;
		lastY = y;
		lastZ = z;

		lastTime = now;
	}

	private void detectTilt(final long now, final long diff,
			final float azimuth, final float pitch, final float roll,
			final SensorEvent event) {

		final float resetThreshold = 3.0f;

		if (pitch > -resetThreshold && pitch < resetThreshold) {
			lastPitch = pitch;
			return;
		}

		if (roll > -resetThreshold && roll < resetThreshold) {
			lastRoll = roll;
			return;
		}

		if (diff < TIME_THRESHOLD) {
			return;
		}

		// if (Debug.DEBUG) {
		// Log.d(TAG, " @@@@@@@@@@@@@@@ detectTilt " +
		// azimuth+":"+pitch+":"+roll);
		// }

		final float threshold = 25.0f;

		if (pitch - lastPitch > threshold) {
			sal.onTilt(event, ActionCallback.DIRECTION_DOWN);
			return;
		}

		if (pitch - lastPitch < -threshold) {
			sal.onTilt(event, ActionCallback.DIRECTION_UP);
			return;
		}

		if (roll - lastRoll > threshold) {
			sal.onTilt(event, ActionCallback.DIRECTION_LEFT);
			return;
		}

		if (roll - lastRoll < -threshold) {
			sal.onTilt(event, ActionCallback.DIRECTION_RIGHT);
			return;
		}

		lastTime = now;
	}

}
