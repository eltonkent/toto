package toto.device.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

/**
 * Ambient light sensor reporting.
 */
public class AmbientLightListener extends BaseSensorListener {
	public static interface AmbientLightCallback extends
			BaseSensorListener.BaseSensorCallback {
		public void tooDark();

		public void brightEnough();

		public void onSensorUnavailable();
	}

	private static final float TOO_DARK_LUX = 45.0f;
	private static final float BRIGHT_ENOUGH_LUX = 450.0f;

	private AmbientLightCallback callback;

	public AmbientLightListener(Context context, AmbientLightCallback callback) {
		super(context, Sensor.TYPE_LIGHT, SensorManager.SENSOR_DELAY_NORMAL);
		this.callback = callback;
		if (!isRegistered) {
			callback.onSensorUnavailable();
		}
	}

	@Override
	protected void handleSensorEvent(SensorEvent sensorEvent) {
		float ambientLightLux = sensorEvent.values[0];
		if (ambientLightLux <= TOO_DARK_LUX) {
			callback.tooDark();
		} else if (ambientLightLux >= BRIGHT_ENOUGH_LUX) {
			callback.brightEnough();
		}
	}

	@Override
	protected void handleAccuracyChanged(Sensor sensor, int i) {

	}
}
