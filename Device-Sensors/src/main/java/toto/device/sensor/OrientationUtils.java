package toto.device.sensor;

/**
 * Created by Elton Kent on 4/19/2015.
 */
public class OrientationUtils {
    /**
     * Convert side to angle in degrees
     *
     * @param side
     * @return
     */
    public static int toDegrees(final Side side) {
        int degree = 0;
        switch (side) {
            case TOP:
                degree = 0;
                break;
            case RIGHT:
                degree = 90;
                break;
            case BOTTOM:
                degree = 180;
                break;
            case LEFT:
                degree = 270;
                break;
        }

        return degree;

    }
}
