/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.device.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

/**
 * Sensor listener for device orientation
 */
public final class OrientationListener extends BaseSensorListener {

	public static interface OrientationCallback extends
			BaseSensorListener.BaseSensorCallback {

		public void onOrientationChanged(Side newOrientation);

		public void onSensorUnavailable();
	}





	private Side mCurrentSide;

	private OrientationCallback mListener;

	/**
	 * setup sensor and callbacks
	 * 
	 * @param context
	 * @param callback
	 */
	public OrientationListener(final Context context,
			OrientationCallback callback) {
		super(context, Sensor.TYPE_ORIENTATION,
				SensorManager.SENSOR_DELAY_NORMAL);
		this.mListener = callback;
		if (!isRegistered) {
			mListener.onSensorUnavailable();
		}
	}

	public Side getOrientation() {
		return mCurrentSide;
	}

	@Override
	protected void handleSensorEvent(SensorEvent event) {
		final float pitch = event.values[1]; // pitch
		final float roll = event.values[2]; // roll

		Side newSide = Side.TOP;

		if (pitch < -45 && pitch > -135) {
			// top side up
			newSide = Side.TOP;
		} else if (pitch > 45 && pitch < 135) {
			// bottom side up
			newSide = Side.BOTTOM;
		} else if (roll > 45) {
			// right side up
			newSide = Side.RIGHT;
		} else if (roll < -45) {
			// left side up
			newSide = Side.LEFT;
		}

		if (mListener != null && !newSide.equals(mCurrentSide)) {
			mListener.onOrientationChanged(newSide);
			mCurrentSide = newSide;
		}
	}

	@Override
	protected void handleAccuracyChanged(Sensor sensor, int i) {

	}

}
