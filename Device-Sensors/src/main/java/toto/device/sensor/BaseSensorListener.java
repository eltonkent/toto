package toto.device.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Base sensor manager class
 */
public abstract class BaseSensorListener {
	private SensorManager mSensorManager;
	protected boolean isRegistered;
	private int sensorType;
	private int sensorDelay;

	protected interface BaseSensorCallback {
		public void onSensorUnavailable();
	}

	BaseSensorListener(Context context, int sensorType, int sensorDelay) {
		this.sensorType = sensorType;
		this.sensorDelay = sensorDelay;
		mSensorManager = (SensorManager) context
				.getSystemService(Context.SENSOR_SERVICE);
		resume();
	}

	private SensorEventListener sensorEventListener = new SensorEventListener() {
		@Override
		public void onSensorChanged(SensorEvent sensorEvent) {
			handleSensorEvent(sensorEvent);
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int i) {
			handleAccuracyChanged(sensor, i);
		}
	};

	protected abstract void handleSensorEvent(SensorEvent sensorEvent);

	protected abstract void handleAccuracyChanged(Sensor sensor, int i);

	private boolean listenerAdded;

	/**
	 * Stop the sensor from
	 */
	public void stop() {
		if (isRegistered) {
			mSensorManager.unregisterListener(sensorEventListener);
			listenerAdded = false;
		}
	}

	public void resume() {
		if (!listenerAdded) {
			Sensor sensor = mSensorManager.getDefaultSensor(sensorType);
			if (sensor != null) {
				mSensorManager.registerListener(sensorEventListener, sensor,
						sensorDelay);
				isRegistered = true;
				listenerAdded = true;
			} else {
				isRegistered = false;
				listenerAdded = false;
			}
		}
	}

	@Override
	protected void finalize() {
		stop();
	}
}
