package toto.ui.widget.layout;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class VerticalLinearLayout extends LinearLayout {
	public VerticalLinearLayout(final Context context) {
		super(context);
	}

	public VerticalLinearLayout(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}

	// public VerticalLinearLayout(final Context context,
	// final AttributeSet attrs, final int defStyle) {
	// super(context, attrs, defStyle);
	// }

	@Override
	protected void onMeasure(final int widthSpec, final int heightSpec) {
		super.onMeasure(widthSpec, heightSpec);
		setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
	}

	@Override
	protected void dispatchDraw(final Canvas canvas) {
		// canvas.rotate(90.0F);
		// canvas.translate(0.0F, -getWidth());
		// super.onDraw(canvas);
		canvas.save();
		canvas.translate(0, getHeight());
		canvas.rotate(-90, 0, 0);
		super.dispatchDraw(canvas);
		canvas.restore();
	}
}
