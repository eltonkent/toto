package toto.ui.widget.layout;
///*******************************************************************************
// * Mobifluence Interactive
// * mobifluence.com (c) 2013
// * NOTICE:
// * All information contained herein is, and remains the property of
// * Mobifluence Interactive and its suppliers, if any.  The intellectual
// * and technical concepts contained herein are proprietary to
// * Mobifluence Interactive and its suppliers  are protected by
// * trade secret or copyright law. Dissemination of this information
// * or reproduction of this material is strictly forbidden unless prior
// * written permission is obtained from Mobifluence Interactive.
// *
// * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
// * which is part of the binary distribution.
// * API Design - Elton Kent
// ******************************************************************************/
//package rage.ui.widget.layout;
//
//import android.os.Parcel;
//import android.os.Parcelable;
//import android.view.View.BaseSavedState;
//
//class HorizontalScrollViewSavedState extends BaseSavedState {
//	public static final Parcelable.Creator<HorizontalScrollViewSavedState> CREATOR = new Parcelable.Creator<HorizontalScrollViewSavedState>() {
//		@Override
//		public HorizontalScrollViewSavedState createFromParcel(final Parcel in) {
//			return new HorizontalScrollViewSavedState(in);
//		}
//
//		@Override
//		public HorizontalScrollViewSavedState[] newArray(final int size) {
//			return new HorizontalScrollViewSavedState[size];
//		}
//	};
//
//	int currentPage = -1;
//
//	private HorizontalScrollViewSavedState(final Parcel in) {
//		super(in);
//		currentPage = in.readInt();
//	}
//
//	HorizontalScrollViewSavedState(final Parcelable superState) {
//		super(superState);
//	}
//
//	@Override
//	public void writeToParcel(final Parcel out, final int flags) {
//		super.writeToParcel(out, flags);
//		out.writeInt(currentPage);
//	}
// }
