package toto.ui.widget.layout;

import java.util.HashSet;
import java.util.Set;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.graphics.Xfermode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mi.toto.ui.layout.R;

/**
 * View group that arranges children in a circle based on available space.
 * <p>
 * <br/>
 * <b>Styled Attributes</b><br/>
 * <table cellspacing="1" cellpadding="3">
 * <tr>
 * <th><b>Attribute</b></td>
 * <td><b>Type</b></td>
 * <td><b>Default</b></td>
 * <td><b>Description</b></td>
 * </tr>
 * 
 * 
 * <tr>
 * <td><code>innerRadius</code></td>
 * <td>Dimension</td>
 * <td>80px</td>
 * <td>Radius of inner circle. increasing the inner radius bring the child views
 * closer to the center of the circle layout</td>
 * </tr>
 * 
 * <tr>
 * <td><code>sliceDivider</code></td>
 * <td>Color</td>
 * <td>android.R.color.darker_gray</td>
 * <td>Color of divider between slices if the <code>layoutMode </code> is pie.</td>
 * </tr>
 * 
 * <tr>
 * <td><code>innerCircle</code></td>
 * <td>Color or drawable reference</td>
 * <td>android.R.color.white</td>
 * <td>Color or drawable that will be drawn in center of pie.</td>
 * </tr>
 * 
 * <tr>
 * <td><code>dividerWidth </code></td>
 * <td>Dimension</td>
 * <td>1</td>
 * <td>Width of the divider between slices. if the <code>layoutMode </code> is
 * pie.</td>
 * </tr>
 * 
 * <tr>
 * <td><code>layoutMode</code></td>
 * <td><b>Enum:</b><br/>
 * normal<br/>
 * pie</td>
 * <td>normal</td>
 * <td>Layout algorithm for the circle layout. The normal mode respects padding
 * between children while the pie mode arranges children like pie slices</td>
 * </tr>
 * 
 * <tr>
 * <td><code>dividerWidth</code></td>
 * <td>Dimension</td>
 * <td>1</td>
 * <td>Width of the divider between slices. if the layout mode is pie.</td>
 * </tr>
 * 
 * <tr>
 * <td><code>angleOffset</code></td>
 * <td>float</td>
 * <td>90</td>
 * <td>Width of the divider between slices. if the layout mode is pie.</td>
 * </tr>
 * 
 * <tr>
 * <td><code>angleRange</code></td>
 * <td>float</td>
 * <td>360</td>
 * <td>Total angular range that should be used for the circular layout</td>
 * </tr>
 * 
 * </table>
 * <img src="../../../../resources/circlelayout.png"/>
 * </p>
 * 
 * @author ekent4
 */
public class CircleLayout extends ViewGroup {

	public static final int LAYOUT_NORMAL = 0;
	public static final int LAYOUT_PIE = 1;

	private int mLayoutMode = LAYOUT_NORMAL;

	private Drawable mInnerCircle;

	private float mAngleOffset = 90f;
	private float mAngleRange = 360f;

	private float mDividerWidth;
	private int mInnerRadius;

	private final Paint mDividerPaint;
	private final Paint mCirclePaint;

	private final RectF mBounds = new RectF();

	private Bitmap mDst;
	private Bitmap mSrc;
	private Canvas mSrcCanvas;
	private Canvas mDstCanvas;
	private final Xfermode mXfer;
	private final Paint mXferPaint;

	private View mMotionTarget;

	private Bitmap mDrawingCache;
	private Canvas mCachedCanvas;
	private final Set<View> mDirtyViews = new HashSet<View>();
	private boolean mCached = false;

	public CircleLayout(final Context context) {
		this(context, null);
	}

	public CircleLayout(final Context context, final AttributeSet attrs) {
		super(context, attrs);

		mDividerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

		final TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.CircleLayout);

		try {
			final int dividerColor = a.getColor(
					R.styleable.CircleLayout_slice_divider,
					android.R.color.darker_gray);
			mInnerCircle = a.getDrawable(R.styleable.CircleLayout_inner_circle);

			if (mInnerCircle instanceof ColorDrawable) {
				final int innerColor = a.getColor(
						R.styleable.CircleLayout_inner_circle,
						android.R.color.white);
				mCirclePaint.setColor(innerColor);
			}
			mDividerPaint.setColor(dividerColor);
			mAngleOffset = a.getFloat(R.styleable.CircleLayout_angle_offset,
					mAngleOffset);
			mAngleRange = a.getFloat(R.styleable.CircleLayout_angle_range,
					mAngleRange);
			mDividerWidth = a.getDimensionPixelSize(
					R.styleable.CircleLayout_divider_width, 1);
			mInnerRadius = a.getDimensionPixelSize(
					R.styleable.CircleLayout_inner_radius, 80);
			mLayoutMode = a.getInteger(R.styleable.CircleLayout_layout_mode,
					LAYOUT_NORMAL);
		} finally {
			a.recycle();
		}
		mDividerPaint.setStrokeWidth(mDividerWidth);
		mXfer = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
		mXferPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

		// // Turn off hardware acceleration if possible
		// if (Build.VERSION.SDK_INT >= 11) {
		// setLayerType(LAYER_TYPE_SOFTWARE, null);
		// }
	}

	@SuppressLint("Override")
	public void setLayoutMode(final int mode) {
		mLayoutMode = mode;
		requestLayout();
		invalidate();
	}

	@SuppressLint("Override")
	public int getLayoutMode() {
		return mLayoutMode;
	}

	public int getRadius() {
		final int width = getWidth();
		final int height = getHeight();

		final float minDimen = width > height ? height : width;

		final float radius = (minDimen - mInnerRadius) / 2f;

		return (int) radius;
	}

	public void getCenter(final PointF p) {
		p.set(getWidth() / 2f, getHeight() / 2);
	}

	public void setAngleOffset(final float offset) {
		mAngleOffset = offset;
		requestLayout();
		invalidate();
	}

	public float getAngleOffset() {
		return mAngleOffset;
	}

	public void setInnerRadius(final int radius) {
		mInnerRadius = radius;
		requestLayout();
		invalidate();
	}

	public int getInnerRadius() {
		return mInnerRadius;
	}

	public void setInnerCircle(final Drawable d) {
		mInnerCircle = d;
		requestLayout();
		invalidate();
	}

	public void setInnerCircle(final int res) {
		mInnerCircle = getContext().getResources().getDrawable(res);
		requestLayout();
		invalidate();
	}

	public void setInnerCircleColor(final int color) {
		mInnerCircle = new ColorDrawable(color);
		requestLayout();
		invalidate();
	}

	public Drawable getInnerCircle() {
		return mInnerCircle;
	}

	@Override
	protected void onMeasure(final int widthMeasureSpec,
			final int heightMeasureSpec) {
		final int count = getChildCount();

		int maxHeight = 0;
		int maxWidth = 0;

		// Find rightmost and bottommost child
		for (int i = 0; i < count; i++) {
			final View child = getChildAt(i);
			if (child.getVisibility() != GONE) {
				measureChild(child, widthMeasureSpec, heightMeasureSpec);
				maxWidth = Math.max(maxWidth, child.getMeasuredWidth());
				maxHeight = Math.max(maxHeight, child.getMeasuredHeight());
			}
		}

		// Check against our minimum height and width
		maxHeight = Math.max(maxHeight, getSuggestedMinimumHeight());
		maxWidth = Math.max(maxWidth, getSuggestedMinimumWidth());

		final int width = resolveSize(maxWidth, widthMeasureSpec);
		final int height = resolveSize(maxHeight, heightMeasureSpec);

		setMeasuredDimension(width, height);

		if (mSrc != null
				&& (mSrc.getWidth() != width || mSrc.getHeight() != height)) {
			mDst.recycle();
			mSrc.recycle();
			mDrawingCache.recycle();

			mDst = null;
			mSrc = null;
			mDrawingCache = null;
		}

		if (mSrc == null) {
			mSrc = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
			mDst = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
			mDrawingCache = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);

			mSrcCanvas = new Canvas(mSrc);
			mDstCanvas = new Canvas(mDst);
			mCachedCanvas = new Canvas(mDrawingCache);
		}
	}

	private LayoutParams layoutParams(final View child) {
		return (LayoutParams) child.getLayoutParams();
	}

	@Override
	protected void onLayout(final boolean changed, final int l, final int t,
			final int r, final int b) {
		final int childs = getChildCount();

		float totalWeight = 0f;

		for (int i = 0; i < childs; i++) {
			final View child = getChildAt(i);

			final LayoutParams lp = layoutParams(child);

			totalWeight += lp.weight;
		}

		final int width = getWidth();
		final int height = getHeight();

		final float minDimen = width > height ? height : width;
		final float radius = (minDimen - mInnerRadius) / 2f;

		mBounds.set(width / 2 - minDimen / 2, height / 2 - minDimen / 2, width
				/ 2 + minDimen / 2, height / 2 + minDimen / 2);

		float startAngle = mAngleOffset;

		for (int i = 0; i < childs; i++) {
			final View child = getChildAt(i);

			final LayoutParams lp = layoutParams(child);

			final float angle = mAngleRange / totalWeight * lp.weight;

			final float centerAngle = startAngle + angle / 2f;
			final int x;
			final int y;

			if (childs > 1) {
				x = (int) (radius * Math.cos(Math.toRadians(centerAngle)))
						+ width / 2;
				y = (int) (radius * Math.sin(Math.toRadians(centerAngle)))
						+ height / 2;
			} else {
				x = width / 2;
				y = height / 2;
			}

			final int halfChildWidth = child.getMeasuredWidth() / 2;
			final int halfChildHeight = child.getMeasuredHeight() / 2;

			final int left = lp.width != LayoutParams.FILL_PARENT ? x
					- halfChildWidth : 0;
			final int top = lp.height != LayoutParams.FILL_PARENT ? y
					- halfChildHeight : 0;
			final int right = lp.width != LayoutParams.FILL_PARENT ? x
					+ halfChildWidth : width;
			final int bottom = lp.height != LayoutParams.FILL_PARENT ? y
					+ halfChildHeight : height;

			child.layout(left, top, right, bottom);

			if (left != child.getLeft() || top != child.getTop()
					|| right != child.getRight() || bottom != child.getBottom()
					|| lp.startAngle != startAngle
					|| lp.endAngle != startAngle + angle) {
				mCached = false;
			}

			lp.startAngle = startAngle;

			startAngle += angle;

			lp.endAngle = startAngle;
		}

		invalidate();
	}

	@Override
	protected LayoutParams generateDefaultLayoutParams() {
		return new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
	}

	@Override
	protected LayoutParams generateLayoutParams(final ViewGroup.LayoutParams p) {
		final LayoutParams lp = new LayoutParams(p.width, p.height);

		if (p instanceof LinearLayout.LayoutParams) {
			lp.weight = ((LinearLayout.LayoutParams) p).weight;
		}

		return lp;
	}

	@Override
	public LayoutParams generateLayoutParams(final AttributeSet attrs) {
		return new LayoutParams(getContext(), attrs);
	}

	@Override
	protected boolean checkLayoutParams(final ViewGroup.LayoutParams p) {
		return p instanceof LayoutParams;
	}

	@Override
	public boolean dispatchTouchEvent(final MotionEvent ev) {
		if (mLayoutMode == LAYOUT_NORMAL) {
			return super.dispatchTouchEvent(ev);
		}

		final int action = ev.getAction();
		final float x = ev.getX() - getWidth() / 2f;
		final float y = ev.getY() - getHeight() / 2f;

		if (action == MotionEvent.ACTION_DOWN) {

			if (mMotionTarget != null) {
				final MotionEvent cancelEvent = MotionEvent.obtain(ev);
				cancelEvent.setAction(MotionEvent.ACTION_CANCEL);

				cancelEvent.offsetLocation(-mMotionTarget.getLeft(),
						-mMotionTarget.getTop());

				mMotionTarget.dispatchTouchEvent(cancelEvent);
				cancelEvent.recycle();

				mMotionTarget = null;
			}

			final float radius = (float) Math.sqrt(x * x + y * y);

			if (radius < mInnerRadius || radius > getWidth() / 2f
					|| radius > getHeight() / 2f) {
				return false;
			}

			float angle = (float) Math.toDegrees(Math.atan2(y, x));

			if (angle < 0)
				angle += mAngleRange;

			final int childs = getChildCount();

			for (int i = 0; i < childs; i++) {
				final View child = getChildAt(i);
				final LayoutParams lp = layoutParams(child);

				final float startAngle = lp.startAngle % mAngleRange;
				float endAngle = lp.endAngle % mAngleRange;
				float touchAngle = angle;

				if (startAngle > endAngle) {
					if (touchAngle < startAngle && touchAngle < endAngle) {
						touchAngle += mAngleRange;
					}

					endAngle += mAngleRange;
				}

				if (startAngle <= touchAngle && endAngle >= touchAngle) {
					ev.offsetLocation(-child.getLeft(), -child.getTop());

					final boolean dispatched = child.dispatchTouchEvent(ev);

					if (dispatched) {
						mMotionTarget = child;

						return true;
					} else {
						ev.setLocation(0f, 0f);

						return onTouchEvent(ev);
					}
				}
			}
		} else if (mMotionTarget != null) {
			ev.offsetLocation(-mMotionTarget.getLeft(), -mMotionTarget.getTop());

			mMotionTarget.dispatchTouchEvent(ev);

			if (action == MotionEvent.ACTION_UP
					|| action == MotionEvent.ACTION_CANCEL) {
				mMotionTarget = null;
			}
		}

		return onTouchEvent(ev);
	}

	private void drawChild(final Canvas canvas, final View child,
			final LayoutParams lp) {
		mSrcCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
		mDstCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

		mSrcCanvas.save();

		final int childLeft = child.getLeft();
		final int childTop = child.getTop();
		final int childRight = child.getRight();
		final int childBottom = child.getBottom();

		mSrcCanvas.clipRect(childLeft, childTop, childRight, childBottom,
				Op.REPLACE);
		mSrcCanvas.translate(childLeft, childTop);

		child.draw(mSrcCanvas);

		mSrcCanvas.restore();

		mXferPaint.setXfermode(null);
		mXferPaint.setColor(Color.BLACK);

		final float sweepAngle = (lp.endAngle - lp.startAngle) % 361;

		mDstCanvas
				.drawArc(mBounds, lp.startAngle, sweepAngle, true, mXferPaint);
		mXferPaint.setXfermode(mXfer);
		mDstCanvas.drawBitmap(mSrc, 0f, 0f, mXferPaint);

		canvas.drawBitmap(mDst, 0f, 0f, null);
	}

	private void redrawDirty(final Canvas canvas) {
		for (final View child : mDirtyViews) {
			drawChild(canvas, child, layoutParams(child));
		}

		if (mMotionTarget != null) {
			drawChild(canvas, mMotionTarget, layoutParams(mMotionTarget));
		}
	}

	private void drawDividers(final Canvas canvas, final float halfWidth,
			final float halfHeight, final float radius) {
		final int childs = getChildCount();

		if (childs < 2) {
			return;
		}

		for (int i = 0; i < childs; i++) {
			final View child = getChildAt(i);
			final LayoutParams lp = layoutParams(child);

			canvas.drawLine(halfWidth, halfHeight,
					radius * (float) Math.cos(Math.toRadians(lp.startAngle))
							+ halfWidth,
					radius * (float) Math.sin(Math.toRadians(lp.startAngle))
							+ halfHeight, mDividerPaint);

			if (i == childs - 1) {
				canvas.drawLine(halfWidth, halfHeight,
						radius * (float) Math.cos(Math.toRadians(lp.endAngle))
								+ halfWidth,
						radius * (float) Math.sin(Math.toRadians(lp.endAngle))
								+ halfHeight, mDividerPaint);
			}
		}
	}

	private void drawInnerCircle(final Canvas canvas, final float halfWidth,
			final float halfHeight) {
		if (mInnerCircle != null) {
			if (!(mInnerCircle instanceof ColorDrawable)) {
				mInnerCircle
						.setBounds((int) halfWidth - mInnerRadius,
								(int) halfHeight - mInnerRadius,
								(int) halfWidth + mInnerRadius,
								(int) halfHeight + mInnerRadius);

				mInnerCircle.draw(canvas);
			} else {
				canvas.drawCircle(halfWidth, halfHeight, mInnerRadius,
						mCirclePaint);
			}
		}
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		if (mLayoutMode == LAYOUT_NORMAL) {
			super.dispatchDraw(canvas);
			return;
		}

		if (mSrc == null || mDst == null || mSrc.isRecycled()
				|| mDst.isRecycled()) {
			return;
		}

		final int childs = getChildCount();

		final float halfWidth = getWidth() / 2f;
		final float halfHeight = getHeight() / 2f;

		final float radius = halfWidth > halfHeight ? halfHeight : halfWidth;

		if (mCached && mDrawingCache != null && !mDrawingCache.isRecycled()
				&& mDirtyViews.size() < childs / 2) {
			canvas.drawBitmap(mDrawingCache, 0f, 0f, null);

			redrawDirty(canvas);

			drawDividers(canvas, halfWidth, halfHeight, radius);

			drawInnerCircle(canvas, halfWidth, halfHeight);

			return;
		} else {
			mCached = false;
		}

		Canvas sCanvas = null;

		if (mCachedCanvas != null) {
			sCanvas = canvas;
			canvas = mCachedCanvas;
		}

		final Drawable bkg = getBackground();
		if (bkg != null) {
			bkg.draw(canvas);
		}

		for (int i = 0; i < childs; i++) {
			final View child = getChildAt(i);
			final LayoutParams lp = layoutParams(child);

			drawChild(canvas, child, lp);
		}

		drawDividers(canvas, halfWidth, halfHeight, radius);

		drawInnerCircle(canvas, halfWidth, halfHeight);

		if (mCachedCanvas != null) {
			sCanvas.drawBitmap(mDrawingCache, 0f, 0f, null);
			mDirtyViews.clear();
			mCached = true;
		}
	}

	public static class LayoutParams extends ViewGroup.LayoutParams {

		private float startAngle;
		private float endAngle;

		public float weight = 1f;

		public LayoutParams(final int width, final int height) {
			super(width, height);
		}

		public LayoutParams(final Context context, final AttributeSet attrs) {
			super(context, attrs);
		}
	}

}