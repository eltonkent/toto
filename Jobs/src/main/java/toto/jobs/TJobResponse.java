/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.jobs;

import java.io.Serializable;

/**
 * 
 */
@SuppressWarnings("serial")
public class TJobResponse<T> implements Serializable {

	public static int STATUS_SUCCESS = 10;
	public static int STATUS_FAILURE = -1;

	private T data;
	private int responseid;
	private int responseStatus = STATUS_SUCCESS;

	private Throwable t;

	public T getData() {
		return data;
	}

	public int getResponseId() {
		return responseid;
	}

	public int getResponseStatus() {
		return responseStatus;
	}

	Throwable getT() {
		return t;
	}

	public void setData(final T data) {
		this.data = data;
	}

	void setResponseId(final int response) {
		this.responseid = response;
	}

	void setResponseStatus(final int responseStatus) {
		this.responseStatus = responseStatus;
	}

	void setT(final Throwable t) {
		this.t = t;
	}

}
