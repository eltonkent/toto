/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.jobs;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.util.Log;

import toto.net.client.http.HttpClient;

/**
 * Async task to download a list of bitmaps
 * <p>
 * This task takes a list of java.net.URI instances that point to bitmaps. The
 * bitmaps are downloaded(using Http GET) sequentially and sent to the UI using
 * the {@link toto.jobs.TJob.JobNotifier#onJobCompletedUI(TJobResponse)}
 * method .<br/>
 * <b>Usage</b>
 * 
 * <pre>
 * URI url = URI.create(&quot;http://www.foo.com/image1.png&quot;);
 * 
 * BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
 * 
 * Object[] param = new Object[2];
 * param[0] = url;
 * param[1] = bitmapOptions;
 * new DownloadBitmapJob(this, this).execute(param);
 * </pre>
 * 
 * </p>
 * 
 */
public class DownloadBitmapJob extends TJob {

	public DownloadBitmapJob(final Context context, final JobNotifier notifier) {
		super(context, notifier);
	}

	@Override
	protected TJobResponse<Bitmap> doInBackground(final Object... params) {
		/* sending */
		final URI uri = (URI) params[0];
		final Options opt = (Options) params[1];
		final TJobResponse<Bitmap> response = new TJobResponse();
		response.setResponseId(0);
		final HttpClient helper = new HttpClient(uri);
		try {
			Log.d("ToTo", "Downloading Bitmap->" + uri);
			final HttpResponse entity = helper.execute();
			final InputStream is = entity.getEntity().getContent();
			final Bitmap bitmap = BitmapFactory.decodeStream(is, null, opt);
			response.setData(bitmap);
			response.setResponseStatus(TJobResponse.STATUS_SUCCESS);
			Log.d("ToTo", "Complete!");
			getNotifier().onJobCompletedNonUI(response);
		} catch (final IOException e) {
			response.setResponseStatus(-1);
			response.setT(e);
			e.printStackTrace();
		} catch (final URISyntaxException e) {
			response.setResponseStatus(TJobResponse.STATUS_FAILURE);
			response.setT(e);
			e.printStackTrace();
		}
		return response;
	}
}
