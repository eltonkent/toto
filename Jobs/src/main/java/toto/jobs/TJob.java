/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.jobs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;

/**
 * Extension to Async task with optional connectivity management features.
 * 
 * @author ekent4
 * 
 */
public abstract class TJob extends
		AsyncTask<Object, Object, TJobResponse> {

	/**
	 * Task Progress callback
	 * <p>
	 * Set the template type to the response type
	 * </p>
	 * 
	 */
	public interface JobNotifier<T> {

		/**
		 * Called if there is an error in
		 * 
		 * @param t
		 */
		public void onError(Throwable t);

		/**
		 * Called when the task is completed. Called from the UI thread.
		 */
		public void onJobCompletedUI(TJobResponse<T> response);

		/**
		 * Called when the task is started . Called from the UI thread.
		 */
		public void onJobStarted();

		/**
		 * Process response on the same background thread running the task
		 */
		public void onJobCompletedNonUI(TJobResponse<T> response);

	}

	/**
	 * Network changes notifier
	 * 
	 * @see toto.jobs.TJob#setConnectionNotifier(toto.jobs.TJob.ConnectionNotifier)
	 * 
	 */
	public interface ConnectionNotifier {

		/**
		 * Called from the UI thread when data connectivity is lost.
		 */
		public void onNetworkDisconnected();
	}

	/**
	 * Set data connectivity changes for this task
	 * 
	 * @param notifier
	 */
	public void setConnectionNotifier(final ConnectionNotifier notifier) {
		if (taskCompleted) {
			throw new IllegalStateException(
					"The task has already been completed. please setKey this notifier before calling execute");
		}
		this.connNotifier = notifier;
		final IntentFilter networkFilter = new IntentFilter();
		networkFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkStateReceiver();
		appContext.registerReceiver(receiver, networkFilter);
	}

	private class NetworkStateReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(final Context context, final Intent intent) {
			if (intent.hasExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY)) {
				final boolean isConnected = intent.getBooleanExtra(
						ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
				if (!isConnected && connNotifier != null) {
					connNotifier.onNetworkDisconnected();
				}

			}
		}

	}

	protected Context appContext;
	protected JobNotifier notifier;
	protected ConnectionNotifier connNotifier;
	private NetworkStateReceiver receiver;
	private boolean taskCompleted;

	public TJob(final Context context, final JobNotifier notifier) {
		this.notifier = notifier;
		this.appContext = context;
	}

	public TJob(final Context context) {
		this(context, null);
	}

	public Context getAppContext() {
		return appContext;
	}

	public JobNotifier getNotifier() {
		return notifier;
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (this.notifier == null)
			return;
		notifier.onJobStarted();
	}

	protected void onPostExecute(final TJobResponse response) {
		if (this.notifier == null)
			return;
		if (response.getResponseStatus() > 0) {
			notifier.onJobCompletedUI(response);
		} else {
			notifier.onError(response.getT());
		}
		taskCompleted = true;
		if (connNotifier != null && receiver != null) {
			appContext.unregisterReceiver(receiver);
		}
	}

	public void setAppContext(final Context appContext) {
		this.appContext = appContext;
	}

	public void setNotifier(final JobNotifier notifier) {
		this.notifier = notifier;
	}

}
