/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.jobs;

import java.io.File;
import java.io.IOException;

import toto.io.file.FileUtils;
import android.content.Context;
import android.util.Log;

/**
 * Task to back the application's database file to the sdcard Parameters
 * 
 * <pre>
 * Object[] param = new Object[4];
 * // database package
 * param[0] = "com.myapp.databasebu";
 * // database name
 * param[1] = "mydata.db";
 *  //database path
 *  param[2]="data/data/
 *  //path on the sdcard
 *  param[3]=new File("sdcard/mybackup");
 *  //execute the task
 *  new DownloadBitmapJob(this, this).execute(param);
 * </pre>
 * 
 * @author elton.kent
 * 
 */
public class ExportDatabaseFileJob extends TJob {// AsyncTask<String,
													// Void, Boolean> {
	private static final String TAG = ExportDatabaseFileJob.class
			.getSimpleName();

	public ExportDatabaseFileJob(final Context context,
			final JobNotifier notifier) {
		super(context, notifier);
	}

	// automatically done on worker thread (separate from UI thread)
	@Override
	protected TJobResponse<Boolean> doInBackground(final Object... params) {
		final String dbName = (String) params[1];
		final String dbPath = (String) params[2];
		final File dbFile = new File(dbPath + dbName);
		// path on sd by convention
		final File exportDir = (File) params[3];
		if (!exportDir.exists()) {
			// boolean result =
			exportDir.mkdirs();
			// Log.i(TAG, "create directory " + (result ? "succesful" :
			// "failed"));
		}
		final TJobResponse<Boolean> resp = new TJobResponse<Boolean>();
		final File file = new File(exportDir, dbName);

		try {
			file.createNewFile();
			FileUtils.copyFileNio(dbFile, file);
			resp.setData(true);

		} catch (final IOException e) {
			Log.e(TAG, e.getMessage(), e);
			resp.setData(false);
		}
		return resp;
	}
}
