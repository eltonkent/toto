package toto.jobs;

import android.os.AsyncTask;
import android.os.AsyncTask.Status;

public class JobUtils {

	/**
	 * Cancel an Async task
	 * 
	 * @param task
	 */
	public static void cancelAsyncTask(
			@SuppressWarnings("rawtypes") final AsyncTask task) {
		if (task != null && task.getStatus() != Status.FINISHED) {
			task.cancel(true);
		}
	}

}
