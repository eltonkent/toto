/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.media.camera;

import java.lang.reflect.Method;
import java.util.regex.Pattern;

import toto.lang.reflect.ClassUtils;
import toto.lang.reflect.MethodUtils;
import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.IBinder;
import android.view.Display;
import android.view.WindowManager;

public final class CameraUtils {

	private static final Pattern COMMA_PATTERN = Pattern.compile(",");
	private static final Object iHardwareService;
	private static final Method setFlashEnabledMethod;

	static {
		iHardwareService = getHardwareService();
		setFlashEnabledMethod = getSetFlashEnabledMethod(iHardwareService);
	}

	public static void disableFlashlight() {
		setFlashlight(false);
	}

	public static void enableFlashlight() {
		setFlashlight(true);
	}

	private static Point findBestPreviewSizeValue(
			final CharSequence previewSizeValueString,
			final Point screenResolution) {
		int bestX = 0;
		int bestY = 0;
		int diff = Integer.MAX_VALUE;
		for (String previewSize : COMMA_PATTERN.split(previewSizeValueString)) {

			previewSize = previewSize.trim();
			final int dimPosition = previewSize.indexOf('x');
			if (dimPosition < 0) {
				continue;
			}

			int newX;
			int newY;
			try {
				newX = Integer.parseInt(previewSize.substring(0, dimPosition));
				newY = Integer.parseInt(previewSize.substring(dimPosition + 1));
			} catch (final NumberFormatException nfe) {
				continue;
			}

			final int newDiff = Math.abs(newX - screenResolution.x)
					+ Math.abs(newY - screenResolution.y);
			if (newDiff == 0) {
				bestX = newX;
				bestY = newY;
				break;
			} else if (newDiff < diff) {
				bestX = newX;
				bestY = newY;
				diff = newDiff;
			}

		}

		if (bestX > 0 && bestY > 0) {
			return new Point(bestX, bestY);
		}
		return null;
	}

	private static Point getCameraResolution(
			final Camera.Parameters parameters, final Point screenResolution) {

		String previewSizeValueString = parameters.get("preview-size-values");
		// saw this on Xperia
		if (previewSizeValueString == null) {
			previewSizeValueString = parameters.get("preview-size-value");
		}

		Point cameraResolution = null;

		if (previewSizeValueString != null) {
			cameraResolution = findBestPreviewSizeValue(previewSizeValueString,
					screenResolution);
		}

		if (cameraResolution == null) {
			// Ensure that the camera resolution is a multiple of 8, as the
			// screen may not be.
			cameraResolution = new Point((screenResolution.x >> 3) << 3,
					(screenResolution.y >> 3) << 3);
		}

		return cameraResolution;
	}

	/**
	 * Get the resolution of the device camera relevant to the screen resolution
	 * 
	 * @param context
	 * @return
	 */
	public static Point getCameraResolution(final Context context) {
		Camera cam = Camera.open();
		if (cam == null)
			return null;
		final Camera.Parameters params = cam.getParameters();
		final WindowManager manager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		final Display display = manager.getDefaultDisplay();
		final Point screenResolution = new Point(display.getWidth(),
				display.getHeight());
		final Point cameraResolution = getCameraResolution(params,
				screenResolution);
		cam.release();
		cam = null;
		return cameraResolution;
	}

	private static Object getHardwareService() {
		final Class<?> serviceManagerClass = ClassUtils
				.getClassSilent("android.os.ServiceManager");
		if (serviceManagerClass == null) {
			return null;
		}

		final Method getServiceMethod = MethodUtils.getAccessibleMethod(
				serviceManagerClass, "getService", String.class);
		if (getServiceMethod == null) {
			return null;
		}

		final Object hardwareService = MethodUtils.invokeSilent(null,
				getServiceMethod, "hardware");
		if (hardwareService == null) {
			return null;
		}

		final Class<?> iHardwareServiceStubClass = ClassUtils
				.getClassSilent("android.os.IHardwareService$Stub");
		if (iHardwareServiceStubClass == null) {
			return null;
		}

		final Method asInterfaceMethod = MethodUtils.getAccessibleMethod(
				iHardwareServiceStubClass, "asInterface", IBinder.class);
		if (asInterfaceMethod == null) {
			return null;
		}

		return MethodUtils.invokeSilent(null, asInterfaceMethod,
				hardwareService);
	}

	private static Method getSetFlashEnabledMethod(final Object iHardwareService) {
		if (iHardwareService == null) {
			return null;
		}
		final Class<?> proxyClass = iHardwareService.getClass();
		return MethodUtils.getAccessibleMethod(proxyClass,
				"setFlashlightEnabled", boolean.class);
	}

	private static void setFlashlight(final boolean active) {
		if (iHardwareService != null) {
			MethodUtils.invokeSilent(iHardwareService, setFlashEnabledMethod,
					active);
		}
	}
}
