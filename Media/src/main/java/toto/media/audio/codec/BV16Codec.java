package toto.media.audio.codec;

import com.mi.toto.ToTo;

/**
 * <a href="https://www.broadcom.com/support/broadvoice/">Broadcom voice 16</a>
 * codec.
 */
public class BV16Codec extends BaseAudioCodec implements AudioCodec {

	public BV16Codec() {
		CODEC_NAME = "BV16";
		CODEC_USER_NAME = "BV16";
		CODEC_DESCRIPTION = "16kbit";
		CODEC_NUMBER = 106;
		CODEC_DEFAULT_SETTING = "always";
	}

	public native int open();

	public native int decode(byte encoded[], short lin[], int size);

	public native int encode(short lin[], int offset, byte encoded[], int size);

	public native void close();

	@Override
	public String codecName() {
		return "BroadcomVoice-16";
	}

	public void init() {
		open();
	}

	static {
		ToTo.loadLib("Audio_BV16Codec");
	}
}
