package toto.media.audio.codec;

/**
 * Represents an audio codec interface.
 */
public interface AudioCodec {

	/**
	 * Decode a linear pcm audio stream
	 * 
	 * @param encoded
	 *            The encoded audio stream
	 * @param lin
	 *            The linear pcm audio frame buffer in which to place the
	 *            decoded stream
	 * @param size
	 *            The size of the encoded frame
	 * @returns The size of the decoded frame
	 */
	int decode(byte encoded[], short lin[], int size);

	/**
	 * Encode a linear pcm audio stream
	 * 
	 * @param lin
	 *            The linear stream to encode
	 * @param offset
	 *            The offset into the linear stream to begin
	 * @returns the length (in bytes) of the encoded stream
	 */
	int encode(short lin[], int offset, byte alaw[], int frames);

	/**
	 * The sampling rate for this particular codec
	 */
	int samp_rate();

	/**
	 * The audio frame size for this particular codec
	 */
	int frame_size();

	/**
	 * Optionally used to free any resources allocated in init after encoding or
	 * decoding is complete
	 */
	void close();

	/**
	 * <p/>
	 * checks to see if the user has enabled the codec.
	 * 
	 * @returns true if the codec can be used
	 */
	boolean isEnabled();

	boolean isValid();

	/**
	 * @returns The user friendly string for the codec (should include both the
	 *          name and the bandwidth
	 */
	String getTitle();

	/**
	 * @returns The RTP assigned name string for the codec
	 */
	String name();

	String key();

	String getValue();

	/**
	 * @returns The commonly used name for the codec.
	 */
	String codecName();

	/**
	 * @returns The RTP assigned number for the codec
	 */
	int number();

}
