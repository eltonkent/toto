package toto.media.audio.codec;

/**
 * <a href="http://en.wikipedia.org/wiki/U-law">U-Law</a> algorithm.
 */
public class ULaw extends BaseAudioCodec implements AudioCodec {
	public ULaw() {
		CODEC_NAME = "PCMU";
		CODEC_USER_NAME = "PCMU";
		CODEC_DESCRIPTION = "64kbit";
		CODEC_NUMBER = 0;
		CODEC_DEFAULT_SETTING = "wlanor3g";

	}

	public int decode(byte enc[], short lin[], int frames) {
		G711Codec.ulaw2linear(enc, lin, frames);

		return frames;
	}

	public int encode(short lin[], int offset, byte enc[], int frames) {
		G711Codec.linear2ulaw(lin, offset, enc, frames);

		return frames;
	}

	public void close() {
	}

	@Override
	public String codecName() {
		return "U-Law";
	}
}
