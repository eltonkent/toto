package toto.media.audio.codec;

import com.mi.toto.ToTo;

/**
 * Skype codec <a href="http://en.wikipedia.org/wiki/SILK">silk-8</a>
 */
public class SILK8Codec extends BaseAudioCodec implements AudioCodec {

	public SILK8Codec() {
		CODEC_USER_NAME = "SILK";
		CODEC_NAME = "silk8";
		CODEC_DESCRIPTION = "6-20kbit";
		CODEC_NUMBER = 117;
		CODEC_DEFAULT_SETTING = "always";
	}

	/**
	 * open silk8 codec. default compression is 0.
	 * 
	 * <pre>
	 *                 | fs (Hz) | BR (kbps)
	 * ----------------+---------+---------
	 * Narrowband	   | 8000    | 6 -20
	 * Mediumband      | 12000   | 7 -25
	 * Wideband        | 16000   | 8 -30
	 * Super Wideband  | 24000   | 12 -40
	 * 
	 * Table 1: fs specifies the audio sampling frequency in Hertz (Hz); BR
	 * specifies the adaptive bit rate range in kilobits per second (kbps).
	 * 
	 * Complexity can be scaled to optimize for CPU resources in real-time,
	 * mostly in trade-off to network bit rate. 0 is least CPU demanding and
	 * highest bit rate.
	 * </pre>
	 * 
	 * @param compression
	 * @return
	 */
	public native int open(int compression);

	public native int decode(byte encoded[], short lin[], int size);

	public native int encode(short lin[], int offset, byte encoded[], int size);

	public native void close();

	@Override
	public String codecName() {
		return "Silk-8";
	}

	static {
		ToTo.loadLib("Audio_Silk8Codec");
	}

}
