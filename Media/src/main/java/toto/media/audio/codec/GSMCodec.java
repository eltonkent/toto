package toto.media.audio.codec;

import com.mi.toto.ToTo;

/**
 * <a href="http://en.wikipedia.org/wiki/GSM">GSM</a> codec implementation.
 */
public class GSMCodec extends BaseAudioCodec implements AudioCodec {
	public GSMCodec() {
		CODEC_NAME = "GSMCodec";
		CODEC_USER_NAME = "GSMCodec";
		CODEC_DESCRIPTION = "13kbit";
		CODEC_NUMBER = 3;
		CODEC_DEFAULT_SETTING = "always";
	}

	public native int open();

	public native int decode(byte encoded[], short lin[], int size);

	public native int encode(short lin[], int offset, byte encoded[], int size);

	public native void close();

	@Override
	public String codecName() {
		return "GSMCodec";
	}

	static {
		ToTo.loadLib("Audio_GSMCodec");
	}

}
