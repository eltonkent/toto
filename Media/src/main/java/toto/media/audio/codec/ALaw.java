package toto.media.audio.codec;

/**
 * <a href="http://en.wikipedia.org/wiki/A-law_algorithm">ALaw</a> codec
 * implementation.
 */
public class ALaw extends BaseAudioCodec implements AudioCodec {
	public ALaw() {
		CODEC_NAME = "PCMA";
		CODEC_USER_NAME = "PCMA";
		CODEC_DESCRIPTION = "64kbit";
		CODEC_NUMBER = 8;
		CODEC_DEFAULT_SETTING = "wlanor3g";

	}

	public void init() {
		G711Codec.init();
	}

	public int decode(byte enc[], short lin[], int frames) {
		G711Codec.alaw2linear(enc, lin, frames);

		return frames;
	}

	public int encode(short lin[], int offset, byte enc[], int frames) {
		G711Codec.linear2alaw(lin, offset, enc, frames);

		return frames;
	}

	public void close() {
	}

	@Override
	public String codecName() {
		return "A-Law";
	}
}
