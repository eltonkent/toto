package toto.media.audio.codec;

import com.mi.toto.ToTo;

/**
 * <a href="http://en.wikipedia.org/wiki/G.722">G722</a> codec implementation.
 */
public class G722Codec extends BaseAudioCodec implements AudioCodec {

	public G722Codec() {
		CODEC_NAME = "G722Codec HD Voice";
		CODEC_USER_NAME = "G722Codec";
		CODEC_DESCRIPTION = "64kbit";
		CODEC_NUMBER = 9;
		CODEC_DEFAULT_SETTING = "wlanor3g";
		CODEC_SAMPLE_RATE = 16000;
		CODEC_FRAME_SIZE = 320;
	}

	/**
	 * 
	 * <p>
	 * Acceptable values for bitrate are 48000, 56000 or 64000. default is 64000
	 * </p>
	 * 
	 * @param brate
	 * @return
	 */
	public native int open(int brate);

	public native int decode(byte encoded[], short lin[], int size);

	public native int encode(short lin[], int offset, byte encoded[], int size);

	public native void close();

	@Override
	public String codecName() {
		return "G722";
	}

	static {
		ToTo.loadLib("Audio_G722Codec");
	}

}
