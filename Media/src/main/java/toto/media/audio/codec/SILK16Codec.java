package toto.media.audio.codec;

import com.mi.toto.ToTo;

/**
 * skype codec <a href="http://en.wikipedia.org/wiki/SILK">silk-16</a>
 */
public class SILK16Codec extends BaseAudioCodec implements AudioCodec {

	private static final int DEFAULT_COMPLEXITY = 0;

	public SILK16Codec() {
		CODEC_USER_NAME = "SILK";
		CODEC_NAME = "silk16";
		CODEC_DESCRIPTION = "8-30kbit";
		CODEC_NUMBER = 119;
		CODEC_DEFAULT_SETTING = "wlanor3g";
		CODEC_SAMPLE_RATE = 16000;
		CODEC_FRAME_SIZE = 320;
	}

	/**
	 * <pre>
	 *                 | fs (Hz) | BR (kbps)
	 * ----------------+---------+---------
	 * Narrowband	   | 8000    | 6 -20
	 * Mediumband      | 12000   | 7 -25
	 * Wideband        | 16000   | 8 -30
	 * Super Wideband  | 24000   | 12 -40
	 * 
	 * Table 1: fs specifies the audio sampling frequency in Hertz (Hz); BR
	 * specifies the adaptive bit rate range in kilobits per second (kbps).
	 * 
	 * Complexity can be scaled to optimize for CPU resources in real-time,
	 * mostly in trade-off to network bit rate. 0 is least CPU demanding and
	 * highest bit rate.
	 * </pre>
	 * 
	 * Default is 0
	 */
	public native int open(int compression);

	public native int decode(byte encoded[], short lin[], int size);

	public native int encode(short lin[], int offset, byte encoded[], int size);

	public native void close();

	@Override
	public String codecName() {
		return "Silk-16";
	}

	static {
		ToTo.loadLib("Audio_Silk16Codec");
	}
}
