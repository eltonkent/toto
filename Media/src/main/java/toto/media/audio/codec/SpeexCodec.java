package toto.media.audio.codec;

import com.mi.toto.ToTo;

/**
 * <a href="http://en.wikipedia.org/wiki/Speex">Speex</a> audio codec
 * implementation.
 */
public class SpeexCodec extends BaseAudioCodec implements AudioCodec {

	public SpeexCodec() {
		CODEC_NAME = "speex";
		CODEC_USER_NAME = "speex";
		CODEC_DESCRIPTION = "11kbit";
		CODEC_NUMBER = 97;
		CODEC_DEFAULT_SETTING = "always";
	}

	/**
	 * Iniitalize speex using the specified compression quality value.
	 * <p>
	 * 
	 * <pre>
	 * 1 : 4kbps (very noticeable artifacts, usually intelligible)
	 * 2 : 6kbps (very noticeable artifacts, good intelligibility)
	 * 4 : 8kbps (noticeable artifacts sometimes)
	 * 6 : 11kpbs (artifacts usually only noticeable with headphones). Recommended
	 * 8 : 15kbps (artifacts not usually noticeable)
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param compression
	 * @return
	 */
	public native int open(int compression);

	public native int decode(byte encoded[], short lin[], int size);

	public native int encode(short lin[], int offset, byte encoded[], int size);

	/**
	 * Close the codec
	 */
	public native void close();

	@Override
	public String codecName() {
		return "Speex";
	}

	static {
		ToTo.loadLib("Audio_SpeexCodec");

	}
}
