/*
 * Copyright (C) 2009 The Sipdroid Open Source Project
 * 
 * This file is part of Sipdroid (http://www.sipdroid.org)
 * 
 * Sipdroid is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package toto.media.audio.codec;

import com.mi.toto.ToTo;

/**
 * skype codec <a href="http://en.wikipedia.org/wiki/SILK">silk-24</a>
 */
public class SILK24Codec extends BaseAudioCodec implements AudioCodec {

	public SILK24Codec() {
		CODEC_USER_NAME = "SILK";
		CODEC_NAME = "silk24";
		CODEC_DESCRIPTION = "12-40kbit";
		CODEC_NUMBER = 120;
		CODEC_DEFAULT_SETTING = "never";
		CODEC_SAMPLE_RATE = 24000;
		CODEC_FRAME_SIZE = 480;
	}

	/**
	 * <pre>
	 *                 | fs (Hz) | BR (kbps)
	 * ----------------+---------+---------
	 * Narrowband	   | 8000    | 6 -20
	 * Mediumband      | 12000   | 7 -25
	 * Wideband        | 16000   | 8 -30
	 * Super Wideband  | 24000   | 12 -40
	 * 
	 * Table 1: fs specifies the audio sampling frequency in Hertz (Hz); BR
	 * specifies the adaptive bit rate range in kilobits per second (kbps).
	 * 
	 * Complexity can be scaled to optimize for CPU resources in real-time,
	 * mostly in trade-off to network bit rate. 0 is least CPU demanding and
	 * highest bit rate.
	 * </pre>
	 * 
	 * Default is 0
	 */
	public native int open(int compression);

	public native int decode(byte encoded[], short lin[], int size);

	public native int encode(short lin[], int offset, byte encoded[], int size);

	public native void close();

	@Override
	public String codecName() {
		return "Silk-24";
	}

	static {
		ToTo.loadLib("Audio_Silk24Codec");
	}
}
