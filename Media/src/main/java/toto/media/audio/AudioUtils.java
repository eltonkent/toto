/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.media.audio;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import toto.io.IOUtils;
import android.content.Context;
import android.content.res.AssetManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.util.Log;
import android.media.AudioTrack;

public class AudioUtils {

	public static final int SAMPLE_RATE = 44100; // 44.1[kHz]
	public static final int PREFERED_BUFFER_SIZE = 44100;

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static double[] load16BitPCMRawDataFileAsDoubleArray(final File file) {
		InputStream in = null;
		if (file.isFile()) {
			final long size = file.length();
			try {
				in = new FileInputStream(file);
				return IOUtils.readStreamAsDoubleArray(in, size);
			} catch (final Exception e) {
			} finally {
				IOUtils.closeQuietly(in);
			}
		}
		throw new RuntimeException("couldn't read:" + file.getName());
	}

	/**
	 * Load sound file from assets
	 * <p>
	 * <b>Usage</b><br/>
	 * <code>readRawPcmDataFromAssets(getApplicationContext(),"piano.wav");</code>
	 * </code>
	 * </p>
	 * 
	 * @see AudioUtils#readRawPcmDataFromStream(InputStream)
	 * @param context
	 * @param name
	 * @return
	 * @throws IOException
	 */
	public static byte[] readRawPcmDataFromAssets(final Context context,
			final String name) throws IOException {
		final AssetManager as = context.getResources().getAssets();
		return readRawPcmDataFromStream(as.open(name));

	}

	public static byte[] readRawPcmDataFromStream(final InputStream is) {
		final byte[] buffer = new byte[PREFERED_BUFFER_SIZE];
		try {
			for (int i = 0; i < 100; i++) {
				is.read();
			}
			for (int i = 0; i < buffer.length; i++) {
				buffer[i] = (byte) (is.read());
			}
		} catch (final IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(is);
		}
		return buffer;
	}

	public static byte[] pitchShift(final byte[] org, final float orgHz,
			final float dstHz) {
		final float scale = orgHz / dstHz;
		final int len = org.length;
		final byte[] result = new byte[org.length];
		final int resultlen = (int) (len * scale);
		for (int i = 0; i < resultlen; i++) {
			final int j = Math.round(i / scale);
			if (j >= len || i >= len)
				break;
			result[i] = org[j];
		}
		return result;
	}

	public static void play(final byte[] soundData, final int buffersize) {
		final AudioTrack atAudio = new AudioTrack(AudioManager.STREAM_MUSIC,
				SAMPLE_RATE, AudioFormat.CHANNEL_CONFIGURATION_MONO,
				AudioFormat.ENCODING_PCM_8BIT, buffersize,
				AudioTrack.MODE_STATIC);
		if (atAudio.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
			atAudio.pause();
			atAudio.reloadStaticData();
		}
		Log.v("AudioUtils",
				"length:" + soundData.length + ", State:"
						+ atAudio.getPlayState());
		final int ret = atAudio.write(soundData, 0, soundData.length);
		if (ret == AudioTrack.ERROR_INVALID_OPERATION) {
			Log.d("AudioUtils", "pcmdat: object wasn't properly initialized");
		} else if (ret == AudioTrack.ERROR_BAD_VALUE) {
			Log.d("AudioUtils",
					"pcmdat: parameters don't resolve to valid data and indexes");
		} else {
			atAudio.play();
		}
	}

	// public void record(OutputStream os,int channelConfiguration) {
	// int bufferSize = AudioRecord.getMinBufferSize(frequency,
	// channelConfiguration,
	// audioEncoding);
	// AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
	// frequency,
	// channelConfiguration, audioEncoding, bufferSize);
	// recorder.startRecording();
	//
	// }
}
