/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.media;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore.Images;

public class MediaUtils {

	/**
	 * Interface for notifying clients of the result of scanning a requested
	 * media file.
	 */
	public interface OnScanCompletedListener {
		/**
		 * Called to notify the client when the media scanner has finished
		 * scanning a file.
		 * 
		 * @param path
		 *            the path to the file that has been scanned.
		 * @param uri
		 *            the Uri for the file if the scanning operation succeeded
		 *            and the file was added to the media database, or null if
		 *            scanning failed.
		 */
		public void onScanCompleted(String path, Uri uri);
	}

	/**
	 * Get all the images in SDcard/Gallery
	 * 
	 * @param context
	 */
	public List<File> getAllExternalImages(final Context context) {
		final Cursor c = context.getContentResolver().query(
				Images.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
			final List<File> imageList = new ArrayList<File>();
			while (c.isAfterLast() == false) {
				final long id = c.getLong(c.getColumnIndex(BaseColumns._ID));
				final Uri imageUri = Uri
						.parse(Images.Media.EXTERNAL_CONTENT_URI + "/" + id);
				final String uri = imageUri.toString();
				if (uri.startsWith(ContentResolver.SCHEME_CONTENT)
						|| uri.startsWith(ContentResolver.SCHEME_FILE)) {
					imageList.add(new File(uri));
				}
				c.moveToNext();
			}
			return imageList;

		}
		return null;
	}

	/**
	 * Scan a given media file and notify using callback
	 * 
	 * @see OnScanCompletedListener
	 * @param context
	 * @param paths
	 *            to scan for
	 * @param mimeTypes
	 *            to look for
	 * @param callback
	 */
	public static void scanFile(final Context context, final String[] paths,
			final String[] mimeTypes, final OnScanCompletedListener callback) {

		final ClientProxy client = new ClientProxy(paths, mimeTypes, callback);
		final MediaScannerConnection connection = new MediaScannerConnection(
				context, client);
		client.mConnection = connection;
		connection.connect();
	}

	private static final class ClientProxy implements
			MediaScannerConnectionClient {
		private final String[] mPaths;
		private final String[] mMimeTypes;
		private final OnScanCompletedListener mClient;
		private MediaScannerConnection mConnection;
		private int mNextPath;

		private ClientProxy(final String[] paths, final String[] mimeTypes,
				final OnScanCompletedListener client) {
			mPaths = paths;
			mMimeTypes = mimeTypes;
			mClient = client;
		}

		public void onMediaScannerConnected() {
			scanNextPath();
		}

		public void onScanCompleted(final String path, final Uri uri) {
			if (mClient != null) {
				mClient.onScanCompleted(path, uri);
			}
			scanNextPath();
		}

		void scanNextPath() {
			if (mNextPath >= mPaths.length) {
				mConnection.disconnect();
				return;
			}
			final String mimeType = mMimeTypes != null ? mMimeTypes[mNextPath]
					: null;
			mConnection.scanFile(mPaths[mNextPath], mimeType);
			mNextPath++;
		}
	}

}
