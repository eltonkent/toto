package toto.media.pictures;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import java.util.List;

/**
 * Created by eltonkent on 12/10/15.
 */
public class PictureUtils {

    /**
     * Get the paths of all the images on the device. This method looks at the Camera (DCIM) storage locations.
     * @param paths the list to populate the paths
     *              @param context
     */
    public static void getImagePaths(Context context
                                     ,List<String> paths){
        if(paths==null){
            return;
        }
         String[] PROJECTION = {
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.DATE_ADDED
        };
        Cursor cursorExternal = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, PROJECTION, null, null, null);
        Cursor cursorInternal = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, PROJECTION, null, null, null);
        processCursor(cursorExternal,paths);
        processCursor(cursorInternal,paths);
    }

    private  static void processCursor(Cursor cursor,List<String> imagePaths){
        if (cursor != null && cursor.moveToFirst()) {
            do {
                String pictureUri = cursor.getString(0);
                String lcPath = pictureUri.toLowerCase();
                imagePaths.add(pictureUri);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
    }
}
