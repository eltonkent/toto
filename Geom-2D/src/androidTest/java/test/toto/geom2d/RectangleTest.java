package test.toto.geom2d;

import test.toto.ToToTestCase;

public class RectangleTest extends ToToTestCase {
	
	public void testTwoRectangle(){
		
		toto.geom2d.Rectangle reg=new toto.geom2d.Rectangle(10, 10, 50, 50);
		assertEquals(7.0710678118654755, reg.distance(5, 5));
		
		toto.geom2d.Rectangle reg2=new toto.geom2d.Rectangle(15, 15, 55, 55);
		assertEquals(true, reg.intersects(reg2));
		
		toto.geom2d.Rectangle reg3=new toto.geom2d.Rectangle(15, 15, 20, 20);
		assertEquals(true,reg.contains(reg3));
	}
	
	

}

