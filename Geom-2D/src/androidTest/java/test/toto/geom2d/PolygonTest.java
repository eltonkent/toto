package test.toto.geom2d;

import test.toto.ToToTestCase;
import toto.geom2d.Polygon;

public class PolygonTest extends ToToTestCase {

	public void testTriangulate(){
		int[] sp = { 325, 272, 118, 272, 118, 98, 325, 98 };
		int[] polygon =Polygon.triangulatePolygonNative(sp);
		assertEquals(polygon.length, 12);
	}
}
