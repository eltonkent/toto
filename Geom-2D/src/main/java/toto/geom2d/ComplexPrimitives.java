/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.geom2d;

public class ComplexPrimitives {
	/**
	 * Create the geometry of an arrow. The arrow is positioned at the end (last
	 * point) of the specified polyline, as follows:
	 * 
	 * 0,4--, \ --, \ --, \ --, \ --, -------------------------3-----------1 /
	 * --' / --' / --' / --' 2--'
	 * 
	 * @param x
	 *            X coordinates of polyline of where arrow is positioned in the
	 *            end. Must contain at least two points.
	 * @param y
	 *            Y coordinates of polyline of where arrow is positioned in the
	 *            end.
	 * @param length
	 *            Length along the main axis from point 1 to the projection of
	 *            point 0.
	 * @param angle
	 *            Angle between the main axis and the line 1,0 (and 1,2) in
	 *            radians.
	 * @param inset
	 *            Specification of point 3 [0.0-1.0], 1.0 will put point 3 at
	 *            distance length from 1, 0.0 will put it at point 1.
	 * @return Array of the five coordinates [x,y,...].
	 */
	public static int[] createArrow(final int[] x, final int[] y,
			final double length, final double angle, final double inset) {
		final int[] arrow = new int[10];

		final int x0 = x[x.length - 1];
		final int y0 = y[y.length - 1];

		arrow[2] = x0;
		arrow[3] = y0;

		// Find position of interior of the arrow along the polyline
		final int[] pos1 = new int[2];
		Polygon.findPolygonPosition(x, y, length, pos1);

		// Angles
		final double dx = x0 - pos1[0];
		final double dy = y0 - pos1[1];

		// Polyline angle
		double v = dx == 0.0 ? Math.PI / 2.0 : Math.atan(Math.abs(dy / dx));

		v = (dx > 0.0) && (dy <= 0.0) ? Math.PI + v
				: (dx > 0.0) && (dy >= 0.0) ? Math.PI - v : (dx <= 0.0)
						&& (dy < 0.0) ? -v : (dx <= 0.0) && (dy > 0.0) ? +v
						: 0.0;

		final double v0 = v + angle;
		final double v1 = v - angle;

		final double edgeLength = length / Math.cos(angle);

		arrow[0] = x0 + (int) Math.round(edgeLength * Math.cos(v0));
		arrow[1] = y0 - (int) Math.round(edgeLength * Math.sin(v0));

		arrow[4] = x0 + (int) Math.round(edgeLength * Math.cos(v1));
		arrow[5] = y0 - (int) Math.round(edgeLength * Math.sin(v1));

		final double c1 = inset * length;

		arrow[6] = x0 + (int) Math.round(c1 * Math.cos(v));
		arrow[7] = y0 - (int) Math.round(c1 * Math.sin(v));

		// Close polygon
		arrow[8] = arrow[0];
		arrow[9] = arrow[1];

		return arrow;
	}

	/**
	 * Create the geometry of a sector of an ellipse.
	 * 
	 * @param x0
	 *            X coordinate of center of ellipse.
	 * @param y0
	 *            Y coordinate of center of ellipse.
	 * @param dx
	 *            X radius of ellipse.
	 * @param dy
	 *            Y radius of ellipse.
	 * @param angle0
	 *            First angle of sector (in radians).
	 * @param angle1
	 *            Second angle of sector (in radians).
	 * @return Geometry of secor [x,y,...]
	 */
	public static int[] createSector(final int x0, final int y0, final int dx,
			final int dy, final double angle0, final double angle1) {
		// Determine a sensible number of points for arc
		final double angleSpan = Math.abs(angle1 - angle0);
		final double arcDistance = Math.max(dx, dy) * angleSpan;
		final int nPoints = (int) Math.round(arcDistance / 15);
		final double angleStep = angleSpan / (nPoints - 1);

		final int[] xy = new int[nPoints * 2 + 4];

		int index = 0;
		for (int i = 0; i < nPoints; i++) {
			final double angle = angle0 + angleStep * i;
			final double x = dx * Math.cos(angle);
			final double y = dy * Math.sin(angle);

			xy[index + 0] = x0 + (int) Math.round(x);
			xy[index + 1] = y0 - (int) Math.round(y);

			index += 2;
		}

		// Start and end geometry at center of ellipse to make it a closed
		// polygon
		xy[nPoints * 2 + 0] = x0;
		xy[nPoints * 2 + 1] = y0;
		xy[nPoints * 2 + 2] = xy[0];
		xy[nPoints * 2 + 3] = xy[1];

		return xy;
	}

	/**
	 * Create geometry of a star. Integer domain.
	 * 
	 * @param x0
	 *            X center of star.
	 * @param y0
	 *            Y center of star.
	 * @param innerRadius
	 *            Inner radis of arms.
	 * @param outerRadius
	 *            Outer radius of arms.
	 * @param nArms
	 *            Number of arms.
	 * @return Geometry of star [x,y,x,y,...].
	 */
	public static int[] createStar(final int x0, final int y0,
			final int innerRadius, final int outerRadius, final int nArms) {
		final int nPoints = nArms * 2 + 1;

		final int[] xy = new int[nPoints * 2];

		final double angleStep = 2.0 * Math.PI / nArms / 2.0;

		for (int i = 0; i < nArms * 2; i++) {
			final double angle = i * angleStep;
			final double radius = (i % 2) == 0 ? innerRadius : outerRadius;

			final double x = x0 + radius * Math.cos(angle);
			final double y = y0 + radius * Math.sin(angle);

			xy[i * 2 + 0] = (int) Math.round(x);
			xy[i * 2 + 1] = (int) Math.round(y);
		}

		// Close polygon
		xy[nPoints * 2 - 2] = xy[0];
		xy[nPoints * 2 - 1] = xy[1];

		return xy;
	}

}
