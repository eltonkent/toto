package toto.geom2d;

import android.graphics.Canvas;
import android.graphics.Paint;

public abstract class T2DShape implements Cloneable {
	private String name;
	private int id;

	public void setName(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public abstract boolean contains(float x, float y);

	public abstract boolean contains(Point point);

	public abstract void draw(Canvas canvas, Paint paint);
}
