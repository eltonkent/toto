package toto.geom2d;

import java.io.Serializable;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * A convenient 2D ellipse class, based on the circle class
 * 
 * @author tonyp7
 */
public class Ellipse extends T2DShape implements Serializable {

	public float x, y;
	public float width, height;

	private static final long serialVersionUID = 7381533206532032099L;

	/** Construct a new ellipse with all values setKey to zero */
	public Ellipse() {

	}

	public Ellipse(final Ellipse ellipse) {
		this.x = ellipse.x;
		this.y = ellipse.y;
		this.width = ellipse.width;
		this.height = ellipse.height;
	}

	public Ellipse(final float x, final float y, final float width,
			final float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public boolean contains(float x, float y) {
		x = x - this.x;
		y = y - this.y;

		return (x * x) / (width * 0.5f * width * 0.5f) + (y * y)
				/ (height * 0.5f * height * 0.5f) <= 1.0f;
	}

	@Override
	public boolean contains(final Point point) {
		return contains(point.x, point.y);
	}

	/**
	 * Get the bounds of the oval
	 * 
	 * @return
	 */
	public RectF getBounds() {
		final float halfW = width / 2;
		final float halfH = height / 2;
		return new RectF(x - halfW, y - halfH, x + halfW, y + halfH);
	}

	@Override
	public void draw(final Canvas canvas, final Paint paint) {
		canvas.drawOval(getBounds(), paint);
	}

	public Point centroid() {
		return new Point((x + width) / 2, (y + height / 2));
	}

	public void set(final float x, final float y, final float width,
			final float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void set(final Ellipse ellipse) {
		x = ellipse.x;
		y = ellipse.y;
		width = ellipse.width;
		height = ellipse.height;
	}

	/**
	 * Sets the x and y-coordinates of ellipse center
	 * 
	 * @param x
	 *            The x-coordinate
	 * @param y
	 *            The y-coordinate
	 * @return this ellipse for chaining
	 */
	public Ellipse setPosition(final float x, final float y) {
		this.x = x;
		this.y = y;

		return this;
	}

	/**
	 * Sets the width and height of this ellipse
	 * 
	 * @param width
	 *            The width
	 * @param height
	 *            The height
	 * @return this ellipse for chaining
	 */
	public Ellipse setSize(final float width, final float height) {
		this.width = width;
		this.height = height;

		return this;
	}

}
