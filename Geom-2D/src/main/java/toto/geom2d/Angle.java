/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.geom2d;

import java.io.Serializable;

public class Angle implements Serializable {

	private final float anglePercent;
	/** The constant for 2*PI, equivalent to 360 degrees. */
	public final static double M_2PI = Math.PI * 2;

	/**
	 * Create an angle in radians
	 * 
	 * @param rad
	 */
	public Angle(final float rad) {
		anglePercent = (float) (rad / (2 * Math.PI));
	}

	public Angle add(final Angle a) {
		final float newAngleRad = (float) ((anglePercent + a.anglePercent) * 2 * Math.PI);
		return new Angle(newAngleRad);
	}

	public float getDegrees() {
		return (anglePercent * 360);
	}

	public float getRadians() {
		return (float) (anglePercent * 2 * Math.PI);
	}

	public Angle subtract(final Angle a) {
		final float newAngleRad = (float) ((anglePercent - a.anglePercent) * 2 * Math.PI);
		return new Angle(newAngleRad);
	}

	/**
	 * 
	 /** Returns the horizontal angle formed by the line joining the origin
	 * and the given <code>point</code>.
	 * 
	 * @param point
	 * @return
	 */
	public static float horizontalAngle(Point point) {
		return (float) ((float) (Math.atan2(point.y, point.x) + M_2PI) % (M_2PI));
	}

	/**
	 * <p>
	 * Computes the pseudo-angle of a line joining the 2 points. The
	 * pseudo-angle has same ordering property has natural angle, but is
	 * expected to be computed faster. The result is given between 0 and 360.
	 * </p>
	 * 
	 * @param p1
	 *            the initial point
	 * @param p2
	 *            the final point
	 * @return the pseudo angle of line joining p1 to p2
	 */
	public float pseudoAngle(Point p1, Point p2) {
		double dx = p2.x - p1.x;
		double dy = p2.y - p1.y;
		double s = Math.abs(dx) + Math.abs(dy);
		double t = (s == 0) ? 0.0 : dy / s;
		if (dx < 0) {
			t = 2 - t;
		} else if (dy < 0) {
			t += 4;
		}
		return (float) t * 90;
	}

	/**
	 * Returns the horizontal angle formed by the line joining the origin and
	 * the point with given coordinate.
	 */
	public static float horizontalAngle(Vector2D vect) {
		return (float) ((float) (Math.atan2(vect.y, vect.x) + M_2PI) % (M_2PI));
	}

	/**
	 * Returns the horizontal angle formed by the line joining the two given
	 * points.
	 */
	public static float horizontalAngle(Point p1, Point p2) {
		return (float) ((float) (Math.atan2(p2.y - p1.y, p2.x - p1.x) + M_2PI) % (M_2PI));
	}

	/**
	 * Returns the absolute angle between the ray formed by (p2, p1) and the ray
	 * formed by (p2, p3). Result is given in radians, between 0 and PI.
	 */
	public static float absoluteAngle(Point p1, Point p2, Point p3) {
		double angle1 = horizontalAngle(Vector2D.between(p2, p1));
		double angle2 = horizontalAngle(Vector2D.between(p2, p3));
		angle1 = (angle2 - angle1 + M_2PI) % (M_2PI);
		if (angle1 < Math.PI)
			return (float) angle1;
		else
			return (float) (M_2PI - angle1);
	}

	/**
	 * Normalize an angle (restricted input range).
	 * <p/>
	 * 
	 * @param x
	 *            the angle in degrees.
	 * @return the angle reduced to the range [&minus;180&deg;, 180&deg;).
	 *         <p/>
	 *         <i>x</i> must lie in [&minus;540&deg;, 540&deg;).
	 *         ****************
	 *         ****************************************************
	 */
	public static double normalize(final double x) {
		return x >= 180 ? x - 360 : (x < -180 ? x + 360 : x);
	}

	/**
	 * Check if angle <code>n</code> is between <code>angleA</code> and
	 * <code>angleB</code>
	 * <p/>
	 * 
	 * @param n
	 *            angle to find
	 * @param angleA
	 *            first angle
	 * @param angleB
	 *            second angle
	 * @return true if <code>n</code> is between angle<code> a</code> and angle
	 *         <code>b</code>
	 */
	public static boolean isBetween(float n, float angleA, float angleB) {
		n = (360 + (n % 360)) % 360;
		angleA = (3600000 + angleA) % 360;
		angleB = (3600000 + angleB) % 360;

		if (angleA < angleB)
			return angleA <= n && n <= angleB;
		return 0 <= n && n <= angleB || angleA <= n && n < 360;
	}

	/**
	 * Get the angle of the point in the given area marked by the provided width
	 * and height (theta).
	 * <p>
	 * Given a area(width,height), this method returns the angle at which the
	 * point(x,y) exists from the center of the area.
	 * </p>
	 * 
	 * @param x
	 *            point x
	 * @param y
	 *            point
	 * @param width
	 * @param height
	 * @return angle of <code>x,y</code>
	 */
	public static float getAngleOfPoint(final float x, final float y,
			final float width, final float height) {
		final float sx = x - (width / 2.0f);
		final float sy = y - (height / 2.0f);
		final float length = (float) Math.sqrt(sx * sx + sy * sy);
		final float nx = sx / length;
		final float ny = sy / length;
		final float theta = (float) Math.atan2(ny, nx);
		final float rad2deg = (float) (180.0 / Math.PI);
		final float theta2 = theta * rad2deg;
		return (theta2 < 0) ? theta2 + 360.0f : theta2;
	}

	public static float getAngleFromCircleArcLength(final float distance,
			final float radius) {
		return (float) ((Math.PI / 2.0) - (180.0 * distance)
				/ (Math.PI * radius));
	}

	/**
	 * Get the distance between two angles
	 * 
	 * @param angleA
	 * @param angleB
	 * @return The distance between angles a and b
	 */
	public static float angleDistance(final float angleA, final float angleB) {
		float diff = Math.abs(angleA - angleB);
		if (diff > Math.PI) {
			diff = (float) (2 * Math.PI - diff);
		}

		return diff;
	}

	static public final float radiansToDegrees = (float) (180f / Math.PI);
	static public final float degreesToRadians = (float) (Math.PI / 180);

	public static float convertToDegrees(final float rad) {
		return (float) ((rad / (2 * Math.PI)) * 360);
	}

	public static float convertToRadians(final float degrees) {
		return (float) ((degrees / 360) * 2 * Math.PI);
	}

}
