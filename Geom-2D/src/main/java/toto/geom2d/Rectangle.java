/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.geom2d;

import java.io.Serializable;

//import toto.db.odb.Value;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Class representing a 2D rectangle.
 * <p>
 * If multiple threads are accessing this object. its best to have them
 * synchornized.
 * </p>
 * 
 * @see RectangleD
 * @author Mobifluence Interactive
 * 
 */
public class Rectangle extends T2DShape implements Serializable /*,Value*/ {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1548706456869886757L;

	/**
	 * Also used for setting left
	 * 
	 * @param x
	 */
	public void setX(final float x) {
		this.x_left = x;
		calcWidthHeight();
	}

	/**
	 * Also used for setting top.
	 * 
	 * @param y
	 */
	public void setY(final float y) {
		this.y_top = y;
		calcWidthHeight();
	}

	/**
	 * The X coordinate of the top-left corner of the rectangle.
	 * 
	 * @see #setLocation(int, int)
	 * @serial the x coordinate
	 */
	protected float x_left;

	/**
	 * The Y coordinate of the top-left corner of the rectangle.
	 * 
	 * @see #setLocation(int, int)
	 * @serial the y coordinate
	 */
	protected float y_top;

	public Rectangle setWidth(final float width) {
		this.width = width;
		calcRightBottom();
		return this;
	}

	public Rectangle setHeight(final float height) {
		this.height = height;
		calcRightBottom();
		return this;
	}

	/**
	 * The width of the rectangle.
	 * 
	 * @see #setSize(float, float)
	 * @serial
	 */
	protected float width;

	/**
	 * The height of the rectangle.
	 * 
	 * @see #setSize(float, float)
	 * @serial
	 */
	protected float height;

	/**
	 * the right coordinate of the rectangle .( remember as right also known as
	 * width)
	 */
	protected float width_right;

	/**
	 * the bottom coordinate of the rectangle
	 */
	float height_bottom;

	/**
	 * must be called if there are changes ONLY to right_width ,left_x,
	 * bottom_height or top_y
	 */
	private void calcWidthHeight() {
		this.width = width_right - x_left;
		this.height = height_bottom - y_top;
	}



	public float getLeft() {
		return x_left;
	}

	public float getRight() {
		return width_right;
	}

	public float getTop() {
		return y_top;
	}

	public float getBottom() {
		return height_bottom;
	}

	/**
	 * Initializes a new instance of <code>Rectangle</code> with a top left
	 * corner at (0,0) and a width and height of 0.
	 */
	public Rectangle() {
	}

	/**
	 * Initializes a new instance of <code>Rectangle</code> from the coordinates
	 * of the specified rectangle.
	 * 
	 * @param r
	 *            the rectangle to copy from
	 * @since 1.1
	 */
	public Rectangle(final Rectangle r) {
		this.x_left = r.x_left;
		this.y_top = r.y_top;
		this.width_right = r.width_right;
		this.height_bottom = r.height_bottom;
		calcWidthHeight();

	}

	/**
	 * Initializes a new instance of <code>Rectangle</code> from the specified
	 * inputs.
	 * 
	 * @param x
	 *            the X coordinate of the top left corner
	 * @param y
	 *            the Y coordinate of the top left corner
	 * @param width
	 *            the width of the rectangle
	 * @param height
	 *            the height of the rectangle
	 */
	public Rectangle(final float x, final float y, final float width,
			final float height) {
		this.x_left = x;
		this.y_top = y;
		this.width = width;
		this.height = height;
		calcRightBottom();
	}

	public Rectangle(final int left, final int top, final int right,
			final int bottom) {
		this.y_top = top;
		this.x_left = left;
		this.height_bottom = bottom;
		this.width_right = right;
		calcWidthHeight();
	}

	/**
	 * must be called if there are changes ONLY to x_left, y_top, width, height.
	 */
	private void calcRightBottom() {
		this.width_right = x_left + width;
		this.height_bottom = y_top + height;
	}

	/**
	 * Initializes a new instance of <code>Rectangle</code> with the specified
	 * width and height. The upper left corner of the rectangle will be at the
	 * origin (0,0).
	 * 
	 * @param width
	 *            the width of the rectangle
	 * @param height
	 *            the height of the rectange
	 */
	public Rectangle(final int width, final int height) {
		this.width = width;
		this.height = height;
		calcRightBottom();
	}

	/**
	 * Get the X coordinate of the upper-left corner.
	 * 
	 * @return the value of x, as a double
	 * @see #getLeft()
	 */
	public float getX() {
		return x_left;
	}

	/**
	 * Get the Y coordinate of the upper-left corner.
	 * 
	 * @return the value of y, as a double
	 * @see #getTop()
	 */
	public float getY() {
		return y_top;
	}

	/**
	 * Calculate distance from the specified point to the rectangle
	 */
	public double distance(final float x, final float y) {
		if (x >= x_left && x <= width_right) {
			if (y >= y_top) {
				if (y <= height_bottom) {
					return 0;
				} else {
					return y - height_bottom;
				}
			} else {
				return y_top - y;
			}
		} else if (y >= y_top && y <= height_bottom) {
			if (x < x_left) {
				return x_left - x;
			} else {
				return x - width_right;
			}
		}
		final float dx = x < x_left ? x_left - x : x - width_right;
		final float dy = y < y_top ? y_top - y : y - height_bottom;
		return Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Get the width of the rectangle.
	 * 
	 * @return the value of width, as a double
	 */
	public float getWidth() {
		return width;
	}

	/**
	 * Calculates the center of the rectangle. Results are located in the given
	 * Vector2
	 * 
	 * @param vector
	 *            the Vector2 to use
	 * @return the given vector with results stored inside
	 */
	public Vector2D getCenter(final Vector2D vector) {
		vector.x = x_left + width / 2;
		vector.y = y_top + height / 2;
		return vector;
	}

	/**
	 * Sets the x and y-coordinates of the bottom left corner
	 * 
	 * @param x
	 *            The x-coordinate
	 * @param y
	 *            The y-coordinate
	 * @return this rectangle for chaining
	 */
	public Rectangle setPosition(final float x, final float y) {
		this.x_left = x;
		this.y_top = y;
		calcWidthHeight();
		return this;
	}

	/**
	 * Moves this rectangle so that its center point is located at a given
	 * position
	 * 
	 * @param x
	 *            the position's x
	 * @param y
	 *            the position's y
	 * @return this for chaining
	 */
	public Rectangle setCenter(final float x, final float y) {
		setPosition(x - width / 2, y - height / 2);
		return this;
	}

	/**
	 * Get the height of the rectangle.
	 * 
	 * @return the value of height, as a double
	 */
	public float getHeight() {
		return height;
	}

	/**
	 * Returns the bounds of this rectangle. A pretty useless method, as this is
	 * already a rectangle; it is included to mimic the <code>getBounds</code>
	 * method in Component.
	 * 
	 * @return a copy of this rectangle
	 * @see #setBounds(Rectangle)
	 * @since 1.1
	 */
	public Rectangle getBounds() {
		return new Rectangle(this);
	}

	/**
	 * Updates this rectangle to match the dimensions of the specified
	 * rectangle.
	 * 
	 * @param r
	 *            the rectangle to update from
	 * @throws NullPointerException
	 *             if r is null
	 * @see #setBounds(float, float, float, float)
	 * @since 1.1
	 */
	public void setBounds(final Rectangle r) {
		setBounds(r.x_left, r.y_top, r.width, r.height);
	}

	/**
	 * Updates this rectangle to have the specified dimensions.
	 * 
	 * @param x
	 *            the new X coordinate of the upper left hand corner
	 * @param y
	 *            the new Y coordinate of the upper left hand corner
	 * @param width
	 *            the new width of this rectangle
	 * @param height
	 *            the new height of this rectangle
	 * @since 1.1
	 */
	public void setBounds(final float x, final float y, final float width,
			final float height) {
		reshape(x, y, width, height);
	}

	/**
	 * Updates this rectangle to have the specified dimensions, rounded to the
	 * integer precision used by this class (the values are rounded "outwards"
	 * so that the stored rectangle completely encloses the specified double
	 * precision rectangle).
	 * 
	 * @param x
	 *            the new X coordinate of the upper left hand corner
	 * @param y
	 *            the new Y coordinate of the upper left hand corner
	 * @param width
	 *            the new width of this rectangle
	 * @param height
	 *            the new height of this rectangle
	 * @since 1.2
	 */
	public void setRect(final double x, final double y, final double width,
			final double height) {
		this.x_left = (int) Math.floor(x);
		this.y_top = (int) Math.floor(y);
		this.width = (int) Math.ceil(x + width) - this.x_left;
		this.height = (int) Math.ceil(y + height) - this.y_top;
		calcRightBottom();
	}

	public Point centroid() {
		return new Point((x_left + width) / 2, (y_top + height / 2));
	}

	/**
	 * Updates this rectangle to have the specified dimensions.
	 * 
	 * @param x
	 *            the new X coordinate of the upper left hand corner
	 * @param y
	 *            the new Y coordinate of the upper left hand corner
	 * @param width
	 *            the new width of this rectangle
	 * @param height
	 *            the new height of this rectangle
	 * @deprecated use {@link #setBounds(float, float, float, float)} instead
	 */
	@Deprecated
	public void reshape(final float x, final float y, final float width,
			final float height) {
		this.x_left = x;
		this.y_top = y;
		this.width = width;
		this.height = height;
		calcRightBottom();

	}

	/**
	 * Moves the location of this rectangle by setting its upper left corner to
	 * the specified coordinates.
	 * 
	 * @param x
	 *            the new X coordinate for this rectangle
	 * @param y
	 *            the new Y coordinate for this rectangle
	 * @since 1.1
	 */
	public void setLocation(final int x, final int y) {
		move(x, y);
	}

	/**
	 * Moves the location of this rectangle by setting its upper left corner to
	 * the specified coordinates.
	 * 
	 * @param x
	 *            the new X coordinate for this rectangle
	 * @param y
	 *            the new Y coordinate for this rectangle
	 * @deprecated use {@link #setLocation(int, int)} instead
	 */
	@Deprecated
	public void move(final int x, final int y) {
		this.x_left = x;
		this.y_top = y;
		calcRightBottom();
	}

	/**
	 * Join two rectangles. This rectangle is updates to contain cover of this
	 * and specified rectangle.
	 * 
	 * @param r
	 *            rectangle to be joined with this rectangle
	 */
	public final void join(final Rectangle r) {
		if (x_left > r.x_left) {
			x_left = r.x_left;
		}
		if (width_right < r.width_right) {
			width_right = r.width_right;
		}
		if (y_top > r.y_top) {
			y_top = r.y_top;
		}
		if (height_bottom < r.height_bottom) {
			height_bottom = r.height_bottom;
		}
	}

	/**
	 * Clone rectangle
	 */
	public Object clone() {
		try {
			final Rectangle r = (Rectangle) super.clone();
			r.y_top = this.y_top;
			r.x_left = this.x_left;
			r.height_bottom = this.height_bottom;
			r.width_right = this.width_right;
			r.width = this.width;
			r.height = this.height;
			return r;
		} catch (final CloneNotSupportedException e) {
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Non destructive join of two rectangles.
	 * 
	 * @param a
	 *            first joined rectangle
	 * @param b
	 *            second joined rectangle
	 * @return rectangle containing cover of these two rectangles
	 */
	public static Rectangle join(final Rectangle a, final Rectangle b) {
		final Rectangle r = new Rectangle(a);
		r.join(b);
		return r;
	}

	/**
	 * Area of covered rectangle for two sepcified rectangles
	 */
	public static long joinArea(final Rectangle a, final Rectangle b) {
		final float left = (a.x_left < b.x_left) ? a.x_left : b.x_left;
		final float right = (a.width_right > b.width_right) ? a.width_right
				: b.width_right;
		final float top = (a.y_top < b.y_top) ? a.y_top : b.y_top;
		final float bottom = (a.height_bottom > b.height_bottom) ? a.height_bottom
				: b.height_bottom;
		return (long) ((bottom - top) * (right - left));
	}

	/**
	 * Translate the location of this rectangle by the given amounts.
	 * 
	 * @param dx
	 *            the x distance to move by
	 * @param dy
	 *            the y distance to move by
	 * @see #setLocation(int, int)
	 */
	public void translate(final int dx, final int dy) {
		x_left += dx;
		y_top += dy;
		calcRightBottom();
	}

	/**
	 * Sets the size of this rectangle based on the specified dimensions.
	 * 
	 * @param width
	 *            the new width of the rectangle
	 * @param height
	 *            the new height of the rectangle
	 * @since 1.1
	 */
	public void setSize(final float width, final float height) {
		resize(width, height);
	}

	/**
	 * Sets the size of this rectangle based on the specified dimensions.
	 * 
	 * @param width
	 *            the new width of the rectangle
	 * @param height
	 *            the new height of the rectangle
	 * @deprecated use {@link #setSize(float, float)} instead
	 */
	@Deprecated
	public void resize(final float width, final float height) {
		this.width = width;
		this.height = height;
		calcRightBottom();
	}

	/**
	 * Tests whether or not the specified point is inside this rectangle.
	 * According to the contract of Shape, a point on the border is in only if
	 * it has an adjacent point inside the rectangle in either the increasing x
	 * or y direction.
	 * 
	 * @param p
	 *            the point to test
	 * @return true if the point is inside the rectangle
	 * @throws NullPointerException
	 *             if p is null
	 * @see #contains(float, float)
	 * @since 1.1
	 */
	public boolean contains(final Point p) {
		return contains(p.x, p.y);
	}

	@Override
	public void draw(final Canvas canvas, final Paint paint) {
		canvas.drawRect(x_left, y_top, this.width_right, this.height_bottom,
				paint);
	}

	/**
	 * Checks whether all points in the given rectangle are contained in this
	 * rectangle.
	 * 
	 * @param r
	 *            the rectangle to check
	 * @return true if r is contained in this rectangle
	 */
	public boolean contains(final Rectangle r) {
		return x_left <= r.x_left && y_top <= r.y_top
				&& width_right >= r.width_right
				&& height_bottom >= r.height_bottom;
	}

	public int hashCode() {
		return (int) y_top ^ ((int) height_bottom << 1) ^ ((int) x_left << 2)
				^ ((int) width_right << 3);
	}

	public boolean contains(final Line line) {
		return contains(line.x1, line.y1) && contains(line.x2, line.y2);
	}

	/**
	 * @param r
	 *            the other {@link Rectangle}
	 * @return whether this rectangle overlaps the other rectangle.
	 */
	public boolean overlaps(final Rectangle r) {
		return x_left < r.x_left + r.width && x_left + width > r.x_left
				&& y_top < r.y_top + r.height && y_top + height > r.y_top;
	}

	/**
	 * Merges this rectangle with the other rectangle.
	 * 
	 * @param rect
	 *            the other rectangle
	 * @return this rectangle for chaining
	 */
	public Rectangle merge(final Rectangle rect) {
		final float minX = Math.min(x_left, rect.x_left);
		final float maxX = Math.max(x_left + width, rect.x_left + rect.width);
		x_left = minX;
		width = maxX - minX;

		final float minY = Math.min(y_top, rect.y_top);
		final float maxY = Math.max(y_top + height, rect.y_top + rect.height);
		y_top = minY;
		height = maxY - minY;
		calcRightBottom();
		return this;
	}

	/**
	 * Calculates the aspect ratio ( width / height ) of this rectangle
	 * 
	 * @return the aspect ratio of this rectangle. Returns Float.NaN if height
	 *         is 0 to avoid ArithmeticException
	 */
	public float getAspectRatio() {
		return (height == 0) ? Float.NaN : width / height;
	}

	/**
	 * Tests whether or not the specified point is inside this rectangle.
	 * 
	 * @param x
	 *            the X coordinate of the point to test
	 * @param y
	 *            the Y coordinate of the point to test
	 * @return true if the point is inside the rectangle
	 */
	public boolean contains(final float x, final float y) {
		return width > 0 && height > 0 && x >= this.x_left
				&& x < this.x_left + width && y >= this.y_top
				&& y < this.y_top + height;
	}

	/**
	 * Rectangle area
	 */
	public final long area() {
		return (long) ((height_bottom - y_top) * (width_right - x_left));
	}

	/**
	 * Tests whether or not the specified rectangle intersects this rectangle.
	 * This means the two rectangles share at least one internal point.
	 * 
	 * @param r
	 *            the rectangle to test against
	 * @return true if the specified rectangle intersects this one
	 * @throws NullPointerException
	 *             if r is null
	 * @since 1.2
	 */
	// public boolean intersects(final Rectangle r) {
	// return r.width > 0 && r.height > 0 && width > 0 && height > 0
	// && r.x_left < x_left + width && r.x_left + r.width > x_left
	// && r.y_top < y_top + height && r.y_top + r.height > y_top;
	// }

	/**
	 * Checks if this rectangle intersects with specified rectangle
	 */
	public final boolean intersects(final Rectangle r) {
		return x_left <= r.width_right && y_top <= r.height_bottom
				&& width_right >= r.x_left && height_bottom >= r.y_top;
	}

	/**
	 * Expands the rectangle by the specified amount. The horizontal and
	 * vertical expansion values are applied both to the X,Y coordinate of this
	 * rectangle, and its width and height. Thus the width and height will
	 * increase by 2h and 2v accordingly.
	 * 
	 * @param h
	 *            the horizontal expansion value
	 * @param v
	 *            the vertical expansion value
	 */
	public Rectangle grow(final int h, final int v) {
		x_left -= h;
		y_top -= v;
		width += h + h;
		height += v + v;
		calcRightBottom();
		return this;
	}

	/**
	 * Tests whether or not this rectangle is empty. An empty rectangle has a
	 * non-positive width or height.
	 * 
	 * @return true if the rectangle is empty
	 */
	public boolean isEmpty() {
		return width <= 0 || height <= 0;
	}

	/**
	 * Fits this rectangle around another rectangle while maintaining aspect
	 * ratio This scales and centers the rectangle to the other rectangle (e.g.
	 * Having a camera translate and scale to show a given area)
	 * 
	 * @param rect
	 *            the other rectangle to fit this rectangle around
	 * @return this rectangle for chaining
	 */
	public Rectangle fitOutside(final Rectangle rect) {
		final float ratio = getAspectRatio();

		if (ratio > rect.getAspectRatio()) {
			// Wider than tall
			setSize(rect.height * ratio, rect.height);
		} else {
			// Taller than wide
			setSize(rect.width, rect.width / ratio);
		}

		setPosition((rect.x_left + rect.width / 2) - width / 2,
				(rect.y_top + rect.height / 2) - height / 2);
		return this;
	}

	/**
	 * Fits this rectangle into another rectangle while maintaining aspect
	 * ratio. This scales and centers the rectangle to the other rectangle (e.g.
	 * Scaling a texture within a arbitrary cell without squeezing)
	 * 
	 * @param rect
	 *            the other rectangle to fit this rectangle inside
	 * @return this rectangle for chaining
	 */
	public Rectangle fitInside(final Rectangle rect) {
		final float ratio = getAspectRatio();

		if (ratio < rect.getAspectRatio()) {
			// Taller than wide
			setSize(rect.height * ratio, rect.height);
		} else {
			// Wider than tall
			setSize(rect.width, rect.width / ratio);
		}

		setPosition((rect.x_left + rect.width / 2) - width / 2,
				(rect.y_top + rect.height / 2) - height / 2);
		return this;
	}

	@Override
	public String toString() {
		return "Rectangle{" + "x=" + x_left + ", y=" + y_top + ", width="
				+ width + ", height=" + height + '}';
	}
}
