/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.geom2d;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Circle extends T2DShape implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8011849731172289208L;
	float x;
	float y;
	float radius;

	public float getX() {
		return x;
	}

	public void setX(final float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(final float y) {
		this.y = y;
	}

	public Point getCenter() {
		return new Point(x, y);
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(final float radius) {
		this.radius = radius;
	}

	public Circle(final float x, final float y, final float radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public Circle(final Point center, final float radius) {
		this(center.x, center.y, radius);
	}

	/**
	 * Check if the given point is in the cicle
	 * 
	 * @param px
	 *            Point x coordinate
	 * @param py
	 *            Point y coordinate
	 * @return
	 */
	public boolean contains(final float px, final float py) {
		final double magic = Math.sqrt(Math.pow(x - px, 2)
				+ Math.pow(y - py, 2));
		return magic < radius;
	}

	public boolean contains(final Point point) {
		return contains(point.x, point.y);
	}

	@Override
	public void draw(final Canvas canvas, final Paint paint) {
		canvas.drawCircle(x, y, radius, paint);
	}

	/**
	 * Check if the circle contains the given line.
	 * 
	 * @param line
	 * @return
	 */
	public boolean contains(final Line line) {
		return contains(line.x1, line.y1) && contains(line.x2, line.y2);
	}

	/**
	 * Check if the this circle contains all four points of the rectangle.
	 * 
	 * @param rect
	 * @return
	 */
	public boolean contains(final Rectangle rect) {
		return contains(rect.x_left, rect.y_top)
				&& contains(rect.x_left + rect.width, rect.y_top)
				&& contains(rect.x_left, rect.y_top + rect.height)
				&& contains(rect.x_left + rect.width, rect.y_top + rect.height);
	}

	/**
	 * @param c
	 *            the other {@link Circle}
	 * @return whether this circle contains the other circle.
	 */
	public boolean contains(final Circle c) {
		final float dx = x - c.x;
		final float dy = y - c.y;
		// The distance to the furthest point on circle c is the distance
		// between the center of the two circles plus the radius.
		// We use the squared distance so we can avoid a sqrt.
		final float maxDistanceSqrd = dx * dx + dy * dy + c.radius * c.radius;
		return maxDistanceSqrd <= radius * radius;
	}

	/**
	 * Get the intersection points of the two circles.
	 * 
	 * @param circle
	 * @return
	 */
	public List<Point> intersection(final Circle circle) {
		final List<Point> list = new ArrayList<Point>();

		final Point center1 = getCenter();
		final Point center2 = circle.getCenter();
		final float r1 = getRadius();
		final float r2 = circle.getRadius();
		final float d = center1.distance(center2);// Point2D.distance(center1,
		// center2);

		// case of no intersection
		if (d < Math.abs(r1 - r2) || d > (r1 + r2))
			return list;

		// Angle of line from center1 to center2
		final float angle = Angle.horizontalAngle(center1, center2);

		// position of intermediate point
		final float d1 = d / 2 + (r1 * r1 - r2 * r2) / (2 * d);
		final Point tmp = Point.createPolar(center1, d1, angle);

		// Add the 2 intersection points
		final float h = (float) Math.sqrt(r1 * r1 - d1 * d1);
		list.add(Point.createPolar(tmp, h, (float) (angle + Math.PI / 2)));
		list.add(Point.createPolar(tmp, h, (float) (angle - Math.PI / 2)));
		return list;
	}

	/**
	 * @param c
	 *            the other {@link Circle}
	 * @return whether this circle overlaps the other circle.
	 */
	public boolean overlaps(final Circle c) {
		final float dx = x - c.x;
		final float dy = y - c.y;
		final float distance = dx * dx + dy * dy;
		final float radiusSum = radius + c.radius;
		return distance < radiusSum * radiusSum;
	}

	/**
	 * Check if this circle is colliding with another circle
	 * 
	 * @param c2
	 * @return true, if the two circles are colliding.
	 */
	public boolean isColliding(final Circle c2) {
		final double a = radius + c2.radius;
		final double dx = x - c2.x;
		final double dy = y - c2.y;
		return a * a > (dx * dx + dy * dy);
	}

	// /**
	// * Get the new point in a circle if the given <code>point</code> is
	// rotated
	// * by an <code>angle</code>
	// *
	// * @param point to be rotated
	// * @param angle the point should be rotated by
	// * @return The new point after rotation
	// */
	// public Point rotatePoint(final PointF point,
	// final float angle) {
	// final PointF newPoint = new PointF();
	// final float centerX = point.x - x;
	// final float centerY = point.y - y;
	// newPoint.x = (float) (Math.cos(angle) * (centerX) - Math.sin(angle) *
	// (centerY) + x);
	// newPoint.y = (float) (Math.sin(angle) * (centerX) + Math.cos(angle) *
	// (centerY) + y);
	// return newPoint;
	// }

	public static Point findPointOnCircle(final float radius,
			final float angleInDegrees, final Point origin) {
		// Convert from degrees to radians via multiplication by PI/180
		final float x = (float) (radius * Math.cos(angleInDegrees * Math.PI
				/ 180F))
				+ origin.x;
		final float y = (float) (radius * Math.sin(angleInDegrees * Math.PI
				/ 180F))
				+ origin.y;
		return new Point(x, y);
	}

	/**
	 * Check if this circle touches a rectangle
	 * 
	 * @param other
	 *            The rectangle to check against
	 * @return True if they touch
	 */
	public boolean intersects(final Rectangle other) {
		final Rectangle box = other;

		if (box.contains(x + radius, y + radius)) {
			return true;
		}

		final float x1 = box.getX();
		final float y1 = box.getY();
		final float x2 = box.getX() + box.getWidth();
		final float y2 = box.getY() + box.getHeight();

		final Line[] lines = new Line[4];
		lines[0] = new Line(x1, y1, x2, y1);
		lines[1] = new Line(x2, y1, x2, y2);
		lines[2] = new Line(x2, y2, x1, y2);
		lines[3] = new Line(x1, y2, x1, y1);

		final float r2 = getRadius() * getRadius();

		final Vector2D pos = new Vector2D(getX(), getY());

		for (int i = 0; i < 4; i++) {
			final float dis = lines[i].distanceSquared(pos);
			if (dis < r2) {
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o)
			return true;
		if (o == null || ((Object) this).getClass() != o.getClass())
			return false;

		final Circle circle = (Circle) o;

		if (Float.compare(circle.radius, radius) != 0)
			return false;
		if (Float.compare(circle.x, x) != 0)
			return false;
		if (Float.compare(circle.y, y) != 0)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
		result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
		result = 31 * result
				+ (radius != +0.0f ? Float.floatToIntBits(radius) : 0);
		return result;
	}
}
