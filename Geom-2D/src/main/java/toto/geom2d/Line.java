package toto.geom2d;

import java.io.Serializable;
import java.util.Arrays;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * 2d Line shape.
 */
public class Line extends T2DShape implements Serializable {

	float x1, y1, x2, y2;

	public Line(final float x1, final float y1, final float x2, final float y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;

	}

	/**
	 * Returns the unique intersection of two straight objects. If the
	 * intersection doesn't exist (parallel lines, short edge), return null.
	 */
	public Point intersection(final Line line2) {
		// Compute denominator, and tests its validity
		final double denom = x2 * line2.y2 - y2 * line2.x2;
		if (Math.abs(denom) < 1e-12)
			return null;

		// Compute position of intersection point
		final double t = ((y1 - line2.y1) * line2.x2 - (x1 - line2.x1)
				* line2.y2)
				/ denom;
		return new Point((float) (x1 + t * x2), (float) (y1 + t * y2));
	}

	public boolean contains(final float x, final float y) {
		return ((x - x1) * y2 - (y - y1) * x2 < 0);
	}

	@Override
	public boolean contains(final Point point) {
		return contains(point.x, point.y);
	}

	@Override
	public void draw(final Canvas canvas, final Paint paint) {
		canvas.drawLine(x1, y1, x2, y2, paint);
	}

	static final float VERY_SMALL_DISTANCE = 0.000001f;

	/**
	 * Checks if the line is vertical with the tolerance of 0.000001
	 */
	public boolean isVertical() {
		return isVertical(VERY_SMALL_DISTANCE);
	}

	/**
	 * Checks if a line is vertical.
	 */
	public boolean isVertical(final float tolerance) {
		return ((Math.abs(x1 - x2) < tolerance));
	}

	public boolean isParallelWith(final Line line2) {
		// If both lines are vertical, they are parallel
		if (isVertical(VERY_SMALL_DISTANCE)
				&& line2.isVertical(VERY_SMALL_DISTANCE)) {
			return true;
		} else // If one of them is vertical, they are not parallel
		if (isVertical() || line2.isVertical()) {
			return false;
		} else // General case. If their slopes are the same, they are parallel
		{
			return (Math.abs(getSlope() - line2.getSlope()) < VERY_SMALL_DISTANCE);
		}
	}

	/**
	 * Return the slope of a line
	 * 
	 * @return
	 */
	public double getSlope() {
		return (((y1) - y2) / ((x1) - x2));
	}

	/**
	 * The end points (x2,y2) are extended by the given length
	 * 
	 * @param toLength
	 */
	public void extend(final float toLength) {
		final float oldLength = length();
		final float lengthFraction = oldLength != 0.0f ? toLength / oldLength
				: 0.0f;
		x2 = x1 + (x2 - x1) * lengthFraction;
		y2 = y1 + (y2 - y1) * lengthFraction;
	}

	public float length() {
		final Vector2D v = createVector();
		return length(v.x, v.y);
	}

	public static float length(final float ax, final float ay, final float bx,
			final float by) {
		final Vector2D v = createVector(ax, ay, bx, by);
		return length(v.x, v.y);
	}

	/**
	 * create length from vector
	 * 
	 * @param vx
	 * @param vy
	 * @return
	 */
	private static float length(final float vx, final float vy) {
		return (float) Math.sqrt(vx * vx + vy * vy);
	}

	public static float length(final Point lineStart, final Point lineEnd) {
		final Vector2D v = createVector(lineStart, lineEnd);
		return length(v.x, v.y);
	}

	public static Vector2D createVector(final Point lineStart,
			final Point lineEnd) {
		return createVector(lineStart.x, lineStart.y, lineEnd.x, lineEnd.y);
	}

	private static Vector2D createVector(final float ax, final float ay,
			final float bx, final float by) {
		return new Vector2D(bx - ax, by - ay);
	}

	public Vector2D createVector() {
		final Vector2D vect = new Vector2D(x2 - x1, y2 - y1);
		return vect;
	}

	/**
	 * Compute the intersection between two line segments, or two lines of
	 * infinite length.
	 * 
	 * @param intersection
	 *            The intersection point is populated in this {@see Point}
	 *            object
	 * @return -1 if lines are parallel (x,y unset), -2 if lines are parallel
	 *         and overlapping (x, y center) 0 if intesrection outside segments
	 *         (x,y setKey) +1 if segments intersect (x,y setKey)
	 */
	public int intersection(final Line line, final Point intersection) {
		final float LIMIT = 1e-5f;
		final float INFINITY = 1e10f;
		float x, y;
		//
		// Convert the lines to the form y = ax + b
		//

		// Slope of the two lines
		final float a0 = equals(x1, x2, LIMIT) ? INFINITY : (y1 - y2)
				/ (x1 - x2);
		final float a1 = equals(line.x1, line.x2, LIMIT) ? INFINITY
				: (line.y1 - line.y2) / (line.x1 - line.x2);

		final float b0 = y1 - a0 * x1;
		final float b1 = line.y1 - a1 * line.x1;

		// Check if lines are parallel
		if (equals(a0, a1)) {
			if (!equals(b0, b1)) {
				return -1; // Parallell non-overlapping
			} else {
				if (equals(x1, x2)) {
					if ((Math.min(y1, y2) < Math.max(line.y1, line.y2))
							|| (Math.max(y1, y2) > Math.min(line.y1, line.y2))) {
						final float twoMiddle = y1 + y2 + line.y1 + line.y2
								- min(y1, y2, line.y1, line.y2)
								- max(y1, y2, line.y1, line.y2);
						y = (twoMiddle) / 2.0f;
						x = (y - b0) / a0;
					} else {
						return -1; // Parallell non-overlapping
					}
				} else {
					if ((Math.min(x1, x2) < Math.max(line.x1, line.x2))
							|| (Math.max(x1, x2) > Math.min(line.x1, line.x2))) {
						final float twoMiddle = x1 + x2 + line.x1 + line.x2
								- min(x1, x2, line.x1, line.x2)
								- max(x1, x2, line.x1, line.x2);
						x = (twoMiddle) / 2.0f;
						y = a0 * x + b0;
					} else {
						return -1;
					}
				}
				intersection.x = x;
				intersection.y = y;
				return -2;
			}
		}

		// Find correct intersection point
		if (equals(a0, INFINITY)) {
			x = x1;
			y = a1 * x + b1;
		} else if (equals(a1, INFINITY)) {
			x = line.x1;
			y = a0 * x + b0;
		} else {
			x = -(b0 - b1) / (a0 - a1);
			y = a0 * x + b0;
		}

		intersection.x = x;
		intersection.y = y;

		// final float x0, final float y0,
		// final float x1, final float y1, final float x2, final float y2, final
		// float x3,
		// final float y3,

		// Then check if intersection is within line segments
		float distanceFrom1;
		if (equals(x1, x2)) {
			if (y1 < y2) {
				distanceFrom1 = y < y1 ? length(new Point(x, y), new Point(x1,
						y1)) : y > y2 ? length(new Point(x, y), new Point(x2,
						y2)) : 0.0f;
			} else {
				distanceFrom1 = y < y2 ? length(new Point(x, y), new Point(x2,
						y2)) : y > y1 ? length(new Point(x, y), new Point(x1,
						y1)) : 0.0f;
			}
		} else {
			if (x1 < x2) {
				distanceFrom1 = x < x1 ? length(new Point(x, y), new Point(x1,
						y1)) : x > x2 ? length(new Point(x, y), new Point(x2,
						y2)) : 0.0f;
			} else {
				distanceFrom1 = x < x2 ? length(new Point(x, y), new Point(x2,
						y2)) : x > x1 ? length(new Point(x, y), new Point(x1,
						y1)) : 0.0f;
			}
		}

		float distanceFrom2;
		if (equals(line.x1, line.x2)) {
			if (line.y1 < line.y2) {
				distanceFrom2 = y < line.y1 ? length(new Point(x, y),
						new Point(line.x1, line.y1)) : y > line.y2 ? length(
						new Point(x, y), new Point(line.x2, line.y2)) : 0.0f;
			} else {
				distanceFrom2 = y < line.y2 ? length(new Point(x, y),
						new Point(line.x2, line.y2)) : y > line.y1 ? length(
						new Point(x, y), new Point(line.x1, line.y1)) : 0.0f;
			}
		} else {
			if (line.x1 < line.x2) {
				distanceFrom2 = x < line.x1 ? length(new Point(x, y),
						new Point(line.x1, line.y1)) : x > line.x2 ? length(
						new Point(x, y), new Point(line.x2, line.y2)) : 0.0f;
			} else {
				distanceFrom2 = x < line.x2 ? length(new Point(x, y),
						new Point(line.x2, line.y2)) : x > line.x1 ? length(
						new Point(x, y), new Point(line.x1, line.y1)) : 0.0f;
			}
		}

		return equals(distanceFrom1, 0.0f) && equals(distanceFrom2, 0.0f) ? 1
				: 0;
	}

	/**
	 * Check if two double precision numbers are "equal", i.e. close enough to a
	 * prespecified limit.
	 * 
	 * @param a
	 *            First number to check
	 * @param b
	 *            Second number to check
	 * @return True if the twho numbers are "equal", false otherwise
	 */
	private boolean equals(final float a, final float b) {
		return equals(a, b, 1.0e-5f);
	}

	/**
	 * Check if two double precision numbers are "equal", i.e. close enough to a
	 * given limit.
	 * 
	 * @param a
	 *            First number to check
	 * @param b
	 *            Second number to check
	 * @param limit
	 *            The definition of "equal".
	 * @return True if the twho numbers are "equal", false otherwise
	 */
	private boolean equals(final float a, final float b, final float limit) {
		return Math.abs(a - b) < limit;
	}

	/**
	 * Return smallest of four numbers.
	 * 
	 * @param a
	 *            First number to find smallest among.
	 * @param b
	 *            Second number to find smallest among.
	 * @param c
	 *            Third number to find smallest among.
	 * @param d
	 *            Fourth number to find smallest among.
	 * @return Smallest of a, b, c and d.
	 */
	private float min(final float a, final float b, final float c, final float d) {
		return Math.min(Math.min(a, b), Math.min(c, d));
	}

	/**
	 * Return largest of four numbers.
	 * 
	 * @param a
	 *            First number to find largest among.
	 * @param b
	 *            Second number to find largest among.
	 * @param c
	 *            Third number to find largest among.
	 * @param d
	 *            Fourth number to find largest among.
	 * @return Largest of a, b, c and d.
	 */
	private float max(final float a, final float b, final float c, final float d) {
		return Math.max(Math.max(a, b), Math.max(c, d));
	}

	/**
	 * Location of point with respect to this line
	 * 
	 * @param point
	 * @return -1 if the point is on the left of the line, 1 if the point is on
	 *         the right of the line, 0 if the point lies on the line
	 */
	public int locationOfPoint(final Point point) {
		float px = point.x;
		float py = point.y;
		x2 -= x1;
		y2 -= y1;
		px -= x1;
		py -= y1;
		final float ccw = px * y2 - py * x2;
		return ccw < 0.0 ? -1 : ccw > 0.0 ? 1 : 0;
	}

	/**
	 * Check if two points are on the same side of a given line. Algorithm from
	 * Sedgewick page 350
	 * 
	 * @param point1
	 * @param point2
	 * @return < 0 if points on opposite sides. =0 if one of the points is
	 *         exactly on the line >0 if points on same side.
	 */
	public int sameSide(final Point point1, final Point point2) {
		int sameSide = 0;

		final float dx = x2 - x1;
		final float dy = y2 - y1;
		final float dx1 = point1.x - x1;
		final float dy1 = point1.y - y1;
		final float dx2 = point2.x - x2;
		final float dy2 = point2.y - y2;

		// Cross product of the vector from the endpoint of the line to the
		// point
		final float c1 = dx * dy1 - dy * dx1;
		final float c2 = dx * dy2 - dy * dx2;

		if ((c1 != 0) && (c2 != 0)) {
			sameSide = (c1 < 0) != (c2 < 0) ? -1 : 1;
		} else if ((dx == 0) && (dx1 == 0) && (dx2 == 0)) {
			sameSide = !isBetween(y1, y2, point1.y)
					&& !isBetween(y1, y2, point2.y) ? 1 : 0;
		} else if ((dy == 0) && (dy1 == 0) && (dy2 == 0)) {
			sameSide = !isBetween(x1, x2, point1.x)
					&& !isBetween(x1, x2, point2.x) ? 1 : 0;
		}
		return sameSide;
	}

	/**
	 * Get the shortest distance squared from a point to this line
	 * 
	 * @param point
	 *            The point from which we want the distance
	 * @return The distance squared from the line to the point
	 */
	public float distanceSquared(final Vector2D point) {
		final Vector2D closest = new Vector2D(0, 0);
		getClosestPoint(point, closest);
		closest.sub(point.x, point.y);
		return closest.len2();

	}

	/**
	 * Get the closest point on the line to a given point
	 * 
	 * @param point
	 *            The point which we want to project
	 * @param result
	 *            The point on the line closest to the given point
	 */
	public void getClosestPoint(final Vector2D point, final Vector2D result) {
		final Vector2D loc = new Vector2D(point);
		loc.sub(x1, y1);
		final float dx = (x2 - x1);
		final float dy = (y2 - y1);
		final Vector2D vec = new Vector2D(dx, dy);
		float projDistance = vec.dot(loc);

		projDistance /= vec.len2();

		if (projDistance < 0) {
			result.set(x1, y1);
			return;
		}
		if (projDistance > 1) {
			result.set(x2, y2);
			return;
		}

		result.x = x1 + projDistance * vec.x;
		result.y = y1 + projDistance * vec.y;
	}

	/**
	 * Return true if c is between a and b.
	 */
	private static boolean isBetween(final float a, final float b, final float c) {
		return b > a ? (c >= a) && (c <= b) : (c >= b) && (c <= a);
	}

	/**
	 * Removes unnecessary vertices from a line strip. Uses the Ramer Douglas
	 * Peucker algorithm
	 * 
	 * @param input
	 *            in x,y,x,y format
	 * @param maxD
	 *            The maximum distance a point from the input setKey will be
	 *            from the output shape
	 * @param loop
	 *            <code>true</code> for a line loop rather than a strip
	 * @return a decimated vertex array, in x,y,x,y... format
	 */
	public static float[] decimate(final float[] input, final float maxD,
			final boolean loop) {
		final boolean[] marked = new boolean[input.length / 2];
		Arrays.fill(marked, false);

		final int end = loop ? marked.length : marked.length - 1;

		rdp(input, marked, maxD, 0, end);

		// build output list
		int count = 0;
		for (int i = 0; i < marked.length; i++) {
			if (marked[i]) {
				count++;
			}
		}

		final float[] output = new float[count * 2];
		int index = 0;
		for (int i = 0; i < marked.length; i++) {
			if (marked[i]) {
				output[index++] = input[2 * i];
				output[index++] = input[2 * i + 1];
			}
		}
		return output;
	}

	/**
	 * Recursive step. Finds the point furthest from the start-end segment,
	 * divide and conquer on that vertex if it is outside the tolerance value
	 * 
	 * @param verts
	 * @param marked
	 * @param maxD
	 * @param start
	 * @param end
	 */
	private static void rdp(final float[] verts, final boolean[] marked,
			final float maxD, final int start, final int end) {
		marked[start % marked.length] = true;
		marked[end % marked.length] = true;

		// find furthest from line
		float maxDistance = -1;
		int maxIndex = -1;

		final float ax = verts[2 * start % verts.length];
		final float ay = verts[(2 * start + 1) % verts.length];
		final float bx = verts[2 * end % verts.length];
		final float by = verts[(2 * end + 1) % verts.length];

		for (int i = start + 1; i < end; i++) {
			final float px = verts[2 * i % verts.length];
			final float py = verts[(2 * i + 1) % verts.length];

			final float d = distance(ax, ay, bx, by, px, py);

			if ((d > maxD) && (d > maxDistance)) {
				maxDistance = d;
				maxIndex = i;
			}
		}

		if (maxIndex != -1) {
			// recurse
			rdp(verts, marked, maxD, start, maxIndex);
			rdp(verts, marked, maxD, maxIndex, end);
		}
	}

	private static float distance(final float x1, final float y1,
			final float x2, final float y2, final float x, final float y) {
		float vx = x2 - x1;
		float vy = y2 - y1;
		final float wx = x - x1;
		final float wy = y - y1;

		final double c1 = wx * vx + wy * vy;
		final double c2 = vx * vx + vy * vy;

		if (c1 <= 0) {
			return (float) Math.hypot((x1 - x), (y1 - y));
		}
		if (c1 >= c2) {
			return (float) Math.hypot(x2 - x, (y2 - y));
		}

		final double b = c1 / c2;
		vx *= b;
		vy *= b;

		vx += x1;
		vy += y1;

		return (float) Math.hypot((vx - x), (vy - y));
	}

	public float distance(final float x, final float y) {
		return distance(x1, y1, x2, y2, x, y);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o)
			return true;
		if (o == null || ((Object) this).getClass() != o.getClass())
			return false;

		final Line line = (Line) o;

		if (Float.compare(line.x1, x1) != 0)
			return false;
		if (Float.compare(line.x2, x2) != 0)
			return false;
		if (Float.compare(line.y1, y1) != 0)
			return false;
		if (Float.compare(line.y2, y2) != 0)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (x1 != +0.0f ? Float.floatToIntBits(x1) : 0);
		result = 31 * result + (y1 != +0.0f ? Float.floatToIntBits(y1) : 0);
		result = 31 * result + (x2 != +0.0f ? Float.floatToIntBits(x2) : 0);
		result = 31 * result + (y2 != +0.0f ? Float.floatToIntBits(y2) : 0);
		return result;
	}

	/**
	 * Distance to point .
	 * 
	 * @param point
	 * @return The distance from the point to this line segment.
	 */
	public float distance(final Point point) {
		return distance(point.x, point.y);
	}

	public void rotate(final Point origin, final float angle) {
		final float cos = (float) Math.cos((Math.PI * angle) / 180);
		final float sin = (float) Math.sin((Math.PI * angle) / 180);

	}

	public Point getStart() {
		;
		return new Point(x1, y1);
	}

	public Point getEnd() {
		;
		return new Point(x2, y2);
	}

	/**
	 * Rotates a Line2D object around a Point2D object.
	 * <p/>
	 * the line to rotate
	 * 
	 * @param origin
	 *            the point around which to rotate
	 * @param cos
	 *            the cosine of the rotation angle
	 * @param sin
	 *            the sine of the rotation angle
	 */
	private void rotate(final Point origin, final float cos, final float sin) {
		Point test = Point.rotate(getStart(), origin, cos, sin);
		x1 = test.x;
		y1 = test.y;
		test = Point.rotate(getEnd(), origin, cos, sin);
		x2 = test.x;
		y2 = test.y;
	}

	/**
	 * Translate (move) a Line2D object on the X and Y axes.
	 * 
	 * @param dx
	 *            the X-axis offset
	 * @param dy
	 *            the Y-axis offset
	 */
	public void translate(final float dx, final float dy) {
		x1 += dx;
		y1 += dy;
		x2 += dx;
		y2 += dy;
	}

	public boolean intersects(final Rectangle rect) {
		// Is one of the line endpoints inside the rectangle

		if (rect.contains(getStart()) || rect.contains(getEnd())) {
			return true;
		}

		// If it intersects it goes through. Need to check three sides only.

		// Check against top rectangle line
		if (intersects(new Line(rect.x_left, rect.y_top, rect.width,
				rect.height))) { // lx0,
			// ly0,
			// lx1,
			// ly1,
			// x0,
			// y0,
			// x1,
			// y0))
			// {
			return true;
		}

		// Check against left rectangle line
		if (intersects(new Line(rect.x_left, rect.y_top, rect.x_left, y1))) {
			return true;
		}

		// Check against bottom rectangle line
		if (intersects(new Line(rect.x_left, y1, x1, y1))) {
			return true;
		}

		return false;
	}

	/**
	 * Check if this line intersects another line
	 * 
	 * @param line
	 */
	public boolean intersects(final Line line) {
		final float s1 = sameSide(x1, y1, x2, y2, line.x1, line.y1, line.x2,
				line.y2);
		final float s2 = sameSide(line.x1, line.y1, line.x2, line.y2, x1, y1,
				x2, y2);
		return (s1 <= 0) && (s2 <= 0);
	}

	/**
	 * Check if two points are on the same side of a given line. Algorithm from
	 * Sedgewick page 350.
	 * 
	 * @param x0
	 *            , y0, x1, y1 The line.
	 * @param px0
	 *            , py0 First point.
	 * @param px1
	 *            , py1 Second point.
	 * @return <0 if points on opposite sides. =0 if one of the points is
	 *         exactly on the line >0 if points on same side.
	 */
	public static int sameSide(final float x0, final float y0, final float x1,
			final float y1, final float px0, final float py0, final float px1,
			final float py1) {
		int sameSide = 0;

		final float dx = x1 - x0;
		final float dy = y1 - y0;
		final float dx1 = px0 - x0;
		final float dy1 = py0 - y0;
		final float dx2 = px1 - x1;
		final float dy2 = py1 - y1;

		// Cross product of the vector from the endpoint of the line to the
		// point
		final float c1 = dx * dy1 - dy * dx1;
		final float c2 = dx * dy2 - dy * dx2;

		if ((c1 != 0) && (c2 != 0)) {
			sameSide = (c1 < 0) != (c2 < 0) ? -1 : 1;
		} else if ((dx == 0) && (dx1 == 0) && (dx2 == 0)) {
			sameSide = !isBetween(y0, y1, py0) && !isBetween(y0, y1, py1) ? 1
					: 0;
		} else if ((dy == 0) && (dy1 == 0) && (dy2 == 0)) {
			sameSide = !isBetween(x0, x1, px0) && !isBetween(x0, x1, px1) ? 1
					: 0;
		}

		return sameSide;
	}

	/**
	 * return -1 if point is on left, 1 if on right
	 * 
	 * @param ax
	 * @param ay
	 * @param bx
	 * @param by
	 * @param px
	 * @param py
	 * @return -1 if the point is on the left of the line, 1 if the point is on
	 *         the right of the line, 0 if the point lies on the line
	 */
	public static int relativeCCW(final float ax, final float ay, float bx,
			float by, float px, float py) {
		bx -= ax;
		by -= ay;
		px -= ax;
		py -= ay;
		final float ccw = px * by - py * bx;

		return ccw < 0.0 ? -1 : ccw > 0.0 ? 1 : 0;
	}

	public Point centroid() {
		return midPoint();
	}

	public Vector2D getVector() {
		return Vector2D.between(getStart(), getEnd());
	}

	/**
	 * Location a point with the given fraction from the start point.
	 * 
	 * @param fractionFromStart
	 * @return
	 */
	public Point computePoint(final float fractionFromStart) {
		final Point point = new Point();
		point.x = x1 + fractionFromStart * (x2 - x1);
		point.y = y1 + fractionFromStart * (y2 - y1);
		return point;
	}

	/**
	 * Compute the midpoint
	 * 
	 * @return
	 */
	public Point midPoint() {
		return new Point((x1 + x2) / 2, (y1 + y2) / 2);
	}
}
