/**
 * 2D Geometric primitive utilities. Indispensable routines that drive the math behind  games and custom widgets.
 * @author ekent4
 *
 */
package toto.geom2d;