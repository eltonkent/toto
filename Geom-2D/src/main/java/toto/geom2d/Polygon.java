package toto.geom2d;

import java.io.Serializable;
import java.util.ArrayList;

import com.mi.toto.ToTo;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;

/**
 * Geometric 2D polygon
 * 
 * @author ekent4
 */
public class Polygon extends T2DShape implements Serializable {
	private final ArrayList<Float> x = new ArrayList<Float>();
	private final ArrayList<Float> y = new ArrayList<Float>();

	// number of points (don't rely on array size)
	private final int mTotalPoints;

	// bounding box
	private float mBoundTop = -1;
	private float mBoundBottom = -1;
	private float mBoundLeft = -1;
	private float mBoundRight = -1;

	/**
	 * Create polygon with comma-separated xy points
	 * <p>
	 * <p/>
	 * </p>
	 * 
	 * @param points
	 */
	public Polygon(final String points) {
		final String[] v = points.split(",");

		for (int i = 0; i < v.length - 1; i += 2) {
			final float x = Float.parseFloat(v[i]);
			final float y = Float.parseFloat(v[i + 1]);
			this.x.add(x);
			this.y.add(y);
			calcBounds(x, y);
		}
		mTotalPoints = x.size();
		// add point zero to the end to make
		// computing area and centroid easier
		closePolygon();
	}

	public void addPoint(final float px, final float py) {
		openPolygon();
		x.add(px);
		y.add(py);
		calcBounds(px, py);
		closePolygon();
	}

	public void addPoint(final Point point) {
		addPoint(point.x, point.y);
	}

	private void calcBounds(final float x, final float y) {
		mBoundTop = (mBoundTop == -1) ? y : Math.min(mBoundTop, y);
		mBoundBottom = (mBoundBottom == -1) ? y : Math.max(mBoundBottom, y);
		mBoundLeft = (mBoundLeft == -1) ? x : Math.min(mBoundLeft, x);
		mBoundRight = (mBoundRight == -1) ? x : Math.max(mBoundRight, x);
	}

	private void openPolygon() {
		if (x.size() > 0) {
			x.remove(x.size() - 1);
			y.remove(y.size() - 1);
		}
	}

	private void closePolygon() {
		if (x.size() > 0) {
			x.add(x.get(0));
			y.add(y.get(0));
		}
	}

	public Polygon(final float[] xPoints, final float[] yPoints) {
		if (xPoints.length != yPoints.length) {
			throw new IllegalArgumentException(
					"xPoints.length dont match yPoints.length");
		}
		for (int i = 0; i < xPoints.length; i++) {
			x.add(xPoints[i]);
			y.add(yPoints[i]);
			calcBounds(xPoints[i], yPoints[i]);
		}
		mTotalPoints = x.size();
		closePolygon();
	}

	// public PointF getCenterOfGravity() {
	// return PolygonUtils.computeCentreOfGravity(x, y);
	// }

	/**
	 * Get the area of the polygon
	 * 
	 * @return
	 */
	public float getArea() {
		return getAreaInternal();
	}

	private float getAreaInternal() {
		final int n = x.size();

		float area = 0.0f;
		for (int i = 0; i < n - 1; i++) {
			area += (x.get(i) * y.get(i + 1)) - (x.get(i + 1) * y.get(i));
		}
		area += (x.get(n - 1) * y.get(0)) - (x.get(0) * y.get(n - 1));

		area *= 0.5;

		return area;
	}

	public boolean contains(final float x, final float y) {
		return containsInternal(x, y);
	}

	/**
	 * Moves a Polygon (with respect to it's origin) to specified point.
	 * 
	 * @param point
	 *            the point to move to
	 */
	public void moveTo(final PointF point) {
		translate(point.x, point.y);
	}

	public void rotate(final float ox, final float oy, final float cos,
			final float sin) {
		Point temp;
		for (int i = 0; i < x.size(); i++) {
			temp = Point.rotate(x.get(i), y.get(i), ox, oy, cos, sin);
			x.set(i, temp.x);
			y.set(i, temp.y);
		}
	}

	Path path;

	/**
	 * Get the path of this polygon
	 * 
	 * @return
	 */
	public Path getPath() {
		if (path == null) {
			path = new Path();
		}
		path.reset();
		if (x.size() > 1) {
			path.moveTo(x.get(0), y.get(0));
			for (int i = 1; i < x.size(); i++) {
				path.lineTo(x.get(i), y.get(i));
			}
			path.close();
		}
		return path;
	}

	@Override
	public boolean contains(final Point point) {
		return contains(point.x, point.y);
	}

	@Override
	public void draw(final Canvas canvas, final Paint paint) {
		canvas.drawPath(getPath(), paint);
	}

	/**
	 * Check if a given point is inside a given (complex) polygon.
	 * 
	 * @param px
	 * @param py
	 * @return True if the given point is inside the polygon, false otherwise.
	 * @throws IllegalArgumentException
	 *             if <code>x</code> or <code>y</code> is null or their array
	 *             lengths do not match
	 */
	private boolean containsInternal(final float px, final float py) {
		boolean isInside = false;
		final int nPoints = x.size();

		int j = 0;
		for (int i = 0; i < nPoints; i++) {
			j++;
			if (j == nPoints) {
				j = 0;
			}

			if (((y.get(i) < py) && (y.get(j) >= py))
					|| ((y.get(j) < py) && (y.get(i) >= py))) {
				if (x.get(i) + (py - y.get(i)) / (y.get(j) - y.get(i))
						* (x.get(j) - x.get(i)) < px) {
					isInside = !isInside;
				}
			}
		}

		return isInside;
	}

	/**
	 * Translate polygon
	 * 
	 * @param dx
	 * @param dy
	 */
	public void translate(final float dx, final float dy) {
		Point temp;
		final int size = x.size();
		for (int i = 0; i < size; i++) {
			temp = new Point(x.get(i), y.get(i));
			temp.translate(dx, dy);
			x.set(i, temp.x);
			y.set(i, temp.y);
		}
	}

	/**
	 * Compute centorid (center of gravity) of specified polygon.
	 * 
	 * @return Centroid [x,y] of specified polygon.
	 */
	public Point centroid() {
		float cx = 0.0f;
		float cy = 0.0f;

		final int n = x.size();
		for (int i = 0; i < n - 1; i++) {
			final float a = x.get(i) * y.get(i + 1) - x.get(i + 1) * y.get(i);
			cx += (x.get(i) + x.get(i + 1)) * a;
			cy += (y.get(i) + y.get(i + 1)) * a;
		}
		final float a = x.get(n - 1) * y.get(0) - x.get(0) * y.get(n - 1);
		cx += (x.get(n - 1) + x.get(0)) * a;
		cy += (y.get(n - 1) + y.get(0)) * a;

		final float area = getArea();

		cx /= 6 * area;
		cy /= 6 * area;

		return new Point(cx, cy);
	}



	/**
	 * Return the x,y position at distance "length" into the given polyline.
	 * 
	 * @param x
	 *            X coordinates of polyline
	 * @param y
	 *            Y coordinates of polyline
	 * @param length
	 *            Requested position
	 * @param position
	 *            Preallocated to int[2]
	 * @return True if point is within polyline, false otherwise
	 */
	public static boolean findPolygonPosition(final int[] x, final int[] y,
			final double length, final int[] position) {
		if (length < 0) {
			return false;
		}

		double accumulatedLength = 0.0;
		for (int i = 1; i < x.length; i++) {
			final double legLength = Line.length(new Point(x[i - 1], y[i - 1]),
					new Point(x[i], y[i]));
			if (legLength + accumulatedLength >= length) {
				final double part = length - accumulatedLength;
				final double fraction = part / legLength;
				position[0] = (int) Math.round(x[i - 1] + fraction
						* (x[i] - x[i - 1]));
				position[1] = (int) Math.round(y[i - 1] + fraction
						* (y[i] - y[i - 1]));
				return true;
			}

			accumulatedLength += legLength;
		}

		// Length is longer than polyline
		return false;
	}

}
