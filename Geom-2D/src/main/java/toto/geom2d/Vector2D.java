package toto.geom2d;

import java.io.Serializable;

/**
 * 2D Vector
 * 
 * @see toto.geom3d.Vector3D
 */
public class Vector2D implements Serializable {
	private static final long serialVersionUID = 913902788239530931L;

	public final static Vector2D X = new Vector2D(1, 0);
	public final static Vector2D Y = new Vector2D(0, 1);
	public final static Vector2D Zero = new Vector2D(0, 0);

	/**
	 * the x-component of this vector *
	 */
	public float x;
	/**
	 * the y-component of this vector *
	 */
	public float y;

	/**
	 * Constructs a new vector at (0,0)
	 */
	public Vector2D() {
	}

	/**
	 * Constructs a vector with the given components
	 * 
	 * @param x
	 *            The x-component
	 * @param y
	 *            The y-component
	 */
	public Vector2D(final float x, final float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Constructs a vector between points
	 * 
	 * @param point1
	 * @param point2
	 * @return
	 */
	public static Vector2D between(final Point point1, final Point point2) {
		return new Vector2D(point2.x - point1.x, point2.y - point1.y);
	}

	/**
	 * Constructs a vector from the given vector
	 * 
	 * @param v
	 *            The vector
	 */
	public Vector2D(final Vector2D v) {
		set(v);
	}

	/**
	 * @return a copy of this vector
	 */
	public Vector2D cpy() {
		return new Vector2D(this);
	}

	/**
	 * @return The euclidian length
	 */
	public float len() {
		return (float) Math.sqrt(x * x + y * y);
	}

	/**
	 * @return The squared euclidian length
	 */
	public float len2() {
		return x * x + y * y;
	}

	/**
	 * Sets this vector from the given vector
	 * 
	 * @param v
	 *            The vector
	 * @return This vector for chaining
	 */
	public Vector2D set(final Vector2D v) {
		x = v.x;
		y = v.y;
		return this;
	}

	/**
	 * Sets the components of this vector
	 * 
	 * @param x
	 *            The x-component
	 * @param y
	 *            The y-component
	 * @return This vector for chaining
	 */
	public Vector2D set(final float x, final float y) {
		this.x = x;
		this.y = y;
		return this;
	}

	/**
	 * Subtracts the given vector from this vector.
	 * 
	 * @param v
	 *            The vector
	 * @return This vector for chaining
	 */
	public Vector2D subtract(final Vector2D v) {
		x -= v.x;
		y -= v.y;
		return this;
	}

	/**
	 * Normalizes this vector. Does nothing if it is zero.
	 * 
	 * @return This vector for chaining
	 */
	public Vector2D normalize() {
		final float len = len();
		if (len != 0) {
			x /= len;
			y /= len;
		}
		return this;
	}

	/**
	 * Adds the given vector to this vector
	 * 
	 * @param v
	 *            The vector
	 * @return This vector for chaining
	 */
	public Vector2D add(final Vector2D v) {
		x += v.x;
		y += v.y;
		return this;
	}

	/**
	 * Adds the given components to this vector
	 * 
	 * @param x
	 *            The x-component
	 * @param y
	 *            The y-component
	 * @return This vector for chaining
	 */
	public Vector2D add(final float x, final float y) {
		this.x += x;
		this.y += y;
		return this;
	}

	/**
	 * @param v
	 *            The other vector
	 * @return The dot product between this and the other vector
	 */
	public float dot(final Vector2D v) {
		return x * v.x + y * v.y;
	}

	/**
	 * Multiplies this vector by a scalar
	 * 
	 * @param scalar
	 *            The scalar
	 * @return This vector for chaining
	 */
	public Vector2D scl(final float scalar) {
		x *= scalar;
		y *= scalar;
		return this;
	}

	/**
	 * @deprecated Use {@link #scl(float)} instead.
	 */
	public Vector2D mul(final float scalar) {
		return scl(scalar);
	}

	/**
	 * Multiplies this vector by a scalar
	 * 
	 * @return This vector for chaining
	 */
	public Vector2D scl(final float x, final float y) {
		this.x *= x;
		this.y *= y;
		return this;
	}

	/**
	 * @deprecated Use {@link #scl(float, float)} instead.
	 */
	public Vector2D mul(final float x, final float y) {
		return scl(x, y);
	}

	/**
	 * Multiplies this vector by a vector
	 * 
	 * @return This vector for chaining
	 */
	public Vector2D scl(final Vector2D v) {
		this.x *= v.x;
		this.y *= v.y;
		return this;
	}

	public Vector2D mul(final Vector2D v) {
		return scl(v);
	}

	public Vector2D div(final float value) {
		return this.scl(1 / value);
	}

	public Vector2D div(final float vx, final float vy) {
		return this.scl(1 / vx, 1 / vy);
	}

	public Vector2D div(final Vector2D other) {
		return this.scl(1 / other.x, 1 / other.y);
	}

	/**
	 * @param v
	 *            The other vector
	 * @return the distance between this and the other vector
	 */
	public float dst(final Vector2D v) {
		final float x_d = v.x - x;
		final float y_d = v.y - y;
		return (float) Math.sqrt(x_d * x_d + y_d * y_d);
	}

	/**
	 * @param x
	 *            The x-component of the other vector
	 * @param y
	 *            The y-component of the other vector
	 * @return the distance between this and the other vector
	 */
	public float dst(final float x, final float y) {
		final float x_d = x - this.x;
		final float y_d = y - this.y;
		return (float) Math.sqrt(x_d * x_d + y_d * y_d);
	}

	/**
	 * @param v
	 *            The other vector
	 * @return the squared distance between this and the other vector
	 */
	public float dst2(final Vector2D v) {
		final float x_d = v.x - x;
		final float y_d = v.y - y;
		return x_d * x_d + y_d * y_d;
	}

	/**
	 * @param x
	 *            The x-component of the other vector
	 * @param y
	 *            The y-component of the other vector
	 * @return the squared distance between this and the other vector
	 */
	public float dst2(final float x, final float y) {
		final float x_d = x - this.x;
		final float y_d = y - this.y;
		return x_d * x_d + y_d * y_d;
	}

	/**
	 * Limits this vector's length to given value
	 * 
	 * @param limit
	 *            Max length
	 * @return This vector for chaining
	 */
	public Vector2D limit(final float limit) {
		if (len2() > limit * limit) {
			normalize();
			scl(limit);
		}
		return this;
	}

	/**
	 * Clamps this vector's length to given value
	 * 
	 * @param min
	 *            Min length
	 * @param max
	 *            Max length
	 * @return This vector for chaining
	 */
	public Vector2D clamp(final float min, final float max) {
		final float l2 = len2();
		if (l2 == 0f)
			return this;
		if (l2 > max * max)
			return normalize().scl(max);
		if (l2 < min * min)
			return normalize().scl(min);
		return this;
	}

	public String toString() {
		return "[" + x + ":" + y + "]";
	}

	/**
	 * Substracts the other vector from this vector.
	 * 
	 * @param x
	 *            The x-component of the other vector
	 * @param y
	 *            The y-component of the other vector
	 * @return This vector for chaining
	 */
	public Vector2D sub(final float x, final float y) {
		this.x -= x;
		this.y -= y;
		return this;
	}

	/**
	 * Left-multiplies this vector by the given matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return this vector
	 */
//	public Vector2D mul(final Matrix3 mat) {
//		final float x = this.x * mat.val[0] + this.y * mat.val[3] + mat.val[6];
//		final float y = this.x * mat.val[1] + this.y * mat.val[4] + mat.val[7];
//		this.x = x;
//		this.y = y;
//		return this;
//	}

	/**
	 * Calculates the 2D cross product between this and the given vector.
	 * 
	 * @param v
	 *            the other vector
	 * @return the cross product
	 */
	public float crs(final Vector2D v) {
		return this.x * v.y - this.y * v.x;
	}

	/**
	 * Calculates the 2D cross product between this and the given vector.
	 * 
	 * @param x
	 *            the x-coordinate of the other vector
	 * @param y
	 *            the y-coordinate of the other vector
	 * @return the cross product
	 */
	public float crs(final float x, final float y) {
		return this.x * y - this.y * x;
	}

	/**
	 * @return the angle in degrees of this vector (point) relative to the
	 *         x-axis. Angles are towards the positive y-axis (typically
	 *         counter-clockwise) and between 0 and 360.
	 */
	public float angle() {
		float angle = (float) Math.atan2(y, x) * Angle.radiansToDegrees;
		if (angle < 0)
			angle += 360;
		return angle;
	}

	/**
	 * Sets the angle of the vector in degrees relative to the x-axis, towards
	 * the positive y-axis (typically counter-clockwise).
	 * 
	 * @param degrees
	 *            The angle to setKey.
	 */
	public Vector2D setAngle(final float degrees) {
		this.set(len(), 0f);
		this.rotate(degrees);

		return this;
	}

	/**
	 * Rotates the Vector2D by the given angle, counter-clockwise assuming the
	 * y-axis points up.
	 * 
	 * @param degrees
	 *            the angle in degrees
	 */
	public Vector2D rotate(final float degrees) {
		final float rad = degrees * Angle.degreesToRadians;
		final float cos = (float) Math.cos(rad);
		final float sin = (float) Math.sin(rad);

		final float newX = this.x * cos - this.y * sin;
		final float newY = this.x * sin + this.y * cos;

		this.x = newX;
		this.y = newY;

		return this;
	}

	/**
	 * Rotates the Vector2D by 90 degrees in the specified direction, where >= 0
	 * is counter-clockwise and < 0 is clockwise.
	 */
	public Vector2D rotate90(final int dir) {
		final float x = this.x;
		if (dir >= 0) {
			this.x = -y;
			y = x;
		} else {
			this.x = y;
			y = -x;
		}
		return this;
	}

	/**
	 * Linearly interpolates between this vector and the target vector by alpha
	 * which is in the range [0,1]. The result is stored in this vector.
	 * 
	 * @param target
	 *            The target vector
	 * @param alpha
	 *            The interpolation coefficient
	 * @return This vector for chaining.
	 */
	public Vector2D lerp(final Vector2D target, final float alpha) {
		final float invAlpha = 1.0f - alpha;
		this.x = (x * invAlpha) + (target.x * alpha);
		this.y = (y * invAlpha) + (target.y * alpha);
		return this;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;

		final Vector2D vector2D = (Vector2D) o;

		if (Float.compare(vector2D.x, x) != 0)
			return false;
		if (Float.compare(vector2D.y, y) != 0)
			return false;

		return true;
	}

	/**
	 * Compares this vector with the other vector, using the supplied epsilon
	 * for fuzzy equality testing.
	 * 
	 * @param obj
	 * @param epsilon
	 * @return whether the vectors are the same.
	 */
	public boolean epsilonEquals(final Vector2D obj, final float epsilon) {
		if (obj == null)
			return false;
		if (Math.abs(obj.x - x) > epsilon)
			return false;
		if (Math.abs(obj.y - y) > epsilon)
			return false;
		return true;
	}

	/**
	 * Compares this vector with the other vector, using the supplied epsilon
	 * for fuzzy equality testing.
	 * 
	 * @param x
	 * @param y
	 * @param epsilon
	 * @return whether the vectors are the same.
	 */
	public boolean epsilonEquals(final float x, final float y,
			final float epsilon) {
		if (Math.abs(x - this.x) > epsilon)
			return false;
		if (Math.abs(y - this.y) > epsilon)
			return false;
		return true;
	}
}