package toto.geom2d;

import com.mi.toto.Conditions;
//import toto.db.odb.ICloneable;
//import toto.db.odb.Value;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Rectangle class with double precision.
 */
public class RectangleD extends T2DShape  /*implements Value, ICloneable*/ {
	protected double top;
	protected double left;
	protected double bottom;
	protected double right;

	/**
	 * Smallest Y coordinate of the rectangle
	 */
	public final double getTop() {
		return top;
	}

	/**
	 * Smallest X coordinate of the rectangle
	 */
	public final double getLeft() {
		return left;
	}

	/**
	 * Greatest Y coordinate of the rectangle
	 */
	public final double getBottom() {
		return bottom;
	}

	/**
	 * Greatest X coordinate of the rectangle
	 */
	public final double getRight() {
		return right;
	}

	/**
	 * Rectangle area
	 */
	public final double area() {
		return (bottom - top) * (right - left);
	}

	/**
	 * Area of covered rectangle for two sepcified rectangles
	 */
	public static double joinArea(final RectangleD a, final RectangleD b) {
		final double left = (a.left < b.left) ? a.left : b.left;
		final double right = (a.right > b.right) ? a.right : b.right;
		final double top = (a.top < b.top) ? a.top : b.top;
		final double bottom = (a.bottom > b.bottom) ? a.bottom : b.bottom;
		return (bottom - top) * (right - left);
	}

	/**
	 * Calculate dostance from the specified poin to the rectange
	 */
	public double distance(final double x, final double y) {
		if (x >= left && x <= right) {
			if (y >= top) {
				if (y <= bottom) {
					return 0;
				} else {
					return y - bottom;
				}
			} else {
				return top - y;
			}
		} else if (y >= top && y <= bottom) {
			if (x < left) {
				return left - x;
			} else {
				return x - right;
			}
		}
		final double dx = x < left ? left - x : x - right;
		final double dy = y < top ? top - y : y - bottom;
		return Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Clone rectangle
	 */
	public Object clone() {
		try {
			final RectangleD r = (RectangleD) super.clone();
			r.top = this.top;
			r.left = this.left;
			r.bottom = this.bottom;
			r.right = this.right;
			return r;
		} catch (final CloneNotSupportedException e) {
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Create copy of the rectangle
	 */
	public RectangleD(final RectangleD r) {
		this.top = r.top;
		this.left = r.left;
		this.bottom = r.bottom;
		this.right = r.right;
	}

	/**
	 * Construct rectangle with specified coordinates
	 */
	public RectangleD(final double top, final double left, final double bottom,
			final double right) {
		Conditions.assertArgument(top <= bottom && left <= right);
		this.top = top;
		this.left = left;
		this.bottom = bottom;
		this.right = right;
	}

	/**
	 * Default constructor for ODb
	 */
	public RectangleD() {
	}

	/**
	 * Join two rectangles. This rectangle is updates to contain cover of this
	 * and specified rectangle.
	 * 
	 * @param r
	 *            rectangle to be joined with this rectangle
	 */
	public final void join(final RectangleD r) {
		if (left > r.left) {
			left = r.left;
		}
		if (right < r.right) {
			right = r.right;
		}
		if (top > r.top) {
			top = r.top;
		}
		if (bottom < r.bottom) {
			bottom = r.bottom;
		}
	}

	/**
	 * Non destructive join of two rectangles.
	 * 
	 * @param a
	 *            first joined rectangle
	 * @param b
	 *            second joined rectangle
	 * @return rectangle containing cover of these two rectangles
	 */
	public static RectangleD join(final RectangleD a, final RectangleD b) {
		final RectangleD r = new RectangleD(a);
		r.join(b);
		return r;
	}

	/**
	 * Checks if this rectangle intersects with specified rectangle
	 */
	public final boolean intersects(final RectangleD r) {
		return left <= r.right && top <= r.bottom && right >= r.left
				&& bottom >= r.top;
	}

	/**
	 * Checks if this rectangle contains the specified rectangle
	 */
	public final boolean contains(final RectangleD r) {
		return left <= r.left && top <= r.top && right >= r.right
				&& bottom >= r.bottom;
	}

	/**
	 * Check if two rectangles are equal
	 */
	public boolean equals(final Object o) {
		if (o instanceof RectangleD) {
			final RectangleD r = (RectangleD) o;
			return left == r.left && top == r.top && right == r.right
					&& bottom == r.bottom;
		}
		return false;
	}

	/**
	 * Hash code consists of all rectangle coordinates
	 */
	public int hashCode() {
		return (int) (Double.doubleToLongBits(top)
				^ (Double.doubleToLongBits(bottom) << 1)
				^ (Double.doubleToLongBits(left) << 2) ^ (Double
				.doubleToLongBits(right) << 3));
	}

	public String toString() {
		return "top=" + top + ", left=" + left + ", bottom=" + bottom
				+ ", right=" + right;
	}

	@Override
	public boolean contains(final float x, final float y) {
		final double width = right - left;
		final double height = bottom - top;
		return width > 0 && height > 0 && x >= this.left
				&& x < this.left + width && y >= this.top
				&& y < this.top + height;
	}

	@Override
	public boolean contains(final Point point) {
		return contains(point.x, point.y);
	}

	@Override
	public void draw(final Canvas canvas, final Paint paint) {
		canvas.drawRect((float) left, (float) top, (float) right,
				(float) bottom, paint);

	}

}
