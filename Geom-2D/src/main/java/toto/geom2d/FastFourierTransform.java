/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.geom2d;

/**
 * Fast Fourier Transform implementation
 * 
 * @author ekent4
 * 
 */
public class FastFourierTransform {

	// Weighting factors
	protected float[] w1;
	protected float[] w2;
	protected float[] w3;

	public FastFourierTransform(final int logN) {
		// Prepare the weighting factors
		w1 = new float[logN];
		w2 = new float[logN];
		w3 = new float[logN];
		int N = 1;
		for (int k = 0; k < logN; k++) {
			N <<= 1;
			final double angle = -2.0 * Math.PI / N;
			w1[k] = (float) Math.sin(0.5 * angle);
			w2[k] = -2.0f * w1[k] * w1[k];
			w3[k] = (float) Math.sin(angle);
		}
	}

	private void scramble(final int n, final float[] real, final float[] imag) {
		int j = 0;

		for (int i = 0; i < n; i++) {
			if (i > j) {
				float t;
				t = real[j];
				real[j] = real[i];
				real[i] = t;
				t = imag[j];
				imag[j] = imag[i];
				imag[i] = t;
			}
			int m = n >> 1;
			while (j >= m && m >= 2) {
				j -= m;
				m >>= 1;
			}
			j += m;
		}
	}

	private void butterflies(final int n, final int logN, final int direction,
			final float[] real, final float[] imag) {
		int N = 1;

		for (int k = 0; k < logN; k++) {
			float w_re, w_im, wp_re, wp_im, temp_re, temp_im, wt;
			final int half_N = N;
			N <<= 1;
			wt = direction * w1[k];
			wp_re = w2[k];
			wp_im = direction * w3[k];
			w_re = 1.0f;
			w_im = 0.0f;
			for (int offset = 0; offset < half_N; offset++) {
				for (int i = offset; i < n; i += N) {
					final int j = i + half_N;
					final float re = real[j];
					final float im = imag[j];
					temp_re = (w_re * re) - (w_im * im);
					temp_im = (w_im * re) + (w_re * im);
					real[j] = real[i] - temp_re;
					real[i] += temp_re;
					imag[j] = imag[i] - temp_im;
					imag[i] += temp_im;
				}
				wt = w_re;
				w_re = wt * wp_re - w_im * wp_im + w_re;
				w_im = w_im * wp_re + wt * wp_im + w_im;
			}
		}
		if (direction == -1) {
			final float nr = 1.0f / n;
			for (int i = 0; i < n; i++) {
				real[i] *= nr;
				imag[i] *= nr;
			}
		}
	}

	public void transform1D(final float[] real, final float[] imag,
			final int logN, final int n, final boolean forward) {
		scramble(n, real, imag);
		butterflies(n, logN, forward ? 1 : -1, real, imag);
	}

	public void transform2D(final float[] real, final float[] imag,
			final int cols, final int rows, final boolean forward) {
		final int log2cols = log2(cols);
		final int log2rows = log2(rows);
		final int n = Math.max(rows, cols);
		final float[] rtemp = new float[n];
		final float[] itemp = new float[n];

		// FFT the rows
		for (int y = 0; y < rows; y++) {
			final int offset = y * cols;
			System.arraycopy(real, offset, rtemp, 0, cols);
			System.arraycopy(imag, offset, itemp, 0, cols);
			transform1D(rtemp, itemp, log2cols, cols, forward);
			System.arraycopy(rtemp, 0, real, offset, cols);
			System.arraycopy(itemp, 0, imag, offset, cols);
		}

		// FFT the columns
		for (int x = 0; x < cols; x++) {
			int index = x;
			for (int y = 0; y < rows; y++) {
				rtemp[y] = real[index];
				itemp[y] = imag[index];
				index += cols;
			}
			transform1D(rtemp, itemp, log2rows, rows, forward);
			index = x;
			for (int y = 0; y < rows; y++) {
				real[index] = rtemp[y];
				imag[index] = itemp[y];
				index += cols;
			}
		}
	}

	private int log2(final int n) {
		int m = 1;
		int log2n = 0;

		while (m < n) {
			m *= 2;
			log2n++;
		}
		return m == n ? log2n : -1;
	}

}
