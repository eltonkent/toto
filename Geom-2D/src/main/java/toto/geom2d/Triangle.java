/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected y2 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.geom2d;

import java.io.Serializable;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

public class Triangle extends T2DShape implements Serializable {
	private float x1, y1, x2, y2, x3, y3;

	public Triangle(float x1, float y1, float x2, float y2, float x3, float y3) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.x3 = x3;
		this.y3 = y3;
	}

	public float area(float x1, float y1, float x2, float y2, float x3, float y3) {
		return Math.abs((x1 - x3) * (y2 - y1) - (x1 - x2) * (y3 - y1)) * 0.5f;
	}

	/**
	 * Compute the centre of gravity
	 * 
	 * @param centroid
	 * @return
	 */
	public Point centroid(Point centroid) {
		centroid.x = (x1 + x2 + x3) / 3;
		centroid.y = (y1 + y2 + y3) / 3;
		return centroid;
	}

	/**
	 * Returns true if the given point is inside the triangle.
	 */
	public boolean contains(float px, float py) {
		float px1 = px - x1;
		float py1 = py - y1;
		boolean side12 = (x2 - x1) * py1 - (y2 - y1) * px1 > 0;
		if ((x3 - x1) * py1 - (y3 - y1) * px1 > 0 == side12)
			return false;
		if ((x3 - x2) * (py - y2) - (y3 - y2) * (px - x2) > 0 != side12)
			return false;
		return true;
	}

	public boolean contains(Point point) {
		return contains(point.x, point.y);
	}

	private Path path;

	public Path getPath() {
		if (path == null) {
			path = new Path();
		}
		path.moveTo(x1, y1);
		path.lineTo(x2, y2);
		path.lineTo(x3, y3);
		path.lineTo(x1, y1);
		path.reset();

		return path;
	}

	@Override
	public void draw(Canvas canvas, Paint paint) {
		canvas.drawPath(getPath(), paint);
	}
}
