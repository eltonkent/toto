package toto.geom2d;

import java.io.Serializable;
import java.util.Collection;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * The <code>Point2D</code> class defines a point representing a location in
 * {@code (x, y)} coordinate space.
 */
public class Point extends T2DShape implements Serializable, Cloneable {
	/**
	 * The X coordinate of this <code>Point2D</code>.
	 * 
	 * @serial
	 * @since 1.2
	 */
	protected float x;

	/**
	 * The Y coordinate of this <code>Point2D</code>.
	 * 
	 * @serial
	 * @since 1.2
	 */
	protected float y;

	/**
	 * Constructs and initializes a <code>Point2D</code> with coordinates
	 * (0,&nbsp;0).
	 * 
	 * @since 1.2
	 */
	public Point() {
	}

	/**
	 * Constructs and initializes a <code>Point2D</code> with the specified
	 * coordinates.
	 * 
	 * @param x
	 *            the X coordinate of the newly constructed <code>Point2D</code>
	 * @param y
	 *            the Y coordinate of the newly constructed <code>Point2D</code>
	 * @since 1.2
	 */
	public Point(final float x, final float y) {
		this.x = x;
		this.y = y;
	}

	public Point(final Point p) {
		this.x = p.x;
		this.y = p.y;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @since 1.2
	 */
	public float getX() {
		return x;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @since 1.2
	 */
	public float getY() {
		return y;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @since 1.2
	 */
	public void setLocation(final float x, final float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns a <code>String</code> that represents the value of this
	 * <code>Point2D</code>.
	 * 
	 * @return a string representation of this <code>Point2D</code>.
	 * @since 1.2
	 */
	public String toString() {
		return "Point2D.Float[" + x + ", " + y + "]";
	}

	/**
	 * Sets the location of this <code>Point2D</code> to the same coordinates as
	 * the specified <code>Point2D</code> object.
	 * 
	 * @param p
	 *            the specified <code>Point2D</code> to which to setKey this
	 *            <code>Point2D</code>
	 * @since 1.2
	 */
	public void setLocation(final Point p) {
		setLocation(p.getX(), p.getY());
	}

	/**
	 * Returns the square of the distance between two points.
	 * 
	 * @param x1
	 *            the X coordinate of the first specified point
	 * @param y1
	 *            the Y coordinate of the first specified point
	 * @param x2
	 *            the X coordinate of the second specified point
	 * @param y2
	 *            the Y coordinate of the second specified point
	 * @return the square of the distance between the two sets of specified
	 *         coordinates.
	 * @since 1.2
	 */
	public static float distanceSq(float x1, float y1, final float x2,
			final float y2) {
		x1 -= x2;
		y1 -= y2;
		return (x1 * x1 + y1 * y1);
	}

	/**
	 * Returns the distance between two points.
	 * 
	 * @param p1x
	 *            the X coordinate of the first specified point
	 * @param p1y
	 *            the Y coordinate of the first specified point
	 * @param p2x
	 *            the X coordinate of the second specified point
	 * @param p2y
	 *            the Y coordinate of the second specified point
	 * @return the distance between the two sets of specified coordinates.
	 */
	public static float distance(final float p1x, final float p1y,
			final float p2x, final float p2y) {
		final float deltaX = Math.abs(p1x - p2x);
		final float deltaY = Math.abs(p1y - p2y);
		return (float) Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
	}

	/**
	 * Computes the orientation of the 3 points: returns +1 is the path
	 * P0->P1->P2 turns Counter-Clockwise, -1 if the path turns Clockwise, and 0
	 * if the point P2 is located on the line segment [P0 P1]. Algorithm taken
	 * from Sedgewick.
	 * 
	 * @param p0
	 *            the initial point
	 * @param p1
	 *            the middle point
	 * @param p2
	 *            the last point
	 * @return +1, 0 or -1, depending on the relative position of the points
	 */
	public static int ccw(final Point p0, final Point p1, final Point p2) {
		final double x0 = p0.x;
		final double y0 = p0.y;
		final double dx1 = p1.x - x0;
		final double dy1 = p1.y - y0;
		final double dx2 = p2.x - x0;
		final double dy2 = p2.y - y0;

		if (dx1 * dy2 > dy1 * dx2)
			return +1;
		if (dx1 * dy2 < dy1 * dx2)
			return -1;
		if ((dx1 * dx2 < 0) || (dy1 * dy2 < 0))
			return -1;
		if (Math.hypot(dx1, dy1) < Math.hypot(dx2, dy2))
			return +1;
		return 0;
	}

	/**
	 * Returns the square of the distance from this <code>Point2D</code> to a
	 * specified point.
	 * 
	 * @param px
	 *            the X coordinate of the specified point to be measured against
	 *            this <code>Point2D</code>
	 * @param py
	 *            the Y coordinate of the specified point to be measured against
	 *            this <code>Point2D</code>
	 * @return the square of the distance between this <code>Point2D</code> and
	 *         the specified point.
	 * @since 1.2
	 */
	public float distanceSq(float px, float py) {
		px -= getX();
		py -= getY();
		return (px * px + py * py);
	}

	/**
	 * Returns the square of the distance from this <code>Point2D</code> to a
	 * specified <code>Point2D</code>.
	 * 
	 * @param pt
	 *            the specified point to be measured against this
	 *            <code>Point2D</code>
	 * @return the square of the distance between this <code>Point2D</code> to a
	 *         specified <code>Point2D</code>.
	 * @since 1.2
	 */
	public float distanceSq(final Point pt) {
		final double px = pt.getX() - this.getX();
		final double py = pt.getY() - this.getY();
		return (float) (px * px + py * py);
	}

	/**
	 * Returns the distance from this <code>Point2D</code> to a specified point.
	 * 
	 * @param px
	 *            the X coordinate of the specified point to be measured against
	 *            this <code>Point2D</code>
	 * @param py
	 *            the Y coordinate of the specified point to be measured against
	 *            this <code>Point2D</code>
	 * @return the distance between this <code>Point2D</code> and a specified
	 *         point.
	 * @since 1.2
	 */
	public float distance(float px, float py) {
		px -= x;
		py -= y;
		return (float) Math.sqrt(px * px + py * py);
	}

	/**
	 * Returns the distance from this <code>Point2D</code> to a specified
	 * <code>Point2D</code>.
	 * 
	 * @param pt
	 *            the specified point to be measured against this
	 *            <code>Point2D</code>
	 * @return the distance between this <code>Point2D</code> and the specified
	 *         <code>Point2D</code>.
	 * @since 1.2
	 */
	public float distance(final Point pt) {
		final float px = pt.getX() - x;
		final float py = pt.getY() - y;
		return (float) Math.sqrt(px * px + py * py);
	}

	/**
	 * Rotate point at given <code>angle</code>
	 * 
	 * @param angle
	 * @return
	 */
	public void rotate(final float angle) {
		x = (float) (Math.cos(angle) * x - Math.sin(angle) * y);
		y = (float) (Math.sin(angle) * x + Math.cos(angle) * y);
	}

	public static Point rotate(final float x, final float y, final float angle) {
		final float newX = (float) (Math.cos(angle) * x - Math.sin(angle) * y);
		final float newY = (float) (Math.sin(angle) * x + Math.cos(angle) * y);
		return new Point(newX, newY);
	}

	public static Point rotate(float px, float py, final float ox,
			final float oy, final float cos, final float sin) {
		final float x = px - ox;
		final float y = py - oy;
		px = ((x * cos) - (y * sin));
		py = ((x * sin) + (y * cos));
		px += ox;
		py += oy;
		return new Point(px, py);
	}

	/**
	 * Rotate a Point object around another Point
	 * 
	 * @param origin
	 *            the point around which to rotate
	 * @param angle
	 *            the angle (in degrees) of rotation
	 */
	public Point rotate(final Point origin, final float angle) {
		final float cos = (float) Math.cos((Math.PI * angle) / 180f);
		final float sin = (float) Math.cos((Math.PI * angle) / 180f);
		return rotate(this, origin, cos, sin);
	}

	/**
	 * Rotate a Point object around another Point object
	 * <p>
	 * The new coordinates are setKey in <code>point</code>
	 * </p>
	 * 
	 * @param point
	 *            the point to rotate
	 * @param origin
	 *            the point around which to rotate
	 * @param cos
	 *            the cosine of the rotation angle
	 * @param sin
	 *            the sine of the rotation angle
	 * @return The new coordinate of <code>point</code>
	 */
	public static Point rotate(final Point point, final Point origin,
			final float cos, final float sin) {
		return rotate(point.x, point.y, origin.x, origin.y, cos, sin);
	}

	/**
	 * Returns the hashcode for this <code>Point2D</code>.
	 * 
	 * @return a hash code for this <code>Point2D</code>.
	 */
	public int hashCode() {
		long bits = java.lang.Double.doubleToLongBits(getX());
		bits ^= java.lang.Double.doubleToLongBits(getY()) * 31;
		return (((int) bits) ^ ((int) (bits >> 32)));
	}

	/**
	 * Translates this point, at location {@code (x, y)}, by {@code dx} along
	 * the {@code x} axis and {@code dy} along the {@code y} axis so that it now
	 * represents the point {@code (x+dx, y+dy)}.
	 * 
	 * @param dx
	 *            the distance to move this point along the X axis
	 * @param dy
	 *            the distance to move this point along the Y axis
	 */
	public void translate(final float dx, final float dy) {
		x = x + dx;
		y = y + dy;
	}

	/**
	 * Returns the euclidian distance from (0,0) to (x,y)
	 * 
	 * @return
	 */
	public float length() {
		return length(x, y);
	}

	/**
	 * Returns the euclidian distance from (0,0) to (x,y)
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public static float length(final float x, final float y) {
		return (float) Math.sqrt(x * x + y * y);
	}

	/**
	 * Subracts p's x and y values from this points x and y values
	 */
	public Point subtract(final Point p2) {
		return new Point(x - p2.x, y - p2.y);
	}

	public Point add(final Point p2) {
		return new Point(x + p2.x, y + p2.y);
	}

	/**
	 * convert an x,y point to degrees.
	 * 
	 * @return
	 */
	public float toDegrees() {
		final float distanceFromCenter = Point.length((x - 0.5f), (y - 0.5f));
		if (distanceFromCenter < 0.1f || distanceFromCenter > 0.5f) { // ignore
			// center
			// and
			// out
			// of
			// bounds
			// events
			return Float.NaN;
		} else {
			return (float) Math.toDegrees(Math.atan2(x - 0.5f, y - 0.5f));
		}
	}

	/**
	 * Creates a new point from polar coordinates <code>rho</code> and
	 * <code>theta</code>.
	 */
	public Point createPolar(final float rho, final float theta) {
		return new Point((float) (x + rho * Math.cos(theta)), (float) (y + rho
				* Math.sin(theta)));
	}

	/**
	 * Creates a new point from polar coordinates <code>rho</code> and
	 * <code>theta</code>, from the given point.
	 */
	public static Point createPolar(final Point point, final float rho,
			final float theta) {
		return new Point((float) (point.x + rho * Math.cos(theta)),
				(float) (point.y + rho * Math.sin(theta)));
	}

	/**
	 * Tests if the three points are colinear.
	 * 
	 * @return true if three points lie on the same line.
	 */
	public static boolean isColinear(final Point p1, final Point p2,
			final Point p3) {
		double dx1, dx2, dy1, dy2;
		dx1 = p2.x - p1.x;
		dy1 = p2.y - p1.y;
		dx2 = p3.x - p1.x;
		dy2 = p3.y - p1.y;

		// tests if the two lines are parallel
		return Math.abs(dx1 * dy2 - dy1 * dx2) < 1e-12;
	}

	/**
	 * Computes the centroid, or center of mass, of a collection of points.
	 * 
	 * @param points
	 *            a collection of points
	 * @return the centroid of the points
	 */
	public static Point centroid(final Collection<? extends Point> points) {
		final int n = points.size();
		double sx = 0, sy = 0;
		for (final Point point : points) {
			sx += point.x;
			sy += point.y;
		}
		return new Point((float) sx / n, (float) sy / n);
	}

	/**
	 * @param scaleX
	 *            scale factor in X direction
	 * @param scaleY
	 *            scale factor in y direction
	 */
	public void scale(final float scaleX, final float scaleY) {
		this.x = this.x * scaleX;
		this.y = this.y * scaleY;
	}

	public void draw(final Canvas canvas, final Paint paint) {
		canvas.drawPoint(x, y, paint);
	}

	@Override
	public boolean contains(final float x, final float y) {
		return contains(new Point(x, y));
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o)
			return true;
		if (o == null || ((Object) this).getClass() != o.getClass())
			return false;

		final Point point = (Point) o;

		if (Float.compare(point.x, x) != 0)
			return false;
		if (Float.compare(point.y, y) != 0)
			return false;

		return true;
	}

	@Override
	public boolean contains(final Point point) {
		return this.equals(point);
	}

	/**
	 * Get the angle (in degrees) between this point and the target point
	 * 
	 * @param target
	 *            other target point.
	 * @return
	 */
	public float getAngle(final Point target) {
		float angle = (float) Math.toDegrees(Math.atan2(target.x - x, target.y
				- y));
		if (angle < 0) {
			angle += 360;
		}
		return angle;
	}
}