/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections.map;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * A map with expiration. This class contains a worker thread that will
 * periodically check this class in order to determine if any objects should be
 * removed based on the provided time-to-live value.
 * 
 */
public class ExpiringMap<K, V> implements Map<K, V> {

	/**
	 * A listener for expired object events.
	 * 
	 * @see ExpiringMap
	 */
	public static interface ExpirationListener<E> {
		void expired(E expiredObject);
	}

	/**
	 * A Thread that monitors an {@link ExpiringMap} and will remove elements
	 * that have passed the threshold.
	 * 
	 */
	public class Expirer implements Runnable {
		private long expirationIntervalMillis;

		private final Thread expirerThread;

		private boolean running = false;

		private final ReadWriteLock stateLock = new ReentrantReadWriteLock();

		private long timeToLiveMillis;

		/**
		 * Creates a new instance of Expirer.
		 * 
		 */
		public Expirer() {
			expirerThread = new Thread(this, "ExpiringMapExpirer-"
					+ expirerCount++);
			expirerThread.setDaemon(true);
		}

		/**
		 * Get the interval in which an object will live in the map before it is
		 * removed.
		 * 
		 * @return The time in seconds.
		 */
		public int getExpirationInterval() {
			stateLock.readLock().lock();

			try {
				return (int) expirationIntervalMillis / 1000;
			} finally {
				stateLock.readLock().unlock();
			}
		}

		/**
		 * Returns the Time-to-live value.
		 * 
		 * @return The time-to-live (seconds)
		 */
		public int getTimeToLive() {
			stateLock.readLock().lock();

			try {
				return (int) timeToLiveMillis / 1000;
			} finally {
				stateLock.readLock().unlock();
			}
		}

		/**
		 * Checks to see if the thread is running
		 * 
		 * @return If the thread is running, true. Otherwise false.
		 */
		public boolean isRunning() {
			stateLock.readLock().lock();

			try {
				return running;
			} finally {
				stateLock.readLock().unlock();
			}
		}

		private void processExpires() {
			final long timeNow = System.currentTimeMillis();

			for (final ExpiringObject o : delegate.values()) {

				if (timeToLiveMillis <= 0) {
					continue;
				}

				final long timeIdle = timeNow - o.getLastAccessTime();

				if (timeIdle >= timeToLiveMillis) {
					delegate.remove(o.getKey());

					for (final ExpirationListener<V> listener : expirationListeners) {
						listener.expired(o.getValue());
					}
				}
			}
		}

		@Override
		public void run() {
			while (running) {
				processExpires();

				try {
					Thread.sleep(expirationIntervalMillis);
				} catch (final InterruptedException e) {
					// Do nothing
				}
			}
		}

		/**
		 * Set the interval in which an object will live in the map before it is
		 * removed.
		 * 
		 * @param expirationInterval
		 *            The time in seconds
		 */
		public void setExpirationInterval(final long expirationInterval) {
			stateLock.writeLock().lock();

			try {
				this.expirationIntervalMillis = expirationInterval * 1000;
			} finally {
				stateLock.writeLock().unlock();
			}
		}

		/**
		 * Update the value for the time-to-live
		 * 
		 * @param timeToLive
		 *            The time-to-live (seconds)
		 */
		public void setTimeToLive(final long timeToLive) {
			stateLock.writeLock().lock();

			try {
				this.timeToLiveMillis = timeToLive * 1000;
			} finally {
				stateLock.writeLock().unlock();
			}
		}

		/**
		 * Kick off this thread which will look for old objects and remove them.
		 * 
		 */
		public void startExpiring() {
			stateLock.writeLock().lock();

			try {
				if (!running) {
					running = true;
					expirerThread.start();
				}
			} finally {
				stateLock.writeLock().unlock();
			}
		}

		/**
		 * If this thread has not started, then start it. Otherwise just return;
		 */
		public void startExpiringIfNotStarted() {
			stateLock.readLock().lock();
			try {
				if (running) {
					return;
				}
			} finally {
				stateLock.readLock().unlock();
			}

			stateLock.writeLock().lock();
			try {
				if (!running) {
					running = true;
					expirerThread.start();
				}
			} finally {
				stateLock.writeLock().unlock();
			}
		}

		/**
		 * Stop the thread from monitoring the map.
		 */
		public void stopExpiring() {
			stateLock.writeLock().lock();

			try {
				if (running) {
					running = false;
					expirerThread.interrupt();
				}
			} finally {
				stateLock.writeLock().unlock();
			}
		}
	}

	private class ExpiringObject {
		private final K key;

		private long lastAccessTime;

		private final ReadWriteLock lastAccessTimeLock = new ReentrantReadWriteLock();

		private final V value;

		ExpiringObject(final K key, final V value, final long lastAccessTime) {
			if (value == null) {
				throw new IllegalArgumentException(
						"An expiring object cannot be null.");
			}

			this.key = key;
			this.value = value;
			this.lastAccessTime = lastAccessTime;
		}

		@Override
		public boolean equals(final Object obj) {
			return value.equals(obj);
		}

		public K getKey() {
			return key;
		}

		public long getLastAccessTime() {
			lastAccessTimeLock.readLock().lock();

			try {
				return lastAccessTime;
			} finally {
				lastAccessTimeLock.readLock().unlock();
			}
		}

		public V getValue() {
			return value;
		}

		@Override
		public int hashCode() {
			return value.hashCode();
		}

		public void setLastAccessTime(final long lastAccessTime) {
			lastAccessTimeLock.writeLock().lock();

			try {
				this.lastAccessTime = lastAccessTime;
			} finally {
				lastAccessTimeLock.writeLock().unlock();
			}
		}
	}

	/**
	 * The default value, 1
	 */
	public static final int DEFAULT_EXPIRATION_INTERVAL = 1;

	/**
	 * The default value, 60
	 */
	public static final int DEFAULT_TIME_TO_LIVE = 60;

	private static volatile int expirerCount = 1;

	private final ConcurrentHashMap<K, ExpiringObject> delegate;

	private final CopyOnWriteArrayList<ExpirationListener<V>> expirationListeners;

	private final Expirer expirer;

	/**
	 * Creates a new instance of ExpiringMap using the default values
	 * DEFAULT_TIME_TO_LIVE and DEFAULT_EXPIRATION_INTERVAL
	 * 
	 */
	public ExpiringMap() {
		this(DEFAULT_TIME_TO_LIVE, DEFAULT_EXPIRATION_INTERVAL);
	}

	private ExpiringMap(
			final ConcurrentHashMap<K, ExpiringObject> delegate,
			final CopyOnWriteArrayList<ExpirationListener<V>> expirationListeners,
			final int timeToLive, final int expirationInterval) {
		this.delegate = delegate;
		this.expirationListeners = expirationListeners;

		this.expirer = new Expirer();
		expirer.setTimeToLive(timeToLive);
		expirer.setExpirationInterval(expirationInterval);
	}

	/**
	 * Creates a new instance of ExpiringMap using the supplied time-to-live
	 * value and the default value for DEFAULT_EXPIRATION_INTERVAL
	 * 
	 * @param timeToLive
	 *            The time-to-live value (seconds)
	 */
	public ExpiringMap(final int timeToLive) {
		this(timeToLive, DEFAULT_EXPIRATION_INTERVAL);
	}

	/**
	 * Creates a new instance of ExpiringMap using the supplied values and a
	 * {@link ConcurrentHashMap} for the internal data structure.
	 * 
	 * @param timeToLive
	 *            The time-to-live value (seconds)
	 * @param expirationInterval
	 *            The time between checks to see if a value should be removed
	 *            (seconds)
	 */
	public ExpiringMap(final int timeToLive, final int expirationInterval) {
		this(new ConcurrentHashMap<K, ExpiringObject>(),
				new CopyOnWriteArrayList<ExpirationListener<V>>(), timeToLive,
				expirationInterval);
	}

	public void addExpirationListener(final ExpirationListener<V> listener) {
		expirationListeners.add(listener);
	}

	@Override
	public void clear() {
		delegate.clear();
	}

	@Override
	public boolean containsKey(final Object key) {
		return delegate.containsKey(key);
	}

	@Override
	public boolean containsValue(final Object value) {
		return delegate.containsValue(value);
	}

	@Override
	public Set<Map.Entry<K, V>> entrySet() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean equals(final Object obj) {
		return delegate.equals(obj);
	}

	@Override
	public V get(final Object key) {
		final ExpiringObject object = delegate.get(key);

		if (object != null) {
			object.setLastAccessTime(System.currentTimeMillis());

			return object.getValue();
		}

		return null;
	}

	public int getExpirationInterval() {
		return expirer.getExpirationInterval();
	}

	public Expirer getExpirer() {
		return expirer;
	}

	public int getTimeToLive() {
		return expirer.getTimeToLive();
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

	@Override
	public boolean isEmpty() {
		return delegate.isEmpty();
	}

	@Override
	public Set<K> keySet() {
		return delegate.keySet();
	}

	@Override
	public V put(final K key, final V value) {
		final ExpiringObject answer = delegate.put(key, new ExpiringObject(key,
				value, System.currentTimeMillis()));
		if (answer == null) {
			return null;
		}

		return answer.getValue();
	}

	@Override
	public void putAll(final Map<? extends K, ? extends V> inMap) {
		for (final Entry<? extends K, ? extends V> e : inMap.entrySet()) {
			this.put(e.getKey(), e.getValue());
		}
	}

	@Override
	public V remove(final Object key) {
		final ExpiringObject answer = delegate.remove(key);
		if (answer == null) {
			return null;
		}

		return answer.getValue();
	}

	public void removeExpirationListener(final ExpirationListener<V> listener) {
		expirationListeners.remove(listener);
	}

	public void setExpirationInterval(final int expirationInterval) {
		expirer.setExpirationInterval(expirationInterval);
	}

	public void setTimeToLive(final int timeToLive) {
		expirer.setTimeToLive(timeToLive);
	}

	@Override
	public int size() {
		return delegate.size();
	}

	@Override
	public Collection<V> values() {
		throw new UnsupportedOperationException();
	}
}
