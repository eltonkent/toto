package toto.util.collections.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import android.util.Pair;

/**
 * A bi-directional map <code>K <-> V</code>.
 * 
 * <p>
 * Note that this class is <b>not</b> thread-safe.
 * </p>
 * 
 * 
 * @param <K>
 * @param <V>
 */
public class TwoWayMap<K, V> {
	private final Map<K, V> kToV = new HashMap<K, V>();
	private final Map<V, K> vToK = new HashMap<V, K>();

	public void add(final K x, final V y) {
		kToV.put(x, y);
		vToK.put(y, x);
	}

	public Iterator<K> keyIterator() {
		return kToV.keySet().iterator();
	}

	public Iterator<V> valueIterator() {
		return vToK.keySet().iterator();
	}

	public Iterator<Pair<K, V>> kvIterator() {
		final Iterator<Map.Entry<K, V>> i = kToV.entrySet().iterator();
		return new Iterator<Pair<K, V>>() {
			public void remove() {
				i.remove();
			}

			public boolean hasNext() {
				return i.hasNext();
			}

			public Pair<K, V> next() {
				final Map.Entry<K, V> e = i.next();
				return new Pair<K, V>(e.getKey(), e.getValue());
			}
		};
	}

	public Set<V> getValueSet() {
		return vToK.keySet();
	}

	public Set<K> getKeySet() {
		return kToV.keySet();
	}

	public V removeKey(final K key) {
		final V y = kToV.remove(key);
		if (y != null)
			vToK.remove(y);
		return y;
	}

	public K removeValue(final V value) {
		final K x = vToK.remove(value);
		if (x != null)
			kToV.remove(x);
		return x;
	}

	public V getValue(final K key) {
		return kToV.get(key);
	}

	public K getKey(final V value) {
		return vToK.get(value);
	}

	public boolean containsKey(final K key) {
		return kToV.containsKey(key);
	}

	public boolean containsValue(final V value) {
		return vToK.containsKey(value);
	}

	public boolean isEmtpy() {
		return kToV.isEmpty();
	}

	public void clear() {
		kToV.clear();
		vToK.clear();
	}
}
