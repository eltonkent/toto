/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections.map;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OrderRetainingMap extends HashMap {

	private static class ArraySet extends ArrayList implements Set {
	}

	private final ArraySet keyOrder = new ArraySet();

	private final List valueOrder = new ArrayList();

	public OrderRetainingMap() {
		super();
	}

	public OrderRetainingMap(final Map m) {
		super();
		for (final Iterator iter = m.entrySet().iterator(); iter.hasNext();) {
			final Map.Entry entry = (Map.Entry) iter.next();
			put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public Set entrySet() {
		final Map.Entry[] entries = new Map.Entry[size()];
		for (final Iterator iter = super.entrySet().iterator(); iter.hasNext();) {
			final Map.Entry entry = (Map.Entry) iter.next();
			entries[keyOrder.indexOf(entry.getKey())] = entry;
		}
		final Set set = new ArraySet();
		set.addAll(Arrays.asList(entries));
		return Collections.unmodifiableSet(set);
	}

	@Override
	public Set keySet() {
		return Collections.unmodifiableSet(keyOrder);
	}

	@Override
	public Object put(final Object key, final Object value) {
		final int idx = keyOrder.lastIndexOf(key);
		if (idx < 0) {
			keyOrder.add(key);
			valueOrder.add(value);
		} else {
			valueOrder.set(idx, value);
		}
		return super.put(key, value);
	}

	@Override
	public Object remove(final Object key) {
		final int idx = keyOrder.lastIndexOf(key);
		if (idx != 0) {
			keyOrder.remove(idx);
			valueOrder.remove(idx);
		}
		return super.remove(key);
	}

	@Override
	public Collection values() {
		return Collections.unmodifiableList(valueOrder);
	}
}
