/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.collections.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

/**
 */
public class PresortedMap implements SortedMap {

	private static class ArraySet extends ArrayList implements Set {
	}

	private static class ArraySetComparator implements Comparator {

		private Map.Entry[] array;
		private final ArrayList list;

		ArraySetComparator(final ArrayList list) {
			this.list = list;
		}

		@Override
		public int compare(final Object object1, final Object object2) {
			if (array == null || list.size() != array.length) {
				final Map.Entry[] a = new Map.Entry[list.size()];
				if (array != null) {
					System.arraycopy(array, 0, a, 0, array.length);
				}
				for (int i = array == null ? 0 : array.length; i < list.size(); ++i) {
					a[i] = (Map.Entry) list.get(i);
				}
				array = a;
			}
			int idx1 = Integer.MAX_VALUE, idx2 = Integer.MAX_VALUE;
			for (int i = 0; i < array.length
					&& !(idx1 < Integer.MAX_VALUE && idx2 < Integer.MAX_VALUE); ++i) {
				if (idx1 == Integer.MAX_VALUE && object1 == array[i].getKey()) {
					idx1 = i;
				}
				if (idx2 == Integer.MAX_VALUE && object2 == array[i].getKey()) {
					idx2 = i;
				}
			}
			return idx1 - idx2;
		}
	}

	private final Comparator comparator;

	private final PresortedMap.ArraySet set;

	public PresortedMap() {
		this(null, new ArraySet());
	}

	public PresortedMap(final Comparator comparator) {
		this(comparator, new ArraySet());
	}

	private PresortedMap(final Comparator comparator,
			final PresortedMap.ArraySet set) {
		this.comparator = comparator != null ? comparator
				: new ArraySetComparator(set);
		this.set = set;
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Comparator comparator() {
		return comparator;
	}

	@Override
	public boolean containsKey(final Object key) {
		return false;
	}

	@Override
	public boolean containsValue(final Object value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Set entrySet() {
		return set;
	}

	@Override
	public Object firstKey() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object get(final Object key) {
		throw new UnsupportedOperationException();
	}

	@Override
	public SortedMap headMap(final Object toKey) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isEmpty() {
		return set.isEmpty();
	}

	@Override
	public Set keySet() {
		final Set keySet = new ArraySet();
		for (final Iterator iterator = set.iterator(); iterator.hasNext();) {
			final Entry entry = (Entry) iterator.next();
			keySet.add(entry.getKey());
		}
		return keySet;
	}

	@Override
	public Object lastKey() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object put(final Object key, final Object value) {
		set.add(new Entry() {

			@Override
			public Object getKey() {
				return key;
			}

			@Override
			public Object getValue() {
				return value;
			}

			@Override
			public Object setValue(final Object value) {
				throw new UnsupportedOperationException();
			}
		});
		return null;
	}

	@Override
	public void putAll(final Map m) {
		for (final Iterator iter = m.entrySet().iterator(); iter.hasNext();) {
			set.add(iter.next());
		}
	}

	@Override
	public Object remove(final Object key) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int size() {
		return set.size();
	}

	@Override
	public SortedMap subMap(final Object fromKey, final Object toKey) {
		throw new UnsupportedOperationException();
	}

	@Override
	public SortedMap tailMap(final Object fromKey) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection values() {
		final Set values = new ArraySet();
		for (final Iterator iterator = set.iterator(); iterator.hasNext();) {
			final Entry entry = (Entry) iterator.next();
			values.add(entry.getValue());
		}
		return values;
	}
}
