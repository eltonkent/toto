package toto.beans;

import java.beans.PropertyChangeListener;

public interface PropertyEditor {

	public void setAsText(String text) throws IllegalArgumentException;

	public String[] getTags();

	public String getJavaInitializationString();

	public String getAsText();

	public void setValue(Object value);

	public Object getValue();

	public void removePropertyChangeListener(PropertyChangeListener listener);

	public void addPropertyChangeListener(PropertyChangeListener listener);

	public boolean supportsCustomEditor();

	public boolean isPaintable();
}
