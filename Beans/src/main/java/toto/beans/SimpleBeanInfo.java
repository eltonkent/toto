package toto.beans;

public class SimpleBeanInfo implements BeanInfo {

	public SimpleBeanInfo() {
		// expected
	}

	public PropertyDescriptor[] getPropertyDescriptors() {
		return null;
	}

	public MethodDescriptor[] getMethodDescriptors() {
		return null;
	}

	public EventSetDescriptor[] getEventSetDescriptors() {
		return null;
	}

	public BeanInfo[] getAdditionalBeanInfo() {
		return null;
	}

	public BeanDescriptor getBeanDescriptor() {
		return null;
	}

	public int getDefaultPropertyIndex() {
		return -1;
	}

	public int getDefaultEventIndex() {
		return -1;
	}
}
