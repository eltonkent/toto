package toto.beans;

public interface ExceptionListener {

	public void exceptionThrown(Exception e);
}
