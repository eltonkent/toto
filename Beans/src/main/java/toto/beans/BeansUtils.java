package toto.beans;

import java.lang.reflect.Method;
import java.util.Arrays;

class BeansUtils {

	static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

	static final String NEW = "new"; //$NON-NLS-1$

	static final String NEWINSTANCE = "newInstance"; //$NON-NLS-1$

	static final String NEWARRAY = "newArray"; //$NON-NLS-1$

	static final String FORNAME = "forName"; //$NON-NLS-1$

	static final String GET = "get"; //$NON-NLS-1$

	static final String IS = "is"; //$NON-NLS-1$

	static final String SET = "set"; //$NON-NLS-1$

	static final String ADD = "add"; //$NON-NLS-1$

	static final String PUT = "put"; //$NON-NLS-1$

	static final String NULL = "null"; //$NON-NLS-1$

	static final String QUOTE = "\"\""; //$NON-NLS-1$

	static final int getHashCode(final Object obj) {
		return obj != null ? obj.hashCode() : 0;
	}

	static final int getHashCode(final boolean bool) {
		return bool ? 1 : 0;
	}

	static String toASCIILowerCase(final String string) {
		final char[] charArray = string.toCharArray();
		final StringBuilder sb = new StringBuilder(charArray.length);
		for (int index = 0; index < charArray.length; index++) {
			if ('A' <= charArray[index] && charArray[index] <= 'Z') {
				sb.append((char) (charArray[index] + ('a' - 'A')));
			} else {
				sb.append(charArray[index]);
			}
		}
		return sb.toString();
	}

	static String toASCIIUpperCase(final String string) {
		final char[] charArray = string.toCharArray();
		final StringBuilder sb = new StringBuilder(charArray.length);
		for (int index = 0; index < charArray.length; index++) {
			if ('a' <= charArray[index] && charArray[index] <= 'z') {
				sb.append((char) (charArray[index] - ('a' - 'A')));
			} else {
				sb.append(charArray[index]);
			}
		}
		return sb.toString();
	}

	static boolean isPrimitiveWrapper(final Class<?> wrapper,
			final Class<?> base) {
		return (base == boolean.class) && (wrapper == Boolean.class)
				|| (base == byte.class) && (wrapper == Byte.class)
				|| (base == char.class) && (wrapper == Character.class)
				|| (base == short.class) && (wrapper == Short.class)
				|| (base == int.class) && (wrapper == Integer.class)
				|| (base == long.class) && (wrapper == Long.class)
				|| (base == float.class) && (wrapper == Float.class)
				|| (base == double.class) && (wrapper == Double.class);
	}

	private static final String EQUALS_METHOD = "equals";

	private static final Class<?>[] EQUALS_PARAMETERS = new Class<?>[] { Object.class };

	static boolean declaredEquals(final Class<?> clazz) {
		for (final Method declaredMethod : clazz.getDeclaredMethods()) {
			if (EQUALS_METHOD.equals(declaredMethod.getName())
					&& Arrays.equals(declaredMethod.getParameterTypes(),
							EQUALS_PARAMETERS)) {
				return true;
			}
		}
		return false;
	}

	static String idOfClass(final Class<?> clazz) {
		Class<?> theClass = clazz;
		final StringBuilder sb = new StringBuilder();
		if (theClass.isArray()) {
			do {
				sb.append("Array"); //$NON-NLS-1$
				theClass = theClass.getComponentType();
			} while (theClass.isArray());
		}
		String clazzName = theClass.getName();
		clazzName = clazzName.substring(clazzName.lastIndexOf('.') + 1);
		return clazzName + sb.toString();
	}
}
