package toto.beans;

/**
 * Describes a bean's global information.
 * 
 * @author Mobifluence Interactive
 */
public class BeanDescriptor extends FeatureDescriptor {

	private final Class<?> beanClass;

	private final Class<?> customizerClass;

	/**
	 * <p>
	 * Constructs an instance with the bean's {@link Class} and a customizer
	 * {@link Class}. The descriptor's {@link #getName()} is set as the
	 * unqualified name of the <code>beanClass</code>.
	 * </p>
	 * 
	 * @param beanClass
	 *            The bean's Class.
	 * @param customizerClass
	 *            The bean's customizer Class.
	 */
	public BeanDescriptor(final Class<?> beanClass,
			final Class<?> customizerClass) {
		if (beanClass == null) {
			throw new NullPointerException();
		}
		setName(getShortClassName(beanClass));
		this.beanClass = beanClass;
		this.customizerClass = customizerClass;
	}

	/**
	 * <p>
	 * Constructs an instance with the bean's {@link Class}. The descriptor's
	 * {@link #getName()} is set as the unqualified name of the
	 * <code>beanClass</code>.
	 * </p>
	 * 
	 * @param beanClass
	 *            The bean's Class.
	 */
	public BeanDescriptor(final Class<?> beanClass) {
		this(beanClass, null);
	}

	/**
	 * <p>
	 * Gets the bean's customizer {@link Class}/
	 * </p>
	 * 
	 * @return A {@link Class} instance or <code>null</code>.
	 */
	public Class<?> getCustomizerClass() {
		return customizerClass;
	}

	/**
	 * <p>
	 * Gets the bean's {@link Class}.
	 * </p>
	 * 
	 * @return A {@link Class} instance.
	 */
	public Class<?> getBeanClass() {
		return beanClass;
	}

	/**
	 * <p>
	 * Utility method for getting the unqualified name of a {@link Class}.
	 * </p>
	 * 
	 * @param leguminaClass
	 *            The Class to get the name from.
	 * @return A String instance or <code>null</code>.
	 */
	private String getShortClassName(final Class<?> leguminaClass) {
		if (leguminaClass == null) {
			return null;
		}
		final String beanClassName = leguminaClass.getName();
		final int lastIndex = beanClassName.lastIndexOf("."); //$NON-NLS-1$
		return (lastIndex == -1) ? beanClassName : beanClassName
				.substring(lastIndex + 1);
	}

}
