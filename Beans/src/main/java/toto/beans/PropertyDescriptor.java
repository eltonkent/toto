package toto.beans;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class PropertyDescriptor extends FeatureDescriptor {
	private Method getter;

	private Method setter;

	private Class<?> propertyEditorClass;

	boolean constrained;

	boolean bound;

	public PropertyDescriptor(final String propertyName,
			final Class<?> beanClass, final String getterName,
			final String setterName) throws IntrospectionException {
		super();
		if (beanClass == null) {
			throw new IntrospectionException("beans.03"); //$NON-NLS-1$
		}
		if (propertyName == null || propertyName.length() == 0) {
			throw new IntrospectionException("beans.04"); //$NON-NLS-1$
		}
		this.setName(propertyName);
		if (getterName != null) {
			if (getterName.length() == 0) {
				throw new IntrospectionException(
						"read or write method cannot be empty."); //$NON-NLS-1$    
			}
			try {
				setReadMethod(beanClass, getterName);
			} catch (final IntrospectionException e) {
				setReadMethod(beanClass,
						createDefaultMethodName(propertyName, "get")); //$NON-NLS-1$
			}
		}
		if (setterName != null) {
			if (setterName.length() == 0) {
				throw new IntrospectionException(
						"read or write method cannot be empty."); //$NON-NLS-1$    
			}
			setWriteMethod(beanClass, setterName);
		}
	}

	public PropertyDescriptor(final String propertyName, final Method getter,
			final Method setter) throws IntrospectionException {
		super();
		if (propertyName == null || propertyName.length() == 0) {
			throw new IntrospectionException("beans.04"); //$NON-NLS-1$
		}
		this.setName(propertyName);
		setReadMethod(getter);
		setWriteMethod(setter);
	}

	public PropertyDescriptor(final String propertyName,
			final Class<?> beanClass) throws IntrospectionException {
		if (beanClass == null) {
			throw new IntrospectionException("beans.03"); //$NON-NLS-1$
		}
		if (propertyName == null || propertyName.length() == 0) {
			throw new IntrospectionException("beans.04"); //$NON-NLS-1$
		}
		this.setName(propertyName);
		try {
			setReadMethod(beanClass,
					createDefaultMethodName(propertyName, "is")); //$NON-NLS-1$
		} catch (final Exception e) {
			setReadMethod(beanClass,
					createDefaultMethodName(propertyName, "get")); //$NON-NLS-1$
		}

		setWriteMethod(beanClass, createDefaultMethodName(propertyName, "set")); //$NON-NLS-1$
	}

	public void setWriteMethod(final Method setter)
			throws IntrospectionException {
		if (setter != null) {
			final int modifiers = setter.getModifiers();
			if (!Modifier.isPublic(modifiers)) {
				throw new IntrospectionException("beans.05"); //$NON-NLS-1$
			}
			final Class<?>[] parameterTypes = setter.getParameterTypes();
			if (parameterTypes.length != 1) {
				throw new IntrospectionException("beans.06"); //$NON-NLS-1$
			}
			final Class<?> parameterType = parameterTypes[0];
			final Class<?> propertyType = getPropertyType();
			if (propertyType != null && !propertyType.equals(parameterType)) {
				throw new IntrospectionException("beans.07"); //$NON-NLS-1$
			}
		}
		this.setter = setter;
	}

	public void setReadMethod(final Method getter)
			throws IntrospectionException {
		if (getter != null) {
			final int modifiers = getter.getModifiers();
			if (!Modifier.isPublic(modifiers)) {
				throw new IntrospectionException("beans.0A"); //$NON-NLS-1$
			}
			final Class<?>[] parameterTypes = getter.getParameterTypes();
			if (parameterTypes.length != 0) {
				throw new IntrospectionException("beans.08"); //$NON-NLS-1$
			}
			final Class<?> returnType = getter.getReturnType();
			if (returnType.equals(Void.TYPE)) {
				throw new IntrospectionException("beans.33"); //$NON-NLS-1$
			}
			final Class<?> propertyType = getPropertyType();
			if ((propertyType != null) && !returnType.equals(propertyType)) {
				throw new IntrospectionException("beans.09"); //$NON-NLS-1$
			}
		}
		this.getter = getter;
	}

	public Method getWriteMethod() {
		return setter;
	}

	public Method getReadMethod() {
		return getter;
	}

	@Override
	public boolean equals(final Object object) {
		boolean result = object instanceof PropertyDescriptor;
		if (result) {
			final PropertyDescriptor pd = (PropertyDescriptor) object;
			final boolean gettersAreEqual = (this.getter == null)
					&& (pd.getReadMethod() == null) || (this.getter != null)
					&& (this.getter.equals(pd.getReadMethod()));
			final boolean settersAreEqual = (this.setter == null)
					&& (pd.getWriteMethod() == null) || (this.setter != null)
					&& (this.setter.equals(pd.getWriteMethod()));
			final boolean propertyTypesAreEqual = this.getPropertyType() == pd
					.getPropertyType();
			final boolean propertyEditorClassesAreEqual = this
					.getPropertyEditorClass() == pd.getPropertyEditorClass();
			final boolean boundPropertyAreEqual = this.isBound() == pd
					.isBound();
			final boolean constrainedPropertyAreEqual = this.isConstrained() == pd
					.isConstrained();
			result = gettersAreEqual && settersAreEqual
					&& propertyTypesAreEqual && propertyEditorClassesAreEqual
					&& boundPropertyAreEqual && constrainedPropertyAreEqual;
		}
		return result;
	}

	@Override
	public int hashCode() {
		return BeansUtils.getHashCode(getter) + BeansUtils.getHashCode(setter)
				+ BeansUtils.getHashCode(getPropertyType())
				+ BeansUtils.getHashCode(getPropertyEditorClass())
				+ BeansUtils.getHashCode(isBound())
				+ BeansUtils.getHashCode(isConstrained());
	}

	public void setPropertyEditorClass(final Class<?> propertyEditorClass) {
		this.propertyEditorClass = propertyEditorClass;
	}

	public Class<?> getPropertyType() {
		Class<?> result = null;
		if (getter != null) {
			result = getter.getReturnType();
		} else if (setter != null) {
			final Class<?>[] parameterTypes = setter.getParameterTypes();
			result = parameterTypes[0];
		}
		return result;
	}

	public Class<?> getPropertyEditorClass() {
		return propertyEditorClass;
	}

	public void setConstrained(final boolean constrained) {
		this.constrained = constrained;
	}

	public void setBound(final boolean bound) {
		this.bound = bound;
	}

	public boolean isConstrained() {
		return constrained;
	}

	public boolean isBound() {
		return bound;
	}

	String createDefaultMethodName(final String propertyName,
			final String prefix) {
		String result = null;
		if (propertyName != null) {
			final String bos = toASCIIUpperCase(propertyName.substring(0, 1));
			final String eos = propertyName.substring(1, propertyName.length());
			result = prefix + bos + eos;
		}
		return result;
	}

	public static String toASCIIUpperCase(final String string) {
		final char[] charArray = string.toCharArray();
		final StringBuilder sb = new StringBuilder(charArray.length);
		for (int index = 0; index < charArray.length; index++) {
			if ('a' <= charArray[index] && charArray[index] <= 'z') {
				sb.append((char) (charArray[index] - ('a' - 'A')));
			} else {
				sb.append(charArray[index]);
			}
		}
		return sb.toString();
	}

	void setReadMethod(final Class<?> beanClass, final String getterName)
			throws IntrospectionException {
		try {
			final Method readMethod = beanClass.getMethod(getterName,
					new Class[] {});
			setReadMethod(readMethod);
		} catch (final Exception e) {
			throw new IntrospectionException(e.getLocalizedMessage());
		}
	}

	void setWriteMethod(final Class<?> beanClass, final String setterName)
			throws IntrospectionException {
		Method writeMethod = null;
		try {
			if (getter != null) {
				writeMethod = beanClass.getMethod(setterName,
						new Class[] { getter.getReturnType() });
			} else {
				Class<?> clazz = beanClass;
				Method[] methods = null;
				while (clazz != null && writeMethod == null) {
					methods = clazz.getDeclaredMethods();
					for (final Method method : methods) {
						if (setterName.equals(method.getName())) {
							if (method.getParameterTypes().length == 1) {
								writeMethod = method;
								break;
							}
						}
					}
					clazz = clazz.getSuperclass();
				}
			}
		} catch (final Exception e) {
			throw new IntrospectionException(e.getLocalizedMessage());
		}
		if (writeMethod == null) {
			throw new IntrospectionException("beans.64" + setterName); //$NON-NLS-1$
		}
		setWriteMethod(writeMethod);
	}

	public PropertyEditor createPropertyEditor(final Object bean) {
		PropertyEditor editor;
		if (propertyEditorClass == null) {
			return null;
		}
		if (!PropertyEditor.class.isAssignableFrom(propertyEditorClass)) {
			// beans.48=Property editor is not assignable from the
			// PropertyEditor interface
			throw new ClassCastException("beans.48"); //$NON-NLS-1$
		}
		try {
			Constructor<?> constr;
			try {
				// try to look for the constructor with single Object argument
				constr = propertyEditorClass.getConstructor(Object.class);
				editor = (PropertyEditor) constr.newInstance(bean);
			} catch (final NoSuchMethodException e) {
				// try no-argument constructor
				constr = propertyEditorClass.getConstructor();
				editor = (PropertyEditor) constr.newInstance();
			}
		} catch (final Exception e) {
			// beans.47=Unable to instantiate property editor
			final RuntimeException re = new RuntimeException("beans.47" + e); //$NON-NLS-1$
			throw re;
		}
		return editor;
	}
}
