package toto.di.barcode;

import java.util.List;

/**
 * <p>
 * Encapsulates the result of decoding a matrix of bits. This typically applies
 * to 2D barcode formats. For now it contains the raw bytes obtained, as well as
 * a String interpretation of those bytes, if applicable.
 * </p>
 * 
 */
final class DecoderResult {

	private final byte[] rawBytes;
	private final String text;
	private final List<byte[]> byteSegments;
	private final String ecLevel;
	private Integer errorsCorrected;
	private Integer erasures;
	private Object other;

	DecoderResult(byte[] rawBytes, String text, List<byte[]> byteSegments,
			String ecLevel) {
		this.rawBytes = rawBytes;
		this.text = text;
		this.byteSegments = byteSegments;
		this.ecLevel = ecLevel;
	}

	byte[] getRawBytes() {
		return rawBytes;
	}

	String getText() {
		return text;
	}

	List<byte[]> getByteSegments() {
		return byteSegments;
	}

	String getECLevel() {
		return ecLevel;
	}

	Integer getErrorsCorrected() {
		return errorsCorrected;
	}

	void setErrorsCorrected(Integer errorsCorrected) {
		this.errorsCorrected = errorsCorrected;
	}

	Integer getErasures() {
		return erasures;
	}

	void setErasures(Integer erasures) {
		this.erasures = erasures;
	}

	Object getOther() {
		return other;
	}

	void setOther(Object other) {
		this.other = other;
	}

}