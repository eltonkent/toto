package toto.di.barcode;

import java.util.Map;

import toto.text.StringUtils;
import toto.util.collections.matrix.BitMatrix;

/**
 * <p>
 * Encapsulates functionality and implementation that is common to
 * one-dimensional barcodes.
 * </p>
 * 
 * @author dsbnatut@gmail.com (Kazuki Nishiura)
 */
abstract class OneDimensionalCodeWriter implements Writer {

	@Override
	public final BitMatrix encode(String contents, BarcodeType format,
			int width, int height) throws BarcodeWriterException {
		return encode(contents, format, width, height, null);
	}

	/**
	 * Encode the contents following specified format. {@code width} and
	 * {@code height} are required size. This method may return bigger size
	 * {@code BitMatrix} when specified size is too small. The user can setKey
	 * both {@code width} and {@code height} to zero to get minimum size
	 * barcode. If negative value is setKey to {@code width} or {@code height},
	 * {@code InvalidDataException} is thrown.
	 */
	@Override
	public BitMatrix encode(String contents, BarcodeType format, int width,
			int height, Map<EncodeHintType, ?> hints)
			throws BarcodeWriterException {
		if (StringUtils.isEmpty(contents)) {
			throw new InvalidBarcodeDataException("Found empty contents");
		}

		if (width < 0 || height < 0) {
			throw new InvalidBarcodeDataException(
					"Negative size is not allowed. Input: " + width + 'x'
							+ height);
		}

		int sidesMargin = getDefaultMargin();
		if (hints != null) {
			Integer sidesMarginInt = (Integer) hints.get(EncodeHintType.MARGIN);
			if (sidesMarginInt != null) {
				sidesMargin = sidesMarginInt;
			}
		}

		boolean[] code = encode(contents);
		return renderResult(code, width, height, sidesMargin);
	}

	/**
	 * @return a byte array of horizontal pixels (0 = white, 1 = black)
	 */
	private static BitMatrix renderResult(boolean[] code, int width,
			int height, int sidesMargin) {
		int inputWidth = code.length;
		// Add quiet zone on both sides.
		int fullWidth = inputWidth + sidesMargin;
		int outputWidth = Math.max(width, fullWidth);
		int outputHeight = Math.max(1, height);

		int multiple = outputWidth / fullWidth;
		int leftPadding = (outputWidth - (inputWidth * multiple)) / 2;

		BitMatrix output = new BitMatrix(outputWidth, outputHeight);
		for (int inputX = 0, outputX = leftPadding; inputX < inputWidth; inputX++, outputX += multiple) {
			if (code[inputX]) {
				output.setRegion(outputX, 0, multiple, outputHeight);
			}
		}
		return output;
	}

	/**
	 * Appends the given pattern to the target array starting at pos.
	 * 
	 * @param startColor
	 *            starting color - false for white, true for black
	 * @return the number of elements added to target.
	 */
	protected static int appendPattern(boolean[] target, int pos,
			int[] pattern, boolean startColor) {
		boolean color = startColor;
		int numAdded = 0;
		for (int len : pattern) {
			for (int j = 0; j < len; j++) {
				target[pos++] = color;
			}
			numAdded += len;
			color = !color; // flip color after each segment
		}
		return numAdded;
	}

	int getDefaultMargin() {
		// CodaBar spec requires a side margin to be more than ten times wider
		// than narrow space.
		// This seems like a decent idea for a default for all formats.
		return 10;
	}

	/**
	 * Encode the contents to boolean array expression of one-dimensional
	 * barcode. Start code and end code should be included in result, and side
	 * margins should not be included.
	 * 
	 * @return a {@code boolean[]} of horizontal pixels (false = white, true =
	 *         black)
	 */
	abstract boolean[] encode(String contents);
}
