package toto.di.barcode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import toto.util.collections.list.BitArray;

final class MultiFormatOneDReader extends OneDReader {

	private final OneDReader[] readers;

	MultiFormatOneDReader(final Map<DecodeHintType, ?> hints) {
		@SuppressWarnings("unchecked")
		final Collection<BarcodeType> possibleFormats = hints == null ? null
				: (Collection<BarcodeType>) hints
						.get(DecodeHintType.POSSIBLE_FORMATS);
		final boolean useCode39CheckDigit = hints != null
				&& hints.get(DecodeHintType.ASSUME_CODE_39_CHECK_DIGIT) != null;
		final Collection<OneDReader> readers = new ArrayList<OneDReader>();
		if (possibleFormats != null) {
			if (possibleFormats.contains(BarcodeType.EAN_13)
					|| possibleFormats.contains(BarcodeType.UPC_A)
					|| possibleFormats.contains(BarcodeType.EAN_8)
					|| possibleFormats.contains(BarcodeType.UPC_E)) {
				readers.add(new MultiFormatUPCEANReader(hints));
			}
			if (possibleFormats.contains(BarcodeType.CODE_39)) {
				readers.add(new Code39Reader(useCode39CheckDigit));
			}
			if (possibleFormats.contains(BarcodeType.CODE_93)) {
				readers.add(new Code39Reader());
			}
			if (possibleFormats.contains(BarcodeType.CODE_128)) {
				readers.add(new Code128Reader());
			}
			if (possibleFormats.contains(BarcodeType.ITF)) {
				readers.add(new ITFReader());
			}
			if (possibleFormats.contains(BarcodeType.CODABAR)) {
				readers.add(new CodaBarReader());
			}
			if (possibleFormats.contains(BarcodeType.RSS_14)) {
				readers.add(new RSS14Reader());
			}
			// if (possibleFormats.contains(BarcodeType.RSS_EXPANDED)) {
			// readers.add(new RSSExpandedReader());
			// }
		}
		if (readers.isEmpty()) {
			readers.add(new MultiFormatUPCEANReader(hints));
			readers.add(new Code39Reader());
			readers.add(new CodaBarReader());
			readers.add(new Code39Reader());
			readers.add(new Code128Reader());
			readers.add(new ITFReader());
			readers.add(new RSS14Reader());
			// readers.add(new RSSExpandedReader());
		}
		this.readers = readers.toArray(new OneDReader[readers.size()]);
	}

	@Override
	public Result decodeRow(final int rowNumber, final BitArray row,
			final Map<DecodeHintType, ?> hints) throws BarcodeNotFoundException {
		for (final OneDReader reader : readers) {
			try {
				return reader.decodeRow(rowNumber, row, hints);
			} catch (final ReaderException re) {
				// continue
			}
		}

		throw new BarcodeNotFoundException();
	}

	@Override
	public void reset() {
		for (final Reader reader : readers) {
			reader.reset();
		}
	}

}