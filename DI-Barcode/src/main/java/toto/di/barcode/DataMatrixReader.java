package toto.di.barcode;

import java.util.List;
import java.util.Map;

import toto.bitmap.BinaryBitmap;
import toto.bitmap.ToToBitmapException;
import toto.util.collections.matrix.BitMatrix;

/**
 * This implementation can detect and decode Data Matrix codes in an image.
 * 
 */
final class DataMatrixReader implements Reader {

	private static final ResultPoint[] NO_POINTS = new ResultPoint[0];

	private final DataMatrixDecoder decoder = new DataMatrixDecoder();

	/**
	 * Locates and decodes a Data Matrix code in an image.
	 * 
	 * @return a String representing the content encoded by the Data Matrix code
	 * @throws BarcodeNotFoundException
	 *             if a Data Matrix code cannot be found
	 * @throws FormatException
	 *             if a Data Matrix code cannot be decoded
	 * @throws BarCodeChecksumException
	 *             if error correction fails
	 */
	@Override
	public Result decode(BinaryBitmap image) throws BarcodeNotFoundException,
			BarCodeChecksumException, FormatException {
		return decode(image, null);
	}

	@Override
	public Result decode(BinaryBitmap image, Map<DecodeHintType, ?> hints)
			throws BarcodeNotFoundException, BarCodeChecksumException,
			FormatException {
		DecoderResult decoderResult;
		ResultPoint[] points;
		if (hints != null && hints.containsKey(DecodeHintType.PURE_BARCODE)) {
			BitMatrix bits = null;
			try {
				bits = extractPureBits(image.getBlackMatrix());
			} catch (ToToBitmapException e) {
				e.printStackTrace();
				throw new BarcodeNotFoundException();
			}
			decoderResult = decoder.decode(bits);
			points = NO_POINTS;
		} else {
			DetectorResult detectorResult = null;
			try {
				detectorResult = new DataMatrixDetector(
                        image.getBlackMatrix()).detect();
			} catch (ToToBitmapException e) {
				e.printStackTrace();
				throw new BarcodeNotFoundException();
			}
			decoderResult = decoder.decode(detectorResult.getBits());
			points = detectorResult.getPoints();
		}
		Result result = new Result(decoderResult.getText(),
				decoderResult.getRawBytes(), points, BarcodeType.DATA_MATRIX);
		List<byte[]> byteSegments = decoderResult.getByteSegments();
		if (byteSegments != null) {
			result.putMetadata(ResultMetadataType.BYTE_SEGMENTS, byteSegments);
		}
		String ecLevel = decoderResult.getECLevel();
		if (ecLevel != null) {
			result.putMetadata(ResultMetadataType.ERROR_CORRECTION_LEVEL,
					ecLevel);
		}
		return result;
	}

	@Override
	public void reset() {
		// do nothing
	}

	/**
	 * This method detects a code in a "pure" image -- that is, pure monochrome
	 * image which contains only an unrotated, unskewed, image of a code, with
	 * some white border around it. This is a specialized method that works
	 * exceptionally fast in this special case.
	 * 
	 * @see QRCodeReader#extractPureBits(BitMatrix)
	 */
	private static BitMatrix extractPureBits(BitMatrix image)
			throws BarcodeNotFoundException {

		int[] leftTopBlack = image.getTopLeftOnBit();
		int[] rightBottomBlack = image.getBottomRightOnBit();
		if (leftTopBlack == null || rightBottomBlack == null) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		int moduleSize = moduleSize(leftTopBlack, image);

		int top = leftTopBlack[1];
		int bottom = rightBottomBlack[1];
		int left = leftTopBlack[0];
		int right = rightBottomBlack[0];

		int matrixWidth = (right - left + 1) / moduleSize;
		int matrixHeight = (bottom - top + 1) / moduleSize;
		if (matrixWidth <= 0 || matrixHeight <= 0) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		// Push in the "border" by half the module width so that we start
		// sampling in the middle of the module. Just in case the image is a
		// little off, this will help recover.
		int nudge = moduleSize >> 1;
		top += nudge;
		left += nudge;

		// Now just read off the bits
		BitMatrix bits = new BitMatrix(matrixWidth, matrixHeight);
		for (int y = 0; y < matrixHeight; y++) {
			int iOffset = top + y * moduleSize;
			for (int x = 0; x < matrixWidth; x++) {
				if (image.get(left + x * moduleSize, iOffset)) {
					bits.set(x, y);
				}
			}
		}
		return bits;
	}

	private static int moduleSize(int[] leftTopBlack, BitMatrix image)
			throws BarcodeNotFoundException {
		int width = image.getWidth();
		int x = leftTopBlack[0];
		int y = leftTopBlack[1];
		while (x < width && image.get(x, y)) {
			x++;
		}
		if (x == width) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		int moduleSize = x - leftTopBlack[0];
		if (moduleSize == 0) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}
		return moduleSize;
	}

}