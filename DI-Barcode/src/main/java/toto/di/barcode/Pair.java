package toto.di.barcode;

final class Pair extends DataCharacter {

	private final FinderPattern finderPattern;
	private int count;

	Pair(final int value, final int checksumPortion,
			final FinderPattern finderPattern) {
		super(value, checksumPortion);
		this.finderPattern = finderPattern;
	}

	FinderPattern getFinderPattern() {
		return finderPattern;
	}

	int getCount() {
		return count;
	}

	void incrementCount() {
		count++;
	}

}