package toto.di.barcode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import toto.util.collections.list.BitArray;
import android.content.res.Resources.NotFoundException;

/**
 * Decodes RSS-14, including truncated and stacked variants. See ISO/IEC
 * 24724:2006.
 */
final class RSS14Reader extends AbstractRSSReader {

	private static final int[] OUTSIDE_EVEN_TOTAL_SUBSET = { 1, 10, 34, 70, 126 };
	private static final int[] INSIDE_ODD_TOTAL_SUBSET = { 4, 20, 48, 81 };
	private static final int[] OUTSIDE_GSUM = { 0, 161, 961, 2015, 2715 };
	private static final int[] INSIDE_GSUM = { 0, 336, 1036, 1516 };
	private static final int[] OUTSIDE_ODD_WIDEST = { 8, 6, 4, 3, 1 };
	private static final int[] INSIDE_ODD_WIDEST = { 2, 4, 6, 8 };

	private static final int[][] FINDER_PATTERNS = { { 3, 8, 2, 1 },
			{ 3, 5, 5, 1 }, { 3, 3, 7, 1 }, { 3, 1, 9, 1 }, { 2, 7, 4, 1 },
			{ 2, 5, 6, 1 }, { 2, 3, 8, 1 }, { 1, 5, 7, 1 }, { 1, 3, 9, 1 }, };

	private final List<Pair> possibleLeftPairs;
	private final List<Pair> possibleRightPairs;

	public RSS14Reader() {
		possibleLeftPairs = new ArrayList<Pair>();
		possibleRightPairs = new ArrayList<Pair>();
	}

	@Override
	public Result decodeRow(final int rowNumber, final BitArray row,
			final Map<DecodeHintType, ?> hints) throws RBarcodeException,
			BarcodeNotFoundException {
		final Pair leftPair = decodePair(row, false, rowNumber, hints);
		addOrTally(possibleLeftPairs, leftPair);
		row.reverse();
		final Pair rightPair = decodePair(row, true, rowNumber, hints);
		addOrTally(possibleRightPairs, rightPair);
		row.reverse();
		final int lefSize = possibleLeftPairs.size();
		for (int i = 0; i < lefSize; i++) {
			final Pair left = possibleLeftPairs.get(i);
			if (left.getCount() > 1) {
				final int rightSize = possibleRightPairs.size();
				for (int j = 0; j < rightSize; j++) {
					final Pair right = possibleRightPairs.get(j);
					if (right.getCount() > 1) {
						if (checkChecksum(left, right)) {
							return constructResult(left, right);
						}
					}
				}
			}
		}
		throw new RBarcodeException("Not Found");
	}

	private static void addOrTally(final Collection<Pair> possiblePairs,
			final Pair pair) {
		if (pair == null) {
			return;
		}
		boolean found = false;
		for (final Pair other : possiblePairs) {
			if (other.getValue() == pair.getValue()) {
				other.incrementCount();
				found = true;
				break;
			}
		}
		if (!found) {
			possiblePairs.add(pair);
		}
	}

	@Override
	public void reset() {
		possibleLeftPairs.clear();
		possibleRightPairs.clear();
	}

	private static Result constructResult(final Pair leftPair,
			final Pair rightPair) {
		final long symbolValue = 4537077L * leftPair.getValue()
				+ rightPair.getValue();
		final String text = String.valueOf(symbolValue);

		final StringBuilder buffer = new StringBuilder(14);
		for (int i = 13 - text.length(); i > 0; i--) {
			buffer.append('0');
		}
		buffer.append(text);

		int checkDigit = 0;
		for (int i = 0; i < 13; i++) {
			final int digit = buffer.charAt(i) - '0';
			checkDigit += (i & 0x01) == 0 ? 3 * digit : digit;
		}
		checkDigit = 10 - (checkDigit % 10);
		if (checkDigit == 10) {
			checkDigit = 0;
		}
		buffer.append(checkDigit);

		final ResultPoint[] leftPoints = leftPair.getFinderPattern()
				.getResultPoints();
		final ResultPoint[] rightPoints = rightPair.getFinderPattern()
				.getResultPoints();
		return new Result(String.valueOf(buffer.toString()), null,
				new ResultPoint[] { leftPoints[0], leftPoints[1],
						rightPoints[0], rightPoints[1], }, BarcodeType.RSS_14);
	}

	private static boolean checkChecksum(final Pair leftPair,
			final Pair rightPair) {
		// int leftFPValue = leftPair.getFinderPattern().getValue();
		// int rightFPValue = rightPair.getFinderPattern().getValue();
		// if ((leftFPValue == 0 && rightFPValue == 8) ||
		// (leftFPValue == 8 && rightFPValue == 0)) {
		// }
		final int checkValue = (leftPair.getChecksumPortion() + 16 * rightPair
				.getChecksumPortion()) % 79;
		int targetCheckValue = 9 * leftPair.getFinderPattern().getValue()
				+ rightPair.getFinderPattern().getValue();
		if (targetCheckValue > 72) {
			targetCheckValue--;
		}
		if (targetCheckValue > 8) {
			targetCheckValue--;
		}
		return checkValue == targetCheckValue;
	}

	private Pair decodePair(final BitArray row, final boolean right,
			final int rowNumber, final Map<DecodeHintType, ?> hints)
			throws BarcodeNotFoundException {
		try {
			final int[] startEnd = findFinderPattern(row, 0, right);
			final FinderPattern pattern = parseFoundFinderPattern(row,
					rowNumber, right, startEnd);

			final ResultPointCallback resultPointCallback = hints == null ? null
					: (ResultPointCallback) hints
							.get(DecodeHintType.NEED_RESULT_POINT_CALLBACK);

			if (resultPointCallback != null) {
				float center = (startEnd[0] + startEnd[1]) / 2.0f;
				if (right) {
					// row is actually reversed
					center = row.getSize() - 1 - center;
				}
				resultPointCallback.foundPossibleResultPoint(new ResultPoint(
						center, rowNumber));
			}

			final DataCharacter outside = decodeDataCharacter(row, pattern,
					true);
			final DataCharacter inside = decodeDataCharacter(row, pattern,
					false);
			return new Pair(1597 * outside.getValue() + inside.getValue(),
					outside.getChecksumPortion() + 4
							* inside.getChecksumPortion(), pattern);
		} catch (final NotFoundException ignored) {
			return null;
		}
	}

	private DataCharacter decodeDataCharacter(final BitArray row,
			final FinderPattern pattern, final boolean outsideChar)
			throws BarcodeNotFoundException {

		final int[] counters = getDataCharacterCounters();
		counters[0] = 0;
		counters[1] = 0;
		counters[2] = 0;
		counters[3] = 0;
		counters[4] = 0;
		counters[5] = 0;
		counters[6] = 0;
		counters[7] = 0;

		if (outsideChar) {
			recordPatternInReverse(row, pattern.getStartEnd()[0], counters);
		} else {
			recordPattern(row, pattern.getStartEnd()[1] + 1, counters);
			// reverse it
			for (int i = 0, j = counters.length - 1; i < j; i++, j--) {
				final int temp = counters[i];
				counters[i] = counters[j];
				counters[j] = temp;
			}
		}

		final int numModules = outsideChar ? 16 : 15;
		final float elementWidth = (float) count(counters) / (float) numModules;

		final int[] oddCounts = this.getOddCounts();
		final int[] evenCounts = this.getEvenCounts();
		final float[] oddRoundingErrors = this.getOddRoundingErrors();
		final float[] evenRoundingErrors = this.getEvenRoundingErrors();

		for (int i = 0; i < counters.length; i++) {
			final float value = counters[i] / elementWidth;
			int count = (int) (value + 0.5f); // Round
			if (count < 1) {
				count = 1;
			} else if (count > 8) {
				count = 8;
			}
			final int offset = i >> 1;
			if ((i & 0x01) == 0) {
				oddCounts[offset] = count;
				oddRoundingErrors[offset] = value - count;
			} else {
				evenCounts[offset] = count;
				evenRoundingErrors[offset] = value - count;
			}
		}

		adjustOddEvenCounts(outsideChar, numModules);

		int oddSum = 0;
		int oddChecksumPortion = 0;
		for (int i = oddCounts.length - 1; i >= 0; i--) {
			oddChecksumPortion *= 9;
			oddChecksumPortion += oddCounts[i];
			oddSum += oddCounts[i];
		}
		int evenChecksumPortion = 0;
		int evenSum = 0;
		for (int i = evenCounts.length - 1; i >= 0; i--) {
			evenChecksumPortion *= 9;
			evenChecksumPortion += evenCounts[i];
			evenSum += evenCounts[i];
		}
		final int checksumPortion = oddChecksumPortion + 3
				* evenChecksumPortion;

		if (outsideChar) {
			if ((oddSum & 0x01) != 0 || oddSum > 12 || oddSum < 4) {
				throw new RBarcodeException("Not Found");
			}
			final int group = (12 - oddSum) / 2;
			final int oddWidest = OUTSIDE_ODD_WIDEST[group];
			final int evenWidest = 9 - oddWidest;
			final int vOdd = getRSSvalue(oddCounts, oddWidest, false);
			final int vEven = getRSSvalue(evenCounts, evenWidest, true);
			final int tEven = OUTSIDE_EVEN_TOTAL_SUBSET[group];
			final int gSum = OUTSIDE_GSUM[group];
			return new DataCharacter(vOdd * tEven + vEven + gSum,
					checksumPortion);
		} else {
			if ((evenSum & 0x01) != 0 || evenSum > 10 || evenSum < 4) {
				throw new RBarcodeException("Not Found");
			}
			final int group = (10 - evenSum) / 2;
			final int oddWidest = INSIDE_ODD_WIDEST[group];
			final int evenWidest = 9 - oddWidest;
			final int vOdd = getRSSvalue(oddCounts, oddWidest, true);
			final int vEven = getRSSvalue(evenCounts, evenWidest, false);
			final int tOdd = INSIDE_ODD_TOTAL_SUBSET[group];
			final int gSum = INSIDE_GSUM[group];
			return new DataCharacter(vEven * tOdd + vOdd + gSum,
					checksumPortion);
		}

	}

	private int[] findFinderPattern(final BitArray row, int rowOffset,
			final boolean rightFinderPattern) throws NotFoundException {

		final int[] counters = getDecodeFinderCounters();
		counters[0] = 0;
		counters[1] = 0;
		counters[2] = 0;
		counters[3] = 0;

		final int width = row.getSize();
		boolean isWhite = false;
		while (rowOffset < width) {
			isWhite = !row.get(rowOffset);
			if (rightFinderPattern == isWhite) {
				// Will encounter white first when searching for right finder
				// pattern
				break;
			}
			rowOffset++;
		}

		int counterPosition = 0;
		int patternStart = rowOffset;
		for (int x = rowOffset; x < width; x++) {
			if (row.get(x) ^ isWhite) {
				counters[counterPosition]++;
			} else {
				if (counterPosition == 3) {
					if (isFinderPattern(counters)) {
						return new int[] { patternStart, x };
					}
					patternStart += counters[0] + counters[1];
					counters[0] = counters[2];
					counters[1] = counters[3];
					counters[2] = 0;
					counters[3] = 0;
					counterPosition--;
				} else {
					counterPosition++;
				}
				counters[counterPosition] = 1;
				isWhite = !isWhite;
			}
		}
		throw new RBarcodeException("Not Found");

	}

	private FinderPattern parseFoundFinderPattern(final BitArray row,
			final int rowNumber, final boolean right, final int[] startEnd)
			throws NotFoundException {
		// Actually we found elements 2-5
		final boolean firstIsBlack = row.get(startEnd[0]);
		int firstElementStart = startEnd[0] - 1;
		// Locate element 1
		while (firstElementStart >= 0 && firstIsBlack
				^ row.get(firstElementStart)) {
			firstElementStart--;
		}
		firstElementStart++;
		final int firstCounter = startEnd[0] - firstElementStart;
		// Make 'counters' hold 1-4
		final int[] counters = getDecodeFinderCounters();
		System.arraycopy(counters, 0, counters, 1, counters.length - 1);
		counters[0] = firstCounter;
		final int value = parseFinderValue(counters, FINDER_PATTERNS);
		int start = firstElementStart;
		int end = startEnd[1];
		if (right) {
			// row is actually reversed
			start = row.getSize() - 1 - start;
			end = row.getSize() - 1 - end;
		}
		return new FinderPattern(value, new int[] { firstElementStart,
				startEnd[1] }, start, end, rowNumber);
	}

	private void adjustOddEvenCounts(final boolean outsideChar,
			final int numModules) throws NotFoundException {

		final int oddSum = count(getOddCounts());
		final int evenSum = count(getEvenCounts());
		final int mismatch = oddSum + evenSum - numModules;
		final boolean oddParityBad = (oddSum & 0x01) == (outsideChar ? 1 : 0);
		final boolean evenParityBad = (evenSum & 0x01) == 1;

		boolean incrementOdd = false;
		boolean decrementOdd = false;
		boolean incrementEven = false;
		boolean decrementEven = false;

		if (outsideChar) {
			if (oddSum > 12) {
				decrementOdd = true;
			} else if (oddSum < 4) {
				incrementOdd = true;
			}
			if (evenSum > 12) {
				decrementEven = true;
			} else if (evenSum < 4) {
				incrementEven = true;
			}
		} else {
			if (oddSum > 11) {
				decrementOdd = true;
			} else if (oddSum < 5) {
				incrementOdd = true;
			}
			if (evenSum > 10) {
				decrementEven = true;
			} else if (evenSum < 4) {
				incrementEven = true;
			}
		}

		/*
		 * if (mismatch == 2) { if (!(oddParityBad && evenParityBad)) { throw
		 * ReaderException.getInstance(); } decrementOdd = true; decrementEven =
		 * true; } else if (mismatch == -2) { if (!(oddParityBad &&
		 * evenParityBad)) { throw ReaderException.getInstance(); } incrementOdd
		 * = true; incrementEven = true; } else
		 */if (mismatch == 1) {
			if (oddParityBad) {
				if (evenParityBad) {
					new RBarcodeException("Not Found");
				}
				decrementOdd = true;
			} else {
				if (!evenParityBad) {
					new RBarcodeException("Not Found");
				}
				decrementEven = true;
			}
		} else if (mismatch == -1) {
			if (oddParityBad) {
				if (evenParityBad) {
					new RBarcodeException("Not Found");
				}
				incrementOdd = true;
			} else {
				if (!evenParityBad) {
					new RBarcodeException("Not Found");
				}
				incrementEven = true;
			}
		} else if (mismatch == 0) {
			if (oddParityBad) {
				if (!evenParityBad) {
					new RBarcodeException("Not Found");
				}
				// Both bad
				if (oddSum < evenSum) {
					incrementOdd = true;
					decrementEven = true;
				} else {
					decrementOdd = true;
					incrementEven = true;
				}
			} else {
				if (evenParityBad) {
					new RBarcodeException("Not Found");
				}
				// Nothing to do!
			}
		} else {
			new RBarcodeException("Not Found");
		}

		if (incrementOdd) {
			if (decrementOdd) {
				new RBarcodeException("Not Found");
			}
			increment(getOddCounts(), getOddRoundingErrors());
		}
		if (decrementOdd) {
			decrement(getOddCounts(), getOddRoundingErrors());
		}
		if (incrementEven) {
			if (decrementEven) {
				new RBarcodeException("Not Found");
			}
			increment(getEvenCounts(), getOddRoundingErrors());
		}
		if (decrementEven) {
			decrement(getEvenCounts(), getEvenRoundingErrors());
		}

	}

	public static int getRSSvalue(final int[] widths, final int maxWidth,
			final boolean noNarrow) {
		final int elements = widths.length;
		int n = 0;
		for (final int width : widths) {
			n += width;
		}
		int val = 0;
		int narrowMask = 0;
		for (int bar = 0; bar < elements - 1; bar++) {
			int elmWidth;
			for (elmWidth = 1, narrowMask |= 1 << bar; elmWidth < widths[bar]; elmWidth++, narrowMask &= ~(1 << bar)) {
				int subVal = combins(n - elmWidth - 1, elements - bar - 2);
				if (noNarrow
						&& (narrowMask == 0)
						&& (n - elmWidth - (elements - bar - 1) >= elements
								- bar - 1)) {
					subVal -= combins(n - elmWidth - (elements - bar), elements
							- bar - 2);
				}
				if (elements - bar - 1 > 1) {
					int lessVal = 0;
					for (int mxwElement = n - elmWidth - (elements - bar - 2); mxwElement > maxWidth; mxwElement--) {
						lessVal += combins(n - elmWidth - mxwElement - 1,
								elements - bar - 3);
					}
					subVal -= lessVal * (elements - 1 - bar);
				} else if (n - elmWidth > maxWidth) {
					subVal--;
				}
				val += subVal;
			}
			n -= elmWidth;
		}
		return val;
	}

	private static int combins(final int n, final int r) {
		int maxDenom;
		int minDenom;
		if (n - r > r) {
			minDenom = r;
			maxDenom = n - r;
		} else {
			minDenom = n - r;
			maxDenom = r;
		}
		int val = 1;
		int j = 1;
		for (int i = n; i > maxDenom; i--) {
			val *= i;
			if (j <= minDenom) {
				val /= j;
				j++;
			}
		}
		while (j <= minDenom) {
			val /= j;
			j++;
		}
		return val;
	}
}