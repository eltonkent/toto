package toto.di.barcode;

enum PDF417Compaction {

	AUTO, TEXT, BYTE, NUMERIC

}