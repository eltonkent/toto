package toto.di.barcode;

import toto.util.collections.matrix.ByteMatrix;

/**
 */
final class QRCode {

	static final int NUM_MASK_PATTERNS = 8;

	private QRMode mode;
	private QRErrorCorrectionLevel ecLevel;
	private QRVersion version;
	private int maskPattern;
	private ByteMatrix matrix;

	QRCode() {
		maskPattern = -1;
	}

	QRMode getMode() {
		return mode;
	}

	QRErrorCorrectionLevel getECLevel() {
		return ecLevel;
	}

	QRVersion getVersion() {
		return version;
	}

	int getMaskPattern() {
		return maskPattern;
	}

	ByteMatrix getMatrix() {
		return matrix;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(200);
		result.append("<<\n");
		result.append(" mode: ");
		result.append(mode);
		result.append("\n ecLevel: ");
		result.append(ecLevel);
		result.append("\n version: ");
		result.append(version);
		result.append("\n maskPattern: ");
		result.append(maskPattern);
		if (matrix == null) {
			result.append("\n matrix: null\n");
		} else {
			result.append("\n matrix:\n");
			result.append(matrix.toString());
		}
		result.append(">>\n");
		return result.toString();
	}

	void setMode(QRMode value) {
		mode = value;
	}

	void setECLevel(QRErrorCorrectionLevel value) {
		ecLevel = value;
	}

	void setVersion(QRVersion version) {
		this.version = version;
	}

	void setMaskPattern(int value) {
		maskPattern = value;
	}

	void setMatrix(ByteMatrix value) {
		matrix = value;
	}

	// Check if "mask_pattern" is valid.
	static boolean isValidMaskPattern(int maskPattern) {
		return maskPattern >= 0 && maskPattern < NUM_MASK_PATTERNS;
	}

}
