package toto.di.barcode;


/**
 * <p>
 * A field based on powers of a generator integer, modulo some modulus.
 * </p>
 * 
 */
final class PDF417ModulusGF {

	public static final PDF417ModulusGF PDF417_GF = new PDF417ModulusGF(
			PDF417Common.NUMBER_OF_CODEWORDS, 3);

	private final int[] expTable;
	private final int[] logTable;
	private final PDF417ModulusPoly zero;
	private final PDF417ModulusPoly one;
	private final int modulus;

	private PDF417ModulusGF(int modulus, int generator) {
		this.modulus = modulus;
		expTable = new int[modulus];
		logTable = new int[modulus];
		int x = 1;
		for (int i = 0; i < modulus; i++) {
			expTable[i] = x;
			x = (x * generator) % modulus;
		}
		for (int i = 0; i < modulus - 1; i++) {
			logTable[expTable[i]] = i;
		}
		// logTable[0] == 0 but this should never be used
		zero = new PDF417ModulusPoly(this, new int[] { 0 });
		one = new PDF417ModulusPoly(this, new int[] { 1 });
	}

	PDF417ModulusPoly getZero() {
		return zero;
	}

	PDF417ModulusPoly getOne() {
		return one;
	}

	PDF417ModulusPoly buildMonomial(int degree, int coefficient) {
		if (degree < 0) {
			throw new IllegalArgumentException();
		}
		if (coefficient == 0) {
			return zero;
		}
		int[] coefficients = new int[degree + 1];
		coefficients[0] = coefficient;
		return new PDF417ModulusPoly(this, coefficients);
	}

	int add(int a, int b) {
		return (a + b) % modulus;
	}

	int subtract(int a, int b) {
		return (modulus + a - b) % modulus;
	}

	int exp(int a) {
		return expTable[a];
	}

	int log(int a) {
		if (a == 0) {
			throw new IllegalArgumentException();
		}
		return logTable[a];
	}

	int inverse(int a) {
		if (a == 0) {
			throw new ArithmeticException();
		}
		return expTable[modulus - logTable[a] - 1];
	}

	int multiply(int a, int b) {
		if (a == 0 || b == 0) {
			return 0;
		}
		return expTable[(logTable[a] + logTable[b]) % (modulus - 1)];
	}

	int getSize() {
		return modulus;
	}

}
