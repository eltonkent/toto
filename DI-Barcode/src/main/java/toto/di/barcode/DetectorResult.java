package toto.di.barcode;

import toto.util.collections.matrix.BitMatrix;

/**
 * <p>
 * Encapsulates the result of detecting a barcode in an image. This includes the
 * raw matrix of black/white pixels corresponding to the barcode, and possibly
 * points of interest in the image, like the location of finder patterns or
 * corners of the barcode in the image.
 * </p>
 * 
 */
class DetectorResult {

	private final BitMatrix bits;
	private final ResultPoint[] points;

	DetectorResult(BitMatrix bits, ResultPoint[] points) {
		this.bits = bits;
		this.points = points;
	}

	final BitMatrix getBits() {
		return bits;
	}

	final ResultPoint[] getPoints() {
		return points;
	}

}