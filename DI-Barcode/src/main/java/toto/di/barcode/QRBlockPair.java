package toto.di.barcode;

final class QRBlockPair {

	private final byte[] dataBytes;
	private final byte[] errorCorrectionBytes;

	QRBlockPair(byte[] data, byte[] errorCorrection) {
		dataBytes = data;
		errorCorrectionBytes = errorCorrection;
	}

	byte[] getDataBytes() {
		return dataBytes;
	}

	byte[] getErrorCorrectionBytes() {
		return errorCorrectionBytes;
	}

}
