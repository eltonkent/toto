package toto.di.barcode;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Map;

import toto.geom2d.Rectangle;
import toto.text.StringUtils;
import toto.util.collections.matrix.BitMatrix;
import toto.util.collections.matrix.ByteMatrix;

/**
 * This object renders a Data Matrix code as a BitMatrix 2D array of greyscale
 * values.
 * 
 * @author dswitkin@google.com (Daniel Switkin)
 * @author Guillaume Le Biller Added to zxing lib.
 */
final class DataMatrixWriter implements Writer {

	@Override
	public BitMatrix encode(String contents, BarcodeType format, int width,
			int height) {
		return encode(contents, format, width, height, null);
	}

	@Override
	public BitMatrix encode(String contents, BarcodeType format, int width,
			int height, Map<EncodeHintType, ?> hints) {

		if (StringUtils.isEmpty(contents)) {
			throw new InvalidBarcodeDataException("Found empty contents");
		}

		if (format != BarcodeType.DATA_MATRIX) {
			throw new InvalidBarcodeDataException(
					"Can only encode DATA_MATRIX, but got " + format);
		}

		if (width < 0 || height < 0) {
			throw new InvalidBarcodeDataException(
					"Requested dimensions are too small: " + width + 'x'
							+ height);
		}

		// Try to get force shape & min / max size
		SymbolShapeHint shape = SymbolShapeHint.FORCE_NONE;
		Rectangle minSize = null;
		Rectangle maxSize = null;
		if (hints != null) {
			SymbolShapeHint requestedShape = (SymbolShapeHint) hints
					.get(EncodeHintType.DATA_MATRIX_SHAPE);
			if (requestedShape != null) {
				shape = requestedShape;
			}
			Rectangle requestedMinSize = (Rectangle) hints
					.get(EncodeHintType.MIN_SIZE);
			if (requestedMinSize != null) {
				minSize = requestedMinSize;
			}
			Rectangle requestedMaxSize = (Rectangle) hints
					.get(EncodeHintType.MAX_SIZE);
			if (requestedMaxSize != null) {
				maxSize = requestedMaxSize;
			}
		}
		// Log.d("Size","minSize"+minSize);
		// Log.d("Size","minSize"+maxSize);
		// minSize=new Rectangle(0,0,width,height);
		// maxSize=new Rectangle(0,0,width,height);

		// 1. step: Data encodation
		String encoded = HighLevelEncoder.encodeHighLevel(contents, shape,
				minSize, maxSize);

		SymbolInfo symbolInfo = SymbolInfo.lookup(encoded.length(), shape,
				minSize, maxSize, true);

		// 2. step: ECC generation
		String codewords = ErrorCorrection.encodeECC200(encoded, symbolInfo);

		// 3. step: Module placement in Matrix
		DefaultPlacement placement = new DefaultPlacement(codewords,
				symbolInfo.getSymbolDataWidth(),
				symbolInfo.getSymbolDataHeight());
		placement.place();
		// 4. step: low-level encoding
		return encodeLowLevel(placement, symbolInfo);
	}

	/**
	 * Encode the given symbol info to a bit matrix.
	 * 
	 * @param placement
	 *            The DataMatrix placement.
	 * @param symbolInfo
	 *            The symbol info to encode.
	 * @return The bit matrix generated.
	 */
	private static BitMatrix encodeLowLevel(DefaultPlacement placement,
			SymbolInfo symbolInfo) {
		int symbolWidth = symbolInfo.getSymbolDataWidth();
		int symbolHeight = symbolInfo.getSymbolDataHeight();

		ByteMatrix matrix = new ByteMatrix(symbolInfo.getSymbolWidth(),
				symbolInfo.getSymbolHeight());

		int matrixY = 0;

		for (int y = 0; y < symbolHeight; y++) {
			// Fill the top edge with alternate 0 / 1
			int matrixX;
			if ((y % symbolInfo.matrixHeight) == 0) {
				matrixX = 0;
				for (int x = 0; x < symbolInfo.getSymbolWidth(); x++) {
					matrix.set(matrixX, matrixY, (x % 2) == 0);
					matrixX++;
				}
				matrixY++;
			}
			matrixX = 0;
			for (int x = 0; x < symbolWidth; x++) {
				// Fill the right edge with full 1
				if ((x % symbolInfo.matrixWidth) == 0) {
					matrix.set(matrixX, matrixY, true);
					matrixX++;
				}
				matrix.set(matrixX, matrixY, placement.getBit(x, y));
				matrixX++;
				// Fill the right edge with alternate 0 / 1
				if ((x % symbolInfo.matrixWidth) == symbolInfo.matrixWidth - 1) {
					matrix.set(matrixX, matrixY, (y % 2) == 0);
					matrixX++;
				}
			}
			matrixY++;
			// Fill the bottom edge with full 1
			if ((y % symbolInfo.matrixHeight) == symbolInfo.matrixHeight - 1) {
				matrixX = 0;
				for (int x = 0; x < symbolInfo.getSymbolWidth(); x++) {
					matrix.set(matrixX, matrixY, true);
					matrixX++;
				}
				matrixY++;
			}
		}

		return convertByteMatrixToBitMatrix(matrix);
	}

	/**
	 * Convert the ByteMatrix to BitMatrix.
	 * 
	 * @param matrix
	 *            The input matrix.
	 * @return The output matrix.
	 */
	private static BitMatrix convertByteMatrixToBitMatrix(ByteMatrix matrix) {
		int matrixWidgth = matrix.getWidth();
		int matrixHeight = matrix.getHeight();

		BitMatrix output = new BitMatrix(matrixWidgth, matrixHeight);
		output.clear();
		for (int i = 0; i < matrixWidgth; i++) {
			for (int j = 0; j < matrixHeight; j++) {
				// Zero is white in the bytematrix
				if (matrix.get(i, j) == 1) {
					output.set(i, j);
				}
			}
		}

		return output;
	}

	/**
	 * Enumeration for DataMatrix symbol shape hint. It can be used to force
	 * square or rectangular symbols.
	 */
	private static enum SymbolShapeHint {

		FORCE_NONE, FORCE_SQUARE, FORCE_RECTANGLE,

	}

	/**
	 * Symbol info table for DataMatrix.
	 * 
	 */
	private static class SymbolInfo {

		static final SymbolInfo[] PROD_SYMBOLS = {
				new SymbolInfo(false, 3, 5, 8, 8, 1),
				new SymbolInfo(false, 5, 7, 10, 10, 1),
				/* rect */new SymbolInfo(true, 5, 7, 16, 6, 1),
				new SymbolInfo(false, 8, 10, 12, 12, 1),
				/* rect */new SymbolInfo(true, 10, 11, 14, 6, 2),
				new SymbolInfo(false, 12, 12, 14, 14, 1),
				/* rect */new SymbolInfo(true, 16, 14, 24, 10, 1),

				new SymbolInfo(false, 18, 14, 16, 16, 1),
				new SymbolInfo(false, 22, 18, 18, 18, 1),
				/* rect */new SymbolInfo(true, 22, 18, 16, 10, 2),
				new SymbolInfo(false, 30, 20, 20, 20, 1),
				/* rect */new SymbolInfo(true, 32, 24, 16, 14, 2),
				new SymbolInfo(false, 36, 24, 22, 22, 1),
				new SymbolInfo(false, 44, 28, 24, 24, 1),
				/* rect */new SymbolInfo(true, 49, 28, 22, 14, 2),

				new SymbolInfo(false, 62, 36, 14, 14, 4),
				new SymbolInfo(false, 86, 42, 16, 16, 4),
				new SymbolInfo(false, 114, 48, 18, 18, 4),
				new SymbolInfo(false, 144, 56, 20, 20, 4),
				new SymbolInfo(false, 174, 68, 22, 22, 4),

				new SymbolInfo(false, 204, 84, 24, 24, 4, 102, 42),
				new SymbolInfo(false, 280, 112, 14, 14, 16, 140, 56),
				new SymbolInfo(false, 368, 144, 16, 16, 16, 92, 36),
				new SymbolInfo(false, 456, 192, 18, 18, 16, 114, 48),
				new SymbolInfo(false, 576, 224, 20, 20, 16, 144, 56),
				new SymbolInfo(false, 696, 272, 22, 22, 16, 174, 68),
				new SymbolInfo(false, 816, 336, 24, 24, 16, 136, 56),
				new SymbolInfo(false, 1050, 408, 18, 18, 36, 175, 68),
				new SymbolInfo(false, 1304, 496, 20, 20, 36, 163, 62),
				new DataMatrixSymbolInfo144(), };

		private static SymbolInfo[] symbols = PROD_SYMBOLS;

		/**
		 * Overrides the symbol info setKey used by this class. Used for testing
		 * purposes.
		 * 
		 * @param override
		 *            the symbol info setKey to use
		 */
		static void overrideSymbolSet(SymbolInfo[] override) {
			symbols = override;
		}

		private final boolean rectangular;
		private final int dataCapacity;
		private final int errorCodewords;
		final int matrixWidth;
		final int matrixHeight;
		private final int dataRegions;
		private final int rsBlockData;
		private final int rsBlockError;

		SymbolInfo(boolean rectangular, int dataCapacity, int errorCodewords,
				int matrixWidth, int matrixHeight, int dataRegions) {
			this(rectangular, dataCapacity, errorCodewords, matrixWidth,
					matrixHeight, dataRegions, dataCapacity, errorCodewords);
		}

		SymbolInfo(boolean rectangular, int dataCapacity, int errorCodewords,
				int matrixWidth, int matrixHeight, int dataRegions,
				int rsBlockData, int rsBlockError) {
			this.rectangular = rectangular;
			this.dataCapacity = dataCapacity;
			this.errorCodewords = errorCodewords;
			this.matrixWidth = matrixWidth;
			this.matrixHeight = matrixHeight;
			this.dataRegions = dataRegions;
			this.rsBlockData = rsBlockData;
			this.rsBlockError = rsBlockError;
		}

		static SymbolInfo lookup(int dataCodewords) {
			return lookup(dataCodewords, SymbolShapeHint.FORCE_NONE, true);
		}

		static SymbolInfo lookup(int dataCodewords, SymbolShapeHint shape) {
			return lookup(dataCodewords, shape, true);
		}

		static SymbolInfo lookup(int dataCodewords, boolean allowRectangular,
				boolean fail) {
			SymbolShapeHint shape = allowRectangular ? SymbolShapeHint.FORCE_NONE
					: SymbolShapeHint.FORCE_SQUARE;
			return lookup(dataCodewords, shape, fail);
		}

		private static SymbolInfo lookup(int dataCodewords,
				SymbolShapeHint shape, boolean fail) {
			return lookup(dataCodewords, shape, null, null, fail);
		}

		static SymbolInfo lookup(int dataCodewords, SymbolShapeHint shape,
				Rectangle minSize, Rectangle maxSize, boolean fail) {
			for (SymbolInfo symbol : symbols) {
				if (shape == SymbolShapeHint.FORCE_SQUARE && symbol.rectangular) {
					continue;
				}
				if (shape == SymbolShapeHint.FORCE_RECTANGLE
						&& !symbol.rectangular) {
					continue;
				}
				if (minSize != null
						&& (symbol.getSymbolWidth() < minSize.getWidth() || symbol
								.getSymbolHeight() < minSize.getHeight())) {
					continue;
				}
				if (maxSize != null
						&& (symbol.getSymbolWidth() > maxSize.getWidth() || symbol
								.getSymbolHeight() > maxSize.getHeight())) {
					continue;
				}
				if (dataCodewords <= symbol.dataCapacity) {
					return symbol;
				}
			}
			if (fail) {
				throw new IllegalArgumentException(
						"Can't find a symbol arrangement that matches the message. Data codewords: "
								+ dataCodewords);
			}
			return null;
		}

		final int getHorizontalDataRegions() {
			switch (dataRegions) {
			case 1:
				return 1;
			case 2:
				return 2;
			case 4:
				return 2;
			case 16:
				return 4;
			case 36:
				return 6;
			default:
				throw new IllegalStateException(
						"Cannot handle this number of data regions");
			}
		}

		final int getVerticalDataRegions() {
			switch (dataRegions) {
			case 1:
				return 1;
			case 2:
				return 1;
			case 4:
				return 2;
			case 16:
				return 4;
			case 36:
				return 6;
			default:
				throw new IllegalStateException(
						"Cannot handle this number of data regions");
			}
		}

		final int getSymbolDataWidth() {
			return getHorizontalDataRegions() * matrixWidth;
		}

		final int getSymbolDataHeight() {
			return getVerticalDataRegions() * matrixHeight;
		}

		final int getSymbolWidth() {
			return getSymbolDataWidth() + (getHorizontalDataRegions() * 2);
		}

		final int getSymbolHeight() {
			return getSymbolDataHeight() + (getVerticalDataRegions() * 2);
		}

		int getCodewordCount() {
			return dataCapacity + errorCodewords;
		}

		int getInterleavedBlockCount() {
			return dataCapacity / rsBlockData;
		}

		final int getDataCapacity() {
			return dataCapacity;
		}

		final int getErrorCodewords() {
			return errorCodewords;
		}

		int getDataLengthForInterleavedBlock(int index) {
			return rsBlockData;
		}

		final int getErrorLengthForInterleavedBlock(int index) {
			return rsBlockError;
		}

		@Override
		public final String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(rectangular ? "Rectangular Symbol:" : "Square Symbol:");
			sb.append(" data region ").append(matrixWidth).append('x')
					.append(matrixHeight);
			sb.append(", symbol size ").append(getSymbolWidth()).append('x')
					.append(getSymbolHeight());
			sb.append(", symbol data size ").append(getSymbolDataWidth())
					.append('x').append(getSymbolDataHeight());
			sb.append(", codewords ").append(dataCapacity).append('+')
					.append(errorCodewords);
			return sb.toString();
		}

	}

	private static class DataMatrixSymbolInfo144 extends SymbolInfo {

		DataMatrixSymbolInfo144() {
			super(false, 1558, 620, 22, 22, 36, -1, 62);
		}

		@Override
		int getInterleavedBlockCount() {
			return 10;
		}

		@Override
		int getDataLengthForInterleavedBlock(int index) {
			return (index <= 8) ? 156 : 155;
		}

	}

	/**
	 * Error Correction Code for ECC200.
	 */
	private static final class ErrorCorrection {

		/**
		 * Lookup table which factors to use for which number of error
		 * correction codewords. See FACTORS.
		 */
		private static final int[] FACTOR_SETS = { 5, 7, 10, 11, 12, 14, 18,
				20, 24, 28, 36, 42, 48, 56, 62, 68 };

		/**
		 * Precomputed polynomial factors for ECC 200.
		 */
		private static final int[][] FACTORS = {
				{ 228, 48, 15, 111, 62 },
				{ 23, 68, 144, 134, 240, 92, 254 },
				{ 28, 24, 185, 166, 223, 248, 116, 255, 110, 61 },
				{ 175, 138, 205, 12, 194, 168, 39, 245, 60, 97, 120 },
				{ 41, 153, 158, 91, 61, 42, 142, 213, 97, 178, 100, 242 },
				{ 156, 97, 192, 252, 95, 9, 157, 119, 138, 45, 18, 186, 83, 185 },
				{ 83, 195, 100, 39, 188, 75, 66, 61, 241, 213, 109, 129, 94,
						254, 225, 48, 90, 188 },
				{ 15, 195, 244, 9, 233, 71, 168, 2, 188, 160, 153, 145, 253,
						79, 108, 82, 27, 174, 186, 172 },
				{ 52, 190, 88, 205, 109, 39, 176, 21, 155, 197, 251, 223, 155,
						21, 5, 172, 254, 124, 12, 181, 184, 96, 50, 193 },
				{ 211, 231, 43, 97, 71, 96, 103, 174, 37, 151, 170, 53, 75, 34,
						249, 121, 17, 138, 110, 213, 141, 136, 120, 151, 233,
						168, 93, 255 },
				{ 245, 127, 242, 218, 130, 250, 162, 181, 102, 120, 84, 179,
						220, 251, 80, 182, 229, 18, 2, 4, 68, 33, 101, 137, 95,
						119, 115, 44, 175, 184, 59, 25, 225, 98, 81, 112 },
				{ 77, 193, 137, 31, 19, 38, 22, 153, 247, 105, 122, 2, 245,
						133, 242, 8, 175, 95, 100, 9, 167, 105, 214, 111, 57,
						121, 21, 1, 253, 57, 54, 101, 248, 202, 69, 50, 150,
						177, 226, 5, 9, 5 },
				{ 245, 132, 172, 223, 96, 32, 117, 22, 238, 133, 238, 231, 205,
						188, 237, 87, 191, 106, 16, 147, 118, 23, 37, 90, 170,
						205, 131, 88, 120, 100, 66, 138, 186, 240, 82, 44, 176,
						87, 187, 147, 160, 175, 69, 213, 92, 253, 225, 19 },
				{ 175, 9, 223, 238, 12, 17, 220, 208, 100, 29, 175, 170, 230,
						192, 215, 235, 150, 159, 36, 223, 38, 200, 132, 54,
						228, 146, 218, 234, 117, 203, 29, 232, 144, 238, 22,
						150, 201, 117, 62, 207, 164, 13, 137, 245, 127, 67,
						247, 28, 155, 43, 203, 107, 233, 53, 143, 46 },
				{ 242, 93, 169, 50, 144, 210, 39, 118, 202, 188, 201, 189, 143,
						108, 196, 37, 185, 112, 134, 230, 245, 63, 197, 190,
						250, 106, 185, 221, 175, 64, 114, 71, 161, 44, 147, 6,
						27, 218, 51, 63, 87, 10, 40, 130, 188, 17, 163, 31,
						176, 170, 4, 107, 232, 7, 94, 166, 224, 124, 86, 47,
						11, 204 },
				{ 220, 228, 173, 89, 251, 149, 159, 56, 89, 33, 147, 244, 154,
						36, 73, 127, 213, 136, 248, 180, 234, 197, 158, 177,
						68, 122, 93, 213, 15, 160, 227, 236, 66, 139, 153, 185,
						202, 167, 179, 25, 220, 232, 96, 210, 231, 136, 223,
						239, 181, 241, 59, 52, 172, 25, 49, 232, 211, 189, 64,
						54, 108, 153, 132, 63, 96, 103, 82, 186 } };

		private static final int MODULO_VALUE = 0x12D;

		private static final int[] LOG;
		private static final int[] ALOG;

		static {
			// Create log and antilog table
			LOG = new int[256];
			ALOG = new int[255];

			int p = 1;
			for (int i = 0; i < 255; i++) {
				ALOG[i] = p;
				LOG[p] = i;
				p <<= 1;
				if (p >= 256) {
					p ^= MODULO_VALUE;
				}
			}
		}

		private ErrorCorrection() {
		}

		/**
		 * Creates the ECC200 error correction for an encoded message.
		 * 
		 * @param codewords
		 *            the codewords
		 * @param symbolInfo
		 *            information about the symbol to be encoded
		 * @return the codewords with interleaved error correction.
		 */
		static String encodeECC200(String codewords, SymbolInfo symbolInfo) {
			if (codewords.length() != symbolInfo.getDataCapacity()) {
				throw new IllegalArgumentException(
						"The number of codewords does not match the selected symbol");
			}
			StringBuilder sb = new StringBuilder(symbolInfo.getDataCapacity()
					+ symbolInfo.getErrorCodewords());
			sb.append(codewords);
			int blockCount = symbolInfo.getInterleavedBlockCount();
			if (blockCount == 1) {
				String ecc = createECCBlock(codewords,
						symbolInfo.getErrorCodewords());
				sb.append(ecc);
			} else {
				sb.setLength(sb.capacity());
				int[] dataSizes = new int[blockCount];
				int[] errorSizes = new int[blockCount];
				int[] startPos = new int[blockCount];
				for (int i = 0; i < blockCount; i++) {
					dataSizes[i] = symbolInfo
							.getDataLengthForInterleavedBlock(i + 1);
					errorSizes[i] = symbolInfo
							.getErrorLengthForInterleavedBlock(i + 1);
					startPos[i] = 0;
					if (i > 0) {
						startPos[i] = startPos[i - 1] + dataSizes[i];
					}
				}
				for (int block = 0; block < blockCount; block++) {
					StringBuilder temp = new StringBuilder(dataSizes[block]);
					for (int d = block; d < symbolInfo.getDataCapacity(); d += blockCount) {
						temp.append(codewords.charAt(d));
					}
					String ecc = createECCBlock(temp.toString(),
							errorSizes[block]);
					int pos = 0;
					for (int e = block; e < errorSizes[block] * blockCount; e += blockCount) {
						sb.setCharAt(symbolInfo.getDataCapacity() + e,
								ecc.charAt(pos++));
					}
				}
			}
			return sb.toString();

		}

		private static String createECCBlock(CharSequence codewords,
				int numECWords) {
			return createECCBlock(codewords, 0, codewords.length(), numECWords);
		}

		private static String createECCBlock(CharSequence codewords, int start,
				int len, int numECWords) {
			int table = -1;
			for (int i = 0; i < FACTOR_SETS.length; i++) {
				if (FACTOR_SETS[i] == numECWords) {
					table = i;
					break;
				}
			}
			if (table < 0) {
				throw new InvalidBarcodeDataException(
						"Illegal number of error correction codewords specified: "
								+ numECWords);
			}
			int[] poly = FACTORS[table];
			char[] ecc = new char[numECWords];
			for (int i = 0; i < numECWords; i++) {
				ecc[i] = 0;
			}
			for (int i = start; i < start + len; i++) {
				int m = ecc[numECWords - 1] ^ codewords.charAt(i);
				for (int k = numECWords - 1; k > 0; k--) {
					if (m != 0 && poly[k] != 0) {
						ecc[k] = (char) (ecc[k - 1] ^ ALOG[(LOG[m] + LOG[poly[k]]) % 255]);
					} else {
						ecc[k] = ecc[k - 1];
					}
				}
				if (m != 0 && poly[0] != 0) {
					ecc[0] = (char) ALOG[(LOG[m] + LOG[poly[0]]) % 255];
				} else {
					ecc[0] = 0;
				}
			}
			char[] eccReversed = new char[numECWords];
			for (int i = 0; i < numECWords; i++) {
				eccReversed[i] = ecc[numECWords - i - 1];
			}
			return String.valueOf(eccReversed);
		}

	}

	/**
	 * Symbol Character Placement Program. Adapted from Annex M.1 in ISO/IEC
	 * 16022:2000(E).
	 */
	private static class DefaultPlacement {

		private final CharSequence codewords;
		private final int numrows;
		private final int numcols;
		private final byte[] bits;

		/**
		 * Main constructor
		 * 
		 * @param codewords
		 *            the codewords to place
		 * @param numcols
		 *            the number of columns
		 * @param numrows
		 *            the number of rows
		 */
		DefaultPlacement(CharSequence codewords, int numcols, int numrows) {
			this.codewords = codewords;
			this.numcols = numcols;
			this.numrows = numrows;
			this.bits = new byte[numcols * numrows];
			Arrays.fill(this.bits, (byte) -1); // Initialize with "not setKey"
												// value
		}

		final int getNumrows() {
			return numrows;
		}

		final int getNumcols() {
			return numcols;
		}

		final byte[] getBits() {
			return bits;
		}

		final boolean getBit(int col, int row) {
			return bits[row * numcols + col] == 1;
		}

		final void setBit(int col, int row, boolean bit) {
			bits[row * numcols + col] = bit ? (byte) 1 : (byte) 0;
		}

		final boolean hasBit(int col, int row) {
			return bits[row * numcols + col] >= 0;
		}

		final void place() {
			int pos = 0;
			int row = 4;
			int col = 0;

			do {
				/*
				 * repeatedly first check for one of the special corner cases,
				 * then...
				 */
				if ((row == numrows) && (col == 0)) {
					corner1(pos++);
				}
				if ((row == numrows - 2) && (col == 0) && ((numcols % 4) != 0)) {
					corner2(pos++);
				}
				if ((row == numrows - 2) && (col == 0) && (numcols % 8 == 4)) {
					corner3(pos++);
				}
				if ((row == numrows + 4) && (col == 2) && ((numcols % 8) == 0)) {
					corner4(pos++);
				}
				/* sweep upward diagonally, inserting successive characters... */
				do {
					if ((row < numrows) && (col >= 0) && !hasBit(col, row)) {
						utah(row, col, pos++);
					}
					row -= 2;
					col += 2;
				} while (row >= 0 && (col < numcols));
				row++;
				col += 3;

				/*
				 * and then sweep downward diagonally, inserting successive
				 * characters, ...
				 */
				do {
					if ((row >= 0) && (col < numcols) && !hasBit(col, row)) {
						utah(row, col, pos++);
					}
					row += 2;
					col -= 2;
				} while ((row < numrows) && (col >= 0));
				row += 3;
				col++;

				/* ...until the entire array is scanned */
			} while ((row < numrows) || (col < numcols));

			/*
			 * Lastly, if the lower righthand corner is untouched, fill in fixed
			 * pattern
			 */
			if (!hasBit(numcols - 1, numrows - 1)) {
				setBit(numcols - 1, numrows - 1, true);
				setBit(numcols - 2, numrows - 2, true);
			}
		}

		private void module(int row, int col, int pos, int bit) {
			if (row < 0) {
				row += numrows;
				col += 4 - ((numrows + 4) % 8);
			}
			if (col < 0) {
				col += numcols;
				row += 4 - ((numcols + 4) % 8);
			}
			// Note the conversion:
			int v = codewords.charAt(pos);
			v &= 1 << (8 - bit);
			setBit(col, row, v != 0);
		}

		/**
		 * Places the 8 bits of a utah-shaped symbol character in ECC200.
		 * 
		 * @param row
		 *            the row
		 * @param col
		 *            the column
		 * @param pos
		 *            character position
		 */
		private void utah(int row, int col, int pos) {
			module(row - 2, col - 2, pos, 1);
			module(row - 2, col - 1, pos, 2);
			module(row - 1, col - 2, pos, 3);
			module(row - 1, col - 1, pos, 4);
			module(row - 1, col, pos, 5);
			module(row, col - 2, pos, 6);
			module(row, col - 1, pos, 7);
			module(row, col, pos, 8);
		}

		private void corner1(int pos) {
			module(numrows - 1, 0, pos, 1);
			module(numrows - 1, 1, pos, 2);
			module(numrows - 1, 2, pos, 3);
			module(0, numcols - 2, pos, 4);
			module(0, numcols - 1, pos, 5);
			module(1, numcols - 1, pos, 6);
			module(2, numcols - 1, pos, 7);
			module(3, numcols - 1, pos, 8);
		}

		private void corner2(int pos) {
			module(numrows - 3, 0, pos, 1);
			module(numrows - 2, 0, pos, 2);
			module(numrows - 1, 0, pos, 3);
			module(0, numcols - 4, pos, 4);
			module(0, numcols - 3, pos, 5);
			module(0, numcols - 2, pos, 6);
			module(0, numcols - 1, pos, 7);
			module(1, numcols - 1, pos, 8);
		}

		private void corner3(int pos) {
			module(numrows - 3, 0, pos, 1);
			module(numrows - 2, 0, pos, 2);
			module(numrows - 1, 0, pos, 3);
			module(0, numcols - 2, pos, 4);
			module(0, numcols - 1, pos, 5);
			module(1, numcols - 1, pos, 6);
			module(2, numcols - 1, pos, 7);
			module(3, numcols - 1, pos, 8);
		}

		private void corner4(int pos) {
			module(numrows - 1, 0, pos, 1);
			module(numrows - 1, numcols - 1, pos, 2);
			module(0, numcols - 3, pos, 3);
			module(0, numcols - 2, pos, 4);
			module(0, numcols - 1, pos, 5);
			module(1, numcols - 3, pos, 6);
			module(1, numcols - 2, pos, 7);
			module(1, numcols - 1, pos, 8);
		}

	}

	/**
	 * DataMatrix ECC 200 data encoder following the algorithm described in
	 * ISO/IEC 16022:200(E) in annex S.
	 */
	private static final class HighLevelEncoder {

		/**
		 * Padding character
		 */
		private static final char PAD = 129;
		/**
		 * mode latch to C40 encodation mode
		 */
		static final char LATCH_TO_C40 = 230;
		/**
		 * mode latch to Base 256 encodation mode
		 */
		static final char LATCH_TO_BASE256 = 231;
		/**
		 * FNC1 Codeword
		 */
		// private static final char FNC1 = 232;
		/**
		 * Structured Append Codeword
		 */
		// private static final char STRUCTURED_APPEND = 233;
		/**
		 * Reader Programming
		 */
		// private static final char READER_PROGRAMMING = 234;
		/**
		 * Upper Shift
		 */
		static final char UPPER_SHIFT = 235;
		/**
		 * 05 Macro
		 */
		private static final char MACRO_05 = 236;
		/**
		 * 06 Macro
		 */
		private static final char MACRO_06 = 237;
		/**
		 * mode latch to ANSI X.12 encodation mode
		 */
		static final char LATCH_TO_ANSIX12 = 238;
		/**
		 * mode latch to Text encodation mode
		 */
		static final char LATCH_TO_TEXT = 239;
		/**
		 * mode latch to EDIFACT encodation mode
		 */
		static final char LATCH_TO_EDIFACT = 240;
		/**
		 * ECI character (Extended Channel Interpretation)
		 */
		// private static final char ECI = 241;

		/**
		 * Unlatch from C40 encodation
		 */
		static final char C40_UNLATCH = 254;
		/**
		 * Unlatch from X12 encodation
		 */
		static final char X12_UNLATCH = 254;

		/**
		 * 05 Macro header
		 */
		private static final String MACRO_05_HEADER = "[)>\u001E05\u001D";
		/**
		 * 06 Macro header
		 */
		private static final String MACRO_06_HEADER = "[)>\u001E06\u001D";
		/**
		 * Macro trailer
		 */
		private static final String MACRO_TRAILER = "\u001E\u0004";

		static final int ASCII_ENCODATION = 0;
		static final int C40_ENCODATION = 1;
		static final int TEXT_ENCODATION = 2;
		static final int X12_ENCODATION = 3;
		static final int EDIFACT_ENCODATION = 4;
		static final int BASE256_ENCODATION = 5;

		private HighLevelEncoder() {
		}

		/*
		 * Converts the message to a byte array using the default encoding
		 * (cp437) as defined by the specification
		 * 
		 * @param msg the message
		 * 
		 * @return the byte array of the message
		 */

		/*
		 * static byte[] getBytesForMessage(String msg) { return
		 * msg.getBytes(Charset.forName("cp437")); //See 4.4.3 and annex B of
		 * ISO/IEC 15438:2001(E) }
		 */

		private static char randomize253State(char ch, int codewordPosition) {
			int pseudoRandom = ((149 * codewordPosition) % 253) + 1;
			int tempVariable = ch + pseudoRandom;
			return tempVariable <= 254 ? (char) tempVariable
					: (char) (tempVariable - 254);
		}

		/**
		 * Performs message encoding of a DataMatrix message using the algorithm
		 * described in annex P of ISO/IEC 16022:2000(E).
		 * 
		 * @param msg
		 *            the message
		 * @return the encoded message (the char values range from 0 to 255)
		 */
		static String encodeHighLevel(String msg) {
			return encodeHighLevel(msg, SymbolShapeHint.FORCE_NONE, null, null);
		}

		/**
		 * Performs message encoding of a DataMatrix message using the algorithm
		 * described in annex P of ISO/IEC 16022:2000(E).
		 * 
		 * @param msg
		 *            the message
		 * @param shape
		 *            requested shape. May be {@code SymbolShapeHint.FORCE_NONE}
		 *            , {@code SymbolShapeHint.FORCE_SQUARE} or
		 *            {@code SymbolShapeHint.FORCE_RECTANGLE}.
		 * @param minSize
		 *            the minimum symbol size constraint or null for no
		 *            constraint
		 * @param maxSize
		 *            the maximum symbol size constraint or null for no
		 *            constraint
		 * @return the encoded message (the char values range from 0 to 255)
		 */
		static String encodeHighLevel(String msg, SymbolShapeHint shape,
				Rectangle minSize, Rectangle maxSize) {
			// the codewords 0..255 are encoded as Unicode characters
			Encoder[] encoders = { new ASCIIEncoder(), new C40Encoder(),
					new TextEncoder(), new X12Encoder(), new EdifactEncoder(),
					new Base256Encoder() };

			EncoderContext context = new EncoderContext(msg);
			context.setSymbolShape(shape);
			context.setSizeConstraints(minSize, maxSize);

			if (msg.startsWith(MACRO_05_HEADER) && msg.endsWith(MACRO_TRAILER)) {
				context.writeCodeword(MACRO_05);
				context.setSkipAtEnd(2);
				context.pos += MACRO_05_HEADER.length();
			} else if (msg.startsWith(MACRO_06_HEADER)
					&& msg.endsWith(MACRO_TRAILER)) {
				context.writeCodeword(MACRO_06);
				context.setSkipAtEnd(2);
				context.pos += MACRO_06_HEADER.length();
			}

			int encodingMode = ASCII_ENCODATION; // Default mode
			while (context.hasMoreCharacters()) {
				encoders[encodingMode].encode(context);
				if (context.getNewEncoding() >= 0) {
					encodingMode = context.getNewEncoding();
					context.resetEncoderSignal();
				}
			}
			int len = context.getCodewordCount();
			context.updateSymbolInfo();
			int capacity = context.getSymbolInfo().getDataCapacity();
			if (len < capacity) {
				if (encodingMode != ASCII_ENCODATION
						&& encodingMode != BASE256_ENCODATION) {
					context.writeCodeword('\u00fe'); // Unlatch (254)
				}
			}
			// Padding
			StringBuilder codewords = context.getCodewords();
			if (codewords.length() < capacity) {
				codewords.append(PAD);
			}
			while (codewords.length() < capacity) {
				codewords
						.append(randomize253State(PAD, codewords.length() + 1));
			}

			return context.getCodewords().toString();
		}

		static int lookAheadTest(CharSequence msg, int startpos, int currentMode) {
			if (startpos >= msg.length()) {
				return currentMode;
			}
			float[] charCounts;
			// step J
			if (currentMode == ASCII_ENCODATION) {
				charCounts = new float[] { 0, 1, 1, 1, 1, 1.25f };
			} else {
				charCounts = new float[] { 1, 2, 2, 2, 2, 2.25f };
				charCounts[currentMode] = 0;
			}

			int charsProcessed = 0;
			while (true) {
				// step K
				if ((startpos + charsProcessed) == msg.length()) {
					int min = Integer.MAX_VALUE;
					byte[] mins = new byte[6];
					int[] intCharCounts = new int[6];
					min = findMinimums(charCounts, intCharCounts, min, mins);
					int minCount = getMinimumCount(mins);

					if (intCharCounts[ASCII_ENCODATION] == min) {
						return ASCII_ENCODATION;
					}
					if (minCount == 1 && mins[BASE256_ENCODATION] > 0) {
						return BASE256_ENCODATION;
					}
					if (minCount == 1 && mins[EDIFACT_ENCODATION] > 0) {
						return EDIFACT_ENCODATION;
					}
					if (minCount == 1 && mins[TEXT_ENCODATION] > 0) {
						return TEXT_ENCODATION;
					}
					if (minCount == 1 && mins[X12_ENCODATION] > 0) {
						return X12_ENCODATION;
					}
					return C40_ENCODATION;
				}

				char c = msg.charAt(startpos + charsProcessed);
				charsProcessed++;

				// step L
				if (isDigit(c)) {
					charCounts[ASCII_ENCODATION] += 0.5;
				} else if (isExtendedASCII(c)) {
					charCounts[ASCII_ENCODATION] = (int) Math
							.ceil(charCounts[ASCII_ENCODATION]);
					charCounts[ASCII_ENCODATION] += 2;
				} else {
					charCounts[ASCII_ENCODATION] = (int) Math
							.ceil(charCounts[ASCII_ENCODATION]);
					charCounts[ASCII_ENCODATION]++;
				}

				// step M
				if (isNativeC40(c)) {
					charCounts[C40_ENCODATION] += 2.0f / 3.0f;
				} else if (isExtendedASCII(c)) {
					charCounts[C40_ENCODATION] += 8.0f / 3.0f;
				} else {
					charCounts[C40_ENCODATION] += 4.0f / 3.0f;
				}

				// step N
				if (isNativeText(c)) {
					charCounts[TEXT_ENCODATION] += 2.0f / 3.0f;
				} else if (isExtendedASCII(c)) {
					charCounts[TEXT_ENCODATION] += 8.0f / 3.0f;
				} else {
					charCounts[TEXT_ENCODATION] += 4.0f / 3.0f;
				}

				// step O
				if (isNativeX12(c)) {
					charCounts[X12_ENCODATION] += 2.0f / 3.0f;
				} else if (isExtendedASCII(c)) {
					charCounts[X12_ENCODATION] += 13.0f / 3.0f;
				} else {
					charCounts[X12_ENCODATION] += 10.0f / 3.0f;
				}

				// step P
				if (isNativeEDIFACT(c)) {
					charCounts[EDIFACT_ENCODATION] += 3.0f / 4.0f;
				} else if (isExtendedASCII(c)) {
					charCounts[EDIFACT_ENCODATION] += 17.0f / 4.0f;
				} else {
					charCounts[EDIFACT_ENCODATION] += 13.0f / 4.0f;
				}

				// step Q
				if (isSpecialB256(c)) {
					charCounts[BASE256_ENCODATION] += 4;
				} else {
					charCounts[BASE256_ENCODATION]++;
				}

				// step R
				if (charsProcessed >= 4) {
					int[] intCharCounts = new int[6];
					byte[] mins = new byte[6];
					findMinimums(charCounts, intCharCounts, Integer.MAX_VALUE,
							mins);
					int minCount = getMinimumCount(mins);

					if (intCharCounts[ASCII_ENCODATION] < intCharCounts[BASE256_ENCODATION]
							&& intCharCounts[ASCII_ENCODATION] < intCharCounts[C40_ENCODATION]
							&& intCharCounts[ASCII_ENCODATION] < intCharCounts[TEXT_ENCODATION]
							&& intCharCounts[ASCII_ENCODATION] < intCharCounts[X12_ENCODATION]
							&& intCharCounts[ASCII_ENCODATION] < intCharCounts[EDIFACT_ENCODATION]) {
						return ASCII_ENCODATION;
					}
					if (intCharCounts[BASE256_ENCODATION] < intCharCounts[ASCII_ENCODATION]
							|| (mins[C40_ENCODATION] + mins[TEXT_ENCODATION]
									+ mins[X12_ENCODATION] + mins[EDIFACT_ENCODATION]) == 0) {
						return BASE256_ENCODATION;
					}
					if (minCount == 1 && mins[EDIFACT_ENCODATION] > 0) {
						return EDIFACT_ENCODATION;
					}
					if (minCount == 1 && mins[TEXT_ENCODATION] > 0) {
						return TEXT_ENCODATION;
					}
					if (minCount == 1 && mins[X12_ENCODATION] > 0) {
						return X12_ENCODATION;
					}
					if (intCharCounts[C40_ENCODATION] + 1 < intCharCounts[ASCII_ENCODATION]
							&& intCharCounts[C40_ENCODATION] + 1 < intCharCounts[BASE256_ENCODATION]
							&& intCharCounts[C40_ENCODATION] + 1 < intCharCounts[EDIFACT_ENCODATION]
							&& intCharCounts[C40_ENCODATION] + 1 < intCharCounts[TEXT_ENCODATION]) {
						if (intCharCounts[C40_ENCODATION] < intCharCounts[X12_ENCODATION]) {
							return C40_ENCODATION;
						}
						if (intCharCounts[C40_ENCODATION] == intCharCounts[X12_ENCODATION]) {
							int p = startpos + charsProcessed + 1;
							while (p < msg.length()) {
								char tc = msg.charAt(p);
								if (isX12TermSep(tc)) {
									return X12_ENCODATION;
								}
								if (!isNativeX12(tc)) {
									break;
								}
								p++;
							}
							return C40_ENCODATION;
						}
					}
				}
			}
		}

		private static int findMinimums(float[] charCounts,
				int[] intCharCounts, int min, byte[] mins) {
			Arrays.fill(mins, (byte) 0);
			for (int i = 0; i < 6; i++) {
				intCharCounts[i] = (int) Math.ceil(charCounts[i]);
				int current = intCharCounts[i];
				if (min > current) {
					min = current;
					Arrays.fill(mins, (byte) 0);
				}
				if (min == current) {
					mins[i]++;

				}
			}
			return min;
		}

		private static int getMinimumCount(byte[] mins) {
			int minCount = 0;
			for (int i = 0; i < 6; i++) {
				minCount += mins[i];
			}
			return minCount;
		}

		static boolean isDigit(char ch) {
			return ch >= '0' && ch <= '9';
		}

		static boolean isExtendedASCII(char ch) {
			return ch >= 128 && ch <= 255;
		}

		private static boolean isNativeC40(char ch) {
			return (ch == ' ') || (ch >= '0' && ch <= '9')
					|| (ch >= 'A' && ch <= 'Z');
		}

		private static boolean isNativeText(char ch) {
			return (ch == ' ') || (ch >= '0' && ch <= '9')
					|| (ch >= 'a' && ch <= 'z');
		}

		private static boolean isNativeX12(char ch) {
			return isX12TermSep(ch) || (ch == ' ') || (ch >= '0' && ch <= '9')
					|| (ch >= 'A' && ch <= 'Z');
		}

		private static boolean isX12TermSep(char ch) {
			return (ch == '\r') // CR
					|| (ch == '*') || (ch == '>');
		}

		private static boolean isNativeEDIFACT(char ch) {
			return ch >= ' ' && ch <= '^';
		}

		private static boolean isSpecialB256(char ch) {
			return false; // TODO NOT IMPLEMENTED YET!!!
		}

		/**
		 * Determines the number of consecutive characters that are encodable
		 * using numeric compaction.
		 * 
		 * @param msg
		 *            the message
		 * @param startpos
		 *            the start position within the message
		 * @return the requested character count
		 */
		static int determineConsecutiveDigitCount(CharSequence msg, int startpos) {
			int count = 0;
			int len = msg.length();
			int idx = startpos;
			if (idx < len) {
				char ch = msg.charAt(idx);
				while (isDigit(ch) && idx < len) {
					count++;
					idx++;
					if (idx < len) {
						ch = msg.charAt(idx);
					}
				}
			}
			return count;
		}

		static void illegalCharacter(char c) {
			String hex = Integer.toHexString(c);
			hex = "0000".substring(0, 4 - hex.length()) + hex;
			throw new IllegalArgumentException("Illegal character: " + c
					+ " (0x" + hex + ')');
		}

	}

	interface Encoder {

		int getEncodingMode();

		void encode(EncoderContext context);

	}

	private static final class EncoderContext {

		private final String msg;
		private SymbolShapeHint shape;
		private Rectangle minSize;
		private Rectangle maxSize;
		private final StringBuilder codewords;
		int pos;
		private int newEncoding;
		private SymbolInfo symbolInfo;
		private int skipAtEnd;

		EncoderContext(String msg) {
			// From this point on Strings are not Unicode anymore!
			byte[] msgBinary = new byte[0];
			try {
				msgBinary = msg.getBytes("ISO-8859-1");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			StringBuilder sb = new StringBuilder(msgBinary.length);
			for (int i = 0, c = msgBinary.length; i < c; i++) {
				char ch = (char) (msgBinary[i] & 0xff);
				if (ch == '?' && msg.charAt(i) != '?') {
					throw new IllegalArgumentException(
							"Message contains characters outside ISO-8859-1 encoding.");
				}
				sb.append(ch);
			}
			this.msg = sb.toString(); // Not Unicode here!
			shape = SymbolShapeHint.FORCE_NONE;
			this.codewords = new StringBuilder(msg.length());
			newEncoding = -1;
		}

		void setSymbolShape(SymbolShapeHint shape) {
			this.shape = shape;
		}

		void setSizeConstraints(Rectangle minSize, Rectangle maxSize) {
			this.minSize = minSize;
			this.maxSize = maxSize;
		}

		String getMessage() {
			return this.msg;
		}

		void setSkipAtEnd(int count) {
			this.skipAtEnd = count;
		}

		char getCurrentChar() {
			return msg.charAt(pos);
		}

		char getCurrent() {
			return msg.charAt(pos);
		}

		StringBuilder getCodewords() {
			return codewords;
		}

		void writeCodewords(String codewords) {
			this.codewords.append(codewords);
		}

		void writeCodeword(char codeword) {
			this.codewords.append(codeword);
		}

		int getCodewordCount() {
			return this.codewords.length();
		}

		int getNewEncoding() {
			return newEncoding;
		}

		void signalEncoderChange(int encoding) {
			this.newEncoding = encoding;
		}

		void resetEncoderSignal() {
			this.newEncoding = -1;
		}

		boolean hasMoreCharacters() {
			return pos < getTotalMessageCharCount();
		}

		private int getTotalMessageCharCount() {
			return msg.length() - skipAtEnd;
		}

		int getRemainingCharacters() {
			return getTotalMessageCharCount() - pos;
		}

		SymbolInfo getSymbolInfo() {
			return symbolInfo;
		}

		void updateSymbolInfo() {
			updateSymbolInfo(getCodewordCount());
		}

		void updateSymbolInfo(int len) {
			if (this.symbolInfo == null
					|| len > this.symbolInfo.getDataCapacity()) {
				this.symbolInfo = SymbolInfo.lookup(len, shape, minSize,
						maxSize, true);
			}
		}

		void resetSymbolInfo() {
			this.symbolInfo = null;
		}
	}

	private static final class ASCIIEncoder implements Encoder {

		@Override
		public int getEncodingMode() {
			return HighLevelEncoder.ASCII_ENCODATION;
		}

		@Override
		public void encode(EncoderContext context) {
			// step B
			int n = HighLevelEncoder.determineConsecutiveDigitCount(
					context.getMessage(), context.pos);
			if (n >= 2) {
				context.writeCodeword(encodeASCIIDigits(context.getMessage()
						.charAt(context.pos),
						context.getMessage().charAt(context.pos + 1)));
				context.pos += 2;
			} else {
				char c = context.getCurrentChar();
				int newMode = HighLevelEncoder.lookAheadTest(
						context.getMessage(), context.pos, getEncodingMode());
				if (newMode != getEncodingMode()) {
					switch (newMode) {
					case HighLevelEncoder.BASE256_ENCODATION:
						context.writeCodeword(HighLevelEncoder.LATCH_TO_BASE256);
						context.signalEncoderChange(HighLevelEncoder.BASE256_ENCODATION);
						return;
					case HighLevelEncoder.C40_ENCODATION:
						context.writeCodeword(HighLevelEncoder.LATCH_TO_C40);
						context.signalEncoderChange(HighLevelEncoder.C40_ENCODATION);
						return;
					case HighLevelEncoder.X12_ENCODATION:
						context.writeCodeword(HighLevelEncoder.LATCH_TO_ANSIX12);
						context.signalEncoderChange(HighLevelEncoder.X12_ENCODATION);
						break;
					case HighLevelEncoder.TEXT_ENCODATION:
						context.writeCodeword(HighLevelEncoder.LATCH_TO_TEXT);
						context.signalEncoderChange(HighLevelEncoder.TEXT_ENCODATION);
						break;
					case HighLevelEncoder.EDIFACT_ENCODATION:
						context.writeCodeword(HighLevelEncoder.LATCH_TO_EDIFACT);
						context.signalEncoderChange(HighLevelEncoder.EDIFACT_ENCODATION);
						break;
					default:
						throw new IllegalStateException("Illegal mode: "
								+ newMode);
					}
				} else if (HighLevelEncoder.isExtendedASCII(c)) {
					context.writeCodeword(HighLevelEncoder.UPPER_SHIFT);
					context.writeCodeword((char) (c - 128 + 1));
					context.pos++;
				} else {
					context.writeCodeword((char) (c + 1));
					context.pos++;
				}

			}
		}

		private char encodeASCIIDigits(char digit1, char digit2) {
			if (HighLevelEncoder.isDigit(digit1)
					&& HighLevelEncoder.isDigit(digit2)) {
				int num = (digit1 - 48) * 10 + (digit2 - 48);
				return (char) (num + 130);
			}
			throw new IllegalArgumentException("not digits: " + digit1 + digit2);
		}

	}

	private static final class X12Encoder extends C40Encoder {

		@Override
		public int getEncodingMode() {
			return HighLevelEncoder.X12_ENCODATION;
		}

		@Override
		public void encode(EncoderContext context) {
			// step C
			StringBuilder buffer = new StringBuilder();
			while (context.hasMoreCharacters()) {
				char c = context.getCurrentChar();
				context.pos++;

				encodeChar(c, buffer);

				int count = buffer.length();
				if ((count % 3) == 0) {
					writeNextTriplet(context, buffer);

					int newMode = HighLevelEncoder.lookAheadTest(
							context.getMessage(), context.pos,
							getEncodingMode());
					if (newMode != getEncodingMode()) {
						context.signalEncoderChange(newMode);
						break;
					}
				}
			}
			handleEOD(context, buffer);
		}

		@Override
		int encodeChar(char c, StringBuilder sb) {
			if (c == '\r') {
				sb.append('\0');
			} else if (c == '*') {
				sb.append('\1');
			} else if (c == '>') {
				sb.append('\2');
			} else if (c == ' ') {
				sb.append('\3');
			} else if (c >= '0' && c <= '9') {
				sb.append((char) (c - 48 + 4));
			} else if (c >= 'A' && c <= 'Z') {
				sb.append((char) (c - 65 + 14));
			} else {
				HighLevelEncoder.illegalCharacter(c);
			}
			return 1;
		}

		@Override
		void handleEOD(EncoderContext context, StringBuilder buffer) {
			context.updateSymbolInfo();
			int available = context.getSymbolInfo().getDataCapacity()
					- context.getCodewordCount();
			int count = buffer.length();
			if (count == 2) {
				context.writeCodeword(HighLevelEncoder.X12_UNLATCH);
				context.pos -= 2;
				context.signalEncoderChange(HighLevelEncoder.ASCII_ENCODATION);
			} else if (count == 1) {
				context.pos--;
				if (available > 1) {
					context.writeCodeword(HighLevelEncoder.X12_UNLATCH);
				}
				// NOP - No unlatch necessary
				context.signalEncoderChange(HighLevelEncoder.ASCII_ENCODATION);
			}
		}
	}

	private static class C40Encoder implements Encoder {

		@Override
		public int getEncodingMode() {
			return HighLevelEncoder.C40_ENCODATION;
		}

		@Override
		public void encode(EncoderContext context) {
			// step C
			StringBuilder buffer = new StringBuilder();
			while (context.hasMoreCharacters()) {
				char c = context.getCurrentChar();
				context.pos++;

				int lastCharSize = encodeChar(c, buffer);

				int unwritten = (buffer.length() / 3) * 2;

				int curCodewordCount = context.getCodewordCount() + unwritten;
				context.updateSymbolInfo(curCodewordCount);
				int available = context.getSymbolInfo().getDataCapacity()
						- curCodewordCount;

				if (!context.hasMoreCharacters()) {
					// Avoid having a single C40 value in the last triplet
					StringBuilder removed = new StringBuilder();
					if ((buffer.length() % 3) == 2) {
						if (available < 2 || available > 2) {
							lastCharSize = backtrackOneCharacter(context,
									buffer, removed, lastCharSize);
						}
					}
					while ((buffer.length() % 3) == 1
							&& ((lastCharSize <= 3 && available != 1) || lastCharSize > 3)) {
						lastCharSize = backtrackOneCharacter(context, buffer,
								removed, lastCharSize);
					}
					break;
				}

				int count = buffer.length();
				if ((count % 3) == 0) {
					int newMode = HighLevelEncoder.lookAheadTest(
							context.getMessage(), context.pos,
							getEncodingMode());
					if (newMode != getEncodingMode()) {
						context.signalEncoderChange(newMode);
						break;
					}
				}
			}
			handleEOD(context, buffer);
		}

		private int backtrackOneCharacter(EncoderContext context,
				StringBuilder buffer, StringBuilder removed, int lastCharSize) {
			int count = buffer.length();
			buffer.delete(count - lastCharSize, count);
			context.pos--;
			char c = context.getCurrentChar();
			lastCharSize = encodeChar(c, removed);
			context.resetSymbolInfo(); // Deal with possible reduction in symbol
										// size
			return lastCharSize;
		}

		static void writeNextTriplet(EncoderContext context,
				StringBuilder buffer) {
			context.writeCodewords(encodeToCodewords(buffer, 0));
			buffer.delete(0, 3);
		}

		/**
		 * Handle "end of data" situations
		 * 
		 * @param context
		 *            the encoder context
		 * @param buffer
		 *            the buffer with the remaining encoded characters
		 */
		void handleEOD(EncoderContext context, StringBuilder buffer) {
			int unwritten = (buffer.length() / 3) * 2;
			int rest = buffer.length() % 3;

			int curCodewordCount = context.getCodewordCount() + unwritten;
			context.updateSymbolInfo(curCodewordCount);
			int available = context.getSymbolInfo().getDataCapacity()
					- curCodewordCount;

			if (rest == 2) {
				buffer.append('\0'); // Shift 1
				while (buffer.length() >= 3) {
					writeNextTriplet(context, buffer);
				}
				if (context.hasMoreCharacters()) {
					context.writeCodeword(HighLevelEncoder.C40_UNLATCH);
				}
			} else if (available == 1 && rest == 1) {
				while (buffer.length() >= 3) {
					writeNextTriplet(context, buffer);
				}
				if (context.hasMoreCharacters()) {
					context.writeCodeword(HighLevelEncoder.C40_UNLATCH);
				}
				// else no unlatch
				context.pos--;
			} else if (rest == 0) {
				while (buffer.length() >= 3) {
					writeNextTriplet(context, buffer);
				}
				if (available > 0 || context.hasMoreCharacters()) {
					context.writeCodeword(HighLevelEncoder.C40_UNLATCH);
				}
			} else {
				throw new IllegalStateException(
						"Unexpected case. Please report!");
			}
			context.signalEncoderChange(HighLevelEncoder.ASCII_ENCODATION);
		}

		int encodeChar(char c, StringBuilder sb) {
			if (c == ' ') {
				sb.append('\3');
				return 1;
			} else if (c >= '0' && c <= '9') {
				sb.append((char) (c - 48 + 4));
				return 1;
			} else if (c >= 'A' && c <= 'Z') {
				sb.append((char) (c - 65 + 14));
				return 1;
			} else if (c >= '\0' && c <= '\u001f') {
				sb.append('\0'); // Shift 1 Set
				sb.append(c);
				return 2;
			} else if (c >= '!' && c <= '/') {
				sb.append('\1'); // Shift 2 Set
				sb.append((char) (c - 33));
				return 2;
			} else if (c >= ':' && c <= '@') {
				sb.append('\1'); // Shift 2 Set
				sb.append((char) (c - 58 + 15));
				return 2;
			} else if (c >= '[' && c <= '_') {
				sb.append('\1'); // Shift 2 Set
				sb.append((char) (c - 91 + 22));
				return 2;
			} else if (c >= '\u0060' && c <= '\u007f') {
				sb.append('\2'); // Shift 3 Set
				sb.append((char) (c - 96));
				return 2;
			} else if (c >= '\u0080') {
				sb.append("\1\u001e"); // Shift 2, Upper Shift
				int len = 2;
				len += encodeChar((char) (c - 128), sb);
				return len;
			} else {
				throw new IllegalArgumentException("Illegal character: " + c);
			}
		}

		private static String encodeToCodewords(CharSequence sb, int startPos) {
			char c1 = sb.charAt(startPos);
			char c2 = sb.charAt(startPos + 1);
			char c3 = sb.charAt(startPos + 2);
			int v = (1600 * c1) + (40 * c2) + c3 + 1;
			char cw1 = (char) (v / 256);
			char cw2 = (char) (v % 256);
			return new String(new char[] { cw1, cw2 });
		}

	}

	private static final class TextEncoder extends C40Encoder {

		@Override
		public int getEncodingMode() {
			return HighLevelEncoder.TEXT_ENCODATION;
		}

		@Override
		public int encodeChar(char c, StringBuilder sb) {
			if (c == ' ') {
				sb.append('\3');
				return 1;
			}
			if (c >= '0' && c <= '9') {
				sb.append((char) (c - 48 + 4));
				return 1;
			}
			if (c >= 'a' && c <= 'z') {
				sb.append((char) (c - 97 + 14));
				return 1;
			}
			if (c >= '\0' && c <= '\u001f') {
				sb.append('\0'); // Shift 1 Set
				sb.append(c);
				return 2;
			}
			if (c >= '!' && c <= '/') {
				sb.append('\1'); // Shift 2 Set
				sb.append((char) (c - 33));
				return 2;
			}
			if (c >= ':' && c <= '@') {
				sb.append('\1'); // Shift 2 Set
				sb.append((char) (c - 58 + 15));
				return 2;
			}
			if (c >= '[' && c <= '_') {
				sb.append('\1'); // Shift 2 Set
				sb.append((char) (c - 91 + 22));
				return 2;
			}
			if (c == '\u0060') {
				sb.append('\2'); // Shift 3 Set
				sb.append((char) (c - 96));
				return 2;
			}
			if (c >= 'A' && c <= 'Z') {
				sb.append('\2'); // Shift 3 Set
				sb.append((char) (c - 65 + 1));
				return 2;
			}
			if (c >= '{' && c <= '\u007f') {
				sb.append('\2'); // Shift 3 Set
				sb.append((char) (c - 123 + 27));
				return 2;
			}
			if (c >= '\u0080') {
				sb.append("\1\u001e"); // Shift 2, Upper Shift
				int len = 2;
				len += encodeChar((char) (c - 128), sb);
				return len;
			}
			HighLevelEncoder.illegalCharacter(c);
			return -1;
		}

	}

	private static final class EdifactEncoder implements Encoder {

		@Override
		public int getEncodingMode() {
			return HighLevelEncoder.EDIFACT_ENCODATION;
		}

		@Override
		public void encode(EncoderContext context) {
			// step F
			StringBuilder buffer = new StringBuilder();
			while (context.hasMoreCharacters()) {
				char c = context.getCurrentChar();
				encodeChar(c, buffer);
				context.pos++;

				int count = buffer.length();
				if (count >= 4) {
					context.writeCodewords(encodeToCodewords(buffer, 0));
					buffer.delete(0, 4);

					int newMode = HighLevelEncoder.lookAheadTest(
							context.getMessage(), context.pos,
							getEncodingMode());
					if (newMode != getEncodingMode()) {
						context.signalEncoderChange(HighLevelEncoder.ASCII_ENCODATION);
						break;
					}
				}
			}
			buffer.append((char) 31); // Unlatch
			handleEOD(context, buffer);
		}

		/**
		 * Handle "end of data" situations
		 * 
		 * @param context
		 *            the encoder context
		 * @param buffer
		 *            the buffer with the remaining encoded characters
		 */
		private static void handleEOD(EncoderContext context,
				CharSequence buffer) {
			try {
				int count = buffer.length();
				if (count == 0) {
					return; // Already finished
				}
				if (count == 1) {
					// Only an unlatch at the end
					context.updateSymbolInfo();
					int available = context.getSymbolInfo().getDataCapacity()
							- context.getCodewordCount();
					int remaining = context.getRemainingCharacters();
					if (remaining == 0 && available <= 2) {
						return; // No unlatch
					}
				}

				if (count > 4) {
					throw new IllegalStateException("Count must not exceed 4");
				}
				int restChars = count - 1;
				String encoded = encodeToCodewords(buffer, 0);
				boolean endOfSymbolReached = !context.hasMoreCharacters();
				boolean restInAscii = endOfSymbolReached && restChars <= 2;

				if (restChars <= 2) {
					context.updateSymbolInfo(context.getCodewordCount()
							+ restChars);
					int available = context.getSymbolInfo().getDataCapacity()
							- context.getCodewordCount();
					if (available >= 3) {
						restInAscii = false;
						context.updateSymbolInfo(context.getCodewordCount()
								+ encoded.length());
						// available = context.symbolInfo.dataCapacity -
						// context.getCodewordCount();
					}
				}

				if (restInAscii) {
					context.resetSymbolInfo();
					context.pos -= restChars;
				} else {
					context.writeCodewords(encoded);
				}
			} finally {
				context.signalEncoderChange(HighLevelEncoder.ASCII_ENCODATION);
			}
		}

		private static void encodeChar(char c, StringBuilder sb) {
			if (c >= ' ' && c <= '?') {
				sb.append(c);
			} else if (c >= '@' && c <= '^') {
				sb.append((char) (c - 64));
			} else {
				HighLevelEncoder.illegalCharacter(c);
			}
		}

		private static String encodeToCodewords(CharSequence sb, int startPos) {
			int len = sb.length() - startPos;
			if (len == 0) {
				throw new IllegalStateException(
						"StringBuilder must not be empty");
			}
			char c1 = sb.charAt(startPos);
			char c2 = len >= 2 ? sb.charAt(startPos + 1) : 0;
			char c3 = len >= 3 ? sb.charAt(startPos + 2) : 0;
			char c4 = len >= 4 ? sb.charAt(startPos + 3) : 0;

			int v = (c1 << 18) + (c2 << 12) + (c3 << 6) + c4;
			char cw1 = (char) ((v >> 16) & 255);
			char cw2 = (char) ((v >> 8) & 255);
			char cw3 = (char) (v & 255);
			StringBuilder res = new StringBuilder(3);
			res.append(cw1);
			if (len >= 2) {
				res.append(cw2);
			}
			if (len >= 3) {
				res.append(cw3);
			}
			return res.toString();
		}

	}

	private static final class Base256Encoder implements Encoder {

		@Override
		public int getEncodingMode() {
			return HighLevelEncoder.BASE256_ENCODATION;
		}

		@Override
		public void encode(EncoderContext context) {
			StringBuilder buffer = new StringBuilder();
			buffer.append('\0'); // Initialize length field
			while (context.hasMoreCharacters()) {
				char c = context.getCurrentChar();
				buffer.append(c);

				context.pos++;

				int newMode = HighLevelEncoder.lookAheadTest(
						context.getMessage(), context.pos, getEncodingMode());
				if (newMode != getEncodingMode()) {
					context.signalEncoderChange(newMode);
					break;
				}
			}
			int dataCount = buffer.length() - 1;
			int lengthFieldSize = 1;
			int currentSize = context.getCodewordCount() + dataCount
					+ lengthFieldSize;
			context.updateSymbolInfo(currentSize);
			boolean mustPad = (context.getSymbolInfo().getDataCapacity() - currentSize) > 0;
			if (context.hasMoreCharacters() || mustPad) {
				if (dataCount <= 249) {
					buffer.setCharAt(0, (char) dataCount);
				} else if (dataCount > 249 && dataCount <= 1555) {
					buffer.setCharAt(0, (char) ((dataCount / 250) + 249));
					buffer.insert(1, (char) (dataCount % 250));
				} else {
					throw new IllegalStateException(
							"Message length not in valid ranges: " + dataCount);
				}
			}
			for (int i = 0, c = buffer.length(); i < c; i++) {
				context.writeCodeword(randomize255State(buffer.charAt(i),
						context.getCodewordCount() + 1));
			}
		}

		private static char randomize255State(char ch, int codewordPosition) {
			int pseudoRandom = ((149 * codewordPosition) % 255) + 1;
			int tempVariable = ch + pseudoRandom;
			if (tempVariable <= 255) {
				return (char) tempVariable;
			} else {
				return (char) (tempVariable - 256);
			}
		}

	}

}
