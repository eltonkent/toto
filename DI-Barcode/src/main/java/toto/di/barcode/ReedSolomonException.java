package toto.di.barcode;

/**
 * <p>
 * Thrown when an exception occurs during Reed-Solomon decoding, such as when
 * there are too many errors to correct.
 * </p>
 * 
 */
final class ReedSolomonException extends Exception {

	ReedSolomonException(String message) {
		super(message);
	}

}