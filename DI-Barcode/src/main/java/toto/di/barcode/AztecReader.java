package toto.di.barcode;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import toto.bitmap.BinaryBitmap;
import toto.bitmap.ToToBitmapException;
import toto.math.MathUtils;
import toto.util.collections.matrix.BitMatrix;

/**
 * This implementation can detect and decode Aztec codes in an image.
 * 
 * @author David Olivier
 */
final class AztecReader implements Reader {

	/**
	 * Locates and decodes a Data Matrix code in an image.
	 * 
	 * @return a String representing the content encoded by the Data Matrix code
	 * @throws BarcodeNotFoundException
	 *             if a Data Matrix code cannot be found
	 * @throws FormatException
	 *             if a Data Matrix code cannot be decoded
	 */
	@Override
	public Result decode(BinaryBitmap image) throws BarcodeNotFoundException,
			FormatException {
		return decode(image, null);
	}

	@Override
	public Result decode(BinaryBitmap image, Map<DecodeHintType, ?> hints)
			throws BarcodeNotFoundException, FormatException {

		BarcodeNotFoundException notFoundException = null;
		FormatException formatException = null;
		Detector detector = null;
		try {
			detector = new Detector(image.getBlackMatrix());
		} catch (ToToBitmapException e) {
			e.printStackTrace();
			throw  new BarcodeNotFoundException();
		}
		ResultPoint[] points = null;
		DecoderResult decoderResult = null;
		try {
			AztecDetectorResult detectorResult = detector.detect(false);
			points = detectorResult.getPoints();
			decoderResult = new Decoder().decode(detectorResult);
		} catch (BarcodeNotFoundException e) {
			notFoundException = e;
		} catch (FormatException e) {
			formatException = e;
		}
		if (decoderResult == null) {
			try {
				AztecDetectorResult detectorResult = detector.detect(true);
				points = detectorResult.getPoints();
				decoderResult = new Decoder().decode(detectorResult);
			} catch (BarcodeNotFoundException e) {
				if (notFoundException != null) {
					throw notFoundException;
				}
				if (formatException != null) {
					throw formatException;
				}
				throw e;
			} catch (FormatException e) {
				// throw the exception from the non-mirror case, instead
				if (notFoundException != null) {
					throw notFoundException;
				}
				if (formatException != null) {
					throw formatException;
				}
				throw e;
			}
		}

		if (hints != null) {
			ResultPointCallback rpcb = (ResultPointCallback) hints
					.get(DecodeHintType.NEED_RESULT_POINT_CALLBACK);
			if (rpcb != null) {
				for (ResultPoint point : points) {
					rpcb.foundPossibleResultPoint(point);
				}
			}
		}

		Result result = new Result(decoderResult.getText(),
				decoderResult.getRawBytes(), points, BarcodeType.AZTEC);

		List<byte[]> byteSegments = decoderResult.getByteSegments();
		if (byteSegments != null) {
			result.putMetadata(ResultMetadataType.BYTE_SEGMENTS, byteSegments);
		}
		String ecLevel = decoderResult.getECLevel();
		if (ecLevel != null) {
			result.putMetadata(ResultMetadataType.ERROR_CORRECTION_LEVEL,
					ecLevel);
		}

		return result;
	}

	@Override
	public void reset() {
		// do nothing
	}

	static final class AztecDetectorResult extends DetectorResult {

		private final boolean compact;
		private final int nbDatablocks;
		private final int nbLayers;

		AztecDetectorResult(BitMatrix bits, ResultPoint[] points,
				boolean compact, int nbDatablocks, int nbLayers) {
			super(bits, points);
			this.compact = compact;
			this.nbDatablocks = nbDatablocks;
			this.nbLayers = nbLayers;
		}

		int getNbLayers() {
			return nbLayers;
		}

		int getNbDatablocks() {
			return nbDatablocks;
		}

		boolean isCompact() {
			return compact;
		}

	}

	/**
	 * <p>
	 * The main class which implements Aztec Code decoding -- as opposed to
	 * locating and extracting the Aztec Code from an image.
	 * </p>
	 * 
	 * @author David Olivier
	 */
	private static final class Decoder {

		private enum Table {
			UPPER, LOWER, MIXED, DIGIT, PUNCT, BINARY
		}

		private static final String[] UPPER_TABLE = { "CTRL_PS", " ", "A", "B",
				"C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
				"O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
				"CTRL_LL", "CTRL_ML", "CTRL_DL", "CTRL_BS" };

		private static final String[] LOWER_TABLE = { "CTRL_PS", " ", "a", "b",
				"c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
				"o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
				"CTRL_US", "CTRL_ML", "CTRL_DL", "CTRL_BS" };

		private static final String[] MIXED_TABLE = { "CTRL_PS", " ", "\1",
				"\2", "\3", "\4", "\5", "\6", "\7", "\b", "\t", "\n", "\13",
				"\f", "\r", "\33", "\34", "\35", "\36", "\37", "@", "\\", "^",
				"_", "`", "|", "~", "\177", "CTRL_LL", "CTRL_UL", "CTRL_PL",
				"CTRL_BS" };

		private static final String[] PUNCT_TABLE = { "", "\r", "\r\n", ". ",
				", ", ": ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*",
				"+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "[",
				"]", "{", "}", "CTRL_UL" };

		private static final String[] DIGIT_TABLE = { "CTRL_PS", " ", "0", "1",
				"2", "3", "4", "5", "6", "7", "8", "9", ",", ".", "CTRL_UL",
				"CTRL_US" };

		private AztecDetectorResult ddata;

		DecoderResult decode(AztecDetectorResult detectorResult)
				throws FormatException {
			ddata = detectorResult;
			BitMatrix matrix = detectorResult.getBits();
			boolean[] rawbits = extractBits(matrix);
			boolean[] correctedBits = correctBits(rawbits);
			String result = getEncodedData(correctedBits);
			return new DecoderResult(null, result, null, null);
		}

		// This method is used for testing the high-level encoder
		static String highLevelDecode(boolean[] correctedBits) {
			return getEncodedData(correctedBits);
		}

		/**
		 * Gets the string encoded in the aztec code bits
		 * 
		 * @return the decoded string
		 */
		private static String getEncodedData(boolean[] correctedBits) {
			int endIndex = correctedBits.length;
			Table latchTable = Table.UPPER; // table most recently latched to
			Table shiftTable = Table.UPPER; // table to use for the next read
			StringBuilder result = new StringBuilder(20);
			int index = 0;
			while (index < endIndex) {
				if (shiftTable == Table.BINARY) {
					if (endIndex - index < 5) {
						break;
					}
					int length = readCode(correctedBits, index, 5);
					index += 5;
					if (length == 0) {
						if (endIndex - index < 11) {
							break;
						}
						length = readCode(correctedBits, index, 11) + 31;
						index += 11;
					}
					for (int charCount = 0; charCount < length; charCount++) {
						if (endIndex - index < 8) {
							index = endIndex; // Force outer loop to exit
							break;
						}
						int code = readCode(correctedBits, index, 8);
						result.append((char) code);
						index += 8;
					}
					// Go back to whatever mode we had been in
					shiftTable = latchTable;
				} else {
					int size = shiftTable == Table.DIGIT ? 4 : 5;
					if (endIndex - index < size) {
						break;
					}
					int code = readCode(correctedBits, index, size);
					index += size;
					String str = getCharacter(shiftTable, code);
					if (str.startsWith("CTRL_")) {
						// Table changes
						shiftTable = getTable(str.charAt(5));
						if (str.charAt(6) == 'L') {
							latchTable = shiftTable;
						}
					} else {
						result.append(str);
						// Go back to whatever mode we had been in
						shiftTable = latchTable;
					}
				}
			}
			return result.toString();
		}

		/**
		 * gets the table corresponding to the char passed
		 */
		private static Table getTable(char t) {
			switch (t) {
			case 'L':
				return Table.LOWER;
			case 'P':
				return Table.PUNCT;
			case 'M':
				return Table.MIXED;
			case 'D':
				return Table.DIGIT;
			case 'B':
				return Table.BINARY;
			case 'U':
			default:
				return Table.UPPER;
			}
		}

		/**
		 * Gets the character (or string) corresponding to the passed code in
		 * the given table
		 * 
		 * @param table
		 *            the table used
		 * @param code
		 *            the code of the character
		 */
		private static String getCharacter(Table table, int code) {
			switch (table) {
			case UPPER:
				return UPPER_TABLE[code];
			case LOWER:
				return LOWER_TABLE[code];
			case MIXED:
				return MIXED_TABLE[code];
			case PUNCT:
				return PUNCT_TABLE[code];
			case DIGIT:
				return DIGIT_TABLE[code];
			default:
				// Should not reach here.
				throw new IllegalStateException("Bad table");
			}
		}

		/**
		 * <p>
		 * Performs RS error correction on an array of bits.
		 * </p>
		 * 
		 * @return the corrected array
		 * @throws FormatException
		 *             if the input contains too many errors
		 */
		private boolean[] correctBits(boolean[] rawbits) throws FormatException {
			GenericGF gf;
			int codewordSize;

			if (ddata.getNbLayers() <= 2) {
				codewordSize = 6;
				gf = GenericGF.AZTEC_DATA_6;
			} else if (ddata.getNbLayers() <= 8) {
				codewordSize = 8;
				gf = GenericGF.AZTEC_DATA_8;
			} else if (ddata.getNbLayers() <= 22) {
				codewordSize = 10;
				gf = GenericGF.AZTEC_DATA_10;
			} else {
				codewordSize = 12;
				gf = GenericGF.AZTEC_DATA_12;
			}

			int numDataCodewords = ddata.getNbDatablocks();
			int numCodewords = rawbits.length / codewordSize;
			int offset = rawbits.length % codewordSize;
			int numECCodewords = numCodewords - numDataCodewords;

			int[] dataWords = new int[numCodewords];
			for (int i = 0; i < numCodewords; i++, offset += codewordSize) {
				dataWords[i] = readCode(rawbits, offset, codewordSize);
			}

			try {
				ReedSolomonDecoder rsDecoder = new ReedSolomonDecoder(gf);
				rsDecoder.decode(dataWords, numECCodewords);
			} catch (ReedSolomonException ignored) {
				throw FormatException.getFormatInstance();
			}

			// Now perform the unstuffing operation.
			// First, count how many bits are going to be thrown out as stuffing
			int mask = (1 << codewordSize) - 1;
			int stuffedBits = 0;
			for (int i = 0; i < numDataCodewords; i++) {
				int dataWord = dataWords[i];
				if (dataWord == 0 || dataWord == mask) {
					throw FormatException.getFormatInstance();
				} else if (dataWord == 1 || dataWord == mask - 1) {
					stuffedBits++;
				}
			}
			// Now, actually unpack the bits and remove the stuffing
			boolean[] correctedBits = new boolean[numDataCodewords
					* codewordSize - stuffedBits];
			int index = 0;
			for (int i = 0; i < numDataCodewords; i++) {
				int dataWord = dataWords[i];
				if (dataWord == 1 || dataWord == mask - 1) {
					// next codewordSize-1 bits are all zeros or all ones
					Arrays.fill(correctedBits, index, index + codewordSize - 1,
							dataWord > 1);
					index += codewordSize - 1;
				} else {
					for (int bit = codewordSize - 1; bit >= 0; --bit) {
						correctedBits[index++] = (dataWord & (1 << bit)) != 0;
					}
				}
			}
			return correctedBits;
		}

		/**
		 * Gets the array of bits from an Aztec Code matrix
		 * 
		 * @return the array of bits
		 */
		boolean[] extractBits(BitMatrix matrix) {
			boolean compact = ddata.isCompact();
			int layers = ddata.getNbLayers();
			int baseMatrixSize = compact ? 11 + layers * 4 : 14 + layers * 4; // not
																				// including
																				// alignment
																				// lines
			int[] alignmentMap = new int[baseMatrixSize];
			boolean[] rawbits = new boolean[totalBitsInLayer(layers, compact)];

			if (compact) {
				for (int i = 0; i < alignmentMap.length; i++) {
					alignmentMap[i] = i;
				}
			} else {
				int matrixSize = baseMatrixSize + 1 + 2
						* ((baseMatrixSize / 2 - 1) / 15);
				int origCenter = baseMatrixSize / 2;
				int center = matrixSize / 2;
				for (int i = 0; i < origCenter; i++) {
					int newOffset = i + i / 15;
					alignmentMap[origCenter - i - 1] = center - newOffset - 1;
					alignmentMap[origCenter + i] = center + newOffset + 1;
				}
			}
			for (int i = 0, rowOffset = 0; i < layers; i++) {
				int rowSize = compact ? (layers - i) * 4 + 9
						: (layers - i) * 4 + 12;
				// The top-left most point of this layer is <low, low> (not
				// including alignment lines)
				int low = i * 2;
				// The bottom-right most point of this layer is <high, high>
				// (not including alignment lines)
				int high = baseMatrixSize - 1 - low;
				// We pull bits from the two 2 x rowSize columns and two rowSize
				// x 2 rows
				for (int j = 0; j < rowSize; j++) {
					int columnOffset = j * 2;
					for (int k = 0; k < 2; k++) {
						// left column
						rawbits[rowOffset + columnOffset + k] = matrix.get(
								alignmentMap[low + k], alignmentMap[low + j]);
						// bottom row
						rawbits[rowOffset + 2 * rowSize + columnOffset + k] = matrix
								.get(alignmentMap[low + j], alignmentMap[high
										- k]);
						// right column
						rawbits[rowOffset + 4 * rowSize + columnOffset + k] = matrix
								.get(alignmentMap[high - k], alignmentMap[high
										- j]);
						// top row
						rawbits[rowOffset + 6 * rowSize + columnOffset + k] = matrix
								.get(alignmentMap[high - j], alignmentMap[low
										+ k]);
					}
				}
				rowOffset += rowSize * 8;
			}
			return rawbits;
		}

		/**
		 * Reads a code of given length and at given index in an array of bits
		 */
		private static int readCode(boolean[] rawbits, int startIndex,
				int length) {
			int res = 0;
			for (int i = startIndex; i < startIndex + length; i++) {
				res <<= 1;
				if (rawbits[i]) {
					res++;
				}
			}
			return res;
		}

		private static int totalBitsInLayer(int layers, boolean compact) {
			return ((compact ? 88 : 112) + 16 * layers) * layers;
		}
	}

	/**
	 * Encapsulates logic that can detect an Aztec Code in an image, even if the
	 * Aztec Code is rotated or skewed, or partially obscured.
	 * 
	 * @author David Olivier
	 * @author Frank Yellin
	 */
	private static final class Detector {

		static final class Point {
			private final int x;
			private final int y;

			ResultPoint toResultPoint() {
				return new ResultPoint(getX(), getY());
			}

			Point(int x, int y) {
				this.x = x;
				this.y = y;
			}

			int getX() {
				return x;
			}

			int getY() {
				return y;
			}

			@Override
			public String toString() {
				return "<" + x + ' ' + y + '>';
			}
		}

		private final BitMatrix image;

		private boolean compact;
		private int nbLayers;
		private int nbDataBlocks;
		private int nbCenterLayers;
		private int shift;

		Detector(BitMatrix image) {
			this.image = image;
		}

		AztecDetectorResult detect() throws BarcodeNotFoundException {
			return detect(false);
		}

		/**
		 * Detects an Aztec Code in an image.
		 * 
		 * @return {@link AztecDetectorResult} encapsulating results of
		 *         detecting an Aztec Code
		 * @throws BarcodeNotFoundException
		 *             if no Aztec Code can be found
		 */
		AztecDetectorResult detect(boolean isMirror)
				throws BarcodeNotFoundException {

			// 1. Get the center of the aztec matrix
			Point pCenter = getMatrixCenter();

			// 2. Get the center points of the four diagonal points just outside
			// the bull's eye
			// [topRight, bottomRight, bottomLeft, topLeft]
			ResultPoint[] bullsEyeCorners = getBullsEyeCorners(pCenter);

			if (isMirror) {
				ResultPoint temp = bullsEyeCorners[0];
				bullsEyeCorners[0] = bullsEyeCorners[2];
				bullsEyeCorners[2] = temp;
			}

			// 3. Get the size of the matrix and other parameters from the
			// bull's eye
			extractParameters(bullsEyeCorners);

			// 4. Sample the grid
			BitMatrix bits = sampleGrid(image, bullsEyeCorners[shift % 4],
					bullsEyeCorners[(shift + 1) % 4],
					bullsEyeCorners[(shift + 2) % 4],
					bullsEyeCorners[(shift + 3) % 4]);

			// 5. Get the corners of the matrix.
			ResultPoint[] corners = getMatrixCornerPoints(bullsEyeCorners);

			return new AztecDetectorResult(bits, corners, compact,
					nbDataBlocks, nbLayers);
		}

		/**
		 * Extracts the number of data layers and data blocks from the layer
		 * around the bull's eye.
		 * 
		 * @param bullsEyeCorners
		 *            the array of bull's eye corners
		 * @throws BarcodeNotFoundException
		 *             in case of too many errors or invalid parameters
		 */
		private void extractParameters(ResultPoint[] bullsEyeCorners)
				throws BarcodeNotFoundException {
			if (!isValid(bullsEyeCorners[0]) || !isValid(bullsEyeCorners[1])
					|| !isValid(bullsEyeCorners[2])
					|| !isValid(bullsEyeCorners[3])) {
				throw BarcodeNotFoundException.getNotFoundInstance();
			}
			int length = 2 * nbCenterLayers;
			// Get the bits around the bull's eye
			int[] sides = {
					sampleLine(bullsEyeCorners[0], bullsEyeCorners[1], length), // Right
																				// side
					sampleLine(bullsEyeCorners[1], bullsEyeCorners[2], length), // Bottom
					sampleLine(bullsEyeCorners[2], bullsEyeCorners[3], length), // Left
																				// side
					sampleLine(bullsEyeCorners[3], bullsEyeCorners[0], length) // Top
			};

			// bullsEyeCorners[shift] is the corner of the bulls'eye that has
			// three
			// orientation marks.
			// sides[shift] is the row/column that goes from the corner with
			// three
			// orientation marks to the corner with two.
			shift = getRotation(sides, length);

			// Flatten the parameter bits into a single 28- or 40-bit long
			long parameterData = 0;
			for (int i = 0; i < 4; i++) {
				int side = sides[(shift + i) % 4];
				if (compact) {
					// Ea ch side of the form ..XXXXXXX. where Xs are parameter
					// data
					parameterData <<= 7;
					parameterData += (side >> 1) & 0x7F;
				} else {
					// Each side of the form ..XXXXX.XXXXX. where Xs are
					// parameter data
					parameterData <<= 10;
					parameterData += ((side >> 2) & (0x1f << 5))
							+ ((side >> 1) & 0x1F);
				}
			}

			// Corrects parameter data using RS. Returns just the data portion
			// without the error correction.
			int correctedData = getCorrectedParameterData(parameterData,
					compact);

			if (compact) {
				// 8 bits: 2 bits layers and 6 bits data blocks
				nbLayers = (correctedData >> 6) + 1;
				nbDataBlocks = (correctedData & 0x3F) + 1;
			} else {
				// 16 bits: 5 bits layers and 11 bits data blocks
				nbLayers = (correctedData >> 11) + 1;
				nbDataBlocks = (correctedData & 0x7FF) + 1;
			}
		}

		private static final int[] EXPECTED_CORNER_BITS = { 0xee0, // 07340 XXX
																	// .XX X..
																	// ...
				0x1dc, // 00734 ... XXX .XX X..
				0x83b, // 04073 X.. ... XXX .XX
				0x707, // 03407 .XX X.. ... XXX
		};

		private static int getRotation(int[] sides, int length)
				throws BarcodeNotFoundException {
			// In a normal pattern, we expect to See
			// ** .* D A
			// * *
			//
			// . *
			// .. .. C B
			//
			// Grab the 3 bits from each of the sides the form the locator
			// pattern and concatenate
			// into a 12-bit integer. Start with the bit at A
			int cornerBits = 0;
			for (int side : sides) {
				// XX......X where X's are orientation marks
				int t = ((side >> (length - 2)) << 1) + (side & 1);
				cornerBits = (cornerBits << 3) + t;
			}
			// Mov the bottom bit to the top, so that the three bits of the
			// locator pattern at A are
			// together. cornerBits is now:
			// 3 orientation bits at A || 3 orientation bits at B || ... || 3
			// orientation bits at D
			cornerBits = ((cornerBits & 1) << 11) + (cornerBits >> 1);
			// The result shift indicates which element of BullsEyeCorners[]
			// goes into the top-left
			// corner. Since the four rotation values have a Hamming distance of
			// 8, we
			// can easily tolerate two errors.
			for (int shift = 0; shift < 4; shift++) {
				if (Integer.bitCount(cornerBits ^ EXPECTED_CORNER_BITS[shift]) <= 2) {
					return shift;
				}
			}
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		/**
		 * Corrects the parameter bits using Reed-Solomon algorithm.
		 * 
		 * @param parameterData
		 *            parameter bits
		 * @param compact
		 *            true if this is a compact Aztec code
		 * @throws BarcodeNotFoundException
		 *             if the array contains too many errors
		 */
		private static int getCorrectedParameterData(long parameterData,
				boolean compact) throws BarcodeNotFoundException {
			int numCodewords;
			int numDataCodewords;

			if (compact) {
				numCodewords = 7;
				numDataCodewords = 2;
			} else {
				numCodewords = 10;
				numDataCodewords = 4;
			}

			int numECCodewords = numCodewords - numDataCodewords;
			int[] parameterWords = new int[numCodewords];
			for (int i = numCodewords - 1; i >= 0; --i) {
				parameterWords[i] = (int) parameterData & 0xF;
				parameterData >>= 4;
			}
			try {
				ReedSolomonDecoder rsDecoder = new ReedSolomonDecoder(
						GenericGF.AZTEC_PARAM);
				rsDecoder.decode(parameterWords, numECCodewords);
			} catch (ReedSolomonException ignored) {
				throw BarcodeNotFoundException.getNotFoundInstance();
			}
			// Toss the error correction. Just return the data as an integer
			int result = 0;
			for (int i = 0; i < numDataCodewords; i++) {
				result = (result << 4) + parameterWords[i];
			}
			return result;
		}

		/**
		 * Finds the corners of a bull-eye centered on the passed point. This
		 * returns the centers of the diagonal points just outside the bull's
		 * eye Returns [topRight, bottomRight, bottomLeft, topLeft]
		 * 
		 * @param pCenter
		 *            Center point
		 * @return The corners of the bull-eye
		 * @throws BarcodeNotFoundException
		 *             If no valid bull-eye can be found
		 */
		private ResultPoint[] getBullsEyeCorners(Point pCenter)
				throws BarcodeNotFoundException {

			Point pina = pCenter;
			Point pinb = pCenter;
			Point pinc = pCenter;
			Point pind = pCenter;

			boolean color = true;

			for (nbCenterLayers = 1; nbCenterLayers < 9; nbCenterLayers++) {
				Point pouta = getFirstDifferent(pina, color, 1, -1);
				Point poutb = getFirstDifferent(pinb, color, 1, 1);
				Point poutc = getFirstDifferent(pinc, color, -1, 1);
				Point poutd = getFirstDifferent(pind, color, -1, -1);

				// d a
				//
				// c b

				if (nbCenterLayers > 2) {
					float q = distance(poutd, pouta) * nbCenterLayers
							/ (distance(pind, pina) * (nbCenterLayers + 2));
					if (q < 0.75
							|| q > 1.25
							|| !isWhiteOrBlackRectangle(pouta, poutb, poutc,
									poutd)) {
						break;
					}
				}

				pina = pouta;
				pinb = poutb;
				pinc = poutc;
				pind = poutd;

				color = !color;
			}

			if (nbCenterLayers != 5 && nbCenterLayers != 7) {
				throw BarcodeNotFoundException.getNotFoundInstance();
			}

			compact = nbCenterLayers == 5;

			// Expand the square by .5 pixel in each direction so that we're on
			// the border
			// between the white square and the black square
			ResultPoint pinax = new ResultPoint(pina.getX() + 0.5f,
					pina.getY() - 0.5f);
			ResultPoint pinbx = new ResultPoint(pinb.getX() + 0.5f,
					pinb.getY() + 0.5f);
			ResultPoint pincx = new ResultPoint(pinc.getX() - 0.5f,
					pinc.getY() + 0.5f);
			ResultPoint pindx = new ResultPoint(pind.getX() - 0.5f,
					pind.getY() - 0.5f);

			// Expand the square so that its corners are the centers of the
			// points
			// just outside the bull's eye.
			return expandSquare(
					new ResultPoint[] { pinax, pinbx, pincx, pindx },
					2 * nbCenterLayers - 3, 2 * nbCenterLayers);
		}

		/**
		 * Finds a candidate center point of an Aztec code from an image
		 * 
		 * @return the center point
		 */
		private Point getMatrixCenter() {

			ResultPoint pointA;
			ResultPoint pointB;
			ResultPoint pointC;
			ResultPoint pointD;

			// Get a white rectangle that can be the border of the matrix in
			// center bull's eye or
			try {

				ResultPoint[] cornerPoints = new WhiteRectangleDetector(image)
						.detect();
				pointA = cornerPoints[0];
				pointB = cornerPoints[1];
				pointC = cornerPoints[2];
				pointD = cornerPoints[3];

			} catch (BarcodeNotFoundException e) {

				// This exception can be in case the initial rectangle is white
				// In that case, surely in the bull's eye, we try to expand the
				// rectangle.
				int cx = image.getWidth() / 2;
				int cy = image.getHeight() / 2;
				pointA = getFirstDifferent(new Point(cx + 7, cy - 7), false, 1,
						-1).toResultPoint();
				pointB = getFirstDifferent(new Point(cx + 7, cy + 7), false, 1,
						1).toResultPoint();
				pointC = getFirstDifferent(new Point(cx - 7, cy + 7), false,
						-1, 1).toResultPoint();
				pointD = getFirstDifferent(new Point(cx - 7, cy - 7), false,
						-1, -1).toResultPoint();

			}

			// Compute the center of the rectangle
			int cx = MathUtils.round((pointA.getX() + pointD.getX()
					+ pointB.getX() + pointC.getX()) / 4.0f);
			int cy = MathUtils.round((pointA.getY() + pointD.getY()
					+ pointB.getY() + pointC.getY()) / 4.0f);

			// Redetermine the white rectangle starting from previously computed
			// center.
			// This will ensure that we end up with a white rectangle in center
			// bull's eye
			// in order to compute a more accurate center.
			try {
				ResultPoint[] cornerPoints = new WhiteRectangleDetector(image,
						15, cx, cy).detect();
				pointA = cornerPoints[0];
				pointB = cornerPoints[1];
				pointC = cornerPoints[2];
				pointD = cornerPoints[3];
			} catch (BarcodeNotFoundException e) {
				// This exception can be in case the initial rectangle is white
				// In that case we try to expand the rectangle.
				pointA = getFirstDifferent(new Point(cx + 7, cy - 7), false, 1,
						-1).toResultPoint();
				pointB = getFirstDifferent(new Point(cx + 7, cy + 7), false, 1,
						1).toResultPoint();
				pointC = getFirstDifferent(new Point(cx - 7, cy + 7), false,
						-1, 1).toResultPoint();
				pointD = getFirstDifferent(new Point(cx - 7, cy - 7), false,
						-1, -1).toResultPoint();
			}

			// Recompute the center of the rectangle
			cx = MathUtils
					.round((pointA.getX() + pointD.getX() + pointB.getX() + pointC
							.getX()) / 4.0f);
			cy = MathUtils
					.round((pointA.getY() + pointD.getY() + pointB.getY() + pointC
							.getY()) / 4.0f);

			return new Point(cx, cy);
		}

		/**
		 * Gets the Aztec code corners from the bull's eye corners and the
		 * parameters.
		 * 
		 * @param bullsEyeCorners
		 *            the array of bull's eye corners
		 * @return the array of aztec code corners
		 */
		private ResultPoint[] getMatrixCornerPoints(
				ResultPoint[] bullsEyeCorners) {
			return expandSquare(bullsEyeCorners, 2 * nbCenterLayers,
					getDimension());
		}

		/**
		 * Creates a BitMatrix by sampling the provided image. topLeft,
		 * topRight, bottomRight, and bottomLeft are the centers of the squares
		 * on the diagonal just outside the bull's eye.
		 */
		private BitMatrix sampleGrid(BitMatrix image, ResultPoint topLeft,
				ResultPoint topRight, ResultPoint bottomRight,
				ResultPoint bottomLeft) throws BarcodeNotFoundException {

			GridSampler sampler = GridSampler.getInstance();
			int dimension = getDimension();

			float low = dimension / 2.0f - nbCenterLayers;
			float high = dimension / 2.0f + nbCenterLayers;

			return sampler.sampleGrid(image, dimension, dimension,
					low,
					low, // topleft
					high,
					low, // topright
					high,
					high, // bottomright
					low,
					high, // bottomleft
					topLeft.getX(), topLeft.getY(), topRight.getX(),
					topRight.getY(), bottomRight.getX(), bottomRight.getY(),
					bottomLeft.getX(), bottomLeft.getY());
		}

		/**
		 * Samples a line.
		 * 
		 * @param p1
		 *            start point (inclusive)
		 * @param p2
		 *            end point (exclusive)
		 * @param size
		 *            number of bits
		 * @return the array of bits as an int (first bit is high-order bit of
		 *         result)
		 */
		private int sampleLine(ResultPoint p1, ResultPoint p2, int size) {
			int result = 0;

			float d = distance(p1, p2);
			float moduleSize = d / size;
			float px = p1.getX();
			float py = p1.getY();
			float dx = moduleSize * (p2.getX() - p1.getX()) / d;
			float dy = moduleSize * (p2.getY() - p1.getY()) / d;
			for (int i = 0; i < size; i++) {
				if (image.get(MathUtils.round(px + i * dx),
						MathUtils.round(py + i * dy))) {
					result |= 1 << (size - i - 1);
				}
			}
			return result;
		}

		/**
		 * @return true if the border of the rectangle passed in parameter is
		 *         compound of white points only or black points only
		 */
		private boolean isWhiteOrBlackRectangle(Point p1, Point p2, Point p3,
				Point p4) {

			int corr = 3;

			p1 = new Point(p1.getX() - corr, p1.getY() + corr);
			p2 = new Point(p2.getX() - corr, p2.getY() - corr);
			p3 = new Point(p3.getX() + corr, p3.getY() - corr);
			p4 = new Point(p4.getX() + corr, p4.getY() + corr);

			int cInit = getColor(p4, p1);

			if (cInit == 0) {
				return false;
			}

			int c = getColor(p1, p2);

			if (c != cInit) {
				return false;
			}

			c = getColor(p2, p3);

			if (c != cInit) {
				return false;
			}

			c = getColor(p3, p4);

			return c == cInit;

		}

		/**
		 * Gets the color of a segment
		 * 
		 * @return 1 if segment more than 90% black, -1 if segment is more than
		 *         90% white, 0 else
		 */
		private int getColor(Point p1, Point p2) {
			float d = distance(p1, p2);
			float dx = (p2.getX() - p1.getX()) / d;
			float dy = (p2.getY() - p1.getY()) / d;
			int error = 0;

			float px = p1.getX();
			float py = p1.getY();

			boolean colorModel = image.get(p1.getX(), p1.getY());

			for (int i = 0; i < d; i++) {
				px += dx;
				py += dy;
				if (image.get(MathUtils.round(px), MathUtils.round(py)) != colorModel) {
					error++;
				}
			}

			float errRatio = error / d;

			if (errRatio > 0.1f && errRatio < 0.9f) {
				return 0;
			}

			return (errRatio <= 0.1f) == colorModel ? 1 : -1;
		}

		/**
		 * Gets the coordinate of the first point with a different color in the
		 * given direction
		 */
		private Point getFirstDifferent(Point init, boolean color, int dx,
				int dy) {
			int x = init.getX() + dx;
			int y = init.getY() + dy;

			while (isValid(x, y) && image.get(x, y) == color) {
				x += dx;
				y += dy;
			}

			x -= dx;
			y -= dy;

			while (isValid(x, y) && image.get(x, y) == color) {
				x += dx;
			}
			x -= dx;

			while (isValid(x, y) && image.get(x, y) == color) {
				y += dy;
			}
			y -= dy;

			return new Point(x, y);
		}

		/**
		 * Expand the square represented by the corner points by pushing out
		 * equally in all directions
		 * 
		 * @param cornerPoints
		 *            the corners of the square, which has the bull's eye at its
		 *            center
		 * @param oldSide
		 *            the original length of the side of the square in the
		 *            target bit matrix
		 * @param newSide
		 *            the new length of the size of the square in the target bit
		 *            matrix
		 * @return the corners of the expanded square
		 */
		private static ResultPoint[] expandSquare(ResultPoint[] cornerPoints,
				float oldSide, float newSide) {
			float ratio = newSide / (2 * oldSide);
			float dx = cornerPoints[0].getX() - cornerPoints[2].getX();
			float dy = cornerPoints[0].getY() - cornerPoints[2].getY();
			float centerx = (cornerPoints[0].getX() + cornerPoints[2].getX()) / 2.0f;
			float centery = (cornerPoints[0].getY() + cornerPoints[2].getY()) / 2.0f;

			ResultPoint result0 = new ResultPoint(centerx + ratio * dx, centery
					+ ratio * dy);
			ResultPoint result2 = new ResultPoint(centerx - ratio * dx, centery
					- ratio * dy);

			dx = cornerPoints[1].getX() - cornerPoints[3].getX();
			dy = cornerPoints[1].getY() - cornerPoints[3].getY();
			centerx = (cornerPoints[1].getX() + cornerPoints[3].getX()) / 2.0f;
			centery = (cornerPoints[1].getY() + cornerPoints[3].getY()) / 2.0f;
			ResultPoint result1 = new ResultPoint(centerx + ratio * dx, centery
					+ ratio * dy);
			ResultPoint result3 = new ResultPoint(centerx - ratio * dx, centery
					- ratio * dy);

			return new ResultPoint[] { result0, result1, result2, result3 };
		}

		private boolean isValid(int x, int y) {
			return x >= 0 && x < image.getWidth() && y > 0
					&& y < image.getHeight();
		}

		private boolean isValid(ResultPoint point) {
			int x = MathUtils.round(point.getX());
			int y = MathUtils.round(point.getY());
			return isValid(x, y);
		}

		private static float distance(Point a, Point b) {
			return toto.geom2d.Point.distance(a.getX(), a.getY(), b.getX(),
					b.getY());
		}

		private static float distance(ResultPoint a, ResultPoint b) {
			return toto.geom2d.Point.distance(a.getX(), a.getY(), b.getX(),
					b.getY());
		}

		private int getDimension() {
			if (compact) {
				return 4 * nbLayers + 11;
			}
			if (nbLayers <= 4) {
				return 4 * nbLayers + 15;
			}
			return 4 * nbLayers + 2 * ((nbLayers - 4) / 8 + 1) + 15;
		}

	}

}
