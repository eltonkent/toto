package toto.di.barcode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import toto.util.collections.list.BitArray;

final class MultiFormatUPCEANReader extends OneDReader {

	private final UPCEANReader[] readers;

	MultiFormatUPCEANReader(final Map<DecodeHintType, ?> hints) {
		@SuppressWarnings("unchecked")
		final Collection<BarcodeType> possibleFormats = hints == null ? null
				: (Collection<BarcodeType>) hints
						.get(DecodeHintType.POSSIBLE_FORMATS);
		final Collection<UPCEANReader> readers = new ArrayList<UPCEANReader>();
		if (possibleFormats != null) {
			if (possibleFormats.contains(BarcodeType.EAN_13)) {
				readers.add(new EAN13Reader());
			} else if (possibleFormats.contains(BarcodeType.UPC_A)) {
				readers.add(new UPCAReader());
			}
			if (possibleFormats.contains(BarcodeType.EAN_8)) {
				readers.add(new EAN8Reader());
			}
			if (possibleFormats.contains(BarcodeType.UPC_E)) {
				readers.add(new UPCEReader());
			}
		}
		if (readers.isEmpty()) {
			readers.add(new EAN13Reader());
			// UPC-A is covered by EAN-13
			readers.add(new EAN8Reader());
			readers.add(new UPCEReader());
		}
		this.readers = readers.toArray(new UPCEANReader[readers.size()]);
	}

	@Override
	public Result decodeRow(final int rowNumber, final BitArray row,
			final Map<DecodeHintType, ?> hints) throws BarcodeNotFoundException {
		// Compute this location once and reuse it on multiple implementations
		final int[] startGuardPattern = UPCEANReader.findStartGuardPattern(row);
		for (final UPCEANReader reader : readers) {
			Result result;
			try {
				result = reader.decodeRow(rowNumber, row, startGuardPattern,
						hints);
			} catch (final ReaderException ignored) {
				continue;
			}
			// Special case: a 12-digit code encoded in UPC-A is identical to a
			// "0"
			// followed by those 12 digits encoded as EAN-13. Each will
			// recognize such a code,
			// UPC-A as a 12-digit string and EAN-13 as a 13-digit string
			// starting with "0".
			// Individually these are correct and their readers will both read
			// such a code
			// and correctly call it EAN-13, or UPC-A, respectively.
			//
			// In this case, if we've been looking for both types, we'd like to
			// call it
			// a UPC-A code. But for efficiency we only run the EAN-13 decoder
			// to also read
			// UPC-A. So we special case it here, and convert an EAN-13 result
			// to a UPC-A
			// result if appropriate.
			//
			// But, don't return UPC-A if UPC-A was not a requested format!
			final boolean ean13MayBeUPCA = result.getBarcodeFormat() == BarcodeType.EAN_13
					&& result.getText().charAt(0) == '0';
			@SuppressWarnings("unchecked")
			final Collection<BarcodeType> possibleFormats = hints == null ? null
					: (Collection<BarcodeType>) hints
							.get(DecodeHintType.POSSIBLE_FORMATS);
			final boolean canReturnUPCA = possibleFormats == null
					|| possibleFormats.contains(BarcodeType.UPC_A);

			if (ean13MayBeUPCA && canReturnUPCA) {
				// Transfer the metdata across
				final Result resultUPCA = new Result(result.getText()
						.substring(1), result.getRawBytes(),
						result.getResultPoints(), BarcodeType.UPC_A);
				resultUPCA.putAllMetadata(result.getResultMetadata());
				return resultUPCA;
			}
			return result;
		}

		throw new BarcodeNotFoundException();
	}

	@Override
	public void reset() {
		for (final Reader reader : readers) {
			reader.reset();
		}
	}

}
