package toto.di.barcode;

import java.util.List;

import toto.util.collections.matrix.BitMatrix;

/**
 */
final class PDF417DetectorResult {

	private final BitMatrix bits;
	private final List<ResultPoint[]> points;

	PDF417DetectorResult(BitMatrix bits, List<ResultPoint[]> points) {
		this.bits = bits;
		this.points = points;
	}

	BitMatrix getBits() {
		return bits;
	}

	List<ResultPoint[]> getPoints() {
		return points;
	}

}
