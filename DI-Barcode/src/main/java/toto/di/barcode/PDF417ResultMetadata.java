package toto.di.barcode;

/**
 */
final class PDF417ResultMetadata {

	private int segmentIndex;
	private String fileId;
	private int[] optionalData;
	private boolean lastSegment;

	int getSegmentIndex() {
		return segmentIndex;
	}

	void setSegmentIndex(int segmentIndex) {
		this.segmentIndex = segmentIndex;
	}

	String getFileId() {
		return fileId;
	}

	void setFileId(String fileId) {
		this.fileId = fileId;
	}

	int[] getOptionalData() {
		return optionalData;
	}

	void setOptionalData(int[] optionalData) {
		this.optionalData = optionalData;
	}

	boolean isLastSegment() {
		return lastSegment;
	}

	void setLastSegment(boolean lastSegment) {
		this.lastSegment = lastSegment;
	}

}
