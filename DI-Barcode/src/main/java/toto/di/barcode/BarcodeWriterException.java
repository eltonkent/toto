package toto.di.barcode;

/**
 * A base class which covers the range of exceptions which may occur when
 * encoding a barcode using the Writer framework.
 * 
 */
public final class BarcodeWriterException extends Exception {

	public BarcodeWriterException() {
	}

	public BarcodeWriterException(String message) {
		super(message);
	}

	public BarcodeWriterException(Throwable cause) {
		super(cause);
	}

}
