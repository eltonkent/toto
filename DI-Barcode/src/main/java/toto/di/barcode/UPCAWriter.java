package toto.di.barcode;

import java.util.Map;

import toto.util.collections.matrix.BitMatrix;

/**
 * This object renders a UPC-A code as a {@link BitMatrix}.
 * 
 */
final class UPCAWriter implements Writer {

	private final EAN13Writer subWriter = new EAN13Writer();

	@Override
	public BitMatrix encode(String contents, BarcodeType format, int width,
			int height) throws BarcodeWriterException {
		return encode(contents, format, width, height, null);
	}

	@Override
	public BitMatrix encode(String contents, BarcodeType format, int width,
			int height, Map<EncodeHintType, ?> hints)
			throws BarcodeWriterException {
		if (format != BarcodeType.UPC_A) {
			throw new InvalidBarcodeDataException(
					"Can only encode UPC-A, but got " + format);
		}
		return subWriter.encode(preencode(contents), BarcodeType.EAN_13, width,
				height, hints);
	}

	/**
	 * Transform a UPC-A code into the equivalent EAN-13 code, and add a check
	 * digit if it is not already present.
	 */
	private static String preencode(String contents) {
		int length = contents.length();
		if (length == 11) {
			// No check digit present, calculate it and add it
			int sum = 0;
			for (int i = 0; i < 11; ++i) {
				sum += (contents.charAt(i) - '0') * (i % 2 == 0 ? 3 : 1);
			}
			contents += (1000 - sum) % 10;
		} else if (length != 12) {
			throw new InvalidBarcodeDataException(
					"Requested contents should be 11 or 12 digits long, but got "
							+ contents.length());
		}
		return '0' + contents;
	}
}
