package toto.di.barcode;

import java.util.EnumMap;
import java.util.Map;

/**
 * <p>
 * Encapsulates the result of decoding a barcode within an image.
 * </p>
 * 
 */
final class Result {

	private final String text;
	private final byte[] rawBytes;
	private ResultPoint[] resultPoints;
	private final BarcodeType format;
	private Map<ResultMetadataType, Object> resultMetadata;
	private final long timestamp;

	Result(final String text, final byte[] rawBytes,
			final ResultPoint[] resultPoints, final BarcodeType format) {
		this(text, rawBytes, resultPoints, format, System.currentTimeMillis());
	}

	Result(final String text, final byte[] rawBytes,
			final ResultPoint[] resultPoints, final BarcodeType format,
			final long timestamp) {
		this.text = text;
		this.rawBytes = rawBytes;
		this.resultPoints = resultPoints;
		this.format = format;
		this.resultMetadata = null;
		this.timestamp = timestamp;
	}

	/**
	 * @return raw text encoded by the barcode
	 */
	String getText() {
		return text;
	}

	/**
	 * @return raw bytes encoded by the barcode, if applicable, otherwise
	 *         {@code null}
	 */
	public byte[] getRawBytes() {
		return rawBytes;
	}

	/**
	 * @return points related to the barcode in the image. These are typically
	 *         points identifying finder patterns or the corners of the barcode.
	 *         The exact meaning is specific to the type of barcode that was
	 *         decoded.
	 */
	ResultPoint[] getResultPoints() {
		return resultPoints;
	}

	/**
	 * @return {@link BarcodeType} representing the format of the barcode that
	 *         was decoded
	 */
	BarcodeType getBarcodeFormat() {
		return format;
	}

	/**
	 * @return {@link java.util.Map} mapping {@link ResultMetadataType} keys to
	 *         values. May be {@code null}. This contains optional metadata
	 *         about what was detected about the barcode, like orientation.
	 */
	Map<ResultMetadataType, Object> getResultMetadata() {
		return resultMetadata;
	}

	void putMetadata(final ResultMetadataType type, final Object value) {
		if (resultMetadata == null) {
			resultMetadata = new EnumMap<ResultMetadataType, Object>(
					ResultMetadataType.class);
		}
		resultMetadata.put(type, value);
	}

	void putAllMetadata(final Map<ResultMetadataType, Object> metadata) {
		if (metadata != null) {
			if (resultMetadata == null) {
				resultMetadata = metadata;
			} else {
				resultMetadata.putAll(metadata);
			}
		}
	}

	void addResultPoints(final ResultPoint[] newPoints) {
		final ResultPoint[] oldPoints = resultPoints;
		if (oldPoints == null) {
			resultPoints = newPoints;
		} else if (newPoints != null && newPoints.length > 0) {
			final ResultPoint[] allPoints = new ResultPoint[oldPoints.length
					+ newPoints.length];
			System.arraycopy(oldPoints, 0, allPoints, 0, oldPoints.length);
			System.arraycopy(newPoints, 0, allPoints, oldPoints.length,
					newPoints.length);
			resultPoints = allPoints;
		}
	}

	public long getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		return text;
	}

}
