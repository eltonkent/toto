package toto.di.barcode;

import java.util.Map;

import toto.util.collections.matrix.BitMatrix;

/**
 * Created by Elton Kent on 12/26/13.
 */
final class MaxicodeDecoder {

	private static final int ALL = 0;
	private static final int EVEN = 1;
	private static final int ODD = 2;

	private final ReedSolomonDecoder rsDecoder;

	MaxicodeDecoder() {
		rsDecoder = new ReedSolomonDecoder(GenericGF.MAXICODE_FIELD_64);
	}

	DecoderResult decode(BitMatrix bits) throws BarCodeChecksumException,
			FormatException {
		return decode(bits, null);
	}

	DecoderResult decode(BitMatrix bits, Map<DecodeHintType, ?> hints)
			throws FormatException, BarCodeChecksumException {
		MaxicodeBitMatrixParser parser = new MaxicodeBitMatrixParser(bits);
		byte[] codewords = parser.readCodewords();

		correctErrors(codewords, 0, 10, 10, ALL);
		int mode = codewords[0] & 0x0F;
		byte[] datawords;
		switch (mode) {
		case 2:
		case 3:
		case 4:
			correctErrors(codewords, 20, 84, 40, EVEN);
			correctErrors(codewords, 20, 84, 40, ODD);
			datawords = new byte[94];
			break;
		case 5:
			correctErrors(codewords, 20, 68, 56, EVEN);
			correctErrors(codewords, 20, 68, 56, ODD);
			datawords = new byte[78];
			break;
		default:
			throw FormatException.getFormatInstance();
		}

		System.arraycopy(codewords, 0, datawords, 0, 10);
		System.arraycopy(codewords, 20, datawords, 10, datawords.length - 10);

		return MaxicodeDecodedBitStreamParser.decode(datawords, mode);
	}

	private void correctErrors(byte[] codewordBytes, int start,
			int dataCodewords, int ecCodewords, int mode)
			throws BarCodeChecksumException {
		int codewords = dataCodewords + ecCodewords;

		// in EVEN or ODD mode only half the codewords
		int divisor = mode == ALL ? 1 : 2;

		// First read into an array of ints
		int[] codewordsInts = new int[codewords / divisor];
		for (int i = 0; i < codewords; i++) {
			if ((mode == ALL) || (i % 2 == (mode - 1))) {
				codewordsInts[i / divisor] = codewordBytes[i + start] & 0xFF;
			}
		}
		try {
			rsDecoder.decode(codewordsInts, ecCodewords / divisor);
		} catch (ReedSolomonException ignored) {
			throw BarCodeChecksumException.getChecksumInstance();
		}
		// Copy back into array of bytes -- only need to worry about the bytes
		// that were data
		// We don't care about errors in the error-correction codewords
		for (int i = 0; i < dataCodewords; i++) {
			if ((mode == ALL) || (i % 2 == (mode - 1))) {
				codewordBytes[i + start] = (byte) codewordsInts[i / divisor];
			}
		}
	}

}
