/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.zip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import toto.io.streams.BitOutputStream;

public class LZWCompressor {

	// private static final int MAX_TABLE_SIZE = 1 << 12;

	private final static class ByteArray {
		private final byte bytes[];
		private final int hash;
		private final int length;
		private final int start;

		public ByteArray(final byte bytes[], final int start, final int length) {
			this.bytes = bytes;
			this.start = start;
			this.length = length;

			int tempHash = length;

			for (int i = 0; i < length; i++) {
				final int b = 0xff & bytes[i + start];
				tempHash = tempHash + (tempHash << 8) ^ b ^ i;
			}

			hash = tempHash;
		}

		@Override
		public final boolean equals(final Object o) {
			if (o == null) {
				return false;
			}
			final ByteArray other = (ByteArray) o;
			if (other.hash != hash) {
				return false;
			}
			if (other.length != length) {
				return false;
			}

			for (int i = 0; i < length; i++) {
				if (other.bytes[i + other.start] != bytes[i + start]) {
					return false;
				}
			}

			return true;
		}

		@Override
		public final int hashCode() {
			return hash;
		}
	}

	public static interface Listener {
		public void clearCode(int code);

		public void dataCode(int code);

		public void eoiCode(int code);

		public void init(int clearCode, int eoiCode);
	}

	private final int byteOrder;

	private final int clearCode;
	private int codes = -1;
	private int codeSize;
	private final boolean earlyLimit;
	private final int eoiCode;

	private final int initialCodeSize;

	private final Listener listener;

	private final Map map = new HashMap();

	public LZWCompressor(final int initialCodeSize, final int byteOrder,
			final boolean earlyLimit) {
		this(initialCodeSize, byteOrder, earlyLimit, null);
	}

	public LZWCompressor(final int initialCodeSize, final int byteOrder,
			final boolean earlyLimit, final Listener listener) {
		this.listener = listener;
		this.byteOrder = byteOrder;
		this.earlyLimit = earlyLimit;

		this.initialCodeSize = initialCodeSize;

		clearCode = 1 << initialCodeSize;
		eoiCode = clearCode + 1;

		if (null != listener) {
			listener.init(clearCode, eoiCode);
		}

		InitializeStringTable();
	}

	private final boolean addTableEntry(final BitOutputStream bos,
			final byte bytes[], final int start, final int length)
			throws IOException {
		final Object key = arrayToKey(bytes, start, length);
		return addTableEntry(bos, key);
	}

	private final boolean addTableEntry(final BitOutputStream bos,
			final Object key) throws IOException {
		boolean cleared = false;

		{
			int limit = (1 << codeSize);
			if (earlyLimit) {
				limit--;
			}

			if (codes == limit) {
				if (codeSize < 12) {
					incrementCodeSize();
				} else {
					writeClearCode(bos);
					clearTable();
					cleared = true;
				}
			}
		}

		if (!cleared) {
			map.put(key, new Integer(codes));
			codes++;
		}

		return cleared;
	}

	private final Object arrayToKey(final byte b) {
		return arrayToKey(new byte[] { b, }, 0, 1);
	}

	private final Object arrayToKey(final byte bytes[], final int start,
			final int length) {
		return new ByteArray(bytes, start, length);
	}

	private final void clearTable() {
		InitializeStringTable();
		incrementCodeSize();
	}

	private final int codeFromString(final byte bytes[], final int start,
			final int length) throws IOException {
		final Object key = arrayToKey(bytes, start, length);
		final Object o = map.get(key);
		if (o == null) {
			throw new IOException("CodeFromString");
		}
		return ((Integer) o).intValue();
	}

	public byte[] compress(final byte bytes[]) throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream(
				bytes.length);
		final BitOutputStream bos = new BitOutputStream(baos, byteOrder);

		InitializeStringTable();
		clearTable();
		writeClearCode(bos);
		int w_start = 0;
		int w_length = 0;

		for (int i = 0; i < bytes.length; i++) {
			if (isInTable(bytes, w_start, w_length + 1)) {
				w_length++;
			} else {
				final int code = codeFromString(bytes, w_start, w_length);
				writeDataCode(bos, code);
				addTableEntry(bos, bytes, w_start, w_length + 1);

				w_start = i;
				w_length = 1;
			}
		} /* end of for loop */

		final int code = codeFromString(bytes, w_start, w_length);
		writeDataCode(bos, code);

		writeEoiCode(bos);

		bos.flushCache();

		return baos.toByteArray();
	}

	private final void incrementCodeSize() {
		if (codeSize != 12) {
			codeSize++;
		}
	}

	private final void InitializeStringTable() {
		codeSize = initialCodeSize;

		final int intial_entries_count = (1 << codeSize) + 2;

		map.clear();
		for (codes = 0; codes < intial_entries_count; codes++) {
			if ((codes != clearCode) && (codes != eoiCode)) {
				final Object key = arrayToKey((byte) codes);

				map.put(key, new Integer(codes));
			}
		}
	}

	private final boolean isInTable(final byte bytes[], final int start,
			final int length) {
		final Object key = arrayToKey(bytes, start, length);

		return map.containsKey(key);
	}

	private final void writeClearCode(final BitOutputStream bos)
			throws IOException {
		if (null != listener) {
			listener.dataCode(clearCode);
		}
		writeCode(bos, clearCode);
	}

	private final void writeCode(final BitOutputStream bos, final int code)
			throws IOException {
		bos.writeBits(code, codeSize);
	}

	private final void writeDataCode(final BitOutputStream bos, final int code)
			throws IOException {
		if (null != listener) {
			listener.dataCode(code);
		}
		writeCode(bos, code);
	};

	private final void writeEoiCode(final BitOutputStream bos)
			throws IOException {
		if (null != listener) {
			listener.eoiCode(eoiCode);
		}
		writeCode(bos, eoiCode);
	}
}
