package toto.util.zip;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <a href="http://en.wikipedia.org/wiki/LZW">Lempel-Ziv-Welch</a> compression.
 * 
 * @author ekent4
 * 
 */
public class LZW {

	public static byte[] compressLZW(final byte src[],
			final int LZWMinimumCodeSize, final int byteOrder,
			final boolean earlyLimit) throws IOException

	{
		final LZWCompressor compressor = new LZWCompressor(LZWMinimumCodeSize,
				byteOrder, earlyLimit);

		final byte compressed[] = compressor.compress(src);

		return compressed;
	}

	public static byte[] decompressLZW(final byte compressed[],
			final int LZWMinimumCodeSize, final int expectedSize,
			final int byteOrder) throws IOException {
		final InputStream is = new ByteArrayInputStream(compressed);

		final LZWDecompressor decompressor = new LZWDecompressor(
				LZWMinimumCodeSize, byteOrder);
		final byte[] result = decompressor.decompress(is, expectedSize);

		return result;
	}

}
