/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.zip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import toto.io.streams.BitInputStream;
import toto.math.BinaryConstants;

public final class LZWDecompressor {
	public static interface Listener {
		public void code(int code);

		public void init(int clearCode, int eoiCode);
	}

	private static final int MAX_TABLE_SIZE = 1 << 12;
	private final int byteOrder;
	private final int clearCode;
	private int codes = -1;

	private int codeSize;

	private final int eoiCode;

	private final int initialCodeSize;

	private final Listener listener;

	private final byte[][] table;

	private boolean tiffLZWMode = false;

	private int written = 0;

	/**
	 * 
	 * @param initialCodeSize
	 *            intial table size any positive integer.
	 * @param byteOrder
	 *            {@link BinaryConstants}
	 */
	public LZWDecompressor(final int initialCodeSize, final int byteOrder) {
		this(initialCodeSize, byteOrder, null);
	}

	public LZWDecompressor(final int initialCodeSize, final int byteOrder,
			final Listener listener) {
		this.listener = listener;
		this.byteOrder = byteOrder;

		this.initialCodeSize = initialCodeSize;

		table = new byte[MAX_TABLE_SIZE][];
		clearCode = 1 << initialCodeSize;
		eoiCode = clearCode + 1;

		if (null != listener) {
			listener.init(clearCode, eoiCode);
		}

		InitializeTable();
	}

	private final void addStringToTable(final byte bytes[]) throws IOException {
		if (codes < (1 << codeSize)) {
			table[codes] = bytes;
			codes++;
		} else {
			throw new IOException("AddStringToTable: codes: " + codes
					+ " code_size: " + codeSize);
		}

		checkCodeSize();
	}

	private final byte[] appendBytes(final byte bytes[], final byte b) {
		final byte result[] = new byte[bytes.length + 1];

		System.arraycopy(bytes, 0, result, 0, bytes.length);
		result[result.length - 1] = b;
		return result;
	}

	private final void checkCodeSize() // throws IOException
	{
		int limit = (1 << codeSize);
		if (tiffLZWMode) {
			limit--;
		}

		if (codes == limit) {
			incrementCodeSize();
		}
	}

	private final void clearTable() {
		codes = (1 << initialCodeSize) + 2;
		codeSize = initialCodeSize;
		incrementCodeSize();
	}

	public byte[] decompress(final InputStream is, final int expectedLength)
			throws IOException {
		int code, oldCode = -1;
		final BitInputStream mbis = new BitInputStream(is, byteOrder);
		if (tiffLZWMode) {
			mbis.setTiffLZWMode();
		}

		final ByteArrayOutputStream baos = new ByteArrayOutputStream(
				expectedLength);

		clearTable();

		while ((code = getNextCode(mbis)) != eoiCode) {
			if (code == clearCode) {
				clearTable();

				if (written >= expectedLength) {
					break;
				}
				code = getNextCode(mbis);

				if (code == eoiCode) {
					break;
				}
				writeToResult(baos, stringFromCode(code));

				oldCode = code;
			} // end of ClearCode case
			else {
				if (isInTable(code)) {
					writeToResult(baos, stringFromCode(code));

					addStringToTable(appendBytes(stringFromCode(oldCode),
							firstChar(stringFromCode(code))));
					oldCode = code;
				} else {
					final byte OutString[] = appendBytes(
							stringFromCode(oldCode),
							firstChar(stringFromCode(oldCode)));
					writeToResult(baos, OutString);
					addStringToTable(OutString);
					oldCode = code;
				}
			} // end of not-ClearCode case

			if (written >= expectedLength) {
				break;
			}
		} // end of while loop

		final byte result[] = baos.toByteArray();

		return result;
	}

	private final byte firstChar(final byte bytes[]) {
		return bytes[0];
	}

	private final int getNextCode(final BitInputStream is) throws IOException {
		final int code = is.readBits(codeSize);

		if (null != listener) {
			listener.code(code);
		}
		return code;
	}

	private final void incrementCodeSize() // throws IOException
	{
		if (codeSize != 12) {
			codeSize++;
		}
	}

	private final void InitializeTable() {
		codeSize = initialCodeSize;

		final int intial_entries_count = 1 << codeSize + 2;

		for (int i = 0; i < intial_entries_count; i++) {
			table[i] = new byte[] { (byte) i, };
		}
	}

	private final boolean isInTable(final int Code) {
		return Code < codes;
	}

	public void setTiffLZWMode() {
		tiffLZWMode = true;
	}

	private final byte[] stringFromCode(final int code) throws IOException {
		if ((code >= codes) || (code < 0)) {
			throw new IOException("Bad Code: " + code + " codes: " + codes
					+ " code_size: " + codeSize + ", table: " + table.length);
		}

		return table[code];
	}

	private final void writeToResult(final OutputStream os, final byte bytes[])
			throws IOException {
		os.write(bytes);
		written += bytes.length;
	}
}
