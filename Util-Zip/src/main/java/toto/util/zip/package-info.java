/**
 * Lossless Data compression algorithms
 * <p>
 * Implementations include, LZW, LZ4, QuickLZ, PackBits, Snappy
 * </p>
 *
 */
package toto.util.zip;