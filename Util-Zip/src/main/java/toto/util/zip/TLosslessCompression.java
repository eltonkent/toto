package toto.util.zip;

import java.io.IOException;

/**
 * Compression algorithm interface.
 * 
 * @author Mobifluence Interactive
 * 
 */
public interface TLosslessCompression {

	public byte[] compress(byte[] source) throws IOException;

	public byte[] decompress(byte[] source) throws IOException;
}
