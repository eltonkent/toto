package toto.util.zip;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import toto.io.IOUtils;

public class Zip implements TLosslessCompression {
	public final byte[] compress(final byte bytes[]) throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final DeflaterOutputStream dos = new DeflaterOutputStream(baos);
		dos.write(bytes);
		dos.close();
		return baos.toByteArray();
	}

	public final byte[] decompress(final byte bytes[]) throws IOException
	// slow, probably.
	{
		final ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		final InflaterInputStream zIn = new InflaterInputStream(in);
		return IOUtils.toByteArray(zIn);
	}

}
