/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.zip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import toto.io.streams.FastByteArrayOutputStream;

/**
 * Fast simple lossless compression for run-length encoding of data.
 * <p>
 * <a href="http://en.wikipedia.org/wiki/PackBits">Pack Bits</a>
 * </p>
 * 
 * @author ekent4
 * 
 */
public final class PackBits {

	public static byte[] compress(final byte bytes[]) throws IOException {
		final FastByteArrayOutputStream baos = new toto.io.streams.FastByteArrayOutputStream(
				bytes.length * 2); // max
		// length
		// 1
		// extra
		// byte
		// for
		// every
		// 128

		int ptr = 0;
		while (ptr < bytes.length) {
			int dup = findNextDuplicate(bytes, ptr);

			if (dup == ptr) // write run length
			{
				final int len = findRunLength(bytes, dup);
				final int actual_len = Math.min(len, 128);
				baos.write(-(actual_len - 1));
				baos.write(bytes[ptr]);
				ptr += actual_len;
			} else { // write literals
				int len = dup - ptr;

				if (dup > 0) {
					final int runlen = findRunLength(bytes, dup);
					if (runlen < 3) // may want to discard next run.
					{
						final int nextptr = ptr + len + runlen;
						final int nextdup = findNextDuplicate(bytes, nextptr);
						if (nextdup != nextptr) // discard 2-byte run
						{
							dup = nextdup;
							len = dup - ptr;
						}
					}
				}

				if (dup < 0) {
					len = bytes.length - ptr;
				}
				final int actual_len = Math.min(len, 128);

				baos.write(actual_len - 1);
				for (int i = 0; i < actual_len; i++) {
					baos.write(bytes[ptr]);
					ptr++;
				}
			}
		}
		final byte result[] = baos.toByteArray();
		baos.close();
		return result;

	}

	public static byte[] decompress(final byte bytes[], final int expected)
			throws IOException {
		int total = 0;

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();

		// Loop until you get the number of unpacked bytes you are expecting:
		int i = 0;
		while (total < expected)

		{
			// Read the next source byte into n.
			if (i >= bytes.length) {
				throw new IOException("Unpack bits source exhausted: " + i
						+ ", done + " + total + ", expected + " + expected);
			}

			final int n = bytes[i++];
			// If n is between 0 and 127 inclusive, copy the next n+1 bytes
			// literally.
			if ((n >= 0) && (n <= 127)) {

				final int count = n + 1;

				total += count;
				for (int j = 0; j < count; j++) {
					baos.write(bytes[i++]);
				}
			}
			// Else if n is between -127 and -1 inclusive, copy the next byte
			// -n+1
			// times.
			else if ((n >= -127) && (n <= -1)) {
				final int b = bytes[i++];
				final int count = -n + 1;

				total += count;
				for (int j = 0; j < count; j++) {
					baos.write(b);
				}
			} else if (n == -128) {
				throw new IOException("Packbits: " + n);
				// Else if n is between -127 and -1 inclusive, copy the next
				// byte -n+1
				// times.
				// else
				// Else if n is -128, noop.
			}
		}
		final byte result[] = baos.toByteArray();
		baos.close();
		return result;

	}

	private static int findNextDuplicate(final byte bytes[], final int start) {
		// int last = -1;
		if (start >= bytes.length) {
			return -1;
		}

		byte prev = bytes[start];

		for (int i = start + 1; i < bytes.length; i++) {
			final byte b = bytes[i];

			if (b == prev) {
				return i - 1;
			}

			prev = b;
		}

		return -1;
	}

	private static int findRunLength(final byte bytes[], final int start) {
		final byte b = bytes[start];

		int i;

		for (i = start + 1; (i < bytes.length) && (bytes[i] == b); i++) {
			;
		}

		return i - start;
	}
}
