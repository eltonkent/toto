package toto.graphics;

import java.io.File;
import java.lang.ref.SoftReference;

import toto.util.collections.cache.LRUCache;
import android.content.Context;
import android.graphics.Typeface;

/**
 * Fast Typeface LRU cache.
 */
public final class TypefaceCache {
	private static LRUCache<String, SoftReference<ToToTypeface>> cache = new LRUCache<String, SoftReference<ToToTypeface>>(
			1024 * 1024);

	/**
	 * Load typeface from asset path.
	 * 
	 * @param context
	 * @param assetPath
	 * @return
	 */
	public static ToToTypeface loadFromAssets(final Context context,
			final String assetPath) {
		final SoftReference<ToToTypeface> ref = cache.fetch(assetPath);
        ToToTypeface typeface = (ref == null) ? null : ref.get();
		if (typeface == null) {
			final Typeface tf = Typeface.createFromAsset(context.getAssets(),
					assetPath);
			if (tf != null) {
				typeface = new ToToTypeface();
				typeface.typeface = tf;
				typeface.tfId = assetPath;
				cache.cache(assetPath,
						new SoftReference<ToToTypeface>(typeface));
			}
		}
		return typeface;
	}

	/**
	 * Load typeface from file
	 * 
	 * @param context
	 * @param file
	 * @return
	 */
	public static ToToTypeface loadFromFile(final Context context,
			final File file) {
		final String id = file.toString();
		final SoftReference<ToToTypeface> ref = cache.fetch(id);
        ToToTypeface typeface = (ref == null) ? null : ref.get();
		if (typeface == null) {
			final Typeface tf = Typeface.createFromFile(file);
			if (tf != null) {
				typeface = new ToToTypeface();
				typeface.typeface = tf;
				typeface.tfId = id;
				cache.cache(id, new SoftReference<ToToTypeface>(typeface));
			}
		}
		return typeface;
	}

	/**
	 * The typeface manager automatically purges unused typefaces. Use this
	 * method if you want to manually purge a typeface from the cache.
	 * 
	 * @param typeface
	 */
	public static void purge(final ToToTypeface typeface) {
		if (typeface != null)
			cache.remove(typeface.tfId);
	}

	public final static class ToToTypeface {
		private String tfId;
		private Typeface typeface;

		private ToToTypeface() {

		}

		/**
		 * Get the loaded typeface.
		 * 
		 * @return
		 */
		public Typeface getTypeface() {
			return typeface;
		}
	}
}
