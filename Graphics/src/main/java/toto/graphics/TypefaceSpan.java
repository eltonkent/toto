package toto.graphics;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

import toto.graphics.TypefaceCache.ToToTypeface;

/**
 * Text span that uses custom typefaces. The typeface that is used should be in
 * the root level of the asset folder
 * <p>
 * <code>
 * <font color="green">//Roboto.ttf is in the assets folder.</font><br/>
 * TypefaceSpan tfSpan=new TypefaceSpan(this, "Roboto.ttf");<br/>
 * SpannableString s = new SpannableString("My Title");<br/>
 * s.setSpan(tfSpan, 0, s.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);<br/>
 * 
 * <font color="green">//setting the typeface for textview</font><br/>
 * TextView mytextView=...</br>
 * myTextView.setText(s);<br/>
 * </code>
 * </p>
 * 
 * @see ToToTypeface
 * @author Elton Kent
 *
 */
public class TypefaceSpan extends MetricAffectingSpan {

	private final Typeface mTypeface;

	/**
	 * Load the {@link android.graphics.Typeface} and apply to a {@link android.text.Spannable}.
	 */
	public TypefaceSpan(final Context context, final ToToTypeface typeFace) {
		mTypeface = typeFace.getTypeface();
	}

	@Override
	public void updateMeasureState(final TextPaint p) {
		p.setTypeface(mTypeface);
		// Note: This flag is required for proper typeface rendering
		p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
	}

	@Override
	public void updateDrawState(final TextPaint tp) {
		tp.setTypeface(mTypeface);

		// Note: This flag is required for proper typeface rendering
		tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
	}

}
