/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.graphics;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Allows you to perform layer based drawing on a canvas.
 * 
 * @author elton.kent
 * 
 */
public class CanvasLayer {
	private final Bitmap bitmap;
	private final Paint bmPaint;
	/**
	 * Canvas to be used for drawing operations on this layer
	 */
	public final Canvas canvas;

	public CanvasLayer(final int width, final int height) {
		bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		bmPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	}

	/**
	 * The source canvas
	 * 
	 * @param src
	 *            The canvas to draw this layer onto.
	 */
	public void drawLayer(final Canvas src) {
		drawLayer(src, 0, 0);
	}

	public void drawLayer(final Canvas src, final float x, final float y) {
		src.drawBitmap(bitmap, x, y, bmPaint);
	}

}
