package toto.lang;

public class LongUtils {

	public static long fromBytes(final byte[] bytes, final int offset) {
		long ret = 0;
		for (int i = offset; i < (offset + 8); i++) {
			ret <<= 8;
			ret |= bytes[i] & 0xFF;
		}
		return ret;
	}

	public static long fromBytes(final byte[] bytes) {
		return fromBytes(bytes, 0);
	}

	/**
	 * Convert integer to array of bytes.
	 * 
	 * @param i
	 * @return
	 */
	public static byte[] toBytes(final long v) {
		final byte[] longBytes = new byte[8];
		toBytes(v, longBytes);
		return longBytes;
	}

	/**
	 * 
	 * @param v
	 * @param longBytes
	 *            8 element long byte array to store the long bytes
	 */
	public static void toBytes(final long v, final byte[] longBytes) {
		longBytes[0] = (byte) (v >>> 56);
		longBytes[1] = (byte) (v >>> 48);
		longBytes[2] = (byte) (v >>> 40);
		longBytes[3] = (byte) (v >>> 32);
		longBytes[4] = (byte) (v >>> 24);
		longBytes[5] = (byte) (v >>> 16);
		longBytes[6] = (byte) (v >>> 8);
		longBytes[7] = (byte) (v >>> 0);
	}

}
