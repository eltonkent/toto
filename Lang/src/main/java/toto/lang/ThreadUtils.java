package toto.lang;

public class ThreadUtils {

	/**
	 * Start and join an array of threads
	 * 
	 * @param threads
	 */
	public static void startAndJoin(final Thread[] threads) {
		for (int ithread = 0; ithread < threads.length; ++ithread) {
			threads[ithread].setPriority(Thread.NORM_PRIORITY);
			threads[ithread].start();
		}

		try {
			for (int ithread = 0; ithread < threads.length; ++ithread) {
				threads[ithread].join();
			}
		} catch (final InterruptedException ie) {
			throw new RuntimeException(ie);
		}
	}

	/**
	 * Create a thread array based on the number of available processors
	 * 
	 * @param nb
	 * @return
	 */
	public static Thread[] createThreadArray(int nb) {
		if (nb == 0) {
			nb = Runtime.getRuntime().availableProcessors();
		}
		final Thread[] threads = new Thread[nb];

		return threads;
	}

	public static Thread[] createThreadArray() {
		return createThreadArray(0);
	}

}
