/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.lang;

import static com.mi.toto.Conditions.checkArgument;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for primitives.
 * 
 */
public final class PrimitiveUtils {
	private final static Map BOX = new HashMap();
	private final static Map NAMED_PRIMITIVE = new HashMap();
	private final static Map REPRESENTING_CHAR = new HashMap();
	private final static Map UNBOX = new HashMap();

	static {
		final Class[][] boxing = new Class[][] { { Byte.TYPE, Byte.class },
				{ Character.TYPE, Character.class },
				{ Short.TYPE, Short.class }, { Integer.TYPE, Integer.class },
				{ Long.TYPE, Long.class }, { Float.TYPE, Float.class },
				{ Double.TYPE, Double.class }, { Boolean.TYPE, Boolean.class },
				{ Void.TYPE, Void.class }, };
		final Character[] representingChars = { new Character('B'),
				new Character('C'), new Character('S'), new Character('I'),
				new Character('J'), new Character('F'), new Character('D'),
				new Character('Z'), null };
		for (int i = 0; i < boxing.length; i++) {
			final Class primitiveType = boxing[i][0];
			final Class boxedType = boxing[i][1];
			BOX.put(primitiveType, boxedType);
			UNBOX.put(boxedType, primitiveType);
			NAMED_PRIMITIVE.put(primitiveType.getName(), primitiveType);
			REPRESENTING_CHAR.put(primitiveType, representingChars[i]);
		}
	}

	/**
	 * Get the boxed type for a primitive.
	 * 
	 * @param type
	 *            the primitive type
	 * @return the boxed type or null
	 */
	static public Class box(final Class type) {
		return (Class) BOX.get(type);
	}

	/**
	 * Check for a boxed type.
	 * 
	 * @param type
	 *            the type to check
	 * @return <code>true</code> if the type is boxed
	 * @since 1.4
	 */
	static public boolean isBoxed(final Class type) {
		return UNBOX.containsKey(type);
	}

	/**
	 * Get the primitive type by name.
	 * 
	 * @param name
	 *            the name of the type
	 * @return the Java type or <code>null</code>
	 * @since 1.4
	 */
	static public Class primitiveType(final String name) {
		return (Class) NAMED_PRIMITIVE.get(name);
	}

	/**
	 * Get the representing character of a primitive type.
	 * 
	 * @param type
	 *            the primitive type
	 * @return the representing character or 0
	 * @since 1.4
	 */
	static public char representingChar(final Class type) {
		final Character ch = (Character) REPRESENTING_CHAR.get(type);
		return ch == null ? 0 : ch.charValue();
	}

	private static byte[] toByteArray(final long data) {
		return new byte[] { (byte) (data >> 56 & 0xff),
				(byte) (data >> 48 & 0xff), (byte) (data >> 40 & 0xff),
				(byte) (data >> 32 & 0xff), (byte) (data >> 24 & 0xff),
				(byte) (data >> 16 & 0xff), (byte) (data >> 8 & 0xff),
				(byte) (data >> 0 & 0xff), };
	}

	public static byte[] toByteArray(final double data) {
		return toByteArray(Double.doubleToRawLongBits(data));
	}

	public static double toDouble(final byte[] data) {
		if (data == null || data.length != 8)
			return 0x0;
		// ---------- simple:
		return Double.longBitsToDouble(toLong(data));
	}

	private static long toLong(final byte[] data) {
		if (data == null || data.length != 8)
			return 0x0;
		// ----------
		return (long) (0xff & data[0]) << 56 | (long) (0xff & data[1]) << 48
				| (long) (0xff & data[2]) << 40 | (long) (0xff & data[3]) << 32
				| (long) (0xff & data[4]) << 24 | (long) (0xff & data[5]) << 16
				| (long) (0xff & data[6]) << 8 | (long) (0xff & data[7]) << 0;
	}

	/**
	 * Get the primitive type for a boxed one.
	 * 
	 * @param type
	 *            the boxed type
	 * @return the primitive type or null
	 */
	static public Class unbox(final Class type) {
		return (Class) UNBOX.get(type);
	}

	public static void intToBin(final long l, final byte[] b) {
		b[0] = (byte) (l & 0xff);
		b[1] = (byte) ((l >>> 8) & 0xff);
		b[2] = (byte) ((l >>> 16) & 0xff);
		b[3] = (byte) ((l >>> 24) & 0xff);
		b[4] = (byte) ((l >>> 32) & 0xff);
		b[5] = (byte) ((l >>> 40) & 0xff);
		b[6] = (byte) ((l >>> 48) & 0xff);
		b[7] = (byte) ((l >>> 56) & 0xff);
	}

	public static long binToInt(final byte[] b) {
		return binToIntOffset(b, 0);
	}

	public static long binToIntOffset(final byte[] b, final int off) {
		return (b[off]) | ((long) b[off + 1]) << 8 | ((long) b[off + 2]) << 16
				| ((long) b[off + 3]) << 24 | ((long) b[off + 4]) << 32
				| ((long) b[off + 5]) << 40 | ((long) b[off + 6]) << 48
				| ((long) b[off + 7]) << 56;
	}

	/**
	 * Returns a big-endian representation of {@code value} in a 4-element byte
	 * array; equivalent to {@code ByteBuffer.allocate(4).putInt(value).array()}
	 * . For example, the input value {@code 0x12131415} would yield the byte
	 * array {@code 0x12, 0x13, 0x14, 0x15} .
	 * 
	 * 
	 */
	public static byte[] toByteArray(final int value) {
		return new byte[] { (byte) (value >> 24), (byte) (value >> 16),
				(byte) (value >> 8), (byte) value };
	}

	/**
	 * Returns the {@code int} value whose big-endian representation is stored
	 * in the first 4 bytes of {@code bytes}; equivalent to
	 * {@code ByteBuffer.wrap(bytes).getInt()}. For example, the input byte
	 * array {@code 0x12, 0x13, 0x14, 0x15, 0x33} would yield the {@code int}
	 * value {@code 0x12131415}.
	 * 
	 * <p>
	 * Arguably, it's preferable to use {@link java.nio.ByteBuffer}; that
	 * library exposes much more flexibility at little cost in readability.
	 * 
	 * @throws IllegalArgumentException
	 *             if {@code bytes} has fewer than 4 elements
	 */
	public static int fromByteArray(final byte[] bytes) {
		checkArgument(bytes.length >= IntegerUtils.BYTES,
				"array too small: %s < %s", bytes.length, IntegerUtils.BYTES);
		return fromBytes(bytes[0], bytes[1], bytes[2], bytes[3]);
	}

	/**
	 * Returns the {@code int} value whose byte representation is the given 4
	 * bytes, in big-endian order; equivalent to {@code fromByteArray(new
	 * byte[] b1, b2, b3, b4})}.
	 * 
	 * @since 7.0
	 */
	public static int fromBytes(final byte b1, final byte b2, final byte b3,
			final byte b4) {
		return b1 << 24 | (b2 & 0xFF) << 16 | (b3 & 0xFF) << 8 | (b4 & 0xFF);
	}

	public static byte[] intToByteArray(int[] value) {
		byte[] data = new byte[value.length << 2];
		int cnt = 0;
		int end = value.length;
		for (int i = 0; i < end; i++) {
			int vali = value[i];
			data[(cnt++)] = ((byte) (vali >>> 24));
			data[(cnt++)] = ((byte) (vali >>> 16));
			data[(cnt++)] = ((byte) (vali >>> 8));
			data[(cnt++)] = ((byte) vali);
		}
		return data;
	}

	public static int[] byteArrayToInt(byte[] b) {
		int[] data = new int[b.length + 3 >> 2];
		int cnt = 0;
		int end = b.length;
		for (int i = 0; i < end; i++) {
			int val = b[i] << 24;
			i++;
			if (i < end) {
				val |= (b[i] & 0xFF) << 16;
			}
			i++;
			if (i < end) {
				val |= (b[i] & 0xFF) << 8;
			}
			i++;
			if (i < end) {
				val |= b[i] & 0xFF;
			}
			data[(cnt++)] = val;
		}
		return data;
	}
}
