package toto.lang.bitshift;

/**
 * Bit level conversion utilities
 * 
 * @author ekent4
 * 
 */
public class BitConverter {

	/**
	 * Convert a byte into a long value without making it negative.
	 */
	public static long byteToLong(final byte b) {
		long val = b & 0x7F;
		if ((b & 0x80) != 0) {
			val += 128;
		}
		return val;
	}

	/**
	 * Convert 4 bytes from the buffer at offset into a long value.
	 */
	public static long fourByteToLong(final byte[] bytes, final int offset) {
		return (byteToLong(bytes[offset + 0])
				+ (byteToLong(bytes[offset + 1]) << 8)
				+ (byteToLong(bytes[offset + 2]) << 16) + (byteToLong(bytes[offset + 3]) << 24));
	}
}
