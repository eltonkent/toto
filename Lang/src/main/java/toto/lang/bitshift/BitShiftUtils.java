package toto.lang.bitshift;

public class BitShiftUtils {
	// max value to limit it to 4 bytes
	private static final long MAX_VALUE = 0xFFFFFFFFL;

	/**
	 * Left shift val by shift bits. Cut down to 4 bytes.
	 */
	public static long leftShift(final long val, final int shift) {
		return (val << shift) & MAX_VALUE;
	}

	public static long rotateLeft(final long l, final int shift) {
		return (l << shift) | l >>> (64 - shift);
	}

}
