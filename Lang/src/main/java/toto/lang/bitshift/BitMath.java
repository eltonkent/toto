package toto.lang.bitshift;

public class BitMath {
	private static final long MAX_VALUE = 0xFFFFFFFFL;

	/**
	 * Do addition and turn into 4 bytes.
	 */
	public static long add(final long val, final long add) {
		return (val + add) & MAX_VALUE;
	}

	/**
	 * Do subtraction and turn into 4 bytes.
	 */
	public static long subtract(final long val, final long subtract) {
		return (val - subtract) & MAX_VALUE;
	}

	/**
	 * Left shift val by shift bits and turn in 4 bytes.
	 */
	public static long xor(final long val, final long xor) {
		return (val ^ xor) & MAX_VALUE;
	}

}
