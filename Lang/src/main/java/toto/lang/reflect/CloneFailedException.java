/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.lang.reflect;

/**
 * Exception thrown when a clone cannot be created. In contrast to
 * {@link CloneNotSupportedException} this is a {@link RuntimeException}.
 * 
 */
public class CloneFailedException extends RuntimeException {
	// ~ Static fields/initializers
	// ---------------------------------------------

	private static final long serialVersionUID = 20091223L;

	// ~ Constructors
	// -----------------------------------------------------------

	/**
	 * Constructs a CloneFailedException.
	 * 
	 * @param message
	 *            description of the exception
	 * @since upcoming
	 */
	public CloneFailedException(final String message) {
		super(message);
	}

	/**
	 * Constructs a CloneFailedException.
	 * 
	 * @param message
	 *            description of the exception
	 * @param cause
	 *            cause of the exception
	 * @since upcoming
	 */
	public CloneFailedException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a CloneFailedException.
	 * 
	 * @param cause
	 *            cause of the exception
	 * @since upcoming
	 */
	public CloneFailedException(final Throwable cause) {
		super(cause);
	}
}
