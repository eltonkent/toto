/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.lang.reflect;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

/**
 * Represents a mapping of {@link QName} instances to Java class names allowing
 * class aliases and namespace aware mappings of QNames to class names.
 * 
 * @version $Revision: 1345 $
 */
public class QNameMap {

	private String defaultNamespace = "";
	private String defaultPrefix = "";
	private Map javaToQName;
	// lets make the mapping a no-op unless we specify some mapping
	private Map qnameToJava;

	public String getDefaultNamespace() {
		return defaultNamespace;
	}

	public String getDefaultPrefix() {
		return defaultPrefix;
	}

	/**
	 * Returns the Java class name that should be used for the given QName. If
	 * no explicit mapping has been made then the localPart of the QName is used
	 * which is the normal default in XStream.
	 */
	public String getJavaClassName(final QName qname) {
		if (qnameToJava != null) {
			final String answer = (String) qnameToJava.get(qname);
			if (answer != null) {
				return answer;
			}
		}
		return qname.getLocalPart();
	}

	/**
	 * Returns the Java class name that should be used for the given QName. If
	 * no explicit mapping has been made then the localPart of the QName is used
	 * which is the normal default in XStream.
	 */
	public QName getQName(final String javaClassName) {
		if (javaToQName != null) {
			final QName answer = (QName) javaToQName.get(javaClassName);
			if (answer != null) {
				return answer;
			}
		}
		return new QName(defaultNamespace, javaClassName, defaultPrefix);
	}

	/**
	 * Registers the mapping of the type to the QName
	 */
	public synchronized void registerMapping(final QName qname, final Class type) {
		registerMapping(qname, type.getName());
	}

	/**
	 * Registers the mapping of the Java class name to the QName
	 */
	public synchronized void registerMapping(final QName qname,
			final String javaClassName) {
		if (javaToQName == null) {
			javaToQName = Collections.synchronizedMap(new HashMap());
		}
		if (qnameToJava == null) {
			qnameToJava = Collections.synchronizedMap(new HashMap());
		}
		javaToQName.put(javaClassName, qname);
		qnameToJava.put(qname, javaClassName);
	}

	public void setDefaultNamespace(final String defaultNamespace) {
		this.defaultNamespace = defaultNamespace;
	}

	public void setDefaultPrefix(final String defaultPrefix) {
		this.defaultPrefix = defaultPrefix;
	}
}
