/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.lang.reflect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility library to provide helper methods for Java enums.
 * 
 * <p>
 * #ThreadSafe#
 * </p>
 * 
 */
public class EnumUtils {

	/**
	 * Gets the <code>enum</code> for the class, returning <code>null</code> if
	 * not found.
	 * <p>
	 * This method differs from {@link Enum#valueOf} in that it does not throw
	 * an exception for an invalid enum name.
	 * 
	 * @param enumClass
	 *            the class of the <code>enum</code> to get, not null
	 * @param enumName
	 *            the enum name
	 * @return the enum or null if not found
	 */
	public static <E extends Enum<E>> E getEnum(final Class<E> enumClass,
			final String enumName) {
		try {
			return Enum.valueOf(enumClass, enumName);
		} catch (final IllegalArgumentException ex) {
			return null;
		}
	}

	/**
	 * Gets the <code>List</code> of <code>enums</code>.
	 * <p>
	 * This method is useful when you need a list of enums rather than an array.
	 * 
	 * @param enumClass
	 *            the class of the <code>enum</code> to get, not null
	 * @return the modifiable list of enums, never null
	 */
	public static <E extends Enum<E>> List<E> getEnumList(
			final Class<E> enumClass) {
		return new ArrayList<E>(Arrays.asList(enumClass.getEnumConstants()));
	}

	/**
	 * Gets the <code>Map</code> of <code>enums</code> by name.
	 * <p>
	 * This method is useful when you need a map of enums by name.
	 * 
	 * @param enumClass
	 *            the class of the <code>enum</code> to get, not null
	 * @return the modifiable map of enum names to enums, never null
	 */
	public static <E extends Enum<E>> Map<String, E> getEnumMap(
			final Class<E> enumClass) {
		final Map<String, E> map = new LinkedHashMap<String, E>();
		for (final E e : enumClass.getEnumConstants()) {
			map.put(e.name(), e);
		}
		return map;
	}

	/**
	 * Checks if the specified name is a valid <code>enum</code> for the class.
	 * <p>
	 * This method differs from {@link Enum#valueOf} in that checks if the name
	 * is a valid enum without needing to catch the exception.
	 * 
	 * @param enumClass
	 *            the class of the <code>enum</code> to get, not null
	 * @param enumName
	 *            the enum name
	 * @return true if the enum name is valid, otherwise false
	 */
	public static <E extends Enum<E>> boolean isValidEnum(
			final Class<E> enumClass, final String enumName) {
		try {
			Enum.valueOf(enumClass, enumName);
			return true;
		} catch (final IllegalArgumentException ex) {
			return false;
		}
	}

	/**
	 * This constructor is public to permit tools that require a JavaBean
	 * instance to operate.
	 */
	public EnumUtils() {
	}

}
