/**
 * Object utilities as well as classes that help with reflection operations on classes, methods,  fields and enums.
 *
 */
package toto.lang.reflect;