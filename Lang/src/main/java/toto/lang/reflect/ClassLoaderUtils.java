/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.lang.reflect;

import java.io.File;

import android.annotation.TargetApi;
import android.os.Build;
import dalvik.system.DexClassLoader;

/**
 * A class to centralize the class loader management code.
 */
public class ClassLoaderUtils {

	/**
	 * Load a class from a dex'd jar file.
	 * 
	 * @param jarPath
	 *            the path to the jar file
	 * @param loadLocation
	 *            temporary location to load the jar file into.
	 * @param className
	 *            the full path of the class to load
	 * @return the loaded class if found
	 * @throws ClassNotFoundException
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public static Class<Object> loadClassFromJar(final File jarPath,
			final File loadLocation, final String className)
			throws ClassNotFoundException {
		final DexClassLoader classLoader = new DexClassLoader(
				jarPath.toString(), loadLocation.getAbsolutePath(), null,
				ClassLoaderUtils.class.getClassLoader());
		return (Class<Object>) classLoader.loadClass(className);
	}

	/**
	 * Get the loader for the given class.
	 * 
	 * @param clazz
	 *            the class to retrieve the loader for
	 * @return the class loader that loaded the provided class
	 */
	public static ClassLoader getClassLoader(final Class clazz) {
		ClassLoader callersLoader = clazz.getClassLoader();
		if (callersLoader == null) {
			callersLoader = ClassLoader.getSystemClassLoader();
		}
		return callersLoader;
	}

	/**
	 * Return the class loader to be used for instantiating application objects
	 * when required. This is determined based upon the following rules:
	 * <ul>
	 * <li>The specified class loader, if any</li>
	 * <li>The thread context class loader, if it exists and
	 * <code>useContextClassLoader</code> is true</li>
	 * <li>The class loader used to load the calling class.
	 * <li>The System class loader.
	 * </ul>
	 */
	public static ClassLoader getClassLoader(final ClassLoader specifiedLoader,
			final boolean useContextClassLoader, final Class callingClass) {
		if (specifiedLoader != null) {
			return specifiedLoader;
		}
		if (useContextClassLoader) {
			final ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			if (classLoader != null) {
				return classLoader;
			}
		}
		return getClassLoader(callingClass);
	}

	/**
	 * Return the class loader to be used for instantiating application objects
	 * when a context class loader is not specified. This is determined based
	 * upon the following rules:
	 * <ul>
	 * <li>The specified class loader, if any</li>
	 * <li>The class loader used to load the calling class.
	 * <li>The System class loader.
	 * </ul>
	 */
	public static ClassLoader getClassLoader(final ClassLoader specifiedLoader,
			final Class callingClass) {
		if (specifiedLoader != null) {
			return specifiedLoader;
		}
		return getClassLoader(callingClass);
	}

	/**
	 * Loads the given class using the current Thread's context class loader
	 * first otherwise use the class loader which loaded this class.
	 * 
	 * @param className
	 *            The class to be loaded
	 * @param callingClass
	 *            The class which is calling this method
	 * @return The Loaded class
	 */
	public static Class loadClass(final String className,
			final Class callingClass) throws ClassNotFoundException {
		final ClassLoader loader = Thread.currentThread()
				.getContextClassLoader();
		if (loader == null) {
			return getClassLoader(callingClass).loadClass(className);
		} else {
			return loader.loadClass(className);
		}
	}

	/**
	 * Loads the given class using:
	 * <ol>
	 * <li>the specified classloader,</li>
	 * <li>the current Thread's context class loader first, if asked</li>
	 * <li>otherwise use the class loader which loaded this class.</li>
	 * </ol>
	 */
	public static Class loadClass(final String className,
			final ClassLoader specifiedLoader, final boolean useContextLoader,
			final Class callingClass) throws ClassNotFoundException {
		Class clazz = null;
		if (specifiedLoader != null) {
			try {
				clazz = specifiedLoader.loadClass(className);
			} catch (final ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		if (clazz == null && useContextLoader) {
			final ClassLoader contextLoader = Thread.currentThread()
					.getContextClassLoader();
			if (contextLoader != null) {
				try {
					clazz = contextLoader.loadClass(className);
				} catch (final ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		if (clazz == null) {
			final ClassLoader loader = getClassLoader(callingClass);
			clazz = loader.loadClass(className);
		}
		return clazz;
	}
}
