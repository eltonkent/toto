/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.lang.reflect;

public class PackageUtils {
	public static void checkPackageAccess(final Class<Object> clazz) {
		checkPackageAccess(clazz.getName());
	}

	public static void checkPackageAccess(final String name) {
		final SecurityManager s = System.getSecurityManager();
		if (s != null) {
			String cname = name.replace('/', '.');
			if (cname.startsWith("[")) {
				final int b = cname.lastIndexOf('[') + 2;
				if (b > 1 && b < cname.length()) {
					cname = cname.substring(b);
				}
			}
			final int i = cname.lastIndexOf('.');
			if (i != -1) {
				s.checkPackageAccess(cname.substring(0, i));
			}
		}
	}

	public static boolean isPackageAccessible(final Class<Object> clazz) {
		try {
			checkPackageAccess(clazz);
		} catch (final SecurityException e) {
			return false;
		}
		return true;
	}
}
