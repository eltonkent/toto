/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.lang.reflect;

// adapted from org.apache.axis.utils.IDKey

/**
 * Wrap an identity key (System.identityHashCode()) so that an object can only
 * be equal() to itself.
 * 
 * This is necessary to disambiguate the occasional duplicate identityHashCodes
 * that can occur.
 * 
 * @author Apache Software Foundation
 */
final class IDKey {
	private final int id;
	private final Object value;

	/**
	 * Constructor for IDKey
	 * 
	 * @param _value
	 *            The value
	 */
	public IDKey(final Object _value) {
		// This is the Object hashcode
		id = System.identityHashCode(_value);
		// There have been some cases (LANG-459) that return the
		// same identity hash code for different objects. So
		// the value is also added to disambiguate these cases.
		value = _value;
	}

	/**
	 * checks if instances are equal
	 * 
	 * @param other
	 *            The other object to compare to
	 * @return if the instances are for the same object
	 */
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof IDKey)) {
			return false;
		}
		final IDKey idKey = (IDKey) other;
		if (id != idKey.id) {
			return false;
		}
		// Note that identity equals is used.
		return value == idKey.value;
	}

	/**
	 * returns hashcode - i.e. the system identity hashcode.
	 * 
	 * @return the hashcode
	 */
	@Override
	public int hashCode() {
		return id;
	}
}
