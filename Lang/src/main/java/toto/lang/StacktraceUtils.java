package toto.lang;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *Java Stacktrace manipulation and formatting utilities.
 */
public class StacktraceUtils {

    /**
     * Remove all entries of the given packages from a java stacktrace.
     *<p>
     *     The if the pacakge name appears in the root cause (the first line) it will not be removed.
     *</p>
     *
     * @param t
     * @param packages names to remove. for instance, if you would like to remove the "com.something" just add it to the list
     * @return
     */
    public static String removePackageEntries(Throwable t, List<String> packages) {
        StringBuilder sb = new StringBuilder(stackTraceToString(t));
        return replacePackageEntries(sb,packages,"");
    }

    /**
     * Remove the given pacakge entries
     * @param stackTrace
     * @param packages
     * @return
     */
    public static String removePackageEntries(String stackTrace,List<String> packages){
        StringBuilder sb=new StringBuilder(stackTrace);
        return replacePackageEntries(sb,packages,"");
    }


    private static String constructRegex(List<String> packages) {
        if (packages == null || packages.isEmpty()) {
            throw new IllegalArgumentException("Packages cannot be empty or null");
        }
        String pkg = null;
        final String regexPrefix = "(?m)^at ";
        final String regexSuffix="*\n|";
        StringBuilder regex = new StringBuilder();
        for (int i = 0; i < packages.size(); i++) {
            pkg = packages.get(i);
            regex.append(regexPrefix);
            regex.append(pkg);
            if (!pkg.endsWith(".")) {
                regex.append('.');
            }
            regex.append(regexSuffix);
            //this constructs (?m)^at <package name>.*|
        }
        //remove the last '|'
        regex.setLength(regex.length() - 1);
        return regex.toString();
    }

    /**
     * Replace all entries of the given packages from a java stacktrace.
     *
     * @param t
     * @param packages    names to remove. for instance, if you would like to replace the "com.something" just add it to the list
     * @param replaceWith
     * @return
     */
    public static String replacePackageEntries(StringBuilder t, List<String> packages, String replaceWith) {
        String regex=constructRegex(packages);
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(t);
        //check if you need to delete the line or replace the line.
        if(!replaceWith.equals("")){
            replaceWith="\n"+replaceWith;
        }
        return matcher.replaceAll(replaceWith);
    }


    /**
     * Convert a throwable  object to string (stack trace) representation.
     * @param t
     * @return
     */
    public static String stackTraceToString(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        return sw.toString();
    }

    /**
     * Traverses the stack trace element array looking for instances that contain the first or second
     * Strings in the className property.
     * @param firstClass The first class to look for.
     * @param secondClass The second class to look for.
     * @param stackTrace The stack trace.
     * @return true if the first class appears first, false if the second appears first
     * @throws IllegalArgumentException if neither class is found.
     */
    public static boolean isFrameContainingXBeforeFrameContainingY(String firstClass, String secondClass, StackTraceElement[] stackTrace) {
        for (StackTraceElement element : stackTrace) {
            if (element.getClassName().contains(firstClass)) {
                return true;
            }
            else if (element.getClassName().contains(secondClass)) {
                return false;
            }
        }
        throw new IllegalArgumentException("Neither " + firstClass + " nor " + secondClass + " class found");
    }

}
