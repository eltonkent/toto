/**
 * Utilities that help perform really low level operations on primitives and Thread management.
 * @author Mobifluence Interactive
 *
 */
package toto.lang;