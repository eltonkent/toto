/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.lang;

/**
 * Monitors a thread, interrupting it of it reaches the specified timout.
 * <p>
 * <div> This works by sleeping until the specified timout amount and then
 * interrupting the thread being monitored. If the thread being monitored
 * completes its work before being interrupted, it should
 * <code>interrupt()<code>
 * the <i>monitor</i> thread.
 * 
 * <pre>
 *       long timeoutInMillis = 1000;
 *       try {
 *           Thread monitor = ThreadMonitor.start(timeoutInMillis);
 *           <span>// do some work here</span>
 *           ThreadMonitor.stop(monitor);
 *       } catch (InterruptedException e) {
 *           <span>// timed amount was reached</span>
 *       }
 * </pre>
 * </div>
 * </p>
 * 
 */
public class ThreadMonitor implements Runnable {

	private final Thread thread;
	private final long timeout;

	/**
	 * Start monitoring the current thread.
	 * 
	 * @param timeout
	 *            The timout amount in milliseconds or no timeout if the value
	 *            is zero or less
	 * @return The monitor thread or <code>null</code> if the timout amount is
	 *         not greater than zero
	 */
	public static Thread start(final long timeout) {
		return start(Thread.currentThread(), timeout);
	}

	/**
	 * Start monitoring the specified thread.
	 * 
	 * @param thread
	 *            The thread The thread to monitor
	 * @param timeout
	 *            The timout amount in milliseconds or no timeout if the value
	 *            is zero or less
	 * @return The monitor thread or <code>null</code> if the timout amount is
	 *         not greater than zero
	 */
	public static Thread start(final Thread thread, final long timeout) {
		Thread monitor = null;
		if (timeout > 0) {
			final ThreadMonitor timout = new ThreadMonitor(thread, timeout);
			monitor = new Thread(timout, ThreadMonitor.class.getSimpleName());
			monitor.setDaemon(true);
			monitor.start();
		}
		return monitor;
	}

	/**
	 * Stop monitoring the specified thread.
	 * 
	 * @param thread
	 *            The monitor thread, may be <code>null</code>
	 */
	public static void stop(final Thread thread) {
		if (thread != null) {
			thread.interrupt();
		}
	}

	/**
	 * Construct and new monitor.
	 * 
	 * @param thread
	 *            The thread to monitor
	 * @param timeout
	 *            The timout amount in milliseconds
	 */
	private ThreadMonitor(final Thread thread, final long timeout) {
		this.thread = thread;
		this.timeout = timeout;
	}

	/**
	 * Sleep until the specified timout amount and then interrupt the thread
	 * being monitored.
	 * 
	 * @see Runnable#run()
	 */
	@Override
	public void run() {
		try {
			Thread.sleep(timeout);
			thread.interrupt();
		} catch (final InterruptedException e) {
			// timeout not reached
		}
	}
}
