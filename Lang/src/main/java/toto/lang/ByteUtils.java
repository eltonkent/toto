package toto.lang;

import static com.mi.toto.Conditions.checkArgument;
import static com.mi.toto.Conditions.checkNotNull;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * Static utility methods pertaining to {@code byte} primitives, that are not
 * already found in either {@link Byte} or {@link Arrays}, <i>and interpret
 * bytes as neither signed nor unsigned</i>. The methods which specifically
 * treat bytes as signed or unsigned are found in {@link SignedBytes} and
 * {@link UnsignedBytes}.
 *
 *
 */
public final class ByteUtils {
    private ByteUtils() {
    }

    /**
     * Returns a hash code for {@code value}; equal to the result of invoking
     * {@code ((Byte) value).hashCode()}.
     *
     * @param value
     *            a primitive {@code byte} value
     * @return a hash code for the value
     */
    public static int hashCode(final byte value) {
        return value;
    }

    /**
     * Returns {@code true} if {@code target} is present as an element anywhere
     * in {@code array}.
     *
     * @param array
     *            an array of {@code byte} values, possibly empty
     * @param target
     *            a primitive {@code byte} value
     * @return {@code true} if {@code array[i] == target} for some value of
     *         {@code i}
     */
    public static boolean contains(final byte[] array, final byte target) {
        for (final byte value : array) {
            if (value == target) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the index of the first appearance of the value {@code target} in
     * {@code array}.
     *
     * @param array
     *            an array of {@code byte} values, possibly empty
     * @param target
     *            a primitive {@code byte} value
     * @return the least index {@code i} for which {@code array[i] == target},
     *         or {@code -1} if no such index exists.
     */
    public static int indexOf(final byte[] array, final byte target) {
        return indexOf(array, target, 0, array.length);
    }

    public static int indexOf(final byte[] array, final byte target,
                              final int start, final int end) {
        for (int i = start; i < end; i++) {
            if (array[i] == target) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns the start position of the first occurrence of the specified
     * {@code target} within {@code array}, or {@code -1} if there is no such
     * occurrence.
     *
     * <p>
     * More formally, returns the lowest index {@code i} such that
     * {@code java.util.Arrays.copyOfRange(array, i, i + target.length)}
     * contains exactly the same elements as {@code target}.
     *
     * @param array
     *            the array to search for the sequence {@code target}
     * @param target
     *            the array to search for as a sub-sequence of {@code array}
     */
    public static int indexOf(final byte[] array, final byte[] target) {
        if (target.length == 0) {
            return 0;
        }

        outer: for (int i = 0; i < array.length - target.length + 1; i++) {
            for (int j = 0; j < target.length; j++) {
                if (array[i + j] != target[j]) {
                    continue outer;
                }
            }
            return i;
        }
        return -1;
    }

    /**
     * Returns the index of the last appearance of the value {@code target} in
     * {@code array}.
     *
     * @param array
     *            an array of {@code byte} values, possibly empty
     * @param target
     *            a primitive {@code byte} value
     * @return the greatest index {@code i} for which {@code array[i] == target}
     *         , or {@code -1} if no such index exists.
     */
    public static int lastIndexOf(final byte[] array, final byte target) {
        return lastIndexOf(array, target, 0, array.length);
    }

    public static int lastIndexOf(final byte[] array, final byte target,
                                  final int start, final int end) {
        for (int i = end - 1; i >= start; i--) {
            if (array[i] == target) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns the values from each provided array combined into a single array.
     * For example, {@code concat(new byte[] a, b}, new byte[] {}, new byte[]
     * {c}} returns the array {@code a, b, c} .
     *
     * @param arrays
     *            zero or more {@code byte} arrays
     * @return a single array containing all the values from the source arrays,
     *         in order
     */
    public static byte[] concat(final byte[]... arrays) {
        int length = 0;
        for (final byte[] array : arrays) {
            length += array.length;
        }
        final byte[] result = new byte[length];
        int pos = 0;
        for (final byte[] array : arrays) {
            System.arraycopy(array, 0, result, pos, array.length);
            pos += array.length;
        }
        return result;
    }

    /**
     * Returns an array containing the same values as {@code array}, but
     * guaranteed to be of a specified minimum length. If {@code array} already
     * has a length of at least {@code minLength}, it is returned directly.
     * Otherwise, a new array of size {@code minLength + padding} is returned,
     * containing the values of {@code array}, and zeroes in the remaining
     * places.
     *
     * @param array
     *            the source array
     * @param minLength
     *            the minimum length the returned array must guarantee
     * @param padding
     *            an extra amount to "grow" the array by if growth is necessary
     * @throws IllegalArgumentException
     *             if {@code minLength} or {@code padding} is negative
     * @return an array containing the values of {@code array}, with guaranteed
     *         minimum length {@code minLength}
     */

    public static byte[] ensureCapacity(final byte[] array,
                                        final int minLength, final int padding) {
        checkArgument(minLength >= 0, "Invalid minLength: %s", minLength);
        checkArgument(padding >= 0, "Invalid padding: %s", padding);
        return (array.length < minLength) ? copyOf(array, minLength + padding)
                : array;
    }

    // Arrays.copyOf() requires Java 6
    private static byte[] copyOf(final byte[] original, final int length) {
        final byte[] copy = new byte[length];
        System.arraycopy(original, 0, copy, 0,
                Math.min(original.length, length));
        return copy;
    }




    public static short unpack2(final byte[] arr, final int offs) {
        return (short) ((arr[offs] << 8) | (arr[offs + 1] & 0xFF));
    }

    public static int unpack4(final byte[] arr, final int offs) {
        return (arr[offs] << 24) | ((arr[offs + 1] & 0xFF) << 16)
                | ((arr[offs + 2] & 0xFF) << 8) | (arr[offs + 3] & 0xFF);
    }

    public static long unpack8(final byte[] arr, final int offs) {
        return ((long) unpack4(arr, offs) << 32)
                | (unpack4(arr, offs + 4) & 0xFFFFFFFFL);
    }

    public static float unpackF4(final byte[] arr, final int offs) {
        return Float.intBitsToFloat(unpack4(arr, offs));
    }

    public static double unpackF8(final byte[] arr, final int offs) {
        return Double.longBitsToDouble(unpack8(arr, offs));
    }

    public static int skipString(final byte[] arr, int offs) {
        final int len = unpack4(arr, offs);
        offs += 4;
        if (len > 0) {
            offs += len * 2;
        } else if (len < -1) {
            offs -= len + 2;
        }
        return offs;
    }

    public static String unpackString(final byte[] arr, int offs,
                                      final String encoding) throws UnsupportedEncodingException {
        int len = unpack4(arr, offs);
        offs += 4;
        String str = null;
        if (len >= 0) {
            final char[] chars = new char[len];
            for (int i = 0; i < len; i++) {
                chars[i] = (char) unpack2(arr, offs);
                offs += 2;
            }
            str = new String(chars);
        } else if (len < -1) {
            len = -len - 2;
            if (encoding != null) {
                str = new String(arr, offs, len, encoding);
            } else {
                str = new String(arr, offs, len);
            }
            offs += len;
        }
        return str;
    }

    public static String unpackString(final ArrayPos pos, final String encoding)
            throws UnsupportedEncodingException {
        int offs = pos.offs;
        final byte[] arr = pos.body;
        int len = unpack4(arr, offs);
        offs += 4;
        String str = null;
        if (len >= 0) {
            final char[] chars = new char[len];
            for (int i = 0; i < len; i++) {
                chars[i] = (char) unpack2(arr, offs);
                offs += 2;
            }
            str = new String(chars);
        } else if (len < -1) {
            len = -len - 2;
            if (encoding != null) {
                str = new String(arr, offs, len, encoding);
            } else {
                str = new String(arr, offs, len);
            }
            offs += len;
        }
        pos.offs = offs;
        return str;
    }

    public static class ArrayPos {
        public byte[] body;
        public int offs;

        public ArrayPos(final byte[] body, final int offs) {
            this.body = body;
            this.offs = offs;
        }
    }

    public static void pack2(final byte[] arr, final int offs, final short val) {
        arr[offs] = (byte) (val >> 8);
        arr[offs + 1] = (byte) val;
    }

    public static void pack4(final byte[] arr, final int offs, final int val) {
        arr[offs] = (byte) (val >> 24);
        arr[offs + 1] = (byte) (val >> 16);
        arr[offs + 2] = (byte) (val >> 8);
        arr[offs + 3] = (byte) val;
    }

    public static void pack8(final byte[] arr, final int offs, final long val) {
        pack4(arr, offs, (int) (val >> 32));
        pack4(arr, offs + 4, (int) val);
    }

    public static void packF4(final byte[] arr, final int offs, final float val) {
        pack4(arr, offs, Float.floatToIntBits(val));
    }

    public static void packF8(final byte[] arr, final int offs, final double val) {
        pack8(arr, offs, Double.doubleToLongBits(val));
    }

    public static int packString(final byte[] arr, int offs, final String str,
                                 final String encoding) throws UnsupportedEncodingException {
        if (str == null) {
            pack4(arr, offs, -1);
            offs += 4;
        } else if (encoding == null) {
            final int n = str.length();
            pack4(arr, offs, n);
            offs += 4;
            for (int i = 0; i < n; i++) {
                pack2(arr, offs, (short) str.charAt(i));
                offs += 2;
            }
        } else {
            final byte[] bytes = str.getBytes(encoding);
            pack4(arr, offs, -2 - bytes.length);
            System.arraycopy(bytes, 0, arr, offs + 4, bytes.length);
            offs += 4 + bytes.length;
        }
        return offs;
    }

    public static int sizeof(final String str, final String encoding)
            throws UnsupportedEncodingException {
        return str == null ? 4 : encoding == null ? 4 + str.length() * 2
                : 4 + new String(str).getBytes(encoding).length;
    }

    /**
     * Convert the byte array to an int starting from the given offset.
     *
     * @param b
     *            The byte array
     * @param offset
     *            The array offset
     * @return The integer
     */
    public static int toInt(final byte[] b, final int offset) {
        int value = 0x00000000;
        for (int i = 0; i < 3; i++) {
            final int shift = (3 - 1 - i) * 8;
            value |= (b[i + offset] & 0x000000FF) << shift;
        }
        value = value & 0x00FFFFFF;
        return value;
    }
}
