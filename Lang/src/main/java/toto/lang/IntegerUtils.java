package toto.lang;

import static com.mi.toto.Conditions.checkElementIndex;
import static com.mi.toto.Conditions.checkNotNull;
import static com.mi.toto.Conditions.checkPositionIndexes;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.RandomAccess;

/**
 * Static utility methods pertaining to {@code int} primitives, that are not
 * already found in either {@link Integer} or {@link Arrays}.
 * 
 */
public final class IntegerUtils {
	private IntegerUtils() {
	}

	/**
	 * The number of bytes required to represent a primitive {@code int} value.
	 */
	public static final int BYTES = Integer.SIZE / Byte.SIZE;

	/**
	 * The largest power of two that can be represented as an {@code int}.
	 * 
	 * @since 10.0
	 */
	public static final int MAX_POWER_OF_TWO = 1 << (Integer.SIZE - 2);

	/**
	 * Returns a hash code for {@code value}; equal to the result of invoking
	 * {@code ((Integer) value).hashCode()}.
	 * 
	 * @param value
	 *            a primitive {@code int} value
	 * @return a hash code for the value
	 */
	public static int hashCode(final int value) {
		return value;
	}

	/**
	 * Returns the {@code int} value that is equal to {@code value}, if
	 * possible.
	 * 
	 * @param value
	 *            any value in the range of the {@code int} type
	 * @return the {@code int} value that equals {@code value}
	 * @throws IllegalArgumentException
	 *             if {@code value} is greater than {@link Integer#MAX_VALUE} or
	 *             less than {@link Integer#MIN_VALUE}
	 */
	public static int checkedCast(final long value) {
		final int result = (int) value;
		if (result != value) {
			// don't use checkArgument here, to avoid boxing
			throw new IllegalArgumentException("Out of range: " + value);
		}
		return result;
	}

	/**
	 * Returns the {@code int} nearest in value to {@code value}.
	 * 
	 * @param value
	 *            any {@code long} value
	 * @return the same value cast to {@code int} if it is in the range of the
	 *         {@code int} type, {@link Integer#MAX_VALUE} if it is too large,
	 *         or {@link Integer#MIN_VALUE} if it is too small
	 */
	public static int saturatedCast(final long value) {
		if (value > Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		}
		if (value < Integer.MIN_VALUE) {
			return Integer.MIN_VALUE;
		}
		return (int) value;
	}

	/**
	 * Compares the two specified {@code int} values. The sign of the value
	 * returned is the same as that of {@code ((Integer) a).compareTo(b)}.
	 * 
	 * <p>
	 * <b>Note:</b> projects using JDK 7 or later should use the equivalent
	 * {@link Integer#compareTo(Integer)} method instead.
	 * 
	 * @param a
	 *            the first {@code int} to compare
	 * @param b
	 *            the second {@code int} to compare
	 * @return a negative value if {@code a} is less than {@code b}; a positive
	 *         value if {@code a} is greater than {@code b}; or zero if they are
	 *         equal
	 */
	public static int compare(final int a, final int b) {
		return (a < b) ? -1 : ((a > b) ? 1 : 0);
	}

	/**
	 * Returns a comparator that compares two {@code int} arrays
	 * lexicographically. That is, it compares, using {@link #compare(int, int)}
	 * ), the first pair of values that follow any common prefix, or when one
	 * array is a prefix of the other, treats the shorter array as the lesser.
	 * For example, {@code [] < [1] < [1, 2] < [2]}.
	 * 
	 * <p>
	 * The returned comparator is inconsistent with
	 * {@link Object#equals(Object)} (since arrays support only identity
	 * equality), but it is consistent with {@link Arrays#equals(int[], int[])}.
	 * 
	 * @see <a href="http://en.wikipedia.org/wiki/Lexicographical_order">
	 *      Lexicographical order article at Wikipedia</a>
	 * @since 2.0
	 */
	public static Comparator<int[]> lexicographicalComparator() {
		return LexicographicalComparator.INSTANCE;
	}

	private enum LexicographicalComparator implements Comparator<int[]> {
		INSTANCE;

		@Override
		public int compare(final int[] left, final int[] right) {
			final int minLength = Math.min(left.length, right.length);
			for (int i = 0; i < minLength; i++) {
				final int result = IntegerUtils.compare(left[i], right[i]);
				if (result != 0) {
					return result;
				}
			}
			return left.length - right.length;
		}
	}

	/**
	 * Returns an array containing each value of {@code collection}, converted
	 * to a {@code int} value in the manner of {@link Number#intValue}.
	 * 
	 * <p>
	 * Elements are copied from the argument collection as if by
	 * {@code collection.toArray()}. Calling this method is as thread-safe as
	 * calling that method.
	 * 
	 * @param collection
	 *            a collection of {@code Number} instances
	 * @return an array containing the same values as {@code collection}, in the
	 *         same order, converted to primitives
	 * @throws NullPointerException
	 *             if {@code collection} or any of its elements is null
	 * @since 1.0 (parameter was {@code Collection<Integer>} before 12.0)
	 */
	public static int[] toArray(final Collection<? extends Number> collection) {
		if (collection instanceof IntArrayAsList) {
			return ((IntArrayAsList) collection).toIntArray();
		}

		final Object[] boxedArray = collection.toArray();
		final int len = boxedArray.length;
		final int[] array = new int[len];
		for (int i = 0; i < len; i++) {
			// checkNotNull for GWT (do not optimize)
			array[i] = ((Number) checkNotNull(boxedArray[i])).intValue();
		}
		return array;
	}

	/**
	 * Returns a fixed-size list backed by the specified array, similar to
	 * {@link Arrays#asList(Object[])}. The list supports
	 * {@link List#set(int, Object)}, but any attempt to setKey a value to
	 * {@code null} will result in a {@link NullPointerException}.
	 * 
	 * <p>
	 * The returned list maintains the values, but not the identities, of
	 * {@code Integer} objects written to or read from it. For example, whether
	 * {@code list.get(0) == list.get(0)} is true for the returned list is
	 * unspecified.
	 * 
	 * @param backingArray
	 *            the array to back the list
	 * @return a list view of the array
	 */
	public static List<Integer> asList(final int... backingArray) {
		if (backingArray.length == 0) {
			return Collections.emptyList();
		}
		return new IntArrayAsList(backingArray);
	}

	private static class IntArrayAsList extends AbstractList<Integer> implements
			RandomAccess, Serializable {
		final int[] array;
		final int start;
		final int end;

		IntArrayAsList(final int[] array) {
			this(array, 0, array.length);
		}

		IntArrayAsList(final int[] array, final int start, final int end) {
			this.array = array;
			this.start = start;
			this.end = end;
		}

		@Override
		public int size() {
			return end - start;
		}

		@Override
		public boolean isEmpty() {
			return false;
		}

		@Override
		public Integer get(final int index) {
			checkElementIndex(index, size());
			return array[start + index];
		}

		@Override
		public boolean contains(final Object target) {
			// Overridden to prevent a ton of boxing
			return (target instanceof Integer)
					&& ArrayUtils.indexOf(array, (Integer) target, start, end) != -1;
		}

		@Override
		public int indexOf(final Object target) {
			// Overridden to prevent a ton of boxing
			if (target instanceof Integer) {
				final int i = ArrayUtils.indexOf(array, (Integer) target,
						start, end);
				if (i >= 0) {
					return i - start;
				}
			}
			return -1;
		}

		@Override
		public int lastIndexOf(final Object target) {
			// Overridden to prevent a ton of boxing
			if (target instanceof Integer) {
				final int i = ArrayUtils.lastIndexOf(array, (Integer) target,
						start, end);
				if (i >= 0) {
					return i - start;
				}
			}
			return -1;
		}

		@Override
		public Integer set(final int index, final Integer element) {
			checkElementIndex(index, size());
			final int oldValue = array[start + index];
			// checkNotNull for GWT (do not optimize)
			array[start + index] = checkNotNull(element);
			return oldValue;
		}

		@Override
		public List<Integer> subList(final int fromIndex, final int toIndex) {
			final int size = size();
			checkPositionIndexes(fromIndex, toIndex, size);
			if (fromIndex == toIndex) {
				return Collections.emptyList();
			}
			return new IntArrayAsList(array, start + fromIndex, start + toIndex);
		}

		@Override
		public boolean equals(final Object object) {
			if (object == this) {
				return true;
			}
			if (object instanceof IntArrayAsList) {
				final IntArrayAsList that = (IntArrayAsList) object;
				final int size = size();
				if (that.size() != size) {
					return false;
				}
				for (int i = 0; i < size; i++) {
					if (array[start + i] != that.array[that.start + i]) {
						return false;
					}
				}
				return true;
			}
			return super.equals(object);
		}

		@Override
		public int hashCode() {
			int result = 1;
			for (int i = start; i < end; i++) {
				result = 31 * result + IntegerUtils.hashCode(array[i]);
			}
			return result;
		}

		@Override
		public String toString() {
			final StringBuilder builder = new StringBuilder(size() * 5);
			builder.append('[').append(array[start]);
			for (int i = start + 1; i < end; i++) {
				builder.append(", ").append(array[i]);
			}
			return builder.append(']').toString();
		}

		int[] toIntArray() {
			// Arrays.copyOfRange() is not available under GWT
			final int size = size();
			final int[] result = new int[size];
			System.arraycopy(array, start, result, 0, size);
			return result;
		}

		private static final long serialVersionUID = 0;
	}

	/**
	 * Convert integer to array of bytes.
	 * 
	 * @param i
	 * @return
	 */
	public static byte[] toBytes(final int i) {
		final byte[] result = new byte[4];
		toBytes(i, result);
		return result;
	}

	/**
	 * 
	 * @param i
	 * @param intBytes
	 *            4 element byte array
	 */
	public static void toBytes(final int i, final byte[] intBytes) {
		intBytes[0] = (byte) (i >> 24);
		intBytes[1] = (byte) (i >> 16);
		intBytes[2] = (byte) (i >> 8);
		intBytes[3] = (byte) (i /* >> 0 */);
	}

	public static int fromBytes(final byte[] bytes, final int offset) {
		int ret = 0;
		for (int i = offset; i < (offset + 4); i++) {
			ret <<= 8;
			ret |= bytes[i] & 0xFF;
		}
		return ret;
	}

	public static int fromBytes(final byte[] bytes) {
		return fromBytes(bytes, 0);
	}

	/**
	 * Converts an integer array to byte array. <b>Do not use this method if the
	 * <code>intArray</code> is used after this method call.</b>
	 * <p>
	 * The intArray is recycled (all values changed to 0). The heap consumed is
	 * ~ the same as the <code>intArray</code>
	 * </p>
	 * 
	 * @param intArray
	 * @param from
	 *            Starting index of the conversion.
	 * @return
	 * @see #toBytes(int[], int)
	 * @see ByteUtils#toInt(byte[], int)
	 * 
	 */
	public static byte[] toBytesEfficient(final int[] intArray, final int from) {
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		final DataOutputStream dos = new DataOutputStream(bos);
		int i;
		try {
			for (i = 0; i < intArray.length; i++) {
				dos.writeInt(intArray[i]);
				intArray[i] = 0;
			}
			dos.flush();
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return bos.toByteArray();
	}

	/**
	 * Converts an integer array to byte array.
	 * 
	 * @param intArray
	 * @param from
	 *            Starting index of the conversion.
	 * @return
	 * @see #toBytesEfficient(int[], int)
	 * @see ByteUtils#toInt(byte[], int)
	 */
	public static byte[] toBytes(final int[] intArray, final int from) {
		final int len = intArray.length - from;
		final ByteBuffer bb = ByteBuffer.allocate(4 * len);
		int i;
		for (i = from; i < intArray.length; i++) {
			bb.putInt(intArray[i]);
		}
		return bb.array();
	}
}
