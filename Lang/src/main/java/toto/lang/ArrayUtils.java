/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.lang;

import static com.mi.toto.Conditions.checkArgument;
import static com.mi.toto.Conditions.checkNotNull;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

import toto.lang.reflect.EqualsBuilder;
import toto.lang.reflect.ToStringBuilder;
import toto.lang.reflect.ToStringStyle;

/**
 * Array utilities with an extension to java.util.Arrays.
 * <p>
 * <div> Operations on arrays, primitive arrays (like <code>int[]</code>) and
 * primitive wrapper arrays (like <code>Integer[]</code>).
 * 
 * This class tries to handle <code>null</code> input gracefully. An exception
 * will not be thrown for a <code>null</code> array input. However, an Object
 * array that contains a <code>null</code> element may throw an exception. Each
 * method documents its behaviour. #ThreadSafe# </div>
 * </p>
 */
public class ArrayUtils {

	/**
	 * The index value when an element is not found in a list or array:
	 * <code>-1</code>. This value is returned by methods in this class and can
	 * also be used in comparisons with values returned by various method from
	 * {@link java.util.List}.
	 */
	public static final int INDEX_NOT_FOUND = -1;

	/**
	 * Moves a number of entries in an array to another point in the array,
	 * shifting those inbetween as required.
	 * 
	 * @param array
	 *            The array to alter
	 * @param moveFrom
	 *            The (0 based) index of the first entry to move
	 * @param moveTo
	 *            The (0 based) index of the positition to move to
	 * @param numToMove
	 *            The number of entries to move
	 */
	public static void arrayMoveWithin(final Object[] array,
			final int moveFrom, final int moveTo, final int numToMove) {
		// If we're not asked to do anything, return now
		if (numToMove <= 0) {
			return;
		}
		if (moveFrom == moveTo) {
			return;
		}

		// Check that the values supplied are valid
		if (moveFrom < 0 || moveFrom >= array.length) {
			throw new IllegalArgumentException(
					"The moveFrom must be a valid array index");
		}
		if (moveTo < 0 || moveTo >= array.length) {
			throw new IllegalArgumentException(
					"The moveTo must be a valid array index");
		}
		if (moveFrom + numToMove > array.length) {
			throw new IllegalArgumentException(
					"Asked to move more entries than the array has");
		}
		if (moveTo + numToMove > array.length) {
			throw new IllegalArgumentException(
					"Asked to move to a position that doesn't have enough space");
		}

		// Grab the bit to move
		final Object[] toMove = new Object[numToMove];
		System.arraycopy(array, moveFrom, toMove, 0, numToMove);

		// Grab the bit to be shifted
		Object[] toShift;
		int shiftTo;
		if (moveFrom > moveTo) {
			// Moving to an earlier point in the array
			// Grab everything between the two points
			toShift = new Object[(moveFrom - moveTo)];
			System.arraycopy(array, moveTo, toShift, 0, toShift.length);
			shiftTo = moveTo + numToMove;
		} else {
			// Moving to a later point in the array
			// Grab everything from after the toMove block, to the new point
			toShift = new Object[(moveTo - moveFrom)];
			System.arraycopy(array, moveFrom + numToMove, toShift, 0,
					toShift.length);
			shiftTo = moveFrom;
		}

		// Copy the moved block to its new location
		System.arraycopy(toMove, 0, array, moveTo, toMove.length);

		// And copy the shifted block to the shifted location
		System.arraycopy(toShift, 0, array, shiftTo, toShift.length);

		// We're done - array will now have everything moved as required
	}

	/**
	 * <p>
	 * Copies the given array and adds the given element at the end of the new
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * The new array contains the same elements of the input array plus the
	 * given element in the last position. The component type of the new array
	 * is the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, true)          = [true]
	 * ArrayUtils.add([true], false)       = [true, false]
	 * ArrayUtils.add([true, false], true) = [true, false, true]
	 * </pre>
	 * 
	 * @param array
	 *            the array to copy and add the element to, may be
	 *            <code>null</code>
	 * @param element
	 *            the object to add at the last index of the new array
	 * @return A new array containing the existing elements plus the new element
	 * @since 2.1
	 */
	public static boolean[] add(final boolean[] array, final boolean element) {
		final boolean[] newArray = (boolean[]) copyArrayGrow1(array,
				Boolean.TYPE);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	/**
	 * <p>
	 * Inserts the specified element at the specified position in the array.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * plus the given element on the specified position. The component type of
	 * the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, 0, true)          = [true]
	 * ArrayUtils.add([true], 0, false)       = [false, true]
	 * ArrayUtils.add([false], 1, true)       = [false, true]
	 * ArrayUtils.add([true, false], 1, true) = [true, true, false]
	 * </pre>
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @return A new array containing the existing elements and the new element
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >
	 *             array.length).
	 */
	public static boolean[] add(final boolean[] array, final int index,
			final boolean element) {
		return (boolean[]) add(array, index, Boolean.valueOf(element),
				Boolean.TYPE);
	}

	/**
	 * <p>
	 * Copies the given array and adds the given element at the end of the new
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * The new array contains the same elements of the input array plus the
	 * given element in the last position. The component type of the new array
	 * is the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, 0)   = [0]
	 * ArrayUtils.add([1], 0)    = [1, 0]
	 * ArrayUtils.add([1, 0], 1) = [1, 0, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to copy and add the element to, may be
	 *            <code>null</code>
	 * @param element
	 *            the object to add at the last index of the new array
	 * @return A new array containing the existing elements plus the new element
	 * @since 2.1
	 */
	public static byte[] add(final byte[] array, final byte element) {
		final byte[] newArray = (byte[]) copyArrayGrow1(array, Byte.TYPE);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	/**
	 * <p>
	 * Inserts the specified element at the specified position in the array.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * plus the given element on the specified position. The component type of
	 * the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add([1], 0, 2)         = [2, 1]
	 * ArrayUtils.add([2, 6], 2, 3)      = [2, 6, 3]
	 * ArrayUtils.add([2, 6], 0, 1)      = [1, 2, 6]
	 * ArrayUtils.add([2, 6, 3], 2, 1)   = [2, 6, 1, 3]
	 * </pre>
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @return A new array containing the existing elements and the new element
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >
	 *             array.length).
	 */
	public static byte[] add(final byte[] array, final int index,
			final byte element) {
		return (byte[]) add(array, index, Byte.valueOf(element), Byte.TYPE);
	}

	/**
	 * <p>
	 * Copies the given array and adds the given element at the end of the new
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * The new array contains the same elements of the input array plus the
	 * given element in the last position. The component type of the new array
	 * is the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, '0')       = ['0']
	 * ArrayUtils.add(['1'], '0')      = ['1', '0']
	 * ArrayUtils.add(['1', '0'], '1') = ['1', '0', '1']
	 * </pre>
	 * 
	 * @param array
	 *            the array to copy and add the element to, may be
	 *            <code>null</code>
	 * @param element
	 *            the object to add at the last index of the new array
	 * @return A new array containing the existing elements plus the new element
	 * @since 2.1
	 */
	public static char[] add(final char[] array, final char element) {
		final char[] newArray = (char[]) copyArrayGrow1(array, Character.TYPE);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	/**
	 * <p>
	 * Inserts the specified element at the specified position in the array.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * plus the given element on the specified position. The component type of
	 * the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, 0, 'a')            = ['a']
	 * ArrayUtils.add(['a'], 0, 'b')           = ['b', 'a']
	 * ArrayUtils.add(['a', 'b'], 0, 'c')      = ['c', 'a', 'b']
	 * ArrayUtils.add(['a', 'b'], 1, 'k')      = ['a', 'k', 'b']
	 * ArrayUtils.add(['a', 'b', 'c'], 1, 't') = ['a', 't', 'b', 'c']
	 * </pre>
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @return A new array containing the existing elements and the new element
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >
	 *             array.length).
	 */
	public static char[] add(final char[] array, final int index,
			final char element) {
		return (char[]) add(array, index, Character.valueOf(element),
				Character.TYPE);
	}

	/**
	 * <p>
	 * Copies the given array and adds the given element at the end of the new
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * The new array contains the same elements of the input array plus the
	 * given element in the last position. The component type of the new array
	 * is the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, 0)   = [0]
	 * ArrayUtils.add([1], 0)    = [1, 0]
	 * ArrayUtils.add([1, 0], 1) = [1, 0, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to copy and add the element to, may be
	 *            <code>null</code>
	 * @param element
	 *            the object to add at the last index of the new array
	 * @return A new array containing the existing elements plus the new element
	 * @since 2.1
	 */
	public static double[] add(final double[] array, final double element) {
		final double[] newArray = (double[]) copyArrayGrow1(array, Double.TYPE);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	/**
	 * <p>
	 * Inserts the specified element at the specified position in the array.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * plus the given element on the specified position. The component type of
	 * the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add([1.1], 0, 2.2)              = [2.2, 1.1]
	 * ArrayUtils.add([2.3, 6.4], 2, 10.5)        = [2.3, 6.4, 10.5]
	 * ArrayUtils.add([2.6, 6.7], 0, -4.8)        = [-4.8, 2.6, 6.7]
	 * ArrayUtils.add([2.9, 6.0, 0.3], 2, 1.0)    = [2.9, 6.0, 1.0, 0.3]
	 * </pre>
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @return A new array containing the existing elements and the new element
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >
	 *             array.length).
	 */
	public static double[] add(final double[] array, final int index,
			final double element) {
		return (double[]) add(array, index, Double.valueOf(element),
				Double.TYPE);
	}

	/**
	 * <p>
	 * Copies the given array and adds the given element at the end of the new
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * The new array contains the same elements of the input array plus the
	 * given element in the last position. The component type of the new array
	 * is the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, 0)   = [0]
	 * ArrayUtils.add([1], 0)    = [1, 0]
	 * ArrayUtils.add([1, 0], 1) = [1, 0, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to copy and add the element to, may be
	 *            <code>null</code>
	 * @param element
	 *            the object to add at the last index of the new array
	 * @return A new array containing the existing elements plus the new element
	 * @since 2.1
	 */
	public static float[] add(final float[] array, final float element) {
		final float[] newArray = (float[]) copyArrayGrow1(array, Float.TYPE);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	/**
	 * <p>
	 * Inserts the specified element at the specified position in the array.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * plus the given element on the specified position. The component type of
	 * the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add([1.1f], 0, 2.2f)               = [2.2f, 1.1f]
	 * ArrayUtils.add([2.3f, 6.4f], 2, 10.5f)        = [2.3f, 6.4f, 10.5f]
	 * ArrayUtils.add([2.6f, 6.7f], 0, -4.8f)        = [-4.8f, 2.6f, 6.7f]
	 * ArrayUtils.add([2.9f, 6.0f, 0.3f], 2, 1.0f)   = [2.9f, 6.0f, 1.0f, 0.3f]
	 * </pre>
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @return A new array containing the existing elements and the new element
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >
	 *             array.length).
	 */
	public static float[] add(final float[] array, final int index,
			final float element) {
		return (float[]) add(array, index, Float.valueOf(element), Float.TYPE);
	}

	/**
	 * <p>
	 * Copies the given array and adds the given element at the end of the new
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * The new array contains the same elements of the input array plus the
	 * given element in the last position. The component type of the new array
	 * is the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, 0)   = [0]
	 * ArrayUtils.add([1], 0)    = [1, 0]
	 * ArrayUtils.add([1, 0], 1) = [1, 0, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to copy and add the element to, may be
	 *            <code>null</code>
	 * @param element
	 *            the object to add at the last index of the new array
	 * @return A new array containing the existing elements plus the new element
	 * @since 2.1
	 */
	public static int[] add(final int[] array, final int element) {
		final int[] newArray = (int[]) copyArrayGrow1(array, Integer.TYPE);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	/**
	 * <p>
	 * Inserts the specified element at the specified position in the array.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * plus the given element on the specified position. The component type of
	 * the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add([1], 0, 2)         = [2, 1]
	 * ArrayUtils.add([2, 6], 2, 10)     = [2, 6, 10]
	 * ArrayUtils.add([2, 6], 0, -4)     = [-4, 2, 6]
	 * ArrayUtils.add([2, 6, 3], 2, 1)   = [2, 6, 1, 3]
	 * </pre>
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @return A new array containing the existing elements and the new element
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >
	 *             array.length).
	 */
	public static int[] add(final int[] array, final int index,
			final int element) {
		return (int[]) add(array, index, Integer.valueOf(element), Integer.TYPE);
	}

	/**
	 * <p>
	 * Inserts the specified element at the specified position in the array.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * plus the given element on the specified position. The component type of
	 * the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add([1L], 0, 2L)           = [2L, 1L]
	 * ArrayUtils.add([2L, 6L], 2, 10L)      = [2L, 6L, 10L]
	 * ArrayUtils.add([2L, 6L], 0, -4L)      = [-4L, 2L, 6L]
	 * ArrayUtils.add([2L, 6L, 3L], 2, 1L)   = [2L, 6L, 1L, 3L]
	 * </pre>
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @return A new array containing the existing elements and the new element
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >
	 *             array.length).
	 */
	public static long[] add(final long[] array, final int index,
			final long element) {
		return (long[]) add(array, index, Long.valueOf(element), Long.TYPE);
	}

	/**
	 * <p>
	 * Copies the given array and adds the given element at the end of the new
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * The new array contains the same elements of the input array plus the
	 * given element in the last position. The component type of the new array
	 * is the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, 0)   = [0]
	 * ArrayUtils.add([1], 0)    = [1, 0]
	 * ArrayUtils.add([1, 0], 1) = [1, 0, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to copy and add the element to, may be
	 *            <code>null</code>
	 * @param element
	 *            the object to add at the last index of the new array
	 * @return A new array containing the existing elements plus the new element
	 * @since 2.1
	 */
	public static long[] add(final long[] array, final long element) {
		final long[] newArray = (long[]) copyArrayGrow1(array, Long.TYPE);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	/**
	 * Underlying implementation of add(array, index, element) methods. The last
	 * parameter is the class, which may not equal element.getClass for
	 * primitives.
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @param clss
	 *            the type of the element being added
	 * @return A new array containing the existing elements and the new element
	 */
	private static Object add(final Object array, final int index,
			final Object element, final Class<?> clss) {
		if (array == null) {
			if (index != 0) {
				throw new IndexOutOfBoundsException("Index: " + index
						+ ", Length: 0");
			}
			final Object joinedArray = Array.newInstance(clss, 1);
			Array.set(joinedArray, 0, element);
			return joinedArray;
		}
		final int length = Array.getLength(array);
		if (index > length || index < 0) {
			throw new IndexOutOfBoundsException("Index: " + index
					+ ", Length: " + length);
		}
		final Object result = Array.newInstance(clss, length + 1);
		System.arraycopy(array, 0, result, 0, index);
		Array.set(result, index, element);
		if (index < length) {
			System.arraycopy(array, index, result, index + 1, length - index);
		}
		return result;
	}

	/**
	 * <p>
	 * Inserts the specified element at the specified position in the array.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * plus the given element on the specified position. The component type of
	 * the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add([1], 0, 2)         = [2, 1]
	 * ArrayUtils.add([2, 6], 2, 10)     = [2, 6, 10]
	 * ArrayUtils.add([2, 6], 0, -4)     = [-4, 2, 6]
	 * ArrayUtils.add([2, 6, 3], 2, 1)   = [2, 6, 1, 3]
	 * </pre>
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @return A new array containing the existing elements and the new element
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >
	 *             array.length).
	 */
	public static short[] add(final short[] array, final int index,
			final short element) {
		return (short[]) add(array, index, Short.valueOf(element), Short.TYPE);
	}

	/**
	 * <p>
	 * Copies the given array and adds the given element at the end of the new
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * The new array contains the same elements of the input array plus the
	 * given element in the last position. The component type of the new array
	 * is the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, 0)   = [0]
	 * ArrayUtils.add([1], 0)    = [1, 0]
	 * ArrayUtils.add([1, 0], 1) = [1, 0, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to copy and add the element to, may be
	 *            <code>null</code>
	 * @param element
	 *            the object to add at the last index of the new array
	 * @return A new array containing the existing elements plus the new element
	 * @since 2.1
	 */
	public static short[] add(final short[] array, final short element) {
		final short[] newArray = (short[]) copyArrayGrow1(array, Short.TYPE);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	/**
	 * <p>
	 * Inserts the specified element at the specified position in the array.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * plus the given element on the specified position. The component type of
	 * the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, 0, null)      = [null]
	 * ArrayUtils.add(null, 0, "a")       = ["a"]
	 * ArrayUtils.add(["a"], 1, null)     = ["a", null]
	 * ArrayUtils.add(["a"], 1, "b")      = ["a", "b"]
	 * ArrayUtils.add(["a", "b"], 3, "c") = ["a", "b", "c"]
	 * </pre>
	 * 
	 * @param array
	 *            the array to add the element to, may be <code>null</code>
	 * @param index
	 *            the position of the new object
	 * @param element
	 *            the object to add
	 * @return A new array containing the existing elements and the new element
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >
	 *             array.length).
	 * @throws IllegalArgumentException
	 *             if both array and element are null
	 */
	public static <T> T[] add(final T[] array, final int index, final T element) {
		Class<?> clss = null;
		if (array != null) {
			clss = array.getClass().getComponentType();
		} else if (element != null) {
			clss = element.getClass();
		} else {
			throw new IllegalArgumentException(
					"Array and element cannot both be null");
		}
		@SuppressWarnings("unchecked")
		// the add method creates an array of type clss, which is type T
		final T[] newArray = (T[]) add(array, index, element, clss);
		return newArray;
	}

	/**
	 * <p>
	 * Copies the given array and adds the given element at the end of the new
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * The new array contains the same elements of the input array plus the
	 * given element in the last position. The component type of the new array
	 * is the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, a new one element array is
	 * returned whose component type is the same as the element, unless the
	 * element itself is null, in which case the return type is Object[]
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.add(null, null)      = [null]
	 * ArrayUtils.add(null, "a")       = ["a"]
	 * ArrayUtils.add(["a"], null)     = ["a", null]
	 * ArrayUtils.add(["a"], "b")      = ["a", "b"]
	 * ArrayUtils.add(["a", "b"], "c") = ["a", "b", "c"]
	 * </pre>
	 * 
	 * @param array
	 *            the array to "add" the element to, may be <code>null</code>
	 * @param element
	 *            the object to add, may be <code>null</code>
	 * @return A new array containing the existing elements plus the new element
	 *         The returned array type will be that of the input array (unless
	 *         null), in which case it will have the same type as the element.
	 *         If both are null, an IllegalArgumentException is thrown
	 * @throws IllegalArgumentException
	 *             if both arguments are null
	 * @since 2.1
	 */
	public static <T> T[] add(final T[] array, final T element) {
		Class<?> type;
		if (array != null) {
			type = array.getClass();
		} else if (element != null) {
			type = element.getClass();
		} else {
			throw new IllegalArgumentException("Arguments cannot both be null");
		}
		@SuppressWarnings("unchecked")
		final// type must be T
		T[] newArray = (T[]) copyArrayGrow1(array, type);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	/**
	 * <p>
	 * Adds all the elements of the given arrays into a new array.
	 * </p>
	 * <p>
	 * The new array contains all of the element of <code>array1</code> followed
	 * by all of the elements <code>array2</code>. When an array is returned, it
	 * is always a new array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * </pre>
	 * 
	 * @param array1
	 *            the first array whose elements are added to the new array.
	 * @param array2
	 *            the second array whose elements are added to the new array.
	 * @return The new boolean[] array.
	 * @since 2.1
	 */
	public static boolean[] addAll(final boolean[] array1,
			final boolean... array2) {
		if (array1 == null) {
			return array2.clone();
		} else if (array2 == null) {
			return array1.clone();
		}
		final boolean[] joinedArray = new boolean[array1.length + array2.length];
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
		return joinedArray;
	}

	/**
	 * <p>
	 * Adds all the elements of the given arrays into a new array.
	 * </p>
	 * <p>
	 * The new array contains all of the element of <code>array1</code> followed
	 * by all of the elements <code>array2</code>. When an array is returned, it
	 * is always a new array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * </pre>
	 * 
	 * @param array1
	 *            the first array whose elements are added to the new array.
	 * @param array2
	 *            the second array whose elements are added to the new array.
	 * @return The new byte[] array.
	 * @since 2.1
	 */
	public static byte[] addAll(final byte[] array1, final byte... array2) {
		if (array1 == null) {
			return array2.clone();
		} else if (array2 == null) {
			return array1.clone();
		}
		final byte[] joinedArray = new byte[array1.length + array2.length];
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
		return joinedArray;
	}

	/**
	 * <p>
	 * Adds all the elements of the given arrays into a new array.
	 * </p>
	 * <p>
	 * The new array contains all of the element of <code>array1</code> followed
	 * by all of the elements <code>array2</code>. When an array is returned, it
	 * is always a new array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * </pre>
	 * 
	 * @param array1
	 *            the first array whose elements are added to the new array.
	 * @param array2
	 *            the second array whose elements are added to the new array.
	 * @return The new char[] array.
	 * @since 2.1
	 */
	public static char[] addAll(final char[] array1, final char... array2) {
		if (array1 == null) {
			return array2.clone();
		} else if (array2 == null) {
			return array1.clone();
		}
		final char[] joinedArray = new char[array1.length + array2.length];
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
		return joinedArray;
	}

	/**
	 * <p>
	 * Adds all the elements of the given arrays into a new array.
	 * </p>
	 * <p>
	 * The new array contains all of the element of <code>array1</code> followed
	 * by all of the elements <code>array2</code>. When an array is returned, it
	 * is always a new array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * </pre>
	 * 
	 * @param array1
	 *            the first array whose elements are added to the new array.
	 * @param array2
	 *            the second array whose elements are added to the new array.
	 * @return The new double[] array.
	 * @since 2.1
	 */
	public static double[] addAll(final double[] array1, final double... array2) {
		if (array1 == null) {
			return array2.clone();
		} else if (array2 == null) {
			return array1.clone();
		}
		final double[] joinedArray = new double[array1.length + array2.length];
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
		return joinedArray;
	}

	/**
	 * <p>
	 * Adds all the elements of the given arrays into a new array.
	 * </p>
	 * <p>
	 * The new array contains all of the element of <code>array1</code> followed
	 * by all of the elements <code>array2</code>. When an array is returned, it
	 * is always a new array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * </pre>
	 * 
	 * @param array1
	 *            the first array whose elements are added to the new array.
	 * @param array2
	 *            the second array whose elements are added to the new array.
	 * @return The new float[] array.
	 * @since 2.1
	 */
	public static float[] addAll(final float[] array1, final float... array2) {
		if (array1 == null) {
			return array2.clone();
		} else if (array2 == null) {
			return array1.clone();
		}
		final float[] joinedArray = new float[array1.length + array2.length];
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
		return joinedArray;
	}

	/**
	 * <p>
	 * Adds all the elements of the given arrays into a new array.
	 * </p>
	 * <p>
	 * The new array contains all of the element of <code>array1</code> followed
	 * by all of the elements <code>array2</code>. When an array is returned, it
	 * is always a new array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * </pre>
	 * 
	 * @param array1
	 *            the first array whose elements are added to the new array.
	 * @param array2
	 *            the second array whose elements are added to the new array.
	 * @return The new int[] array.
	 * @since 2.1
	 */
	public static int[] addAll(final int[] array1, final int... array2) {
		if (array1 == null) {
			return array2.clone();
		} else if (array2 == null) {
			return array1.clone();
		}
		final int[] joinedArray = new int[array1.length + array2.length];
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
		return joinedArray;
	}

	/**
	 * <p>
	 * Adds all the elements of the given arrays into a new array.
	 * </p>
	 * <p>
	 * The new array contains all of the element of <code>array1</code> followed
	 * by all of the elements <code>array2</code>. When an array is returned, it
	 * is always a new array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * </pre>
	 * 
	 * @param array1
	 *            the first array whose elements are added to the new array.
	 * @param array2
	 *            the second array whose elements are added to the new array.
	 * @return The new long[] array.
	 * @since 2.1
	 */
	public static long[] addAll(final long[] array1, final long... array2) {
		if (array1 == null) {
			return array2.clone();
		} else if (array2 == null) {
			return array1.clone();
		}
		final long[] joinedArray = new long[array1.length + array2.length];
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
		return joinedArray;
	}

	/**
	 * <p>
	 * Adds all the elements of the given arrays into a new array.
	 * </p>
	 * <p>
	 * The new array contains all of the element of <code>array1</code> followed
	 * by all of the elements <code>array2</code>. When an array is returned, it
	 * is always a new array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * </pre>
	 * 
	 * @param array1
	 *            the first array whose elements are added to the new array.
	 * @param array2
	 *            the second array whose elements are added to the new array.
	 * @return The new short[] array.
	 * @since 2.1
	 */
	public static short[] addAll(final short[] array1, final short... array2) {
		if (array1 == null) {
			return array2.clone();
		} else if (array2 == null) {
			return array1.clone();
		}
		final short[] joinedArray = new short[array1.length + array2.length];
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
		return joinedArray;
	}

	/**
	 * <p>
	 * Adds all the elements of the given arrays into a new array.
	 * </p>
	 * <p>
	 * The new array contains all of the element of <code>array1</code> followed
	 * by all of the elements <code>array2</code>. When an array is returned, it
	 * is always a new array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.addAll(null, null)     = null
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * ArrayUtils.addAll([null], [null]) = [null, null]
	 * ArrayUtils.addAll(["a", "b", "c"], ["1", "2", "3"]) = ["a", "b", "c", "1", "2", "3"]
	 * </pre>
	 * 
	 * @param array1
	 *            the first array whose elements are added to the new array, may
	 *            be <code>null</code>
	 * @param array2
	 *            the second array whose elements are added to the new array,
	 *            may be <code>null</code>
	 * @return The new array, <code>null</code> if both arrays are
	 *         <code>null</code>. The type of the new array is the type of the
	 *         first array, unless the first array is null, in which case the
	 *         type is the same as the second array.
	 * @throws IllegalArgumentException
	 *             if the array types are incompatible
	 * @since 2.1
	 */
	public static <T> T[] addAll(final T[] array1, final T... array2) {
		if (array1 == null) {
			return array2.clone();
		} else if (array2 == null) {
			return array1.clone();
		}
		final Class<?> type1 = array1.getClass().getComponentType();
		@SuppressWarnings("unchecked")
		final// OK, because array is of type T
		T[] joinedArray = (T[]) Array.newInstance(type1, array1.length
				+ array2.length);
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		try {
			System.arraycopy(array2, 0, joinedArray, array1.length,
					array2.length);
		} catch (final ArrayStoreException ase) {
			// Check if problem was due to incompatible types
			/*
			 * We do this here, rather than before the copy because: - it would
			 * be a wasted check most of the time - safer, in case check turns
			 * out to be too strict
			 */
			final Class<?> type2 = array2.getClass().getComponentType();
			if (!type1.isAssignableFrom(type2)) {
				throw new IllegalArgumentException("Cannot store "
						+ type2.getName() + " in an array of "
						+ type1.getName(), ase);
			}
			throw ase; // No, so rethrow original
		}
		return joinedArray;
	}

	/**
	 * <p>
	 * Checks if the value is in the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * The method returns <code>false</code> if a <code>null</code> array is
	 * passed in.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through
	 * @param valueToFind
	 *            the value to find
	 * @return <code>true</code> if the array contains the object
	 */
	public static boolean contains(final boolean[] array,
			final boolean valueToFind) {
		return indexOf(array, valueToFind) != INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Checks if the value is in the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * The method returns <code>false</code> if a <code>null</code> array is
	 * passed in.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through
	 * @param valueToFind
	 *            the value to find
	 * @return <code>true</code> if the array contains the object
	 */
	public static boolean contains(final byte[] array, final byte valueToFind) {
		return indexOf(array, valueToFind) != INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Checks if the value is in the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * The method returns <code>false</code> if a <code>null</code> array is
	 * passed in.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through
	 * @param valueToFind
	 *            the value to find
	 * @return <code>true</code> if the array contains the object
	 * @since 2.1
	 */
	public static boolean contains(final char[] array, final char valueToFind) {
		return indexOf(array, valueToFind) != INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Checks if the value is in the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * The method returns <code>false</code> if a <code>null</code> array is
	 * passed in.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through
	 * @param valueToFind
	 *            the value to find
	 * @return <code>true</code> if the array contains the object
	 */
	public static boolean contains(final double[] array,
			final double valueToFind) {
		return indexOf(array, valueToFind) != INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Checks if a value falling within the given tolerance is in the given
	 * array. If the array contains a value within the inclusive range defined
	 * by (value - tolerance) to (value + tolerance).
	 * </p>
	 * <p/>
	 * <p>
	 * The method returns <code>false</code> if a <code>null</code> array is
	 * passed in.
	 * </p>
	 * 
	 * @param array
	 *            the array to search
	 * @param valueToFind
	 *            the value to find
	 * @param tolerance
	 *            the array contains the tolerance of the search
	 * @return true if value falling within tolerance is in array
	 */
	public static boolean contains(final double[] array,
			final double valueToFind, final double tolerance) {
		return indexOf(array, valueToFind, 0, tolerance) != INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Checks if the value is in the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * The method returns <code>false</code> if a <code>null</code> array is
	 * passed in.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through
	 * @param valueToFind
	 *            the value to find
	 * @return <code>true</code> if the array contains the object
	 */
	public static boolean contains(final float[] array, final float valueToFind) {
		return indexOf(array, valueToFind) != INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Checks if the value is in the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * The method returns <code>false</code> if a <code>null</code> array is
	 * passed in.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through
	 * @param valueToFind
	 *            the value to find
	 * @return <code>true</code> if the array contains the object
	 */
	public static boolean contains(final int[] array, final int valueToFind) {
		return indexOf(array, valueToFind) != INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Checks if the value is in the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * The method returns <code>false</code> if a <code>null</code> array is
	 * passed in.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through
	 * @param valueToFind
	 *            the value to find
	 * @return <code>true</code> if the array contains the object
	 */
	public static boolean contains(final long[] array, final long valueToFind) {
		return indexOf(array, valueToFind) != INDEX_NOT_FOUND;
	}

	/**
	 * Checks that value is present as at least one of the elements of the
	 * array.
	 * 
	 * @param array
	 *            the array to check in
	 * @param value
	 *            the value to check for
	 * @return true if the value is present in the array
	 */
	public static <T> boolean contains(final T[] array, final T value) {
		for (final T element : array) {
			if (element == null) {
				if (value == null)
					return true;
			} else {
				if (value != null && element.equals(value))
					return true;
			}
		}
		return false;
	}

	/**
	 * <p>
	 * Checks if the value is in the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * The method returns <code>false</code> if a <code>null</code> array is
	 * passed in.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through
	 * @param valueToFind
	 *            the value to find
	 * @return <code>true</code> if the array contains the object
	 */
	public static boolean contains(final short[] array, final short valueToFind) {
		return indexOf(array, valueToFind) != INDEX_NOT_FOUND;
	}

	/**
	 * Returns a copy of the given array of size 1 greater than the argument.
	 * The last value of the array is left to the default value.
	 * 
	 * @param array
	 *            The array to copy, must not be <code>null</code>.
	 * @param newArrayComponentType
	 *            If <code>array</code> is <code>null</code>, create a size 1
	 *            array of this type.
	 * @return A new copy of the array of size 1 greater than the input.
	 */
	private static Object copyArrayGrow1(final Object array,
			final Class<?> newArrayComponentType) {
		if (array != null) {
			final int arrayLength = Array.getLength(array);
			final Object newArray = Array.newInstance(array.getClass()
					.getComponentType(), arrayLength + 1);
			System.arraycopy(array, 0, newArray, 0, arrayLength);
			return newArray;
		}
		return Array.newInstance(newArrayComponentType, 1);
	}

	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Returns the length of the specified array. This method can deal with
	 * <code>Object</code> arrays and with primitive arrays.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, <code>0</code> is returned.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.getLength(null)            = 0
	 * ArrayUtils.getLength([])              = 0
	 * ArrayUtils.getLength([null])          = 1
	 * ArrayUtils.getLength([true, false])   = 2
	 * ArrayUtils.getLength([1, 2, 3])       = 3
	 * ArrayUtils.getLength(["a", "b", "c"]) = 3
	 * </pre>
	 * 
	 * @param array
	 *            the array to retrieve the length from, may be null
	 * @return The length of the array, or <code>0</code> if the array is
	 *         <code>null</code>
	 * @throws IllegalArgumentException
	 *             if the object arguement is not an array.
	 * @since 2.1
	 */
	public static int getLength(final Object array) {
		if (array == null) {
			return 0;
		}
		return Array.getLength(array);
	}

	// boolean IndexOf
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given value in the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final boolean[] array, final boolean valueToFind) {
		return indexOf(array, valueToFind, 0);
	}

	/**
	 * <p>
	 * Finds the index of the given value in the array starting at the given
	 * index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the index to start searching at
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final boolean[] array, final boolean valueToFind,
			int startIndex) {
		if (ArrayUtils.isEmpty(array)) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		for (int i = startIndex; i < array.length; i++) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	// byte IndexOf
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given value in the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final byte[] array, final byte valueToFind) {
		return indexOf(array, valueToFind, 0);
	}

	/**
	 * <p>
	 * Finds the index of the given value in the array starting at the given
	 * index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the index to start searching at
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final byte[] array, final byte valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		for (int i = startIndex; i < array.length; i++) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	// char IndexOf
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given value in the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 * @since 2.1
	 */
	public static int indexOf(final char[] array, final char valueToFind) {
		return indexOf(array, valueToFind, 0);
	}

	/**
	 * <p>
	 * Finds the index of the given value in the array starting at the given
	 * index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the index to start searching at
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 * @since 2.1
	 */
	public static int indexOf(final char[] array, final char valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		for (int i = startIndex; i < array.length; i++) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	// double IndexOf
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given value in the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final double[] array, final double valueToFind) {
		return indexOf(array, valueToFind, 0);
	}

	/**
	 * <p>
	 * Finds the index of the given value within a given tolerance in the array.
	 * This method will return the index of the first value which falls between
	 * the region defined by valueToFind - tolerance and valueToFind +
	 * tolerance.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param tolerance
	 *            tolerance of the search
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final double[] array, final double valueToFind,
			final double tolerance) {
		return indexOf(array, valueToFind, 0, tolerance);
	}

	/**
	 * <p>
	 * Finds the index of the given value in the array starting at the given
	 * index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the index to start searching at
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final double[] array, final double valueToFind,
			int startIndex) {
		if (ArrayUtils.isEmpty(array)) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		for (int i = startIndex; i < array.length; i++) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Finds the index of the given value in the array starting at the given
	 * index. This method will return the index of the first value which falls
	 * between the region defined by valueToFind - tolerance and valueToFind +
	 * tolerance.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the index to start searching at
	 * @param tolerance
	 *            tolerance of the search
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final double[] array, final double valueToFind,
			int startIndex, final double tolerance) {
		if (ArrayUtils.isEmpty(array)) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		final double min = valueToFind - tolerance;
		final double max = valueToFind + tolerance;
		for (int i = startIndex; i < array.length; i++) {
			if (array[i] >= min && array[i] <= max) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	// float IndexOf
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given value in the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final float[] array, final float valueToFind) {
		return indexOf(array, valueToFind, 0);
	}

	/**
	 * <p>
	 * Finds the index of the given value in the array starting at the given
	 * index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the index to start searching at
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final float[] array, final float valueToFind,
			int startIndex) {
		if (ArrayUtils.isEmpty(array)) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		for (int i = startIndex; i < array.length; i++) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	// int IndexOf
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given value in the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final int[] array, final int valueToFind) {
		return indexOf(array, valueToFind, 0);
	}

	// IndexOf search
	// ----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given value in the array starting at the given
	 * index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the index to start searching at
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final int[] array, final int valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		for (int i = startIndex; i < array.length; i++) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	// long IndexOf
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given value in the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final long[] array, final long valueToFind) {
		return indexOf(array, valueToFind, 0);
	}

	/**
	 * <p>
	 * Finds the index of the given value in the array starting at the given
	 * index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the index to start searching at
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final long[] array, final long valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		for (int i = startIndex; i < array.length; i++) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	// Object IndexOf
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given object in the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param objectToFind
	 *            the object to find, may be <code>null</code>
	 * @return the index of the object within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int indexOf(final Object[] array, final Object objectToFind) {
		return indexOf(array, objectToFind, 0);
	}

	/**
	 * <p>
	 * Finds the index of the given object in the array starting at the given
	 * index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param objectToFind
	 *            the object to find, may be <code>null</code>
	 * @param startIndex
	 *            the index to start searching at
	 * @return the index of the object within the array starting at the index,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int indexOf(final Object[] array, final Object objectToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		if (objectToFind == null) {
			for (int i = startIndex; i < array.length; i++) {
				if (array[i] == null) {
					return i;
				}
			}
		} else if (array.getClass().getComponentType().isInstance(objectToFind)) {
			for (int i = startIndex; i < array.length; i++) {
				if (objectToFind.equals(array[i])) {
					return i;
				}
			}
		}
		return INDEX_NOT_FOUND;
	}

	// short IndexOf
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the index of the given value in the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final short[] array, final short valueToFind) {
		return indexOf(array, valueToFind, 0);
	}

	/**
	 * <p>
	 * Finds the index of the given value in the array starting at the given
	 * index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex is treated as zero. A startIndex larger than the
	 * array length will return {@link #INDEX_NOT_FOUND} (<code>-1</code>).
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the index to start searching at
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int indexOf(final short[] array, final short valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		for (int i = startIndex; i < array.length; i++) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Checks if an array of primitive booleans is empty or <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is empty or <code>null</code>
	 * @since 2.1
	 */
	public static boolean isEmpty(final boolean[] array) {
		if (array == null || array.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * Checks if an array of primitive bytes is empty or <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is empty or <code>null</code>
	 * @since 2.1
	 */
	public static boolean isEmpty(final byte[] array) {
		if (array == null || array.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * Checks if an array of primitive chars is empty or <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is empty or <code>null</code>
	 * @since 2.1
	 */
	public static boolean isEmpty(final char[] array) {
		if (array == null || array.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * Checks if an array of primitive doubles is empty or <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is empty or <code>null</code>
	 * @since 2.1
	 */
	public static boolean isEmpty(final double[] array) {
		if (array == null || array.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * Checks if an array of primitive floats is empty or <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is empty or <code>null</code>
	 * @since 2.1
	 */
	public static boolean isEmpty(final float[] array) {
		if (array == null || array.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * Checks if an array of primitive ints is empty or <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is empty or <code>null</code>
	 * @since 2.1
	 */
	public static boolean isEmpty(final int[] array) {
		if (array == null || array.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * Checks if an array of primitive longs is empty or <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is empty or <code>null</code>
	 * @since 2.1
	 */
	public static boolean isEmpty(final long[] array) {
		if (array == null || array.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * Checks if an array of primitive shorts is empty or <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is empty or <code>null</code>
	 * @since 2.1
	 */
	public static boolean isEmpty(final short[] array) {
		if (array == null || array.length == 0) {
			return true;
		}
		return false;
	}

	// ----------------------------------------------------------------------

	/**
	 * <p>
	 * Checks if an array of Objects is empty or <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is empty or <code>null</code>
	 * @since 2.1
	 */
	public static <T> boolean isEmpty(final T[] array) {
		if (array == null || array.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * Compares two arrays, using equals(), handling multi-dimensional arrays
	 * correctly.
	 * </p>
	 * <p/>
	 * <p>
	 * Multi-dimensional primitive arrays are also handled correctly by this
	 * method.
	 * </p>
	 * 
	 * @param array1
	 *            the left hand array to compare, may be <code>null</code>
	 * @param array2
	 *            the right hand array to compare, may be <code>null</code>
	 * @return <code>true</code> if the arrays are equal
	 */
	public static boolean isEquals(final Object array1, final Object array2) {
		return new EqualsBuilder().append(array1, array2).isEquals();
	}

	/**
	 * <p>
	 * Checks if an array of primitive booleans is not empty or not
	 * <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is not empty or not
	 *         <code>null</code>
	 * @since 2.5
	 */
	public static boolean isNotEmpty(final boolean[] array) {
		return (array != null && array.length != 0);
	}

	/**
	 * <p>
	 * Checks if an array of primitive bytes is not empty or not
	 * <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is not empty or not
	 *         <code>null</code>
	 * @since 2.5
	 */
	public static boolean isNotEmpty(final byte[] array) {
		return (array != null && array.length != 0);
	}

	/**
	 * <p>
	 * Checks if an array of primitive chars is not empty or not
	 * <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is not empty or not
	 *         <code>null</code>
	 * @since 2.5
	 */
	public static boolean isNotEmpty(final char[] array) {
		return (array != null && array.length != 0);
	}

	/**
	 * <p>
	 * Checks if an array of primitive doubles is not empty or not
	 * <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is not empty or not
	 *         <code>null</code>
	 * @since 2.5
	 */
	public static boolean isNotEmpty(final double[] array) {
		return (array != null && array.length != 0);
	}

	/**
	 * <p>
	 * Checks if an array of primitive floats is not empty or not
	 * <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is not empty or not
	 *         <code>null</code>
	 * @since 2.5
	 */
	public static boolean isNotEmpty(final float[] array) {
		return (array != null && array.length != 0);
	}

	/**
	 * <p>
	 * Checks if an array of primitive ints is not empty or not
	 * <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is not empty or not
	 *         <code>null</code>
	 * @since 2.5
	 */
	public static boolean isNotEmpty(final int[] array) {
		return (array != null && array.length != 0);
	}

	/**
	 * <p>
	 * Checks if an array of primitive longs is not empty or not
	 * <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is not empty or not
	 *         <code>null</code>
	 * @since 2.5
	 */
	public static boolean isNotEmpty(final long[] array) {
		return (array != null && array.length != 0);
	}

	/**
	 * <p>
	 * Checks if an array of primitive shorts is not empty or not
	 * <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is not empty or not
	 *         <code>null</code>
	 * @since 2.5
	 */
	public static boolean isNotEmpty(final short[] array) {
		return (array != null && array.length != 0);
	}

	// ----------------------------------------------------------------------

	/**
	 * <p>
	 * Checks if an array of Objects is not empty or not <code>null</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to test
	 * @return <code>true</code> if the array is not empty or not
	 *         <code>null</code>
	 * @since 2.5
	 */
	public static <T> boolean isNotEmpty(final T[] array) {
		return (array != null && array.length != 0);
	}

	/**
	 * <p>
	 * Checks whether two arrays are the same length, treating <code>null</code>
	 * arrays as length <code>0</code>.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, may be <code>null</code>
	 * @param array2
	 *            the second array, may be <code>null</code>
	 * @return <code>true</code> if length of arrays matches, treating
	 *         <code>null</code> as an empty array
	 */
	public static boolean isSameLength(final boolean[] array1,
			final boolean[] array2) {
		if ((array1 == null && array2 != null && array2.length > 0)
				|| (array2 == null && array1 != null && array1.length > 0)
				|| (array1 != null && array2 != null && array1.length != array2.length)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * Checks whether two arrays are the same length, treating <code>null</code>
	 * arrays as length <code>0</code>.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, may be <code>null</code>
	 * @param array2
	 *            the second array, may be <code>null</code>
	 * @return <code>true</code> if length of arrays matches, treating
	 *         <code>null</code> as an empty array
	 */
	public static boolean isSameLength(final byte[] array1, final byte[] array2) {
		if ((array1 == null && array2 != null && array2.length > 0)
				|| (array2 == null && array1 != null && array1.length > 0)
				|| (array1 != null && array2 != null && array1.length != array2.length)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * Checks whether two arrays are the same length, treating <code>null</code>
	 * arrays as length <code>0</code>.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, may be <code>null</code>
	 * @param array2
	 *            the second array, may be <code>null</code>
	 * @return <code>true</code> if length of arrays matches, treating
	 *         <code>null</code> as an empty array
	 */
	public static boolean isSameLength(final char[] array1, final char[] array2) {
		if ((array1 == null && array2 != null && array2.length > 0)
				|| (array2 == null && array1 != null && array1.length > 0)
				|| (array1 != null && array2 != null && array1.length != array2.length)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * Checks whether two arrays are the same length, treating <code>null</code>
	 * arrays as length <code>0</code>.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, may be <code>null</code>
	 * @param array2
	 *            the second array, may be <code>null</code>
	 * @return <code>true</code> if length of arrays matches, treating
	 *         <code>null</code> as an empty array
	 */
	public static boolean isSameLength(final double[] array1,
			final double[] array2) {
		if ((array1 == null && array2 != null && array2.length > 0)
				|| (array2 == null && array1 != null && array1.length > 0)
				|| (array1 != null && array2 != null && array1.length != array2.length)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * Checks whether two arrays are the same length, treating <code>null</code>
	 * arrays as length <code>0</code>.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, may be <code>null</code>
	 * @param array2
	 *            the second array, may be <code>null</code>
	 * @return <code>true</code> if length of arrays matches, treating
	 *         <code>null</code> as an empty array
	 */
	public static boolean isSameLength(final float[] array1,
			final float[] array2) {
		if ((array1 == null && array2 != null && array2.length > 0)
				|| (array2 == null && array1 != null && array1.length > 0)
				|| (array1 != null && array2 != null && array1.length != array2.length)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * Checks whether two arrays are the same length, treating <code>null</code>
	 * arrays as length <code>0</code>.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, may be <code>null</code>
	 * @param array2
	 *            the second array, may be <code>null</code>
	 * @return <code>true</code> if length of arrays matches, treating
	 *         <code>null</code> as an empty array
	 */
	public static boolean isSameLength(final int[] array1, final int[] array2) {
		if ((array1 == null && array2 != null && array2.length > 0)
				|| (array2 == null && array1 != null && array1.length > 0)
				|| (array1 != null && array2 != null && array1.length != array2.length)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * Checks whether two arrays are the same length, treating <code>null</code>
	 * arrays as length <code>0</code>.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, may be <code>null</code>
	 * @param array2
	 *            the second array, may be <code>null</code>
	 * @return <code>true</code> if length of arrays matches, treating
	 *         <code>null</code> as an empty array
	 */
	public static boolean isSameLength(final long[] array1, final long[] array2) {
		if ((array1 == null && array2 != null && array2.length > 0)
				|| (array2 == null && array1 != null && array1.length > 0)
				|| (array1 != null && array2 != null && array1.length != array2.length)) {
			return false;
		}
		return true;
	}

	// Is same length
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Checks whether two arrays are the same length, treating <code>null</code>
	 * arrays as length <code>0</code>.
	 * <p/>
	 * <p>
	 * Any multi-dimensional aspects of the arrays are ignored.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, may be <code>null</code>
	 * @param array2
	 *            the second array, may be <code>null</code>
	 * @return <code>true</code> if length of arrays matches, treating
	 *         <code>null</code> as an empty array
	 */
	public static boolean isSameLength(final Object[] array1,
			final Object[] array2) {
		if ((array1 == null && array2 != null && array2.length > 0)
				|| (array2 == null && array1 != null && array1.length > 0)
				|| (array1 != null && array2 != null && array1.length != array2.length)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * Checks whether two arrays are the same length, treating <code>null</code>
	 * arrays as length <code>0</code>.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, may be <code>null</code>
	 * @param array2
	 *            the second array, may be <code>null</code>
	 * @return <code>true</code> if length of arrays matches, treating
	 *         <code>null</code> as an empty array
	 */
	public static boolean isSameLength(final short[] array1,
			final short[] array2) {
		if ((array1 == null && array2 != null && array2.length > 0)
				|| (array2 == null && array1 != null && array1.length > 0)
				|| (array1 != null && array2 != null && array1.length != array2.length)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * Checks whether two arrays are the same type taking into account
	 * multi-dimensional arrays.
	 * </p>
	 * 
	 * @param array1
	 *            the first array, must not be <code>null</code>
	 * @param array2
	 *            the second array, must not be <code>null</code>
	 * @return <code>true</code> if type of arrays matches
	 * @throws IllegalArgumentException
	 *             if either array is <code>null</code>
	 */
	public static boolean isSameType(final Object array1, final Object array2) {
		if (array1 == null || array2 == null) {
			throw new IllegalArgumentException("The Array must not be null");
		}
		return array1.getClass().getName().equals(array2.getClass().getName());
	}

	/**
	 * <p>
	 * Finds the last index of the given value within the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) if
	 * <code>null</code> array input.
	 * </p>
	 * 
	 * @param array
	 *            the array to travers backwords looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the object to find
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final boolean[] array,
			final boolean valueToFind) {
		return lastIndexOf(array, valueToFind, Integer.MAX_VALUE);
	}

	/**
	 * <p>
	 * Finds the last index of the given value in the array starting at the
	 * given index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final boolean[] array,
			final boolean valueToFind, int startIndex) {
		if (ArrayUtils.isEmpty(array)) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		for (int i = startIndex; i >= 0; i--) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Finds the last index of the given value within the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to travers backwords looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the object to find
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final byte[] array, final byte valueToFind) {
		return lastIndexOf(array, valueToFind, Integer.MAX_VALUE);
	}

	/**
	 * <p>
	 * Finds the last index of the given value in the array starting at the
	 * given index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final byte[] array, final byte valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		for (int i = startIndex; i >= 0; i--) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Finds the last index of the given value within the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to travers backwords looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the object to find
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 * @since 2.1
	 */
	public static int lastIndexOf(final char[] array, final char valueToFind) {
		return lastIndexOf(array, valueToFind, Integer.MAX_VALUE);
	}

	/**
	 * <p>
	 * Finds the last index of the given value in the array starting at the
	 * given index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 * @since 2.1
	 */
	public static int lastIndexOf(final char[] array, final char valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		for (int i = startIndex; i >= 0; i--) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Finds the last index of the given value within the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to travers backwords looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the object to find
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final double[] array, final double valueToFind) {
		return lastIndexOf(array, valueToFind, Integer.MAX_VALUE);
	}

	/**
	 * <p>
	 * Finds the last index of the given value within a given tolerance in the
	 * array. This method will return the index of the last value which falls
	 * between the region defined by valueToFind - tolerance and valueToFind +
	 * tolerance.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to search through for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param tolerance
	 *            tolerance of the search
	 * @return the index of the value within the array, {@link #INDEX_NOT_FOUND}
	 *         (<code>-1</code>) if not found or <code>null</code> array input
	 */
	public static int lastIndexOf(final double[] array,
			final double valueToFind, final double tolerance) {
		return lastIndexOf(array, valueToFind, Integer.MAX_VALUE, tolerance);
	}

	/**
	 * <p>
	 * Finds the last index of the given value in the array starting at the
	 * given index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final double[] array,
			final double valueToFind, int startIndex) {
		if (ArrayUtils.isEmpty(array)) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		for (int i = startIndex; i >= 0; i--) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Finds the last index of the given value in the array starting at the
	 * given index. This method will return the index of the last value which
	 * falls between the region defined by valueToFind - tolerance and
	 * valueToFind + tolerance.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @param tolerance
	 *            search for value within plus/minus this amount
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final double[] array,
			final double valueToFind, int startIndex, final double tolerance) {
		if (ArrayUtils.isEmpty(array)) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		final double min = valueToFind - tolerance;
		final double max = valueToFind + tolerance;
		for (int i = startIndex; i >= 0; i--) {
			if (array[i] >= min && array[i] <= max) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Finds the last index of the given value within the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to travers backwords looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the object to find
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final float[] array, final float valueToFind) {
		return lastIndexOf(array, valueToFind, Integer.MAX_VALUE);
	}

	/**
	 * <p>
	 * Finds the last index of the given value in the array starting at the
	 * given index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final float[] array, final float valueToFind,
			int startIndex) {
		if (ArrayUtils.isEmpty(array)) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		for (int i = startIndex; i >= 0; i--) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Finds the last index of the given value in the array starting at the
	 * given index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final int[] array, final int valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		for (int i = startIndex; i >= 0; i--) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	// Primitive/Object array converters
	// ----------------------------------------------------------------------

	/**
	 * <p>
	 * Finds the last index of the given value within the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to travers backwords looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the object to find
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final long[] array, final long valueToFind) {
		return lastIndexOf(array, valueToFind, Integer.MAX_VALUE);
	}

	/**
	 * <p>
	 * Finds the last index of the given value in the array starting at the
	 * given index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final long[] array, final long valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		for (int i = startIndex; i >= 0; i--) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Finds the last index of the given object within the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to travers backwords looking for the object, may be
	 *            <code>null</code>
	 * @param objectToFind
	 *            the object to find, may be <code>null</code>
	 * @return the last index of the object within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final Object[] array,
			final Object objectToFind) {
		return lastIndexOf(array, objectToFind, Integer.MAX_VALUE);
	}

	/**
	 * <p>
	 * Finds the last index of the given object in the array starting at the
	 * given index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param objectToFind
	 *            the object to find, may be <code>null</code>
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @return the last index of the object within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final Object[] array,
			final Object objectToFind, int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		if (objectToFind == null) {
			for (int i = startIndex; i >= 0; i--) {
				if (array[i] == null) {
					return i;
				}
			}
		} else if (array.getClass().getComponentType().isInstance(objectToFind)) {
			for (int i = startIndex; i >= 0; i--) {
				if (objectToFind.equals(array[i])) {
					return i;
				}
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Finds the last index of the given value within the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to travers backwords looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the object to find
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final short[] array, final short valueToFind) {
		return lastIndexOf(array, valueToFind, Integer.MAX_VALUE);
	}

	/**
	 * <p>
	 * Finds the last index of the given value in the array starting at the
	 * given index.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns {@link #INDEX_NOT_FOUND} (<code>-1</code>) for a
	 * <code>null</code> input array.
	 * </p>
	 * <p/>
	 * <p>
	 * A negative startIndex will return {@link #INDEX_NOT_FOUND} (
	 * <code>-1</code>). A startIndex larger than the array length will search
	 * from the end of the array.
	 * </p>
	 * 
	 * @param array
	 *            the array to traverse for looking for the object, may be
	 *            <code>null</code>
	 * @param valueToFind
	 *            the value to find
	 * @param startIndex
	 *            the start index to travers backwards from
	 * @return the last index of the value within the array,
	 *         {@link #INDEX_NOT_FOUND} (<code>-1</code>) if not found or
	 *         <code>null</code> array input
	 */
	public static int lastIndexOf(final short[] array, final short valueToFind,
			int startIndex) {
		if (array == null) {
			return INDEX_NOT_FOUND;
		}
		if (startIndex < 0) {
			return INDEX_NOT_FOUND;
		} else if (startIndex >= array.length) {
			startIndex = array.length - 1;
		}
		for (int i = startIndex; i >= 0; i--) {
			if (valueToFind == array[i]) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.remove([true], 0)              = []
	 * ArrayUtils.remove([true, false], 0)       = [false]
	 * ArrayUtils.remove([true, false], 1)       = [true]
	 * ArrayUtils.remove([true, true, false], 1) = [true, false]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	public static boolean[] remove(final boolean[] array, final int index) {
		return (boolean[]) remove((Object) array, index);
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.remove([1], 0)          = []
	 * ArrayUtils.remove([1, 0], 0)       = [0]
	 * ArrayUtils.remove([1, 0], 1)       = [1]
	 * ArrayUtils.remove([1, 0, 1], 1)    = [1, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	public static byte[] remove(final byte[] array, final int index) {
		return (byte[]) remove((Object) array, index);
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.remove(['a'], 0)           = []
	 * ArrayUtils.remove(['a', 'b'], 0)      = ['b']
	 * ArrayUtils.remove(['a', 'b'], 1)      = ['a']
	 * ArrayUtils.remove(['a', 'b', 'c'], 1) = ['a', 'c']
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	public static char[] remove(final char[] array, final int index) {
		return (char[]) remove((Object) array, index);
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.remove([1.1], 0)           = []
	 * ArrayUtils.remove([2.5, 6.0], 0)      = [6.0]
	 * ArrayUtils.remove([2.5, 6.0], 1)      = [2.5]
	 * ArrayUtils.remove([2.5, 6.0, 3.8], 1) = [2.5, 3.8]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	public static double[] remove(final double[] array, final int index) {
		return (double[]) remove((Object) array, index);
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.remove([1.1], 0)           = []
	 * ArrayUtils.remove([2.5, 6.0], 0)      = [6.0]
	 * ArrayUtils.remove([2.5, 6.0], 1)      = [2.5]
	 * ArrayUtils.remove([2.5, 6.0, 3.8], 1) = [2.5, 3.8]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	public static float[] remove(final float[] array, final int index) {
		return (float[]) remove((Object) array, index);
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.remove([1], 0)         = []
	 * ArrayUtils.remove([2, 6], 0)      = [6]
	 * ArrayUtils.remove([2, 6], 1)      = [2]
	 * ArrayUtils.remove([2, 6, 3], 1)   = [2, 3]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	public static int[] remove(final int[] array, final int index) {
		return (int[]) remove((Object) array, index);
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.remove([1], 0)         = []
	 * ArrayUtils.remove([2, 6], 0)      = [6]
	 * ArrayUtils.remove([2, 6], 1)      = [2]
	 * ArrayUtils.remove([2, 6, 3], 1)   = [2, 3]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	public static long[] remove(final long[] array, final int index) {
		return (long[]) remove((Object) array, index);
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	private static Object remove(final Object array, final int index) {
		final int length = getLength(array);
		if (index < 0 || index >= length) {
			throw new IndexOutOfBoundsException("Index: " + index
					+ ", Length: " + length);
		}

		final Object result = Array.newInstance(array.getClass()
				.getComponentType(), length - 1);
		System.arraycopy(array, 0, result, 0, index);
		if (index < length - 1) {
			System.arraycopy(array, index + 1, result, index, length - index
					- 1);
		}

		return result;
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.remove([1], 0)         = []
	 * ArrayUtils.remove([2, 6], 0)      = [6]
	 * ArrayUtils.remove([2, 6], 1)      = [2]
	 * ArrayUtils.remove([2, 6, 3], 1)   = [2, 3]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	public static short[] remove(final short[] array, final int index) {
		return (short[]) remove((Object) array, index);
	}

	/**
	 * <p>
	 * Removes the element at the specified position from the specified array.
	 * All subsequent elements are shifted to the left (substracts one from
	 * their indices).
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the element on the specified position. The component type of the
	 * returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * <p>
	 * If the input array is <code>null</code>, an IndexOutOfBoundsException
	 * will be thrown, because in that case no valid index can be specified.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.remove(["a"], 0)           = []
	 * ArrayUtils.remove(["a", "b"], 0)      = ["b"]
	 * ArrayUtils.remove(["a", "b"], 1)      = ["a"]
	 * ArrayUtils.remove(["a", "b", "c"], 1) = ["a", "c"]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may not be
	 *            <code>null</code>
	 * @param index
	 *            the position of the element to be removed
	 * @return A new array containing the existing elements except the element
	 *         at the specified position.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (index < 0 || index >=
	 *             array.length), or if the array is <code>null</code>.
	 * @since 2.1
	 */
	@SuppressWarnings("unchecked")
	// remove() always creates an array of the same type as its input
	public static <T> T[] remove(final T[] array, final int index) {
		return (T[]) remove((Object) array, index);
	}

	/**
	 * <p>
	 * Removes the first occurrence of the specified element from the specified
	 * array. All subsequent elements are shifted to the left (substracts one
	 * from their indices). If the array doesn't contains such an element, no
	 * elements are removed from the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the first occurrence of the specified element. The component type
	 * of the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.removeElement(null, true)                = null
	 * ArrayUtils.removeElement([], true)                  = []
	 * ArrayUtils.removeElement([true], false)             = [true]
	 * ArrayUtils.removeElement([true, false], false)      = [true]
	 * ArrayUtils.removeElement([true, false, true], true) = [false, true]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may be <code>null</code>
	 * @param element
	 *            the element to be removed
	 * @return A new array containing the existing elements except the first
	 *         occurrence of the specified element.
	 * @since 2.1
	 */
	public static boolean[] removeElement(final boolean[] array,
			final boolean element) {
		final int index = indexOf(array, element);
		if (index == INDEX_NOT_FOUND) {
			return array.clone();
		}
		return remove(array, index);
	}

	/**
	 * <p>
	 * Removes the first occurrence of the specified element from the specified
	 * array. All subsequent elements are shifted to the left (substracts one
	 * from their indices). If the array doesn't contains such an element, no
	 * elements are removed from the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the first occurrence of the specified element. The component type
	 * of the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.removeElement(null, 1)        = null
	 * ArrayUtils.removeElement([], 1)          = []
	 * ArrayUtils.removeElement([1], 0)         = [1]
	 * ArrayUtils.removeElement([1, 0], 0)      = [1]
	 * ArrayUtils.removeElement([1, 0, 1], 1)   = [0, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may be <code>null</code>
	 * @param element
	 *            the element to be removed
	 * @return A new array containing the existing elements except the first
	 *         occurrence of the specified element.
	 * @since 2.1
	 */
	public static byte[] removeElement(final byte[] array, final byte element) {
		final int index = indexOf(array, element);
		if (index == INDEX_NOT_FOUND) {
			return array.clone();
		}
		return remove(array, index);
	}

	/**
	 * <p>
	 * Removes the first occurrence of the specified element from the specified
	 * array. All subsequent elements are shifted to the left (substracts one
	 * from their indices). If the array doesn't contains such an element, no
	 * elements are removed from the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the first occurrence of the specified element. The component type
	 * of the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.removeElement(null, 'a')            = null
	 * ArrayUtils.removeElement([], 'a')              = []
	 * ArrayUtils.removeElement(['a'], 'b')           = ['a']
	 * ArrayUtils.removeElement(['a', 'b'], 'a')      = ['b']
	 * ArrayUtils.removeElement(['a', 'b', 'a'], 'a') = ['b', 'a']
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may be <code>null</code>
	 * @param element
	 *            the element to be removed
	 * @return A new array containing the existing elements except the first
	 *         occurrence of the specified element.
	 * @since 2.1
	 */
	public static char[] removeElement(final char[] array, final char element) {
		final int index = indexOf(array, element);
		if (index == INDEX_NOT_FOUND) {
			return array.clone();
		}
		return remove(array, index);
	}

	/**
	 * <p>
	 * Removes the first occurrence of the specified element from the specified
	 * array. All subsequent elements are shifted to the left (substracts one
	 * from their indices). If the array doesn't contains such an element, no
	 * elements are removed from the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the first occurrence of the specified element. The component type
	 * of the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.removeElement(null, 1.1)            = null
	 * ArrayUtils.removeElement([], 1.1)              = []
	 * ArrayUtils.removeElement([1.1], 1.2)           = [1.1]
	 * ArrayUtils.removeElement([1.1, 2.3], 1.1)      = [2.3]
	 * ArrayUtils.removeElement([1.1, 2.3, 1.1], 1.1) = [2.3, 1.1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may be <code>null</code>
	 * @param element
	 *            the element to be removed
	 * @return A new array containing the existing elements except the first
	 *         occurrence of the specified element.
	 * @since 2.1
	 */
	public static double[] removeElement(final double[] array,
			final double element) {
		final int index = indexOf(array, element);
		if (index == INDEX_NOT_FOUND) {
			return array.clone();
		}
		return remove(array, index);
	}

	/**
	 * <p>
	 * Removes the first occurrence of the specified element from the specified
	 * array. All subsequent elements are shifted to the left (substracts one
	 * from their indices). If the array doesn't contains such an element, no
	 * elements are removed from the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the first occurrence of the specified element. The component type
	 * of the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.removeElement(null, 1.1)            = null
	 * ArrayUtils.removeElement([], 1.1)              = []
	 * ArrayUtils.removeElement([1.1], 1.2)           = [1.1]
	 * ArrayUtils.removeElement([1.1, 2.3], 1.1)      = [2.3]
	 * ArrayUtils.removeElement([1.1, 2.3, 1.1], 1.1) = [2.3, 1.1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may be <code>null</code>
	 * @param element
	 *            the element to be removed
	 * @return A new array containing the existing elements except the first
	 *         occurrence of the specified element.
	 * @since 2.1
	 */
	public static float[] removeElement(final float[] array, final float element) {
		final int index = indexOf(array, element);
		if (index == INDEX_NOT_FOUND) {
			return array.clone();
		}
		return remove(array, index);
	}

	/**
	 * <p>
	 * Removes the first occurrence of the specified element from the specified
	 * array. All subsequent elements are shifted to the left (substracts one
	 * from their indices). If the array doesn't contains such an element, no
	 * elements are removed from the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the first occurrence of the specified element. The component type
	 * of the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.removeElement(null, 1)      = null
	 * ArrayUtils.removeElement([], 1)        = []
	 * ArrayUtils.removeElement([1], 2)       = [1]
	 * ArrayUtils.removeElement([1, 3], 1)    = [3]
	 * ArrayUtils.removeElement([1, 3, 1], 1) = [3, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may be <code>null</code>
	 * @param element
	 *            the element to be removed
	 * @return A new array containing the existing elements except the first
	 *         occurrence of the specified element.
	 * @since 2.1
	 */
	public static int[] removeElement(final int[] array, final int element) {
		final int index = indexOf(array, element);
		if (index == INDEX_NOT_FOUND) {
			return array.clone();
		}
		return remove(array, index);
	}

	/**
	 * <p>
	 * Removes the first occurrence of the specified element from the specified
	 * array. All subsequent elements are shifted to the left (substracts one
	 * from their indices). If the array doesn't contains such an element, no
	 * elements are removed from the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the first occurrence of the specified element. The component type
	 * of the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.removeElement(null, 1)      = null
	 * ArrayUtils.removeElement([], 1)        = []
	 * ArrayUtils.removeElement([1], 2)       = [1]
	 * ArrayUtils.removeElement([1, 3], 1)    = [3]
	 * ArrayUtils.removeElement([1, 3, 1], 1) = [3, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may be <code>null</code>
	 * @param element
	 *            the element to be removed
	 * @return A new array containing the existing elements except the first
	 *         occurrence of the specified element.
	 * @since 2.1
	 */
	public static long[] removeElement(final long[] array, final long element) {
		final int index = indexOf(array, element);
		if (index == INDEX_NOT_FOUND) {
			return array.clone();
		}
		return remove(array, index);
	}

	/**
	 * <p>
	 * Removes the first occurrence of the specified element from the specified
	 * array. All subsequent elements are shifted to the left (substracts one
	 * from their indices). If the array doesn't contains such an element, no
	 * elements are removed from the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the first occurrence of the specified element. The component type
	 * of the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.removeElement(null, 1)      = null
	 * ArrayUtils.removeElement([], 1)        = []
	 * ArrayUtils.removeElement([1], 2)       = [1]
	 * ArrayUtils.removeElement([1, 3], 1)    = [3]
	 * ArrayUtils.removeElement([1, 3, 1], 1) = [3, 1]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may be <code>null</code>
	 * @param element
	 *            the element to be removed
	 * @return A new array containing the existing elements except the first
	 *         occurrence of the specified element.
	 * @since 2.1
	 */
	public static short[] removeElement(final short[] array, final short element) {
		final int index = indexOf(array, element);
		if (index == INDEX_NOT_FOUND) {
			return array.clone();
		}
		return remove(array, index);
	}

	/**
	 * <p>
	 * Removes the first occurrence of the specified element from the specified
	 * array. All subsequent elements are shifted to the left (substracts one
	 * from their indices). If the array doesn't contains such an element, no
	 * elements are removed from the array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns a new array with the same elements of the input array
	 * except the first occurrence of the specified element. The component type
	 * of the returned array is always the same as that of the input array.
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * ArrayUtils.removeElement(null, "a")            = null
	 * ArrayUtils.removeElement([], "a")              = []
	 * ArrayUtils.removeElement(["a"], "b")           = ["a"]
	 * ArrayUtils.removeElement(["a", "b"], "a")      = ["b"]
	 * ArrayUtils.removeElement(["a", "b", "a"], "a") = ["b", "a"]
	 * </pre>
	 * 
	 * @param array
	 *            the array to remove the element from, may be <code>null</code>
	 * @param element
	 *            the element to be removed
	 * @return A new array containing the existing elements except the first
	 *         occurrence of the specified element.
	 * @since 2.1
	 */
	public static <T> T[] removeElement(final T[] array, final Object element) {
		final int index = indexOf(array, element);
		if (index == INDEX_NOT_FOUND) {
			return array.clone();
		}
		return remove(array, index);
	}

	/**
	 * <p>
	 * Reverses the order of the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method does nothing for a <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to reverse, may be <code>null</code>
	 */
	public static void reverse(final boolean[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		boolean tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * <p>
	 * Reverses the order of the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method does nothing for a <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to reverse, may be <code>null</code>
	 */
	public static void reverse(final byte[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		byte tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * <p>
	 * Reverses the order of the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method does nothing for a <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to reverse, may be <code>null</code>
	 */
	public static void reverse(final char[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		char tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * <p>
	 * Reverses the order of the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method does nothing for a <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to reverse, may be <code>null</code>
	 */
	public static void reverse(final double[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		double tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * <p>
	 * Reverses the order of the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method does nothing for a <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to reverse, may be <code>null</code>
	 */
	public static void reverse(final float[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		float tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * <p>
	 * Reverses the order of the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method does nothing for a <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to reverse, may be <code>null</code>
	 */
	public static void reverse(final int[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		int tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * <p>
	 * Reverses the order of the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method does nothing for a <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to reverse, may be <code>null</code>
	 */
	public static void reverse(final long[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		long tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	// Reverse
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Reverses the order of the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * There is no special handling for multi-dimensional arrays.
	 * </p>
	 * <p/>
	 * <p>
	 * This method does nothing for a <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to reverse, may be <code>null</code>
	 */
	public static void reverse(final Object[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		Object tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * <p>
	 * Reverses the order of the given array.
	 * </p>
	 * <p/>
	 * <p>
	 * This method does nothing for a <code>null</code> input array.
	 * </p>
	 * 
	 * @param array
	 *            the array to reverse, may be <code>null</code>
	 */
	public static void reverse(final short[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		short tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * <p>
	 * Produces a new <code>boolean</code> array containing the elements between
	 * the start and end indices.
	 * </p>
	 * <p/>
	 * <p>
	 * The start index is inclusive, the end index exclusive. Null array input
	 * produces null output.
	 * </p>
	 * 
	 * @param array
	 *            the array
	 * @param startIndexInclusive
	 *            the starting index. Undervalue (&lt;0) is promoted to 0,
	 *            overvalue (&gt;array.length) results in an empty array.
	 * @param endIndexExclusive
	 *            elements up to endIndex-1 are present in the returned
	 *            subarray. Undervalue (&lt; startIndex) produces empty array,
	 *            overvalue (&gt;array.length) is demoted to array length.
	 * @return a new array containing the elements between the start and end
	 *         indices.
	 * @since 2.1
	 */
	public static boolean[] subarray(final boolean[] array,
			int startIndexInclusive, int endIndexExclusive) {
		if (array == null) {
			return null;
		}
		if (startIndexInclusive < 0) {
			startIndexInclusive = 0;
		}
		if (endIndexExclusive > array.length) {
			endIndexExclusive = array.length;
		}
		final int newSize = endIndexExclusive - startIndexInclusive;
		if (newSize <= 0) {
			return new boolean[0];// EMPTY_BOOLEAN_ARRAY;
		}

		final boolean[] subarray = new boolean[newSize];
		System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
		return subarray;
	}

	/**
	 * <p>
	 * Produces a new <code>byte</code> array containing the elements between
	 * the start and end indices.
	 * </p>
	 * <p/>
	 * <p>
	 * The start index is inclusive, the end index exclusive. Null array input
	 * produces null output.
	 * </p>
	 * 
	 * @param array
	 *            the array
	 * @param startIndexInclusive
	 *            the starting index. Undervalue (&lt;0) is promoted to 0,
	 *            overvalue (&gt;array.length) results in an empty array.
	 * @param endIndexExclusive
	 *            elements up to endIndex-1 are present in the returned
	 *            subarray. Undervalue (&lt; startIndex) produces empty array,
	 *            overvalue (&gt;array.length) is demoted to array length.
	 * @return a new array containing the elements between the start and end
	 *         indices.
	 * @since 2.1
	 */
	public static byte[] subarray(final byte[] array, int startIndexInclusive,
			int endIndexExclusive) {
		if (array == null) {
			return null;
		}
		if (startIndexInclusive < 0) {
			startIndexInclusive = 0;
		}
		if (endIndexExclusive > array.length) {
			endIndexExclusive = array.length;
		}
		final int newSize = endIndexExclusive - startIndexInclusive;
		if (newSize <= 0) {
			return new byte[0];// EMPTY_BYTE_ARRAY;
		}

		final byte[] subarray = new byte[newSize];
		System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
		return subarray;
	}

	/**
	 * <p>
	 * Produces a new <code>char</code> array containing the elements between
	 * the start and end indices.
	 * </p>
	 * <p/>
	 * <p>
	 * The start index is inclusive, the end index exclusive. Null array input
	 * produces null output.
	 * </p>
	 * 
	 * @param array
	 *            the array
	 * @param startIndexInclusive
	 *            the starting index. Undervalue (&lt;0) is promoted to 0,
	 *            overvalue (&gt;array.length) results in an empty array.
	 * @param endIndexExclusive
	 *            elements up to endIndex-1 are present in the returned
	 *            subarray. Undervalue (&lt; startIndex) produces empty array,
	 *            overvalue (&gt;array.length) is demoted to array length.
	 * @return a new array containing the elements between the start and end
	 *         indices.
	 * @since 2.1
	 */
	public static char[] subarray(final char[] array, int startIndexInclusive,
			int endIndexExclusive) {
		if (array == null) {
			return null;
		}
		if (startIndexInclusive < 0) {
			startIndexInclusive = 0;
		}
		if (endIndexExclusive > array.length) {
			endIndexExclusive = array.length;
		}
		final int newSize = endIndexExclusive - startIndexInclusive;
		if (newSize <= 0) {
			return new char[0];// EMPTY_CHAR_ARRAY;
		}

		final char[] subarray = new char[newSize];
		System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
		return subarray;
	}

	/**
	 * <p>
	 * Produces a new <code>double</code> array containing the elements between
	 * the start and end indices.
	 * </p>
	 * <p/>
	 * <p>
	 * The start index is inclusive, the end index exclusive. Null array input
	 * produces null output.
	 * </p>
	 * 
	 * @param array
	 *            the array
	 * @param startIndexInclusive
	 *            the starting index. Undervalue (&lt;0) is promoted to 0,
	 *            overvalue (&gt;array.length) results in an empty array.
	 * @param endIndexExclusive
	 *            elements up to endIndex-1 are present in the returned
	 *            subarray. Undervalue (&lt; startIndex) produces empty array,
	 *            overvalue (&gt;array.length) is demoted to array length.
	 * @return a new array containing the elements between the start and end
	 *         indices.
	 * @since 2.1
	 */
	public static double[] subarray(final double[] array,
			int startIndexInclusive, int endIndexExclusive) {
		if (array == null) {
			return null;
		}
		if (startIndexInclusive < 0) {
			startIndexInclusive = 0;
		}
		if (endIndexExclusive > array.length) {
			endIndexExclusive = array.length;
		}
		final int newSize = endIndexExclusive - startIndexInclusive;
		if (newSize <= 0) {
			return new double[0];// EMPTY_DOUBLE_ARRAY;
		}

		final double[] subarray = new double[newSize];
		System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
		return subarray;
	}

	/**
	 * <p>
	 * Produces a new <code>float</code> array containing the elements between
	 * the start and end indices.
	 * </p>
	 * <p/>
	 * <p>
	 * The start index is inclusive, the end index exclusive. Null array input
	 * produces null output.
	 * </p>
	 * 
	 * @param array
	 *            the array
	 * @param startIndexInclusive
	 *            the starting index. Undervalue (&lt;0) is promoted to 0,
	 *            overvalue (&gt;array.length) results in an empty array.
	 * @param endIndexExclusive
	 *            elements up to endIndex-1 are present in the returned
	 *            subarray. Undervalue (&lt; startIndex) produces empty array,
	 *            overvalue (&gt;array.length) is demoted to array length.
	 * @return a new array containing the elements between the start and end
	 *         indices.
	 * @since 2.1
	 */
	public static float[] subarray(final float[] array,
			int startIndexInclusive, int endIndexExclusive) {
		if (array == null) {
			return null;
		}
		if (startIndexInclusive < 0) {
			startIndexInclusive = 0;
		}
		if (endIndexExclusive > array.length) {
			endIndexExclusive = array.length;
		}
		final int newSize = endIndexExclusive - startIndexInclusive;
		if (newSize <= 0) {
			return new float[0];// EMPTY_FLOAT_ARRAY;
		}

		final float[] subarray = new float[newSize];
		System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
		return subarray;
	}

	/**
	 * <p>
	 * Produces a new <code>int</code> array containing the elements between the
	 * start and end indices.
	 * </p>
	 * <p/>
	 * <p>
	 * The start index is inclusive, the end index exclusive. Null array input
	 * produces null output.
	 * </p>
	 * 
	 * @param array
	 *            the array
	 * @param startIndexInclusive
	 *            the starting index. Undervalue (&lt;0) is promoted to 0,
	 *            overvalue (&gt;array.length) results in an empty array.
	 * @param endIndexExclusive
	 *            elements up to endIndex-1 are present in the returned
	 *            subarray. Undervalue (&lt; startIndex) produces empty array,
	 *            overvalue (&gt;array.length) is demoted to array length.
	 * @return a new array containing the elements between the start and end
	 *         indices.
	 * @since 2.1
	 */
	public static int[] subarray(final int[] array, int startIndexInclusive,
			int endIndexExclusive) {
		if (array == null) {
			return null;
		}
		if (startIndexInclusive < 0) {
			startIndexInclusive = 0;
		}
		if (endIndexExclusive > array.length) {
			endIndexExclusive = array.length;
		}
		final int newSize = endIndexExclusive - startIndexInclusive;
		if (newSize <= 0) {
			return new int[0];// EMPTY_INT_ARRAY;
		}

		final int[] subarray = new int[newSize];
		System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
		return subarray;
	}

	/**
	 * <p>
	 * Produces a new <code>long</code> array containing the elements between
	 * the start and end indices.
	 * </p>
	 * <p/>
	 * <p>
	 * The start index is inclusive, the end index exclusive. Null array input
	 * produces null output.
	 * </p>
	 * 
	 * @param array
	 *            the array
	 * @param startIndexInclusive
	 *            the starting index. Undervalue (&lt;0) is promoted to 0,
	 *            overvalue (&gt;array.length) results in an empty array.
	 * @param endIndexExclusive
	 *            elements up to endIndex-1 are present in the returned
	 *            subarray. Undervalue (&lt; startIndex) produces empty array,
	 *            overvalue (&gt;array.length) is demoted to array length.
	 * @return a new array containing the elements between the start and end
	 *         indices.
	 * @since 2.1
	 */
	public static long[] subarray(final long[] array, int startIndexInclusive,
			int endIndexExclusive) {
		if (array == null) {
			return null;
		}
		if (startIndexInclusive < 0) {
			startIndexInclusive = 0;
		}
		if (endIndexExclusive > array.length) {
			endIndexExclusive = array.length;
		}
		final int newSize = endIndexExclusive - startIndexInclusive;
		if (newSize <= 0) {
			return new long[0];// EMPTY_LONG_ARRAY;
		}

		final long[] subarray = new long[newSize];
		System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
		return subarray;
	}

	/**
	 * <p>
	 * Produces a new <code>short</code> array containing the elements between
	 * the start and end indices.
	 * </p>
	 * <p/>
	 * <p>
	 * The start index is inclusive, the end index exclusive. Null array input
	 * produces null output.
	 * </p>
	 * 
	 * @param array
	 *            the array
	 * @param startIndexInclusive
	 *            the starting index. Undervalue (&lt;0) is promoted to 0,
	 *            overvalue (&gt;array.length) results in an empty array.
	 * @param endIndexExclusive
	 *            elements up to endIndex-1 are present in the returned
	 *            subarray. Undervalue (&lt; startIndex) produces empty array,
	 *            overvalue (&gt;array.length) is demoted to array length.
	 * @return a new array containing the elements between the start and end
	 *         indices.
	 * @since 2.1
	 */
	public static short[] subarray(final short[] array,
			int startIndexInclusive, int endIndexExclusive) {
		if (array == null) {
			return null;
		}
		if (startIndexInclusive < 0) {
			startIndexInclusive = 0;
		}
		if (endIndexExclusive > array.length) {
			endIndexExclusive = array.length;
		}
		final int newSize = endIndexExclusive - startIndexInclusive;
		if (newSize <= 0) {
			return new short[0];// EMPTY_SHORT_ARRAY;
		}

		final short[] subarray = new short[newSize];
		System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
		return subarray;
	}

	// Subarrays
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Produces a new array containing the elements between the start and end
	 * indices.
	 * </p>
	 * <p/>
	 * <p>
	 * The start index is inclusive, the end index exclusive. Null array input
	 * produces null output.
	 * </p>
	 * <p/>
	 * <p>
	 * The component type of the subarray is always the same as that of the
	 * input array. Thus, if the input is an array of type <code>Date</code>,
	 * the following usage is envisaged:
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * Date[] someDates = (Date[]) ArrayUtils.subarray(allDates, 2, 5);
	 * </pre>
	 * 
	 * @param array
	 *            the array
	 * @param startIndexInclusive
	 *            the starting index. Undervalue (&lt;0) is promoted to 0,
	 *            overvalue (&gt;array.length) results in an empty array.
	 * @param endIndexExclusive
	 *            elements up to endIndex-1 are present in the returned
	 *            subarray. Undervalue (&lt; startIndex) produces empty array,
	 *            overvalue (&gt;array.length) is demoted to array length.
	 * @return a new array containing the elements between the start and end
	 *         indices.
	 * @since 2.1
	 */
	public static <T> T[] subarray(final T[] array, int startIndexInclusive,
			int endIndexExclusive) {
		if (array == null) {
			return null;
		}
		if (startIndexInclusive < 0) {
			startIndexInclusive = 0;
		}
		if (endIndexExclusive > array.length) {
			endIndexExclusive = array.length;
		}
		final int newSize = endIndexExclusive - startIndexInclusive;
		final Class<?> type = array.getClass().getComponentType();
		if (newSize <= 0) {
			@SuppressWarnings("unchecked")
			// OK, because array is of type T
			final T[] emptyArray = (T[]) Array.newInstance(type, 0);
			return emptyArray;
		}
		@SuppressWarnings("unchecked")
		final// OK, because array is of type T
		T[] subarray = (T[]) Array.newInstance(type, newSize);
		System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
		return subarray;
	}

	// Generic array
	// -----------------------------------------------------------------------

	/**
	 * Create a type-safe generic array.
	 * <p/>
	 * <p>
	 * Arrays are covariant i.e. they cannot be created from a generic type:
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * public static &lt;T&gt; T[] createAnArray(int size) {
	 * 	return T[size]; // compiler error here
	 * }
	 * 
	 * public static &lt;T&gt; T[] createAnArray(int size) {
	 * 	return (T[]) Object[size]; // ClassCastException at runtime
	 * }
	 * </pre>
	 * <p/>
	 * <p>
	 * Therefore new arrays of generic types can be created with this method,
	 * e.g. an arrays of Strings:
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * String[] array = ArrayUtils.toArray(&quot;1&quot;, &quot;2&quot;);
	 * String[] emptyArray = ArrayUtils.&lt;String&gt; toArray();
	 * </pre>
	 * <p/>
	 * The method is typically used in scenarios, where the caller itself uses
	 * generic types that have to be combined into an array.
	 * <p/>
	 * Note, this method makes only sense to provide arguments of the same type
	 * so that the compiler can deduce the type of the array itself. While it is
	 * possible to select the type explicitly like in
	 * <code>Number[] array = ArrayUtils.<Number>toArray(new
	 * Integer(42), new Double(Math.PI))</code>, there is no real advantage to
	 * <code>new
	 * Number[] {new Integer(42), new Double(Math.PI)}</code> anymore.
	 * 
	 * @param <T>
	 *            the array's element type
	 * @param items
	 *            the items of the array
	 * @return the array
	 * @since 3.0
	 */
	public static <T> T[] toArray(final T... items) {
		return items;
	}

	// To map
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Converts the given array into a {@link java.util.Map}. Each element of
	 * the array must be either a {@link java.util.Map.Entry} or an Array,
	 * containing at least two elements, where the first element is used as key
	 * and the second as value.
	 * </p>
	 * <p/>
	 * <p>
	 * This method can be used to initialize:
	 * </p>
	 * <p/>
	 * 
	 * <pre>
	 * // Create a Map mapping colors.
	 * Map colorMap = MapUtils.toMap(new String[][] {{
	 *     {"RED", "#FF0000"},
	 *     {"GREEN", "#00FF00"},
	 *     {"BLUE", "#0000FF"}});
	 * </pre>
	 * <p/>
	 * <p>
	 * This method returns <code>null</code> for a <code>null</code> input
	 * array.
	 * </p>
	 * 
	 * @param array
	 *            an array whose elements are either a
	 *            {@link java.util.Map.Entry} or an Array containing at least
	 *            two elements, may be <code>null</code>
	 * @return a <code>Map</code> that was created from the array
	 * @throws IllegalArgumentException
	 *             if one element of this Array is itself an Array containing
	 *             less then two elements
	 * @throws IllegalArgumentException
	 *             if the array contains elements other than
	 *             {@link java.util.Map.Entry} and an Array
	 */
	public static Map<Object, Object> toMap(final Object[] array) {
		if (array == null) {
			return null;
		}
		final Map<Object, Object> map = new HashMap<Object, Object>(
				(int) (array.length * 1.5));
		for (int i = 0; i < array.length; i++) {
			final Object object = array[i];
			if (object instanceof Map.Entry<?, ?>) {
				final Map.Entry<?, ?> entry = (Map.Entry<?, ?>) object;
				map.put(entry.getKey(), entry.getValue());
			} else if (object instanceof Object[]) {
				final Object[] entry = (Object[]) object;
				if (entry.length < 2) {
					throw new IllegalArgumentException("Array element " + i
							+ ", '" + object + "', has a length less than 2");
				}
				map.put(entry[0], entry[1]);
			} else {
				throw new IllegalArgumentException("Array element " + i + ", '"
						+ object
						+ "', is neither of type Map.Entry nor an Array");
			}
		}
		return map;
	}

	/**
	 * <p>
	 * Converts an array of object Booleans to primitives handling
	 * <code>null</code>.
	 * </p>
	 * <p/>
	 * <p>
	 * This method returns <code>null</code> for a <code>null</code> input
	 * array.
	 * </p>
	 * 
	 * @param array
	 *            a <code>Boolean</code> array, may be <code>null</code>
	 * @param valueForNull
	 *            the value to insert if <code>null</code> found
	 * @return a <code>boolean</code> array, <code>null</code> if null array
	 *         input
	 */
	public static boolean[] toPrimitive(final Boolean[] array,
			final boolean valueForNull) {
		if (array == null) {
			return null;
		} else if (array.length == 0) {
			return new boolean[0];// ]EMPTY_BOOLEAN_ARRAY;
		}
		final boolean[] result = new boolean[array.length];
		for (int i = 0; i < array.length; i++) {
			final Boolean b = array[i];
			result[i] = (b == null ? valueForNull : b.booleanValue());
		}
		return result;
	}

	// Basic methods handling multi-dimensional arrays
	// -----------------------------------------------------------------------

	/**
	 * <p>
	 * Outputs an array as a String, treating <code>null</code> as an empty
	 * array.
	 * </p>
	 * <p/>
	 * <p>
	 * Multi-dimensional arrays are handled correctly, including
	 * multi-dimensional primitive arrays.
	 * </p>
	 * <p/>
	 * <p>
	 * The format is that of Java source code, for example <code>{a,b}</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to get a toString for, may be <code>null</code>
	 * @return a String representation of the array, '{}' if null array input
	 */
	public static String toString(final Object array) {
		return toString(array, "{}");
	}

	// TODO(kevinb): consider making this public
	static int indexOf(final int[] array, final int target, final int start,
			final int end) {
		for (int i = start; i < end; i++) {
			if (array[i] == target) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Returns the start position of the first occurrence of the specified
	 * {@code target} within {@code array}, or {@code -1} if there is no such
	 * occurrence.
	 * 
	 * <p>
	 * More formally, returns the lowest index {@code i} such that
	 * {@code java.util.Arrays.copyOfRange(array, i, i + target.length)}
	 * contains exactly the same elements as {@code target}.
	 * 
	 * @param array
	 *            the array to search for the sequence {@code target}
	 * @param target
	 *            the array to search for as a sub-sequence of {@code array}
	 */
	public static int indexOf(final int[] array, final int[] target) {
		checkNotNull(array, "array");
		checkNotNull(target, "target");
		if (target.length == 0) {
			return 0;
		}

		outer: for (int i = 0; i < array.length - target.length + 1; i++) {
			for (int j = 0; j < target.length; j++) {
				if (array[i + j] != target[j]) {
					continue outer;
				}
			}
			return i;
		}
		return -1;
	}

	/**
	 * Returns the index of the last appearance of the value {@code target} in
	 * {@code array}.
	 * 
	 * @param array
	 *            an array of {@code int} values, possibly empty
	 * @param target
	 *            a primitive {@code int} value
	 * @return the greatest index {@code i} for which {@code array[i] == target}
	 *         , or {@code -1} if no such index exists.
	 */
	public static int lastIndexOf(final int[] array, final int target) {
		return lastIndexOf(array, target, 0, array.length);
	}

	// TODO(kevinb): consider making this public
	static int lastIndexOf(final int[] array, final int target,
			final int start, final int end) {
		for (int i = end - 1; i >= start; i--) {
			if (array[i] == target) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Returns an array containing the same values as {@code array}, but
	 * guaranteed to be of a specified minimum length. If {@code array} already
	 * has a length of at least {@code minLength}, it is returned directly.
	 * Otherwise, a new array of size {@code minLength + padding} is returned,
	 * containing the values of {@code array}, and zeroes in the remaining
	 * places.
	 * 
	 * @param array
	 *            the source array
	 * @param minLength
	 *            the minimum length the returned array must guarantee
	 * @param padding
	 *            an extra amount to "grow" the array by if growth is necessary
	 * @throws IllegalArgumentException
	 *             if {@code minLength} or {@code padding} is negative
	 * @return an array containing the values of {@code array}, with guaranteed
	 *         minimum length {@code minLength}
	 */
	public static int[] ensureCapacity(final int[] array, final int minLength,
			final int padding) {
		checkArgument(minLength >= 0, "Invalid minLength: %s", minLength);
		checkArgument(padding >= 0, "Invalid padding: %s", padding);
		return (array.length < minLength) ? ArrayUtils.copyOf(array, minLength
				+ padding) : array;
	}

	/**
	 * <p>
	 * Outputs an array as a String handling <code>null</code>s.
	 * </p>
	 * <p/>
	 * <p>
	 * Multi-dimensional arrays are handled correctly, including
	 * multi-dimensional primitive arrays.
	 * </p>
	 * <p/>
	 * <p>
	 * The format is that of Java source code, for example <code>{a,b}</code>.
	 * </p>
	 * 
	 * @param array
	 *            the array to get a toString for, may be <code>null</code>
	 * @param stringIfNull
	 *            the String to return if the array is <code>null</code>
	 * @return a String representation of the array
	 */
	public static String toString(final Object array, final String stringIfNull) {
		if (array == null) {
			return stringIfNull;
		}
		return new ToStringBuilder(array, ToStringStyle.SIMPLE_STYLE).append(
				array).toString();
	}

	/**
	 * Copies the specified array, truncating or padding with zeros (if
	 * necessary) so the copy has the specified length. For all indices that are
	 * valid in both the original array and the copy, the two arrays will
	 * contain identical values. For any indices that are valid in the copy but
	 * not the original, the copy will contain 0L. Such indices will exist if
	 * and only if the specified length is greater than that of the original
	 * array.
	 * <p>
	 * Implementation of the missing Arrays.copyof method
	 * </p>
	 * 
	 * @param original
	 *            the array to be copied
	 * @param newLength
	 *            the length of the copy to be returned
	 * @return
	 * @throws java.lang.NegativeArraySizeException
	 *             if newLength is negative
	 */
	public static long[] copyOf(final long[] original, final int newLength) {
		final long[] copy = new long[newLength];
		System.arraycopy(original, 0, copy, 0,
				Math.min(original.length, newLength));
		return copy;
	}

	/**
	 * Copies the specified array, truncating or padding with zeros (if
	 * necessary) so the copy has the specified length. For all indices that are
	 * valid in both the original array and the copy, the two arrays will
	 * contain identical values. For any indices that are valid in the copy but
	 * not the original, the copy will contain 0. Such indices will exist if and
	 * only if the specified length is greater than that of the original array.
	 * 
	 * @param original
	 * @param newLength
	 * @return
	 */
	public static int[] copyOf(final int[] original, final int newLength) {
		final int[] copy = new int[newLength];
		System.arraycopy(original, 0, copy, 0,
				Math.min(original.length, newLength));
		return copy;
	}

	/**
	 * Returns the least value present in {@code array}.
	 * 
	 * @param array
	 *            a <i>nonempty</i> array of {@code int} values
	 * @return the value present in {@code array} that is less than or equal to
	 *         every other value in the array
	 * @throws IllegalArgumentException
	 *             if {@code array} is empty
	 */
	public static int min(final int... array) {
		checkArgument(array.length > 0);
		int min = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}
		return min;
	}

	/**
	 * Returns the greatest value present in {@code array}.
	 * 
	 * @param array
	 *            a <i>nonempty</i> array of {@code int} values
	 * @return the value present in {@code array} that is greater than or equal
	 *         to every other value in the array
	 * @throws IllegalArgumentException
	 *             if {@code array} is empty
	 */
	public static int max(final int... array) {
		checkArgument(array.length > 0);
		int max = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}

	/**
	 * Returns the values from each provided array combined into a single array.
	 * For example, {@code concat(new int[] a, b}, new int[] {}, new int[] {c}}
	 * returns the array {@code a, b, c} .
	 * 
	 * @param arrays
	 *            zero or more {@code int} arrays
	 * @return a single array containing all the values from the source arrays,
	 *         in order
	 */
	public static int[] concat(final int[]... arrays) {
		int length = 0;
		for (final int[] array : arrays) {
			length += array.length;
		}
		final int[] result = new int[length];
		int pos = 0;
		for (final int[] array : arrays) {
			System.arraycopy(array, 0, result, pos, array.length);
			pos += array.length;
		}
		return result;
	}

	/**
	 * Copies the specified array, truncating or padding with null characters
	 * (if necessary) so the copy has the specified length. For all indices that
	 * are valid in both the original array and the copy, the two arrays will
	 * contain identical values. For any indices that are valid in the copy but
	 * not the original, the copy will contain '\\u000'. Such indices will exist
	 * if and only if the specified length is greater than that of the original
	 * array.
	 * 
	 * @param original
	 * @param newLength
	 * @return
	 */
	public static char[] copyOf(final char[] original, final int newLength) {
		final char[] copy = new char[newLength];
		System.arraycopy(original, 0, copy, 0,
				Math.min(original.length, newLength));
		return copy;
	}

	/**
	 * Copies the specified array, truncating or padding with zeros (if
	 * necessary) so the copy has the specified length. For all indices that are
	 * valid in both the original array and the copy, the two arrays will
	 * contain identical values. For any indices that are valid in the copy but
	 * not the original, the copy will contain 0f. Such indices will exist if
	 * and only if the specified length is greater than that of the original
	 * array.
	 * 
	 * @param original
	 * @param newLength
	 * @return
	 */
	public static float[] copyOf(final float[] original, final int newLength) {
		final float[] copy = new float[newLength];
		System.arraycopy(original, 0, copy, 0,
				Math.min(original.length, newLength));
		return copy;
	}

	/**
	 * Copies the specified array, truncating or padding with zeros (if
	 * necessary) so the copy has the specified length. For all indices that are
	 * valid in both the original array and the copy, the two arrays will
	 * contain identical values. For any indices that are valid in the copy but
	 * not the original, the copy will contain 0f. Such indices will exist if
	 * and only if the specified length is greater than that of the original
	 * array.
	 * 
	 * @param original
	 * @param newLength
	 * @return
	 */
	public static byte[] copyOf(final byte[] original, final int newLength) {
		final byte[] copy = new byte[newLength];
		System.arraycopy(original, 0, copy, 0,
				Math.min(original.length, newLength));
		return copy;
	}

	public static final byte[] slice(final byte bytes[], final int start,
			final int count) {
		if (bytes.length < (start + count)) {
			return null;
		}

		final byte result[] = new byte[count];
		System.arraycopy(bytes, start, result, 0, count);

		return result;
	}

	public static final byte[] tail(final byte bytes[], int count) {
		if (count > bytes.length) {
			count = bytes.length;
		}
		return slice(bytes, bytes.length - count, count);
	}

	public static final byte[] head(final byte bytes[], int count) {
		if (count > bytes.length) {
			count = bytes.length;
		}
		return slice(bytes, 0, count);
	}

	/**
	 * Find the index at which the value is 0
	 * 
	 * @param src
	 * @return
	 */
	public static final int findNull(final byte src[]) {
		return findNull(src, 0);
	}

	/**
	 * Find the index at which the value is 0
	 * 
	 * @param src
	 * @param start
	 *            to start from
	 * @return
	 */
	public static final int findNull(final byte src[], final int start) {
		for (int i = start; i < src.length; i++) {
			if (src[i] == 0) {
				return i;
			}

		}
		return -1;
	}

	public static void checkRange(final byte[] buf, final int off) {
		if (off < 0 || off >= buf.length) {
			throw new ArrayIndexOutOfBoundsException(off);
		}
	}

	public static void checkRange(final byte[] buf, final int off, final int len) {
		checkLength(len);
		if (len > 0) {
			checkRange(buf, off);
			checkRange(buf, off + len - 1);
		}
	}

	public static void checkLength(final int len) {
		if (len < 0) {
			throw new IllegalArgumentException("lengths must be >= 0");
		}
	}

}
