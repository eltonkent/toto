package toto.io.codec;

public class ToToEncoderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ToToEncoderException(final String message) {
		super(message);
	}
}
