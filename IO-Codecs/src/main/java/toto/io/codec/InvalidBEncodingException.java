package toto.io.codec;

import java.io.IOException;

/**
 * Exception thrown when a B-encoded stream cannot be decoded.
 * 
 */
public class InvalidBEncodingException extends IOException {

	public static final long serialVersionUID = -1;

	public InvalidBEncodingException(final String message) {
		super(message);
	}
}
