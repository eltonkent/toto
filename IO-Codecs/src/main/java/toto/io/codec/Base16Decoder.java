package toto.io.codec;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * This class decodes a text stream containing into a binary stream.
 * 
 * <p>
 * The Base16 encoding is suitable when binary data needs to be transmitted or
 * stored as text, when case insensitivity is needed. It is defined in <a
 * href="http://www.ietf.org/rfc/rfc3548.txt">RFC 3548</a> <i>The Base16,
 * Base32, and Base64 Data Encodings</i> by S. Josefsson
 * </p>
 * 
 * @author Luc Maisonobe
 * @see Base16Encoder
 */
public class Base16Decoder extends AbstractDecoder {

	/**
	 * Create a decoder wrapping a source of encoded data.
	 * <p>
	 * The decoder built using this constructor will strictly obey <a
	 * href="http://www.ietf.org/rfc/rfc3548.txt">RFC 3548</a>. This means that
	 * if some encoded bytes do not belong to the Base16 alphabet, an
	 * <code>IOException</code> will be thrown at read time.
	 * </p>
	 * <p>
	 * Note that calling this constructor is equivalent to calling
	 * {@link #Base16Decoder(InputStream,boolean) Base16Decoder(<code>in</code>,
	 * <code>-true</code>)}.
	 * </p>
	 * 
	 * @param in
	 *            source of encoded data to decode
	 */
	public Base16Decoder(final InputStream in) {
		super(in);
		strictRFCCompliance = true;
		phase = 0;
	}

	/**
	 * Create a decoder wrapping a source of encoded data.
	 * <p>
	 * If the decoder built using this constructor strictly obeys <a
	 * href="http://www.ietf.org/rfc/rfc3548.txt">RFC 3548</a> and some encoded
	 * bytes do not belong to the Base16 alphabet, then an
	 * <code>IOException</code> will be thrown at read time.
	 * </p>
	 * <p>
	 * Note that calling this constructor with <code>strictRFCCompliance</code>
	 * setKey to true is equivalent to calling the one argument
	 * {@link #Base16Decoder(InputStream) constructor}.
	 * </p>
	 * 
	 * @param in
	 *            source of encoded data to decode
	 * @param strictRFCCompliance
	 *            if true, characters outside of the Base16 alphabet will
	 *            trigger an <code>IOException</code> at read time, otherwise
	 *            they will be silently ignored
	 */
	public Base16Decoder(final InputStream in, final boolean strictRFCCompliance) {
		super(in);
		this.strictRFCCompliance = strictRFCCompliance;
		phase = 0;
	}

	/**
	 * Filter some bytes from the underlying stream.
	 * 
	 * @return number of bytes inserted in the filtered bytes buffer or -1 if
	 *         the underlying stream has no bytes left
	 * @exception IOException
	 *                if the underlying stream throws one
	 */
	protected int filterBytes() throws IOException {

		if (nEncoded < 1) {
			if (readEncodedBytes() < 0) {
				return -1;
			}
		}

		// bytes decoding loop
		final int inserted = 0;
		for (int i = 0; i < nEncoded; ++i) {
			final int b = decode[encoded[i]];
			if (b < 0) {
				if (strictRFCCompliance) {
					throw new IOException("non-Base16 character read in strict"
							+ " RFC 3548 compliance mode");
				}
			} else {
				// combine the various 5-bits bytes to produce 8-bits bytes
				if (phase == 0) {
					b0 = b;
					phase = 1;
				} else {
					putFilteredByte(((b0 & 0xF) << 4) | (b & 0xF));
					phase = 0;
				}
			}
		}

		nEncoded = 0;
		return inserted;

	}

	/** RFC 3548 compliance indicator. */
	private final boolean strictRFCCompliance;

	/** Phase (modulo 2) of encoded bytes read. */
	private int phase;

	/** First byte of the current quantum. */
	private int b0;

	/** Decoding array. */
	private static final int[] decode = new int[255];

	static {
		final int[] code = new int[] { '0', '1', '2', '3', '4', '5', '6', '7',
				'8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		Arrays.fill(decode, -1);
		for (int i = 0; i < code.length; ++i) {
			decode[code[i]] = i;
			if (Character.isLetter((char) code[i])) {
				decode[Character.toLowerCase((char) code[i])] = i;
			}
		}
	}

	/**
	 * Convert a base16 string into a byte array.
	 */
	public static byte[] decode(final String s) {
		final int len = s.length();
		final byte[] r = new byte[len / 2];
		for (int i = 0; i < r.length; i++) {
			int digit1 = s.charAt(i * 2), digit2 = s.charAt(i * 2 + 1);
			if (digit1 >= '0' && digit1 <= '9')
				digit1 -= '0';
			else if (digit1 >= 'A' && digit1 <= 'F')
				digit1 -= 'A' - 10;
			if (digit2 >= '0' && digit2 <= '9')
				digit2 -= '0';
			else if (digit2 >= 'A' && digit2 <= 'F')
				digit2 -= 'A' - 10;

			r[i] = (byte) ((digit1 << 4) + digit2);
		}
		return r;
	}

}
