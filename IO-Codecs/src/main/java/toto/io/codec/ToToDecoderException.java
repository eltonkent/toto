package toto.io.codec;

public class ToToDecoderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ToToDecoderException(final String message) {
		super(message);
	}
}
