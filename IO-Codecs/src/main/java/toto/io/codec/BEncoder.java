package toto.io.codec;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * B-encoding the encoding used by the peer-to-peer file sharing system
 * BitTorrent for storing and transmitting loosely structured data.
 * 
 * <p>
 * This class provides utility methods to encode objects and {@link BEValue}s to
 * B-encoding into a provided output stream.
 * </p>
 * 
 * <p>
 * Inspired by Snark's implementation.
 * </p>
 * 
 * @see <a href="http://en.wikipedia.org/wiki/Bencode">B-encoding
 *      specification</a>
 */
public class BEncoder {

	@SuppressWarnings("unchecked")
	public static void bencode(Object o, final OutputStream out)
			throws IOException, IllegalArgumentException {
		if (o instanceof BEValue) {
			o = ((BEValue) o).getValue();
		}

		if (o instanceof String) {
			bencode((String) o, out);
		} else if (o instanceof byte[]) {
			bencode((byte[]) o, out);
		} else if (o instanceof Number) {
			bencode((Number) o, out);
		} else if (o instanceof List) {
			bencode((List<BEValue>) o, out);
		} else if (o instanceof Map) {
			bencode((Map<String, BEValue>) o, out);
		} else {
			throw new IllegalArgumentException("Cannot bencode: "
					+ o.getClass());
		}
	}

	public static void bencode(final String s, final OutputStream out)
			throws IOException {
		final byte[] bs = s.getBytes("UTF-8");
		bencode(bs, out);
	}

	public static void bencode(final Number n, final OutputStream out)
			throws IOException {
		out.write('i');
		final String s = n.toString();
		out.write(s.getBytes("UTF-8"));
		out.write('e');
	}

	public static void bencode(final List<BEValue> l, final OutputStream out)
			throws IOException {
		out.write('l');
		for (final BEValue value : l) {
			bencode(value, out);
		}
		out.write('e');
	}

	public static void bencode(final byte[] bs, final OutputStream out)
			throws IOException {
		final String l = Integer.toString(bs.length);
		out.write(l.getBytes("UTF-8"));
		out.write(':');
		out.write(bs);
	}

	public static void bencode(final Map<String, BEValue> m,
			final OutputStream out) throws IOException {
		out.write('d');

		// Keys must be sorted.
		final Set<String> s = m.keySet();
		final List<String> l = new ArrayList<String>(s);
		Collections.sort(l);

		for (final String key : l) {
			final Object value = m.get(key);
			bencode(key, out);
			bencode(value, out);
		}

		out.write('e');
	}

	public static ByteBuffer bencode(final Map<String, BEValue> m)
			throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BEncoder.bencode(m, baos);
		baos.close();
		return ByteBuffer.wrap(baos.toByteArray());
	}
}
