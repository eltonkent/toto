/**
 * Super fast cache implementation. Allowing you to cache anything, anywhere.
 * <p>
 * <div>
 * <h3>Using ToTo Cache</h3>
 * <h4>Why use ToTo cache?</h4>
 * Caching is an important for a host of reasons. A few of them being,
 * <ul>
 * <li>Saves time.</li>
 * <li>Lowers data usage for the app.</li>
 * <li>Reduces server load.</li>
 * <li>Provides a snappy user experience.</li>
 * </ul>
 * <h4>ToTo Caching</h4>
 * 	<center><img src="tcache.png"/></center>
 * Using {@link toto.cache.TCache} is extremely simple. All cache entries are stored as Key,Value pairs and at a low level these key-value pairs are stored as an array of bytes (byte[]).
 * TCache  is made up of two basic elements the {@link toto.cache.Storage} and the {@link toto.cache.impl.SerializedStorageConverter}.
 * <h5>Storage Providers</h5>
 * The {@link toto.cache.Storage} is basically a mechanism for storing and retrieving cache entries and implements the mechanism for saving and retrieving byte[] objects. 
 * There are number {@link toto.cache.Storage} implementations in the cache package and you could create your own storage provider by simply extending the {@link toto.cache.Storage} class.
 * <ul>
 * <li> {@link toto.cache.storage.impl.HeapStorage}</li>
 * <li> {@link toto.cache.impl.WeakMemoryStorageProvider}</li>
 * <li> {@link toto.db.kvdb.KVDBStorageProvider}</li>
 * <li> {@link toto.cache.storage.impl.SQLiteStorage}</li>
 * <li> {@link rage.db.odb.ODbStorageProvider}</li>
 * <li> {@link toto.cache.storage.impl.DiskStorage}</li>
 * <li> {@link toto.cache.stototo.cacheDirectoryStorageProvider}</li>
 * </ul>
 *  
 * <h5>Converters</h5>
 * Converters allow the conversion of different various types that are stored as keys or values to byte[] and  object types from byte[]. Converters are only used if the {@link toto.cache.Storage} used is of type {@link toto.cache.storage.SerializedStorage}
 * <ul>
 * <li> {@link toto.cache.impl.converters.BitmapConverter}</li>
 * <li> {@link toto.cache.storage.converter.impl.InputStreamConverter}</li>
 * <li> {@link toto.cache.impl.converters.RegExPatterneEntryConverter}</li>
 * <li> {@link toto.cache.storage.converter.impl.SerializableConverter}</li>
 * <li> {@link toto.cache.storage.converter.impl.StringConverter}</li>
 * <li> {@link toto.cache.storage.converter.impl.CursorConverter}</li>
 * <li> {@link toto.cache.storage.converter.impl.ParcelableConverter}</li>
 * </ul>
 * <h4>Using Rage Caches</h4>
 * To use a ToTo cache we need to decide which converters we are going to use and which storage mechanism ({@link toto.cache.Storage}) we would like to use. Let's create a cache that associates Strings(key) with Bitmaps(Value) and uses the {@link toto.cache.storage.impl.SQLiteStorage}.
 * <h5>Create a Cache Instance</h5>
 * <pre>
 * <span>Location for storage provide. This instance uses a folder on the SDCard  </span>
 * File dbDir = new File("/sdcard/appcache/");
 * SQLiteStorageProvider storage=new SQLiteStorageProvider(getContext(), dbDir);
 * <span>Generics are used to define the types of key and value object.</span>
 * <span>Corresponding to the types, we use the converter classes {@link toto.cache.storage.converter.impl.StringConverter} and {@link toto.cache.impl.converters.BitmapConverter}.</span>
 * RCache<String, Bitmap> bitmapCache = new RCache<String, Bitmap>(new StringConverter(null), new BitmapConverter(CompressFormat.JPEG), storage);
 * </pre> 
 * <h5>Adding cache entries</h5>
 * <pre>
 * Bitmap bitmap=<span>bitmap created somehow</span>
 * <span>//cache using string key and bitmap value</span> 
 * bitmapCache.cache("mybitmap",bitmap);
 * </pre>
 * <h5>Retrieving cache entries</h5>
 * <pre>
 * Bitmap myBitmap=bitmapCache.fetch("mybitmap");
 * </pre>
 * <h5>Closing RCache</h5>
 * Closing the cache is important as it releases resources that are held by storage providers. Its a good practice to close the cache when the application no longer needs it.
 * <pre>
 * bitmapCache.close();
 * <span>//You can even destroy the cache if you are never going to need it. Destroying the cache, removes all cache entries.</span>
 * bitmapCache.destroy();
 * </pre>
 * </div>
 * </p>
 * @author Mobifluence Interactive
 *
 */
package toto.cache;

