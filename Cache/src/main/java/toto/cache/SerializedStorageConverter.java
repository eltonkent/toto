package toto.cache;

import com.mi.toto.ToTo;

import java.io.IOException;

import toto.log.Logger;
import toto.util.zip.TLosslessCompression;

/**
 * Base converter for cache entries. Converters are required for
 * {@link SerializedStorage}
 * 
 * @author Mobifluence Interactive
 * 
 * @param <T>
 */
public abstract class SerializedStorageConverter<T> {

	private EncryptionCallback encryptionCallback;
	private TLosslessCompression compressionAlgorithm;

	public final TLosslessCompression getCompressionAlgorithm() {
		return compressionAlgorithm;
	}

	/**
	 * Compression algorithm to allow compression of serialized cache data.
	 * 
	 * @param compressionAlgorithm
	 * @see #setEncryptionCallback(EncryptionCallback)
	 */
	public final void setCompressionAlgorithm(
			final TLosslessCompression compressionAlgorithm) {
		this.compressionAlgorithm = compressionAlgorithm;
	}

	public final EncryptionCallback getEncryptionCallback() {
		return encryptionCallback;
	}

	/**
	 * Callback to allow encryption of the serialized cache data
	 * 
	 * @see #setCompressionAlgorithm(TLosslessCompression)
	 */
	public final void setEncryptionCallback(
			final EncryptionCallback encryptionCallback) {
		this.encryptionCallback = encryptionCallback;
	}

	public T to(byte[] data) {
		/* perform decompression */
		if (encryptionCallback != null) {
			data = encryptionCallback.doDecryption(data);
		}
		if (compressionAlgorithm != null) {
			try {
				data = compressionAlgorithm.decompress(data);
			} catch (final IOException e) {
				Logger.getDefault().d("Could not decompress entry" + e.getMessage());
				ToTo.logException(e);
				return null;
			}
		}
		return toType(data);
	}

	public byte[] from(final T data) {
		byte[] plain = toBytes(data);
		if (plain != null) {
			if (plain.length > maxConvertedSize) {
                Logger.getDefault().d(
						"Cannot cache entry because its size(" + plain.length
								+ "bytes) is greater than maxConvertedSize:"
								+ maxConvertedSize);
				return null;
			}
			/* perform compression */
			if (compressionAlgorithm != null) {
				try {
					plain = compressionAlgorithm.compress(plain);
				} catch (final IOException e) {
                    Logger.getDefault().d("Could not compress entry" + e.getMessage());
					ToTo.logException(e);
					return null;
				}
			}
			/* perform encryption */
			if (encryptionCallback != null) {
				plain = encryptionCallback.doEncryption(plain);
			}
		}
		return plain;
	}

	private int maxConvertedSize = Integer.MAX_VALUE;

	/**
	 * Set the max number of bytes beyond which the entry will not be
	 * serialized.
	 * <p>
	 * This is used to enforce maximum size for entries The default value is
	 * {@code Integer#MAX_VALUE} , 2147483647
	 * </p>
	 * 
	 * @param bytes
	 */
	public void setMaxConvertedSize(final int bytes) {
		maxConvertedSize = bytes;
	}

	protected abstract T toType(byte[] data);

	protected abstract byte[] toBytes(T data);
}
