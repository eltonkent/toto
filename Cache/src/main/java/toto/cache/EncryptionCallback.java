package toto.cache;

public interface EncryptionCallback {

	public byte[] doEncryption(byte[] plain);

	public byte[] doDecryption(byte[] cipher);
}
