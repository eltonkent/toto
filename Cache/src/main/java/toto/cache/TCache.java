package toto.cache;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import toto.async.AsyncCallback;
import toto.async.AsyncClient;
import toto.async.AsyncOperation;
import toto.util.collections.cache.Cache;
import toto.util.collections.cache.CacheListener;
import android.util.Log;

/**
 * TOTO caching utility that can cache anything anywhere.
 * <p>
 * <div> All caching operations can be performed Synchronous or Asynchronously.
 * There are no duplicate entries and existing values are overwritten. </div>
 * </p>
 * 
 * @author Mobifluence Interactive
 * @param <K>
 *            Type for the cache key
 * @param <V>
 *            Type for the cache value
 */
public class TCache<K, V> implements Cache<K, V> {
	private Storage<K, V> storageProvider;
	private int hits;
	private int misses;

	public final CacheListener<K, V> getCacheListener() {
		return storageProvider.getCacheListener();
	}

	public final void setCacheListener(final CacheListener<K, V> cacheListener) {
		storageProvider.setCacheListener(cacheListener);
	}

	/**
	 * Get the number of items that were fetched from the cache.
	 * 
	 * @return
	 */
	public final int getHits() {
		return hits;
	}

	/**
	 * Get the number of items that were not in the cache.
	 * 
	 * @return
	 */
	public final int getMisses() {
		return misses;
	}

	/**
	 * Perform an operation. The result of which can be cached.
	 * <p>
	 * For instance, a response of content from the Internet could be cached.
	 * </p>
	 * 
	 * @param <V>
	 *            the object to be cached.
	 */
	public static interface CacheOperation<V> {
		public V doOperation();
	}


	/**
	 * Fetch and remove item from cache.
	 * 
	 * @param key
	 * @return
	 */
	public synchronized V take(final K key) {
		final V val = fetch(key);
		if (val != null) {
			remove(key);
		}
		return val;
	}

	/**
	 * 
	 * @param storage
	 * @throws IllegalArgumentException
	 *             if the <code>provider</code> is null
	 */
	public TCache(final Storage<K, V> storage) {
		if (storage == null) {
			throw new IllegalArgumentException("Storage cannot be null");
		}
		this.storageProvider = storage;
	}

	/**
	 * Get value from cache if its available.
	 * 
	 * @param key
	 * @return
	 * @see #fetchAsync(Object, AsyncCallback)
	 */
	public synchronized V fetch(final K key) {
		final V val = storageProvider.get(key);
		if (val != null) {
			hits++;
		} else {
			misses++;
		}
		return val;
	}

	/**
	 * Fetch async if the storage provider is going to take a while to return
	 * the Value.
	 * 
	 * @param key
	 * @param callback
	 * @see #fetch(Object)
	 * @see #fetchThreadSafe(Object)
	 */
	public void fetchAsync(final K key, final AsyncCallback<V> callback) {
		AsyncClient.getReusableInstance().addTask(null,
				new AsyncOperation<V>() {
					@Override
					public boolean isResponseValid(final V t) {
						if (t != null)
							return true;
						return false;
					}

					@Override
					public V doOperation() throws Exception {
						return fetch(key);
					}
				}, callback);
	}

	/**
	 * Destroy cache. Clears cache and removes any back-end storage system.
	 */
	public synchronized void destroy() {
		shutdownEvictionService();
		storageProvider.destroy();
	}

	public void finalize() {
		close();
	}

	/**
	 * Remove the item associated with this <code>key</code>. if any.
	 * 
	 * @param key
	 * @return The <code>value</code> that was removed. Null if the value does
	 *         not exist for the given <code>key</code>
	 */
	public synchronized boolean remove(final K key) {
		if (key == null) {
			return false;
		}
		return storageProvider.remove(key);
	}

	/**
	 * Remove the item associated with this <code>key</code>. if any.
	 * 
	 * @param key
	 * @param callback
	 */
	public void removeAsync(final K key, final AsyncCallback<V> callback) {
		AsyncClient.getReusableInstance().addTask(new AsyncOperation<V>() {

			@Override
			public boolean isResponseValid(final V t) {
				return true;
			}

			@Override
			public V doOperation() throws Exception {
				remove(key);
				return null;
			}
		}, callback);

	}

	/**
	 * Cache the given objects
	 * 
	 * @param key
	 *            cannot be null.
	 * @param value
	 *            cannot be null.
	 * @return true if the cache operation was successful.
	 */
	public synchronized boolean cache(final K key, final V value) {
		if (key == null || value == null) {
			return false;
		}
		final boolean ret = storageProvider.put(key, value);
		doEviction();
		return ret;
	}

	/**
	 * Cache Async the given objects.
	 * 
	 * @param key
	 * @param value
	 * @param callback
	 */
	public void cacheAsync(final K key, final V value,
			final AsyncCallback<Boolean> callback) {
		AsyncClient.getReusableInstance().addTask(null,
				new AsyncOperation<Boolean>() {

					@Override
					public boolean isResponseValid(final Boolean t) {
						return true;
					}

					@Override
					public Boolean doOperation() throws Exception {
						return cache(key, value);
					}
				}, callback);
	}

	/**
	 * Perform an operation and cache.
	 * <p>
	 * {@link #doAndCache(Object, CacheOperation)} first checks if there is an
	 * item available for the given key. If it exists, the item is returned.
	 * Otherwise, the {@link CacheOperation#doOperation()} is called and the
	 * value returned is cached.
	 * </p>
	 * 
	 * @param key
	 * @param operation
	 * @return
	 */
	public boolean doAndCache(final K key, final CacheOperation<V> operation) {
		V val = fetch(key);
		if (val == null) {
			val = operation.doOperation();
			return cache(key, val);
		}
		return false;
	}

	// used for testing
	private long getTimeAdded(final K key) {
		final Storage.EntryMetaInfo info = storageProvider.getEntryMetaInfo(key);
		if (info != null) {
			return info.getTimeAdded();
		}
		return 0;
	}

	// used for testing
	private long getLastAccessed(final K key) {
		final Storage.EntryMetaInfo info = storageProvider.getEntryMetaInfo(key);
		if (info != null) {
			return info.getLastAccessed();
		}
		return 0;
	}

	// used for testing
	private int getTimesAccessed(final K key) {
		final Storage.EntryMetaInfo info = storageProvider.getEntryMetaInfo(key);
		if (info != null) {
			return info.getTimesAccessed();
		}
		return 0;
	}

	/**
	 * Perform an operation asynchronously and cache response.
	 * <p>
	 * {@link #doAndCache(Object, CacheOperation)} first checks if there is an
	 * item available for the given key. If it exists, the item is returned.
	 * Otherwise, the {@link CacheOperation#doOperation()} is called and the
	 * value returned is cached.
	 * </p>
	 * 
	 * @param key
	 * @param operation
	 */
	public void doAndCacheAsync(final android.content.Context context,
			final K key, final CacheOperation<V> operation,
			final AsyncCallback<V> callback) {
		final V val = fetch(key);
		if (val == null) {
			AsyncClient.getReusableInstance().addTask(context,
					new AsyncOperation<V>() {
						@Override
						public boolean isResponseValid(final V t) {
							if (t != null)
								return true;
							return false;
						}

						@Override
						public V doOperation() throws Exception {
							return operation.doOperation();
						}
					}, new AsyncCallback<V>() {

						@Override
						public void onSuccess(final V data) {
							cache(key, val);
							callback.onSuccess(data);
						}

						@Override
						public void onFailure(final V data, final Throwable t) {
							callback.onFailure(data, t);
						}

					});
		}
	}

	/**
	 * Get the current cache storage provider.
	 * 
	 * @return
	 */
	public final Storage<K, V> getStorage() {
		return storageProvider;
	}

	/**
	 * Caching operations are not possible after this method is called.
	 */
	public void close() {
		shutdownEvictionService();
		storageProvider.close();
	}

	private void shutdownEvictionService() {
		evictionStrategy = null;
		evictionService.getQueue().clear();
		evictionService.shutdown();
	}

	/**
	 * Check if the cache is empty.
	 */
	@Override
	public boolean isEmpty() {
		return storageProvider.size() == 0;
	}

	/**
	 * Get the number of entries in this cache.
	 * 
	 * @return number of entries in the cache or 0 if the cache is empty.
	 */
	public int getSize() {
		return storageProvider.size();
	}

	/**
	 * Check if the entry represented by the <code>key</code> is present
	 * 
	 * @param key
	 * @return
	 * @see #containsThreadSafe(Object)
	 */
	@Override
	public synchronized boolean contains(final K key) {
		return storageProvider.get(key) != null;
	}

	/**
	 * Check if the entry represented by the <code>key</code> is present
	 * 
	 * @param key
	 * @return
	 * @see #contains(Object)
	 */
	public synchronized boolean containsThreadSafe(final K key) {
		return contains(key);
	}

	private void doEviction() {
		if (evictionStrategy != null) {
			evictionService.getQueue().clear();
			evictionService.submit(evictionCallable);
		}
	}

	private EvictionStrategy evictionStrategy;

	public final void setEvictionStrategy(
			final EvictionStrategy evictionStrategy) {
		this.evictionStrategy = evictionStrategy;
	}

	public static interface EvictionStrategy {
		public void doEviction();
	}

	/**
	 * All entries older than the given time is removed.
	 * <p>
	 * Removes multiple entries with each eviction cycle.
	 * </p>
	 * 
	 * @author ekent4
	 * 
	 */
	public final class OlderThanEviction implements EvictionStrategy {

		long time;

		/**
		 * Time in milliseconds the entries should be older than.
		 * <p>
		 * For instance, if you want to remove entries older than one day then
		 * <code>time</code> is 86400000 (86400000 = 1day).
		 * </p>
		 * 
		 * @param time
		 */
		public OlderThanEviction(final long time) {
			this.time = time;
		}

		@Override
		public void doEviction() {
			final Map<K, Storage<K, V>.EntryMetaInfo> meta = storageProvider
					.getAllEntriesMetaInfo();
			Log.d("ToTo", "number of entries for eviction :" + meta.size());
			if (meta != null) {
				final long currentTime = System.currentTimeMillis();
				int evitionCount = 0;
				for (final Map.Entry<K, Storage<K, V>.EntryMetaInfo> entry : meta
						.entrySet()) {
					if ((currentTime - entry.getValue().getTimeAdded()) > time) {
						remove(entry.getKey());
						evitionCount++;
					}
				}
				Log.d("ToTo", "Number of entries evicted: " + evitionCount);
			}
		}
	}

	/**
	 * Removes the oldest entry with every eviction cycle.
	 * <p>
	 * Only one entry is removed.
	 * </p>
	 * 
	 * @author ekent4
	 * 
	 */
	public final class OldestEviction implements EvictionStrategy {

		int maxSize;

		/**
		 * Max number of entries before the oldest entry is evicted.
		 * 
		 * @param maxSize
		 */
		public OldestEviction(final int maxSize) {
			this.maxSize = maxSize;
		}

		@Override
		public void doEviction() {
			if (storageProvider.size() > maxSize) {
				final Map<K, Storage<K, V>.EntryMetaInfo> meta = storageProvider
						.getAllEntriesMetaInfo();
				long oldest = Long.MAX_VALUE;
				K key = null;
				final Set<Entry<K, Storage<K, V>.EntryMetaInfo>> set = meta
						.entrySet();
				for (final Entry<K, Storage<K, V>.EntryMetaInfo> entry : set) {
					if (entry.getValue().getTimeAdded() < oldest) {
						oldest = entry.getValue().getTimeAdded();
						key = entry.getKey();
					}
				}
				if (key != null) {
					remove(key);
				}
			}
		}
	}

	/**
	 * LRU eviction removes the least used entry in the cache if the size
	 * exceeds the maximum size specified.
	 * <p>
	 * Removes only one entry with each eviction cycle.
	 * </p>
	 * 
	 * @see LRUEviction#LRUEviction(int)
	 * @see #
	 * @author Elton Kent
	 * 
	 */
	public final class LRUEviction implements EvictionStrategy {

		int maxSize;

		/**
		 * Max number of entries for LRU eviction.
		 * 
		 * @param maxSize
		 */
		public LRUEviction(final int maxSize) {
			this.maxSize = maxSize;
		}

		@Override
		public void doEviction() {
			if (storageProvider.size() > maxSize) {
				final Map<K, Storage<K, V>.EntryMetaInfo> meta = storageProvider
						.getAllEntriesMetaInfo();
				if (meta != null) {
					K key = null;

					int accessedTimes = 1;

					for (final Map.Entry<K, Storage<K, V>.EntryMetaInfo> entry : meta
							.entrySet()) {
						if (entry.getValue().getTimesAccessed() < accessedTimes) {
							accessedTimes = entry.getValue().getTimesAccessed();
							key = entry.getKey();
						}
					}
					if (key != null) {
						remove(key);
					}
				}
			}
		}
	}

	/**
	 * Biggest Entry eviction is a combination of {@link LRUEviction} and
	 * {@link OlderThanEviction}. It adds a third factor for eviction called the
	 * value size (in bytes)
	 * 
	 * @author Elton Kent
	 * 
	 */
	public final class BiggestEntriesEviction implements EvictionStrategy {
		private final int maxSize;
		private final long time;
		private final int biggerThanBytes;

		/**
		 * 
		 * @param maxSize
		 * @param time
		 * @param biggerThanBytes
		 *            remove entries bigger than
		 */
		public BiggestEntriesEviction(final int maxSize, final long time,
				final int biggerThanBytes) {
			this.maxSize = maxSize;
			this.time = time;
			this.biggerThanBytes = biggerThanBytes;
		}

		@Override
		public void doEviction() {
			if (storageProvider.size() > maxSize) {
				final Map<K, Storage<K, V>.EntryMetaInfo> entries = storageProvider
						.getAllEntriesMetaInfo();
				// find the biggest and evct
				// storageProvider.remove(null);
				Storage.EntryMetaInfo meta;
				final long currentTime = System.currentTimeMillis();
				int entriesRemoved = 0;
				if (entries != null) {
					for (final Map.Entry<K, Storage<K, V>.EntryMetaInfo> entry : entries
							.entrySet()) {
						meta = entry.getValue();
						if (meta.getValueSizeInBytes() > biggerThanBytes
								&& (currentTime - entry.getValue()
										.getTimeAdded()) > time) {
							remove(entry.getKey());
							entriesRemoved++;
						}
					}
					Log.d("ToTo", "Number of entries evicted: "
							+ entriesRemoved);
				}
			}
		}
	}

	/** This cache uses a single background thread to evict entries. */
	private final ThreadPoolExecutor evictionService = new ThreadPoolExecutor(
			0, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
	private final Callable<Void> evictionCallable = new Callable<Void>() {
		public Void call() throws Exception {
			synchronized (TCache.this) {
				evictionStrategy.doEviction();
			}
			return null;
		}
	};
}
