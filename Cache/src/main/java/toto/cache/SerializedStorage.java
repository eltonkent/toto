package toto.cache;

import com.mi.toto.ToTo;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import toto.lang.IntegerUtils;
import toto.lang.LongUtils;
import toto.log.Logger;

/**
 * Base class for storage providers that require cache entries to be converted
 * to byte[] and back.
 * 
 * @author Mobifluence Interactive
 * 
 */
public abstract class SerializedStorage<K, V> extends Storage<K, V> {
	protected final SerializedStorageConverter<K> keyConverter;
	protected final SerializedStorageConverter<V> valueConverter;

	public SerializedStorage(final SerializedStorageConverter<K> keyConverter,
							 final SerializedStorageConverter<V> valueConverter) {
		this.keyConverter = keyConverter;
		this.valueConverter = valueConverter;
	}

	@Override
	protected boolean put(final K key, final V value) {
		final byte[] bKey = keyConverter.from(key);
		final byte[] bValue = valueConverter.from(value);
		if (bKey == null || bValue == null) {
			Logger.getDefault().d(
                    "Cache: could not insert value(s) into cache. Key:" + key
                            + " Value:" + value);
			return false;
		}
		final boolean added = save(bKey, bValue);
		if (added && getCacheListener() != null) {
			getCacheListener().onEntryAdded(key, value);
		}
		return added;
	}

	@Override
	protected boolean remove(final K key) {
		final byte[] keyData = keyConverter.from(key);
		if (keyData == null) {
            Logger.getDefault().d(
					"Cache: could not remove value(s) from cache. Key:" + key);
			return false;
		}
		final boolean removed = delete(keyData);
		if (removed && getCacheListener() != null) {
			getCacheListener().onEntryRemoved(key);
		}
		return removed;
	}

	protected V get(final K key) {
		final byte[] keyData = keyConverter.from(key);
		if (keyData == null) {
			return null;
		}
		V val = null;
		final byte[] valData = fetch(keyData);
		if (valData != null) {
			val = valueConverter.to(valData);
		}
		return val;
	}

	/**
	 * read {@link EntryMetaInfo} from a inputstream the stream. is not closed
	 * after the data is read.
	 * 
	 * @param is
	 * @return
	 */
	protected final EntryMetaInfo readMetaInfo(final DataInput is) {
		final byte[] longData = new byte[8];
		/* read time accessed */
		final EntryMetaInfo info = new EntryMetaInfo();
		try {
			/* read number of times accessed */
			is.readFully(longData, 0, 4);
			info.setTimesAccessed(IntegerUtils.fromBytes(longData));
			/* read last accessed on */
			is.readFully(longData);
			info.setLastAccessed(LongUtils.fromBytes(longData));
			/* read time added */
			is.readFully(longData);
			info.setTimeAdded(LongUtils.fromBytes(longData));
			/* value size */
			is.readFully(longData, 0, 4);
			info.setValueSizeInBytes(IntegerUtils.fromBytes(longData));
		} catch (final IOException e) {
			ToTo.logException(e);
			return null;
		}
		return info;
	}

	protected final void writeMetaInfo(final EntryMetaInfo info,
			final DataOutput os) throws IOException {
		final byte[] longBytes = new byte[8];
		/* write number of times accessed */
		IntegerUtils.toBytes(info.getTimesAccessed(), longBytes);
		os.write(longBytes, 0, 4);
		/* write last accessed on */
		LongUtils.toBytes(info.getLastAccessed(), longBytes);
		os.write(longBytes);
		/* read time added */
		LongUtils.toBytes(info.getTimeAdded(), longBytes);
		os.write(longBytes);
		/* value size */
		IntegerUtils.toBytes(info.getTimesAccessed(), longBytes);
		os.write(longBytes, 0, 4);
	}

	protected abstract boolean save(final byte[] key, final byte[] value);

	protected abstract boolean delete(final byte[] key);

	protected abstract byte[] fetch(final byte[] key);

}
