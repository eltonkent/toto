package toto.cache;

import java.util.Map;

import toto.util.collections.cache.CacheListener;

public abstract class Storage<K, V> {

	/**
	 * Information attached to every entry in the cache.
	 * 
	 */
	public final class EntryMetaInfo {

		/**
		 * number of times accessed
		 */
		private long timeAdded;
		/**
		 * number of times accessed
		 */
		private int timesAccessed;
		/**
		 * size of value entry
		 */
		private int valueSizeInBytes;
		/**
		 * time it was last accessed.
		 */
		private long lastAccessed;

		public final long getLastAccessed() {
			return lastAccessed;
		}

		public final void setLastAccessed(final long lastAccessed) {
			this.lastAccessed = lastAccessed;
		}

		public EntryMetaInfo() {

		}

		public long getTimeAdded() {
			return timeAdded;
		}

		public int getTimesAccessed() {
			return timesAccessed;
		}

		public int getValueSizeInBytes() {
			return valueSizeInBytes;
		}

		public void setTimeAdded(final long timeAdded) {
			this.timeAdded = timeAdded;
		}

		public void setTimesAccessed(final int timesAccessed) {
			this.timesAccessed = timesAccessed;
		}

		public void setValueSizeInBytes(final int valueSizeInBytes) {
			this.valueSizeInBytes = valueSizeInBytes;
		}
	}

	private CacheListener<K, V> cacheListener;

	/**
	 * Remove all items from the cache.
	 */
	protected abstract void clear();

	/**
	 * Closes the cache.
	 */
	protected abstract void close();

	/**
	 * Clears the cache and destroys storage backend used by the storage
	 * provider.
	 */
	protected abstract void destroy();

	protected abstract V get(final K key);

	protected final CacheListener<K, V> getCacheListener() {
		return cacheListener;
	}

	protected abstract EntryMetaInfo getEntryMetaInfo(K key);

	protected abstract Map<K, EntryMetaInfo> getAllEntriesMetaInfo();

	protected abstract boolean put(K key, V value);

	protected abstract boolean remove(final K key);

	protected final void setCacheListener(
			final CacheListener<K, V> cacheListener) {
		this.cacheListener = cacheListener;
	}

	/**
	 * 
	 * @return
	 */
	protected abstract int size();

}
