package toto.barcode;

import com.mi.toto.ToTo;

class BarcodeReader {
	static native String process(int width, int height, byte[] imgData);

	static {
		ToTo.loadLib("DI_BarcodeReader");
	}
}
