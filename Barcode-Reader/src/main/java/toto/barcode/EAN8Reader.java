package toto.barcode;

import toto.util.collections.list.BitArray;

public final class EAN8Reader extends UPCEANReader {

	private final int[] decodeMiddleCounters;

	public EAN8Reader() {
		decodeMiddleCounters = new int[4];
	}

	@Override
	protected int decodeMiddle(final BitArray row, final int[] startRange,
			final StringBuilder result) throws BarcodeNotFoundException {
		final int[] counters = decodeMiddleCounters;
		counters[0] = 0;
		counters[1] = 0;
		counters[2] = 0;
		counters[3] = 0;
		final int end = row.getSize();
		int rowOffset = startRange[1];

		for (int x = 0; x < 4 && rowOffset < end; x++) {
			final int bestMatch = decodeDigit(row, counters, rowOffset,
					L_PATTERNS);
			result.append((char) ('0' + bestMatch));
			for (final int counter : counters) {
				rowOffset += counter;
			}
		}

		final int[] middleRange = findGuardPattern(row, rowOffset, true,
				MIDDLE_PATTERN);
		rowOffset = middleRange[1];

		for (int x = 0; x < 4 && rowOffset < end; x++) {
			final int bestMatch = decodeDigit(row, counters, rowOffset,
					L_PATTERNS);
			result.append((char) ('0' + bestMatch));
			for (final int counter : counters) {
				rowOffset += counter;
			}
		}

		return rowOffset;
	}

	@Override
	BarcodeType getBarcodeFormat() {
		return BarcodeType.EAN_8;
	}

}