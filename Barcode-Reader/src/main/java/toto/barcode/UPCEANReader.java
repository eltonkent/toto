package toto.barcode;

import toto.util.collections.list.BitArray;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;

/**
 * <p>
 * Encapsulates functionality and implementation that is common to UPC and EAN
 * families of one-dimensional barcodes.
 * </p>
 * 
 */
abstract class UPCEANReader extends OneDReader {

	// These two values are critical for determining how permissive the decoding
	// will be.
	// We've arrived at these values through a lot of trial and error. Setting
	// them any higher
	// lets false positives creep in quickly.
	private static final int MAX_AVG_VARIANCE = (int) (PATTERN_MATCH_RESULT_SCALE_FACTOR * 0.48f);
	private static final int MAX_INDIVIDUAL_VARIANCE = (int) (PATTERN_MATCH_RESULT_SCALE_FACTOR * 0.7f);

	/**
	 * Start/end guard pattern.
	 */
	static final int[] START_END_PATTERN = { 1, 1, 1, };

	/**
	 * Pattern marking the middle of a UPC/EAN pattern, separating the two
	 * halves.
	 */
	static final int[] MIDDLE_PATTERN = { 1, 1, 1, 1, 1 };

	/**
	 * "Odd", or "L" patterns used to encode UPC/EAN digits.
	 */
	static final int[][] L_PATTERNS = { { 3, 2, 1, 1 }, // 0
			{ 2, 2, 2, 1 }, // 1
			{ 2, 1, 2, 2 }, // 2
			{ 1, 4, 1, 1 }, // 3
			{ 1, 1, 3, 2 }, // 4
			{ 1, 2, 3, 1 }, // 5
			{ 1, 1, 1, 4 }, // 6
			{ 1, 3, 1, 2 }, // 7
			{ 1, 2, 1, 3 }, // 8
			{ 3, 1, 1, 2 } // 9
	};

	/**
	 * As above but also including the "even", or "G" patterns used to encode
	 * UPC/EAN digits.
	 */
	static final int[][] L_AND_G_PATTERNS;

	static {
		L_AND_G_PATTERNS = new int[20][];
		System.arraycopy(L_PATTERNS, 0, L_AND_G_PATTERNS, 0, 10);
		for (int i = 10; i < 20; i++) {
			int[] widths = L_PATTERNS[i - 10];
			int[] reversedWidths = new int[widths.length];
			for (int j = 0; j < widths.length; j++) {
				reversedWidths[j] = widths[widths.length - j - 1];
			}
			L_AND_G_PATTERNS[i] = reversedWidths;
		}
	}

	private final StringBuilder decodeRowStringBuffer;
	private final UPCEANExtensionSupport extensionReader;
	private final EANManufacturerOrgSupport eanManSupport;

	protected UPCEANReader() {
		decodeRowStringBuffer = new StringBuilder(20);
		extensionReader = new UPCEANExtensionSupport();
		eanManSupport = new EANManufacturerOrgSupport();
	}

	static int[] findStartGuardPattern(BitArray row)
			throws BarcodeNotFoundException {
		boolean foundStart = false;
		int[] startRange = null;
		int nextStart = 0;
		int[] counters = new int[START_END_PATTERN.length];
		while (!foundStart) {
			Arrays.fill(counters, 0, START_END_PATTERN.length, 0);
			startRange = findGuardPattern(row, nextStart, false,
					START_END_PATTERN, counters);
			int start = startRange[0];
			nextStart = startRange[1];
			// Make sure there is a quiet zone at least as big as the start
			// pattern before the barcode.
			// If this check would run off the left edge of the image, do not
			// accept this barcode,
			// as it is very likely to be a false positive.
			int quietStart = start - (nextStart - start);
			if (quietStart >= 0) {
				foundStart = row.isRange(quietStart, start, false);
			}
		}
		return startRange;
	}

	@Override
	Result decodeRow(int rowNumber, BitArray row, Map<DecodeHintType, ?> hints)
			throws BarcodeNotFoundException, BarCodeChecksumException,
			FormatException {
		return decodeRow(rowNumber, row, findStartGuardPattern(row), hints);
	}

	/**
	 * <p>
	 * Like {@link #decodeRow(int, BitArray, Map)}, but allows caller
	 * to inform method about where the UPC/EAN start pattern is found. This
	 * allows this to be computed once and reused across many implementations.
	 * </p>
	 */
	Result decodeRow(int rowNumber, BitArray row, int[] startGuardRange,
			Map<DecodeHintType, ?> hints) throws BarcodeNotFoundException,
			BarCodeChecksumException, FormatException {

		ResultPointCallback resultPointCallback = hints == null ? null
				: (ResultPointCallback) hints
						.get(DecodeHintType.NEED_RESULT_POINT_CALLBACK);

		if (resultPointCallback != null) {
			resultPointCallback
					.foundPossibleResultPoint(new ResultPoint(
							(startGuardRange[0] + startGuardRange[1]) / 2.0f,
							rowNumber));
		}

		StringBuilder result = decodeRowStringBuffer;
		result.setLength(0);
		int endStart = decodeMiddle(row, startGuardRange, result);

		if (resultPointCallback != null) {
			resultPointCallback.foundPossibleResultPoint(new ResultPoint(
					endStart, rowNumber));
		}

		int[] endRange = decodeEnd(row, endStart);

		if (resultPointCallback != null) {
			resultPointCallback.foundPossibleResultPoint(new ResultPoint(
					(endRange[0] + endRange[1]) / 2.0f, rowNumber));
		}

		// Make sure there is a quiet zone at least as big as the end pattern
		// after the barcode. The
		// spec might want more whitespace, but in practice this is the maximum
		// we can count on.
		int end = endRange[1];
		int quietEnd = end + (end - endRange[0]);
		if (quietEnd >= row.getSize() || !row.isRange(end, quietEnd, false)) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		String resultString = result.toString();
		// UPC/EAN should never be less than 8 chars anyway
		if (resultString.length() < 8) {
			throw FormatException.getFormatInstance();
		}
		if (!checkChecksum(resultString)) {
			throw BarCodeChecksumException.getChecksumInstance();
		}

		float left = (float) (startGuardRange[1] + startGuardRange[0]) / 2.0f;
		float right = (float) (endRange[1] + endRange[0]) / 2.0f;
		BarcodeType format = getBarcodeFormat();
		Result decodeResult = new Result(resultString, null, // no natural byte
																// representation
																// for these
																// barcodes
				new ResultPoint[] { new ResultPoint(left, (float) rowNumber),
						new ResultPoint(right, (float) rowNumber) }, format);

		try {
			Result extensionResult = extensionReader.decodeRow(rowNumber, row,
					endRange[1]);
			decodeResult.putMetadata(ResultMetadataType.UPC_EAN_EXTENSION,
					extensionResult.getText());
			decodeResult.putAllMetadata(extensionResult.getResultMetadata());
			decodeResult.addResultPoints(extensionResult.getResultPoints());
		} catch (ReaderException re) {
			// continue
		}

		if (format == BarcodeType.EAN_13 || format == BarcodeType.UPC_A) {
			String countryID = eanManSupport
					.lookupCountryIdentifier(resultString);
			if (countryID != null) {
				decodeResult.putMetadata(ResultMetadataType.POSSIBLE_COUNTRY,
						countryID);
			}
		}

		return decodeResult;
	}

	/**
	 * @return {@link #checkStandardUPCEANChecksum(CharSequence)}
	 */
	boolean checkChecksum(String s) throws BarCodeChecksumException,
			FormatException {
		return checkStandardUPCEANChecksum(s);
	}

	/**
	 * Computes the UPC/EAN checksum on a string of digits, and reports whether
	 * the checksum is correct or not.
	 *
	 * @param s
	 *            string of digits to check
	 * @return true iff string of digits passes the UPC/EAN checksum algorithm
	 * @throws FormatException
	 *             if the string does not contain only digits
	 */
	static boolean checkStandardUPCEANChecksum(CharSequence s)
			throws FormatException {
		int length = s.length();
		if (length == 0) {
			return false;
		}

		int sum = 0;
		for (int i = length - 2; i >= 0; i -= 2) {
			int digit = (int) s.charAt(i) - (int) '0';
			if (digit < 0 || digit > 9) {
				throw FormatException.getFormatInstance();
			}
			sum += digit;
		}
		sum *= 3;
		for (int i = length - 1; i >= 0; i -= 2) {
			int digit = (int) s.charAt(i) - (int) '0';
			if (digit < 0 || digit > 9) {
				throw FormatException.getFormatInstance();
			}
			sum += digit;
		}
		return sum % 10 == 0;
	}

	int[] decodeEnd(BitArray row, int endStart) throws BarcodeNotFoundException {
		return findGuardPattern(row, endStart, false, START_END_PATTERN);
	}

	static int[] findGuardPattern(BitArray row, int rowOffset,
			boolean whiteFirst, int[] pattern) throws BarcodeNotFoundException {
		return findGuardPattern(row, rowOffset, whiteFirst, pattern,
				new int[pattern.length]);
	}

	/**
	 * @param row
	 *            row of black/white values to search
	 * @param rowOffset
	 *            position to start search
	 * @param whiteFirst
	 *            if true, indicates that the pattern specifies
	 *            white/black/white/... pixel counts, otherwise, it is
	 *            interpreted as black/white/black/...
	 * @param pattern
	 *            pattern of counts of number of black and white pixels that are
	 *            being searched for as a pattern
	 * @param counters
	 *            array of counters, as long as pattern, to re-use
	 * @return start/end horizontal offset of guard pattern, as an array of two
	 *         ints
	 * @throws BarcodeNotFoundException
	 *             if pattern is not found
	 */
	private static int[] findGuardPattern(BitArray row, int rowOffset,
			boolean whiteFirst, int[] pattern, int[] counters)
			throws BarcodeNotFoundException {
		int patternLength = pattern.length;
		int width = row.getSize();
		boolean isWhite = whiteFirst;
		rowOffset = whiteFirst ? row.getNextUnset(rowOffset) : row
				.getNextSet(rowOffset);
		int counterPosition = 0;
		int patternStart = rowOffset;
		for (int x = rowOffset; x < width; x++) {
			if (row.get(x) ^ isWhite) {
				counters[counterPosition]++;
			} else {
				if (counterPosition == patternLength - 1) {
					if (patternMatchVariance(counters, pattern,
							MAX_INDIVIDUAL_VARIANCE) < MAX_AVG_VARIANCE) {
						return new int[] { patternStart, x };
					}
					patternStart += counters[0] + counters[1];
					System.arraycopy(counters, 2, counters, 0,
							patternLength - 2);
					counters[patternLength - 2] = 0;
					counters[patternLength - 1] = 0;
					counterPosition--;
				} else {
					counterPosition++;
				}
				counters[counterPosition] = 1;
				isWhite = !isWhite;
			}
		}
		throw BarcodeNotFoundException.getNotFoundInstance();
	}

	/**
	 * Attempts to decode a single UPC/EAN-encoded digit.
	 *
	 * @param row
	 *            row of black/white values to decode
	 * @param counters
	 *            the counts of runs of observed black/white/black/... values
	 * @param rowOffset
	 *            horizontal offset to start decoding from
	 * @param patterns
	 *            the setKey of patterns to use to decode -- sometimes different
	 *            encodings for the digits 0-9 are used, and this indicates the
	 *            encodings for 0 to 9 that should be used
	 * @return horizontal offset of first pixel beyond the decoded digit
	 * @throws BarcodeNotFoundException
	 *             if digit cannot be decoded
	 */
	static int decodeDigit(BitArray row, int[] counters, int rowOffset,
			int[][] patterns) throws BarcodeNotFoundException {
		recordPattern(row, rowOffset, counters);
		int bestVariance = MAX_AVG_VARIANCE; // worst variance we'll accept
		int bestMatch = -1;
		int max = patterns.length;
		for (int i = 0; i < max; i++) {
			int[] pattern = patterns[i];
			int variance = patternMatchVariance(counters, pattern,
					MAX_INDIVIDUAL_VARIANCE);
			if (variance < bestVariance) {
				bestVariance = variance;
				bestMatch = i;
			}
		}
		if (bestMatch >= 0) {
			return bestMatch;
		} else {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}
	}

	/**
	 * Get the format of this decoder.
	 *
	 * @return The 1D format.
	 */
	abstract BarcodeType getBarcodeFormat();

	/**
	 * Subclasses override this to decode the portion of a barcode between the
	 * start and end guard patterns.
	 *
	 * @param row
	 *            row of black/white values to search
	 * @param startRange
	 *            start/end offset of start guard pattern
	 * @param resultString
	 *            {@link StringBuilder} to append decoded chars to
	 * @return horizontal offset of first pixel after the "middle" that was
	 *         decoded
	 * @throws BarcodeNotFoundException
	 *             if decoding could not complete successfully
	 */
	protected abstract int decodeMiddle(BitArray row, int[] startRange,
			StringBuilder resultString) throws BarcodeNotFoundException;

	static final class UPCEANExtensionSupport {

		private static final int[] EXTENSION_START_PATTERN = { 1, 1, 2 };

		private final UPCEANExtension2Support twoSupport = new UPCEANExtension2Support();
		private final UPCEANExtension5Support fiveSupport = new UPCEANExtension5Support();

		Result decodeRow(int rowNumber, BitArray row, int rowOffset)
				throws BarcodeNotFoundException {
			int[] extensionStartRange = UPCEANReader.findGuardPattern(row,
					rowOffset, false, EXTENSION_START_PATTERN);
			try {
				return fiveSupport.decodeRow(rowNumber, row,
						extensionStartRange);
			} catch (ReaderException ignored) {
				return twoSupport
						.decodeRow(rowNumber, row, extensionStartRange);
			}
		}

		/**
		 * @see UPCEANExtension2Support
		 */
		static final class UPCEANExtension5Support {

			private static final int[] CHECK_DIGIT_ENCODINGS = { 0x18, 0x14,
					0x12, 0x11, 0x0C, 0x06, 0x03, 0x0A, 0x09, 0x05 };

			private final int[] decodeMiddleCounters = new int[4];
			private final StringBuilder decodeRowStringBuffer = new StringBuilder();

			Result decodeRow(int rowNumber, BitArray row,
					int[] extensionStartRange) throws BarcodeNotFoundException {

				StringBuilder result = decodeRowStringBuffer;
				result.setLength(0);
				int end = decodeMiddle(row, extensionStartRange, result);

				String resultString = result.toString();
				Map<ResultMetadataType, Object> extensionData = parseExtensionString(resultString);

				Result extensionResult = new Result(
						resultString,
						null,
						new ResultPoint[] {
								new ResultPoint(
										(extensionStartRange[0] + extensionStartRange[1]) / 2.0f,
										(float) rowNumber),
								new ResultPoint((float) end, (float) rowNumber), },
						BarcodeType.UPC_EAN_EXTENSION);
				if (extensionData != null) {
					extensionResult.putAllMetadata(extensionData);
				}
				return extensionResult;
			}

			int decodeMiddle(BitArray row, int[] startRange,
					StringBuilder resultString) throws BarcodeNotFoundException {
				int[] counters = decodeMiddleCounters;
				counters[0] = 0;
				counters[1] = 0;
				counters[2] = 0;
				counters[3] = 0;
				int end = row.getSize();
				int rowOffset = startRange[1];

				int lgPatternFound = 0;

				for (int x = 0; x < 5 && rowOffset < end; x++) {
					int bestMatch = UPCEANReader.decodeDigit(row, counters,
							rowOffset, UPCEANReader.L_AND_G_PATTERNS);
					resultString.append((char) ('0' + bestMatch % 10));
					for (int counter : counters) {
						rowOffset += counter;
					}
					if (bestMatch >= 10) {
						lgPatternFound |= 1 << (4 - x);
					}
					if (x != 4) {
						// Read off separator if not last
						rowOffset = row.getNextSet(rowOffset);
						rowOffset = row.getNextUnset(rowOffset);
					}
				}

				if (resultString.length() != 5) {
					throw BarcodeNotFoundException.getNotFoundInstance();
				}

				int checkDigit = determineCheckDigit(lgPatternFound);
				if (extensionChecksum(resultString.toString()) != checkDigit) {
					throw BarcodeNotFoundException.getNotFoundInstance();
				}

				return rowOffset;
			}

			private static int extensionChecksum(CharSequence s) {
				int length = s.length();
				int sum = 0;
				for (int i = length - 2; i >= 0; i -= 2) {
					sum += (int) s.charAt(i) - (int) '0';
				}
				sum *= 3;
				for (int i = length - 1; i >= 0; i -= 2) {
					sum += (int) s.charAt(i) - (int) '0';
				}
				sum *= 3;
				return sum % 10;
			}

			private static int determineCheckDigit(int lgPatternFound)
					throws BarcodeNotFoundException {
				for (int d = 0; d < 10; d++) {
					if (lgPatternFound == CHECK_DIGIT_ENCODINGS[d]) {
						return d;
					}
				}
				throw BarcodeNotFoundException.getNotFoundInstance();
			}

			/**
			 * @param raw
			 *            raw content of extension
			 * @return formatted interpretation of raw content as a
			 *         {@link Map} mapping one
			 *         {@link ResultMetadataType} to appropriate value, or
			 *         {@code null} if not known
			 */
			private static Map<ResultMetadataType, Object> parseExtensionString(
					String raw) {
				if (raw.length() != 5) {
					return null;
				}
				Object value = parseExtension5String(raw);
				if (value == null) {
					return null;
				}
				Map<ResultMetadataType, Object> result = new EnumMap<ResultMetadataType, Object>(
						ResultMetadataType.class);
				result.put(ResultMetadataType.SUGGESTED_PRICE, value);
				return result;
			}

			private static String parseExtension5String(String raw) {
				String currency;
				switch (raw.charAt(0)) {
				case '0':
					currency = "£";
					break;
				case '5':
					currency = "$";
					break;
				case '9':
					// Reference: http://www.jollytech.com
					if ("90000".equals(raw)) {
						// No suggested retail price
						return null;
					}
					if ("99991".equals(raw)) {
						// Complementary
						return "0.00";
					}
					if ("99990".equals(raw)) {
						return "Used";
					}
					// Otherwise... unknown currency?
					currency = "";
					break;
				default:
					currency = "";
					break;
				}
				int rawAmount = Integer.parseInt(raw.substring(1));
				String unitsString = String.valueOf(rawAmount / 100);
				int hundredths = rawAmount % 100;
				String hundredthsString = hundredths < 10 ? "0" + hundredths
						: String.valueOf(hundredths);
				return currency + unitsString + '.' + hundredthsString;
			}

		}

		static final class UPCEANExtension2Support {

			private final int[] decodeMiddleCounters = new int[4];
			private final StringBuilder decodeRowStringBuffer = new StringBuilder();

			Result decodeRow(int rowNumber, BitArray row,
					int[] extensionStartRange) throws BarcodeNotFoundException {

				StringBuilder result = decodeRowStringBuffer;
				result.setLength(0);
				int end = decodeMiddle(row, extensionStartRange, result);

				String resultString = result.toString();
				Map<ResultMetadataType, Object> extensionData = parseExtensionString(resultString);

				Result extensionResult = new Result(
						resultString,
						null,
						new ResultPoint[] {
								new ResultPoint(
										(extensionStartRange[0] + extensionStartRange[1]) / 2.0f,
										(float) rowNumber),
								new ResultPoint((float) end, (float) rowNumber), },
						BarcodeType.UPC_EAN_EXTENSION);
				if (extensionData != null) {
					extensionResult.putAllMetadata(extensionData);
				}
				return extensionResult;
			}

			int decodeMiddle(BitArray row, int[] startRange,
					StringBuilder resultString) throws BarcodeNotFoundException {
				int[] counters = decodeMiddleCounters;
				counters[0] = 0;
				counters[1] = 0;
				counters[2] = 0;
				counters[3] = 0;
				int end = row.getSize();
				int rowOffset = startRange[1];

				int checkParity = 0;

				for (int x = 0; x < 2 && rowOffset < end; x++) {
					int bestMatch = UPCEANReader.decodeDigit(row, counters,
							rowOffset, UPCEANReader.L_AND_G_PATTERNS);
					resultString.append((char) ('0' + bestMatch % 10));
					for (int counter : counters) {
						rowOffset += counter;
					}
					if (bestMatch >= 10) {
						checkParity |= 1 << (1 - x);
					}
					if (x != 1) {
						// Read off separator if not last
						rowOffset = row.getNextSet(rowOffset);
						rowOffset = row.getNextUnset(rowOffset);
					}
				}

				if (resultString.length() != 2) {
					throw BarcodeNotFoundException.getNotFoundInstance();
				}

				if (Integer.parseInt(resultString.toString()) % 4 != checkParity) {
					throw BarcodeNotFoundException.getNotFoundInstance();
				}

				return rowOffset;
			}

			/**
			 * @param raw
			 *            raw content of extension
			 * @return formatted interpretation of raw content as a
			 *         {@link Map} mapping one
			 *         {@link ResultMetadataType} to appropriate value, or
			 *         {@code null} if not known
			 */
			private static Map<ResultMetadataType, Object> parseExtensionString(
					String raw) {
				if (raw.length() != 2) {
					return null;
				}
				Map<ResultMetadataType, Object> result = new EnumMap<ResultMetadataType, Object>(
						ResultMetadataType.class);
				result.put(ResultMetadataType.ISSUE_NUMBER,
						Integer.valueOf(raw));
				return result;
			}

		}

	}

}
