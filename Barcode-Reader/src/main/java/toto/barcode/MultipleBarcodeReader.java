package toto.barcode;

import toto.bitmap.BinaryBitmap;

import java.util.Map;

/**
 * Implementation of this interface attempt to read several barcodes from one
 * image.
 * 
 */
interface MultipleBarcodeReader {

	Result[] decodeMultiple(BinaryBitmap image) throws BarcodeNotFoundException;

	Result[] decodeMultiple(BinaryBitmap image, Map<DecodeHintType, ?> hints)
			throws BarcodeNotFoundException;

}
