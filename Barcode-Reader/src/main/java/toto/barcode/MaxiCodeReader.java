package toto.barcode;

import toto.bitmap.BinaryBitmap;
import toto.bitmap.ToToBitmapException;
import toto.util.collections.matrix.BitMatrix;

import java.util.Map;

/**
 * This implementation can detect and decode a MaxiCode in an image.
 */
public final class MaxiCodeReader implements Reader {

	private static final ResultPoint[] NO_POINTS = new ResultPoint[0];
	private static final int MATRIX_WIDTH = 30;
	private static final int MATRIX_HEIGHT = 33;

	private final MaxicodeDecoder decoder = new MaxicodeDecoder();

	/*
	 * Decoder getDecoder() { return decoder; }
	 */

	/**
	 * Locates and decodes a MaxiCode in an image.
	 * 
	 * @return a String representing the content encoded by the MaxiCode
	 * @throws BarcodeNotFoundException
	 *             if a MaxiCode cannot be found
	 * @throws FormatException
	 *             if a MaxiCode cannot be decoded
	 * @throws BarCodeChecksumException
	 *             if error correction fails
	 */
	@Override
	public Result decode(BinaryBitmap image) throws BarcodeNotFoundException,
			BarCodeChecksumException, FormatException {
		return decode(image, null);
	}

	@Override
	public Result decode(BinaryBitmap image, Map<DecodeHintType, ?> hints)
			throws BarcodeNotFoundException, BarCodeChecksumException,
			FormatException {
		DecoderResult decoderResult;
		if (hints != null && hints.containsKey(DecodeHintType.PURE_BARCODE)) {
			BitMatrix bits = null;
			try {
				bits = extractPureBits(image.getBlackMatrix());
			} catch (ToToBitmapException e) {
				e.printStackTrace();
				throw  new BarcodeNotFoundException();
			}
			decoderResult = decoder.decode(bits, hints);
		} else {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		ResultPoint[] points = NO_POINTS;
		Result result = new Result(decoderResult.getText(),
				decoderResult.getRawBytes(), points, BarcodeType.MAXICODE);

		String ecLevel = decoderResult.getECLevel();
		if (ecLevel != null) {
			result.putMetadata(ResultMetadataType.ERROR_CORRECTION_LEVEL,
					ecLevel);
		}
		return result;
	}

	@Override
	public void reset() {
		// do nothing
	}

	/**
	 * This method detects a code in a "pure" image -- that is, pure monochrome
	 * image which contains only an unrotated, unskewed, image of a code, with
	 * some white border around it. This is a specialized method that works
	 * exceptionally fast in this special case.
	 * 
	 * @see .DataMatrixReader#extractPureBits(BitMatrix)
	 * @see QRCodeReader#extractPureBits(BitMatrix)
	 */
	private static BitMatrix extractPureBits(BitMatrix image)
			throws BarcodeNotFoundException {

		int[] enclosingRectangle = image.getEnclosingRectangle();
		if (enclosingRectangle == null) {
			throw BarcodeNotFoundException.getNotFoundInstance();
		}

		int left = enclosingRectangle[0];
		int top = enclosingRectangle[1];
		int width = enclosingRectangle[2];
		int height = enclosingRectangle[3];

		// Now just read off the bits
		BitMatrix bits = new BitMatrix(MATRIX_WIDTH, MATRIX_HEIGHT);
		for (int y = 0; y < MATRIX_HEIGHT; y++) {
			int iy = top + (y * height + height / 2) / MATRIX_HEIGHT;
			for (int x = 0; x < MATRIX_WIDTH; x++) {
				int ix = left
						+ (x * width + width / 2 + (y & 0x01) * width / 2)
						/ MATRIX_WIDTH;
				if (image.get(ix, iy)) {
					bits.set(x, y);
				}
			}
		}
		return bits;
	}

}
