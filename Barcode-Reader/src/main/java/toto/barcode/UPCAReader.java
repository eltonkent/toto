package toto.barcode;

import toto.bitmap.BinaryBitmap;
import toto.util.collections.list.BitArray;

import java.util.Map;

/**
 * <p>
 * Implements decoding of the UPC-A format.
 * </p>
 * 
 */
final class UPCAReader extends UPCEANReader {

	private final UPCEANReader ean13Reader = new EAN13Reader();

	@Override
	public Result decodeRow(int rowNumber, BitArray row, int[] startGuardRange,
			Map<DecodeHintType, ?> hints) throws BarcodeNotFoundException,
			FormatException, BarCodeChecksumException {
		return maybeReturnResult(ean13Reader.decodeRow(rowNumber, row,
				startGuardRange, hints));
	}

	@Override
	public Result decodeRow(int rowNumber, BitArray row,
			Map<DecodeHintType, ?> hints) throws BarcodeNotFoundException,
			FormatException, BarCodeChecksumException {
		return maybeReturnResult(ean13Reader.decodeRow(rowNumber, row, hints));
	}

	@Override
	public Result decode(BinaryBitmap image) throws BarcodeNotFoundException,
			FormatException {
			return maybeReturnResult(ean13Reader.decode(image));
	}

	@Override
	public Result decode(BinaryBitmap image, Map<DecodeHintType, ?> hints)
			throws BarcodeNotFoundException, FormatException {
			return maybeReturnResult(ean13Reader.decode(image, hints));
	}

	@Override
	BarcodeType getBarcodeFormat() {
		return BarcodeType.UPC_A;
	}

	@Override
	protected int decodeMiddle(BitArray row, int[] startRange,
			StringBuilder resultString) throws BarcodeNotFoundException {
		return ean13Reader.decodeMiddle(row, startRange, resultString);
	}

	private static Result maybeReturnResult(Result result)
			throws FormatException {
		String text = result.getText();
		if (text.charAt(0) == '0') {
			return new Result(text.substring(1), null,
					result.getResultPoints(), BarcodeType.UPC_A);
		} else {
			throw FormatException.getFormatInstance();
		}
	}

}
