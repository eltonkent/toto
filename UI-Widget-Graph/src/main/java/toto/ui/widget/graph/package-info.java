/**
 * Graph view implementations
 * <p>
 * <b>Creating a line graphview</b><br/>
 * <pre>
 *  LineGraphView graphView = new LineGraphView(this, "GraphViewDemo");
 *  graphView.addSeries(new SeriesGraphView(new GraphData[] {
 *  			new GraphData(1, 2.0d), new GraphData(2, 1.5d),
 *  			new GraphData(2.5, 3.0d), new GraphData(3, 2.5d),
 *  				new GraphData(4, 1.0d), new GraphData(5, 3.0d) }));
 *  setContentView(graphView)
 *  </pre>
 * </p>
 * @author Elton Kent
 *
 */
package toto.ui.widget.graph;

