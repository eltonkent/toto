package toto.ui.widget.graph;

/**
 * Contract for setting graph information.
 * <p>
 * Extend this class to be able to define your own graph data contracts.
 * 
 * </p>
 */
public class GraphData {
	public final double valueX;
	public final double valueY;

	public GraphData(double valueX, double valueY) {
		super();
		this.valueX = valueX;
		this.valueY = valueY;
	}

	public double getX() {
		return valueX;
	}

	public double getY() {
		return valueY;
	}
}