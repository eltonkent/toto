package toto.ui.widget.graph;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.util.AttributeSet;

/**
 * Draws a Bar Chart
 * <p>
 * <code>
 * <pre>
 *             GraphSeries exampleSeries = new GraphSeries(new GraphData[] {
 * new GraphData(1, 2.0d)
 * , new GraphData(2, 1.5d)
 * , new GraphData(3, 2.5d)
 * , new GraphData(4, 1.0d)
 * 
 * , new GraphData(5, 1.5d)
 * , new GraphData(6, 2.5d)
 * , new GraphData(7, 1.0d)
 * , new GraphData(8, 1.5d)
 * , new GraphData(9, 2.5d)
 * , new GraphData(10, 1.0d)
 * , new GraphData(11, 1.5d)
 * , new GraphData(12, 2.5d)
 * , new GraphData(13, 1.0d)
 * });
 * BarGraphView graphView = (BarGraphView) findViewById(R.id.barGraph);
 * graphView.setAllowZoom(true);
 * graphView.addSeries(exampleSeries);
 *         </pre>
 * </code> <img src="../../../../resources/bargraph.png"/>
 * </p>
 * 
 * @see toto.ui.widget.graph.GraphViewStyle
 * 
 */
public class BarGraphView extends ToToGraphView {
	private boolean drawValuesOnTop;
	private int valuesOnTopColor = Color.WHITE;

	public BarGraphView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BarGraphView(Context context) {
		super(context);
	}

	@Override
	protected void drawHorizontalLabels(Canvas canvas, float border,
			float horstart, float height, String[] horlabels, float graphwidth) {
		// horizontal labels + lines
		paint.setTextAlign(Align.CENTER);

		int hors = horlabels.length;
		float barwidth = graphwidth / horlabels.length;
		float textOffset = barwidth / 2;
		for (int i = 0; i < horlabels.length; i++) {
			// lines
			float x = ((graphwidth / hors) * i) + horstart;
			paint.setColor(gridColor);
			canvas.drawLine(x, height - border, x, border, paint);

			// text
			x = barwidth * i + textOffset + horstart;
			paint.setColor(horizontalLabelsColor);
			canvas.drawText(horlabels[i], x, height - 4, paint);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void drawSeries(Canvas canvas, GraphData[] values, float graphwidth,
			float graphheight, float border, double minX, double minY,
			double diffX, double diffY, float horstart,
			GraphSeries.GraphViewSeriesStyle style) {
		float colwidth = graphwidth / (values.length);

		paint.setStrokeWidth(style.thickness);

		float offset = 0;

		// draw data
		for (int i = 0; i < values.length; i++) {
			float valY = (float) (values[i].getY() - minY);
			float ratY = (float) (valY / diffY);
			float y = graphheight * ratY;

			// hook for value dependent color
			if (style.getValueDependentColor() != null) {
				paint.setColor(style.getValueDependentColor().get(values[i]));
			} else {
				paint.setColor(style.color);
			}

			float left = (i * colwidth) + horstart - offset;
			float top = (border - y) + graphheight;
			float right = ((i * colwidth) + horstart) + (colwidth - 1) - offset;
			canvas.drawRect(left, top, right, graphheight + border - 1, paint);

			// -----Set values on top of graph---------
			if (drawValuesOnTop) {
				top -= 4;
				if (top <= border)
					top += border + 4;
				paint.setTextAlign(Align.CENTER);
				paint.setColor(valuesOnTopColor);
				canvas.drawText(formatLabel(values[i].getY(), false),
						(left + right) / 2, top, paint);
			}
		}
	}

	public boolean getDrawValuesOnTop() {
		return drawValuesOnTop;
	}

	public int getValuesOnTopColor() {
		return valuesOnTopColor;
	}

	/**
	 * You can setKey the flag to let the RageGraphView draw the values on top
	 * of the bars
	 * 
	 * @param drawValuesOnTop
	 */
	public void setDrawValuesOnTop(boolean drawValuesOnTop) {
		this.drawValuesOnTop = drawValuesOnTop;
	}

	public void setValuesOnTopColor(int valuesOnTopColor) {
		this.valuesOnTopColor = valuesOnTopColor;
	}
}
