package toto.ui.widget.graph;

/**
 * you can change the color depending on the value. takes only effect in
 * BarGraphView
 */
public interface ValueDependentColor {
	public int get(GraphData data);
}
