package toto.ui.widget.graph;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.LinearLayout;

import com.mi.toto.ui.graph.R;
/**
 * RageGraphView is a Android View for creating zoomable and scrollable graphs.
 * This is the abstract base class for all graphs. Use {@link LineGraphView} for
 * creating a line chart.
 * 
 */
abstract public class ToToGraphView extends LinearLayout {
	static final private class GraphViewConfig {
		static final float BORDER = 20;
	}

	private class GraphViewContentView extends View {
		private float lastTouchEventX;
		private float graphwidth;
		private boolean scrollingStarted;

		/**
		 * @param context
		 */
		public GraphViewContentView(final Context context) {
			super(context);
			setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.FILL_PARENT));
		}

		/**
		 * @param canvas
		 */
		@Override
		protected void onDraw(final Canvas canvas) {

			paint.setAntiAlias(true);

			// normal
			paint.setStrokeWidth(0);

			float border = GraphViewConfig.BORDER;
			final float horstart = 0;
			final float height = getHeight();
			final float width = getWidth() - 1;
			double maxY = getMaxY();
			double minY = getMinY();
			final double maxX = getMaxX(false);
			final double minX = getMinX(false);
			final double diffX = maxX - minX;

			// measure bottom text
			if (labelTextHeight == null || horLabelTextWidth == null) {
				paint.setTextSize(textSize);
				final double testX = ((getMaxX(true) - getMinX(true)) * 0.783)
						+ getMinX(true);
				final String testLabel = formatLabel(testX, true);
				paint.getTextBounds(testLabel, 0, testLabel.length(),
						textBounds);
				labelTextHeight = (textBounds.height());
				horLabelTextWidth = (textBounds.width());
			}
			border += labelTextHeight;

			final float graphheight = height - (2 * border);
			graphwidth = width;

			if (horlabels == null) {
				horlabels = generateHorlabels(graphwidth);
			}
			if (verlabels == null) {
				verlabels = generateVerlabels(graphheight);
			}

			// vertical lines
			paint.setTextAlign(Align.LEFT);
			final int vers = verlabels.length - 1;
			for (int i = 0; i < verlabels.length; i++) {
				paint.setColor(gridColor);
				final float y = ((graphheight / vers) * i) + border;
				canvas.drawLine(horstart, y, width, y, paint);
			}

			drawHorizontalLabels(canvas, border, horstart, height, horlabels,
					graphwidth);

			paint.setTextAlign(Align.CENTER);
			canvas.drawText(title, (graphwidth / 2) + horstart, border - 4,
					paint);

			if (maxY == minY) {
				// if min/max is the same, fake it so that we can render a line
				if (maxY == 0) {
					// if both are zero, change the values to prevent division
					// by zero
					maxY = 1.0d;
					minY = 0.0d;
				} else {
					maxY = maxY * 1.05d;
					minY = minY * 0.95d;
				}
			}

			final double diffY = maxY - minY;
			paint.setStrokeCap(Paint.Cap.ROUND);

			for (int i = 0; i < graphSeries.size(); i++) {
				drawSeries(canvas, _values(i), graphwidth, graphheight, border,
						minX, minY, diffX, diffY, horstart,
						graphSeries.get(i).style);
			}

			if (showLegend)
				drawLegend(canvas, height, width);
		}

		private void onMoveGesture(final float f) {
			// view port update
			if (viewportSize != 0) {
				viewportStart -= f * viewportSize / graphwidth;

				// minimal and maximal view limit
				final double minX = getMinX(true);
				final double maxX = getMaxX(true);
				if (viewportStart < minX) {
					viewportStart = minX;
				} else if (viewportStart + viewportSize > maxX) {
					viewportStart = maxX - viewportSize;
				}

				// labels have to be regenerated
				if (!staticHorizontalLabels)
					horlabels = null;
				if (!staticVerticalLabels)
					verlabels = null;
				viewVerLabels.invalidate();
			}
			invalidate();
		}

		/**
		 * @param event
		 */
		@Override
		public boolean onTouchEvent(final MotionEvent event) {
			if (!isScrollable() || isDisableTouch()) {
				return super.onTouchEvent(event);
			}

			boolean handled = false;
			// first scale
			if (scalable && scaleDetector != null) {
				scaleDetector.onTouchEvent(event);
				handled = scaleDetector.isInProgress();
			}
			if (!handled) {
				// Log.d("RageGraphView",
				// "on touch event scale not handled+"+lastTouchEventX);
				// if not scaled, scroll
				if ((event.getAction() & MotionEvent.ACTION_DOWN) == MotionEvent.ACTION_DOWN
						&& (event.getAction() & MotionEvent.ACTION_MOVE) == 0) {
					scrollingStarted = true;
					handled = true;
				}
				if ((event.getAction() & MotionEvent.ACTION_UP) == MotionEvent.ACTION_UP) {
					scrollingStarted = false;
					lastTouchEventX = 0;
					handled = true;
				}
				if ((event.getAction() & MotionEvent.ACTION_MOVE) == MotionEvent.ACTION_MOVE) {
					if (scrollingStarted) {
						if (lastTouchEventX != 0) {
							onMoveGesture(event.getX() - lastTouchEventX);
						}
						lastTouchEventX = event.getX();
						handled = true;
					}
				}
				if (handled)
					invalidate();
			} else {
				// currently scaling
				scrollingStarted = false;
				lastTouchEventX = 0;
			}
			return handled;
		}
	}

	public enum LegendAlign {
		TOP, MIDDLE, BOTTOM
	}

	private class VerLabelsView extends View {
		/**
		 * @param context
		 */
		public VerLabelsView(final Context context) {
			super(context);
			setLayoutParams(new LayoutParams(getGraphViewStyle()
					.getVerticalLabelsWidth() == 0 ? 100 : getGraphViewStyle()
					.getVerticalLabelsWidth(), LayoutParams.FILL_PARENT));
		}

		/**
		 * @param canvas
		 */
		@Override
		protected void onDraw(final Canvas canvas) {
			// normal
			paint.setStrokeWidth(0);

			// measure bottom text
			if (labelTextHeight == null || verLabelTextWidth == null) {
				paint.setTextSize(textSize);
				final double testY = ((getMaxY() - getMinY()) * 0.783)
						+ getMinY();
				final String testLabel = formatLabel(testY, false);
				paint.getTextBounds(testLabel, 0, testLabel.length(),
						textBounds);
				labelTextHeight = (textBounds.height());
				verLabelTextWidth = (textBounds.width());
			}
			if (getGraphViewStyle().getVerticalLabelsWidth() == 0
					&& getLayoutParams().width != verLabelTextWidth
							+ GraphViewConfig.BORDER) {
				setLayoutParams(new LayoutParams(
						(int) (verLabelTextWidth + GraphViewConfig.BORDER),
						LayoutParams.FILL_PARENT));
			} else if (getGraphViewStyle().getVerticalLabelsWidth() != 0
					&& getGraphViewStyle().getVerticalLabelsWidth() != getLayoutParams().width) {
				setLayoutParams(new LayoutParams(getGraphViewStyle()
						.getVerticalLabelsWidth(), LayoutParams.FILL_PARENT));
			}

			float border = GraphViewConfig.BORDER;
			border += labelTextHeight;
			final float height = getHeight();
			final float graphheight = height - (2 * border);

			if (verlabels == null) {
				verlabels = generateVerlabels(graphheight);
			}

			// vertical labels
			paint.setTextAlign(verticalLabelsAlign);
			final int labelsWidth = getWidth();
			int labelsOffset = 0;
			if (verticalLabelsAlign == Align.RIGHT) {
				labelsOffset = labelsWidth;
			} else if (verticalLabelsAlign == Align.CENTER) {
				labelsOffset = labelsWidth / 2;
			}
			final int vers = verlabels.length - 1;
			for (int i = 0; i < verlabels.length; i++) {
				final float y = ((graphheight / vers) * i) + border;
				paint.setColor(verticalLabelsColor);
				canvas.drawText(verlabels[i], labelsOffset, y, paint);
			}

			// reset
			paint.setTextAlign(Align.LEFT);
		}
	}

	protected Paint paint;
	private String[] horlabels;
	private String[] verlabels;
	private String title = "";
	private boolean scrollable;
	private boolean disableTouch;
	private double viewportStart;
	private double viewportSize;
	private View viewVerLabels;
	private ScaleGestureDetector scaleDetector;
	private boolean scalable;
	private final NumberFormat[] numberformatter = new NumberFormat[2];
	private List<GraphSeries> graphSeries;
	private boolean showLegend = false;
	private LegendAlign legendAlign = LegendAlign.MIDDLE;
	private boolean manualYAxis;
	private double manualMaxYValue;
	private double manualMinYValue;
	protected GraphViewStyle graphViewStyle;
	private GraphViewContentView graphViewContentView;
	private CustomLabelFormatter customLabelFormatter;
	private Integer labelTextHeight;
	private Integer horLabelTextWidth;
	private Integer verLabelTextWidth;
	private final Rect textBounds = new Rect();
	private boolean staticHorizontalLabels;
	private boolean staticVerticalLabels;
	protected int verticalLabelsColor = Color.WHITE;
	protected int horizontalLabelsColor = Color.WHITE;
	protected int gridColor = Color.DKGRAY;
	private float textSize = 30f;
	private Align verticalLabelsAlign = Align.LEFT;
	private float legendWidth = 120;
	private float legendBorder = 10;
	private float legendSpacing = 10;
	private float legendMarginBottom = 0;
	private Typeface labelTypeFace;

	/**
	 * Get the typeface for the labels
	 * 
	 * @return
	 */
	public final Typeface getLabelTypeFace() {
		return labelTypeFace;
	}

	/**
	 * Set the typeface for the labels
	 * 
	 * @return
	 */
	public final void setLabelTypeFace(final Typeface labelTypeFace) {
		this.labelTypeFace = labelTypeFace;
	}

	public ToToGraphView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		final int width = attrs.getAttributeIntValue("android", "layout_width",
				LayoutParams.MATCH_PARENT);
		final int height = attrs.getAttributeIntValue("android",
				"layout_height", LayoutParams.MATCH_PARENT);
		setLayoutParams(new LayoutParams(width, height));
		init(context, attrs);
	}

	private void init(final Context context, final AttributeSet attrs) {
		graphViewStyle = new GraphViewStyle();
		graphViewStyle.useTextColorFromTheme(context);

		paint = new Paint();
		graphSeries = new ArrayList<GraphSeries>();

		viewVerLabels = new VerLabelsView(context);
		addView(viewVerLabels);
		graphViewContentView = new GraphViewContentView(context);
		addView(graphViewContentView, new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1));

		if (attrs != null) {
			final TypedArray a = context.obtainStyledAttributes(attrs,
					R.styleable.ToToGraphView);
			horizontalLabelsColor = a.getColor(
					R.styleable.ToToGraphView_horizontal_labelColor,
					horizontalLabelsColor);
			verticalLabelsColor = a.getColor(
					R.styleable.ToToGraphView_vertical_labelColor,
					verticalLabelsColor);
			scalable = a.getBoolean(R.styleable.ToToGraphView_allowZoom, true);
			setAllowZoom(scalable);
			scrollable = a.getBoolean(R.styleable.ToToGraphView_allowScroll,
					true);
			gridColor = a.getColor(R.styleable.ToToGraphView_grid_color,
					gridColor);
			textSize = a.getDimension(R.styleable.ToToGraphView_labelTextSize,
					textSize);
			final int labels = a.getInteger(
					R.styleable.ToToGraphView_align_VLabels, 0);
			switch (labels) {
			case 0:
				verticalLabelsAlign = Align.LEFT;
				break;
			case 1:
				verticalLabelsAlign = Align.RIGHT;
				break;
			case 2:
				verticalLabelsAlign = Align.CENTER;
				break;
			}
			legendWidth = a.getDimension(
					R.styleable.ToToGraphView_legend_width, legendWidth);
			legendBorder = a.getDimension(
					R.styleable.ToToGraphView_legend_border, legendBorder);
			legendSpacing = a.getDimension(
					R.styleable.ToToGraphView_legend_spacing, legendSpacing);
			legendMarginBottom = a.getDimension(
					R.styleable.ToToGraphView_legend_bottomMargin,
					legendMarginBottom);

		}
	}

	/**
	 * @param context
	 */
	public ToToGraphView(final Context context) {
		super(context);
		setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));

		// if (title == null)
		this.title = "";
		// else
		// this.title = title;

		init(context, null);
	}

	private GraphData[] _values(final int idxSeries) {
		final GraphData[] values = graphSeries.get(idxSeries).values;
		synchronized (values) {
			if (viewportStart == 0 && viewportSize == 0) {
				// all data
				return values;
			} else {
				// viewport
				final List<GraphData> listData = new ArrayList<GraphData>();
				for (int i = 0; i < values.length; i++) {
					if (values[i].getX() >= viewportStart) {
						if (values[i].getX() > viewportStart + viewportSize) {
							listData.add(values[i]); // one more for nice
														// scrolling
							break;
						} else {
							listData.add(values[i]);
						}
					} else {
						if (listData.isEmpty()) {
							listData.add(values[i]);
						}
						listData.set(0, values[i]); // one before, for nice
													// scrolling
					}
				}
				return listData.toArray(new GraphData[listData.size()]);
			}
		}
	}

	/**
	 * add a series of data to the graph
	 * 
	 * @param series
	 */
	public void addSeries(final GraphSeries series) {
		series.addGraphView(this);
		graphSeries.add(series);
		redrawAll();
	}

	protected void drawHorizontalLabels(final Canvas canvas,
			final float border, final float horstart, final float height,
			final String[] horlabels, final float graphwidth) {
		// horizontal labels + lines
		final int hors = horlabels.length - 1;
		for (int i = 0; i < horlabels.length; i++) {
			paint.setColor(gridColor);
			final float x = ((graphwidth / hors) * i) + horstart;
			canvas.drawLine(x, height - border, x, border, paint);
			paint.setTextAlign(Align.CENTER);
			if (i == horlabels.length - 1)
				paint.setTextAlign(Align.RIGHT);
			if (i == 0)
				paint.setTextAlign(Align.LEFT);
			paint.setColor(horizontalLabelsColor);
			if (labelTypeFace != null)
				paint.setTypeface(labelTypeFace);
			canvas.drawText(horlabels[i], x, height - 4, paint);
		}
	}

	protected void drawLegend(final Canvas canvas, final float height,
			final float width) {
		final float textSize = paint.getTextSize();
		// final int spacing = getGraphViewStyle().getLegendSpacing();
		// final int border = getGraphViewStyle().getLegendBorder();

		final int shapeSize = (int) (textSize * 0.8d);
		Log.d("RageGraphView", "draw legend size: " + paint.getTextSize());

		// rect
		paint.setARGB(180, 100, 100, 100);
		final float legendHeight = (shapeSize + legendSpacing)
				* graphSeries.size() + 2 * legendBorder - legendSpacing;
		final float lLeft = width - legendWidth - legendBorder * 2;
		float lTop;
		switch (legendAlign) {
		case TOP:
			lTop = 0;
			break;
		case MIDDLE:
			lTop = height / 2 - legendHeight / 2;
			break;
		default:
			lTop = height - GraphViewConfig.BORDER - legendHeight
					- legendMarginBottom;
		}
		final float lRight = lLeft + legendWidth;
		final float lBottom = lTop + legendHeight;
		canvas.drawRoundRect(new RectF(lLeft, lTop, lRight, lBottom), 8, 8,
				paint);

		for (int i = 0; i < graphSeries.size(); i++) {
			paint.setColor(graphSeries.get(i).style.color);
			canvas.drawRect(new RectF(lLeft + legendBorder, lTop + legendBorder
					+ (i * (shapeSize + legendSpacing)), lLeft + legendBorder
					+ shapeSize, lTop + legendBorder
					+ (i * (shapeSize + legendSpacing)) + shapeSize), paint);
			if (graphSeries.get(i).description != null) {
				paint.setColor(Color.WHITE);
				paint.setTextAlign(Align.LEFT);
				canvas.drawText(graphSeries.get(i).description, lLeft
						+ legendBorder + shapeSize + legendSpacing, lTop
						+ legendBorder + shapeSize
						+ (i * (shapeSize + legendSpacing)), paint);
			}
		}
	}

	abstract protected void drawSeries(Canvas canvas, GraphData[] values,
			float graphwidth, float graphheight, float border, double minX,
			double minY, double diffX, double diffY, float horstart,
			GraphSeries.GraphViewSeriesStyle style);

	/**
	 * formats the label use #setCustomLabelFormatter or static labels if you
	 * want custom labels
	 * 
	 * @param value
	 *            x and y values
	 * @param isValueX
	 *            if false, value y wants to be formatted
	 * @return value to display
	 * @deprecated use {@link #setCustomLabelFormatter(CustomLabelFormatter)}
	 */
	@Deprecated
	protected String formatLabel(final double value, final boolean isValueX) {
		if (customLabelFormatter != null) {
			final String label = customLabelFormatter.formatLabel(value,
					isValueX);
			if (label != null) {
				return label;
			}
		}
		final int i = isValueX ? 1 : 0;
		if (numberformatter[i] == null) {
			numberformatter[i] = NumberFormat.getNumberInstance();
			final double highestvalue = isValueX ? getMaxX(false) : getMaxY();
			final double lowestvalue = isValueX ? getMinX(false) : getMinY();
			if (highestvalue - lowestvalue < 0.1) {
				numberformatter[i].setMaximumFractionDigits(6);
			} else if (highestvalue - lowestvalue < 1) {
				numberformatter[i].setMaximumFractionDigits(4);
			} else if (highestvalue - lowestvalue < 20) {
				numberformatter[i].setMaximumFractionDigits(3);
			} else if (highestvalue - lowestvalue < 100) {
				numberformatter[i].setMaximumFractionDigits(1);
			} else {
				numberformatter[i].setMaximumFractionDigits(0);
			}
		}
		return numberformatter[i].format(value);
	}

	private String[] generateHorlabels(final float graphwidth) {
		int numLabels = getGraphViewStyle().getNumHorizontalLabels() - 1;
		if (numLabels < 0) {
			numLabels = (int) (graphwidth / (horLabelTextWidth * 2));
		}

		final String[] labels = new String[numLabels + 1];
		final double min = getMinX(false);
		final double max = getMaxX(false);
		for (int i = 0; i <= numLabels; i++) {
			labels[i] = formatLabel(min + ((max - min) * i / numLabels), true);
		}
		return labels;
	}

	synchronized private String[] generateVerlabels(final float graphheight) {
		int numLabels = getGraphViewStyle().getNumVerticalLabels() - 1;
		if (numLabels < 0) {
			numLabels = (int) (graphheight / (labelTextHeight * 3));
			if (numLabels == 0) {
				Log.w("RageGraphView",
						"Height of Graph is smaller than the label text height, so no vertical labels were shown!");
			}
		}
		final String[] labels = new String[numLabels + 1];
		double min = getMinY();
		double max = getMaxY();
		if (max == min) {
			// if min/max is the same, fake it so that we can render a line
			if (max == 0) {
				// if both are zero, change the values to prevent division by
				// zero
				max = 1.0d;
				min = 0.0d;
			} else {
				max = max * 1.05d;
				min = min * 0.95d;
			}
		}

		for (int i = 0; i <= numLabels; i++) {
			labels[numLabels - i] = formatLabel(min
					+ ((max - min) * i / numLabels), false);
		}
		return labels;
	}

	/**
	 * @return the custom label formatter, if there is one. otherwise null
	 */
	public CustomLabelFormatter getCustomLabelFormatter() {
		return customLabelFormatter;
	}

	/**
	 * @return the graphview style. it will never be null.
	 */
	public GraphViewStyle getGraphViewStyle() {
		return graphViewStyle;
	}

	/**
	 * get the position of the legend
	 * 
	 * @return
	 */
	public LegendAlign getLegendAlign() {
		return legendAlign;
	}

	/**
	 * @return legend width
	 */
	public float getLegendWidth() {
		return legendWidth;
	}

	/**
	 * returns the maximal X value of the current viewport (if viewport is
	 * setKey) otherwise maximal X value of all data.
	 * 
	 * @param ignoreViewport
	 *            warning: only override this, if you really know want you're
	 *            doing!
	 */
	protected double getMaxX(final boolean ignoreViewport) {
		// if viewport is setKey, use this
		if (!ignoreViewport && viewportSize != 0) {
			return viewportStart + viewportSize;
		} else {
			// otherwise use the max x value
			// values must be sorted by x, so the last value has the largest X
			// value
			double highest = 0;
			if (graphSeries.size() > 0) {
				GraphData[] values = graphSeries.get(0).values;
				if (values.length == 0) {
					highest = 0;
				} else {
					highest = values[values.length - 1].getX();
				}
				for (int i = 1; i < graphSeries.size(); i++) {
					values = graphSeries.get(i).values;
					if (values.length > 0) {
						highest = Math.max(highest,
								values[values.length - 1].getX());
					}
				}
			}
			return highest;
		}
	}

	/**
	 * returns the maximal Y value of all data.
	 * <p/>
	 * warning: only override this, if you really know want you're doing!
	 */
	protected double getMaxY() {
		double largest;
		if (manualYAxis) {
			largest = manualMaxYValue;
		} else {
			largest = Integer.MIN_VALUE;
			for (int i = 0; i < graphSeries.size(); i++) {
				final GraphData[] values = _values(i);
				for (int ii = 0; ii < values.length; ii++)
					if (values[ii].getY() > largest)
						largest = values[ii].getY();
			}
		}
		return largest;
	}

	/**
	 * returns the minimal X value of the current viewport (if viewport is
	 * setKey) otherwise minimal X value of all data.
	 * 
	 * @param ignoreViewport
	 *            warning: only override this, if you really know want you're
	 *            doing!
	 */
	protected double getMinX(final boolean ignoreViewport) {
		// if viewport is setKey, use this
		if (!ignoreViewport && viewportSize != 0) {
			return viewportStart;
		} else {
			// otherwise use the min x value
			// values must be sorted by x, so the first value has the smallest X
			// value
			double lowest = 0;
			if (graphSeries.size() > 0) {
				GraphData[] values = graphSeries.get(0).values;
				if (values.length == 0) {
					lowest = 0;
				} else {
					lowest = values[0].getX();
				}
				for (int i = 1; i < graphSeries.size(); i++) {
					values = graphSeries.get(i).values;
					if (values.length > 0) {
						lowest = Math.min(lowest, values[0].getX());
					}
				}
			}
			return lowest;
		}
	}

	/**
	 * returns the minimal Y value of all data.
	 * <p/>
	 * warning: only override this, if you really know want you're doing!
	 */
	protected double getMinY() {
		double smallest;
		if (manualYAxis) {
			smallest = manualMinYValue;
		} else {
			smallest = Integer.MAX_VALUE;
			for (int i = 0; i < graphSeries.size(); i++) {
				final GraphData[] values = _values(i);
				for (int ii = 0; ii < values.length; ii++)
					if (values[ii].getY() < smallest)
						smallest = values[ii].getY();
			}
		}
		return smallest;
	}

	public boolean isDisableTouch() {
		return disableTouch;
	}

	public boolean isScrollable() {
		return scrollable;
	}

	public boolean isShowLegend() {
		return showLegend;
	}

	/**
	 * forces graphview to invalide all views and caches. Normally there is no
	 * need to call this manually.
	 */
	public void redrawAll() {
		if (!staticVerticalLabels)
			verlabels = null;
		if (!staticHorizontalLabels)
			horlabels = null;
		numberformatter[0] = null;
		numberformatter[1] = null;
		labelTextHeight = null;
		horLabelTextWidth = null;
		verLabelTextWidth = null;

		invalidate();
		viewVerLabels.invalidate();
		graphViewContentView.invalidate();
	}

	/**
	 * removes all series
	 */
	public void removeAllSeries() {
		for (final GraphSeries s : graphSeries) {
			s.removeGraphView(this);
		}
		while (!graphSeries.isEmpty()) {
			graphSeries.remove(0);
		}
		redrawAll();
	}

	/**
	 * removes a series
	 * 
	 * @param series
	 *            series to remove
	 */
	public void removeSeries(final GraphSeries series) {
		series.removeGraphView(this);
		graphSeries.remove(series);
		redrawAll();
	}

	/**
	 * removes series
	 * 
	 * @param index
	 */
	public void removeSeries(final int index) {
		if (index < 0 || index >= graphSeries.size()) {
			throw new IndexOutOfBoundsException("No series at index " + index);
		}

		removeSeries(graphSeries.get(index));
	}

	/**
	 * scrolls to the last x-value
	 * 
	 * @throws IllegalStateException
	 *             if scrollable == false
	 */
	public void scrollToEnd() {
		if (!scrollable)
			throw new IllegalStateException(
					"This RageGraphView is not scrollable.");
		final double max = getMaxX(true);
		viewportStart = max - viewportSize;

		// don't clear labels width/height cache
		// so that the display is not flickering
		if (!staticVerticalLabels)
			verlabels = null;
		if (!staticHorizontalLabels)
			horlabels = null;

		invalidate();
		viewVerLabels.invalidate();
		graphViewContentView.invalidate();
	}

	/**
	 * setKey a custom label formatter
	 * 
	 * @param customLabelFormatter
	 */
	public void setCustomLabelFormatter(
			final CustomLabelFormatter customLabelFormatter) {
		this.customLabelFormatter = customLabelFormatter;
	}

	/**
	 * The user can disable any touch gestures, this is useful if you are using
	 * a real time graph, but don't want the user to interact
	 * 
	 * @param disableTouch
	 */
	public void setDisableTouch(final boolean disableTouch) {
		this.disableTouch = disableTouch;
	}

	/**
	 * setKey custom graphview style
	 * 
	 * @param style
	 */
	public void setGraphViewStyle(final GraphViewStyle style) {
		graphViewStyle = style;
		labelTextHeight = null;
	}

	/**
	 * setKey's static horizontal labels (from left to right)
	 * 
	 * @param horlabels
	 *            if null, labels were generated automatically
	 */
	public void setHorizontalLabels(final String[] horlabels) {
		staticHorizontalLabels = horlabels != null;
		this.horlabels = horlabels;
	}

	/**
	 * legend position
	 * 
	 * @param legendAlign
	 */
	public void setLegendAlign(final LegendAlign legendAlign) {
		this.legendAlign = legendAlign;
	}

	/**
	 * legend width
	 * 
	 * @param legendWidth
	 * @deprecated use {@link GraphViewStyle#setLegendWidth(int)}
	 */
	@Deprecated
	public void setLegendWidth(final float legendWidth) {
		this.legendWidth = legendWidth;
	}

	/**
	 * you have to setKey the bounds
	 * {@link #setManualYAxisBounds(double, double)}. That automatically enables
	 * manualYAxis-flag. if you want to disable the menual y axis, call this
	 * method with false.
	 * 
	 * @param manualYAxis
	 */
	public void setManualYAxis(final boolean manualYAxis) {
		this.manualYAxis = manualYAxis;
	}

	/**
	 * setKey manual Y axis limit
	 * 
	 * @param max
	 * @param min
	 */
	public void setManualYAxisBounds(final double max, final double min) {
		manualMaxYValue = max;
		manualMinYValue = min;
		manualYAxis = true;
	}

	/**
	 * this forces scrollable = true
	 * 
	 * @param scalable
	 */
	public void setAllowZoom(final boolean scalable) {
		this.scalable = scalable;
		if (scalable == true && scaleDetector == null) {
			scrollable = true; // automatically forces this
			scaleDetector = new ScaleGestureDetector(getContext(),
					new ScaleGestureDetector.SimpleOnScaleGestureListener() {
						@Override
						public boolean onScale(
								final ScaleGestureDetector detector) {
							final double center = viewportStart + viewportSize
									/ 2;
							viewportSize /= detector.getScaleFactor();
							viewportStart = center - viewportSize / 2;

							// viewportStart must not be < minX
							final double minX = getMinX(true);
							if (viewportStart < minX) {
								viewportStart = minX;
							}

							// viewportStart + viewportSize must not be > maxX
							final double maxX = getMaxX(true);
							if (viewportSize == 0) {
								viewportSize = maxX;
							}
							final double overlap = viewportStart + viewportSize
									- maxX;
							if (overlap > 0) {
								// scroll left
								if (viewportStart - overlap > minX) {
									viewportStart -= overlap;
								} else {
									// maximal scale
									viewportStart = minX;
									viewportSize = maxX - viewportStart;
								}
							}
							redrawAll();
							return true;
						}
					});
		}
	}

	/**
	 * the user can scroll (horizontal) the graph. This is only useful if you
	 * use a viewport {@link #setViewPort(double, double)} which doesn't
	 * displays all data.
	 * 
	 * @param scrollable
	 */
	public void setScrollable(final boolean scrollable) {
		this.scrollable = scrollable;
	}

	public void setShowLegend(final boolean showLegend) {
		this.showLegend = showLegend;
	}

	/**
	 * sets the title of graphview
	 * 
	 * @param title
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * setKey's static vertical labels (from top to bottom)
	 * 
	 * @param verlabels
	 *            if null, labels were generated automatically
	 */
	public void setVerticalLabels(final String[] verlabels) {
		staticVerticalLabels = verlabels != null;
		this.verlabels = verlabels;
	}

	/**
	 * setKey's the viewport for the graph.
	 * 
	 * @param start
	 *            x-value
	 * @param size
	 * @see #setManualYAxisBounds(double, double) to limit the y-viewport
	 */
	public void setViewPort(final double start, final double size) {
		if (size < 0) {
			throw new IllegalArgumentException(
					"Viewport size must be greater than 0!");
		}
		viewportStart = start;
		viewportSize = size;
	}

	public int getHorizontalLabelsColor() {
		return horizontalLabelsColor;
	}

	public int getVerticalLabelsColor() {
		return verticalLabelsColor;
	}

	public void setVerticalLabelsColor(final int c) {
		verticalLabelsColor = c;
	}

	public void setHorizontalLabelsColor(final int c) {
		horizontalLabelsColor = c;
	}

	public int getGridColor() {
		return gridColor;
	}

	/**
	 * Set graph grid color
	 * 
	 * @param c
	 */
	public void setGridColor(final int c) {
		gridColor = c;
	}

	public float getTextSize() {
		return textSize;
	}

	/**
	 * Set the label text size.
	 * 
	 * @param textSize
	 */
	public void setTextSize(final float textSize) {
		this.textSize = textSize;
	}

	/**
	 * Set label alignment
	 * 
	 * @return
	 */
	public Align getVerticalLabelsAlign() {
		return verticalLabelsAlign;
	}

	public void setVerticalLabelsAlign(final Align verticalLabelsAlign) {
		this.verticalLabelsAlign = verticalLabelsAlign;
	}

	public void setLegendWidth(final int legendWidth) {
		this.legendWidth = legendWidth;
	}

	public void setLegendBorder(final float legendBorder) {
		this.legendBorder = legendBorder;
	}

	public void setLegendSpacing(final float legendSpacing) {
		this.legendSpacing = legendSpacing;
	}

	public float getLegendBorder() {
		return legendBorder;
	}

	public float getLegendSpacing() {
		return legendSpacing;
	}

	public float getLegendMarginBottom() {
		return legendMarginBottom;
	}

	public void setLegendMarginBottom(final float legendMarginBottom) {
		this.legendMarginBottom = legendMarginBottom;
	}

}
