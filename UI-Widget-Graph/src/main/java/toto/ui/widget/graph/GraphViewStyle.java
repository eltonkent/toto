package toto.ui.widget.graph;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.view.ContextThemeWrapper;

/**
 * Styles for the RageGraphView Important: Use GraphSeries.GraphViewSeriesStyle
 * for series-specify styling
 * 
 */
public class GraphViewStyle {

	private int verticalLabelsWidth;
	private int numVerticalLabels;
	private int numHorizontalLabels;

	public GraphViewStyle() {
	}

	public int getNumHorizontalLabels() {
		return numHorizontalLabels;
	}

	public int getNumVerticalLabels() {
		return numVerticalLabels;
	}

	public int getVerticalLabelsWidth() {
		return verticalLabelsWidth;
	}

	/**
	 * @param numHorizontalLabels
	 *            0 = auto
	 */
	public void setNumHorizontalLabels(final int numHorizontalLabels) {
		this.numHorizontalLabels = numHorizontalLabels;
	}

	/**
	 * @param numVerticalLabels
	 *            0 = auto
	 */
	public void setNumVerticalLabels(final int numVerticalLabels) {
		this.numVerticalLabels = numVerticalLabels;
	}

	/**
	 * @param verticalLabelsWidth
	 *            0 = auto
	 */
	public void setVerticalLabelsWidth(final int verticalLabelsWidth) {
		this.verticalLabelsWidth = verticalLabelsWidth;
	}

	/**
	 * tries to get the theme's font color and use it for labels
	 * 
	 * @param context
	 *            must be instance of ContextThemeWrapper
	 */
	public void useTextColorFromTheme(final Context context) {
		if (context instanceof ContextThemeWrapper) {
			final TypedArray array = ((ContextThemeWrapper) context).getTheme()
					.obtainStyledAttributes(
							new int[] { android.R.attr.textColorPrimary });
			final int color = array.getColor(0, Color.WHITE);
			array.recycle();

		}
	}
}
