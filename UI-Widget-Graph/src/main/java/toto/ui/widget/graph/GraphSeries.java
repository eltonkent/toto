package toto.ui.widget.graph;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that holds information on Graph data, description and styles a
 * graphview series. holds the data, description and styles
 */
public class GraphSeries {
	/**
	 * graph series style: color and thickness
	 */
	static public class GraphViewSeriesStyle {
		public int color = 0xff0077cc;
		public int thickness = 3;
		private ValueDependentColor valueDependentColor;

		public GraphViewSeriesStyle() {
			super();
		}

		public GraphViewSeriesStyle(int color, int thickness) {
			super();
			this.color = color;
			this.thickness = thickness;
		}

		public ValueDependentColor getValueDependentColor() {
			return valueDependentColor;
		}

		/**
		 * the color depends on the value of the data. only possible in
		 * BarGraphView
		 * 
		 * @param valueDependentColor
		 */
		public void setValueDependentColor(
				ValueDependentColor valueDependentColor) {
			this.valueDependentColor = valueDependentColor;
		}
	}

	final String description;
	final GraphViewSeriesStyle style;
	GraphData[] values;
	private final List<ToToGraphView> rageGraphViews = new ArrayList<ToToGraphView>();

	public GraphSeries(GraphData[] values) {
		description = null;
		style = new GraphViewSeriesStyle();
		this.values = values;
	}

	public GraphSeries(String description, GraphViewSeriesStyle style,
			GraphData[] values) {
		super();
		this.description = description;
		if (style == null) {
			style = new GraphViewSeriesStyle();
		}
		this.style = style;
		this.values = values;
	}

	/**
	 * this graphview will be redrawn if data changes
	 * 
	 * @param rageGraphView
	 */
	public void addGraphView(ToToGraphView rageGraphView) {
		this.rageGraphViews.add(rageGraphView);
	}

	/**
	 * add one data to current data
	 * 
	 * @param value
	 *            the new data to append
	 * @param scrollToEnd
	 *            true => graphview will scroll to the end (maxX)
	 * @deprecated please use {@link #appendData(GraphData, boolean, int)} to
	 *             avoid memory overflow
	 */
	@Deprecated
	public void appendData(GraphData value, boolean scrollToEnd) {
		GraphData[] newValues = new GraphData[values.length + 1];
		int offset = values.length;
		System.arraycopy(values, 0, newValues, 0, offset);

		newValues[values.length] = value;
		values = newValues;
		for (ToToGraphView g : rageGraphViews) {
			if (scrollToEnd) {
				g.scrollToEnd();
			}
		}
	}

	/**
	 * add one data to current data
	 * 
	 * @param value
	 *            the new data to append
	 * @param scrollToEnd
	 *            true => graphview will scroll to the end (maxX)
	 * @param maxDataCount
	 *            if max data count is reached, the oldest data value will be
	 *            lost
	 */
	public void appendData(GraphData value, boolean scrollToEnd,
			int maxDataCount) {
		synchronized (values) {
			int curDataCount = values.length;
			GraphData[] newValues;
			if (curDataCount < maxDataCount) {
				// enough space
				newValues = new GraphData[curDataCount + 1];
				System.arraycopy(values, 0, newValues, 0, curDataCount);
				// append new data
				newValues[curDataCount] = value;
			} else {
				// we have to trim one data
				newValues = new GraphData[maxDataCount];
				System.arraycopy(values, 1, newValues, 0, curDataCount - 1);
				// append new data
				newValues[maxDataCount - 1] = value;
			}
			values = newValues;
		}

		// update linked graph views
		for (ToToGraphView g : rageGraphViews) {
			if (scrollToEnd) {
				g.scrollToEnd();
			}
		}
	}

	/**
	 * @return series styles. never null
	 */
	public GraphViewSeriesStyle getStyle() {
		return style;
	}

	/**
	 * you should use {@link ToToGraphView#removeSeries(GraphSeries)}
	 * 
	 * @param rageGraphView
	 */
	public void removeGraphView(ToToGraphView rageGraphView) {
		rageGraphViews.remove(rageGraphView);
	}

	/**
	 * clears the current data and setKey the new. redraws the graphview(s)
	 * 
	 * @param values
	 *            new data
	 */
	public void resetData(GraphData[] values) {
		this.values = values;
		for (ToToGraphView g : rageGraphViews) {
			g.redrawAll();
		}
	}
}
