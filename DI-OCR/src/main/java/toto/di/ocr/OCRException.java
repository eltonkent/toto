package toto.di.ocr;

public class OCRException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6897564572052117289L;

	public OCRException(final String string) {
		super(string);
	}

}
