package toto.di.ocr;

public class PageIterator {

	/** Pointer to native page iterator. */
	private final int mNativePageIterator;

	/* package */PageIterator(final int nativePageIterator) {
		mNativePageIterator = nativePageIterator;
	}

	/**
	 * Resets the iterator to point to the start of the page.
	 */
	public void begin() {
		nativeBegin(mNativePageIterator);
	}

	/**
	 * Moves to the start of the next object at the given level in the page
	 * hierarchy, and returns false if the end of the page was reached.
	 * <p>
	 * NOTE that {@link PageIteratorLevel#RIL_SYMBOL} will skip non-text blocks,
	 * but all other {@link PageIteratorLevel} level values will visit each
	 * non-text block once. Think of non text blocks as containing a single
	 * para, with a single line, with a single imaginary word.
	 * <p>
	 * Calls to {@link #next} with different levels may be freely intermixed.
	 * <p>
	 * This function iterates words in right-to-left scripts correctly, if the
	 * appropriate language has been loaded into Tesseract.
	 * 
	 * @param level
	 *            the page iterator level. See {@link PageIteratorLevel}.
	 * @return {@code false} if the end of the page was reached, {@code true}
	 *         otherwise.
	 */
	public boolean next(final PageIteratorLevel level) {
		return nativeNext(mNativePageIterator, level.getValue());
	}

	private static native void nativeBegin(int nativeIterator);

	private static native boolean nativeNext(int nativeIterator, int level);
}
