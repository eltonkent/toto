package toto.di.ocr;

import java.io.File;

import toto.di.ocr.patterns.OCRPattern;
import toto.bitmap.nativ.ToToNativeBitmap;
import toto.bitmap.nativ.Pixa;
import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Optical character recognition utility.
 * <p>
 * <b>Usage:</b><br/>
 * The recognition language data files should be in a folder called "tessdata".
 * It should be found at <a href=
 * "https://code.google.com/p/tesseract-ocr/downloads/list">The tesseract
 * project</a><br/>
 * <code>
 * <font color="green">//Set the language data file and OCR Mode.</font><br/>
 * OCRManager baseApi = OCRManager.getInstance();
 * baseApi.n_init("/mnt/sdcard/tessdata/",
 * 				"eng", OCRMode.OCR_DEFAULT);<br/>
 * <font color="green">//Page segment mode informs of the type of scanning to use.</font><br/>
 * baseApi.setPageSegmentMode(PageSegmentMode.PSM_AUTO);<br/>
 * baseApi.setImage(bitmap);<br/>
 * <font color="green">//Get the detected text</font><br/>
 * String outputText = baseApi.getUTF8Text();<br/>
 * </code> <b>Note:</b><br/>
 * This API is not thread safe and therefore should use a single instance across
 * an application
 * </p>
 * 
 * @see OCRManager#shutdown()
 * @see OCRManager#init(String, String, OCRMode)
 * 
 * @author Elton Kent
 * 
 */
public class OCRManager {

	/** Whitelist of characters to recognize. */
	private static final String VAR_CHAR_WHITELIST = "tessedit_char_whitelist";

	/** Blacklist of characters to not recognize. */
	private static final String VAR_CHAR_BLACKLIST = "tessedit_char_blacklist";

	/**
	 * Mode of operation for OCR engine
	 * 
	 * @author Elton Kent
	 * 
	 */
	public enum OCRMode {

		/** Run Tesseract only - fastest */
		OCR_TESSERACT_ONLY(0),

		/** Run Cube only - better accuracy, but slower */
		OCR_CUBE_ONLY(1),

		/** Run both and combine results - best accuracy */
		OCR_TESSERACT_CUBE_COMBINED(2),

		/** Default OCR engine mode. */
		OCR_DEFAULT(3);

		private OCRMode(final int value) {
			this.value = value;
		}

		private int value;

		int getValue() {
			return value;
		}
	}

	private static OCRManager instance;
	private TessBaseAPI ocrAPI;
	private static boolean iShutdown;

	private OCRManager() {

	}

	/**
	 * 
	 * Initialize the OCR manager
	 * 
	 * @param languageDirectory
	 *            The location of the language data files.
	 * @param language
	 *            The language to use.
	 *            <p>
	 *            <i>"eng"</i> for english.<br/>
	 *            <i>"swe"</i> for swedish.<br/>
	 *            <i>"ukr"</i> for ukrainian.<br/>
	 *            <i>"tur"</i> for turkish.<br/>
	 *            <i>"tha"</i> for thai.<br/>
	 *            <i>"por"</i> for portuguese.<br/>
	 *            <i>"nld"</i> for dutch.<br/>
	 *            <i>"ita"</i> for italian.<br/>
	 *            and many more..
	 *            </p>
	 * @param ocrMode
	 *            Mode to use for OCR
	 * @return True if the engine setup was successful.
	 */
	public boolean init(final String languageDirectory, final String language,
			final OCRMode ocrMode) {
		ocrAPI = new TessBaseAPI();
		boolean ret;
		ret = ocrAPI.init(languageDirectory.toString(), language,
				ocrMode.getValue());
		// ifocrAPI.setPageSegMode(PageSegmentMode.PSM_SINGLE_BLOCK);
		return ret;
	}

	/**
	 * Get OCR manager instance.
	 * 
	 * @return
	 * @see #shutdown()
	 */
	public static synchronized OCRManager getInstance() {
		if (instance == null || iShutdown) {
			instance = new OCRManager();
			iShutdown = false;
		}
		return instance;
	}

	/**
	 * Set the image that the OCR should recognize.
	 * <p>
	 * The bitmap config with the most accuracy is {@link Bitmap.Config.ARGB888}
	 * . Bitmaps with other configurations cause different results. <br/>
	 * <ul>
	 * <li><b>RGB_565</b>: Causes text to be repeated.</li>
	 * <li><b>ARGB_4444</b>: Detection with low accuracy.</li>
	 * </ul>
	 * 
	 * </p>
	 * 
	 * @param bmp
	 */
	public void setImage(final ToToNativeBitmap bmp) {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		ocrAPI.setImage(bmp);
	}

	/**
	 * Provides an image for Tesseract to recognize.
	 * 
	 * @param file
	 *            absolute path to the image file
	 */
	public void setImage(final File file) {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		ocrAPI.setImage(file);
	}

	public void setImage(final int[] pixels, final int width, final int height) {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		ocrAPI.setImage(pixels, width, height);
	}

	/**
	 * Shutdown OCR manager and release the resources associated.
	 */
	public void shutdown() {
		iShutdown = true;
		ocrAPI.end();
		instance = null;
	}

	/**
	 * The recognized text is returned as a String which is coded as UTF8.
	 * 
	 * @return the recognized text
	 */
	public String getUTF8Text() {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		return ocrAPI.getUTF8Text();
	}

	/**
	 * Returns the mean confidence of text recognition.
	 * 
	 * @return the mean confidence
	 */
	public int getConfidence() {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		return ocrAPI.meanConfidence();
	}

	public void setPageSegmentMode(final PageSegmentMode mode) {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		ocrAPI.setPageSegMode(mode);
	}

	public ResultIterator getResultIterator() {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		return ocrAPI.getResultIterator();
	}

	/**
	 * Restricts recognition to a sub-rectangle of the image. Call after
	 * SetImage. Each SetRectangle clears the recogntion results so multiple
	 * rectangles can be recognized with the same image.
	 * 
	 * @param left
	 *            the left bound
	 * @param top
	 *            the right bound
	 * @param width
	 *            the width of the bounding box
	 * @param height
	 *            the height of the bounding box
	 */
	public void setRectangle(final int left, final int top, final int width,
			final int height) {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		ocrAPI.setRectangle(left, top, width, height);
	}

	/**
	 * Restricts recognition to a sub-rectangle of the image. Call after
	 * SetImage. Each SetRectangle clears the recogntion results so multiple
	 * rectangles can be recognized with the same image.
	 * 
	 * @param rect
	 *            the bounding rectangle
	 */
	public void setRectangle(final Rect rect) {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		setRectangle(rect.left, rect.top, rect.width(), rect.height());
	}

	/**
	 * Black list characters.
	 * <p>
	 * Characters that are blacklisted are not detected.<br/>
	 * For Instance:<br/>
	 * <font color="green">//omit characters a,e,i,o,u.</font><br/>
	 * blackListCharacters("aeiou");
	 * </p>
	 * 
	 * @param charcters
	 */
	public void blackListCharacters(final String charcters) {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		ocrAPI.setVariable(VAR_CHAR_BLACKLIST, charcters);
	}

	/**
	 * Remove set image or blacklist/whitelist settings.
	 */
	public void clear() {
		ocrAPI.clear();
	}

	/**
	 * Returns the word bounding boxes as a Pixa, in reading order.
	 * 
	 * @return Pixa containing word bounding boxes
	 */
	public Pixa getWords() {
		return ocrAPI.getWords();
	}

	/**
	 * White list characters.
	 * <p>
	 * Characters that are white listed are only detected.<br/>
	 * For Instance:<br/>
	 * <font color="green">//only read characters a,e,i,o,u.</font><br/>
	 * whiteListCharacters("aeiou");
	 * </p>
	 * 
	 * @param charcters
	 */
	public void whiteListCharacters(final String charcters) {
		if (iShutdown) {
			throw new RuntimeException("OCR manager is already shutdown");
		}
		ocrAPI.setVariable(VAR_CHAR_WHITELIST, charcters);
	}

	public boolean matches(final OCRPattern pattern) {
		return pattern.matches();
	}

	/**
	 * Returns all word confidences (between 0 and 100) in an array. The number
	 * of confidences should correspond to the number of space-delimited words
	 * in GetUTF8Text().
	 * 
	 * @return an array of word confidences (between 0 and 100) for each
	 *         space-delimited word returned by GetUTF8Text()
	 */
	public int[] wordConfidences() {
		return ocrAPI.wordConfidences();
	}

	/**
	 * Returns the mean confidence of text recognition.
	 * 
	 * @return the mean confidence
	 */
	public int meanConfidence() {
		return ocrAPI.meanConfidence();
	}

	/**
	 * Returns the textlines as a Pixa.
	 * 
	 * Block IDs are not returned.
	 * 
	 * @return Pixa containing textlines
	 */
	public Pixa getTextlines() {
		return ocrAPI.getTextlines();
	}

	/**
	 * Returns the strips as a Pixa.
	 * 
	 * Block IDs are not returned.
	 * 
	 * @return Pixa containing strips
	 */
	public Pixa getStrips() {
		return ocrAPI.getStrips();
	}

	/**
	 * Returns the result of page layout analysis as a Pixa, in reading order.
	 * 
	 * @return Pixa contaning page layout bounding boxes
	 */
	public Pixa getRegions() {
		return ocrAPI.getRegions();
	}
}
