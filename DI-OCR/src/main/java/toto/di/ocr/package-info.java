/**
 * Fast Optical character recognition utility
 * <p>
 * <div>
 * <h3>OCR using RAGE</h3>
 * The OCR capabilities for RAGE is powered by the Tesseract Engine maintained by google.
 * </div>
 * </p>
 * @author Mobifluence Interactive
 *
 */
package toto.di.ocr;