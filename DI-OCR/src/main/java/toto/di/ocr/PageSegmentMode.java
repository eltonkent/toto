package toto.di.ocr;

/**
 * Page Segmentation constants. for OCR processing.
 * 
 * @author Elton Kent
 * 
 */
public enum PageSegmentMode {

	/** Orientation and script detection only. */
	PSM_OSD_ONLY(0),

	/**
	 * Automatic page segmentation with orientation and script detection. (OSD).
	 */
	PSM_AUTO_OSD(1),

	/** Fully automatic page segmentation, but no OSD, or OCR. */
	PSM_AUTO_ONLY(2),

	/** Fully automatic page segmentation, but no OSD. */
	PSM_AUTO(3),

	/** Assume a single column of text of variable sizes. */
	PSM_SINGLE_COLUMN(4),

	/** Assume a single uniform block of vertically aligned text. */
	PSM_SINGLE_BLOCK_VERT_TEXT(5),

	/** Assume a single uniform block of text. (Default.) */
	PSM_SINGLE_BLOCK(6),
	/** Treat the image as a single text line. */
	PSM_SINGLE_LINE(7),
	/** Treat the image as a single word. */
	PSM_SINGLE_WORD(8),
	/** Treat the image as a single word in a circle. */
	PSM_CIRCLE_WORD(9),
	/** Treat the image as a single character. */
	PSM_SINGLE_CHAR(10),
	/** Find as much text as possible in no particular order. */
	PSM_SPARSE_TEXT(11),
	/** Sparse text with orientation and script detection. */
	PSM_SPARSE_TEXT_OSD(12),
	/** Number of enum entries. */
	PSM_COUNT(13);

	private PageSegmentMode(final int value) {
		this.value = value;
	}

	private int value;

	int getValue() {
		return value;
	}

}