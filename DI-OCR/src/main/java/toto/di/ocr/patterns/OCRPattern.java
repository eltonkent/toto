package toto.di.ocr.patterns;

public interface OCRPattern {
	public boolean matches();
}
