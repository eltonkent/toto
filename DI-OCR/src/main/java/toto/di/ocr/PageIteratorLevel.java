package toto.di.ocr;


/**
 * Elements of the page hierarchy, used in {@link ResultIterator} to provide
 * functions that operate on each level without having to have 5x as many
 * functions.
 * <p>
 * NOTE: At present {@link #RIL_PARA} and {@link #RIL_BLOCK} are equivalent as
 * there is no paragraph internally yet.
 * 
 * @author Elton Kent
 * 
 */
public enum PageIteratorLevel {
	/** Block of text/image/separator line. */
	RIL_BLOCK(0),

	/** Paragraph within a block. */
	RIL_PARA(1),

	/** Line within a paragraph. */
	RIL_TEXTLINE(2),

	/** Word within a text line. */
	RIL_WORD(3),

	/** Symbol/character within a word. */
	RIL_SYMBOL(4);

	private PageIteratorLevel(final int value) {
		this.value = value;
	}

	private int value;

	int getValue() {
		return value;
	}
}
