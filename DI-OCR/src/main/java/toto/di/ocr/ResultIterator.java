package toto.di.ocr;


/**
 * Java interface for the ResultIterator. Does not implement all available JNI
 * methods, but does implement enough to be useful.
 * 
 */
public class ResultIterator extends PageIterator {

	/** Pointer to native result iterator. */
	private final int mNativeResultIterator;

	/* package */ResultIterator(final int nativeResultIterator) {
		super(nativeResultIterator);

		mNativeResultIterator = nativeResultIterator;
	}

	/**
	 * Returns the text string for the current object at the given level.
	 * 
	 * @param level
	 *            the page iterator level. See {@link PageIteratorLevel}.
	 * @return the text string for the current object at the given level.
	 */
	public String getUTF8Text(final PageIteratorLevel level) {
		return nativeGetUTF8Text(mNativeResultIterator, level.getValue());
	}

	/**
	 * Returns the mean confidence of the current object at the given level. The
	 * number should be interpreted as a percent probability (0-100).
	 * 
	 * @param level
	 *            the page iterator level. See {@link PageIteratorLevel}.
	 * @return the mean confidence of the current object at the given level.
	 */
	public float confidence(final PageIteratorLevel level) {
		return nativeConfidence(mNativeResultIterator, level.getValue());
	}

	private static native String nativeGetUTF8Text(int nativeResultIterator,
			int level);

	private static native float nativeConfidence(int nativeResultIterator,
			int level);
}
