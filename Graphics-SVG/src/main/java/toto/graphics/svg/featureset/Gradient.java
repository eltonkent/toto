package toto.graphics.svg.featureset;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;

import android.graphics.Shader;
import android.util.Log;

public abstract class Gradient extends SVGFeature {

	protected String xlink;

	protected final boolean isLinear;
	protected final List<Stop> stops = new ArrayList<Stop>();

	public Gradient(final boolean isLinear) {
		this.isLinear = isLinear;
	}

	public boolean isLinear() {
		return isLinear;
	}

	void addStop(final Attributes atts) {
		final Stop gradStop = new Stop();
		gradStop.read(atts);
		stops.add(gradStop);
	}

	@Override
	public void read(final Attributes atts) {
		super.read(atts);
		String xlink = getStringAttr("href", atts);
		if (xlink != null) {
			if (xlink.startsWith("#")) {
				xlink = xlink.substring(1);
			}
			this.xlink = xlink;
		}

	}

	@Override
	public void read(final String atts) {

	}

	public void addStop(final Stop stop) {
		stops.add(stop);
	}

	protected String getXlink() {
		return xlink;
	}

	protected void setXlink(final String xlink) {
		this.xlink = xlink;
	}

	public void readComplete() {
		reload();
	}

	public int[] getColors() {
		int[] colors = new int[stops.size()];
		for (int i = 0; i < colors.length; i++) {
			colors[i] = stops.get(i).stopColor;
		}
		if (colors.length == 0) {
			Log.e(TAG, "No Gradient colors for " + id
					+ " Default [Black/White] used");
			colors = new int[] { 0x000000, 0xffffff };
		}
		return colors;

	}

	public float[] getPositions() {
		final float[] positions = new float[stops.size()];
		for (int i = 0; i < positions.length; i++) {
			positions[i] = stops.get(i).offset;
		}
		return positions;
	}

	abstract Shader getShader();

}
