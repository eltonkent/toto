package toto.graphics.svg.featureset;

import org.xml.sax.Attributes;

import android.graphics.Shader;

public class LinearGradient extends Gradient {

	private float x1, y1, x2, y2;

	/**
	 * use z indexed variables for rendering
	 */
	private float zX1, zY1, zX2, zY2;

	private android.graphics.LinearGradient shader;

	public LinearGradient() {
		super(true);
	}

	public void reload() {
		zX1 = getZoomFactor(x1);
		zX2 = getZoomFactor(x2);
		zY1 = getZoomFactor(y1);
		zY2 = getZoomFactor(y2);
		shader = new android.graphics.LinearGradient(zX1, zY1, zX2, zY2,
				getColors(), getPositions(), Shader.TileMode.CLAMP);
		if (matrix != null) {
			shader.setLocalMatrix(matrix);
		}

	}

	@Override
	public void read(final Attributes atts) {
		super.read(atts);
		x1 = getFloatAttr("x1", atts, 0f);
		x2 = getFloatAttr("x2", atts, 0f);
		y1 = getFloatAttr("y1", atts, 0f);
		y2 = getFloatAttr("y2", atts, 0f);
	}

	public float getX1() {
		return x1;
	}

	public void setX1(final String x1) {
		this.x1 = getFloatAttr(x1);
	}

	public float getY1() {
		return y1;
	}

	public void setY1(final String y1) {
		this.y1 = getFloatAttr(y1);
	}

	public float getX2() {
		return x2;
	}

	public void setX2(final String x2) {
		this.x2 = getFloatAttr(x2);
	}

	public float getY2() {
		return y2;
	}

	public void setY2(final String y2) {
		this.y2 = getFloatAttr(y2);
	}

	public String asXMLString() {

		final StringBuilder xml = new StringBuilder();
		xml.append("<linearGradient");

		if (xlink != null) {
			xml.append(newLine);
			xml.append("xlink:href=\"" + xlink + "\"");
		}
		xml.append(newLine);
		xml.append("id=\"" + id + "\"");

		if (transform != null) {
			xml.append(newLine);
			xml.append("gradientTransform=\"" + transform + "\"");
		}
		xml.append(newLine);
		xml.append("x1=\"" + x1 + "\"");
		xml.append(newLine);
		xml.append("y1=\"" + y1 + "\"");
		xml.append(newLine);
		xml.append("x2=\"" + x2 + "\"");
		xml.append(newLine);
		xml.append("y2=\"" + y2 + "\"");
		if (stops.isEmpty()) {
			xml.append("/>");
		} else {
			xml.append(">");
		}
		if (!stops.isEmpty()) {
			/* add stop postions */
			for (final Stop stop : stops) {
				xml.append(stop.asXMLString());
			}
		}
		xml.append("\n</linearGradient>");
		return xml.toString();
	}

	Shader getShader() {
		return shader;
	}

	@Override
	String asJSONString() {
		return null;
	}

}
