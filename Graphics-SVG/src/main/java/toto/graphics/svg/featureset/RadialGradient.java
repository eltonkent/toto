package toto.graphics.svg.featureset;

import org.xml.sax.Attributes;

import android.graphics.Shader;

public class RadialGradient extends Gradient {

	private float x, y, radius;

	private float zX, zY, zRadius;
	private android.graphics.RadialGradient shader;

	public RadialGradient() {
		super(false);
	}

	public void reload() {
		zX = getZoomFactor(x);
		zY = getZoomFactor(y);
		zRadius = getZoomFactor(radius);
		shader = new android.graphics.RadialGradient(zX, zY, zRadius,
				getColors(), getPositions(), Shader.TileMode.CLAMP);
		if (matrix != null) {
			shader.setLocalMatrix(matrix);
		}
	}

	@Override
	public void read(final Attributes atts) {
		super.read(atts);
		x = getFloatAttr("cx", atts, 0f);
		y = getFloatAttr("cy", atts, 0f);
		radius = getFloatAttr("r", atts, 0f);
	}

	public String asXMLString() {

		final StringBuilder xml = new StringBuilder();
		xml.append("<radialGradient");

		if (xlink != null) {
			xml.append(newLine);
			xml.append("xlink:href=\"" + xlink + "\"");
		}
		xml.append(newLine);
		xml.append("id=\"" + id + "\"");

		if (transform != null) {
			xml.append(newLine);
			xml.append("gradientTransform=\"" + transform + "\"");
		}
		xml.append(newLine);
		xml.append("cx=\"" + x + "\"");
		xml.append(newLine);
		xml.append("cy=\"" + y + "\"");
		xml.append(newLine);
		xml.append("r=\"" + radius + "\"");
		if (stops.isEmpty()) {
			xml.append("/>");
		} else {
			xml.append(">");
		}
		if (!stops.isEmpty()) {
			/* add stop postions */
			for (final Stop stop : stops) {
				xml.append(stop.asXMLString());
			}
		}
		xml.append("\n</radialGradient>");
		return xml.toString();
	}

	protected float getCX() {
		return x;
	}

	protected void setCX(final String x) {
		this.x = getFloatAttr(x);
	}

	protected float getCY() {
		return y;
	}

	protected void setCY(final String y) {
		this.y = getFloatAttr(y);
	}

	protected float getR() {
		return radius;
	}

	protected void setR(final String r) {
		this.radius = getFloatAttr(r);
	}

	@Override
	String asJSONString() {
		return null;
	}

	Shader getShader() {
		return shader;
	}

}
