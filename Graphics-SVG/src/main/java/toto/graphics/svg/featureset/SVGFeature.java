package toto.graphics.svg.featureset;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import org.xml.sax.Attributes;

import toto.graphics.svg.types.SVGReadWriteable;
import android.graphics.Matrix;
import android.util.Log;

public abstract class SVGFeature implements SVGReadWriteable {
	protected int zoomFactor = 100;

	protected static final String newLine = "\n\t";
	protected static final String newLineIntend1 = "\n\t\t";
	protected static final String newLineIntend2 = "\n\t\t\t";

	protected String id;

	public String getId() {
		return id;
	}

	protected void setId(final String id) {
		this.id = id;
	}

	protected static final String TAG = "RAGE:SVG";

	protected static final String STYLE = "style";

	/**
	 * SUPPORTED TRANSFORMATIONS
	 */
	private static final String TRANSFORM_MATRIX = "matrix(";

	private static final String TRANSFORM_TRANSLATE = "translate(";
	private static final String TRANSFORM_SCALE = "scale(";
	private static final String TRANSFORM_SKEW_X = "skewX(";
	private static final String TRANSFORM_SKEW_Y = "skewY(";
	private static final String TRANSFORM_ROTATE = "rotate(";

	protected Matrix matrix = null;

	/**
	 * Set matrix taking current zoom value into consideration
	 * 
	 * @param matrix
	 */
	public void setMatrix(final Matrix matrix) {
		this.matrix = matrix;
		reload();
	}

	protected Matrix parseTransform(String transfm) {
		if (transfm != null) {
			// Log.d(TAG, s);
			final Matrix matrix = new Matrix();
			while (true) {
				parseTransformItem(transfm, matrix);
				// Log.i(TAG, "Transformed: (" + s + ") " + matrix);
				final int rparen = transfm.indexOf(")");
				if (rparen > 0 && transfm.length() > rparen + 1) {
					transfm = transfm.substring(rparen + 1).replaceFirst(
							"[\\s,]*", "");
				} else {
					break;
				}
			}
		}
		// Log.d(TAG, matrix.toShortString());
		return matrix;
	}

	private Matrix parseTransformItem(final String s, final Matrix matrix) {
		if (s.startsWith(TRANSFORM_MATRIX)) {
			final NumberParser<Float> np = parseNumbers(s
					.substring(TRANSFORM_MATRIX.length()));
			if (np.size() == 6) {
				final Matrix mat = new Matrix();
				mat.setValues(new float[] {
						// Row 1
						np.get(0), np.get(2), getZoomFactor(np.get(4)),
						// Row 2
						np.get(1), np.get(3), getZoomFactor(np.get(5)),
						// Row 3
						0, 0, 1, });
				matrix.preConcat(mat);
			}
		} else if (s.startsWith(TRANSFORM_TRANSLATE)) {
			final NumberParser<Float> np = parseNumbers(s
					.substring(TRANSFORM_TRANSLATE.length()));
			if (np.size() > 0) {
				final float tx = getZoomFactor(np.get(0));
				float ty = 0;
				if (np.size() > 1) {
					ty = getZoomFactor(np.get(1));
				}
				matrix.preTranslate(tx, ty);
			}
		} else if (s.startsWith(TRANSFORM_SCALE)) {
			final NumberParser<Float> np = parseNumbers(s
					.substring(TRANSFORM_SCALE.length()));
			if (np.size() > 0) {
				final float sx = np.get(0);
				float sy = sx;
				if (np.size() > 1) {
					sy = np.get(1);
				}
				matrix.preScale(sx, sy);
			}
		} else if (s.startsWith(TRANSFORM_SKEW_X)) {
			final NumberParser<Float> np = parseNumbers(s
					.substring(TRANSFORM_SKEW_X.length()));
			if (np.size() > 0) {
				final float angle = np.get(0);
				matrix.preSkew((float) Math.tan(angle), 0);
			}
		} else if (s.startsWith(TRANSFORM_SKEW_Y)) {
			final NumberParser<Float> np = parseNumbers(s
					.substring(TRANSFORM_SKEW_Y.length()));
			if (np.size() > 0) {
				final float angle = np.get(0);
				matrix.preSkew(0, (float) Math.tan(angle));
			}
		} else if (s.startsWith(TRANSFORM_ROTATE)) {
			final NumberParser<Float> np = parseNumbers(s
					.substring(TRANSFORM_ROTATE.length()));
			if (np.size() > 0) {
				final float angle = np.get(0);
				float cx = 0;
				float cy = 0;
				if (np.size() > 2) {
					cx = getZoomFactor(np.get(1));
					cy = getZoomFactor(np.get(2));
				}
				matrix.preTranslate(cx, cy);
				matrix.preRotate(angle);
				matrix.preTranslate(-cx, -cy);
			}
		} else {
			Log.i(TAG, "Invalid transform (" + s + ")");
		}
		return matrix;
	}

	protected Float getFloatAttr(final String name,
			final Attributes attributes, final Float defaultValue) {
		return getFloatAttr(getStringAttr(name, attributes));

	}

	protected Float getFloatAttr(String v) {
		if (v == null) {
			return null;
		} else {
			if (v.endsWith("px")) {
				v = v.substring(0, v.length() - 2);
			} else if (v.endsWith("pt")) {
				v = v.substring(0, v.length() - 2);
				final float value = Float.parseFloat(v);
				/* convert points to pixels */
				return (value * 96) / 72;
			}
			// Log.d(TAG, "Float parsing '" + name + "=" + v + "'");
			return Float.parseFloat(v);
		}
	}

	protected String getStringAttr(final String name,
			final Attributes attributes) {
		final int n = attributes.getLength();
		for (int i = 0; i < n; i++) {
			if (attributes.getLocalName(i).equals(name)) {
				return attributes.getValue(i);
			}
		}
		return null;
	}

	protected Float getZoomFactor(Float value) {
		if (value == null)
			return null;
		value = value * zoomFactor;
		// value=value / 100;
		return value / 100;
	}

	protected NumberParser<Float> parseNumbers(final String s) {
		// Util.debug("Parsing numbers from: '" + s + "'");
		final int n = s.length();
		int p = 0;
		final ArrayList<Float> numbers = new ArrayList<Float>();
		boolean skipChar = false;
		for (int i = 1; i < n; i++) {
			if (skipChar) {
				skipChar = false;
				continue;
			}
			final char c = s.charAt(i);
			switch (c) {
			// This ends the parsing, as we are on the next element
			case 'M':
			case 'm':
			case 'Z':
			case 'z':
			case 'L':
			case 'l':
			case 'H':
			case 'h':
			case 'V':
			case 'v':
			case 'C':
			case 'c':
			case 'S':
			case 's':
			case 'Q':
			case 'q':
			case 'T':
			case 't':
			case 'a':
			case 'A':
			case ')': {
				final String str = s.substring(p, i);
				if (str.trim().length() > 0) {
					// Util.debug("  Last: " + str);
					final Float f = Float.parseFloat(str);
					numbers.add(f);
				}
				p = i;
				return new NumberParser<Float>(numbers, p);
			}
			case '\n':
			case '\t':
			case ' ':
			case ',': {
				final String str = s.substring(p, i);
				// Just keep moving if multiple whitespace
				if (str.trim().length() > 0) {
					// Util.debug("  Next: " + str);
					final Float f = Float.parseFloat(str);
					numbers.add(f);
					if (c == '-') {
						p = i;
					} else {
						p = i + 1;
						skipChar = true;
					}
				} else {
					p++;
				}
				break;
			}
			}
		}
		final String last = s.substring(p);
		if (last.length() > 0) {
			// Util.debug("  Last: " + last);
			try {
				numbers.add(Float.parseFloat(last));
			} catch (final NumberFormatException nfe) {
				// Just white-space, forget it
			}
			p = s.length();
		}
		return new NumberParser<Float>(numbers, p);
	}

	protected Float getFloatAttr(final String name, final Attributes attributes) {
		return getFloatAttr(name, attributes, null);
	}

	public int getZoomFactor() {
		return zoomFactor;
	}

	void setZoomFactor(final int zoomFactor) {
		this.zoomFactor = zoomFactor;
		reload();
	}

	abstract String asXMLString();

	abstract String asJSONString();

	abstract void reload();

	public void writeAsXML(final OutputStream os) throws IOException {
		os.write(asXMLString().getBytes());
	}

	public void writeAsJSON(final OutputStream os) throws IOException {
		os.write(asJSONString().getBytes());
	}

	@Override
	public void read(final Attributes atts) {
		id = getStringAttr("id", atts);
		String transFrm = getStringAttr("gradientTransform", atts);
		if (transFrm != null) {
			setTransform(transFrm);
		}
		transFrm = getStringAttr("transform", atts);
		if (transFrm != null) {
			setTransform(transFrm);
		}

	}

	protected String transform;

	public void setTransform(final String transform) {
		this.transform = transform;
		parseTransform(transform);
	}
}
