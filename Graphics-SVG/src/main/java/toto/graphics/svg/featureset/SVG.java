package toto.graphics.svg.featureset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import toto.graphics.svg.types.SVGRenderable;
import android.graphics.Bitmap;

public class SVG {
	private boolean isEditing;
	private int zoom;

	public Bitmap getBitmap() {
		return null;
	}

	public void setZoom(final int zoom) {
		this.zoom = zoom;
		rebuild();
	}

	public Editor edit() {
		isEditing = true;
		return new Editor(this);
	}

	private void rebuild() {

	}

	interface RenderSupport {

	}

	/**
	 * SVG object editor
	 * 
	 * @see SVG#edit()
	 * @author ekent4
	 * 
	 */
	public final class Editor {
		private final SVG inSvg;

		private Editor(final SVG instance) {
			inSvg = instance;
		}

		boolean isValidEditor = true;

		public void addGradient(final Gradient gradient) {
			if (!isValidEditor) {
				return;
			}
			if (gradient.getId() == null) {
				throw new RuntimeException(
						"Gradient has no id. A Gradient without an id is useless");
			}
			gradient.setZoomFactor(zoom);
			gradients.put(gradient.getId(), gradient);
		}

		public Gradient getGradient(final String id) {
			return gradients.get(id);
		}

		public void addPath(final Path path) {
			if (!isValidEditor) {
				return;
			}
			renderElements.add(path);
		}

		public void addLine(final Line line) {
			if (!isValidEditor) {
				return;
			}
			renderElements.add(line);
		}

		public void addRect(final Rect rect) {
			if (!isValidEditor) {
				return;
			}
			renderElements.add(rect);
		}

		public void addText(final Text text) {
			if (!isValidEditor) {
				return;
			}
			renderElements.add(text);
		}

		public void addPolyline(final Polyline polyline) {
			if (!isValidEditor) {
				return;
			}
			renderElements.add(polyline);
		}

		public void addEllipse(final Ellipse ellipse) {
			if (!isValidEditor) {
				return;
			}
			renderElements.add(ellipse);
		}

		public void addCircle(final Circle circle) {
			if (!isValidEditor) {
				return;
			}
			renderElements.add(circle);
		}

		public void addPolygon(final Polygon polygon) {
			if (!isValidEditor) {
				return;
			}
			renderElements.add(polygon);
		}

		public void addGroup(final G group) {
			if (!isValidEditor) {
				return;
			}
		}

		/**
		 * Editing the SVG is not possible after save is called.
		 * 
		 * @see SVG#edit()
		 * @return
		 */
		public void save() {
			isEditing = false;
			isValidEditor = false;
			rebuild();
		}
	}

	private final Map<String, Gradient> gradients = new HashMap<String, Gradient>();
	private final List<SVGRenderable> renderElements = new ArrayList<SVGRenderable>();

	private String version;
	private float width;
	private float height;
	private String id;

	public String getVersion() {
		return version;
	}

	public void setVersion(final String version) {
		this.version = version;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(final float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(final float height) {
		this.height = height;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public static SVG createfromXML() {
		return new SVG();
	}

	private SVG() {

	}

}
