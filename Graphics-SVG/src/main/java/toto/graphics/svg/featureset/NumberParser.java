package toto.graphics.svg.featureset;

import java.util.ArrayList;

/**
 * Simple list utility to iterate over Wrapper types
 * 
 * @author ekent4
 * 
 * @param <T>
 */
final class NumberParser<T extends Number> {
	final ArrayList<T> numbers;
	final int nextCmd;

	NumberParser(final ArrayList<T> numbers, final int nextCmd) {
		this.numbers = numbers;
		this.nextCmd = nextCmd;
	}

	int getNextCmd() {
		return nextCmd;
	}

	T getNumber(final int index) {
		return numbers.get(index);
	}

	int size() {
		return numbers.size();
	}

	T get(final int index) {
		return numbers.get(index);
	}
}
