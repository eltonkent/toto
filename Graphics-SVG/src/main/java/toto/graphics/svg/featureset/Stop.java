package toto.graphics.svg.featureset;

import org.xml.sax.Attributes;

import toto.graphics.svg.util.SVGColors;
import toto.graphics.svg.util.SVGStyleSet;
import android.graphics.Color;

public class Stop extends SVGFeature {
	float offset;

	int stopColor;
	int stopOpacity = 1;

	@Override
	String asXMLString() {
		final StringBuilder xml = new StringBuilder();
		xml.append(newLineIntend1);
		xml.append("<stop");
		xml.append(newLineIntend2);
		xml.append("offset=\"" + offset + "\"");
		xml.append(newLineIntend2);
		xml.append("style=\"");
		xml.append("stop-color:#");
		xml.append(Integer.toHexString(stopColor));
		xml.append(';');
		xml.append("stop-opacity:");
		xml.append(stopOpacity);
		xml.append(';');
		xml.append("\"/>");
		return xml.toString();
	}

	@Override
	void reload() {

	}

	private void processStopAttr(final String styles) {
		/* if the styles are declared in */
		final SVGStyleSet styleSet = new SVGStyleSet(styles);
		final String colorStyle = styleSet.getStyle("stop-color");
		if (colorStyle != null) {
			if (colorStyle.startsWith("#")) {
				stopColor = Integer.parseInt(colorStyle.substring(1), 16);
			} else {
				stopColor = Integer.parseInt(colorStyle, 16);
			}
		}
		final String opacityStyle = styleSet.getStyle("stop-opacity");
		if (opacityStyle != null) {
			final float alpha = Float.parseFloat(opacityStyle);
			final int alphaInt = Math.round(255 * alpha);
			stopColor |= (alphaInt << 24);
		} else {
			stopColor |= 0xFF000000;
		}
	}

	@Override
	public void read(final Attributes atts) {
		super.read(atts);
		/* is stop color or stop opacity */
		offset = getFloatAttr("offset", atts);
		final String styles = getStringAttr("style", atts);
		stopColor = Color.BLACK;
		/* sometimes the styles are declared outside */
		if (styles == null) {
			final String stopcolor = getStringAttr("stop-color", atts);
			if (stopcolor != null) {
				if (stopcolor.startsWith("#")) {
					stopColor = Integer.parseInt(stopcolor.substring(1), 16);
				} else {
					stopColor = SVGColors.mapColor(stopcolor);
				}
			}
		} else {
			processStopAttr(styles);
		}
	}

	@Override
	public void read(final String atts) {

	}

	@Override
	public void readComplete() {

	}

	public void setOffset(final String offset) {
		this.offset = getFloatAttr(offset);
	}

	public void setStyle(final String style) {
		processStopAttr(style);
	}

	protected float getOffset() {
		return offset;
	}

	protected int getStopColor() {
		return stopColor;
	}

	protected void setStopColor(final int stopColor) {
		this.stopColor = stopColor;
	}

	protected int getStopOpacity() {
		return stopOpacity;
	}

	protected void setStopOpacity(final int stopOpacity) {
		this.stopOpacity = stopOpacity;
	}

	@Override
	String asJSONString() {
		return null;
	}
}
