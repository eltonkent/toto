/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.graphics.svg;

import java.util.HashMap;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import toto.xc.xml.util.XMLUtils;

class IDHandler extends DefaultHandler {
	HashMap<String, String> idXml = new HashMap<String, String>();

	final class IdRecording {
		String id;
		int level;
		StringBuilder sb;

		IdRecording(final String id) {
			this.id = id;
			this.level = 0;
			this.sb = new StringBuilder();
		}
	}

	private final Stack<IdRecording> idRecordingStack = new Stack<IdRecording>();

	/**
	 * @param namespaceURI
	 *            (unused)
	 * @param qName
	 *            (unused)
	 */
	private void appendElementString(final StringBuilder sb,
			final String namespaceURI, final String localName,
			final String qName, final Attributes atts) {
		sb.append("<");
		sb.append(localName);
		for (int i = 0; i < atts.getLength(); i++) {
			sb.append(" ");
			sb.append(atts.getQName(i));
			sb.append("='");
			sb.append(XMLUtils.escapeXMLText(atts.getValue(i)));
			sb.append("'");
		}
		sb.append(">");
	}

	@Override
	public void startElement(final String namespaceURI, final String localName,
			final String qName, final Attributes atts) {
		final String id = atts.getValue("id");
		if (id != null) {
			final IdRecording ir = new IdRecording(id);
			idRecordingStack.push(ir);
		}
		if (idRecordingStack.size() > 0) {
			final IdRecording ir = idRecordingStack.lastElement();
			ir.level++;
			appendElementString(ir.sb, namespaceURI, localName, qName, atts);
		}
	}

	@Override
	public void endElement(final String namespaceURI, final String localName,
			final String qName) {
		if (idRecordingStack.size() > 0) {
			final IdRecording ir = idRecordingStack.lastElement();
			ir.sb.append("</");
			ir.sb.append(localName);
			ir.sb.append(">");
			ir.level--;
			if (ir.level == 0) {
				final String xml = ir.sb.toString();
				// Log.d(TAG, "Added element with id " + ir.id +
				// " and content: " + xml);
				idXml.put(ir.id, xml);
				idRecordingStack.pop();
				if (idRecordingStack.size() > 0) {
					idRecordingStack.lastElement().sb.append(xml);
				}
			}
		}
	}
}
