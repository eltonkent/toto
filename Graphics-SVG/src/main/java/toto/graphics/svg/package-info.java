/**
 * Fast and efficient implementation for the missing Scalable Vector Graphics (SVG) support in android.
 *
 */
package toto.graphics.svg;