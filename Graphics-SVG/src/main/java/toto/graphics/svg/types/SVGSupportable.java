package toto.graphics.svg.types;

import org.xml.sax.Attributes;

import android.graphics.Shader;

interface SVGSupportable {

	Shader getShader();

	/**
	 * Read embededded stop tags in support features.
	 * 
	 * @param atts
	 */
	void processStop(final Attributes atts);
}
