package toto.graphics.svg.types;

import java.io.IOException;
import java.io.OutputStream;

import org.xml.sax.Attributes;

/**
 * SVG features that support reading attributes, and serialization
 * 
 * @author ekent4
 * 
 */
public interface SVGReadWriteable {

	/**
	 * Process xml attributes for
	 * 
	 * @param atts
	 */
	void read(Attributes atts);

	/**
	 * read attributes from String.
	 * 
	 * @param atts
	 */
	void read(String atts);

	/**
	 * Called when the feature tag is closed. when reading xml.
	 */
	void readComplete();

	void writeAsXML(final OutputStream os) throws IOException;

	void writeAsJSON(final OutputStream os) throws IOException;
}
