package toto.graphics.svg.types;

import android.graphics.Canvas;

public interface SVGRenderable {

	/**
	 * Prepare the SVG feature.
	 * <p>
	 * Should be called before render.
	 * </p>
	 */
	void prepare();

	void render(Canvas canvas);
}
