/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.graphics.svg;

import toto.io.codec.Base64.OutputStream;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;

/**
 * Describes a vector Picture object, and optionally its bounds.
 * 
 */
public class SVG {

	public static class MetaData {
		public final String title;
		public final String description;

		MetaData(final String title, final String desc) {
			this.title = title;
			this.description = desc;
		}

		@Override
		public String toString() {
			return "MetaData [title=" + title + ", description=" + description
					+ "]";
		}

	}

	private final MetaData data;

	public MetaData getMetaData() {
		return data;
	}

	/**
	 * These are the bounds for the SVG specified as a hidden "bounds" layer in
	 * the SVG.
	 */
	private final RectF bounds;

	/**
	 * These are the estimated bounds of the SVG computed from the SVG elements
	 * while parsing. Note that this could be null if there was a failure to
	 * compute limits (ie. an empty SVG).
	 */
	private RectF limits = null;

	/**
	 * The parsed Picture object.
	 */
	private final Picture picture;

	/**
	 * Construct a new SVG.
	 * 
	 * @param picture
	 *            the parsed picture object.
	 * @param bounds
	 *            the bounds computed from the "bounds" layer in the SVG.
	 */
	SVG(final Picture picture, final MetaData data, final RectF bounds) {
		this.picture = picture;
		this.bounds = bounds;
		this.data = data;
	}

	/**
	 * Create a picture drawable from the SVG.
	 * 
	 * @return the PictureDrawable.
	 */
	public Drawable createDrawable() {
		return new PictureDrawable(picture);
		// return new PictureDrawable(picture) {
		// @Override
		// public int getIntrinsicWidth() {
		// if (bounds != null) {
		// return (int) bounds.width();
		// } else if (limits != null) {
		// return (int) limits.width();
		// } else {
		// return -1;
		// }
		// }
		//
		// @Override
		// public int getIntrinsicHeight() {
		// if (bounds != null) {
		// return (int) bounds.height();
		// } else if (limits != null) {
		// return (int) limits.height();
		// } else {
		// return -1;
		// }
		// }
		// };
	}

	public Bitmap createBitmap(final Bitmap.Config config) {
		final Drawable drawable = createDrawable();
		final Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight(), config);
		final Canvas canvas = new Canvas(bitmap);
		canvas.drawPicture(picture);
		return bitmap;
	}

	/**
	 * Write the rendered image to an ouput stream
	 * 
	 * @param os
	 */
	public void compress(final OutputStream os) {
		picture.writeToStream(os);
	}

	/**
	 * Gets the bounding rectangle for the SVG, if one was specified.
	 * 
	 * @return rectangle representing the bounds.
	 */
	public RectF getBounds() {
		return bounds;
	}

	/**
	 * Gets the bounding rectangle for the SVG that was computed upon parsing.
	 * It may not be entirely accurate for certain curves or transformations,
	 * but is often better than nothing.
	 * 
	 * @return rectangle representing the computed bounds.
	 */
	public RectF getLimits() {
		return limits;
	}

	/**
	 * Get the parsed SVG picture data.
	 * 
	 * @return the picture.
	 */
	public Picture getPicture() {
		return picture;
	}

	/**
	 * Set the limits of the SVG, which are the estimated bounds computed by the
	 * parser.
	 * 
	 * @param limits
	 *            the bounds computed while parsing the SVG, may not be entirely
	 *            accurate.
	 */
	void setLimits(final RectF limits) {
		this.limits = limits;
	}
}
