package toto.graphics.svg.util;

import java.util.HashMap;

public final class SVGStyleSet {
	private final HashMap<String, String> styleMap = new HashMap<String, String>();

	public SVGStyleSet(final String string) {
		final String[] styles = string.split(";");
		for (final String s : styles) {
			final String[] style = s.split(":");
			if (style.length == 2) {
				styleMap.put(style[0], style[1]);
			}
		}
	}

	public String getStyle(final String name) {
		return styleMap.get(name);
	}
}