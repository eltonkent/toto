package toto.xc.bson;

import java.io.UnsupportedEncodingException;

/**
 * Utility class for reading BSON object data from byte array
 * 
 */
class InputBuffer {
	private final byte[] data;
	private final int offset;
	private int position;
	private final int limit;

	private InputBuffer(final byte[] data, final int offset, final int limit) {
		this.data = data;
		this.position = 0;
		this.offset = offset;
		this.limit = limit;
	}

	/**
	 * Reads one byte from buffer
	 */
	byte read() {
		ensure(1);

		final byte result = data[offset + position];
		position += 1;

		return result;
	}

	/**
	 * Reads 4 bytes from buffer as integer value
	 */
	int readInt() {
		ensure(4);

		int result = 0;
		for (int i = 0; i < 4; ++i) {
			result |= (0xFF & data[offset + position + i]) << (i * 8);
		}
		position += 4;

		return result;
	}

	/**
	 * Reads 8 bytes from buffer as long value
	 */
	long readLong() {
		ensure(8);

		long result = 0;
		for (int i = 0; i < 8; ++i) {
			result |= (0xFFL & data[offset + position + i]) << (i * 8);
		}
		position += 8;

		return result;
	}

	/**
	 * Reads bytes from buffer
	 * 
	 * @param count
	 *            count of bytes to read
	 */
	byte[] readBytes(final int count) {
		ensure(count);
		final byte[] bytes = new byte[count];
		System.arraycopy(data, offset + position, bytes, 0, count);
		position += count;

		return bytes;
	}

	/**
	 * Reads c-string (null-terminated) from buffer
	 */
	String readString() {
		return readString(0);
	}

	/**
	 * Reads c-string from buffer with specified length
	 */
	String readString(int length) {
		if (length > 0) {
			ensure(length);
			if ((byte) 0x00 != data[offset + position + length - 1]) {
				throw new BSONException("unexpected end of string");
			}

			try {
				final String s = new String(data, offset + position,
						length - 1, "UTF-8");
				position += length;
				return s;
			} catch (final UnsupportedEncodingException e) {
				throw new BSONException("can not decode string", e);
			}
		}

		length = 0;
		while (position + length < limit
				&& data[offset + position + length] != (byte) 0x00) {
			++length;
		}

		if (position + length > limit) {
			throw new BSONException("unexpected end of string");
		}

		final String s = new String(data, offset + position, length);
		position += length + 1;
		return s;
	}

	/**
	 * Returns <code>true</code> if any bytes available to read from buffer or
	 * <code>false</code> otherwise
	 * 
	 * @return <code>true</code> if any bytes available to read from buffer or
	 *         <code>false</code> otherwise
	 */
	boolean isAvailable() {
		return position < limit;
	}

	/**
	 * Get sub buffer with specified length
	 */
	InputBuffer subBuffer(final int limit) {
		ensure(limit);

		final InputBuffer result = new InputBuffer(data, offset + position,
				limit);
		position += limit;

		return result;
	}

	/**
	 * Checks is buffer contains needed bytes
	 */
	protected void ensure(final int size) {
		if (size > limit - position) {
			throw new BSONException(
					"can not allocate sub buffer: not enought bytes");
		}
	}

	/**
	 * Creates {@link InputBuffer} from byte array
	 */
	static InputBuffer createFromByteArray(final byte[] data) {
		return new InputBuffer(data, 0, data.length);
	}
}
