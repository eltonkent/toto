package toto.xc.bson;

import java.util.Date;
import java.util.regex.Pattern;

/**
 * Default BSON object decoder (from plain byte array to Java object)
 * 
 */
class BSONDecoder {

	private InputBuffer input;

	/**
	 * Decode BSON object
	 * 
	 * @throws IllegalStateException
	 *             if other decoding process active with this decoder
	 */
	BSONObject decode(final byte[] data) throws IllegalStateException {
		if (isBusy()) {
			throw new IllegalStateException("other decoding in process");
		}

		if (data == null) {
			throw new IllegalArgumentException("can not read object from null");
		}

		input = InputBuffer.createFromByteArray(data);
		final BSONObject result = read();
		input = null;

		return result;
	}

	/**
	 * Returns <code>true</code> if decoder is currently in use
	 * 
	 * @return <code>true</code> if decoder is currently in use
	 */
	boolean isBusy() {
		return input != null;
	}

	protected BSONObject read() {
		final int length = input.readInt();

		final BSONObject result = this.readObject(input.subBuffer(length - 5));

		if (0x00 != input.read()) {
			throw new BSONException("unexpected end of document");
		}

		return result;
	}

	protected BSONObject readObject(final InputBuffer input) {
		final BSONObject result = new BSONObject();
		while (input.isAvailable()) {
			final byte type = input.read();
			final String name = input.readString();
			switch (type) {
			case BSON.DOUBLE:
				result.put(name, Double.longBitsToDouble(input.readLong()));
				break;

			case BSON.STRING:
				final int strlen = input.readInt();
				result.put(name, input.readString(strlen));
				break;

			case BSON.OBJECT:
				final int objlen = input.readInt();
				result.put(name, readObject(input.subBuffer(objlen - 5)));
				input.read();
				break;

			case BSON.ARRAY:
				final int arrlen = input.readInt();
				final BSONObject arrObject = readObject(input
						.subBuffer(arrlen - 5));
				final Object[] array = new Object[arrObject.size()];
				for (int i = 0; i < array.length; ++i) {
					array[i] = arrObject.get(String.valueOf(i));
				}
				result.put(name, array);
				input.read();
				break;

			case BSON.BINARY:
				final int binlen = input.readInt();
				final byte subtype = input.read();
				if (0x00 != subtype) {
					throw new BSONException("unexpected binary type: "
							+ subtype);
				}
				result.put(name, input.readBytes(binlen));
				break;

			case BSON.OBJECT_ID:
				result.put(name, new ObjectId(input.readBytes(12)));
				break;

			case BSON.BOOLEAN:
				final byte bvalue = input.read();
				if (0x00 != bvalue && 0x01 != bvalue) {
					throw new BSONException("unexpected boolean value");
				}
				result.put(name, 0x01 == bvalue);
				break;

			case BSON.DATE:
				result.put(name, new Date(input.readLong()));
				break;

			case BSON.NULL:
				result.put(name, null);
				break;

			case BSON.REGEX:
				// noinspection MagicConstant
				result.put(name, Pattern.compile(input.readString(),
						RegexFlag.stringToRegexFlags(input.readString())));
				break;

			case BSON.INT:
				result.put(name, input.readInt());
				break;

			case BSON.LONG:
				result.put(name, input.readLong());
				break;

			default:
				throw new BSONException("unexpected type: " + type);
			}
		}

		return result;
	}
}
