package toto.xc.bson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Util class for convert Java regex flags to BSON string and conversely
 * 
 */
public final class RegexFlag {
	private static List<RegexFlag> regexFlags;
	private static Map<Character, RegexFlag> characterToRegexFlagMap;

	private final int flag;
	private final char character;
	private final boolean supported;

	private RegexFlag(final int flag, final char character,
			final boolean supported) {
		this.flag = flag;
		this.character = character;
		this.supported = supported;
	}

	/**
	 * Convert Java regex flags to BSON string
	 */
	public static String regexFlagsToString(final int flags) {
		final StringBuilder result = new StringBuilder();
		for (final RegexFlag rf : regexFlags) {
			if ((flags & rf.getFlag()) > 0 && rf.isSupported()) {
				result.append(rf.getCharacter());
			}
		}

		return result.toString();
	}

	/**
	 * Read Java regex flags from BSON string
	 */
	public static int stringToRegexFlags(final String str) {
		int flags = 0;

		for (int i = 0; i < str.length(); ++i) {
			final RegexFlag rf = characterToRegexFlagMap.get(str.charAt(i));
			if (rf != null && rf.isSupported()) {
				flags |= rf.getFlag();
			}
		}

		return flags;
	}

	/**
	 * Register flag conversation rules
	 */
	protected static void registerRegexFlag(final int flag,
			final char character, final boolean supported) {
		final RegexFlag rf = new RegexFlag(flag, character, supported);
		regexFlags.add(rf);
		characterToRegexFlagMap.put(rf.getCharacter(), rf);
	}

	/**
	 * Returns Java flag
	 * 
	 * @return Java flag
	 */
	public int getFlag() {
		return flag;
	}

	/**
	 * Returns BSON character for associated Java regex flag
	 * 
	 * @return BSON character for associated Java regex flag
	 */
	public char getCharacter() {
		return character;
	}

	/**
	 * Returns <code>true</code> if BSON supported current Java flag
	 * 
	 * @return <code>true</code> if BSON supported current Java flag
	 */
	public boolean isSupported() {
		return supported;
	}

	static {
		RegexFlag.regexFlags = new ArrayList<RegexFlag>();
		RegexFlag.characterToRegexFlagMap = new HashMap<Character, RegexFlag>();

		RegexFlag.registerRegexFlag(Pattern.CASE_INSENSITIVE, 'i', true);
		RegexFlag.registerRegexFlag(Pattern.MULTILINE, 'm', true);
		RegexFlag.registerRegexFlag(Pattern.COMMENTS, 'x', true);
		RegexFlag.registerRegexFlag(Pattern.DOTALL, 's', true);
		RegexFlag.registerRegexFlag(Pattern.UNICODE_CASE, 'u', true);
		RegexFlag.registerRegexFlag(Pattern.CANON_EQ, 'c', false);
		RegexFlag.registerRegexFlag(Pattern.LITERAL, 't', false);
		RegexFlag.registerRegexFlag(Pattern.UNIX_LINES, 'd', false);

		Collections.sort(RegexFlag.regexFlags, new Comparator<RegexFlag>() {
			public int compare(final RegexFlag o1, final RegexFlag o2) {
				return o1.getCharacter() - o2.getCharacter();
			}
		});
	}
}
