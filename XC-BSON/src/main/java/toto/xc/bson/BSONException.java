package toto.xc.bson;

/**
 */
public class BSONException extends RuntimeException {
	public BSONException() {
		super();
	}

	public BSONException(final String message) {
		super(message);
	}

	public BSONException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public BSONException(final Throwable cause) {
		super(cause);
	}
}
