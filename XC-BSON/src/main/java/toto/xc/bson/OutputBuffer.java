package toto.xc.bson;

import java.io.UnsupportedEncodingException;

/**
 * Utility class for serialize BSON object
 * 
 */
class OutputBuffer {
	public static final int BUFFER_DEFAULT_LENGTH = 512;

	private byte[] buffer;
	private int position;
	private int actualSize;

	{
		buffer = new byte[BUFFER_DEFAULT_LENGTH];
		position = 0;
		actualSize = 0;
	}

	/**
	 * Returns current position in output
	 * 
	 * @return current position in output
	 */
	int getPosition() {
		return position;
	}

	/**
	 * Sets position
	 */
	void setPosition(final int position) {
		this.position = position;
	}

	/**
	 * Returns actual (full) size of buffer (currently writed bytes)
	 * 
	 * @return actual (full) size of buffer (currently writed bytes)
	 */
	int getActualSize() {
		return actualSize;
	}

	/**
	 * Returns buffer as byte array
	 * 
	 * @return buffer as byte array
	 */
	byte[] getResult() {
		final byte[] result = new byte[getActualSize()];
		System.arraycopy(buffer, 0, result, 0, getActualSize());
		return result;
	}

	/**
	 * Writes single byte to buffer
	 */
	void write(final byte data) {
		ensureLength(1);
		buffer[position++] = data;
		actualSize = Math.max(actualSize, position);
	}

	/**
	 * Writes byte array to buffer
	 */
	void write(final byte[] data) {
		this.write(data, 0, data.length);
	}

	/**
	 * Writes part of byte array to buffer
	 * 
	 * @param data
	 *            source byte array
	 * @param offset
	 *            start position in source
	 * @param length
	 *            count bytes to write
	 */
	void write(final byte[] data, final int offset, final int length) {
		ensureLength(length);
		System.arraycopy(data, offset, buffer, position, length);
		position += length;
		actualSize = Math.max(actualSize, position);
	}

	/**
	 * Writes integer value (4 bytes) at specified position
	 * 
	 * @param position
	 *            position to write
	 * @param value
	 *            value
	 */
	void writeIntAt(final int position, final int value) {
		final int save = getPosition();
		setPosition(position);
		writeInt(value);
		setPosition(save);
	}

	/**
	 * Writes integer value to buffer as 4 bytes
	 */
	void writeInt(final int value) {
		this.write(new byte[] { (byte) ((value >>> 0) & 0xFF),
				(byte) ((value >>> 8) & 0xFF), (byte) ((value >>> 16) & 0xFF),
				(byte) ((value >>> 24) & 0xFF) });
	}

	/**
	 * Writes long value to buffer as 8 bytes
	 */
	void writeLong(final long value) {
		this.write(new byte[] { (byte) ((value >>> 0) & 0xFF),
				(byte) ((value >>> 8) & 0xFF), (byte) ((value >>> 16) & 0xFF),
				(byte) ((value >>> 24) & 0xFF), (byte) ((value >>> 32) & 0xFF),
				(byte) ((value >>> 40) & 0xFF), (byte) ((value >>> 48) & 0xFF),
				(byte) ((value >>> 56) & 0xFF), });
	}

	/**
	 * Writes double value to buffers as 8 bytes
	 */
	public void writeDouble(final double value) {
		this.writeLong(Double.doubleToRawLongBits(value));
	}

	/**
	 * Writes {@link String} to buffer as c-style string (null-terminated)
	 * 
	 * @return count of writed bytes
	 */
	int writeString(final String value) {
		final int start = getPosition();
		try {
			this.write(value.getBytes("UTF-8"));
		} catch (final UnsupportedEncodingException e) {
			throw new BSONException("can not encode string", e);
		}
		this.write((byte) 0x00);

		return getPosition() - start;
	}

	/**
	 * Checks internal array size to hold needed data and expand it if need.
	 */
	protected void ensureLength(final int need) {
		if (need <= buffer.length - position) {
			return;
		}

		final int newSize = (int) Math
				.floor(((double) (need + position - buffer.length))
						/ BUFFER_DEFAULT_LENGTH)
				* BUFFER_DEFAULT_LENGTH;
		final byte[] newBuffer = new byte[newSize];
		System.arraycopy(buffer, 0, newBuffer, 0, position);
		buffer = newBuffer;
	}
}
