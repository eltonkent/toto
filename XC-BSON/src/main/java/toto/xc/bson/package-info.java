/**
 * Binary JSON utilities.
 * <p>
 * <b>Creating a BSON Object</b>
 * <code>
 * <pre>
 * BSONObject parrot2 = new BSONObject();
 * parrot2.put("name", "Bounty");
 * parrot2.put("type", "Cockatoo");
 * parrot2.put("male", false);
 * parrot2.put("age", 15);
 * parrot2.put("birthdate", calendar.getTime());
 * parrot2.put("likes", new String[]{"sugar cane"});
 * parrot2.put("extra1", null);
 * </pre>
 * </code>
 * <b>Encoding BSON</b>
 * <code>
 * <pre>
 * byte[] bsonData=BSON.encode(parrot2);
 * </pre>
 * </code>
 * <b>Decoding BSON</b>
 * <code>
 * <pre>
 * BSONObject parrot2=BSON.decode(bsonData);
 * </pre>
 * </code>
 * </p>
 */
package toto.xc.bson;