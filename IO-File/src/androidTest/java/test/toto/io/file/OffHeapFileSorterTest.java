package test.toto.io.file;

import java.io.File;
import java.io.IOException;

import test.toto.ToToTestCase;
import toto.io.file.FileUtils;
import toto.io.file.OffHeapFileSorter;
import toto.io.file.SDCardUtils;

public class OffHeapFileSorterTest extends ToToTestCase {
	String path = SDCardUtils.getDirectory().toString() + "/"+TEST_DIR_NAME+"/FileSort/";

	public OffHeapFileSorterTest() {
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
	}

	public void testSimpleSort() {
		StringBuilder test=new StringBuilder();
		test.append("aaa");
		String testString = "e\nz\na\nc";
		File tempFile = new File(path + "temp");
		try {
			FileUtils.writeByteArrayToFile(testString.getBytes(), tempFile,
					null);
			File outFile=new File(path+"out");
			OffHeapFileSorter.sort(tempFile, outFile);
			byte[] data=FileUtils.readFileToByteArray(outFile);
			String op=new String(data);
			assertEquals("a\nc\ne\nz\n", op);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
