#include <FileUtils.h>

#include <stdio.h>
#include <jni.h>
#include <android/log.h>


#define  LOG_TAG    "ToTo_JNI"
#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

JNIEXPORT jint JNICALL Java_toto_io_file_FileUtils_writeToFile(
		JNIEnv *env, jclass, jstring fileName, jstring content,
		jint contentSize, jboolean append) {
	jboolean isCopy = false;
	const char *fileC = env->GetStringUTFChars(content, &isCopy);
	int ret=writeToFile(env,fileName,contentSize,sizeof(char),append,fileC);
	env->ReleaseStringUTFChars(content, fileC);
	return ret;
}

int writeToFile(JNIEnv *env, jstring  fileName, jint contentSize, int size, jboolean append,
		 const void *fileC) {
	jboolean isCopy = false;
	const char *fileN = env->GetStringUTFChars(fileName, &isCopy);
	char* writeMode;
	if (append) {
		//append to file
		ALOG("Appending string to path: %s", fileN);
		writeMode = "a";
	} else {
		ALOG("Writing string to path: %s", fileN);
		// create and overwrite
		writeMode = "w";
	}
	FILE *fp = fopen(fileN, writeMode);
	if (fp == NULL) {
		ALOG("Could not write to file. Probably because of Manifest Permissions / Directory non existent / File already open");
		env->ReleaseStringUTFChars(fileName, fileN);
		return -1;
	}
	int written = fwrite(fileC, contentSize, size, fp);
	fclose(fp);
	env->ReleaseStringUTFChars(fileName, fileN);
	return written;
}
//currently faster
////	ALOG("Writing file to path: %s",fileN);
//	int filedesc = open(fileN, openFlags, S_IRUSR | S_IRGRP | S_IROTH);
//	if (filedesc < 0) {
////		ALOG("Could not write to file. Probably because of Manifest Permissions / Directory non existent / File already open");
//		env->ReleaseStringUTFChars(fileName, fileN);
//		env->ReleaseStringUTFChars(content, fileC);
//		return -1;
//	}
//	int written = write(filedesc, fileC, contentSize);
////	int i=100;
////	write(filedesc, &i, sizeof(i));
////	ALOG("%d chars written. Now closing file.",written);
//	close(filedesc);
//	env->ReleaseStringUTFChars(fileName, fileN);
//	env->ReleaseStringUTFChars(content, fileC);
//	return written;

JNIEXPORT jint JNICALL Java_toto_io_file_FileUtils_writeIntToFile(
		JNIEnv *env, jclass, jstring fileName, jintArray content,
		jint contentSize, jboolean append) {
	jboolean isCopy = false;
	jint *jarr = env->GetIntArrayElements(content, &isCopy);
	int written = writeToFile(env,fileName,contentSize,sizeof(int),append,jarr);
	env->ReleaseIntArrayElements(content, jarr, 0);
	return written;
}

JNIEXPORT jint JNICALL Java_toto_io_file_FileUtils_writeByteToFile(
		JNIEnv *env, jclass, jstring fileName, jbyteArray content,
		jint contentSize, jboolean append) {
	jboolean isCopy = false;
	jbyte *jarr = (jbyte *)env->GetByteArrayElements(content, &isCopy);
	int written = writeToFile(env,fileName,contentSize,sizeof(jbyte),append,jarr);
	env->ReleaseByteArrayElements(content, jarr, 0);
	return written;
}

