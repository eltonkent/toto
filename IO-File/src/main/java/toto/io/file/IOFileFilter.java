/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;

/**
 * An interface which brings the FileFilter and FilenameFilter interfaces
 * together.
 * 
 * @since 1.0
 * @version $Id$
 */
public interface IOFileFilter extends FileFilter, FilenameFilter {

	/**
	 * Checks to see if the File should be accepted by this filter.
	 * <p>
	 * Defined in {@link java.io.FileFilter}.
	 * 
	 * @param file
	 *            the File to check
	 * @return true if this file matches the test
	 */
	@Override
	boolean accept(File file);

	/**
	 * Checks to see if the File should be accepted by this filter.
	 * <p>
	 * Defined in {@link java.io.FilenameFilter}.
	 * 
	 * @param dir
	 *            the directory File to check
	 * @param name
	 *            the filename within the directory to check
	 * @return true if this file matches the test
	 */
	@Override
	boolean accept(File dir, String name);

}
