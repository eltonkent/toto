package toto.io.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.zip.Deflater;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Sorts files without completely loading them on the heap.
 * <p>
 * </p>
 */
public class OffHeapFileSorter {

	/**
	 * This method calls the garbage collector and then returns the free memory.
	 * This avoids problems with applications where the GC hasn't reclaimed
	 * memory and reports no available memory.
	 * 
	 * @return available memory
	 */
	public static long estimateAvailableMemory() {
		System.gc();
		return Runtime.getRuntime().freeMemory();
	}

	/**
	 * we divide the file into small blocks. If the blocks are too small, we
	 * will be creating too many temporary files. If they are too big, we will
	 * be using too much memory.
	 * 
	 * @param sizeoffile
	 *            how much data (in bytes) can we expect
	 * @param maxtmpfiles
	 *            how many temporary files can we create (e.g., 1024)
	 * @param maxMemory
	 *            Maximum memory to use (in bytes)
	 * @return the estimate
	 */
	public static long estimateBestSizeOfBlocks(final long sizeoffile,
			final int maxtmpfiles, final long maxMemory) {
		// we don't want to open up much more than maxtmpfiles temporary
		// files, better run
		// out of memory first.
		long blocksize = sizeoffile / maxtmpfiles
				+ (sizeoffile % maxtmpfiles == 0 ? 0 : 1);

		// on the other hand, we don't want to create many temporary
		// files
		// for naught. If blocksize is smaller than half the free
		// memory, grow it.
		if (blocksize < maxMemory / 2) {
			blocksize = maxMemory / 2;
		}
		return blocksize;
	}

	/**
	 * This merges several BinaryFileBuffer to an output writer.
	 * 
	 * @param fbw
	 *            A buffer where we write the data.
	 * @param cmp
	 *            A comparator object that tells us how to sort the lines.
	 * @param distinct
	 *            Pass <code>true</code> if duplicate lines should be discarded.
	 * @param buffers
	 *            Where the data should be read.
	 * @return The number of lines sorted.
	 * @throws IOException
	 * 
	 */
	public static int mergeSortedFiles(final BufferedWriter fbw,
			final Comparator<String> cmp, final boolean distinct,
			final List<BinaryFileBuffer> buffers) throws IOException {
		final PriorityQueue<BinaryFileBuffer> pq = new PriorityQueue<BinaryFileBuffer>(
				11, new Comparator<BinaryFileBuffer>() {
					@Override
					public int compare(final BinaryFileBuffer i,
							final BinaryFileBuffer j) {
						return cmp.compare(i.peek(), j.peek());
					}
				});
		for (final BinaryFileBuffer bfb : buffers)
			if (!bfb.empty())
				pq.add(bfb);
		int rowcounter = 0;
		String lastLine = null;
		try {
			while (pq.size() > 0) {
				final BinaryFileBuffer bfb = pq.poll();
				final String r = bfb.pop();
				// Skip duplicate lines
				if (!distinct || !r.equals(lastLine)) {
					fbw.write(r);
					fbw.newLine();
					lastLine = r;
				}
				++rowcounter;
				if (bfb.empty()) {
					bfb.fbr.close();
				} else {
					pq.add(bfb); // add it back
				}
			}
		} finally {
			fbw.close();
			for (final BinaryFileBuffer bfb : pq)
				bfb.close();
		}
		return rowcounter;

	}

	/**
	 * This merges a bunch of temporary flat files
	 * 
	 * @param files
	 *            files to be merged
	 * @param outputfile
	 *            output file
	 * @return The number of lines sorted.
	 * @throws IOException
	 */
	public static int mergeSortedFiles(final List<File> files,
			final File outputfile) throws IOException {
		return mergeSortedFiles(files, outputfile, defaultcomparator,
				Charset.defaultCharset());
	}

	/**
	 * This merges a list of temporary flat files.
	 * 
	 * @param files
	 * @param outputfile
	 * @param cmp
	 * @return The number of lines sorted.
	 * @throws IOException
	 */
	public static int mergeSortedFiles(final List<File> files,
			final File outputfile, final Comparator<String> cmp)
			throws IOException {
		return mergeSortedFiles(files, outputfile, cmp,
				Charset.defaultCharset());
	}

	/**
	 * This merges a bunch of temporary flat files
	 * 
	 * @param files
	 * @param outputfile
	 * @param cmp
	 * @param distinct
	 * @return The number of lines sorted.
	 * @throws IOException
	 */
	public static int mergeSortedFiles(final List<File> files,
			final File outputfile, final Comparator<String> cmp,
			final boolean distinct) throws IOException {
		return mergeSortedFiles(files, outputfile, cmp,
				Charset.defaultCharset(), distinct);
	}

	/**
	 * This merges a bunch of temporary flat files
	 * 
	 * @param files
	 * @param outputfile
	 * @param cmp
	 * @param cs
	 *            character set to use to load the strings
	 * @return The number of lines sorted.
	 * @throws IOException
	 */
	public static int mergeSortedFiles(final List<File> files,
			final File outputfile, final Comparator<String> cmp,
			final Charset cs) throws IOException {
		return mergeSortedFiles(files, outputfile, cmp, cs, false);
	}

	/**
	 * This merges a bunch of temporary flat files
	 * 
	 * @param files
	 *            The {@link List} of sorted {@link File}s to be merged.
	 * @param distinct
	 *            Pass <code>true</code> if duplicate lines should be discarded.
	 * @param outputfile
	 *            The output {@link File} to merge the results to.
	 * @param cmp
	 *            The {@link Comparator} to use to compare {@link String}s.
	 * @param cs
	 *            The {@link Charset} to be used for the byte to character
	 *            conversion.
	 * @return The number of lines sorted.
	 * @throws IOException
	 * 
	 */
	public static int mergeSortedFiles(final List<File> files,
			final File outputfile, final Comparator<String> cmp,
			final Charset cs, final boolean distinct) throws IOException {
		return mergeSortedFiles(files, outputfile, cmp, cs, distinct, false,
				false);
	}

	/**
	 * This merges a bunch of temporary flat files
	 * 
	 * @param files
	 *            The {@link List} of sorted {@link File}s to be merged.
	 * @param distinct
	 *            Pass <code>true</code> if duplicate lines should be discarded.
	 * @param outputfile
	 *            The output {@link File} to merge the results to.
	 * @param cmp
	 *            The {@link Comparator} to use to compare {@link String}s.
	 * @param cs
	 *            The {@link Charset} to be used for the byte to character
	 *            conversion.
	 * @param append
	 *            Pass <code>true</code> if result should append to {@link File}
	 *            instead of overwrite. Default to be false for overloading
	 *            methods.
	 * @param usegzip
	 *            assumes we used gzip compression for temporary files
	 * @return The number of lines sorted.
	 * @throws IOException
	 */
	public static int mergeSortedFiles(final List<File> files,
			final File outputfile, final Comparator<String> cmp,
			final Charset cs, final boolean distinct, final boolean append,
			final boolean usegzip) throws IOException {
		final ArrayList<BinaryFileBuffer> bfbs = new ArrayList<BinaryFileBuffer>();
		for (final File f : files) {
			final int BUFFERSIZE = 2048;
			final InputStream in = new FileInputStream(f);
			BufferedReader br;
			if (usegzip) {
				br = new BufferedReader(new InputStreamReader(
						new GZIPInputStream(in, BUFFERSIZE), cs));
			} else {
				br = new BufferedReader(new InputStreamReader(in, cs));
			}

			final BinaryFileBuffer bfb = new BinaryFileBuffer(br);
			bfbs.add(bfb);
		}
		final BufferedWriter fbw = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(outputfile, append), cs));
		final int rowcounter = mergeSortedFiles(fbw, cmp, distinct, bfbs);
		for (final File f : files)
			f.delete();
		return rowcounter;
	}

	/**
	 * This sorts a file (input) to an output file (output) using default
	 * parameters
	 * 
	 * @param input
	 *            source file
	 * @param output
	 *            output file
	 * @throws IOException
	 */
	public static void sort(final File input, final File output)
			throws IOException {
		mergeSortedFiles(sortInBatch(input), output);
	}

	/**
	 * Sort a list and save it to a temporary file
	 * 
	 * @return the file containing the sorted data
	 * @param tmplist
	 *            data to be sorted
	 * @param cmp
	 *            string comparator
	 * @param cs
	 *            charset to use for output (can use Charset.defaultCharset())
	 * @param tmpdirectory
	 *            location of the temporary files (set to null for default
	 *            location)
	 * @throws IOException
	 */
	public static File sortAndSave(final List<String> tmplist,
			final Comparator<String> cmp, final Charset cs,
			final File tmpdirectory) throws IOException {
		return sortAndSave(tmplist, cmp, cs, tmpdirectory, false, false);
	}

	/**
	 * Sort a list and save it to a temporary file
	 * 
	 * @return the file containing the sorted data
	 * @param tmplist
	 *            data to be sorted
	 * @param cmp
	 *            string comparator
	 * @param cs
	 *            charset to use for output (can use Charset.defaultCharset())
	 * @param tmpdirectory
	 *            location of the temporary files (set to null for default
	 *            location)
	 * @param distinct
	 *            Pass <code>true</code> if duplicate lines should be discarded.
	 * @param usegzip
	 *            set to true if you are using gzip compression for the
	 *            temporary files
	 * @throws IOException
	 */
	public static File sortAndSave(final List<String> tmplist,
			final Comparator<String> cmp, final Charset cs,
			final File tmpdirectory, final boolean distinct,
			final boolean usegzip) throws IOException {
		Collections.sort(tmplist, cmp);
		final File newtmpfile = File.createTempFile("sortInBatch", "flatfile",
				tmpdirectory);
		newtmpfile.deleteOnExit();
		OutputStream out = new FileOutputStream(newtmpfile);
		final int ZIPBUFFERSIZE = 2048;
		if (usegzip)
			out = new GZIPOutputStream(out, ZIPBUFFERSIZE) {
				{
					this.def.setLevel(Deflater.BEST_SPEED);
				}
			};
		final BufferedWriter fbw = new BufferedWriter(new OutputStreamWriter(
				out, cs));
		String lastLine = null;
		try {
			for (final String r : tmplist) {
				// Skip duplicate lines
				if (!distinct || !r.equals(lastLine)) {
					fbw.write(r);
					fbw.newLine();
					lastLine = r;
				}
			}
		} finally {
			fbw.close();
		}
		return newtmpfile;
	}

	/**
	 * This will simply load the file by blocks of lines, then sort them
	 * in-memory, and write the result to temporary files that have to be merged
	 * later.
	 * 
	 * @param fbr
	 *            data source
	 * @param datalength
	 *            estimated data volume (in bytes)
	 * @return a list of temporary flat files
	 * @throws IOException
	 */
	public static List<File> sortInBatch(final BufferedReader fbr,
			final long datalength) throws IOException {
		return sortInBatch(fbr, datalength, defaultcomparator,
				DEFAULTMAXTEMPFILES, estimateAvailableMemory(),
				Charset.defaultCharset(), null, false, 0, false);
	}

	/**
	 * This will simply load the file by blocks of lines, then sort them
	 * in-memory, and write the result to temporary files that have to be merged
	 * later.
	 * 
	 * @param fbr
	 *            data source
	 * @param datalength
	 *            estimated data volume (in bytes)
	 * @param cmp
	 *            string comparator
	 * @param distinct
	 *            Pass <code>true</code> if duplicate lines should be discarded.
	 * @return a list of temporary flat files
	 * @throws IOException
	 */
	public static List<File> sortInBatch(final BufferedReader fbr,
			final long datalength, final Comparator<String> cmp,
			final boolean distinct) throws IOException {
		return sortInBatch(fbr, datalength, cmp, DEFAULTMAXTEMPFILES,
				estimateAvailableMemory(), Charset.defaultCharset(), null,
				distinct, 0, false);
	}

	/**
	 * @param fbr
	 *            data source
	 * @param datalength
	 *            estimated data volume (in bytes)
	 * @param cmp
	 *            string comparator
	 * @param maxtmpfiles
	 *            maximal number of temporary files
	 * @param maxMemory
	 *            maximum amount of memory to use (in bytes)
	 * @param cs
	 *            character set to use (can use Charset.defaultCharset())
	 * @param tmpdirectory
	 *            location of the temporary files (set to null for default
	 *            location)
	 * @param distinct
	 *            Pass <code>true</code> if duplicate lines should be discarded.
	 * @param numHeader
	 *            number of lines to preclude before sorting starts
	 * @param usegzip
	 *            use gzip compression for the temporary files
	 * @return a list of temporary flat files
	 * @throws IOException
	 */
	public static List<File> sortInBatch(final BufferedReader fbr,
			final long datalength, final Comparator<String> cmp,
			final int maxtmpfiles, final long maxMemory, final Charset cs,
			final File tmpdirectory, final boolean distinct,
			final int numHeader, final boolean usegzip) throws IOException {
		final List<File> files = new ArrayList<File>();
		final long blocksize = estimateBestSizeOfBlocks(datalength,
				maxtmpfiles, maxMemory);// in
		// bytes

		try {
			final List<String> tmplist = new ArrayList<String>();
			String line = "";
			try {
				int counter = 0;
				while (line != null) {
					long currentblocksize = 0;// in bytes
					while ((currentblocksize < blocksize)
							&& ((line = fbr.readLine()) != null)) {
						// as long as you have enough
						// memory
						if (counter < numHeader) {
							counter++;
							continue;
						}
						tmplist.add(line);
						currentblocksize += StringSizeEstimator
								.estimatedSizeOf(line);
					}
					files.add(sortAndSave(tmplist, cmp, cs, tmpdirectory,
							distinct, usegzip));
					tmplist.clear();
				}
			} catch (final EOFException oef) {
				if (tmplist.size() > 0) {
					files.add(sortAndSave(tmplist, cmp, cs, tmpdirectory,
							distinct, usegzip));
					tmplist.clear();
				}
			}
		} finally {
			fbr.close();
		}
		return files;
	}

	/**
	 * This will simply load the file by blocks of lines, then sort them
	 * in-memory, and write the result to temporary files that have to be merged
	 * later.
	 * 
	 * @param file
	 *            some flat file
	 * @return a list of temporary flat files
	 * @throws IOException
	 */
	public static List<File> sortInBatch(final File file) throws IOException {
		return sortInBatch(file, defaultcomparator, DEFAULTMAXTEMPFILES,
				Charset.defaultCharset(), null, false);
	}

	/**
	 * This will simply load the file by blocks of lines, then sort them
	 * in-memory, and write the result to temporary files that have to be merged
	 * later.
	 * 
	 * @param file
	 *            some flat file
	 * @param cmp
	 *            string comparator
	 * @return a list of temporary flat files
	 * @throws IOException
	 */
	public static List<File> sortInBatch(final File file,
			final Comparator<String> cmp) throws IOException {
		return sortInBatch(file, cmp, DEFAULTMAXTEMPFILES,
				Charset.defaultCharset(), null, false);
	}

	/**
	 * This will simply load the file by blocks of lines, then sort them
	 * in-memory, and write the result to temporary files that have to be merged
	 * later.
	 * 
	 * @param file
	 *            some flat file
	 * @param cmp
	 *            string comparator
	 * @param distinct
	 *            Pass <code>true</code> if duplicate lines should be discarded.
	 * @return a list of temporary flat files
	 * @throws IOException
	 */
	public static List<File> sortInBatch(final File file,
			final Comparator<String> cmp, final boolean distinct)
			throws IOException {
		return sortInBatch(file, cmp, DEFAULTMAXTEMPFILES,
				Charset.defaultCharset(), null, distinct);
	}

	/**
	 * This will simply load the file by blocks of lines, then sort them
	 * in-memory, and write the result to temporary files that have to be merged
	 * later. You can specify a bound on the number of temporary files that will
	 * be created.
	 * 
	 * @param file
	 *            some flat file
	 * @param cmp
	 *            string comparator
	 * @param maxtmpfiles
	 *            maximal number of temporary files
	 * @param cs
	 *            character set to use (can use Charset.defaultCharset())
	 * @param tmpdirectory
	 *            location of the temporary files (set to null for default
	 *            location)
	 * @param distinct
	 *            Pass <code>true</code> if duplicate lines should be discarded.
	 * @return a list of temporary flat files
	 * @throws IOException
	 */
	public static List<File> sortInBatch(final File file,
			final Comparator<String> cmp, final int maxtmpfiles,
			final Charset cs, final File tmpdirectory, final boolean distinct)
			throws IOException {
		return sortInBatch(file, cmp, maxtmpfiles, cs, tmpdirectory, distinct,
				0, false);
	}

	/**
	 * This will simply load the file by blocks of lines, then sort them
	 * in-memory, and write the result to temporary files that have to be merged
	 * later. You can specify a bound on the number of temporary files that will
	 * be created.
	 * 
	 * @param file
	 *            some flat file
	 * @param cmp
	 *            string comparator
	 * @param maxtmpfiles
	 *            maximal number of temporary files
	 * @param cs
	 *            character set to use (can use Charset.defaultCharset())
	 * @param tmpdirectory
	 *            location of the temporary files (set to null for default
	 *            location)
	 * @param distinct
	 *            Pass <code>true</code> if duplicate lines should be discarded.
	 * @param numHeader
	 *            number of lines to preclude before sorting starts
	 * @param usegzip
	 *            use gzip compression for the temporary files
	 * @return a list of temporary flat files
	 * @throws IOException
	 */
	public static List<File> sortInBatch(final File file,
			final Comparator<String> cmp, final int maxtmpfiles,
			final Charset cs, final File tmpdirectory, final boolean distinct,
			final int numHeader, final boolean usegzip) throws IOException {
		final BufferedReader fbr = new BufferedReader(new InputStreamReader(
				new FileInputStream(file), cs));
		return sortInBatch(fbr, file.length(), cmp, maxtmpfiles,
				estimateAvailableMemory(), cs, tmpdirectory, distinct,
				numHeader, usegzip);
	}

	/**
	 * default comparator between strings.
	 */
	public static Comparator<String> defaultcomparator = new Comparator<String>() {
		@Override
		public int compare(final String r1, final String r2) {
			return r1.compareTo(r2);
		}
	};

	/**
	 * Default maximal number of temporary files allowed.
	 */
	public static int DEFAULTMAXTEMPFILES = 1024;

}

final class StringSizeEstimator {

	private static int OBJ_HEADER;
	private static int ARR_HEADER;
	private static int INT_FIELDS = 12;
	private static int OBJ_REF;
	private static int OBJ_OVERHEAD;
	private static boolean IS_64_BIT_JVM;

	/**
	 * Private constructor to prevent instantiation.
	 */
	private StringSizeEstimator() {
	}

	/**
	 * Class initializations.
	 */
	static {
		// By default we assume 64 bit JVM
		// (defensive approach since we will get
		// larger estimations in case we are not sure)
		IS_64_BIT_JVM = false;
		// check the system property "sun.arch.data.model"
		// not very safe, as it might not work for all JVM implementations
		// nevertheless the worst thing that might happen is that the JVM is
		// 32bit
		// but we assume its 64bit, so we will be counting a few extra bytes per
		// string object
		// no harm done here since this is just an approximation.

		// final String arch = System.getProperty("sun.arch.data.model");
		// if (arch != null) {
		// if (arch.indexOf("32") != -1) {
		// // If exists and is 32 bit then we assume a 32bit JVM
		// IS_64_BIT_JVM = false;
		// }
		// }

		// The sizes below are a bit rough as we don't take into account
		// advanced JVM options such as compressed oops
		// however if our calculation is not accurate it'll be a bit over
		// so there is no danger of an out of memory error because of this.
		OBJ_HEADER = IS_64_BIT_JVM ? 16 : 8;
		ARR_HEADER = IS_64_BIT_JVM ? 24 : 12;
		OBJ_REF = IS_64_BIT_JVM ? 8 : 4;
		OBJ_OVERHEAD = OBJ_HEADER + INT_FIELDS + OBJ_REF + ARR_HEADER;

	}

	/**
	 * Estimates the size of a {@link String} object in bytes.
	 * 
	 * @param s
	 *            The string to estimate memory footprint.
	 * @return The <strong>estimated</strong> size in bytes.
	 */
	static long estimatedSizeOf(final String s) {
		return (s.length() * 2) + OBJ_OVERHEAD;
	}

}

/**
 * This is essentially a thin wrapper on top of a BufferedReader... which keeps
 * the last line in memory.
 * 
 */
final class BinaryFileBuffer {
	public BinaryFileBuffer(final BufferedReader r) throws IOException {
		this.fbr = r;
		reload();
	}

	public void close() throws IOException {
		this.fbr.close();
	}

	public boolean empty() {
		return this.cache == null;
	}

	public String peek() {
		return this.cache;
	}

	public String pop() throws IOException {
		final String answer = peek().toString();// make a copy
		reload();
		return answer;
	}

	private void reload() throws IOException {
		this.cache = this.fbr.readLine();
	}

	public BufferedReader fbr;

	private String cache;

}