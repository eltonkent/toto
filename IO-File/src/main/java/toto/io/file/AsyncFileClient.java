package toto.io.file;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;

import toto.async.AsyncCallback;
import toto.async.AsyncClient;
import toto.async.AsyncOperation;
import toto.io.IOUtils;
import android.content.Context;

/**
 * Read/write files asynchronously.
 * 
 * @see AsyncClient
 * @author Mobifluence Interactive
 * 
 */
public class AsyncFileClient extends AsyncClient {

	/**
	 * Read a file as an array of bytes asynchronously.
	 * 
	 * @param context
	 *            application context.
	 * @param file
	 *            to read.
	 * @param callback
	 *            {@link AsyncCallback}
	 */
	public void readFile(final Context context, final File file,
			final AsyncCallback<byte[]> callback) {
		super.addTask(context, new AsyncFileReadRunnable(file), callback);
	}

	/**
	 * Write give array of bytes to file asynchronously.
	 * 
	 * @param context
	 * @param file
	 * @param dataToWrite
	 * @param callback
	 */
	public void writeFile(final Context context, final File file,
			final byte[] dataToWrite, final AsyncCallback<Void> callback) {
		super.addTask(context, new AsyncFileWriteRunnable(file, dataToWrite),
				callback);
	}

	private class AsyncFileWriteRunnable extends AsyncOperation<Void> {
		final File file;
		final byte[] data;

		private AsyncFileWriteRunnable(final File file, final byte[] data) {
			this.file = file;
			this.data = data;
		}

		@Override
		public boolean isResponseValid(final Void t) {
			return true;
		}

		@Override
		public Void doOperation() throws Exception {
			if (!file.exists()) {
				file.createNewFile();
			}
			final ByteArrayInputStream bis = new ByteArrayInputStream(data);
			final FileOutputStream fos = new FileOutputStream(file);
			IOUtils.copy(bis, fos);
			return null;
		}

	}

	private class AsyncFileReadRunnable extends AsyncOperation<byte[]> {
		private final File file;

		private AsyncFileReadRunnable(final File file) {
			this.file = file;
		}

		@Override
		public boolean isResponseValid(final byte[] t) {
			if (t == null)
				return false;
			return true;
		}

		@Override
		public byte[] doOperation() throws Exception {
			return FileUtils.readFileToByteArray(file);
		}

	}

}
