/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file;

import java.io.File;

public interface FileProgressCallback {

	void beforeDelete(File file);

	void afterDelete(File file);

	/**
	 * When a directory is found in the given path
	 * 
	 * @param totalFilesContained
	 *            Total number of files/directories in the discovered directory.
	 */
	void onDirectoryDiscovered(int totalFilesContained);

}
