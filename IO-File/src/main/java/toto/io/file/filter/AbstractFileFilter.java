/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file.filter;

import java.io.File;

import toto.io.file.IOFileFilter;

/**
 * An abstract class which implements the Java FileFilter and FilenameFilter
 * interfaces via the IOFileFilter interface.
 * <p>
 * <div> Note that a subclass <b>must</b> override one of the accept methods,
 * otherwise your class will infinitely loop. </div>
 * </p>
 */
public abstract class AbstractFileFilter implements IOFileFilter {

	/**
	 * Checks to see if the File should be accepted by this filter.
	 * 
	 * @param file
	 *            the File to check
	 * @return true if this file matches the test
	 */
	@Override
	public boolean accept(final File file) {
		return accept(file.getParentFile(), file.getName());
	}

	/**
	 * Checks to see if the File should be accepted by this filter.
	 * 
	 * @param dir
	 *            the directory File to check
	 * @param name
	 *            the filename within the directory to check
	 * @return true if this file matches the test
	 */
	@Override
	public boolean accept(final File dir, final String name) {
		return accept(new File(dir, name));
	}

	/**
	 * Provide a String representaion of this file filter.
	 * 
	 * @return a String representaion
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
