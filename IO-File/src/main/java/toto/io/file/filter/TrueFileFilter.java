/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file.filter;

import java.io.File;
import java.io.Serializable;

import toto.io.file.IOFileFilter;

/**
 * A file filter that always returns true.
 * 
 * @see FileFilterUtils#trueFileFilter()
 */
public class TrueFileFilter implements IOFileFilter, Serializable {

	/**
	 * Singleton instance of true filter.
	 * 
	 * @since 1.3
	 */
	public static final IOFileFilter TRUE = new TrueFileFilter();
	/**
	 * Singleton instance of true filter. Please use the identical
	 * TrueFileFilter.TRUE constant. The new name is more JDK 1.5 friendly as it
	 * doesn't clash with other values when using static imports.
	 */
	public static final IOFileFilter INSTANCE = TRUE;

	/**
	 * Restrictive consructor.
	 */
	protected TrueFileFilter() {
	}

	/**
	 * Returns true.
	 * 
	 * @param file
	 *            the file to check (ignored)
	 * @return true
	 */
	@Override
	public boolean accept(final File file) {
		return true;
	}

	/**
	 * Returns true.
	 * 
	 * @param dir
	 *            the directory to check (ignored)
	 * @param name
	 *            the filename (ignored)
	 * @return true
	 */
	@Override
	public boolean accept(final File dir, final String name) {
		return true;
	}

}
