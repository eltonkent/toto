/**
 * Efficient file and directory filtering routines
 * <p>
 * <div>
 * <h4>Filter based on name </h4>
 * The following snippet filters files based on the name &quot;Test&quot;
 * <pre>
 * <span>//Get files based on a name filter</span>
 * File[] files=FileFilterUtils.filter(new NameFileFilter(&quot;Test&quot;), new File("sdcard/myapp/myloc/"));
 * </pre>
 * <h4>Filter based on size </h4>
 * <pre>
 * <span>//Get files over 1kb</span>
 * File[] files=FileFilterUtils.filter(new SizeFileFilter(1024 * 1024), new File("sdcard/myapp/myloc/"));
 * </pre>
 * </div>
 * </p>
 * @see FileFilterUtils
 *
 */
package toto.io.file.filter;