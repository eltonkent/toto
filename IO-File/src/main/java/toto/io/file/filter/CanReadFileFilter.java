/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file.filter;

import java.io.File;
import java.io.Serializable;

import toto.io.file.IOFileFilter;

/**
 * This filter accepts <code>File</code>s that can be read.
 * <p>
 * <div> Example, showing how to print out a list of the current directory's
 * <i>readable</i> files:
 * 
 * <pre>
 * File dir = new File(&quot;.&quot;);
 * String[] files = dir.list(CanReadFileFilter.CAN_READ);
 * for (int i = 0; i &lt; files.length; i++) {
 * 	System.out.println(files[i]);
 * }
 * </pre>
 * 
 * Example, showing how to print out a list of the current directory's
 * <i>un-readable</i> files:
 * 
 * <pre>
 * File dir = new File(&quot;.&quot;);
 * String[] files = dir.list(CanReadFileFilter.CANNOT_READ);
 * for (int i = 0; i &lt; files.length; i++) {
 * 	System.out.println(files[i]);
 * }
 * </pre>
 * 
 * Example, showing how to print out a list of the current directory's
 * <i>read-only</i> files:
 * 
 * <pre>
 * File dir = new File(&quot;.&quot;);
 * String[] files = dir.list(CanReadFileFilter.READ_ONLY);
 * for (int i = 0; i &lt; files.length; i++) {
 * 	System.out.println(files[i]);
 * }
 * </pre>
 * 
 * </div>
 * </p>
 */
public class CanReadFileFilter extends AbstractFileFilter implements
		Serializable {

	/** Singleton instance of <i>readable</i> filter */
	public static final IOFileFilter CAN_READ = new CanReadFileFilter();

	/** Singleton instance of not <i>readable</i> filter */
	public static final IOFileFilter CANNOT_READ = new NotFileFilter(CAN_READ);

	/** Singleton instance of <i>read-only</i> filter */
	public static final IOFileFilter READ_ONLY = new AndFileFilter(CAN_READ,
			CanWriteFileFilter.CANNOT_WRITE);

	/**
	 * Restrictive consructor.
	 */
	protected CanReadFileFilter() {
	}

	/**
	 * Checks to see if the file can be read.
	 * 
	 * @param file
	 *            the File to check.
	 * @return <code>true</code> if the file can be read, otherwise
	 *         <code>false</code>.
	 */
	@Override
	public boolean accept(final File file) {
		return file.canRead();
	}

}
