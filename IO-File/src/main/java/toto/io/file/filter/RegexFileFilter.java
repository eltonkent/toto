/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file.filter;

import java.io.File;
import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * Filters files using supplied regular expression(s).
 * <p>
 * <div> See java.util.regex.Pattern for regex matching rules
 * 
 * <pre>
 * File dir = new File(&quot;.&quot;);
 * FileFilter fileFilter = new RegexFileFilter(&quot;&circ;.*[tT]est(-\\d+)?\\.java$&quot;);
 * File[] files = dir.listFiles(fileFilter);
 * for (int i = 0; i &lt; files.length; i++) {
 * 	System.out.println(files[i]);
 * }
 * </pre>
 * 
 * </div>
 * </p>
 */
public class RegexFileFilter extends AbstractFileFilter implements Serializable {

	/** The regular expression pattern that will be used to match filenames */
	private final Pattern pattern;

	/**
	 * Construct a new regular expression filter.
	 * 
	 * @param pattern
	 *            regular string expression to match
	 * @throws IllegalArgumentException
	 *             if the pattern is null
	 */
	public RegexFileFilter(final String pattern) {
		if (pattern == null) {
			throw new IllegalArgumentException("Pattern is missing");
		}

		this.pattern = Pattern.compile(pattern);
	}

	/**
	 * Construct a new regular expression filter with the specified flags case
	 * sensitivity.
	 * 
	 * @param pattern
	 *            regular string expression to match
	 * @param caseSensitivity
	 *            how to handle case sensitivity, null means case-sensitive
	 * @throws IllegalArgumentException
	 *             if the pattern is null
	 */
	public RegexFileFilter(final String pattern, final IOCase caseSensitivity) {
		if (pattern == null) {
			throw new IllegalArgumentException("Pattern is missing");
		}
		int flags = 0;
		if (caseSensitivity != null && !caseSensitivity.isCaseSensitive()) {
			flags = Pattern.CASE_INSENSITIVE;
		}
		this.pattern = Pattern.compile(pattern, flags);
	}

	/**
	 * Construct a new regular expression filter with the specified flags.
	 * 
	 * @param pattern
	 *            regular string expression to match
	 * @param flags
	 *            pattern flags - e.g. {@link Pattern#CASE_INSENSITIVE}
	 * @throws IllegalArgumentException
	 *             if the pattern is null
	 */
	public RegexFileFilter(final String pattern, final int flags) {
		if (pattern == null) {
			throw new IllegalArgumentException("Pattern is missing");
		}
		this.pattern = Pattern.compile(pattern, flags);
	}

	/**
	 * Construct a new regular expression filter for a compiled regular
	 * expression
	 * 
	 * @param pattern
	 *            regular expression to match
	 * @throws IllegalArgumentException
	 *             if the pattern is null
	 */
	public RegexFileFilter(final Pattern pattern) {
		if (pattern == null) {
			throw new IllegalArgumentException("Pattern is missing");
		}

		this.pattern = pattern;
	}

	/**
	 * Checks to see if the filename matches one of the regular expressions.
	 * 
	 * @param dir
	 *            the file directory (ignored)
	 * @param name
	 *            the filename
	 * @return true if the filename matches one of the regular expressions
	 */
	@Override
	public boolean accept(final File dir, final String name) {
		return pattern.matcher(name).matches();
	}

}
