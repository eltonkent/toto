/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file.filter;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

import toto.io.file.FileUtils;

/**
 * Filters files based on a cutoff time, can filter either newer files or files
 * equal to or older.
 * <p>
 * <div> For example, to print all files and directories in the current
 * directory older than one day:
 * 
 * <pre>
 * File dir = new File(&quot;.&quot;);
 * // We are interested in files older than one day
 * long cutoff = System.currentTimeMillis() - (24 * 60 * 60 * 1000);
 * String[] files = dir.list(new AgeFileFilter(cutoff));
 * for (int i = 0; i &lt; files.length; i++) {
 * 	System.out.println(files[i]);
 * }
 * </pre>
 * 
 * </div>
 * </p>
 * 
 * @see FileFilterUtils#ageFileFilter(Date)
 * @see FileFilterUtils#ageFileFilter(File)
 * @see FileFilterUtils#ageFileFilter(long)
 * @see FileFilterUtils#ageFileFilter(Date, boolean)
 * @see FileFilterUtils#ageFileFilter(File, boolean)
 * @see FileFilterUtils#ageFileFilter(long, boolean)
 */
public class AgeFileFilter extends AbstractFileFilter implements Serializable {

	/** The cutoff time threshold. */
	private final long cutoff;
	/** Whether the files accepted will be older or newer. */
	private final boolean acceptOlder;

	/**
	 * Constructs a new age file filter for files equal to or older than a
	 * certain cutoff
	 * 
	 * @param cutoff
	 *            the threshold age of the files
	 */
	public AgeFileFilter(final long cutoff) {
		this(cutoff, true);
	}

	/**
	 * Constructs a new age file filter for files on any one side of a certain
	 * cutoff.
	 * 
	 * @param cutoff
	 *            the threshold age of the files
	 * @param acceptOlder
	 *            if true, older files (at or before the cutoff) are accepted,
	 *            else newer ones (after the cutoff).
	 */
	public AgeFileFilter(final long cutoff, final boolean acceptOlder) {
		this.acceptOlder = acceptOlder;
		this.cutoff = cutoff;
	}

	/**
	 * Constructs a new age file filter for files older than (at or before) a
	 * certain cutoff date.
	 * 
	 * @param cutoffDate
	 *            the threshold age of the files
	 */
	public AgeFileFilter(final Date cutoffDate) {
		this(cutoffDate, true);
	}

	/**
	 * Constructs a new age file filter for files on any one side of a certain
	 * cutoff date.
	 * 
	 * @param cutoffDate
	 *            the threshold age of the files
	 * @param acceptOlder
	 *            if true, older files (at or before the cutoff) are accepted,
	 *            else newer ones (after the cutoff).
	 */
	public AgeFileFilter(final Date cutoffDate, final boolean acceptOlder) {
		this(cutoffDate.getTime(), acceptOlder);
	}

	/**
	 * Constructs a new age file filter for files older than (at or before) a
	 * certain File (whose last modification time will be used as reference).
	 * 
	 * @param cutoffReference
	 *            the file whose last modification time is usesd as the
	 *            threshold age of the files
	 */
	public AgeFileFilter(final File cutoffReference) {
		this(cutoffReference, true);
	}

	/**
	 * Constructs a new age file filter for files on any one side of a certain
	 * File (whose last modification time will be used as reference).
	 * 
	 * @param cutoffReference
	 *            the file whose last modification time is usesd as the
	 *            threshold age of the files
	 * @param acceptOlder
	 *            if true, older files (at or before the cutoff) are accepted,
	 *            else newer ones (after the cutoff).
	 */
	public AgeFileFilter(final File cutoffReference, final boolean acceptOlder) {
		this(cutoffReference.lastModified(), acceptOlder);
	}

	// -----------------------------------------------------------------------
	/**
	 * Checks to see if the last modification of the file matches cutoff
	 * favorably.
	 * <p>
	 * If last modification time equals cutoff and newer files are required,
	 * file <b>IS NOT</b> selected. If last modification time equals cutoff and
	 * older files are required, file <b>IS</b> selected.
	 * 
	 * @param file
	 *            the File to check
	 * @return true if the filename matches
	 */
	@Override
	public boolean accept(final File file) {
		final boolean newer = FileUtils.isFileNewer(file, cutoff);
		return acceptOlder ? !newer : newer;
	}

	/**
	 * Provide a String representaion of this file filter.
	 * 
	 * @return a String representaion
	 */
	@Override
	public String toString() {
		final String condition = acceptOlder ? "<=" : ">";
		return super.toString() + "(" + condition + cutoff + ")";
	}
}
