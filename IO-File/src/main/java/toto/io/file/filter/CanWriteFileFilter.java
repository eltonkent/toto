/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file.filter;

import java.io.File;
import java.io.Serializable;

import toto.io.file.IOFileFilter;

/**
 * This filter accepts <code>File</code>s that can be written to.
 * <p>
 * <div> Example, showing how to print out a list of the current directory's
 * <i>writable</i> files:
 * 
 * <pre>
 * File dir = new File(&quot;.&quot;);
 * String[] files = dir.list(CanWriteFileFilter.CAN_WRITE);
 * for (int i = 0; i &lt; files.length; i++) {
 * 	System.out.println(files[i]);
 * }
 * </pre>
 * 
 * Example, showing how to print out a list of the current directory's
 * <i>un-writable</i> files:
 * 
 * <pre>
 * File dir = new File(&quot;.&quot;);
 * String[] files = dir.list(CanWriteFileFilter.CANNOT_WRITE);
 * for (int i = 0; i &lt; files.length; i++) {
 * 	System.out.println(files[i]);
 * }
 * </pre>
 * 
 * <b>N.B.</b> For read-only files, use <code>CanReadFileFilter.READ_ONLY</code>
 * </div>
 * </p>
 */
public class CanWriteFileFilter extends AbstractFileFilter implements
		Serializable {

	/** Singleton instance of <i>writable</i> filter */
	public static final IOFileFilter CAN_WRITE = new CanWriteFileFilter();

	/** Singleton instance of not <i>writable</i> filter */
	public static final IOFileFilter CANNOT_WRITE = new NotFileFilter(CAN_WRITE);

	/**
	 * Restrictive consructor.
	 */
	protected CanWriteFileFilter() {
	}

	/**
	 * Checks to see if the file can be written to.
	 * 
	 * @param file
	 *            the File to check
	 * @return <code>true</code> if the file can be written to, otherwise
	 *         <code>false</code>.
	 */
	@Override
	public boolean accept(final File file) {
		return file.canWrite();
	}

}
