/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file.filter;

import java.io.File;
import java.io.Serializable;

import toto.io.file.IOFileFilter;

/**
 * This filter produces a logical NOT of the filters specified.
 * 
 * @see FileFilterUtils#notFileFilter(IOFileFilter)
 */
public class NotFileFilter extends AbstractFileFilter implements Serializable {

	/** The filter */
	private final IOFileFilter filter;

	/**
	 * Constructs a new file filter that NOTs the result of another filter.
	 * 
	 * @param filter
	 *            the filter, must not be null
	 * @throws IllegalArgumentException
	 *             if the filter is null
	 */
	public NotFileFilter(final IOFileFilter filter) {
		if (filter == null) {
			throw new IllegalArgumentException("The filter must not be null");
		}
		this.filter = filter;
	}

	/**
	 * Returns the logical NOT of the underlying filter's return value for the
	 * same File.
	 * 
	 * @param file
	 *            the File to check
	 * @return true if the filter returns false
	 */
	@Override
	public boolean accept(final File file) {
		return !filter.accept(file);
	}

	/**
	 * Returns the logical NOT of the underlying filter's return value for the
	 * same arguments.
	 * 
	 * @param file
	 *            the File directory
	 * @param name
	 *            the filename
	 * @return true if the filter returns false
	 */
	@Override
	public boolean accept(final File file, final String name) {
		return !filter.accept(file, name);
	}

	/**
	 * Provide a String representaion of this file filter.
	 * 
	 * @return a String representaion
	 */
	@Override
	public String toString() {
		return super.toString() + "(" + filter.toString() + ")";
	}

}
