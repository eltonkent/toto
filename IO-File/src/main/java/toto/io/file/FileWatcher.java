/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file;

import java.io.File;

import android.os.FileObserver;

/**
 * 
 * Watches a file/directory with the path specified in the constructor
 * 
 * @author Elton Kent
 */
public class FileWatcher extends FileObserver {
	private IFileListener listener;

	public FileWatcher(final String path, final int mask) {
		super(path, mask);
	}

	@Override
	public void onEvent(final int i, final String s) {
		if (listener != null) {
			listener.onFileStatusChanged(i, new File(s));
		}
	}

	public void setFileListener(final IFileListener listener) {
		this.listener = listener;
	}

}
