/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.io.file;


import java.io.File;

import android.os.StatFs;

import toto.log.Logger;

public class SDCardUtils {
	/**
	 * Check if the sdcard is writable
	 * 
	 * @return true if the card is writable.
	 */
	public static boolean canWrite() {
		return canWrite(0);
	}

	/**
	 * Checks if there is space to write on the SDcard
	 * 
	 * @param sizeToWrite
	 *            The size (in bytes) of the data to write.
	 * @return true if the size can be written
	 */
	public static boolean canWrite(final long sizeToWrite) {
		/* check if the card is mounted */
		if (!isMounted()) {
            Logger.getDefault().e("SDCardUtils", "SDcard is not mounted!");
			return false;
			/* check if the card is read only */
		}
		if (isReadOnly()) {
            Logger.getDefault().e("SDCardUtils", "SDcard is readonly!");
			return false;
		}
		if (getFreeSpaceOnSDCard() < sizeToWrite) {
            Logger.getDefault().e("SDCardUtils", "No Space on SDcard!");
			return false;
		}
		return true;
	}

	/**
	 * Get the SDcard directory
	 * 
	 * @return
	 */
	public static File getDirectory() {
		return android.os.Environment.getExternalStorageDirectory();
	}

	/**
	 * Get the free space available to applications on the SDcard in bytes
	 * 
	 * @return
	 */
	public static long getFreeSpaceOnSDCard() {
		final StatFs cardStatistics = new StatFs(getDirectory().toString());
		final long bytesAvailable = (long) cardStatistics.getBlockSize()
				* cardStatistics.getAvailableBlocks();
		return bytesAvailable;
	}

	/**
	 * Check if the SDCard has the needed space
	 * 
	 * @see FileUtils#getUsableSpace(File)
	 * @param needed
	 *            bytes required
	 * @return true if space available
	 */
	public static boolean isSpaceAvailable(final long needed) {
		final long n = needed + FileUtils.ONE_MB * 50;
		return getFreeSpaceOnSDCard() > n;
	}

	/**
	 * Check if the device has an SDcard.
	 * 
	 * @return
	 */
	public static boolean isMounted() {
		return android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
	}

	public static boolean isReadOnly() {
		return android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED_READ_ONLY);
	}

}
