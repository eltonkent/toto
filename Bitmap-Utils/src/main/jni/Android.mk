LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_LDLIBS += -ljnigraphics
LOCAL_LDLIBS +=  -llog 
LOCAL_MODULE    := Bitmap
LOCAL_C_INCLUDES := $(LOCAL_PATH)/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/exif
LOCAL_SRC_FILES := RotationUtils.cpp \
				   BitmapUtils.c \
				   exif/exif.c \
				   exif/gpsinfo.c \
				   exif/iptc.c \
				   exif/jhead.c \
				   exif/jpgfile.c \
				   exif/jpgqguess.c \
				   exif/makernote.c \
				   exif/paths.c 

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
  LOCAL_CFLAGS += -DHAVE_ARMEABI_V7A=1 -mfloat-abi=softfp -mfpu=neon
  LOCAL_C_INCLUDES += $(NDK_ROOT)/sources/android/cpufeatures
  LOCAL_STATIC_LIBRARIES += cpufeatures
endif

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := Bitmap_PNG
LOCAL_SRC_FILES := libpng/png.c \
				libpng/pngerror.c \
				libpng/pngget.c \
				libpng/pngmem.c \
				libpng/pngpread.c \
				libpng/pngread.c \
				libpng/pngrio.c \
				libpng/pngrtran.c \
				libpng/pngrutil.c \
				libpng/pngset.c \
				libpng/pngtrans.c \
				libpng/pngwio.c \
				libpng/pngwrite.c \
				libpng/pngwtran.c \
				libpng/pngwutil.c
LOCAL_LDLIBS 	:= -lz -llog
include $(BUILD_SHARED_LIBRARY)