// Copyright 2010 Google Inc. All Rights Reserved.
// Author: andrewharp@google.com (Andrew Harp)
//
// This file exists to make the optical flow library more portable to different
// platforms.


//typedef unsigned char bool;

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;

typedef signed char int8;
typedef short int16;
typedef signed int int32;
typedef float float32;

