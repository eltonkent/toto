package toto.bitmap.utils;

import android.content.Context;
import android.graphics.*;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;

/**
 * Bitmap cropping utilities
 * 
 * @author ekent4
 * 
 */
public final class CroppingUtils {
	/**
	 * crop the given bitmap
	 * 
	 * @param bitmap
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param outputConfig
	 * @return
	 */
	public static Bitmap crop(final Bitmap bitmap, final int x, final int y,
			final int width, final int height, final Config outputConfig) {
		return Bitmap.createBitmap(bitmap, x, y, width, height, null, false);
	}

	/**
	 * Crop a rectangular bitmap as a circle
	 * 
	 * @param bitmap
	 * @return
	 */
	public static Bitmap cropCircular(final Bitmap bitmap) {
		final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		final Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
				bitmap.getWidth() / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	/**
	 * Scale a bitmap and give it specific values for each rounded corner.
	 * <p>
	 * The flexibility to individually specify the corner radius for each corner
	 * is illustrated below.<br/>
	 * <img src="../../../../../../resources/bitmapcorner.png"><br/>
	 * This bitmap has the following values for the corners</br>
	 * <ul>
	 * <li>Upper Left : <code>10.0f</code></li>
	 * <li>Upper Right: <code>25.0f</code></li>
	 * <li>Lower Right: <code>15f</code></li>
	 * <li>Lower Left : <code>5.0f</code></li>
	 * </ul>
	 * 
	 * </p>
	 * 
	 * @param context
	 *            Context object used to ascertain display density.
	 * @param bitmap
	 *            The original bitmap that will be scaled and have rounded
	 *            corners applied to it.
	 * @param upperLeft
	 *            Corner radius for upper left.
	 * @param upperRight
	 *            Corner radius for upper right.
	 * @param lowerRight
	 *            Corner radius for lower right.
	 * @param lowerLeft
	 *            Corner radius for lower left.
	 * @param endWidth
	 *            Width to which to scale original bitmap. If the width of the
	 *            ImageView is 50dp, just give 50.
	 * @param endHeight
	 *            Height to which to scale original bitmap.If the height of the
	 *            ImageView is 50dp, just give 50.
	 * @return Scaled bitmap with rounded corners.
	 */
	public static Bitmap cropRoundRect(final Context context, Bitmap bitmap,
			float upperLeft, float upperRight, float lowerRight,
			float lowerLeft, final int endWidth, final int endHeight) {
		final float densityMultiplier = context.getResources()
				.getDisplayMetrics().density;

		// scale incoming bitmap to appropriate px size given arguments and
		// display dpi
		bitmap = Bitmap.createScaledBitmap(bitmap,
				Math.round(endWidth * densityMultiplier),
				Math.round(endHeight * densityMultiplier), true);

		// create empty bitmap for drawing
		final Bitmap output = Bitmap.createBitmap(
				Math.round(endWidth * densityMultiplier),
				Math.round(endHeight * densityMultiplier), Config.ARGB_8888);

		// get canvas for empty bitmap
		final Canvas canvas = new Canvas(output);
		final int width = canvas.getWidth();
		final int height = canvas.getHeight();

		// scale the rounded corners appropriately given dpi
		upperLeft *= densityMultiplier;
		upperRight *= densityMultiplier;
		lowerRight *= densityMultiplier;
		lowerLeft *= densityMultiplier;

		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);

		// fill the canvas with transparency
		canvas.drawARGB(0, 0, 0, 0);

		// draw the rounded corners around the image rect. clockwise, starting
		// in upper left.
		canvas.drawCircle(upperLeft, upperLeft, upperLeft, paint);
		canvas.drawCircle(width - upperRight, upperRight, upperRight, paint);
		canvas.drawCircle(width - lowerRight, height - lowerRight, lowerRight,
				paint);
		canvas.drawCircle(lowerLeft, height - lowerLeft, lowerLeft, paint);

		// fill in all the gaps between circles. clockwise, starting at top.
		final RectF rectT = new RectF(upperLeft, 0, width - upperRight,
				height / 2);
		final RectF rectR = new RectF(width / 2, upperRight, width, height
				- lowerRight);
		final RectF rectB = new RectF(lowerLeft, height / 2,
				width - lowerRight, height);
		final RectF rectL = new RectF(0, upperLeft, width / 2, height
				- lowerLeft);

		canvas.drawRect(rectT, paint);
		canvas.drawRect(rectR, paint);
		canvas.drawRect(rectB, paint);
		canvas.drawRect(rectL, paint);

		// setKey up the rect for the image
		final Rect imageRect = new Rect(0, 0, width, height);

		// setKey up paint object such that it only paints on Color.WHITE
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// draw resized bitmap onto imageRect in canvas, using paint as
		// configured above
		canvas.drawBitmap(bitmap, imageRect, imageRect, paint);
		return output;
	}

}
