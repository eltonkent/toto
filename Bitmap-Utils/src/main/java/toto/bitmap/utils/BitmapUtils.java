/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 *
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.bitmap.utils;

import android.content.Context;
import android.graphics.*;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Base64;
import com.mi.toto.Conditions;
import com.mi.toto.ToTo;
import toto.util.collections.matrix.BitMatrix;
import toto.util.collections.matrix.IntMatrix;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.Map;

/**
 * Common bitmap utilities like scaling, rounding corners,transparency
 * 
 * @author ekent4
 */
public final class BitmapUtils {
	static {
		ToTo.loadLib("Bitmap");
	}

	private static final int bottom = 8 * 1024;

	/**
	 * Write bitmap to file using nio
	 * <p>
	 * <p/>
	 * </p>
	 * 
	 * @param bitmap
	 * @param file
	 * @param recycle
	 *            recycle bitmap after writing to <code>file</code>
	 * @return
	 * @throws IOException
	 */
	public static void fastWriteToFile(final Bitmap bitmap, final File file,
			final boolean recycle) throws IOException {
		final RandomAccessFile randomAccessFile = new RandomAccessFile(file,
				"rw");
		final int height = bitmap.getHeight();
		final FileChannel channel = randomAccessFile.getChannel();
		final MappedByteBuffer map = channel.map(MapMode.READ_WRITE, 0,
				bitmap.getRowBytes() * height);
		bitmap.copyPixelsToBuffer(map);
		if (recycle)
			bitmap.recycle();
		channel.close();
		randomAccessFile.close();

	}

	/**
	 * Efficient way to convert an immutable bitmap
	 * <p>
	 * This technique ensures that the memory for only one bitmap is consumed
	 * during the process (if the source bitmap is setKey to be recycled).
	 * </p>
	 * 
	 * @param immutable
	 *            bitmap
	 * @param recycle
	 *            recycle the source bitmap after conversion.
	 * @return
	 * @throws IOException
	 */
	public static Bitmap makeMutable(Bitmap immutable, final boolean recycle)
			throws IOException {
		final File file = new File(Environment.getExternalStorageDirectory()
				+ File.separator + System.currentTimeMillis() + ".tmp");
		final RandomAccessFile randomAccessFile = new RandomAccessFile(file,
				"rw");
		final int width = immutable.getWidth();
		final int height = immutable.getHeight();
		final Config type = immutable.getConfig();
		final FileChannel channel = randomAccessFile.getChannel();
		final MappedByteBuffer map = channel.map(MapMode.READ_WRITE, 0,
				immutable.getRowBytes() * height);
		immutable.copyPixelsToBuffer(map);
		if (recycle)
			immutable.recycle();
		immutable = Bitmap.createBitmap(width, height, type);
		map.position(0);
		immutable.copyPixelsFromBuffer(map);
		channel.close();
		randomAccessFile.close();
		file.delete();
		return immutable;
	}

	/**
	 * Draw one bitmap ontop of another
	 * 
	 * @param src
	 *            bitmap
	 * @param layer
	 *            the bitmap to be drawn ontop of <code>src</code>
	 * @param left
	 *            left coordinate of the <code>layer</code>
	 * @param top
	 *            top coordinate of the<code> layer</code>
	 * @throws IllegalArgumentException
	 *             if <code>src</code> is not mutable
	 */
	public static void addBitmapLayer(final Bitmap src, final Bitmap layer,
			final int left, final int top) throws IllegalArgumentException {
		if (!src.isMutable()) {
			throw new IllegalArgumentException("Bitmap is not mutable");
		}
		final Canvas canvas = new Canvas(src);
		canvas.drawBitmap(layer, left, top, null);
	}

	/**
	 * Compute the inSampleSize value for BitmapFactory.Options.
	 * <p>
	 * The <code>stream</code> is consumed. Hence it cannot be used again.
	 * </p>
	 * 
	 * @param stream
	 *            The Bitmap input stream
	 * @param maxW
	 *            max width of the target bitmap
	 * @param maxH
	 *            max height of the target bitmap
	 * @return
	 */
	public static int computeInSampleSize(final InputStream stream,
			final int maxW, final int maxH) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(stream, null, options);
		final double w = options.outWidth;
		final double h = options.outHeight;
		final int sampleSize = (int) Math.ceil(Math.max(w / maxW, h / maxH));
		return sampleSize;
	}

	/**
	 * Compute the inSampleSize for the BitmapFactory.Options used for image
	 * decoding
	 * 
	 * @param srcW
	 *            The width of the src image to be decoded.
	 * @param srcH
	 *            The height of the src image to be decoded.
	 * @param maxW
	 *            The maximum width of the image after decoding
	 * @param maxH
	 *            The maximum height of the image after decoding
	 * @return
	 */
	public static int computeInSampleSize(final int srcW, final int srcH,
			final int maxW, final int maxH) {

		final int sampleSize = (int) Math.ceil(Math.max(srcW / maxW, srcH
				/ maxH));
		return sampleSize;
	}

	public static int computeSampleSizeFromOptions(
			final BitmapFactory.Options options, final int minSideLength,
			final int maxNumOfPixels) {
		final int initialSize = computeInitialSampleSize(options,
				minSideLength, maxNumOfPixels);

		int roundedSize;
		if (initialSize <= 8) {
			roundedSize = 1;
			while (roundedSize < initialSize) {
				roundedSize <<= 1;
			}
		} else {
			roundedSize = (initialSize + 7) / 8 * 8;
		}

		return roundedSize;
	}

	private static int computeInitialSampleSize(
			final BitmapFactory.Options options, final int minSideLength,
			final int maxNumOfPixels) {
		final double w = options.outWidth;
		final double h = options.outHeight;

		final int lowerBound = (maxNumOfPixels == -1) ? 1 : (int) Math
				.ceil(Math.sqrt(w * h / maxNumOfPixels));
		final int upperBound = (minSideLength == -1) ? 128 : (int) Math.min(
				Math.floor(w / minSideLength), Math.floor(h / minSideLength));

		if (upperBound < lowerBound) {
			// return the larger one when there is no overlapping zone.
			return lowerBound;
		}

		if ((maxNumOfPixels == -1) && (minSideLength == -1)) {
			return 1;
		} else if (minSideLength == -1) {
			return lowerBound;
		} else {
			return upperBound;
		}
	}

	/**
	 * Calculate an inSampleSize for use in a {@link BitmapFactory.Options}
	 * object when decoding bitmaps using the decode* methods from
	 * {@link BitmapFactory}. This implementation calculates the closest
	 * inSampleSize that will result in the final decoded bitmap having a width
	 * and height equal to or larger than the requested width and height. This
	 * implementation does not ensure a power of 2 is returned for inSampleSize
	 * which can be faster when decoding but results in a larger bitmap which
	 * isn't as useful for caching purposes.
	 * 
	 * @param options
	 *            An options object with out* params already populated (run
	 *            through a decode* method with inJustDecodeBounds==true
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return The value to be used for inSampleSize
	 */
	public static int calculateInSampleSize(
			final BitmapFactory.Options options, final int reqWidth,
			final int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}

			// This offers some additional logic in case the image has a strange
			// aspect ratio. For example, a panorama may have a much larger
			// width than height. In these cases the total pixels might still
			// end up being too large to fit comfortably in memory, so we should
			// be more aggressive with sample down the image (=larger
			// inSampleSize).

			final float totalPixels = width * height;

			// Anything more than 2x the requested pixels we'll sample down
			// further.
			final float totalReqPixelsCap = reqWidth * reqHeight * 2;

			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}
		}
		return inSampleSize;
	}

	/**
	 * Convert an image into a 2D array of pixels.
	 * 
	 * @param bitmap
	 * @return
	 * @throws NullPointerException
	 *             if the image is null
	 */
	public static final int[][] convertTo2D(final Bitmap bitmap) {
		if (bitmap == null) {
			throw new NullPointerException("Image is null");
		}
		final int w = bitmap.getWidth();
		final int h = bitmap.getHeight();
		final int[] argb = getPixels(bitmap);
		final int[][] twoD = new int[h][w];
		convertTo2D(argb, w, h, twoD);
		return twoD;
	}

	/**
	 * @param argb
	 *            data
	 * @param srcWidth
	 *            argb image width
	 * @param srcHeight
	 *            argb image height
	 * @param dest
	 *            the converted array int[argb.getHeight()][argb.getWidth()];
	 */
	public static final void convertTo2D(final int[] argb, final int srcWidth,
			final int srcHeight, final int[][] dest) {
		for (int i = 0; i < srcHeight; i++) {
			for (int j = 0; j < srcWidth; j++) {
				dest[i][j] = argb[i * srcWidth + j];
			}
		}
	}

	public static Bitmap createBitmap(final InputStream is) {
		final Bitmap bm = BitmapFactory.decodeStream(is);
		return bm;
	}

	public static Bitmap createBitmapFromAsset(final Context context,
			final String name) {
		try {
			return createBitmap(context.getAssets().open(name));
		} catch (final IOException e) {
			ToTo.logException(e);
		}
		return null;
	}

	/**
	 * Get the size of the given bitmap in KB
	 * 
	 * @param bitmap
	 * @param bitsPerPixel
	 * @return
	 */
	public static long getBitmapSize(final Bitmap bitmap, final int bitsPerPixel) {
		final long top = bitmap.getHeight() * bitmap.getWidth() * bitsPerPixel;
		return top / bottom;
	}

	/**
	 * Get the rotation angle based on the Exif information embedded in the
	 * bitmap.
	 * <p>
	 * 
	 * </p>
	 * 
	 * @param bitmap
	 * @return The angle at which the bitmap should be rotated (90,180, 270) or
	 *         0 if the information is unavailable.
	 */
	public static int getRotationAngle(final File bitmap) throws IOException {
		final ExifInterface exif = new ExifInterface(bitmap.getAbsolutePath());
		final int orientation = exif.getAttributeInt(
				ExifInterface.TAG_ORIENTATION, 1);
		if (orientation == 0) {
			return 0;
		}
		if (orientation == 6) {
			return 90;
		} else if (orientation == 3) {
			return 180;
		} else if (orientation == 8) {
			return 270;
		}
		return 0;
	}

	/**
	 * Get all available information from EXIF data using native methods.
	 * 
	 * @param filename
	 * @return
	 * @throws IOException
	 * @see {@link #getRotationAngle(File)}
	 */
	public static synchronized native Map<String, String> getImageInfoNative(
			String bitmap) throws IOException;

	/**
	 * Get the thumbnail for the given file. using native methods.
	 * 
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public static synchronized native byte[] getThumbnailNative(String filename)
			throws IOException;

	/**
	 * Get the byte[] for the given bitmap
	 * 
	 * @param bitmap
	 * @param compressionFormat
	 * @return
	 */
	public static byte[] getByteArray(final Bitmap bitmap,
			final CompressFormat compressionFormat) {
		final ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(compressionFormat, 100, stream);
		return stream.toByteArray();

	}

	/**
	 * Get the pixels for the given bitmap
	 *
	 * @param bitmap
	 * @return
	 */
	public static int[] getPixels(final Bitmap bitmap) {
		final int w = bitmap.getWidth();
		final int h = bitmap.getHeight();
		final int[] pixels = new int[w * h];
		bitmap.getPixels(pixels, 0, w, 0, 0, w, h);
		return pixels;
	}

	public static int[] getPixelRow(final Bitmap bitmap, final int width,
			final int columnNumber) {
		final int[] sub = new int[width];
		bitmap.getPixels(sub, 0, width, 0, columnNumber, width, 1);
		return sub;
	}

	/**
	 * @param argb
	 * @param row
	 *            array with length == width
	 * @param x
	 * @param y
	 * @param width
	 * @return
	 */
	public static void getPixelRow(final int[] argb, final int[] row,
			final int x, final int y, final int width) {
		System.arraycopy(argb, (y * width) + x, row, 0, width);
	}

	/**
	 * Convert a bitmap to a series of bitmap tiles
	 *
	 * @param bitmap
	 *            to extract the tiles from
	 * @param number
	 *            number of tiles to extract.
	 * @param tileWidth
	 * @param tileHeight
	 * @param fromHorizontal
	 *            if true, extract tiles horizontally from the image. Else
	 *            extract vertically.
	 * @return
	 */
	public static Bitmap[] getTiles(final Bitmap bitmap, final int number,
			final int tileWidth, final int tileHeight,
			final boolean fromHorizontal) {
		final Bitmap[] tiles = new Bitmap[number];
		for (int i = 0; i < number; i++) {
			tiles[i] = Bitmap.createBitmap(bitmap, fromHorizontal ? i
					* tileWidth : 0, fromHorizontal ? 0 : i * tileHeight,
					tileWidth, tileHeight);
		}
		return tiles;
	}

	// /**
	// *
	// * @param image
	// * @param xOffset
	// * of the shadow
	// * @param yOffset
	// * of the shadow
	// * @param size
	// * @param innerColor
	// * of the shadow
	// * @param outerColor
	// * of the shadow
	// * @return
	// */
	// public static final Bitmap dropShadow(Bitmap bitmap, int xOffset, int
	// yOffset, int size,
	// int innerColor, int outerColor) {
	// int w = bitmap.getWidth();
	// int h = bitmap.getHeight();
	// int[] data = getPixels(bitmap);
	// dropShadow(data, w, h, xOffset, yOffset, size, innerColor, outerColor);
	// return Bitmap.createBitmap(data, bitmap.getWidth(), bitmap.getHeight(),
	// Bitmap.Config.ARGB_8888);
	// }
	//
	// /**
	// * <p>
	// * Paints a dropshadow behind a given ARGB-Array, whereas you are able to
	// * specify the shadows inner and outer color.
	// * </p>
	// * <p>
	// * Note that the dropshadow just works for fully opaque pixels and that it
	// * needs a transparent margin to draw the shadow.
	// * </p>
	// * <p>
	// * Choosing the same inner and outer color and varying the transparency is
	// * recommended. Dropshadow just works for fully opaque pixels.
	// * </p>
	// *
	// * @param argbData
	// * of the image
	// * @param width
	// * of the image
	// * @param height
	// * of the image
	// * @param xOffset
	// * use this for finetuning the shadow's horizontal position.
	// * Negative values move the shadow to the left.
	// * @param yOffset
	// * use this for finetuning the shadow's vertical position.
	// * Negative values move the shadow to the top.
	// * @param size
	// * use this for finetuning the shadows radius.
	// * @param innerColor
	// * the inner color of the shadow, which should be less opaque
	// * than the text.
	// * @param outerColor
	// * the outer color of the shadow, which should be less than
	// * opaque the inner color.
	// */
	// private final static void dropShadow(int[] argbData, int width, int
	// height, int xOffset, int yOffset,
	// int size, int innerColor, int outerColor) {
	// // additional Margin for the image because of the shadow
	// int iLeft = size - xOffset < 0 ? 0 : size - xOffset;
	// int iRight = size + xOffset < 0 ? 0 : size + xOffset;
	// int iTop = size - yOffset < 0 ? 0 : size - yOffset;
	// int iBottom = size + yOffset < 0 ? 0 : size + yOffset;
	//
	// // setKey colors
	// int[] gradient = ColorUtil.getGradient(innerColor, outerColor, size);
	//
	// // walk over the text and look for non-transparent Pixels
	// for(int ix = -size + 1; ix < size; ix++){
	// for(int iy = -size + 1; iy < size; iy++){
	//
	// int r = (Math.abs(ix) + Math.abs(iy)) / 2;
	// // }
	// if(r < size){
	// int gColor = gradient[r];
	//
	// for(int col = iLeft, row; col < width/* +iLeft */- iRight; col++){
	// for(row = iTop; row < height - iBottom/* +iTop */- 1; row++){
	//
	// // draw if an opaque pixel is found and the
	// // destination is less opaque then the shadow
	// if(argbData[row * (width /* + size2 */) + col] >>> 24 == 0xFF
	// && argbData[(row + yOffset + iy) * (width /*
	// * size2
	// */) + col + xOffset + ix] >>> 24 < gColor >>> 24){
	// argbData[(row + yOffset + iy) * (width /*
	// * +
	// * size2
	// */) + col + xOffset + ix] = gColor;
	// }
	// }
	// }
	// }
	// }
	// }
	// }

	/**
	 * Write a bitmap to the file specified
	 *
	 * @param bitmap
	 * @param file
	 *            The full path of the file to write to including filename and
	 *            extension. The extension determines the file format.
	 * @throws IOException
	 *             if the file specified is a directory
	 */
	public static void writeBitmapToFile(final Bitmap bitmap, final File file)
			throws IOException {
		if (!file.isDirectory()) {
			throw new IllegalArgumentException("File is a directory");
		}
		final CompressFormat format = (file.getPath().endsWith("jpg") || file
				.getPath().endsWith("JPG")) ? CompressFormat.JPEG
				: CompressFormat.PNG;
		final FileOutputStream fos = new FileOutputStream(file);
		bitmap.compress(format, 100, fos);
		fos.close();
	}

	/**
	 * Set a row of pixels in the given argb array
	 *
	 * @param row
	 *            to be setKey
	 * @param column
	 *            the current column
	 * @param width
	 *            of the bitmap
	 * @param argb
	 *            the argb array of the bitmap
	 */
	public static void setPixelRow(final int[] row, final int column,
			final int width, final int[] argb) {
		for (int i = 0; i < row.length; i++) {
			final int nRow = column * width;
			argb[nRow + i] = row[i];
		}
	}

	public static void recycleBitmap(final Bitmap bitmap) {
		if (bitmap != null) {
			bitmap.recycle();
		}
	}

	/**
	 * Convert a drawable to a bitmap
	 *
	 * @param drawable
	 * @return
	 */
	public static Bitmap toBitmap(final Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		}
		final int width = drawable.getIntrinsicWidth();
		final int height = drawable.getIntrinsicHeight();
		final Bitmap bitmap = Bitmap.createBitmap(width, height, drawable
				.getOpacity() != PixelFormat.OPAQUE ? Config.ARGB_8888
				: Config.RGB_565);
		final Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, width, height);
		drawable.draw(canvas);
		return bitmap;
	}

	/**
	 * Convert an Luma image into Greyscale.
	 *
	 * @param lum
	 *            Integer array representing an Luma image.
	 * @param width
	 *            Width of the image.
	 * @param height
	 *            Height of the image.
	 * @return Bitmap of the Luma image.
	 * @throws NullPointerException
	 *             if RGB integer array is NULL.
	 */
	public static Bitmap lumaToGreyscale(final int[] lum, final int width,
			final int height) {
		if (lum == null)
			throw new NullPointerException();

		final Bitmap bitmap = Bitmap.createBitmap(width, height,
				Config.RGB_565);
		for (int y = 0, xy = 0; y < bitmap.getHeight(); y++) {
			for (int x = 0; x < bitmap.getWidth(); x++, xy++) {
				final int luma = lum[xy];
				bitmap.setPixel(x, y, Color.argb(1, luma, luma, luma));
			}
		}
		return bitmap;
	}

	public static String encodeToBase64(final Bitmap image,
			final CompressFormat format) {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(format, 100, baos);
		final byte[] b = baos.toByteArray();
		final String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
		return imageEncoded;
	}

	/**
	 * Convert a bitmap to int matrix.
	 *
	 * @param bitmap
	 * @return
	 * @see #toBitmap(IntMatrix,
	 *      Config)
	 */
	public static IntMatrix toMatrix(final Bitmap bitmap) {
		final int width = bitmap.getWidth();
		final int height = bitmap.getHeight();
		final IntMatrix matrix = new IntMatrix(width, height);
		for (int y = 0; y < height; y++) {
			final int offset = y * width;
			for (int x = 0; x < width; x++) {
				matrix.set(x, y, bitmap.getPixel(x, y));
			}
		}

		return matrix;
	}

	/**
	 * Draw label on the bottom of bitmap
	 *
	 * @param bitmap
	 *            bitmap to draw on it
	 * @param text
	 *            the text you want to draw on bitmap
	 * @param x
	 *            coordinate of the text.
	 * @param y
	 *            coordinate of the text.
	 * @param textPaint
	 *            set text color and text size.
	 * @param maxChars
	 *            if you don't know the size of text send max character to draw
	 *            on the bitmap to make sure it does not exceed the bitmap size
	 *            or send -1
	 * @return
	 */
	public static Bitmap drawTextOnBitmap(final Bitmap bitmap, String text,
			final float x, final float y, final Paint textPaint,
			final int maxChars) {
		if (text.length() >= maxChars && maxChars != -1)
			text = text.substring(0, maxChars);

		final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		// draw the bitmap
		final Canvas canvas = new Canvas(output);
		final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		canvas.drawBitmap(bitmap, rect, rect, paint);
		// draw the label area
		paint.setColor(Color.rgb(0, 0, 0));
		paint.setAlpha(120);
		final int labelAreaSize = (int) (bitmap.getHeight() * 0.2);
		canvas.drawRect(0, bitmap.getHeight() - labelAreaSize,
				bitmap.getWidth(), bitmap.getHeight(), paint);
		canvas.drawText(text, x, y, textPaint);
		return output;
	}

	/**
	 * Convert BitMatrix to bitmap.
	 *
	 * @param intMatrix
	 * @param config
	 * @return
	 * @see #toMatrix(Bitmap)
	 */
	public static Bitmap toBitmap(final IntMatrix intMatrix, final Config config) {
		final int width = intMatrix.getWidth();
		final int height = intMatrix.getHeight();
		final int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++) {
			final int offset = y * width;
			for (int x = 0; x < width; x++) {
				pixels[offset + x] = intMatrix.get(x, y);
			}
		}
		final Bitmap bitmap = Bitmap.createBitmap(width, height, config);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		return bitmap;
	}

	/**
	 * Convert BitMatrix to bitmap.
	 *
	 * @param result
	 * @param config
	 * @return
	 */
	public static Bitmap toBitmap(final BitMatrix result,
			final Config config, final int background,
			final int foreground) {
		Conditions.checkArgument(result != null, "Provided matrix is null;");
		final int width = result.getWidth();
		final int height = result.getHeight();
		final int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++) {
			final int offset = y * width;
			for (int x = 0; x < width; x++) {
				pixels[offset + x] = result.get(x, y) ? foreground : background;
			}
		}
		final Bitmap bitmap = Bitmap.createBitmap(width, height, config);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		return bitmap;
	}

	public static Bitmap decodeFromBase64(final String input) {
		final byte[] decodedByte = Base64.decode(input, 0);
		return BitmapFactory
				.decodeByteArray(decodedByte, 0, decodedByte.length);
	}

	/**
	 * Computes how similar of two given images represented by their signatures.
	 * 
	 * @return An integer from 0 to 100 is returned indicating how much
	 *         percentage of signature2 is different from signature1.
	 */
	// public static int n_computeDifferenceSignature(final int[] signature1,
	// final int[] signature2) {
	// return diffSignature(signature1, signature2);
	// }

	/**
	 * Computes signature of a given image.
	 * 
	 * @param input
	 *            An array of input pixels in YUV420SP format.
	 * @param width
	 *            The width of the input image.
	 * @param height
	 *            The height of the input image.
	 * @param signatureBuffer
	 *            A buffer for output signature. If it's null or not in the
	 *            right size, this buffer will be ignored and not used. This is
	 *            used to avoid GC.
	 * @return Signature of input image. If signatureBuffer is valid,
	 *         signatureBuffer will be returned. Otherwise a new array will be
	 *         returned and can be used as signature buffer in next function
	 *         call.
	 */
	// public static int[] n_computeSignature(final byte[] input, final int
	// width,
	// final int height, final int[] signatureBuffer) {
	// return computeSignature(input, width, height, signatureBuffer);
	// }
	//
	// private static native int[] computeSignature(byte[] input, int width,
	// int height, int[] signatureBuffer);
	//
	// private static native int diffSignature(int[] signature1, int[]
	// signature2);
}
