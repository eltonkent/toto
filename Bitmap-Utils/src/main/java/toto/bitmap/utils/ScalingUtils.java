package toto.bitmap.utils;

import android.graphics.Bitmap;

public class ScalingUtils {

	/**
	 * Resize the bitmap maintaining aspect ratio
	 * 
	 * @param bitmap
	 * @param maxSide
	 *            if the width of the bitmap is greater than the height then the
	 *            width is equal to the <code>maxSide</code>. likewise for the
	 *            height.
	 * @see BitmapUtils#resizeBitmap(Bitmap, int, int)
	 * @return
	 */
	public static final Bitmap scaleBitmap(final Bitmap bitmap,
			final int maxSide) {
		final int w = bitmap.getWidth();
		final int h = bitmap.getHeight();
		int l = maxSide;
		int i1 = maxSide;
		boolean flag;
		if (w > h) {
			flag = false;
			if (w > maxSide) {
				flag = true;
				i1 = (maxSide * h) / w;
			}
		} else {
			flag = false;
			if (h > maxSide) {
				flag = true;
				l = (maxSide * w) / h;
			}
		}
		if (flag)
			return Bitmap.createScaledBitmap(bitmap, l, i1, true);
		else
			return bitmap;
	}

	/**
	 * Scales an image to fit into the specified available width and height
	 * while maintaining the ratio.
	 * 
	 * @param source
	 *            the source bitmap
	 * @param availableWidth
	 *            the new width for the new rgbdata
	 * @param availableHeight
	 *            the new height for the new rgbdata
	 * @param outputConfig
	 *            Bitmap configuration of the output bitmap
	 * @return the resulting image
	 */
	public static Bitmap scaleToFit(final Bitmap source,
			final int availableWidth, final int availableHeight,
			final Bitmap.Config outputConfig) {
		final int sourceWidth = source.getWidth();
		final int sourceHeight = source.getHeight();

		if (sourceWidth > availableWidth || sourceHeight > availableHeight) {
			final int[] rgbData = BitmapUtils.getPixels(source);

			final double availableRatio = (double) availableWidth
					/ (double) availableHeight;
			final double sourceRatio = (double) sourceWidth
					/ (double) sourceHeight;

			int targetWidth = availableWidth;
			int targetHeight = availableHeight;

			if (availableRatio < sourceRatio) {
				targetHeight = (int) (targetWidth / sourceRatio);
			} else {
				targetWidth = (int) (targetHeight * sourceRatio);
			}

			final int[] newRgbData = new int[targetWidth * targetHeight];
			scale(rgbData, targetWidth, targetHeight, sourceWidth,
					sourceHeight, newRgbData);
			return Bitmap.createBitmap(newRgbData, targetWidth, targetHeight,
					outputConfig);
		}

		return source;
	}

	/**
	 * Resizes a bitmap to the given destination width and height
	 * 
	 * @param input
	 * @param destWidth
	 * @param destHeight
	 * @see BitmapUtils#resizeBitmap(Bitmap, int)
	 * @return
	 */
	public static Bitmap resizeBitmap(final Bitmap input, int destWidth,
			int destHeight) {
		final int srcWidth = input.getWidth();
		final int srcHeight = input.getHeight();
		boolean needsResize = false;
		float p;
		if (srcWidth > destWidth || srcHeight > destHeight) {
			needsResize = true;
			if (srcWidth > srcHeight && srcWidth > destWidth) {
				p = (float) destWidth / (float) srcWidth;
				destHeight = (int) (srcHeight * p);
			} else {
				p = (float) destHeight / (float) srcHeight;
				destWidth = (int) (srcWidth * p);
			}
		} else {
			destWidth = srcWidth;
			destHeight = srcHeight;
		}
		if (needsResize) {
			final Bitmap output = Bitmap.createScaledBitmap(input, destWidth,
					destHeight, true);
			return output;
		} else {
			return input;
		}
	}

	/**
	 * Scales an rgb data unproportional in every new size you want bigger or
	 * smaller than the given original.
	 * 
	 * @param opacity
	 *            the maximum alpha value, 255 (0xFF) means fully opaque, 0
	 *            fully transparent. Lower alpha values will be preserved.
	 * @param rgbData
	 *            the original rgbdata
	 * @param newWidth
	 *            the new width for the new rgbdata
	 * @param newHeight
	 *            the new height for the new rgbdata
	 * @param oldWidth
	 *            the width from the oiginal rgbdata
	 * @param oldHeight
	 *            the height from the oiginal rgbdata
	 * @param newRgbData
	 *            the new rgbdata has to be initialised
	 */
	public static void scale(final int opacity, final int[] rgbData,
			final int newWidth, final int newHeight, final int oldWidth,
			final int oldHeight, final int[] newRgbData) {
		int currentX = 0, currentY = 0;
		final int oldLenght = rgbData.length;
		final int newLength = newRgbData.length;
		int targetArrayIndex;
		final int verticalShrinkFactorPercent = ((newHeight * 100) / oldHeight);
		final int horizontalScaleFactorPercent = ((newWidth * 100) / oldWidth);
		final int alpha = (opacity << 24); // is now 0xAA000000
		for (int i = 0; i < newLength; i++) {
			currentX = (currentX + 1) % newWidth;
			if (currentX == 0) {
				currentY++;
			}
			targetArrayIndex = ((currentX * 100) / horizontalScaleFactorPercent)
					+ (oldWidth * ((currentY * 100) / verticalShrinkFactorPercent));
			if (targetArrayIndex >= oldLenght) {
				targetArrayIndex = oldLenght - 1;
			}
			if (targetArrayIndex < 0) {
				targetArrayIndex = 0;
			}
			int pixel = rgbData[targetArrayIndex];
			if (opacity != 255) {
				final int pixelAlpha = (pixel & 0xff000000) >>> 24;
				if (pixelAlpha > opacity) {
					pixel = (pixel & 0x00ffffff) | alpha;
				}
			}
			newRgbData[i] = pixel;
		}
	}

	/**
	 * Scales an rgb data unproportional in every new size you want bigger or
	 * smaller than the given original.
	 * 
	 * @param rgbData
	 *            the original rgbdata
	 * @param newWidth
	 *            the new width for the new rgbdata
	 * @param newHeight
	 *            the new height for the new rgbdata
	 * @param oldWidth
	 *            the width from the oiginal rgbdata
	 * @param oldHeight
	 *            the height from the oiginal rgbdata
	 * @param newRgbData
	 *            the new rgbdata has to be initialised
	 */
	public static void scale(final int[] rgbData, final int newWidth,
			final int newHeight, final int oldWidth, final int oldHeight,
			final int[] newRgbData) {

		int x, y, dy;
		int srcOffset;
		int destOffset;

		// Calculate the pixel ratio ( << 10 )
		final int pixelRatioWidth = (1024 * oldWidth) / newWidth;
		final int pixelRatioHeight = (1024 * oldHeight) / newHeight;

		y = 0;
		destOffset = 0;
		while (y < newHeight) {
			dy = ((pixelRatioHeight * y) >> 10) * oldWidth;
			srcOffset = 0;

			x = 0;
			while (x < newWidth) {
				newRgbData[destOffset + x] = rgbData[dy + (srcOffset >> 10)];
				srcOffset += pixelRatioWidth;
				x++;
			}

			destOffset += newWidth;
			y++;
		}
	}

	/**
	 * Returns the one scaled Pixel for the given new heigth and width.
	 * 
	 * @param oldLength
	 *            length of the rgb data source.
	 * @param oldWidth
	 *            the old width of the rgb data.
	 * @param oldHeigth
	 *            the old heigth of the rgb data.
	 * @param newWidth
	 *            the new width of the rgb data.
	 * @param newHeigth
	 *            the new heigth of the rgb data.
	 * @param currentX
	 *            the x position of the pixel to be scaled.
	 * @param currentY
	 *            the y position of the pixel to be scaled.
	 * @return position of the scaled pixel in the old rgb data array.
	 */
	public static int scaledPixel(final int oldLength, final int oldWidth,
			final int oldHeigth, final int newWidth, final int newHeigth,
			final int currentX, final int currentY) {
		int targetArrayIndex;
		final int verticalShrinkFactorPercent = ((newHeigth * 100) / oldHeigth);
		final int horizontalScaleFactorPercent = ((newWidth * 100) / oldWidth);
		targetArrayIndex = ((currentX * 100) / horizontalScaleFactorPercent)
				+ (oldWidth * ((currentY * 100) / verticalShrinkFactorPercent));
		if (targetArrayIndex >= oldLength) {
			targetArrayIndex = oldLength - 1;
		}
		if (targetArrayIndex < 0) {
			targetArrayIndex = 0;
		}
		return targetArrayIndex;
	}

	/**
	 * Stretches the rgb data horizontal to the given left and heigth width and
	 * returns the new rgb data array.
	 * 
	 * 
	 * @param argbArray
	 *            the rgb data to be stretched.
	 * @param newLeftHeigth
	 *            the new left heigth of the rgb data.
	 * @param newRigthHeigth
	 *            the new rigth heigth of the rgb data.
	 * @param biggerHeigth
	 *            the bigger heigth of left and rigth heigth.
	 * @param width
	 *            the source width of the rgb data.
	 * @param heigth
	 *            the source heigth of the rgb data.
	 * @param procentualScalingHeight
	 *            the procentual scaling heigth(biggerHeigth -
	 *            smallerSmaller)/widthOfTheOriginalImage).
	 * @param newArgbArray
	 *            the new rgb data where the changes getting in.
	 * @return return the filled newArgbArray with stretched changes.
	 */
	public static int[] stretchHorizontal(final int[] argbArray,
			final int newLeftHeigth, final int newRigthHeigth,
			final int biggerHeigth, final int width, final int heigth,
			int procentualScalingHeight, final int[] newArgbArray) {
		if (procentualScalingHeight == 0) {
			procentualScalingHeight++;
		}
		final int length = newArgbArray.length;
		final int oldLength = argbArray.length;
		// x and y position int the new array
		int idX = 0, idY = 0;
		// x and y position of the old array
		int x = 0, y = 0;
		// position in the new array
		int whereIamAt = 0;
		// Heighth for goal
		int newHeigth = newLeftHeigth;
		// start Heigth to goal
		int startColumn = (biggerHeigth - newHeigth) / 2;
		int endColumn = biggerHeigth - ((biggerHeigth - newHeigth) / 2);

		for (int i = 0; i < length; i++) {

			if (startColumn <= idY && endColumn >= idY) {
				newArgbArray[whereIamAt] = argbArray[scaledPixel(oldLength,
						width, heigth, width, newHeigth, x, y)];
				y = (y + 1) % newHeigth;
			} else {
				newArgbArray[whereIamAt] = 000000;
			}
			idY = (idY + 1) % (biggerHeigth);
			whereIamAt = idX + (idY * width);
			if (idY == 0) {
				idX++;
				x++;
				y = 0;
				if (newLeftHeigth < newRigthHeigth) {
					newHeigth += procentualScalingHeight;
				} else if (newLeftHeigth > newRigthHeigth) {
					newHeigth -= procentualScalingHeight;
				}
				startColumn = (biggerHeigth - newHeigth) / 2;
				endColumn = biggerHeigth - ((biggerHeigth - newHeigth) / 2);
			}
		}
		return newArgbArray;
	}

	/**
	 * Stretches the rgb data horizontal to the given left and rigth stretch
	 * factor and returns the new rgb data array.
	 * 
	 * @param argbArray
	 *            the rgb data to be stretched.
	 * @param leftStrechFactor
	 *            the stretch factor of the left.
	 * @param rightStrechFactor
	 *            the stretch factor of the rigth.
	 * @param width
	 *            the source width of the rgb data.
	 * @param heigth
	 *            the source heigth of the rgb data.
	 * @return stretched rgb data array.
	 */
	public static int[] stretchHorizontal(final int[] argbArray,
			final int leftStrechFactor, final int rightStrechFactor,
			final int width, final int heigth) {
		final int newHeigthLeft = (heigth * leftStrechFactor) / 100;
		final int newHeigthRight = (heigth * rightStrechFactor) / 100;
		int procentualScalingWidth;
		int biggerHeigth;
		if (newHeigthLeft < newHeigthRight) {
			procentualScalingWidth = (newHeigthRight - newHeigthLeft) / width;
			biggerHeigth = newHeigthRight;
		} else {
			procentualScalingWidth = (newHeigthLeft - newHeigthRight) / width;
			biggerHeigth = newHeigthLeft;
		}
		final int[] newArgbArray = new int[biggerHeigth * width];
		return stretchHorizontal(argbArray, newHeigthLeft, newHeigthRight,
				biggerHeigth, width, heigth, procentualScalingWidth,
				newArgbArray);
	}

	/**
	 * Stretches the rgb data vertical to the given top and bottom width and
	 * returns the new rgb data array.
	 * 
	 * 
	 * @param argbArray
	 *            the rgb data to be stretched.
	 * @param newWidthTop
	 *            the new top width of the rgb data.
	 * @param newWidthBottom
	 *            the new bottom width of the rgb data.
	 * @param biggerWidth
	 *            the bigger width of top and bottom width.
	 * @param width
	 *            the source width of the rgb data.
	 * @param heigth
	 *            the source heigth of the rgb data.
	 * @param procentualScalingHeight
	 *            the procentual scaling heigth(biggerWidth -
	 *            smallerWidth)/heigthOfTheOriginalImage).
	 * @param newArgbArray
	 *            the new rgb data where the changes getting in.
	 * @return return filled the newArgbArray with stretched changes.
	 */
	public static int[] stretchVertical(final int[] argbArray, int newWidthTop,
			final int newWidthBottom, final int biggerWidth, final int width,
			final int heigth, int procentualScalingHeight,
			final int[] newArgbArray) {
		if (procentualScalingHeight == 0) {
			procentualScalingHeight++;
		}
		final int length = newArgbArray.length;
		final int oldLength = argbArray.length;
		int insideCurrentY = 0;
		int insideCurrentX = 0, outsideCurrentX = 0;
		int sum1 = (biggerWidth - newWidthTop) / 2;
		int sum2 = biggerWidth - ((biggerWidth - newWidthTop) / 2);
		for (int i = 0; i < length; i++) {

			outsideCurrentX = (outsideCurrentX + 1) % biggerWidth;
			if (outsideCurrentX == 0) {
				if (newWidthTop < newWidthBottom) {
					newWidthTop += procentualScalingHeight;
					sum1 = (biggerWidth - newWidthTop) / 2;
					sum2 = biggerWidth - ((biggerWidth - newWidthTop) / 2);
				} else if (newWidthTop > newWidthBottom) {
					newWidthTop -= procentualScalingHeight;
					sum1 = (biggerWidth - newWidthTop) / 2;
					sum2 = biggerWidth - ((biggerWidth - newWidthTop) / 2);
				}
				insideCurrentY++;
				insideCurrentX = 0;
			}
			if (outsideCurrentX >= sum1 && outsideCurrentX < sum2) {
				insideCurrentX = (insideCurrentX + 1) % newWidthTop;
				newArgbArray[i] = argbArray[scaledPixel(oldLength, width,
						heigth, newWidthTop, heigth, insideCurrentX,
						insideCurrentY)];
			} else {
				newArgbArray[i] = 000000;
			}
		}
		return newArgbArray;
	}

	/**
	 * Stretches the rgb data vertical to the given top and bottom stretch
	 * factor and returns the new rgb data array.
	 * 
	 * @param argbArray
	 *            the rgb data to be stretched.
	 * @param topStrechFactor
	 *            the stretch factor of the top.
	 * @param bottomStrechFactor
	 *            the stretch factor of the bottom.
	 * @param width
	 *            the source width of the rgb data.
	 * @param heigth
	 *            the source heigth of the rgb data.
	 * @return stretched rgb data array.
	 */
	public static int[] stretchVertical(final int[] argbArray,
			final int topStrechFactor, final int bottomStrechFactor,
			final int width, final int heigth) {
		final int newWidthTop = (width * topStrechFactor) / 100;
		final int newWidthBottom = (width * bottomStrechFactor) / 100;
		int procentualScalingHeight;
		int biggerWidth;
		if (newWidthTop < newWidthBottom) {
			procentualScalingHeight = (newWidthBottom - newWidthTop) / heigth;
			biggerWidth = newWidthBottom;
		} else {
			procentualScalingHeight = (newWidthTop - newWidthBottom) / heigth;
			biggerWidth = newWidthTop;
		}
		final int[] newArgbArray = new int[biggerWidth * heigth];
		return stretchVertical(argbArray, newWidthTop, newWidthBottom,
				biggerWidth, width, heigth, procentualScalingHeight,
				newArgbArray);
	}
}
