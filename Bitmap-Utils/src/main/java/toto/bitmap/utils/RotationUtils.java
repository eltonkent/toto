package toto.bitmap.utils;

import toto.math.MathUtils;

public class RotationUtils {

	/**
	 * Returns the new width for the given degree. The new width symbols the
	 * width for an rotated rgb data array with the same degree rotation.
	 * 
	 * @param degree
	 *            the degree value of the rotation.
	 * @param width
	 *            the width of the rgb source.
	 * @param heigth
	 *            the heigth of the rgb source
	 * @return the new width of the rgb data.
	 */
	public static int getRotatedWidth(final float degree, final int width,
			final int heigth, final double angle) {
		final double degreeCos = Math.cos(Math.PI * angle / 180);
		final double degreeSin = Math.sin(Math.PI * angle / 180);
		if (degree == -90 || degree == 90 || degree == 270 || degree == -270) {
			return heigth;
		} else if (degree == 360 || degree == 180 || degree == 0) {
			return width;
		}
		final long pointX1 = 0; // MathUtil.round(0 * degreeCos - 0 *
								// degreeSin);
		final long pointX2 = MathUtils.round(width * degreeCos); // MathUtil.round(width
		// * degreeCos - 0
		// *degreeSin);
		final long pointX3 = MathUtils.round(-heigth * degreeSin); // MathUtil.round(0
		// *degreeCos -
		// heigth
		// *degreeSin);
		final long pointX4 = MathUtils.round(width * degreeCos - heigth
				* degreeSin);
		long minX = pointX1;
		if (pointX2 < minX) {
			minX = pointX2;
		}
		if (pointX3 < minX) {
			minX = pointX3;
		}
		if (pointX4 < minX) {
			minX = pointX4;
		}
		long maxX = pointX1;
		if (pointX2 > maxX) {
			maxX = pointX2;
		}
		if (pointX3 > maxX) {
			maxX = pointX3;
		}
		if (pointX4 > maxX) {
			maxX = pointX4;
		}
		return (int) (maxX - minX);
	}

	/**
	 * Returns the new height for the given degree. The new heigth symbols the
	 * heigth for an rotated rgb data array with the same degree rotation.
	 * 
	 * @param degree
	 *            the degree value of the rotation.
	 * @param width
	 *            the width of the rgb source.
	 * @param heigth
	 *            the heigth of the rgb source
	 * @return the new height of the rgb data.
	 */
	public static int getRotatedHeight(final float degree, final int width,
			final int heigth, final double angle) {
		final double degreeCos = Math.cos(Math.PI * angle / 180);
		final double degreeSin = Math.sin(Math.PI * angle / 180);
		if (degree == -90 || degree == 90 || degree == 270 || degree == -270) {
			return width;
		} else if (degree == 360 || degree == 180 || degree == 0) {
			return heigth;
		}
		final long pointY1 = MathUtils.round(0 * degreeSin + 0 * degreeCos);
		final long pointY2 = MathUtils.round(width * degreeSin + 0 * degreeCos);
		final long pointY3 = MathUtils
				.round(0 * degreeSin + heigth * degreeCos);
		final long pointY4 = MathUtils.round(width * degreeSin + heigth
				* degreeCos);
		long minY = pointY1;
		if (pointY2 < minY) {
			minY = pointY2;
		}
		if (pointY3 < minY) {
			minY = pointY3;
		}
		if (pointY4 < minY) {
			minY = pointY4;
		}
		long maxY = pointY1;
		if (pointY2 > maxY) {
			maxY = pointY2;
		}
		if (pointY3 > maxY) {
			maxY = pointY3;
		}
		if (pointY4 > maxY) {
			maxY = pointY4;
		}
		return (int) (maxY - minY);
	}

	// /**
	// * Rotate a bitmap using native code.
	// * <p>
	// * Uses a native implementation to rotate a bitmap. The source bitmap is
	// * </p>
	// *
	// * @param bitmap
	// * @return
	// */
	// public static Bitmap rotateBitmap90Native(final Bitmap bitmap) {
	// return rotateCcw90(bitmap);
	// }
	//
	// private static native Bitmap rotateCcw90(final Bitmap bitmap);
	//
	// static {
	// RAGE.loadLib("Bitmap");
	// }

}
