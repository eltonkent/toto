package toto.async;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * Base class to implement any async operation.
 * <p>
 * <div> The Async operation would try to deliver the response callbacks on the
 * main thread. Any issues with the handler would send the responses on the same
 * operation thread. </div>
 * </p>
 * 
 * @author Mobifluence Interactive
 * @param <T>
 *            The type of object the operation should return via
 *            {@link #doOperation()()}
 */
public abstract class AsyncOperation<T> implements Runnable {
	AsyncCallback<T> responseListener;

	private static final int FAILURE_MESSAGE = 1;
	private static final int FINISH_MESSAGE = 3;
	private static final int START_MESSAGE = 2;
	private static final int SUCCESS_MESSAGE = 0;

	private Handler handler;

	private Message obtainMessage(final int responseMessage,
			final Object response) {
		Message msg = null;
		if (handler != null) {
			msg = this.handler.obtainMessage(responseMessage, response);
		} else {
			msg = new Message();
			msg.what = responseMessage;
			msg.obj = response;
		}
		return msg;
	}

	public AsyncOperation() {
		if (Looper.myLooper() != null) {
			handler = new Handler() {
				@Override
				public void handleMessage(final Message msg) {
					AsyncOperation.this.handleMessage(msg);
				}
			};
		}
	}

	private void handleMessage(final Message msg) {
		Object[] repsonse;
		switch (msg.what) {
		case SUCCESS_MESSAGE:
			repsonse = (Object[]) msg.obj;
			responseListener.onSuccess((T) repsonse[0]);
			break;
		case FAILURE_MESSAGE:
			repsonse = (Object[]) msg.obj;
			responseListener
					.onFailure((T) repsonse[0], (Throwable) repsonse[1]);
			break;
		case START_MESSAGE:
			responseListener.onStarted();
			break;
		case FINISH_MESSAGE:
			break;
		}
	}

	protected void sendMessage(final Message msg) {
		if (handler != null) {
			handler.sendMessage(msg);
		} else {
			handleMessage(msg);
		}
	}

	/**
	 * Internal method. Do not call directly.
	 */
	@Override
	public void run() {
		sendMessage(obtainMessage(START_MESSAGE, null));
		T response = null;
		try {
			response = doOperation();
			sendMessage(obtainMessage(
					isResponseValid(response) ? SUCCESS_MESSAGE
							: FAILURE_MESSAGE, new Object[] { response, null }));
		} catch (final Exception e) {
			sendMessage(obtainMessage(FAILURE_MESSAGE, new Object[] { response,
					e }));
		}
		sendMessage(obtainMessage(FINISH_MESSAGE, null));
	}

	/**
	 * Return true if <code>t</code> (what is returned by {@link #doOperation()}
	 * ) is considered valid.
	 * 
	 * @param t
	 * @return
	 */
	public abstract boolean isResponseValid(T t);

	/**
	 * Perform required async operation.
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract T doOperation() throws Exception;

}
