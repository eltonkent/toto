package toto.async;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import android.content.Context;

/**
 * Base client for implementing Async operations.
 * 
 * <p>
 * <div> This is similar to android async tasks. except that this is NOT tied to
 * an Activity.<br/>
 * The thread pool can be either a fixed set of threads or scalable(default).
 * This can be modified using the {@link #setThreadPool(ThreadPoolExecutor)}
 * method. </div>
 * </p>
 * 
 * @see toto.io.file.AsyncHttpClient
 * @see toto.io.file.AsyncFileClient
 * 
 */
public class AsyncClient {

	private ThreadPoolExecutor threadPool;
	private final Map<Context, List<WeakReference<Future<?>>>> requestMap;

	/**
	 * Create an Async client using a cached thread pool of variable thread
	 * count.
	 */
	public AsyncClient() {
		this((ThreadPoolExecutor) Executors.newCachedThreadPool());
	}

	/**
	 * Create a async instance using the provided threadpool
	 * 
	 * @param threadPool
	 */
	public AsyncClient(final ThreadPoolExecutor threadPool) {
		this.threadPool = threadPool;
		requestMap = new WeakHashMap<Context, List<WeakReference<Future<?>>>>();
	}

	/**
	 * Cancels any pending (or potentially active) requests associated with the
	 * passed Context.
	 * <p>
	 * <b>Note:</b> This will only affect requests which were created with a
	 * non-null android Context. This method is intended to be used in the
	 * onDestroy method of your android activities to destroy all requests which
	 * are no longer required.
	 * 
	 * @param context
	 *            the android Context instance associated to the request.
	 * @param mayInterruptIfRunning
	 *            specifies if active requests should be cancelled along with
	 *            pending requests.
	 */
	public void cancelRequests(final Context context,
			final boolean mayInterruptIfRunning) {
		final List<WeakReference<Future<?>>> requestList = requestMap
				.get(context);
		if (requestList != null) {
			for (final WeakReference<Future<?>> requestRef : requestList) {
				final Future<?> request = requestRef.get();
				if (request != null) {
					request.cancel(mayInterruptIfRunning);
				}
			}
		}
		requestMap.remove(context);
	}

	/**
	 * Add and start async operation
	 * 
	 * @param context
	 *            Current context. can be null if we dont want to associate
	 *            tasks with context's.
	 * @param task
	 *            to execute.
	 * @param listener
	 *            Callback
	 * @see #addTask(AsyncOperation, AsyncCallback)
	 */
	public <T> void addTask(final Context context,
			final AsyncOperation<T> task, final AsyncCallback<T> listener) {
		task.responseListener = listener;
		final Future<?> request = threadPool.submit(task);
		if (context != null) {
			// Add request to request map
			List<WeakReference<Future<?>>> requestList = requestMap
					.get(context);
			if (requestList == null) {
				requestList = new LinkedList<WeakReference<Future<?>>>();
				requestMap.put(context, requestList);
			}
			requestList.add(new WeakReference<Future<?>>(request));
		}
	}

	/**
	 * Add and start async operation
	 * 
	 * @param task
	 *            to execute.
	 * @param listener
	 *            Callback
	 */
	public <T> void addTask(final AsyncOperation<T> task,
			final AsyncCallback<T> listener) {
		addTask(null, task, listener);
	}

	/**
	 * Overrides the threadpool implementation used when queuing/pooling
	 * requests. By default, Executors.newCachedThreadPool() is used.
	 * 
	 * @param threadPool
	 *            an instance of {@link ThreadPoolExecutor} to use for
	 *            queuing/pooling requests.
	 */
	public void setThreadPool(final ThreadPoolExecutor threadPool) {
		this.threadPool = threadPool;
	}

	private static AsyncClient instance;

	/**
	 * Get a reusable instance of the async client.
	 * <p>
	 * By maintaining a reusable instance, we save resources when new and
	 * dedicated thread pool is created. {@link #AsyncClient()} .<br/>
	 * <b>Note</b>: If any operation decides to have a dedicated thread pool,
	 * creating a new {@link #AsyncClient()} instance is the way to go.
	 * </p>
	 * 
	 * @return Shared {@link AsyncClient} instance.
	 */
	public static synchronized AsyncClient getReusableInstance() {
		if (instance == null) {
			instance = new AsyncClient();
		}
		return instance;
	}
}
