/**
 * Framework for performing operations that are time consuming and therefore
 * should be asynchronous.
 * <p>
 * <div>
 * <h3>Implementing a Async client</h3>
 * Creating an async operation is a simple two step operation.
 * Let us consider implementing a Async client to perform a series of string  manipulations returning a formatted string.
 * <h4>1. Implement the Async operation</h4>
 * First we need to implement the operation that has be performed. We start by extending the {@link toto.async.AsyncOperation} class. Extending {@link toto.async.AsyncOperation} mandates that two methods {@link toto.async.AsyncOperation#doOperation()} and
 * {@link toto.async.AsyncOperation#isResponseValid(Object)} are implemented. The time consuming operation should be implemented in the  {@link toto.async.AsyncOperation#doOperation()} method. In this case, the complex series of string formatting operations.
 * The {@link toto.async.AsyncOperation#isResponseValid(Object)} takes the object that is returned by {@link toto.async.AsyncOperation#doOperation()} and returns true if it considers the object to be valid.
 * <pre>
 * class StringOperation extends AsyncOperation<String> {
 *
 *	private final String textToFormat;
 *
 *	StringOperation(final String textToFormat) {
 *		this.textToFormat = textToFormat;
 *		
 *	}
 *
 *	<span>/*
 *	* Check if the response returned from doOperation is valid.
 *	*  /</span>
 *	@Override
 *	public boolean isResponseValid(final String t) {
 *		if (isFormattedCorrectly(t)) {
 *			return true;
 *		}
 *		return false;
 *	}
 *
 *	<span>/*
 *	* Perform needed string operations.
 *	*  /</span>
 *	@Override
 *	public String doOperation() throws Exception {
 *		<span>// perform formatting operation on string
 *		...
 *		...</span>
 *		return formattedText;
 *	}
 *
 *	private boolean isFormattedCorrectly(final String text) {
 *		<span>// perform check</span>
 *		return true;
 *	}
 * }
 * </pre>
 * <h4>2. Extend the AsyncClient</h4>
 * Extending the {@link toto.async.AsyncClient} class grants access to the thread pool allowing {@link toto.async.AsyncOperation} to run.
 * <pre>
 * public class AsyncStringClient extends AsyncClient {
 * 
 *  <span>//method to perform the long string operation.</span>
 *	public void formatString(final Context context, final String toFormat,
 *			final AsyncCallback<String> callback) {
 *		<span>//</span>
 *		super.addTask(context, new StringOperation(toFormat), callback);
 *	}
 * }
 * </pre>
 * 
 * <h4>Using the Async client</h4>
 * <pre>
 * 	<span>//Create an instance of the Async client</span>
 * 	final AsyncStringClient stringOp=new AsyncStringClient();
 * 		<span>//The string to be formatted.</span>
 *		final String toFormat=...;
 *		stringOp.formatString(getApplicationContext(),toFormat,new AsyncCallback<String>() {
 *
 *			@Override
 *			public void onSuccess(final String data) {
 *				</span>//formatted string</span>
 *			}
 *
 *			<span>//Something when wrong with the Async operation or isResponseValid() returned false.</span>
 *			@Override
 *			public void onFailure(final String data, final Throwable t) {
 *				
 *			}
 *		});
 *	}
 * </pre>
 * </div>
 * 
 * </p>
 */
package toto.async;

