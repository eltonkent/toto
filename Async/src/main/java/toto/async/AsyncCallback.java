package toto.async;

/**
 * Callback class for any asynchronous operation.
 * <p>
 * </p>
 * 
 * @author Mobifluence Interactive
 * 
 * @param <T>
 *            The object type returned by this async operation.
 */
public abstract class AsyncCallback<T> {

	public void onStarted() {
	}

	public void onFinished() {
	}

	/**
	 * Asynchronous operation was a success.
	 * 
	 * @param data
	 */
	public abstract void onSuccess(final T data);

	/**
	 * 
	 * @param data
	 * @param t
	 */
	public abstract void onFailure(final T data, Throwable t);
}
