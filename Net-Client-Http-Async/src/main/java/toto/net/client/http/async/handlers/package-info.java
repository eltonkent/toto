/**
 * Custom response handlers for {@link toto.net.client.asynchttp.AsyncHttpClient
 */
package toto.net.client.http.async.handlers;