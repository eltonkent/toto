package toto.net.client.http.async.handlers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;

import java.io.InputStream;

import toto.net.client.http.async.AsyncHttpHandler;

/**
 * Bitamp handler
 * 
 * @author Mobifluence Interactive
 * @since 1.1
 */
public class BitmapHandler extends AsyncHttpHandler<Bitmap> {

	private final Options options;

	/**
	 *
	 * @param options
	 *            bitmap factory options. can be null
	 * @author Mobifluence Interactive
	 * @since 1.1
	 */
	public BitmapHandler(final Options options) {
		this.options = options;
	}

	@Override
	protected boolean isValid(final Bitmap t) {
		return t != null;
	}

	@Override
	protected Bitmap process(final InputStream is) {
		final Bitmap bitmap = BitmapFactory.decodeStream(is, null, options);
		return bitmap;
	}
}
