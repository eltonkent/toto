package toto.net.client.http.async.handlers;

import java.io.IOException;
import java.io.InputStream;

import toto.io.IOUtils;
import toto.net.client.http.async.AsyncHttpHandler;

public class StringHandler extends AsyncHttpHandler<String> {

	@Override
	public boolean isValid(final String t) {
		return t != null;
	}

	@Override
	public String process(final InputStream is) {
		try {
			return IOUtils.asString(is);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
