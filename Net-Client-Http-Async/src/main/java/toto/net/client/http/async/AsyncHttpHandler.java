package toto.net.client.http.async;

import java.io.InputStream;

public abstract class AsyncHttpHandler<T> {
	protected abstract boolean isValid(final T t);

	protected abstract T process(InputStream is);

}
