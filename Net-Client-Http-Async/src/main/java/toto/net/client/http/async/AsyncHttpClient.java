/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.async;

import android.content.Context;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.HttpVersion;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.SyncBasicHttpContext;

import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.zip.GZIPInputStream;

import toto.async.AsyncCallback;
import toto.async.AsyncClient;
import toto.async.AsyncOperation;
import toto.net.client.http.HttpConstants;
import toto.net.client.http.async.handlers.StringHandler;

/**
 * The AsyncHttpClient can be used to make asynchronous GET, POST, PUT and
 * DELETE HTTP requests in your Android applications. Requests can be made with
 * additional parameters by passing a {@link RequestParams} instance, and
 * responses can be handled by passing an anonymously overridden
 * {@link AsyncHttpResponseHandler} instance.
 * 
 * <b>Requests sent using AsyncHttpClient do NOT have to be in a separate
 * thread. {@link AsyncHttpClient} maintains its own thread pool.</b>
 * 
 * @see RequestParams
 * @see AsyncCallback
 */
public class AsyncHttpClient {
	AsyncClient asyncClient = AsyncClient.getReusableInstance();

	/**
	 * Overrides the threadpool implementation used by the underlying
	 * {@link AsyncClient} implementation.
	 * 
	 * 
	 * @param threadPool
	 *            an instance of {@link ThreadPoolExecutor} to use for
	 *            queuing/pooling requests.
	 * @see AsyncClient#setThreadPool(ThreadPoolExecutor)
	 */
	public void setThreadPool(final ThreadPoolExecutor threadPool) {
		this.asyncClient.setThreadPool(threadPool);
	}

	private class AsyncClientRunnable extends AsyncOperation {

		private final HttpUriRequest uriRequest;
		private final HttpContext context;
		private int executionCount;

		public AsyncClientRunnable(final HttpUriRequest uriRequest,
				final HttpContext context) {
			this.uriRequest = uriRequest;
			this.context = context;
		}

		@Override
		public Object doOperation() throws Exception {
			final HttpResponse response = makeRequestWithRetries();
			if (response != null) {
				final HttpEntity temp = response.getEntity();
				if (temp != null) {
					return handler.process(temp.getContent());
				}
			}
			return null;
		}

		@Override
		public boolean isResponseValid(final Object t) {
			return handler.isValid(t);
		}

		private HttpResponse makeRequest() throws IOException {
			if (!Thread.currentThread().isInterrupted()) {
				final HttpResponse response = httpClient.execute(uriRequest,
						context);
				return response;
			}
			return null;
		}

		private HttpResponse makeRequestWithRetries() throws ConnectException {
			boolean retry = true;
			IOException cause = null;
			final HttpRequestRetryHandler retryHandler = httpClient
					.getHttpRequestRetryHandler();
			while (retry) {
				try {
					return makeRequest();
				} catch (final IOException e) {
					cause = e;
					retry = retryHandler.retryRequest(cause, ++executionCount,
							context);
				} catch (final NullPointerException e) {
					// there's a bug in HttpClient 4.0.x that on some occasions
					// causes
					// DefaultRequestExecutor to throw an NPE, see
					// http://code.google.com/p/android/issues/detail?id=5255
					cause = new IOException("NPE in HttpClient"
							+ e.getMessage());
					retry = retryHandler.retryRequest(cause, ++executionCount,
							context);
				}
			}
			// no retries left, crap out with exception
			final ConnectException ex = new ConnectException();
			ex.initCause(cause);
			throw ex;
		}

	}

	private static class InflatingEntity extends HttpEntityWrapper {
		public InflatingEntity(final HttpEntity wrapped) {
			super(wrapped);
		}

		@Override
		public InputStream getContent() throws IOException {
			return new GZIPInputStream(wrappedEntity.getContent());
		}

		@Override
		public long getContentLength() {
			return -1;
		}
	}

	private static final int DEFAULT_MAX_CONNECTIONS = 10;
	private static final int DEFAULT_MAX_RETRIES = 5;
	private static final int DEFAULT_SOCKET_BUFFER_SIZE = 8192;
	private static final int DEFAULT_SOCKET_TIMEOUT = 10 * 1000;

	private static final String ENCODING_GZIP = "gzip";
	private static int maxConnections = DEFAULT_MAX_CONNECTIONS;

	private static int socketTimeout = DEFAULT_SOCKET_TIMEOUT;
	private final Map<String, String> clientHeaderMap;
	private final DefaultHttpClient httpClient;

	private final HttpContext httpContext;

	private AsyncHttpHandler handler = new StringHandler();

	/**
	 * Creates a new AsyncHttpClient.
	 */
	public AsyncHttpClient() {
		final BasicHttpParams httpParams = new BasicHttpParams();

		ConnManagerParams.setTimeout(httpParams, socketTimeout);
		ConnManagerParams.setMaxConnectionsPerRoute(httpParams,
				new ConnPerRouteBean(maxConnections));
		ConnManagerParams.setMaxTotalConnections(httpParams,
				DEFAULT_MAX_CONNECTIONS);

		HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
		HttpConnectionParams.setConnectionTimeout(httpParams, socketTimeout);
		HttpConnectionParams.setTcpNoDelay(httpParams, true);
		HttpConnectionParams.setSocketBufferSize(httpParams,
				DEFAULT_SOCKET_BUFFER_SIZE);

		HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setUserAgent(httpParams, "ToTo");

		final SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", SSLSocketFactory
				.getSocketFactory(), 443));
		final ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(
				httpParams, schemeRegistry);

		httpContext = new SyncBasicHttpContext(new BasicHttpContext());
		httpClient = new DefaultHttpClient(cm, httpParams);
		httpClient.addRequestInterceptor(new HttpRequestInterceptor() {
			@Override
			public void process(final HttpRequest request,
					final HttpContext context) {
				if (!request
						.containsHeader(HttpConstants.HEADER_ACCEPT_ENCODING)) {
					request.addHeader(HttpConstants.HEADER_ACCEPT_ENCODING,
							ENCODING_GZIP);
				}
				for (final String header : clientHeaderMap.keySet()) {
					request.addHeader(header, clientHeaderMap.get(header));
				}
			}
		});

		httpClient.addResponseInterceptor(new HttpResponseInterceptor() {
			@Override
			public void process(final HttpResponse response,
					final HttpContext context) {
				final HttpEntity entity = response.getEntity();
				final Header encoding = entity.getContentEncoding();
				if (encoding != null) {
					for (final HeaderElement element : encoding.getElements()) {
						if (element.getName().equalsIgnoreCase(ENCODING_GZIP)) {
							response.setEntity(new InflatingEntity(response
									.getEntity()));
							break;
						}
					}
				}
			}
		});
		httpClient.setHttpRequestRetryHandler(new RetryHandler(
				DEFAULT_MAX_RETRIES));
		clientHeaderMap = new HashMap<String, String>();
	}

	private HttpEntityEnclosingRequestBase addEntityToRequestBase(
			final HttpEntityEnclosingRequestBase requestBase,
			final HttpEntity entity) {
		if (entity != null) {
			requestBase.setEntity(entity);
		}

		return requestBase;
	}

	/**
	 * Sets headers that will be added to all requests this client makes (before
	 * sending).
	 * 
	 * @param header
	 *            the name of the header
	 * @param value
	 *            the contents of the header
	 */
	public void addHeader(final String header, final String value) {
		clientHeaderMap.put(header, value);
	}

	/**
	 * Perform a HTTP DELETE request.
	 * 
	 * @param context
	 *            the Android Context which initiated the request.
	 * @param url
	 *            the URL to send the request to.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 * @see #setAsyncHttpHandler(AsyncHttpHandler)
	 */
	public void delete(final Context context, final String url,
			final AsyncCallback<?> responseHandler) {
		final HttpDelete delete = new HttpDelete(url);
		sendRequest(httpClient, httpContext, delete, null, responseHandler,
				context);
	}

	//
	// HTTP GET Requests
	//

	/**
	 * Perform a HTTP DELETE request.
	 * 
	 * @param url
	 *            the URL to send the request to.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 * @see #setAsyncHttpHandler(AsyncHttpHandler)
	 */
	public void delete(final String url, final AsyncCallback<?> responseHandler) {
		delete(null, url, responseHandler);
	}

	/**
	 * Perform a HTTP GET request without any parameters and track the Android
	 * Context which initiated the request.
	 * 
	 * @param context
	 *            the Android Context which initiated the request.
	 * @param url
	 *            the URL to send the request to.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 * @see #setAsyncHttpHandler(AsyncHttpHandler)
	 */
	public void get(final Context context, final String url,
			final AsyncCallback<?> responseHandler) {
		get(context, url, null, responseHandler);
	}

	/**
	 * Perform a HTTP GET request and track the Android Context which initiated
	 * the request.
	 * 
	 * @param context
	 *            the Android Context which initiated the request.
	 * @param url
	 *            the URL to send the request to.
	 * @param params
	 *            additional GET parameters to send with the request.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 * @see #setAsyncHttpHandler(AsyncHttpHandler)
	 */
	public void get(final Context context, final String url,
			final RequestParams params, final AsyncCallback<?> responseHandler) {
		sendRequest(httpClient, httpContext,
				new HttpGet(getUrlWithQueryString(url, params)), null,
				responseHandler, context);
	}

	/**
	 * Perform a HTTP GET request, without any parameters.
	 * 
	 * @param url
	 *            the URL to send the request to.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 * @see #setAsyncHttpHandler(AsyncHttpHandler)
	 */
	public void get(final String url, final AsyncCallback<?> responseHandler) {
		get(null, url, null, responseHandler);
	}

	//
	// HTTP POST Requests
	//

	/**
	 * Perform a HTTP GET request with parameters.
	 * 
	 * @param url
	 *            the URL to send the request to.
	 * @param params
	 *            additional GET parameters to send with the request.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 * @see #setAsyncHttpHandler(AsyncHttpHandler)
	 */
	public void get(final String url, final RequestParams params,
			final AsyncCallback<String> responseHandler) {
		get(null, url, params, responseHandler);
	}

	public final AsyncHttpHandler<?> getAsyncHttpHandler() {
		return handler;
	}

	/**
	 * Get the underlying HttpClient instance. This is useful for setting
	 * additional fine-grained settings for requests by accessing the client's
	 * ConnectionManager, HttpParams and SchemeRegistry.
	 */
	public HttpClient getHttpClient() {
		return this.httpClient;
	}

	private String getUrlWithQueryString(String url, final RequestParams params) {
		if (params != null) {
			final String paramString = params.getParamString();
			url += "?" + paramString;
		}

		return url;
	}

	private HttpEntity paramsToEntity(final RequestParams params) {
		HttpEntity entity = null;
		if (params != null) {
			entity = params.getEntity();
		}
		return entity;
	}

	/**
	 * Perform a HTTP POST request and track the Android Context which initiated
	 * the request.
	 * 
	 * @param context
	 *            the Android Context which initiated the request.
	 * @param url
	 *            the URL to send the request to.
	 * @param entity
	 *            a raw {@link HttpEntity} to send with the request, for
	 *            example, use this to send string/json/xml payloads to a server
	 *            by passing a {@link org.apache.http.entity.StringEntity}.
	 * @param contentType
	 *            the content type of the payload you are sending, for example
	 *            application/json if sending a json payload.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 * @see #setAsyncHttpHandler(AsyncHttpHandler)
	 */
	public void post(final Context context, final String url,
			final HttpEntity entity, final String contentType,
			final AsyncCallback<?> responseHandler) {
		sendRequest(httpClient, httpContext,
				addEntityToRequestBase(new HttpPost(url), entity), contentType,
				responseHandler, context);
	}

	/**
	 * Perform a HTTP POST request and track the Android Context which initiated
	 * the request.
	 * 
	 * @param context
	 *            the Android Context which initiated the request.
	 * @param url
	 *            the URL to send the request to.
	 * @param params
	 *            additional POST parameters or files to send with the request.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 * @see #setAsyncHttpHandler(AsyncHttpHandler)
	 */
	public void post(final Context context, final String url,
			final RequestParams params, final AsyncCallback<?> responseHandler) {
		post(context, url, paramsToEntity(params), null, responseHandler);
	}

	/**
	 * Perform a HTTP POST request, without any parameters.
	 * 
	 * @param url
	 *            the URL to send the request to.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 */
	public void post(final String url, final AsyncCallback<?> responseHandler) {
		post(null, url, null, responseHandler);
	}

	/**
	 * Perform a HTTP POST request with parameters.
	 * 
	 * @param url
	 *            the URL to send the request to.
	 * @param params
	 *            additional POST parameters or files to send with the request.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 */
	public void post(final String url, final RequestParams params,
			final AsyncCallback<?> responseHandler) {
		post(null, url, params, responseHandler);
	}

	/**
	 * Perform a HTTP PUT request and track the Android Context which initiated
	 * the request.
	 * 
	 * @param context
	 *            the Android Context which initiated the request.
	 * @param url
	 *            the URL to send the request to.
	 * @param entity
	 *            a raw {@link HttpEntity} to send with the request, for
	 *            example, use this to send string/json/xml payloads to a server
	 *            by passing a {@link org.apache.http.entity.StringEntity}.
	 * @param contentType
	 *            the content type of the payload you are sending, for example
	 *            application/json if sending a json payload.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 */
	public void put(final Context context, final String url,
			final HttpEntity entity, final String contentType,
			final AsyncCallback<?> responseHandler) {
		sendRequest(httpClient, httpContext,
				addEntityToRequestBase(new HttpPut(url), entity), contentType,
				responseHandler, context);
	}

	/**
	 * Perform a HTTP PUT request and track the Android Context which initiated
	 * the request.
	 * 
	 * @param context
	 *            the Android Context which initiated the request.
	 * @param url
	 *            the URL to send the request to.
	 * @param params
	 *            additional PUT parameters or files to send with the request.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 */
	public void put(final Context context, final String url,
			final RequestParams params, final AsyncCallback<?> responseHandler) {
		put(context, url, paramsToEntity(params), null, responseHandler);
	}

	/**
	 * Perform a HTTP PUT request, without any parameters.
	 * 
	 * @param url
	 *            the URL to send the request to.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 */
	public void put(final String url,
			final AsyncCallback<String> responseHandler) {
		put(null, url, null, responseHandler);
	}

	/**
	 * Perform a HTTP PUT request with parameters.
	 * 
	 * @param url
	 *            the URL to send the request to.
	 * @param params
	 *            additional PUT parameters or files to send with the request.
	 * @param responseHandler
	 *            the response handler instance that should handle the response.
	 * @see #setAsyncHttpHandler(AsyncHttpHandler)
	 */
	public void put(final String url, final RequestParams params,
			final AsyncCallback<?> responseHandler) {
		put(null, url, params, responseHandler);
	}

	@SuppressWarnings("unchecked")
	private void sendRequest(final DefaultHttpClient client,
			final HttpContext httpContext, final HttpUriRequest uriRequest,
			final String contentType, final AsyncCallback<?> responseHandler,
			final Context context) {
		if (contentType != null) {
			uriRequest.addHeader("Content-Type", contentType);
		}
		final AsyncClientRunnable runnable = new AsyncClientRunnable(
				uriRequest, httpContext);
		asyncClient.addTask(context, runnable, responseHandler);
	}

	/**
	 * Set the handler object that handles the http response.
	 * <p>
	 * The default handler is {@link StringHandler}
	 * </p>
	 * 
	 * @param handler
	 * @see toto.net.client.async.handlers.JSONHandler
	 * @see toto.net.client.asynchttp.handlers.StringHandler
	 * @see toto.net.client.asynchttp.handlers.XMLHandler
	 * @see toto.net.client.asynchttp.handlers.BitmapHandler
	 */
	public final void setAsyncHttpHandler(final AsyncHttpHandler<?> handler) {
		this.handler = handler;
	}

	/**
	 * Sets an optional CookieStore to use when making requests
	 * 
	 * @param cookieStore
	 *            The CookieStore implementation to use, usually an instance of
	 *            {@link PersistentCookieStore}
	 */
	public void setCookieStore(final CookieStore cookieStore) {
		httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
	}

	/**
	 * Sets the SSLSocketFactory to user when making requests. By default, a
	 * new, default SSLSocketFactory is used.
	 * 
	 * @param sslSocketFactory
	 *            the socket factory to use for https requests.
	 */
	public void setSSLSocketFactory(final SSLSocketFactory sslSocketFactory) {
		this.httpClient.getConnectionManager().getSchemeRegistry()
				.register(new Scheme("https", sslSocketFactory, 443));
	}

	/**
	 * Sets the User-Agent header to be sent with each request.
	 * 
	 * @param userAgent
	 *            the string to use in the User-Agent header.
	 */
	public void setUserAgent(final String userAgent) {
		HttpProtocolParams.setUserAgent(this.httpClient.getParams(), userAgent);
	}
}
