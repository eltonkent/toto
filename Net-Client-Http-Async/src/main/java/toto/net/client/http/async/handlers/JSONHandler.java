package toto.net.client.http.async.handlers;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import toto.io.IOUtils;
import toto.net.client.http.async.AsyncHttpHandler;
import toto.xc.json.Json;

public class JSONHandler<T> extends AsyncHttpHandler<T> {

	private final Class<T> type;

	/**
	 * 
	 * @param clz
	 *            to deserialize the json response to.
	 */
	public JSONHandler(final Class<T> clz) {
		this.type = clz;
	}

	@Override
	public boolean isValid(final T t) {
		return t != null;
	}

	@Override
	public T process(final InputStream is) {
		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				is));
		final T t = new Json().fromJson(reader, type);
		IOUtils.closeQuietly(reader);
		return t;
	}
}
