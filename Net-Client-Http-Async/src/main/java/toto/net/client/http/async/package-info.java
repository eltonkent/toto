/**
 * Highly responsive and simple Asynchronous Http client implementation.
 * <p>
 * <div>
 * Flexible implementation that allows you to make any http method with support multipart uploads.
 * </div>
 * </p>
 *
 */
package toto.net.client.http.async;