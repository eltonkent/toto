package toto.cache.converter.impl;

import java.nio.charset.Charset;

import toto.cache.SerializedStorageConverter;


public final class StringConverter extends SerializedStorageConverter<String> {

	private final Charset charset;

	/**
	 * Create a new String converter
	 * 
	 * @param charset
	 *            Character set to use for this string converter. can be null.
	 */
	public StringConverter(final Charset charset) {
		this.charset = charset;
	}

	protected String toType(final byte[] data) {
		return charset == null ? new String(data) : new String(data, charset);
	}

	protected byte[] toBytes(final String data) {
		return data.getBytes();
	}

}
