package toto.cache.converter.impl;

import com.mi.toto.ToTo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import toto.cache.SerializedStorageConverter;
import toto.io.IOUtils;

public class InputStreamConverter extends SerializedStorageConverter<InputStream> {

	protected InputStream toType(final byte[] data) {
		return new ByteArrayInputStream(data);
	}

	protected byte[] toBytes(final InputStream data) {
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			IOUtils.copy(data, bos);
			return bos.toByteArray();
		} catch (final IOException e) {
			ToTo.logException(e);
		}
		return null;
	}

}
