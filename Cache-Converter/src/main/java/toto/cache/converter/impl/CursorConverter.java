package toto.cache.converter.impl;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;

import com.mi.toto.ToTo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import toto.cache.SerializedStorageConverter;
import toto.io.IOUtils;

/**
 * Cache converters for caching database Cursors.
 * <p>
 * 
 * </p>
 * 
 * @author Mobifluence Interactive
 * 
 */
public class CursorConverter extends SerializedStorageConverter<Cursor> {

	/**
	 * Type representing cursor column datatype.
	 * 
	 * @author Mobifluence Interactive
	 * 
	 */
	public enum ColumnType {
		/**
		 * Blob column type (byte[])
		 */
		BLOB('b'),
		/**
		 * String type.
		 * <p>
		 * The string encoding can be changed using the cursor converter
		 * constructor with the specific encoding.
		 * </p>
		 */
		STRING('s'),
		/**
		 * 
		 */
		INTEGER('i'),
		/**
		 * 
		 */
		DOUBLE('d'),
		/**
		 * 
		 */
		FLOAT('f'),
		/**
		 * 
		 */
		LONG('l'),
		/**
		 * 
		 */
		SHORT('t'),
		/**
		 * 
		 */
		NULL('n');

		char typeCode;

		private ColumnType(final char type) {
			this.typeCode = type;
		}
	}

	private final ColumnType[] colTypes;

	private final String stringEncoding;

	/**
	 * Array of {@link ColumnType} specifying the datatype for every column.
	 * <p>
	 * If the types are mismatched, those cursor fields end up becoming empty or
	 * null when fetched from the cache.
	 * </p>
	 * 
	 * @param columnTypes
	 *            {@link #CursorConverter(ColumnType[])} representing the
	 *            columns mapped in the table used by the cursor.
	 */
	public CursorConverter(final ColumnType[] columnTypes) {
		this(columnTypes, "UTF-8");
	}

	/**
	 * 
	 * 
	 * @param columnTypes
	 * @param encoding
	 *            Encoding to use for strings. The default is UTF-8.
	 */
	public CursorConverter(final ColumnType[] columnTypes, final String encoding) {
		this.colTypes = columnTypes;
		this.stringEncoding = encoding;
	}

	@Override
	protected Cursor toType(final byte[] data) {
		return createCursor(data);
	}

	@Override
	protected byte[] toBytes(final Cursor data) {
		if (data.isClosed()) {
			throw new RuntimeException("Provided cursor is already closed");
		}
		if (data.getColumnCount() != colTypes.length) {
			throw new RuntimeException(
					" The number of columns in the cursors is not equal to the number of column types specified in Constructor");
		}
		return serializeCursor(data);
	}

	private String loadedEncoding;

	private Cursor createCursor(final byte[] data) {
		final DataInputStream dis = new DataInputStream(
				new ByteArrayInputStream(data));
		TCursor cursor = null;
		try {
			final String magic = dis.readUTF();
			if (!magic.equals("RAGE")) {
				throw new RuntimeException(
						" Could not convert byte[] to cursor.Invalid byte[] data format.");
			}
			loadedEncoding = dis.readUTF();
			final int rows = dis.readInt();
			final int cols = dis.readInt();
			cursor = new TCursor(rows, cols);
			if (rows > 0 && cols > 0) {
				readColumnNames(cols, dis, cursor);
				for (int i = 0; i < rows; i++) {
					readRow(dis, cursor, cols);
					cursor.moveToNextRow();
				}
				cursor.reset();
			}
		} catch (final IOException e) {
			try {
				dis.close();
			} catch (final IOException e1) {
				e1.printStackTrace();
			}
			ToTo.logException(e);
			throw new RuntimeException(
					" Could not convert byte[] to cursor.Error reading byte[] data format.");
		}
		return cursor;
	}

	private void readColumnNames(final int cols, final DataInputStream dis,
			final TCursor cursor) throws IOException {
		final String[] columnNames = new String[cols];
		for (int i = 0; i < cols; i++) {
			columnNames[i] = dis.readUTF();
		}
		cursor.setColumnNames(columnNames);
	}

	private void writeColumnNames(final Cursor cursor,
			final DataOutputStream dos, final String[] colNames)
			throws IOException {
		for (int i = 0; i < colNames.length; i++) {
			dos.writeUTF(colNames[i]);
		}
	}

	private void readRow(final DataInputStream dis, final TCursor cursor,
			final int colsInrow) throws IOException {
		for (int columnIndex = 0; columnIndex < colsInrow; columnIndex++) {
			readField(cursor, dis);
		}
	}

	private void writeRow(final DataOutputStream dos, final Cursor cursor,
			final int colsInRow) throws IOException {
		for (int columnIndex = 0; columnIndex < colsInRow; columnIndex++) {
			if (colTypes != null) {
				writeField(cursor, colTypes[columnIndex], dos, columnIndex);
			} else {
				// TODO: find strategy to decide column types
			}
			// cursor.get
		}
	}

	private void readField(final TCursor cursor, final DataInputStream dis)
			throws IOException {
		final char colType = dis.readChar();
		switch (colType) {
		case 's':
			int length = dis.readInt();
			byte[] data = new byte[length];
			dis.read(data);
			cursor.setField(new String(data, loadedEncoding));
			break;
		case 'i':
			final int i = dis.readInt();
			cursor.setField(i);
			break;
		case 'd':
			final double d = dis.readDouble();
			cursor.setField(d);
			break;
		case 'f':
			final float f = dis.readFloat();
			cursor.setField(f);
			break;
		case 'l':
			final long l = dis.readLong();
			cursor.setField(l);
			break;
		case 'b':
			length = dis.readInt();
			data = new byte[length];
			dis.read(data);
			cursor.setField(data);
			break;
		case 't':
			final short st = dis.readShort();
			cursor.setField(st);
			break;
		case 'n':
			cursor.setField(null);
			break;
		}
		cursor.moveToNextColumn();

	}

	/**
	 * Write a single field to the data stream
	 * 
	 * @param cursor
	 * @param type
	 * @param dos
	 * @param columnIndex
	 * @throws IOException
	 */
	private void writeField(final Cursor cursor, final ColumnType type,
			final DataOutputStream dos, final int columnIndex)
			throws IOException {
		switch (type.typeCode) {
		case 's':
			final String str = cursor.getString(columnIndex);
			if (str == null) {
				dos.writeChar('n');
			} else {
				dos.writeChar('s');
				final byte[] data = str.getBytes(stringEncoding);
				dos.writeInt(data.length);
				dos.write(data);
			}
			break;
		case 'i':
			final int i = cursor.getInt(columnIndex);
			dos.writeChar('i');
			dos.writeInt(i);

			break;
		case 'd':
			final double d = cursor.getDouble(columnIndex);
			dos.writeChar('d');
			dos.writeDouble(d);
			break;
		case 'f':
			final float f = cursor.getFloat(columnIndex);
			dos.writeChar('f');
			dos.writeFloat(f);
			break;
		case 'l':
			final long l = cursor.getLong(columnIndex);
			dos.writeChar('l');
			dos.writeLong(l);
			break;
		case 'b':
			final byte[] blob = cursor.getBlob(columnIndex);
			if (blob == null) {
				dos.writeChar('n');
			} else {
				dos.writeChar('b');
				dos.writeInt(blob.length);
				dos.write(blob);
			}
			break;
		case 't':
			final short st = cursor.getShort(columnIndex);
			dos.writeChar('t');
			dos.writeShort(st);
			break;
		/**
		 * write a null value
		 */
		case 'n':
			dos.writeChar('n');
			break;
		}
	}

	/**
	 * Format
	 * <ol>
	 * <li>String :magic number</li>
	 * <li>int :number of rows</li>
	 * <li>int :number of cols</li>
	 * <li>String[] : column names</li>
	 * </ol>
	 * 
	 * @author Mobifluence Interactive
	 * 
	 */
	private byte[] serializeCursor(final Cursor cursor) {
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		final DataOutputStream dos = new DataOutputStream(bos);
		try {
			final int rows = cursor.getCount();
			final int cols = cursor.getColumnCount();
			dos.writeUTF("RAGE");
			dos.writeUTF(stringEncoding);
			if (rows > 0 && cols > 0) {
				dos.writeInt(rows);
				dos.writeInt(cols);
				writeColumnNames(cursor, dos, cursor.getColumnNames());
				cursor.moveToFirst();
				for (int i = 0; i < rows; i++) {
					writeRow(dos, cursor, cols);
					cursor.moveToNext();
				}
			}
			bos.flush();
		} catch (final IOException e) {
			IOUtils.closeQuietly(dos);
			ToTo.logException(e);
			throw new RuntimeException(" Could not serialize cursor: "
					+ e.getMessage());
		}
		return bos.toByteArray();
	}

	private final class TCursor implements Cursor {

		private Object[][] data;
		private String[] columnNames;
		int rowIndex, columnIndex;
		boolean isClosed;

		TCursor(final int rows, final int cols) {
			data = new Object[rows][cols];
		}

		private void setField(final Object val) {
			data[rowIndex][columnIndex] = val;
		}

		@Override
		public void close() {
			isClosed = true;
			data = null;
			columnNames = null;
		}

		@Override
		public void copyStringToBuffer(final int columnIndex,
				final CharArrayBuffer buffer) {
			buffer.data = getString(columnIndex).toCharArray();
		}

		@Override
		public void deactivate() {
			close();
		}

		@Override
		public byte[] getBlob(final int columnIndex) {
			if (data[rowIndex][columnIndex] instanceof byte[]) {
				return (byte[]) data[rowIndex][columnIndex];
			}
			return null;
		}

		@Override
		public int getColumnCount() {
			return data[0].length;
		}

		@Override
		public int getColumnIndex(final String columnName) {
			for (int i = 0; i < columnNames.length; i++) {
				if (columnName.equals(columnNames[i])) {
					return i;
				}
			}
			return -1;
		}

		@Override
		public int getColumnIndexOrThrow(final String columnName)
				throws IllegalArgumentException {
			for (int i = 0; i < columnNames.length; i++) {
				if (columnName.equals(columnNames[i])) {
					return i;
				}
			}
			throw new IllegalArgumentException("invalid column name:"
					+ columnName);
		}

		@Override
		public String getColumnName(final int columnIndex) {
			if (columnIndex < columnNames.length) {
				return columnNames[columnIndex];
			}
			return null;
		}

		@Override
		public String[] getColumnNames() {
			return columnNames;
		}

		public void setColumnNames(final String[] cols) {
			this.columnNames = cols;
		}

		@Override
		public int getCount() {
			return data.length;
		}

		@Override
		public double getDouble(final int columnIndex) {
			if (data[rowIndex][columnIndex] instanceof Double) {
				return (Double) data[rowIndex][columnIndex];
			}
			return 0;
		}

		public int getType(final int i) {
			return 0;
		}

		@Override
		public Bundle getExtras() {
			return null;
		}

		@Override
		public float getFloat(final int columnIndex) {
			if (data[rowIndex][columnIndex] instanceof Float) {
				return (Float) data[rowIndex][columnIndex];
			}
			return 0;
		}

		@Override
		public int getInt(final int columnIndex) {
			if (data[rowIndex][columnIndex] instanceof Integer) {
				return (Integer) data[rowIndex][columnIndex];
			}
			return 0;
		}

		@Override
		public long getLong(final int columnIndex) {
			if (data[rowIndex][columnIndex] instanceof Long) {
				return (Long) data[rowIndex][columnIndex];
			}
			return 0;
		}

		@Override
		public int getPosition() {
			return rowIndex;
		}

		@Override
		public short getShort(final int columnIndex) {
			if (data[rowIndex][columnIndex] instanceof Short) {
				return (Short) data[rowIndex][columnIndex];
			}
			return 0;
		}

		@Override
		public String getString(final int columnIndex) {
			if (data[rowIndex][columnIndex] instanceof String) {
				return (String) data[rowIndex][columnIndex];
			}
			return null;
		}

		@Override
		public boolean getWantsAllOnMoveCalls() {
			return false;
		}

		@Override
		public boolean isAfterLast() {
			return false;
		}

		@Override
		public boolean isBeforeFirst() {
			return rowIndex == -1;
		}

		@Override
		public boolean isClosed() {
			return isClosed;
		}

		@Override
		public boolean isFirst() {
			return rowIndex == 0;
		}

		@Override
		public boolean isLast() {
			return rowIndex == (getCount() - 1);
		}

		@Override
		public boolean isNull(final int columnIndex) {
			return data[rowIndex][columnIndex] == null;
		}

		@Override
		public boolean move(final int offset) {
			return false;
		}

		@Override
		public boolean moveToFirst() {
			rowIndex = columnIndex = 0;
			return true;
		}

		@Override
		public boolean moveToLast() {
			rowIndex = data.length - 1;
			columnIndex = 0;
			return true;
		}

		@Override
		public boolean moveToNext() {
			if (rowIndex < (getCount() - 1)) {
				rowIndex++;
				return true;
			}
			return false;
		}

		private void moveToNextRow() {
			columnIndex = 0;
			if (rowIndex < (getCount() - 1))
				rowIndex++;
		}

		private void moveToNextColumn() {
			if (columnIndex < (getColumnCount() - 1))
				columnIndex++;
		}

		private void reset() {
			rowIndex = columnIndex = 0;
		}

		@Override
		public boolean moveToPosition(final int position) {
			if (position >= 0 && position < getColumnCount()) {
				rowIndex = position;
				columnIndex = 0;
				return true;
			}
			return false;
		}

		@Override
		public boolean moveToPrevious() {
			if (rowIndex > 0) {
				rowIndex--;
				return true;
			}
			return false;
		}

		@Override
		public void registerContentObserver(final ContentObserver observer) {

		}

		@Override
		public void registerDataSetObserver(final DataSetObserver observer) {
		}

		@Override
		public boolean requery() {
			return false;
		}

		@Override
		public Bundle respond(final Bundle extras) {
			return null;
		}

		@Override
		public void setNotificationUri(final ContentResolver cr, final Uri uri) {

		}

		@Override
		public Uri getNotificationUri() {
			return null;
		}

		@Override
		public void unregisterContentObserver(final ContentObserver observer) {

		}

		@Override
		public void unregisterDataSetObserver(final DataSetObserver observer) {

		}

	}
}
