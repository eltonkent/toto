package toto.cache.converter.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import toto.cache.SerializedStorageConverter;


/**
 * Converter implementation for any {@link Serializable} object
 * 
 * @author Mobifluence Interactive
 * 
 */
public class SerializableConverter<Serializable> extends
		SerializedStorageConverter<Serializable> {

	@Override
	protected final Serializable toType(final byte[] data) {
		try {
			ObjectInputStream objectInputStream=new ObjectInputStream(new ByteArrayInputStream(data));
			Serializable serializable=(Serializable) objectInputStream.readObject();
			objectInputStream.close();
			return serializable;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return  null;
	}

	@Override
	protected final byte[] toBytes(final Serializable data) {
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			final ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(data);
			oos.flush();
			oos.close();
			bos.flush();
			return bos.toByteArray();
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
