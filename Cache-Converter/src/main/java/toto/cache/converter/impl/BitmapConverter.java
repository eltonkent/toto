package toto.cache.converter.impl;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

import toto.cache.SerializedStorageConverter;

/**
 * Storage converter fo bitmap objects
 */
public class BitmapConverter extends SerializedStorageConverter<Bitmap> {

	private final CompressFormat format;

	/**
	 * 
	 * @param format
	 *            Bitmap compression format.
	 */
	public BitmapConverter(final CompressFormat format) {
		this.format = format;
	}

	protected Bitmap toType(final byte[] data) {
		return BitmapFactory.decodeByteArray(data, 0, data.length);
	}

	protected byte[] toBytes(final Bitmap data) {
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		data.compress(format, 100, bos);
		return bos.toByteArray();
	}
}
