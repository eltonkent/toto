package toto.cache.converter.impl;

import android.os.Parcelable;

import com.mi.toto.ToTo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import toto.cache.SerializedStorageConverter;
import toto.io.ParcelUtils;

/**
 * Converter for Android's parcellable serialization format.
 * 
 * @author ekent4
 * 
 */
public class ParcelableConverter extends SerializedStorageConverter<Parcelable> {

	@Override
	protected Parcelable toType(final byte[] data) {
		final ByteArrayInputStream bis = new ByteArrayInputStream(data);
		try {
			return ParcelUtils.readObject(new DataInputStream(bis));
		} catch (final IOException e) {
			ToTo.logException(e);
			return null;
		}
	}

	@Override
	protected byte[] toBytes(final Parcelable data) {
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ParcelUtils.writeObject(new DataOutputStream(bos), data);
			bos.flush();
			return bos.toByteArray();
		} catch (final IOException e) {
			ToTo.logException(e);
			return null;
		}
	}
}
