package toto.cache.converter.impl;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import test.toto.ToToTestCase;

public class ConvertersTest extends ToToTestCase {

//	public void testBitmapConverter() {
//		BitmapConverter converter = new BitmapConverter(CompressFormat.JPEG);
//		try {
//			Method method = BitmapConverter.class.getDeclaredMethod(
//					"toBytes", Bitmap.class);
//			method.setAccessible(true);
//			Bitmap bitmap = BitmapFactory.decodeResource(getContext()
//					.getResources(), test.toto.cache.R.drawable.girlskin);
//			byte[] data = (byte[]) method.invoke(converter, bitmap);
//			assertEquals(data.length, 112142);
//			method = BitmapConverter.class.getDeclaredMethod("toType",
//					byte[].class);
//			method.setAccessible(true);
//			bitmap = (Bitmap) method.invoke(converter, data);
//			assertNotNull(bitmap);
//		} catch (NoSuchMethodException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		} catch (InvocationTargetException e) {
//			e.printStackTrace();
//		}
//	}

	public void testStringConverter() {
		String data="test";
		StringConverter converter = new StringConverter(null);
		byte[] bytData=converter.toBytes(data);
		assertNotNull(bytData);
		String dataStr=converter.toType(bytData);
		assertNotNull(dataStr);
		assertEquals(dataStr,data);

//		try {
//			Method method = StringConverter.class.getDeclaredMethod(
//					"toBytes", String.class);
//			method.setAccessible(true);
//
//			byte[] data = (byte[]) method.invoke(converter, "test");
//			assertEquals(data.length, 4);
//			method = StringConverter.class.getDeclaredMethod("toType",
//					byte[].class);
//			method.setAccessible(true);
//			String test = (String) method.invoke(converter, data);
//			assertNotNull(test);
//		} catch (NoSuchMethodException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		} catch (InvocationTargetException e) {
//			e.printStackTrace();
//		}
	}

	
	public void testCursorConverter() {
//		getContext().deleteDatabase("test_cache.db");
//		String tablename = "testtoto";
//		TestSQLiteHelper helper = new TestSQLiteHelper(getContext());
//		SQLiteDatabase db = helper.getWritableDatabase();
//		RandomString rstring = new RandomString();
//		for (int i = 0; i < 10; i++) {
//			ContentValues values = new ContentValues();
//			values.put(
//					"col_text",
//					"Welcome:"
//							+ i
//							+ " fdgfdgfd sgsfdg fdgfd fdg fdg fdg fdg dfgfd gfdgfd fgdfdgfdg dgfd gfd gfdg fdgfd fdg fdgfd fdgfdg fdgfdgfdgfd");
//			db.insert("testrage", null, values);
//		}
//		long time = System.currentTimeMillis();
//		Cursor query = db.query(tablename,
//				new String[] { "col_id", "col_text" }, "col_id="
//						+ RandomPrimitives.randomInt(1, 9), null, null, null,
//				null);
//		query.moveToFirst();
//		String text = query.getString(1);
//		// query.close();
//		time = System.currentTimeMillis() - time;
//		logI("Query :" + text + " Time taken(ms):" + time);
//		
//		getContext().deleteDatabase("test_cache.db");
		
	}
	
	private class TestSQLiteHelper extends SQLiteOpenHelper {
		public TestSQLiteHelper(Context context) {
			super(context, "test_cache.db", null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("create table testrage (col_id integer primary key autoincrement, col_text text not null);");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS testrage");
			onCreate(db);
		}
	}

}
