package toto.db.odb;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

class AltPersistentSet<T> extends AltBtree<T> implements ODbSet<T> {
	AltPersistentSet() {
		type = ClassDescriptor.tpObject;
		unique = true;
	}

	AltPersistentSet(final boolean unique) {
		type = ClassDescriptor.tpObject;
		this.unique = unique;
	}

	public boolean isEmpty() {
		return nElems == 0;
	}

	public boolean contains(final Object o) {
		final Key key = new Key(o);
		final Iterator<T> i = iterator(key, key, ASCENT_ORDER);
		return i.hasNext();
	}

	public <E> E[] toArray(final E[] arr) {
		return (E[]) super.toArray((T[]) arr);
	}

	public boolean add(final T obj) {
		return put(new Key(obj), obj);
	}

	public boolean remove(final Object o) {
		final T obj = (T) o;
		return removeIfExists(new BtreeKey(checkKey(new Key(obj)), obj));
	}

	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Set)) {
			return false;
		}
		final Collection c = (Collection) o;
		if (c.size() != size()) {
			return false;
		}
		return containsAll(c);
	}

	public int hashCode() {
		int h = 0;
		final Iterator i = iterator();
		while (i.hasNext()) {
			h += getODB().getOid(i.next());
		}
		return h;
	}

	public IterableIterator<T> join(final Iterator<T> with) {
		return with == null ? (IterableIterator<T>) iterator()
				: new JoinSetIterator<T>(getODB(), iterator(), with);
	}
}
