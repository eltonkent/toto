package toto.db.odb;

/**
 * Exception thrown by the ODb query compiler.
 */
public class ODbCompileError extends RuntimeException {
	public ODbCompileError(String msg, int pos) {
		super(msg + " in position " + pos);
	}
}
