package toto.db.odb;

/**
 * Interface implemented by all ODb iterators allowing to get OID of the current
 * object
 */
public interface ODbObjectIterator {
	/**
	 * Get OID of the next object
	 * 
	 * @return OID of the the next element in the iteration or 0 if iteration
	 *         has no more objects.
	 */
	public int nextObjectid();
}
