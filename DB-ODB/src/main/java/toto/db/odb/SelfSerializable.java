package toto.db.odb;

/**
 * Interface of the classes sleft responsible for their serialization
 */
interface SelfSerializable {
	/**
	 * Serialize object
	 * 
	 * @param out
	 *            writer to be used for object serialization
	 */
	void pack(ODbOutputStream out) throws java.io.IOException;

	/**
	 * Deserialize object
	 * 
	 * @param in
	 *            reader to be used for objet deserialization
	 */
	void unpack(ODbInputStream in) throws java.io.IOException;
}