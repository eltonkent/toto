package toto.db.odb;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;

import toto.io.file.FileUtils;

class OSFile implements ODbFile {
	public void write(final long pos, final byte[] buf) {
		try {
			file.seek(pos);
			file.write(buf, 0, buf.length);
		} catch (final IOException x) {
			throw new ODbException(ODbException.FILE_ACCESS_ERROR, x);
		}
	}

	public int read(final long pos, final byte[] buf) {
		try {
			file.seek(pos);
			return file.read(buf, 0, buf.length);
		} catch (final IOException x) {
			throw new ODbException(ODbException.FILE_ACCESS_ERROR, x);
		}
	}

	public void sync() {
		if (!noFlush) {
			try {
				file.getFD().sync();
			} catch (final IOException x) {
				throw new ODbException(ODbException.FILE_ACCESS_ERROR, x);
			}
		}
	}

	public void close() {
		try {
			file.close();
		} catch (final IOException x) {
			throw new ODbException(ODbException.FILE_ACCESS_ERROR, x);
		}
	}

	public boolean tryLock(final boolean shared) {
		try {
			lck = FileUtils.tryToLock(file, shared);
			return lck != null;
		} catch (final IOException x) {
			return true;
		}
	}

	public void lock(final boolean shared) {
		try {
			lck = file.getChannel().lock(0, Long.MAX_VALUE, shared);
		} catch (final IOException x) {
			throw new ODbException(ODbException.LOCK_FAILED, x);
		}
	}

	public void unlock() {
		try {
			lck.release();
		} catch (final IOException x) {
			throw new ODbException(ODbException.LOCK_FAILED, x);
		}
	}

	/*
	 * JDK 1.3 and older public static boolean lockFile(RandomAccessFile file,
	 * boolean shared) { try { Class cls = file.getClass(); Method getChannel =
	 * cls.getMethod("getChannel", new Class[0]); if (getChannel != null) {
	 * Object channel = getChannel.invoke(file, new Object[0]); if (channel !=
	 * null) { cls = channel.getClass(); Class[] paramType = new Class[3];
	 * paramType[0] = Long.TYPE; paramType[1] = Long.TYPE; paramType[2] =
	 * Boolean.TYPE; Method lock = cls.getMethod("lock", paramType); if (lock !=
	 * null) { Object[] param = new Object[3]; param[0] = new Long(0); param[1]
	 * = new Long(Long.MAX_VALUE); param[2] = new Boolean(shared); return
	 * lock.invoke(channel, param) != null; } } } } catch (Exception x) {}
	 * 
	 * return true; }
	 */

	public OSFile(final String filePath, final boolean readOnly,
			final boolean noFlush) {
		this.noFlush = noFlush;
		try {
			file = FileUtils.createRandomAccessFile(filePath, readOnly);
		} catch (final IOException x) {
			throw new ODbException(ODbException.FILE_ACCESS_ERROR, x);
		}
	}

	public long length() {
		try {
			return file.length();
		} catch (final IOException x) {
			return -1;
		}
	}

	protected RandomAccessFile file;
	protected boolean noFlush;
	private FileLock lck;
}
