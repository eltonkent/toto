package toto.db.odb;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Input stream for SelfSerializable and CustomSerializer
 */
public abstract class ODbInputStream extends DataInputStream {
	/**
	 * Read refeernce to the object or content of the enbedded object
	 * 
	 * @return unswizzled object
	 */
	public abstract Object readObject() throws IOException;

	/**
	 * Read string according to the ODb string encoding
	 * 
	 * @return extracted string or null
	 */
	public abstract String readString() throws IOException;

	public ODbInputStream(final InputStream stream) {
		super(stream);
	}
}
