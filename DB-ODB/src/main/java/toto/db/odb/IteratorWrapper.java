package toto.db.odb;

import java.util.Iterator;

class IteratorWrapper<T> extends IterableIterator<T> {
	private Iterator<T> iterator;

	IteratorWrapper(Iterator<T> iterator) {
		this.iterator = iterator;
	}

	public boolean hasNext() {
		return iterator.hasNext();
	}

	public T next() {
		return iterator.next();
	}

	public void remove() {
		iterator.remove();
	}
}
