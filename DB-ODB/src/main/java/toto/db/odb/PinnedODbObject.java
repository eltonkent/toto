package toto.db.odb;

import com.mi.toto.Conditions;

/**
 * Base class for all persistent capable objects. Unlike Persistent class it
 * doesn't define finalize method. It means that object derived from this class
 * rather than Persistent, can be used either with infinite object cache
 * (DEFAULT_PAGE_POOL_SIZE) either with per-thread serializable transactions (in
 * the last case all modified objects are pinned in memory until the end of
 * transaction). Presence of finalize method adds additional memory overhead
 * (especially for small classes) and slow down garbage collections. So using
 * this class instead of Persistent can improve performance of your application.
 */
class PinnedODbObject implements IODbObject, ICloneable {
	public synchronized void load() {
		if (oid != 0 && (state & RAW) != 0) {
			storage.loadObject(this);
		}
	}

	public synchronized void loadAndModify() {
		load();
		modify();
	}

	public final boolean isRaw() {
		return (state & RAW) != 0;
	}

	public final boolean isModified() {
		return (state & DIRTY) != 0;
	}

	public final boolean isDeleted() {
		return (state & DELETED) != 0;
	}

	public final boolean isPersistent() {
		return oid != 0;
	}

	public void makePersistent(final ODb storage) {
		if (oid == 0) {
			storage.makePersistent(this);
		}
	}

	public void store() {
		if ((state & RAW) != 0) {
			throw new ODbException(ODbException.ACCESS_TO_STUB);
		}
		if (storage != null) {
			storage.storeObject(this);
			state &= ~DIRTY;
		}
	}

	public void modify() {
		if ((state & DIRTY) == 0 && oid != 0) {
			if ((state & RAW) != 0) {
				throw new ODbException(ODbException.ACCESS_TO_STUB);
			}
			Conditions.assertArgument((state & DELETED) == 0);
			storage.modifyObject(this);
			state |= DIRTY;
		}
	}

	public PinnedODbObject() {
	}

	public PinnedODbObject(final ODb storage) {
		this.storage = storage;
	}

	public final int getObjectId() {
		return oid;
	}

	public void deallocate() {
		if (oid != 0) {
			storage.removeObject(this);
		}
	}

	public boolean recursiveLoading() {
		return true;
	}

	public final ODb getODB() {
		return storage;
	}

	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (oid == 0) {
			return super.equals(o);
		}
		return o instanceof IODbObject && ((IODbObject) o).getObjectId() == oid;
	}

	public int hashCode() {
		return oid;
	}

	public void onLoad() {
	}

	public void onStore() {
	}

	public void invalidate() {
		state &= ~DIRTY;
		state |= RAW;
	}

	transient ODb storage;
	transient int oid;
	transient int state;

	static public final int RAW = 1;
	static public final int DIRTY = 2;
	static public final int DELETED = 4;

	public void unassignObjectId() {
		oid = 0;
		state = DELETED;
		storage = null;
	}

	public void assignObjectId(final ODb storage, final int oid,
			final boolean raw) {
		this.oid = oid;
		this.storage = storage;
		if (raw) {
			state |= RAW;
		} else {
			state &= ~RAW;
		}
	}

	protected void clearState() {
		state = 0;
		oid = 0;
	}

	public Object clone() throws CloneNotSupportedException {
		final ODbObject p = (ODbObject) super.clone();
		p.oid = 0;
		p.state = 0;
		return p;
	}

	public void readExternal(final java.io.ObjectInput s)
			throws java.io.IOException, ClassNotFoundException {
		oid = s.readInt();
	}

	public void writeExternal(final java.io.ObjectOutput s)
			throws java.io.IOException {
		if (s instanceof ODbImpl.PersistentObjectOutputStream) {
			makePersistent(((ODbImpl.PersistentObjectOutputStream) s)
					.getStorage());
		}
		s.writeInt(oid);
	}
}
