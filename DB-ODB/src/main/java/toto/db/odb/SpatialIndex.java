package toto.db.odb;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import toto.geom2d.Rectangle;

/**
 * Interface of object spatial index. Spatial index is used to allow fast
 * selection of spatial objects belonging to the specified Rectangle. Spatial
 * index is implemented using Guttman R-Tree with quadratic split algorithm.
 */
public interface SpatialIndex<T> extends IODbObject, Resource, ODbTable<T> {
	/**
	 * Find all objects located in the selected Rectangle
	 * 
	 * @param r
	 *            selected Rectangle
	 * @return array of objects which enveloping Rectangle intersects with
	 *         specified Rectangle
	 */
	public Object[] get(Rectangle r);

	/**
	 * Find all objects located in the selected Rectangle
	 * 
	 * @param r
	 *            selected Rectangle
	 * @return array list of objects which enveloping Rectangle intersects with
	 *         specified Rectangle
	 */
	public ArrayList<T> getList(Rectangle r);

	/**
	 * Put new object in the index.
	 * 
	 * @param r
	 *            enveloping Rectangle for the object
	 * @param obj
	 *            object associated with this Rectangle. Object can be not yet
	 *            persistent, in this case its forced to become persistent by
	 *            assigning OID to it.
	 */
	public void put(Rectangle r, T obj);

	/**
	 * Remove object with specified enveloping Rectangle from the tree.
	 * 
	 * @param r
	 *            enveloping Rectangle for the object
	 * @param obj
	 *            object removed from the index
	 * @exception ODbException
	 *                (StorageError.KEY_NOT_FOUND) exception if there is no such
	 *                key in the index
	 */
	public void remove(Rectangle r, T obj);

	/**
	 * Get wrapping Rectangle
	 * 
	 * @return minimal Rectangle containing all rectangles in the index,
	 *         <code>null</code> if index is empty
	 */
	public Rectangle getWrappingRectangle();

	/**
	 * Get iterator through all members of the index This iterator doesn't
	 * support remove() method. It is not possible to modify spatial index
	 * during iteration.
	 * 
	 * @return iterator through all objects in the index
	 */
	public Iterator<T> iterator();

	/**
	 * Get entry iterator through all members of the index This iterator doesn't
	 * support remove() method. It is not possible to modify spatial index
	 * during iteration.
	 * 
	 * @return entry iterator which key specifies recrtangle and value -
	 *         correspondent object
	 */
	public IterableIterator<Map.Entry<Rectangle, T>> entryIterator();

	/**
	 * Get objects which Rectangle intersects with specified Rectangle This
	 * iterator doesn't support remove() method. It is not possible to modify
	 * spatial index during iteration.
	 * 
	 * @param r
	 *            selected Rectangle
	 * @return iterator for objects which enveloping Rectangle overlaps with
	 *         specified Rectangle
	 */
	public IterableIterator<T> iterator(Rectangle r);

	/**
	 * Get entry iterator through objects which Rectangle intersects with
	 * specified Rectangle This iterator doesn't support remove() method. It is
	 * not possible to modify spatial index during iteration.
	 * 
	 * @param r
	 *            selected Rectangle
	 * @return entry iterator for objects which enveloping Rectangle overlaps
	 *         with specified Rectangle
	 */
	public IterableIterator<Map.Entry<Rectangle, T>> entryIterator(Rectangle r);

	/**
	 * Get iterator through all neighbors of the specified point in the order of
	 * increasing distance from the specified point to the wrapper Rectangle of
	 * the object
	 * 
	 * @param x
	 *            x coordinate of the point
	 * @param y
	 *            y coordinate of the point
	 * @return iterator through all objects in the index in the order of
	 *         increasing distance from the specified point
	 */
	public IterableIterator<T> neighborIterator(int x, int y);
}
