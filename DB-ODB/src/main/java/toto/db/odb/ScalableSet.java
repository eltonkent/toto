package toto.db.odb;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

class ScalableSet<T> extends ODbCollection<T> implements ODbSet<T> {
	Link<T> link;
	ODbSet<T> set;

	static final int BTREE_THRESHOLD = 128;

	ScalableSet(ODbImpl storage, int initialSize) {
		super(storage);
		if (initialSize <= BTREE_THRESHOLD) {
			link = storage.<T> createLink(initialSize);
		} else {
			set = storage.<T> createSet();
		}
	}

	ScalableSet() {
	}

	public boolean isEmpty() {
		return size() != 0;
	}

	public int size() {
		return link != null ? link.size() : set.size();
	}

	public void clear() {
		if (link != null) {
			link.clear();
			modify();
		} else {
			set.clear();
		}
	}

	public boolean contains(Object o) {
		return link != null ? link.contains(o) : set.contains(o);
	}

	public Object[] toArray() {
		return link != null ? link.toArray() : set.toArray();
	}

	public <E> E[] toArray(E a[]) {
		return link != null ? link.<E> toArray(a) : set.<E> toArray(a);
	}

	public Iterator<T> iterator() {
		return link != null ? link.iterator() : set.iterator();
	}

	private int binarySearch(T obj) {
		int l = 0, r = link.size();
		ODb storage = getODB();
		int oid = storage.getOid(obj);
		while (l < r) {
			int m = (l + r) >> 1;
			if (storage.getOid(link.getRaw(m)) > oid) {
				l = m + 1;
			} else {
				r = m;
			}
		}
		return r;
	}

	public boolean add(T obj) {
		if (link != null) {
			int i = binarySearch(obj);
			int n = link.size();
			if (i < n && link.getRaw(i).equals(obj)) {
				return false;
			}
			if (n == BTREE_THRESHOLD) {
				set = getODB().<T> createSet();
				for (i = 0; i < n; i++) {
					((ODbSet) set).add(link.getRaw(i));
				}
				link = null;
				modify();
				set.add(obj);
			} else {
				modify();
				link.insert(i, obj);
			}
			return true;
		} else {
			return set.add(obj);
		}
	}

	public boolean remove(Object o) {
		if (link != null) {
			if (link.remove(o)) {
				modify();
				return true;
			}
			return false;
		} else {
			return set.remove(o);
		}
	}

	public int hashCode() {
		int h = 0;
		Iterator<T> i = iterator();
		while (i.hasNext()) {
			h += getODB().getOid(i.next());
		}
		return h;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Set)) {
			return false;
		}
		Collection c = (Collection) o;
		if (c.size() != size()) {
			return false;
		}
		return containsAll(c);
	}

	public void deallocate() {
		if (set != null) {
			set.deallocate();
		}
		super.deallocate();
	}

	public IterableIterator<T> join(Iterator<T> with) {
		return with == null ? (IterableIterator<T>) iterator()
				: new JoinSetIterator<T>(getODB(), iterator(), with);
	}
}
