package toto.db.odb;

import toto.util.collections.algor.PatriciaTrie;

/**
 * ODB specific PatriciaTrie implementation.
 * 
 * @see PatriciaTrie
 * 
 * @param <T>
 */
public interface ODbPatriciaTrie<T> extends PatriciaTrie<T>, IODbObject,
		Resource, ODbTable<T> {

}
