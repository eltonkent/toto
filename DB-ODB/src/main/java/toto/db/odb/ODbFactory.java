package toto.db.odb;

/**
 * Create and load ODb Databases.
 * 
 * @author ekent4
 * 
 */
public class ODbFactory {

	/**
	 * Initialize and load a new ODatabase
	 * 
	 * @return
	 * @see ODb#open(String)
	 * @see ODb#open(OODatabaseFile)
	 * @see ODb#open(String, long)
	 */
	public static ODb createOODB() {
		return StorageFactory.getInstance().createStorage();
	}
}
