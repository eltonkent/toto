package toto.db.odb;

import java.util.List;

/**
 * Interface for ordered collection (sequence). The user can access elements by
 * their integer index (position in the list), and search for elements in the
 * list.
 * <p>
 */
public interface ODbList<E> extends IODbObject, List<E> {
}
