package toto.db.odb;

class DefaultAllocator extends ODbObject implements CustomAllocator {
	DefaultAllocator(ODb storage) {
		super(storage);
	}

	protected DefaultAllocator() {
	}

	public long allocate(long size) {
		return ((ODbImpl) getODB()).allocate(size, 0);
	}

	public long getSegmentBase() {
		return 0;
	}

	public long getSegmentSize() {
		return 1L << ODbImpl.dbLargeDatabaseOffsetBits;
	}

	public long reallocate(long pos, long oldSize, long newSize) {
		ODbImpl db = (ODbImpl) getODB();
		if (((newSize + ODbImpl.dbAllocationQuantum - 1) & ~(ODbImpl.dbAllocationQuantum - 1)) > ((oldSize
				+ ODbImpl.dbAllocationQuantum - 1) & ~(ODbImpl.dbAllocationQuantum - 1))) {
			long newPos = db.allocate(newSize, 0);
			db.cloneBitmap(pos, oldSize);
			db.free(pos, oldSize);
			pos = newPos;
		}
		return pos;
	}

	public void free(long pos, long size) {
		((ODbImpl) getODB()).cloneBitmap(pos, size);
	}

	public void commit() {
	}
}
