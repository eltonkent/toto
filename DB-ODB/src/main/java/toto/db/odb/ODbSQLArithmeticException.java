//-< JSQLArithmeticException.java >----------------------------------*--------*
// JSQL                       Version 1.04       (c) 1999  GARRET    *     ?  *
// (Java SQL)                                                        *   /\|  *
//                                                                   *  /  \  *
//                          Created:     10-Dec-2002  K.A. Knizhnik  * / [] \ *
//                          Last update: 10-Dec-2002  K.A. Knizhnik  * GARRET *
//-------------------------------------------------------------------*--------*
// Exception thown in case of incorect operands for integer operations
//-------------------------------------------------------------------*--------*

package toto.db.odb;

/**
 * Exception thown in case of incorect operands for integer operations
 */
public class ODbSQLArithmeticException extends ODbSQLRuntimeException {
	/**
	 * Constructor of exception
	 */
	public ODbSQLArithmeticException(String msg) {
		super(msg, null, null);
	}
}
