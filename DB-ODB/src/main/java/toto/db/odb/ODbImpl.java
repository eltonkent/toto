package toto.db.odb;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.mi.toto.Conditions;
import toto.db.odb.search.FullTextIndex;
import toto.lang.ByteUtils;
import toto.lang.ByteUtils.ArrayPos;

class ODbImpl implements ODb {
	/**
	 * Initialial database index size - increasing it reduce number of index
	 * reallocation but increase initial database size. Should be set before
	 * openning connection.
	 */
	static final int dbDefaultInitIndexSize = 1024;

	/**
	 * Initial capacity of object hash
	 */
	static final int dbDefaultObjectCacheInitSize = 1319;

	/**
	 * Database extension quantum. Memory is allocate by scanning bitmap. If
	 * there is no large enough hole, then database is extended by the value of
	 * dbDefaultExtensionQuantum This parameter should not be smaller than
	 * dbFirstUserId
	 */
	static final long dbDefaultExtensionQuantum = 1024 * 1024;

	static final long dbDefaultPagePoolLruLimit = 1L << 60;

	static final int dbDatabaseOidBits = 31; // up to 2 milliards of objects
	static final int dbDatabaseOffsetBits = 32; // up to 4 gigabyte
	static final int dbLargeDatabaseOffsetBits = 40; // up to 1 terabyte
	static final int dbMaxObjectOid = (1 << dbDatabaseOidBits) - 1;

	static final int dbAllocationQuantumBits = 5;
	static final int dbAllocationQuantum = 1 << dbAllocationQuantumBits;
	static final int dbBitmapSegmentBits = Page.pageSizeLog + 3
			+ dbAllocationQuantumBits;
	static final int dbBitmapSegmentSize = 1 << dbBitmapSegmentBits;
	static final int dbBitmapPages = 1 << (dbDatabaseOffsetBits - dbBitmapSegmentBits);
	static final int dbLargeBitmapPages = 1 << (dbLargeDatabaseOffsetBits - dbBitmapSegmentBits);
	static final int dbHandlesPerPageBits = Page.pageSizeLog - 3;
	static final int dbHandlesPerPage = 1 << dbHandlesPerPageBits;
	static final int dbDirtyPageBitmapSize = 1 << (dbDatabaseOidBits
			- dbHandlesPerPageBits - 3);

	static final int dbAllocRecursionLimit = 10;

	static final int dbInvalidId = 0;
	static final int dbBitmapId = 1;
	static final int dbFirstUserId = dbBitmapId + dbBitmapPages;

	static final int dbPageObjectFlag = 1;
	static final int dbModifiedFlag = 2;
	static final int dbFreeHandleFlag = 4;
	static final int dbFlagsMask = 7;
	static final int dbFlagsBits = 3;

	/**
	 * Current version of database format. 0 means that database is not
	 * initilized.
	 */
	static final byte dbDatabaseFormatVersion = (byte) 3;

	final int getBitmapPageId(final int i) {
		return i < dbBitmapPages ? dbBitmapId + i
				: header.root[1 - currIndex].bitmapExtent + i
						- bitmapExtentBase;
	}

	final long getPos(final int oid) {
		synchronized (objectCache) {
			if (oid == 0 || oid >= currIndexSize) {
				throw new ODbException(ODbException.INVALID_OID);
			}
			if (multiclientSupport && !isInsideThreadTransaction()) {
				throw new ODbException(ODbException.NOT_IN_TRANSACTION);
			}
			final Page pg = pool
					.getPage(header.root[1 - currIndex].index
							+ ((long) (oid >>> dbHandlesPerPageBits) << Page.pageSizeLog));
			final long pos = ByteUtils.unpack8(pg.data,
					(oid & (dbHandlesPerPage - 1)) << 3);
			pool.unfix(pg);
			return pos;
		}
	}

	final void setPos(final int oid, final long pos) {
		synchronized (objectCache) {
			dirtyPagesMap[oid >>> (dbHandlesPerPageBits + 5)] |= 1 << ((oid >>> dbHandlesPerPageBits) & 31);
			final Page pg = pool
					.putPage(header.root[1 - currIndex].index
							+ ((long) (oid >>> dbHandlesPerPageBits) << Page.pageSizeLog));
			ByteUtils.pack8(pg.data, (oid & (dbHandlesPerPage - 1)) << 3, pos);
			pool.unfix(pg);
		}
	}

	final byte[] get(final int oid) {
		final long pos = getPos(oid);
		if ((pos & (dbFreeHandleFlag | dbPageObjectFlag)) != 0) {
			throw new ODbException(ODbException.INVALID_OID);
		}
		return pool.get(pos & ~dbFlagsMask);
	}

	final Page getPage(final int oid) {
		final long pos = getPos(oid);
		if ((pos & (dbFreeHandleFlag | dbPageObjectFlag)) != dbPageObjectFlag) {
			throw new ODbException(ODbException.DELETED_OBJECT);
		}
		return pool.getPage(pos & ~dbFlagsMask);
	}

	final Page putPage(final int oid) {
		synchronized (objectCache) {
			long pos = getPos(oid);
			if ((pos & (dbFreeHandleFlag | dbPageObjectFlag)) != dbPageObjectFlag) {
				throw new ODbException(ODbException.DELETED_OBJECT);
			}
			if ((pos & dbModifiedFlag) == 0) {
				dirtyPagesMap[oid >>> (dbHandlesPerPageBits + 5)] |= 1 << ((oid >>> dbHandlesPerPageBits) & 31);
				allocate(Page.pageSize, oid);
				cloneBitmap(pos & ~dbFlagsMask, Page.pageSize);
				pos = getPos(oid);
			}
			modified = true;
			return pool.putPage(pos & ~dbFlagsMask);
		}
	}

	int allocatePage() {
		final int oid = allocateId();
		setPos(oid, allocate(Page.pageSize, 0) | dbPageObjectFlag
				| dbModifiedFlag);
		return oid;
	}

	public/* protected */synchronized void removeObject(final Object obj) {
		synchronized (objectCache) {
			if (getOid(obj) == 0) {
				return;
			}
			if (useSerializableTransactions) {
				final ThreadTransactionContext ctx = getTransactionContext();
				if (ctx.nested != 0) { // serializable transaction
					ctx.deleted.add(obj);
					return;
				}
			}
			deallocateObject0(obj);
		}
	}

	public void throwObject(final Object obj) {
		objectCache.remove(getOid(obj));
	}

	private void deallocateObject0(final Object obj) {
		if (listener != null) {
			listener.onObjectDelete(obj);
		}
		final int oid = getOid(obj);
		final long pos = getPos(oid);
		objectCache.remove(oid);
		int offs = (int) pos & (Page.pageSize - 1);
		if ((offs & (dbFreeHandleFlag | dbPageObjectFlag)) != 0) {
			throw new ODbException(ODbException.DELETED_OBJECT);
		}
		final Page pg = pool.getPage(pos - offs);
		offs &= ~dbFlagsMask;
		final int size = ObjectHeader.getSize(pg.data, offs);
		pool.unfix(pg);
		freeId(oid);
		final CustomAllocator allocator = (customAllocatorMap != null) ? getCustomAllocator(obj
				.getClass()) : null;
		if (allocator != null) {
			allocator.free(pos & ~dbFlagsMask, size);
		} else {
			if ((pos & dbModifiedFlag) != 0) {
				free(pos & ~dbFlagsMask, size);
			} else {
				cloneBitmap(pos, size);
			}
		}
		unassignOid(obj);
	}

	final void freePage(final int oid) {
		final long pos = getPos(oid);
		Conditions
				.assertArgument((pos & (dbFreeHandleFlag | dbPageObjectFlag)) == dbPageObjectFlag);
		if ((pos & dbModifiedFlag) != 0) {
			free(pos & ~dbFlagsMask, Page.pageSize);
		} else {
			cloneBitmap(pos & ~dbFlagsMask, Page.pageSize);
		}
		freeId(oid);
	}

	int allocateId() {
		synchronized (objectCache) {
			int oid;
			final int curr = 1 - currIndex;
			setDirty();
			if (reuseOid && (oid = header.root[curr].freeList) != 0) {
				header.root[curr].freeList = (int) (getPos(oid) >> dbFlagsBits);
				Conditions.assertArgument(header.root[curr].freeList >= 0);
				dirtyPagesMap[oid >>> (dbHandlesPerPageBits + 5)] |= 1 << ((oid >>> dbHandlesPerPageBits) & 31);
				return oid;
			}

			if (currIndexSize > dbMaxObjectOid) {
				throw new ODbException(ODbException.TOO_MUCH_OBJECTS);
			}
			if (currIndexSize >= header.root[curr].indexSize) {
				final int oldIndexSize = header.root[curr].indexSize;
				int newIndexSize = oldIndexSize << 1;
				if (newIndexSize < oldIndexSize) {
					newIndexSize = Integer.MAX_VALUE & ~(dbHandlesPerPage - 1);
					if (newIndexSize <= oldIndexSize) {
						throw new ODbException(ODbException.NOT_ENOUGH_SPACE);
					}
				}
				final long newIndex = allocate(newIndexSize * 8L, 0);
				if (currIndexSize >= header.root[curr].indexSize) {
					final long oldIndex = header.root[curr].index;
					pool.copy(newIndex, oldIndex, currIndexSize * 8L);
					header.root[curr].index = newIndex;
					header.root[curr].indexSize = newIndexSize;
					free(oldIndex, oldIndexSize * 8L);
				} else {
					// index was already reallocated
					free(newIndex, newIndexSize * 8L);
				}
			}
			oid = currIndexSize;
			header.root[curr].indexUsed = ++currIndexSize;
			return oid;
		}
	}

	void freeId(final int oid) {
		synchronized (objectCache) {
			setPos(oid,
					((long) (header.root[1 - currIndex].freeList) << dbFlagsBits)
							| dbFreeHandleFlag);
			header.root[1 - currIndex].freeList = oid;
		}
	}

	static final int pageBits = Page.pageSize * 8;
	static final int inc = Page.pageSize / dbAllocationQuantum / 8;

	static final void memset(final Page pg, int offs, final int pattern, int len) {
		final byte[] arr = pg.data;
		final byte pat = (byte) pattern;
		while (--len >= 0) {
			arr[offs++] = pat;
		}
	}

	final void extend(final long size) {
		if (size > header.root[1 - currIndex].size) {
			header.root[1 - currIndex].size = size;
		}
	}

	private static class Location {
		long pos;
		long size;
		Location next;
	}

	final boolean wasReserved(final long pos, final long size) {
		for (Location location = reservedChain; location != null; location = location.next) {
			if ((pos >= location.pos && pos - location.pos < location.size)
					|| (pos <= location.pos && location.pos - pos < size)) {
				return true;
			}
		}
		return false;
	}

	final void reserveLocation(final long pos, final long size) {
		final Location location = new Location();
		location.pos = pos;
		location.size = size;
		location.next = reservedChain;
		reservedChain = location;
		reservedChainLength += 1;
	}

	final void commitLocation() {
		reservedChain = reservedChain.next;
		reservedChainLength -= 1;
	}

	final void setDirty() {
		modified = true;
		if (!header.dirty) {
			header.dirty = true;
			final Page pg = pool.putPage(0);
			header.pack(pg.data);
			pool.flush();
			pool.unfix(pg);
		}
	}

	protected boolean isDirty() {
		return header.dirty;
	}

	final Page putBitmapPage(final int i) {
		return putPage(getBitmapPageId(i));
	}

	final Page getBitmapPage(final int i) {
		return getPage(getBitmapPageId(i));
	}

	final long allocate(long size, final int oid) {
		synchronized (objectCache) {
			setDirty();
			size = (size + dbAllocationQuantum - 1)
					& ~(dbAllocationQuantum - 1);
			Conditions.assertArgument(size != 0);
			allocatedDelta += size;
			if (allocatedDelta > gcThreshold && !insideCloneBitmap) {
				gc0();
			}
			int objBitSize = (int) (size >> dbAllocationQuantumBits);
			Conditions
					.assertArgument(objBitSize == (size >> dbAllocationQuantumBits));
			long pos;
			int holeBitSize = 0;
			final int alignment = (int) size & (Page.pageSize - 1);
			int offs, firstPage, lastPage, i, j;
			int holeBeforeFreePage = 0;
			int freeBitmapPage = 0;
			final int curr = 1 - currIndex;
			Page pg;

			lastPage = header.root[curr].bitmapEnd - dbBitmapId;
			usedSize += size;

			if (alignment == 0) {
				if (reservedChainLength > dbAllocRecursionLimit) {
					firstPage = lastPage - 1;
					offs = 0;
				} else {
					firstPage = currPBitmapPage;
					offs = (currPBitmapOffs + inc - 1) & ~(inc - 1);
				}
			} else {
				firstPage = currRBitmapPage;
				offs = currRBitmapOffs;
			}

			while (true) {
				if (alignment == 0) {
					// allocate page object
					for (i = firstPage; i < lastPage; i++) {
						final int spaceNeeded = objBitSize - holeBitSize < pageBits ? objBitSize
								- holeBitSize
								: pageBits;
						if (bitmapPageAvailableSpace[i] <= spaceNeeded) {
							holeBitSize = 0;
							offs = 0;
							continue;
						}
						pg = getBitmapPage(i);
						int startOffs = offs;
						while (offs < Page.pageSize) {
							if (pg.data[offs++] != 0) {
								offs = (offs + inc - 1) & ~(inc - 1);
								holeBitSize = 0;
							} else if ((holeBitSize += 8) == objBitSize) {
								pos = (((long) i * Page.pageSize + offs) * 8 - holeBitSize) << dbAllocationQuantumBits;
								if (wasReserved(pos, size)) {
									startOffs = offs = (offs + inc - 1)
											& ~(inc - 1);
									holeBitSize = 0;
									continue;
								}
								reserveLocation(pos, size);
								currPBitmapPage = i;
								currPBitmapOffs = offs;
								extend(pos + size);
								if (oid != 0) {
									final long prev = getPos(oid);
									final int marker = (int) prev & dbFlagsMask;
									pool.copy(pos, prev - marker, size);
									setPos(oid, pos | marker | dbModifiedFlag);
								}
								pool.unfix(pg);
								pg = putBitmapPage(i);
								int holeBytes = holeBitSize >> 3;
								if (holeBytes > offs) {
									memset(pg, 0, 0xFF, offs);
									holeBytes -= offs;
									pool.unfix(pg);
									pg = putBitmapPage(--i);
									offs = Page.pageSize;
								}
								while (holeBytes > Page.pageSize) {
									memset(pg, 0, 0xFF, Page.pageSize);
									holeBytes -= Page.pageSize;
									bitmapPageAvailableSpace[i] = 0;
									pool.unfix(pg);
									pg = putBitmapPage(--i);
								}
								memset(pg, offs - holeBytes, 0xFF, holeBytes);
								commitLocation();
								pool.unfix(pg);
								return pos;
							}
						}
						if (startOffs == 0 && holeBitSize == 0
								&& spaceNeeded < bitmapPageAvailableSpace[i]) {
							bitmapPageAvailableSpace[i] = spaceNeeded;
						}
						offs = 0;
						pool.unfix(pg);
					}
				} else {
					for (i = firstPage; i < lastPage; i++) {
						final int spaceNeeded = objBitSize - holeBitSize < pageBits ? objBitSize
								- holeBitSize
								: pageBits;
						if (bitmapPageAvailableSpace[i] <= spaceNeeded) {
							holeBitSize = 0;
							offs = 0;
							continue;
						}
						pg = getBitmapPage(i);
						int startOffs = offs;
						while (offs < Page.pageSize) {
							final int mask = pg.data[offs] & 0xFF;
							if (holeBitSize + Bitmap.firstHoleSize[mask] >= objBitSize) {
								pos = (((long) i * Page.pageSize + offs) * 8 - holeBitSize) << dbAllocationQuantumBits;
								if (wasReserved(pos, size)) {
									startOffs = offs += 1;
									holeBitSize = 0;
									continue;
								}
								reserveLocation(pos, size);
								currRBitmapPage = i;
								currRBitmapOffs = offs;
								extend(pos + size);
								if (oid != 0) {
									final long prev = getPos(oid);
									final int marker = (int) prev & dbFlagsMask;
									pool.copy(pos, prev - marker, size);
									setPos(oid, pos | marker | dbModifiedFlag);
								}
								pool.unfix(pg);
								pg = putBitmapPage(i);
								pg.data[offs] |= (byte) ((1 << (objBitSize - holeBitSize)) - 1);
								if (holeBitSize != 0) {
									if (holeBitSize > offs * 8) {
										memset(pg, 0, 0xFF, offs);
										holeBitSize -= offs * 8;
										pool.unfix(pg);
										pg = putBitmapPage(--i);
										offs = Page.pageSize;
									}
									while (holeBitSize > pageBits) {
										memset(pg, 0, 0xFF, Page.pageSize);
										holeBitSize -= pageBits;
										bitmapPageAvailableSpace[i] = 0;
										pool.unfix(pg);
										pg = putBitmapPage(--i);
									}
									while ((holeBitSize -= 8) > 0) {
										pg.data[--offs] = (byte) 0xFF;
									}
									pg.data[offs - 1] |= (byte) ~((1 << -holeBitSize) - 1);
								}
								pool.unfix(pg);
								commitLocation();
								return pos;
							} else if (Bitmap.maxHoleSize[mask] >= objBitSize) {
								final int holeBitOffset = Bitmap.maxHoleOffset[mask];
								pos = (((long) i * Page.pageSize + offs) * 8 + holeBitOffset) << dbAllocationQuantumBits;
								if (wasReserved(pos, size)) {
									startOffs = offs += 1;
									holeBitSize = 0;
									continue;
								}
								reserveLocation(pos, size);
								currRBitmapPage = i;
								currRBitmapOffs = offs;
								extend(pos + size);
								if (oid != 0) {
									final long prev = getPos(oid);
									final int marker = (int) prev & dbFlagsMask;
									pool.copy(pos, prev - marker, size);
									setPos(oid, pos | marker | dbModifiedFlag);
								}
								pool.unfix(pg);
								pg = putBitmapPage(i);
								pg.data[offs] |= (byte) (((1 << objBitSize) - 1) << holeBitOffset);
								pool.unfix(pg);
								commitLocation();
								return pos;
							}
							offs += 1;
							if (Bitmap.lastHoleSize[mask] == 8) {
								holeBitSize += 8;
							} else {
								holeBitSize = Bitmap.lastHoleSize[mask];
							}
						}
						if (startOffs == 0 && holeBitSize == 0
								&& spaceNeeded < bitmapPageAvailableSpace[i]) {
							bitmapPageAvailableSpace[i] = spaceNeeded;
						}
						offs = 0;
						pool.unfix(pg);
					}
				}
				if (firstPage == 0
						|| reservedChainLength > dbAllocRecursionLimit) {
					if (freeBitmapPage > i) {
						i = freeBitmapPage;
						holeBitSize = holeBeforeFreePage;
					}
					objBitSize -= holeBitSize;
					// number of bits reserved for the object and aligned on
					// page boundary
					final int skip = (objBitSize + Page.pageSize
							/ dbAllocationQuantum - 1)
							& ~(Page.pageSize / dbAllocationQuantum - 1);
					// page aligned position after allocated object
					pos = ((long) i << dbBitmapSegmentBits)
							+ ((long) skip << dbAllocationQuantumBits);

					long extension = (size > extensionQuantum) ? size
							: extensionQuantum;
					int oldIndexSize = 0;
					long oldIndex = 0;
					int morePages = (int) ((extension + Page.pageSize
							* (dbAllocationQuantum * 8 - 1) - 1) / (Page.pageSize * (dbAllocationQuantum * 8 - 1)));
					if (i + morePages > dbLargeBitmapPages) {
						throw new ODbException(ODbException.NOT_ENOUGH_SPACE);
					}
					if (i <= dbBitmapPages && i + morePages > dbBitmapPages) {
						// We are out of space mapped by memory default
						// allocation bitmap
						oldIndexSize = header.root[curr].indexSize;
						if (oldIndexSize <= currIndexSize + dbLargeBitmapPages
								- dbBitmapPages) {
							int newIndexSize = oldIndexSize;
							oldIndex = header.root[curr].index;
							do {
								newIndexSize <<= 1;
								if (newIndexSize < 0) {
									newIndexSize = Integer.MAX_VALUE
											& ~(dbHandlesPerPage - 1);
									if (newIndexSize < currIndexSize
											+ dbLargeBitmapPages
											- dbBitmapPages) {
										throw new ODbException(
												ODbException.NOT_ENOUGH_SPACE);
									}
									break;
								}
							} while (newIndexSize <= currIndexSize
									+ dbLargeBitmapPages - dbBitmapPages);

							if (size + newIndexSize * 8L > extensionQuantum) {
								extension = size + newIndexSize * 8L;
								morePages = (int) ((extension + Page.pageSize
										* (dbAllocationQuantum * 8 - 1) - 1) / (Page.pageSize * (dbAllocationQuantum * 8 - 1)));
							}
							extend(pos + (long) morePages * Page.pageSize
									+ newIndexSize * 8L);
							final long newIndex = pos + (long) morePages
									* Page.pageSize;
							fillBitmap(
									pos
											+ (skip >> 3)
											+ (long) morePages
											* (Page.pageSize
													/ dbAllocationQuantum / 8),
									newIndexSize >>> dbAllocationQuantumBits);
							pool.copy(newIndex, oldIndex, oldIndexSize * 8L);
							header.root[curr].index = newIndex;
							header.root[curr].indexSize = newIndexSize;
						}
						final int[] newBitmapPageAvailableSpace = new int[dbLargeBitmapPages];
						System.arraycopy(bitmapPageAvailableSpace, 0,
								newBitmapPageAvailableSpace, 0, dbBitmapPages);
						for (j = dbBitmapPages; j < dbLargeBitmapPages; j++) {
							newBitmapPageAvailableSpace[j] = Integer.MAX_VALUE;
						}
						bitmapPageAvailableSpace = newBitmapPageAvailableSpace;

						for (j = 0; j < dbLargeBitmapPages - dbBitmapPages; j++) {
							setPos(currIndexSize + j, dbFreeHandleFlag);
						}

						header.root[curr].bitmapExtent = currIndexSize;
						header.root[curr].indexUsed = currIndexSize += dbLargeBitmapPages
								- dbBitmapPages;
					}
					extend(pos + (long) morePages * Page.pageSize);
					long adr = pos;
					int len = objBitSize >> 3;
					// fill bitmap pages used for allocation of object space
					// with 0xFF
					while (len >= Page.pageSize) {
						pg = pool.putPage(adr);
						memset(pg, 0, 0xFF, Page.pageSize);
						pool.unfix(pg);
						adr += Page.pageSize;
						len -= Page.pageSize;
					}
					// fill part of last page responsible for allocation of
					// object space
					pg = pool.putPage(adr);
					memset(pg, 0, 0xFF, len);
					pg.data[len] = (byte) ((1 << (objBitSize & 7)) - 1);
					pool.unfix(pg);

					// mark in bitmap newly allocated object
					fillBitmap(pos + (skip >> 3), morePages
							* (Page.pageSize / dbAllocationQuantum / 8));

					j = i;
					while (--morePages >= 0) {
						setPos(getBitmapPageId(j++), pos | dbPageObjectFlag
								| dbModifiedFlag);
						pos += Page.pageSize;
					}
					header.root[curr].bitmapEnd = j + dbBitmapId;
					j = i + objBitSize / pageBits;
					if (alignment != 0) {
						currRBitmapPage = j;
						currRBitmapOffs = 0;
					} else {
						currPBitmapPage = j;
						currPBitmapOffs = 0;
					}
					while (j > i) {
						bitmapPageAvailableSpace[--j] = 0;
					}

					pos = ((long) i * Page.pageSize * 8 - holeBitSize) << dbAllocationQuantumBits;
					if (oid != 0) {
						final long prev = getPos(oid);
						final int marker = (int) prev & dbFlagsMask;
						pool.copy(pos, prev - marker, size);
						setPos(oid, pos | marker | dbModifiedFlag);
					}

					if (holeBitSize != 0) {
						reserveLocation(pos, size);
						while (holeBitSize > pageBits) {
							holeBitSize -= pageBits;
							pg = putBitmapPage(--i);
							memset(pg, 0, 0xFF, Page.pageSize);
							bitmapPageAvailableSpace[i] = 0;
							pool.unfix(pg);
						}
						pg = putBitmapPage(--i);
						offs = Page.pageSize;
						while ((holeBitSize -= 8) > 0) {
							pg.data[--offs] = (byte) 0xFF;
						}
						pg.data[offs - 1] |= (byte) ~((1 << -holeBitSize) - 1);
						pool.unfix(pg);
						commitLocation();
					}
					if (oldIndex != 0) {
						free(oldIndex, oldIndexSize * 8L);
					}
					return pos;
				}
				if (gcThreshold != Long.MAX_VALUE && !gcDone && !gcActive
						&& !insideCloneBitmap) {
					allocatedDelta -= size;
					usedSize -= size;
					gc0();
					currRBitmapPage = currPBitmapPage = 0;
					currRBitmapOffs = currPBitmapOffs = 0;
					return allocate(size, oid);
				}
				freeBitmapPage = i;
				holeBeforeFreePage = holeBitSize;
				holeBitSize = 0;
				lastPage = firstPage + 1;
				firstPage = 0;
				offs = 0;
			}
		}
	}

	final void fillBitmap(long adr, int len) {
		while (true) {
			final int off = (int) adr & (Page.pageSize - 1);
			final Page pg = pool.putPage(adr - off);
			if (Page.pageSize - off >= len) {
				memset(pg, off, 0xFF, len);
				pool.unfix(pg);
				break;
			} else {
				memset(pg, off, 0xFF, Page.pageSize - off);
				pool.unfix(pg);
				adr += Page.pageSize - off;
				len -= Page.pageSize - off;
			}
		}
	}

	final void free(final long pos, final long size) {
		synchronized (objectCache) {
			Conditions.assertArgument(pos != 0
					&& (pos & (dbAllocationQuantum - 1)) == 0);
			final long quantNo = pos >>> dbAllocationQuantumBits;
			int objBitSize = (int) ((size + dbAllocationQuantum - 1) >>> dbAllocationQuantumBits);
			int pageId = (int) (quantNo >>> (Page.pageSizeLog + 3));
			int offs = (int) (quantNo & (Page.pageSize * 8 - 1)) >> 3;
			Page pg = putBitmapPage(pageId);
			final int bitOffs = (int) quantNo & 7;

			allocatedDelta -= (long) objBitSize << dbAllocationQuantumBits;
			usedSize -= (long) objBitSize << dbAllocationQuantumBits;

			if ((pos & (Page.pageSize - 1)) == 0 && size >= Page.pageSize) {
				if (pageId == currPBitmapPage && offs < currPBitmapOffs) {
					currPBitmapOffs = offs;
				}
			}
			if (pageId == currRBitmapPage && offs < currRBitmapOffs) {
				currRBitmapOffs = offs;
			}
			bitmapPageAvailableSpace[pageId] = Integer.MAX_VALUE;

			if (objBitSize > 8 - bitOffs) {
				objBitSize -= 8 - bitOffs;
				pg.data[offs++] &= (1 << bitOffs) - 1;
				while (objBitSize + offs * 8 > Page.pageSize * 8) {
					memset(pg, offs, 0, Page.pageSize - offs);
					pool.unfix(pg);
					pg = putBitmapPage(++pageId);
					bitmapPageAvailableSpace[pageId] = Integer.MAX_VALUE;
					objBitSize -= (Page.pageSize - offs) * 8;
					offs = 0;
				}
				while ((objBitSize -= 8) > 0) {
					pg.data[offs++] = (byte) 0;
				}
				pg.data[offs] &= (byte) ~((1 << (objBitSize + 8)) - 1);
			} else {
				pg.data[offs] &= (byte) ~(((1 << objBitSize) - 1) << bitOffs);
			}
			pool.unfix(pg);
		}
	}

	static class CloneNode {
		long pos;
		CloneNode next;

		CloneNode(final long pos, final CloneNode list) {
			this.pos = pos;
			this.next = list;
		}
	}

	final void cloneBitmap(long pos, long size) {
		synchronized (objectCache) {
			if (insideCloneBitmap) {
				Conditions.assertArgument(size == Page.pageSize);
				cloneList = new CloneNode(pos, cloneList);
			} else {
				insideCloneBitmap = true;
				while (true) {
					final long quantNo = pos >>> dbAllocationQuantumBits;
					int objBitSize = (int) ((size + dbAllocationQuantum - 1) >>> dbAllocationQuantumBits);
					int pageId = (int) (quantNo >>> (Page.pageSizeLog + 3));
					int offs = (int) (quantNo & (Page.pageSize * 8 - 1)) >> 3;
					final int bitOffs = (int) quantNo & 7;
					int oid = getBitmapPageId(pageId);
					pos = getPos(oid);
					if ((pos & dbModifiedFlag) == 0) {
						dirtyPagesMap[oid >>> (dbHandlesPerPageBits + 5)] |= 1 << ((oid >>> dbHandlesPerPageBits) & 31);
						allocate(Page.pageSize, oid);
						cloneBitmap(pos & ~dbFlagsMask, Page.pageSize);
					}

					if (objBitSize > 8 - bitOffs) {
						objBitSize -= 8 - bitOffs;
						offs += 1;
						while (objBitSize + offs * 8 > Page.pageSize * 8) {
							oid = getBitmapPageId(++pageId);
							pos = getPos(oid);
							if ((pos & dbModifiedFlag) == 0) {
								dirtyPagesMap[oid >>> (dbHandlesPerPageBits + 5)] |= 1 << ((oid >>> dbHandlesPerPageBits) & 31);
								allocate(Page.pageSize, oid);
								cloneBitmap(pos & ~dbFlagsMask, Page.pageSize);
							}
							objBitSize -= (Page.pageSize - offs) * 8;
							offs = 0;
						}
					}
					if (cloneList == null) {
						break;
					}
					pos = cloneList.pos;
					size = Page.pageSize;
					cloneList = cloneList.next;
				}
				insideCloneBitmap = false;
			}
		}
	}

	public void open(final String filePath) {
		open(filePath, DEFAULT_PAGE_POOL_SIZE);
	}

	public void open(final ODbFile file) {
		open(file, DEFAULT_PAGE_POOL_SIZE);
	}

	public synchronized void open(final String filePath, final long pagePoolSize) {
		ODbFile file;
		if (filePath != null) {
			file = filePath.startsWith("@") ? (ODbFile) new MultiFile(
					filePath.substring(1), readOnly, noFlush)
					: (ODbFile) new OSFile(filePath, readOnly, noFlush);
		} else {
			file = new NullFile();
		}
		try {
			open(file, pagePoolSize);
		} catch (final ODbException ex) {
			file.close();
			throw ex;
		}
	}

	public synchronized void open(final String filePath,
			final long pagePoolSize, final String cryptKey) {
		final Rc4File file = new Rc4File(filePath, readOnly, noFlush, cryptKey);
		try {
			open(file, pagePoolSize);
		} catch (final ODbException ex) {
			file.close();
			throw ex;
		}
	}

	public void clearObjectCache() {
		objectCache.clear();
	}

	protected ObjectIdHashTable createObjectCache(final String kind,
			final long pagePoolSize, final int objectCacheSize) {
		if ("strong".equals(kind)) {
			return new StrongHashTable(this, objectCacheSize);
		}
		if ("soft".equals(kind)) {
			return new SoftHashTable(this, objectCacheSize);
		}
		if ("weak".equals(kind)) {
			return new WeakHashTable(this, objectCacheSize);
		}
		if ("pinned".equals(kind)) {
			return new PinWeakHashTable(this, objectCacheSize);
		}
		if ("lru".equals(kind)) {
			return new LruObjectCache(this, objectCacheSize);
		}
		return pagePoolSize == INFINITE_PAGE_POOL ? (ObjectIdHashTable) new StrongHashTable(
				this, objectCacheSize)
				: (ObjectIdHashTable) new LruObjectCache(this, objectCacheSize);
	}

	protected void initialize(final ODbFile file, final long pagePoolSize) {
		this.file = file;
		if (lockFile && !multiclientSupport) {
			if (!file.tryLock(readOnly)) {
				throw new ODbException(ODbException.DATABASE_IS_USED);
			}
		}
		dirtyPagesMap = new int[dbDirtyPageBitmapSize / 4 + 1];
		gcThreshold = Long.MAX_VALUE;
		backgroundGcMonitor = new Object();
		backgroundGcStartMonitor = new Object();
		gcThread = null;
		gcActive = false;
		gcDone = false;
		allocatedDelta = 0;

		reservedChain = null;
		reservedChainLength = 0;
		cloneList = null;
		insideCloneBitmap = false;

		nNestedTransactions = 0;
		nBlockedTransactions = 0;
		nCommittedTransactions = 0;
		scheduledCommitTime = Long.MAX_VALUE;
		transactionMonitor = new Object();
		transactionLock = new ODbResource();

		modified = false;

		objectCache = createObjectCache(cacheKind, pagePoolSize,
				objectCacheInitSize);

		objMap = new ObjectMap(objectCacheInitSize);

		classDescMap = new HashMap();
		descList = null;

		recursiveLoadingPolicy = new HashMap();
		recursiveLoadingPolicyDefined = false;

		header = new Header();
		pool = new PagePool((int) (pagePoolSize / Page.pageSize),
				pagePoolLruLimit);
		pool.open(file);
	}

	public synchronized void open(final ODbFile file, final long pagePoolSize) {
		Page pg;
		int i;

		if (opened) {
			throw new ODbException(ODbException.DATABASE_ALREADY_OPENED);
		}
		initialize(file, pagePoolSize);

		if (multiclientSupport) {
			beginThreadTransaction(readOnly ? READ_ONLY_TRANSACTION
					: READ_WRITE_TRANSACTION);
		}
		final byte[] buf = new byte[Header.sizeof];
		final int rc = file.read(0, buf);
		final int corruptionError = (file instanceof Rc4File || file instanceof CompressedReadWriteFile) ? ODbException.WRONG_CIPHER_KEY
				: ODbException.DATABASE_CORRUPTED;
		if (rc > 0 && rc < Header.sizeof) {
			throw new ODbException(corruptionError);
		}
		header.unpack(buf);
		if (header.curr < 0 || header.curr > 1) {
			throw new ODbException(corruptionError);
		}
		transactionId = header.transactionId;
		if (header.databaseFormatVersion == 0) { // database not initialized
			if (readOnly) {
				throw new ODbException(ODbException.READ_ONLY_DATABASE);
			}
			int indexSize = initIndexSize;
			if (indexSize < dbFirstUserId) {
				indexSize = dbFirstUserId;
			}
			indexSize = (indexSize + dbHandlesPerPage - 1)
					& ~(dbHandlesPerPage - 1);

			bitmapExtentBase = dbBitmapPages;

			header.curr = currIndex = 0;
			long used = Page.pageSize;
			header.root[0].index = used;
			header.root[0].indexSize = indexSize;
			header.root[0].indexUsed = dbFirstUserId;
			header.root[0].freeList = 0;
			used += indexSize * 8L;
			header.root[1].index = used;
			header.root[1].indexSize = indexSize;
			header.root[1].indexUsed = dbFirstUserId;
			header.root[1].freeList = 0;
			used += indexSize * 8L;

			header.root[0].shadowIndex = header.root[1].index;
			header.root[1].shadowIndex = header.root[0].index;
			header.root[0].shadowIndexSize = indexSize;
			header.root[1].shadowIndexSize = indexSize;

			final int bitmapPages = (int) ((used + Page.pageSize
					* (dbAllocationQuantum * 8 - 1) - 1) / (Page.pageSize * (dbAllocationQuantum * 8 - 1)));
			final long bitmapSize = (long) bitmapPages * Page.pageSize;
			int usedBitmapSize = (int) ((used + bitmapSize) >>> (dbAllocationQuantumBits + 3));

			for (i = 0; i < bitmapPages; i++) {
				pg = pool.putPage(used + (long) i * Page.pageSize);
				final byte[] bitmap = pg.data;
				final int n = usedBitmapSize > Page.pageSize ? Page.pageSize
						: usedBitmapSize;
				for (int j = 0; j < n; j++) {
					bitmap[j] = (byte) 0xFF;
				}
				usedBitmapSize -= Page.pageSize;
				pool.unfix(pg);
			}
			final int bitmapIndexSize = ((dbBitmapId + dbBitmapPages) * 8
					+ Page.pageSize - 1)
					& ~(Page.pageSize - 1);
			final byte[] index = new byte[bitmapIndexSize];
			ByteUtils.pack8(index, dbInvalidId * 8, dbFreeHandleFlag);
			for (i = 0; i < bitmapPages; i++) {
				ByteUtils.pack8(index, (dbBitmapId + i) * 8, used
						| dbPageObjectFlag);
				used += Page.pageSize;
			}
			header.root[0].bitmapEnd = dbBitmapId + i;
			header.root[1].bitmapEnd = dbBitmapId + i;
			while (i < dbBitmapPages) {
				ByteUtils.pack8(index, (dbBitmapId + i) * 8, dbFreeHandleFlag);
				i += 1;
			}
			header.root[0].size = used;
			header.root[1].size = used;
			usedSize = used;
			committedIndexSize = currIndexSize = dbFirstUserId;

			pool.write(header.root[1].index, index);
			pool.write(header.root[0].index, index);

			modified = true;
			header.dirty = true;
			header.root[0].size = header.root[1].size;
			pg = pool.putPage(0);
			header.pack(pg.data);
			pool.flush();
			pool.modify(pg);
			header.databaseFormatVersion = dbDatabaseFormatVersion;
			header.pack(pg.data);
			pool.unfix(pg);
			pool.flush();
		} else {
			final int curr = header.curr;
			currIndex = curr;
			if (header.root[curr].indexSize != header.root[curr].shadowIndexSize) {
				throw new ODbException(corruptionError);
			}
			bitmapExtentBase = (header.databaseFormatVersion < 2) ? 0
					: dbBitmapPages;

			if (isDirty()) {
				if (listener != null) {
					listener.databaseCorrupted();
				}
				System.err
						.println("Database was not normally closed: start recovery");
				header.root[1 - curr].size = header.root[curr].size;
				header.root[1 - curr].indexUsed = header.root[curr].indexUsed;
				header.root[1 - curr].freeList = header.root[curr].freeList;
				header.root[1 - curr].index = header.root[curr].shadowIndex;
				header.root[1 - curr].indexSize = header.root[curr].shadowIndexSize;
				header.root[1 - curr].shadowIndex = header.root[curr].index;
				header.root[1 - curr].shadowIndexSize = header.root[curr].indexSize;
				header.root[1 - curr].bitmapEnd = header.root[curr].bitmapEnd;
				header.root[1 - curr].rootObject = header.root[curr].rootObject;
				header.root[1 - curr].classDescList = header.root[curr].classDescList;
				header.root[1 - curr].bitmapExtent = header.root[curr].bitmapExtent;

				modified = true;
				pg = pool.putPage(0);
				header.pack(pg.data);
				pool.unfix(pg);

				pool.copy(header.root[1 - curr].index, header.root[curr].index,
						(header.root[curr].indexUsed * 8L + Page.pageSize - 1)
								& ~(Page.pageSize - 1));
				if (listener != null) {
					listener.recoveryCompleted();
				}
				System.err.println("Recovery completed");
			}
			currIndexSize = header.root[1 - curr].indexUsed;
			committedIndexSize = currIndexSize;
			usedSize = header.root[curr].size;
		}
		final int bitmapSize = header.root[1 - currIndex].bitmapExtent == 0 ? dbBitmapPages
				: dbLargeBitmapPages;
		bitmapPageAvailableSpace = new int[bitmapSize];
		for (i = 0; i < bitmapPageAvailableSpace.length; i++) {
			bitmapPageAvailableSpace[i] = Integer.MAX_VALUE;
		}
		currRBitmapPage = currPBitmapPage = 0;
		currRBitmapOffs = currPBitmapOffs = 0;

		opened = true;
		reloadScheme();

		if (multiclientSupport) {
			// modified = true; ??? Why it is needed here?
			endThreadTransaction();
		} else {
			commit(); // commit scheme changes
		}
	}

	public boolean isOpened() {
		return opened;
	}

	static void checkIfFinal(final ClassDescriptor desc) {
		final Class cls = desc.cls;
		if (cls != null) {
			for (ClassDescriptor next = desc.next; next != null; next = next.next) {
				next.load();
				if (cls.isAssignableFrom(next.cls)) {
					desc.hasSubclasses = true;
				} else if (next.cls.isAssignableFrom(cls)) {
					next.hasSubclasses = true;
				}
			}
		}
	}

	void reloadScheme() {
		classDescMap.clear();
		customAllocatorMap = null;
		customAllocatorList = null;
		defaultAllocator = new DefaultAllocator(this);
		final int descListOid = header.root[1 - currIndex].classDescList;

		final ClassDescriptor metaclass = new ClassDescriptor(this,
				ClassDescriptor.class);
		final ClassDescriptor metafield = new ClassDescriptor(this,
				ClassDescriptor.FieldDescriptor.class);

		classDescMap.put(ClassDescriptor.class, metaclass);
		classDescMap.put(ClassDescriptor.FieldDescriptor.class, metafield);

		if ((compatibilityMode & IBM_JAVA5_COMPATIBILITY_MODE) != 0
				&& getDatabaseFormatVersion() == 1) {
			ClassDescriptor.FieldDescriptor tmp = metaclass.allFields[2];
			metaclass.allFields[2] = metaclass.allFields[3];
			metaclass.allFields[3] = metaclass.allFields[4];
			metaclass.allFields[4] = tmp;
			tmp = metafield.allFields[2];
			metafield.allFields[2] = metafield.allFields[3];
			metafield.allFields[3] = tmp;
		}

		if (descListOid != 0) {
			ClassDescriptor desc;
			descList = findClassDescriptor(descListOid);
			for (desc = descList; desc != null; desc = desc.next) {
				desc.load();
			}
			for (desc = descList; desc != null; desc = desc.next) {
				if (findClassDescriptor(desc.cls) == desc) {
					desc.resolve();
				}
				if (desc.allocator != null) {
					if (customAllocatorMap == null) {
						customAllocatorMap = new HashMap();
						customAllocatorList = new ArrayList();
					}
					final CustomAllocator allocator = desc.allocator;
					allocator.load();
					customAllocatorMap.put(desc.cls, allocator);
					customAllocatorList.add(allocator);
					reserveLocation(allocator.getSegmentBase(),
							allocator.getSegmentSize());
				}
				checkIfFinal(desc);
			}
		} else {
			descList = null;
		}
	}

	final void registerClassDescriptor(final ClassDescriptor desc) {
		classDescMap.put(desc.cls, desc);
		desc.next = descList;
		descList = desc;
		checkIfFinal(desc);
		storeObject0(desc, false);
		header.root[1 - currIndex].classDescList = desc.getObjectId();
		modified = true;
	}

	final ClassDescriptor findClassDescriptor(final Class cls) {
		return (ClassDescriptor) classDescMap.get(cls);
	}

	final ClassDescriptor getClassDescriptor(final Class cls) {
		ClassDescriptor desc = findClassDescriptor(cls);
		if (desc == null) {
			desc = new ClassDescriptor(this, cls);
			registerClassDescriptor(desc);
		}
		return desc;
	}

	public synchronized Object getRootObject() {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final int rootOid = header.root[1 - currIndex].rootObject;
		return (rootOid == 0) ? null : lookupObject(rootOid, null);
	}

	public synchronized void setRootObject(final Object root) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		if (root == null) {
			header.root[1 - currIndex].rootObject = 0;
		} else {
			if (!isPersistent(root)) {
				storeObject0(root, false);
			}
			header.root[1 - currIndex].rootObject = getOid(root);
		}
		modified = true;
	}

	public void commit() {
		if (useSerializableTransactions && getTransactionContext().nested != 0) {
			// Store should not be used in serializable transaction mode
			throw new ODbException(ODbException.INVALID_OPERATION, "commit");
		}
		synchronized (backgroundGcMonitor) {
			synchronized (this) {
				if (!opened) {
					throw new ODbException(ODbException.DATABASE_NOT_OPENED);
				}
				if (!modified) {
					return;
				}
				objectCache.flush();
				if (customAllocatorList != null) {
					final Iterator iterator = customAllocatorList.iterator();
					while (iterator.hasNext()) {
						final CustomAllocator alloc = (CustomAllocator) iterator
								.next();
						if (alloc.isModified()) {
							alloc.store();
						}
						alloc.commit();
					}
				}
				commit0();
				modified = false;
			}
		}
	}

	private final void commit0() {
		int i, j, n;
		int curr = currIndex;
		final int[] map = dirtyPagesMap;
		final int oldIndexSize = header.root[curr].indexSize;
		int newIndexSize = header.root[1 - curr].indexSize;
		final int nPages = committedIndexSize >>> dbHandlesPerPageBits;
		Page pg;

		if (newIndexSize > oldIndexSize) {
			cloneBitmap(header.root[curr].index, oldIndexSize * 8L);
			long newIndex;
			while (true) {
				newIndex = allocate(newIndexSize * 8L, 0);
				if (newIndexSize == header.root[1 - curr].indexSize) {
					break;
				}
				free(newIndex, newIndexSize * 8L);
				newIndexSize = header.root[1 - curr].indexSize;
			}
			header.root[1 - curr].shadowIndex = newIndex;
			header.root[1 - curr].shadowIndexSize = newIndexSize;
			free(header.root[curr].index, oldIndexSize * 8L);
		}
		final long currSize = header.root[1 - curr].size;
		for (i = 0; i < nPages; i++) {
			if ((map[i >> 5] & (1 << (i & 31))) != 0) {
				final Page srcIndex = pool.getPage(header.root[1 - curr].index
						+ (long) i * Page.pageSize);
				final Page dstIndex = pool.getPage(header.root[curr].index
						+ (long) i * Page.pageSize);
				for (j = 0; j < Page.pageSize; j += 8) {
					final long pos = ByteUtils.unpack8(dstIndex.data, j);
					if (ByteUtils.unpack8(srcIndex.data, j) != pos
							&& pos < currSize) {
						if ((pos & dbFreeHandleFlag) == 0) {
							if ((pos & dbPageObjectFlag) != 0) {
								free(pos & ~dbFlagsMask, Page.pageSize);
							} else if (pos != 0) {
								final int offs = (int) pos
										& (Page.pageSize - 1);
								pg = pool.getPage(pos - offs);
								free(pos, ObjectHeader.getSize(pg.data, offs));
								pool.unfix(pg);
							}
						}
					}
				}
				pool.unfix(srcIndex);
				pool.unfix(dstIndex);
			}
		}
		n = committedIndexSize & (dbHandlesPerPage - 1);
		if (n != 0 && (map[i >> 5] & (1 << (i & 31))) != 0) {
			final Page srcIndex = pool.getPage(header.root[1 - curr].index
					+ (long) i * Page.pageSize);
			final Page dstIndex = pool.getPage(header.root[curr].index
					+ (long) i * Page.pageSize);
			j = 0;
			do {
				final long pos = ByteUtils.unpack8(dstIndex.data, j);
				if (ByteUtils.unpack8(srcIndex.data, j) != pos
						&& pos < currSize) {
					if ((pos & dbFreeHandleFlag) == 0) {
						if ((pos & dbPageObjectFlag) != 0) {
							free(pos & ~dbFlagsMask, Page.pageSize);
						} else if (pos != 0) {
							final int offs = (int) pos & (Page.pageSize - 1);
							pg = pool.getPage(pos - offs);
							free(pos, ObjectHeader.getSize(pg.data, offs));
							pool.unfix(pg);
						}
					}
				}
				j += 8;
			} while (--n != 0);
			pool.unfix(srcIndex);
			pool.unfix(dstIndex);
		}
		for (i = 0; i <= nPages; i++) {
			if ((map[i >> 5] & (1 << (i & 31))) != 0) {
				pg = pool.putPage(header.root[1 - curr].index + (long) i
						* Page.pageSize);
				for (j = 0; j < Page.pageSize; j += 8) {
					ByteUtils.pack8(pg.data, j, ByteUtils.unpack8(pg.data, j)
							& ~dbModifiedFlag);
				}
				pool.unfix(pg);
			}
		}
		if (currIndexSize > committedIndexSize) {
			long page = (header.root[1 - curr].index + committedIndexSize * 8L)
					& ~(Page.pageSize - 1);
			final long end = (header.root[1 - curr].index + Page.pageSize - 1 + currIndexSize * 8L)
					& ~(Page.pageSize - 1);
			while (page < end) {
				pg = pool.putPage(page);
				for (j = 0; j < Page.pageSize; j += 8) {
					ByteUtils.pack8(pg.data, j, ByteUtils.unpack8(pg.data, j)
							& ~dbModifiedFlag);
				}
				pool.unfix(pg);
				page += Page.pageSize;
			}
		}
		header.root[1 - curr].usedSize = usedSize;
		pg = pool.putPage(0);
		header.pack(pg.data);
		pool.flush();
		pool.modify(pg);
		Conditions.assertArgument(header.transactionId == transactionId);
		header.transactionId = ++transactionId;
		header.curr = curr ^= 1;
		header.dirty = true;
		header.pack(pg.data);
		pool.unfix(pg);
		pool.flush();
		header.root[1 - curr].size = header.root[curr].size;
		header.root[1 - curr].indexUsed = currIndexSize;
		header.root[1 - curr].freeList = header.root[curr].freeList;
		header.root[1 - curr].bitmapEnd = header.root[curr].bitmapEnd;
		header.root[1 - curr].rootObject = header.root[curr].rootObject;
		header.root[1 - curr].classDescList = header.root[curr].classDescList;
		header.root[1 - curr].bitmapExtent = header.root[curr].bitmapExtent;
		if (currIndexSize == 0 || newIndexSize != oldIndexSize) {
			header.root[1 - curr].index = header.root[curr].shadowIndex;
			header.root[1 - curr].indexSize = header.root[curr].shadowIndexSize;
			header.root[1 - curr].shadowIndex = header.root[curr].index;
			header.root[1 - curr].shadowIndexSize = header.root[curr].indexSize;
			pool.copy(header.root[1 - curr].index, header.root[curr].index,
					currIndexSize * 8L);
			i = (currIndexSize + dbHandlesPerPage * 32 - 1) >>> (dbHandlesPerPageBits + 5);
			while (--i >= 0) {
				map[i] = 0;
			}
		} else {
			for (i = 0; i < nPages; i++) {
				if ((map[i >> 5] & (1 << (i & 31))) != 0) {
					map[i >> 5] -= (1 << (i & 31));
					pool.copy(header.root[1 - curr].index + (long) i
							* Page.pageSize, header.root[curr].index + (long) i
							* Page.pageSize, Page.pageSize);
				}
			}
			if (currIndexSize > i * dbHandlesPerPage
					&& ((map[i >> 5] & (1 << (i & 31))) != 0 || currIndexSize != committedIndexSize)) {
				pool.copy(header.root[1 - curr].index + (long) i
						* Page.pageSize, header.root[curr].index + (long) i
						* Page.pageSize, 8L * currIndexSize - (long) i
						* Page.pageSize);
				j = i >>> 5;
				n = (currIndexSize + dbHandlesPerPage * 32 - 1) >>> (dbHandlesPerPageBits + 5);
				while (j < n) {
					map[j++] = 0;
				}
			}
		}
		gcDone = false;
		currIndex = curr;
		committedIndexSize = currIndexSize;
		if (multiclientSupport) {
			pool.flush();
			pg = pool.putPage(0);
			header.dirty = false;
			header.pack(pg.data);
			pool.unfix(pg);
			pool.flush();
		}
		if (listener != null) {
			listener.onTransactionCommit();
		}
	}

	public synchronized void rollback() {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		if (useSerializableTransactions && getTransactionContext().nested != 0) {
			// Store should not be used in serializable transaction mode
			throw new ODbException(ODbException.INVALID_OPERATION, "rollback");
		}
		objectCache.invalidate();
		synchronized (objectCache) {
			if (!modified) {
				return;
			}
			rollback0();
			modified = false;
			if (reloadObjectsOnRollback) {
				objectCache.reload();
			} else {
				objectCache.clear();
			}
		}
	}

	private final void rollback0() {
		final int curr = currIndex;
		final int[] map = dirtyPagesMap;
		if (header.root[1 - curr].index != header.root[curr].shadowIndex) {
			pool.copy(header.root[curr].shadowIndex, header.root[curr].index,
					8L * committedIndexSize);
		} else {
			final int nPages = (committedIndexSize + dbHandlesPerPage - 1) >>> dbHandlesPerPageBits;
			for (int i = 0; i < nPages; i++) {
				if ((map[i >> 5] & (1 << (i & 31))) != 0) {
					pool.copy(header.root[curr].shadowIndex + (long) i
							* Page.pageSize, header.root[curr].index + (long) i
							* Page.pageSize, Page.pageSize);
				}
			}
		}
		for (int j = (currIndexSize + dbHandlesPerPage * 32 - 1) >>> (dbHandlesPerPageBits + 5); --j >= 0; map[j] = 0)
			;
		header.root[1 - curr].index = header.root[curr].shadowIndex;
		header.root[1 - curr].indexSize = header.root[curr].shadowIndexSize;
		header.root[1 - curr].indexUsed = committedIndexSize;
		header.root[1 - curr].freeList = header.root[curr].freeList;
		header.root[1 - curr].bitmapEnd = header.root[curr].bitmapEnd;
		header.root[1 - curr].size = header.root[curr].size;
		header.root[1 - curr].rootObject = header.root[curr].rootObject;
		header.root[1 - curr].classDescList = header.root[curr].classDescList;
		header.root[1 - curr].bitmapExtent = header.root[curr].bitmapExtent;
		usedSize = header.root[curr].size;
		currIndexSize = committedIndexSize;
		currRBitmapPage = currPBitmapPage = 0;
		currRBitmapOffs = currPBitmapOffs = 0;
		reloadScheme();
		if (listener != null) {
			listener.onTransactionRollback();
		}
	}

	public <T> Query<T> createQuery() {
		return new QueryImpl<T>(this);
	}

	public synchronized <T> ODbSet<T> createScalableSet() {
		return createScalableSet(8);
	}

	public synchronized <T> ODbSet<T> createScalableSet(final int initialSize) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new ScalableSet(this, initialSize);
	}

	public <T> ODbList<T> createList() {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new ODbListImpl<T>(this);
	}

	public <T> ODbList<T> createScalableList() {
		return createScalableList(8);
	}

	public <T> ODbList<T> createScalableList(final int initialSize) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new ScalableList<T>(this, initialSize);
	}

	public <K, V> ODbHash<K, V> createHash() {
		return createHash(101, 2);
	}

	public <K, V> ODbHash<K, V> createHash(final int pageSize,
			final int loadFactor) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new ODbHashImpl<K, V>(this, pageSize, loadFactor);
	}

	public <K extends Comparable, V> ODbMap<K, V> createMap(final Class keyType) {
		return createMap(keyType, 4);
	}

	public <K extends Comparable, V> ODbMap<K, V> createMap(
			final Class keyType, final int initialSize) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new ODbMapImpl<K, V>(this, keyType, initialSize);
	}

	public synchronized <T> ODbSet<T> createSet() {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final ODbSet<T> set = alternativeBtree ? (ODbSet<T>) new AltPersistentSet<T>(
				true) : (ODbSet<T>) new ODbSetImpl<T>(true);
		set.assignObjectId(this, 0, false);
		return set;
	}

	public synchronized <T> ODbSet<T> createBag() {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final ODbSet<T> set = alternativeBtree ? (ODbSet<T>) new AltPersistentSet<T>(
				false) : (ODbSet<T>) new ODbSetImpl<T>(false);
		set.assignObjectId(this, 0, false);
		return set;
	}

	public synchronized <T> ODbBitIndex<T> createBitIndex() {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final ODbBitIndex index = new BitIndexImpl<T>();
		index.assignObjectId(this, 0, false);
		return index;
	}

	public synchronized <T> Index<T> createIndex(final Class keyType,
			final boolean unique) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final Index<T> index = alternativeBtree ? (Index<T>) new AltBtree<T>(
				keyType, unique) : (Index<T>) new Btree<T>(keyType, unique);
		index.assignObjectId(this, 0, false);
		return index;
	}

	public synchronized <T> Index<T> createIndex(final Class[] keyTypes,
			final boolean unique) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final Index<T> index = alternativeBtree ? (Index<T>) new AltBtreeCompoundIndex<T>(
				keyTypes, unique) : (Index<T>) new BtreeCompoundIndex<T>(
				keyTypes, unique);
		index.assignObjectId(this, 0, false);
		return index;
	}

	public synchronized <T> MultidimensionalIndex<T> createMultidimensionalIndex(
			final MultidimensionalComparator<T> comparator) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new KDTree<T>(this, comparator);
	}

	public synchronized <T> MultidimensionalIndex<T> createMultidimensionalIndex(
			final Class type, final String[] fieldNames,
			final boolean treateZeroAsUndefinedValue) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new KDTree<T>(this, type, fieldNames, treateZeroAsUndefinedValue);
	}

	public synchronized <T> Index<T> createThickIndex(final Class keyType) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new ThickIndex<T>(this, keyType);
	}

	public synchronized <T> SpatialIndex<T> createSpatialIndex() {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new Rtree<T>();
	}

	public synchronized <T> SpatialIndexR2<T> createSpatialIndexR2() {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new RtreeR2<T>(this);
	}

	public synchronized <T> SpatialIndexRn<T> createSpatialIndexRn() {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new RtreeRn<T>(this);
	}

	public <T> FieldIndex<T> createFieldIndex(final Class type,
			final String fieldName, final boolean unique) {
		return this.<T> createFieldIndex(type, fieldName, unique, false);
	}

	public <T> FieldIndex<T> createFieldIndex(final Class type,
			final String fieldName, final boolean unique,
			final boolean caseInsensitive) {
		return this.<T> createFieldIndex(type, fieldName, unique,
				caseInsensitive, false);
	}

	public <T> RegexIndex<T> createRegexIndex(final Class type,
			final String fieldName) {
		return createRegexIndex(type, fieldName, true, 3);
	}

	public synchronized <T> RegexIndex<T> createRegexIndex(final Class type,
			final String fieldName, final boolean caseInsensitive,
			final int nGrams) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new RegexIndexImpl(this, type, fieldName, caseInsensitive,
				nGrams);
	}

	public synchronized <T> FieldIndex<T> createFieldIndex(final Class type,
			final String fieldName, final boolean unique,
			final boolean caseInsensitive, final boolean thick) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final FieldIndex<T> index = thick ? caseInsensitive ? (FieldIndex<T>) new ThickCaseInsensitiveFieldIndex(
				this, type, fieldName) : (FieldIndex<T>) new ThickFieldIndex(
				this, type, fieldName)
				: caseInsensitive ? alternativeBtree ? (FieldIndex<T>) new AltBtreeCaseInsensitiveFieldIndex<T>(
						type, fieldName, unique)
						: (FieldIndex<T>) new BtreeCaseInsensitiveFieldIndex<T>(
								type, fieldName, unique)
						: alternativeBtree ? (FieldIndex<T>) new AltBtreeFieldIndex<T>(
								type, fieldName, unique)
								: (FieldIndex<T>) new BtreeFieldIndex<T>(type,
										fieldName, unique);
		index.assignObjectId(this, 0, false);
		return index;
	}

	public <T> FieldIndex<T> createFieldIndex(final Class type,
			final String[] fieldNames, final boolean unique) {
		return this.<T> createFieldIndex(type, fieldNames, unique, false);
	}

	public synchronized <T> FieldIndex<T> createFieldIndex(final Class type,
			final String[] fieldNames, final boolean unique,
			final boolean caseInsensitive) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final FieldIndex<T> index = caseInsensitive ? alternativeBtree ? (FieldIndex<T>) new AltBtreeCaseInsensitiveMultiFieldIndex(
				type, fieldNames, unique)
				: (FieldIndex<T>) new BtreeCaseInsensitiveMultiFieldIndex(type,
						fieldNames, unique)
				: alternativeBtree ? (FieldIndex<T>) new AltBtreeMultiFieldIndex(
						type, fieldNames, unique)
						: (FieldIndex<T>) new BtreeMultiFieldIndex(type,
								fieldNames, unique);
		index.assignObjectId(this, 0, false);
		return index;
	}

	public synchronized <T> Index<T> createRandomAccessIndex(
			final Class keyType, final boolean unique) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final Index<T> index = new RndBtree<T>(keyType, unique);
		index.assignObjectId(this, 0, false);
		return index;
	}

	public synchronized <T> Index<T> createRandomAccessIndex(
			final Class[] keyTypes, final boolean unique) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final Index<T> index = new RndBtreeCompoundIndex<T>(keyTypes, unique);
		index.assignObjectId(this, 0, false);
		return index;
	}

	public <T> FieldIndex<T> createRandomAccessFieldIndex(final Class type,
			final String fieldName, final boolean unique) {
		return this.<T> createRandomAccessFieldIndex(type, fieldName, unique,
				false);
	}

	public synchronized <T> FieldIndex<T> createRandomAccessFieldIndex(
			final Class type, final String fieldName, final boolean unique,
			final boolean caseInsensitive) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final FieldIndex<T> index = caseInsensitive ? (FieldIndex) new RndBtreeCaseInsensitiveFieldIndex<T>(
				type, fieldName, unique)
				: (FieldIndex) new RndBtreeFieldIndex<T>(type, fieldName,
						unique);
		index.assignObjectId(this, 0, false);
		return index;
	}

	public <T> FieldIndex<T> createRandomAccessFieldIndex(final Class type,
			final String[] fieldNames, final boolean unique) {
		return this.<T> createRandomAccessFieldIndex(type, fieldNames, unique,
				false);
	}

	public synchronized <T> FieldIndex<T> createRandomAccessFieldIndex(
			final Class type, final String[] fieldNames, final boolean unique,
			final boolean caseInsensitive) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		final FieldIndex<T> index = caseInsensitive ? (FieldIndex) new RndBtreeCaseInsensitiveMultiFieldIndex(
				type, fieldNames, unique)
				: (FieldIndex) new RndBtreeMultiFieldIndex(type, fieldNames,
						unique);
		index.assignObjectId(this, 0, false);
		return index;
	}

	public <T> OdbSortedCollection<T> createSortedCollection(
			final ODbComparator<T> comparator, final boolean unique) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new Ttree<T>(this, comparator, unique);
	}

	public <T extends Comparable> OdbSortedCollection<T> createSortedCollection(
			final boolean unique) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		return new Ttree<T>(this, new DefaultPersistentComparator<T>(), unique);
	}

	public <T> Link<T> createLink() {
		return createLink(8);
	}

	public <T> Link<T> createLink(final int initialSize) {
		return new LinkImpl<T>(this, initialSize);
	}

	public <M, O> Relation<M, O> createRelation(final O owner) {
		return new RelationImpl<M, O>(this, owner);
	}

	public ODbBlob createBlob() {
		return new BlobImpl(this, Page.pageSize - BlobImpl.headerSize);
	}

	public ODbBlob createRandomAccessBlob() {
		return new RandomAccessBlobImpl(this);
	}

	public <T extends TimeSeries.Tick> TimeSeries<T> createTimeSeries(
			final Class blockClass, final long maxBlockTimeInterval) {
		return new TimeSeriesImpl<T>(this, blockClass, maxBlockTimeInterval);
	}

	public <T> ODbPatriciaTrie<T> createPatriciaTrie() {
		return new PTrie<T>();
	}

	public FullTextIndex createFullTextIndex(final FullTextSearchHelper helper) {
		return new FullTextIndexImpl(this, helper);
	}

	public FullTextIndex createFullTextIndex() {
		return createFullTextIndex(new FullTextSearchHelper(this));
	}

	final long getGCPos(final int oid) {
		final Page pg = pool.getPage(header.root[currIndex].index
				+ ((long) (oid >>> dbHandlesPerPageBits) << Page.pageSizeLog));
		final long pos = ByteUtils.unpack8(pg.data,
				(oid & (dbHandlesPerPage - 1)) << 3);
		pool.unfix(pg);
		return pos;
	}

	final void markOid(final int oid) {
		if (oid != 0) {
			final long pos = getGCPos(oid);
			if ((pos & (dbFreeHandleFlag | dbPageObjectFlag)) != 0) {
				throw new ODbException(ODbException.INVALID_OID);
			}
			if (pos < header.root[currIndex].size) {
				// object was not allocated by custom allocator
				final int bit = (int) (pos >>> dbAllocationQuantumBits);
				if ((blackBitmap[bit >>> 5] & (1 << (bit & 31))) == 0) {
					greyBitmap[bit >>> 5] |= 1 << (bit & 31);
				}
			}
		}
	}

	final Page getGCPage(final int oid) {
		return pool.getPage(getGCPos(oid) & ~dbFlagsMask);
	}

	public void setGcThreshold(final long maxAllocatedDelta) {
		gcThreshold = maxAllocatedDelta;
	}

	private void mark() {
		final int bitmapSize = (int) (header.root[currIndex].size >>> (dbAllocationQuantumBits + 5)) + 1;
		boolean existsNotMarkedObjects;
		long pos;
		int i, j;

		if (listener != null) {
			listener.gcStarted();
		}

		greyBitmap = new int[bitmapSize];
		blackBitmap = new int[bitmapSize];
		final int rootOid = header.root[currIndex].rootObject;
		if (rootOid != 0) {
			markOid(rootOid);
			do {
				existsNotMarkedObjects = false;
				for (i = 0; i < bitmapSize; i++) {
					if (greyBitmap[i] != 0) {
						existsNotMarkedObjects = true;
						for (j = 0; j < 32; j++) {
							if ((greyBitmap[i] & (1 << j)) != 0) {
								pos = (((long) i << 5) + j) << dbAllocationQuantumBits;
								greyBitmap[i] &= ~(1 << j);
								blackBitmap[i] |= 1 << j;
								final int offs = (int) pos
										& (Page.pageSize - 1);
								final Page pg = pool.getPage(pos - offs);
								final int typeOid = ObjectHeader.getType(
										pg.data, offs);
								if (typeOid != 0) {
									final ClassDescriptor desc = findClassDescriptor(typeOid);
									if (Btree.class.isAssignableFrom(desc.cls)) {
										final Btree btree = new Btree(pg.data,
												ObjectHeader.sizeof + offs);
										btree.assignObjectId(this, 0, false);
										btree.markTree();
									} else if (desc.hasReferences) {
										markObject(pool.get(pos),
												ObjectHeader.sizeof, desc);
									}
								}
								pool.unfix(pg);
							}
						}
					}
				}
			} while (existsNotMarkedObjects);
		}
	}

	private int sweep() {
		int nDeallocated = 0;
		long pos;
		gcDone = true;
		for (int i = dbFirstUserId, j = committedIndexSize; i < j; i++) {
			pos = getGCPos(i);
			if (pos != 0
					&& ((int) pos & (dbPageObjectFlag | dbFreeHandleFlag)) == 0) {
				final int bit = (int) (pos >>> dbAllocationQuantumBits);
				if ((blackBitmap[bit >>> 5] & (1 << (bit & 31))) == 0) {
					// object is not accessible
					if (getPos(i) == pos) {
						final int offs = (int) pos & (Page.pageSize - 1);
						final Page pg = pool.getPage(pos - offs);
						final int typeOid = ObjectHeader.getType(pg.data, offs);
						if (typeOid != 0) {
							final ClassDescriptor desc = findClassDescriptor(typeOid);
							nDeallocated += 1;
							if (Btree.class.isAssignableFrom(desc.cls)) {
								final Btree btree = new Btree(pg.data,
										ObjectHeader.sizeof + offs);
								pool.unfix(pg);
								btree.assignObjectId(this, i, false);
								btree.deallocate();
							} else {
								final int size = ObjectHeader.getSize(pg.data,
										offs);
								pool.unfix(pg);
								freeId(i);
								objectCache.remove(i);
								cloneBitmap(pos, size);
							}
							if (listener != null) {
								listener.deallocateObject(desc.cls, i);
							}
						}
					}
				}
			}
		}

		greyBitmap = null;
		blackBitmap = null;
		allocatedDelta = 0;
		gcActive = false;

		if (listener != null) {
			listener.gcCompleted(nDeallocated);
		}
		return nDeallocated;
	}

	class GcThread extends Thread {
		private boolean go;

		GcThread() {
			start();
		}

		void activate() {
			synchronized (backgroundGcStartMonitor) {
				go = true;
				backgroundGcStartMonitor.notify();
			}
		}

		public void run() {
			try {
				while (true) {
					synchronized (backgroundGcStartMonitor) {
						while (!go && opened) {
							backgroundGcStartMonitor.wait();
						}
						if (!opened) {
							return;
						}
						go = false;
					}
					synchronized (backgroundGcMonitor) {
						if (!opened) {
							return;
						}
						mark();
						synchronized (ODbImpl.this) {
							synchronized (objectCache) {
								sweep();
							}
						}
					}
				}
			} catch (final InterruptedException x) {
			}
		}
	}

	public synchronized int gc() {
		return gc0();
	}

	private int gc0() {
		synchronized (objectCache) {
			if (!opened) {
				throw new ODbException(ODbException.DATABASE_NOT_OPENED);
			}
			if (gcDone || gcActive) {
				return 0;
			}
			gcActive = true;
			if (backgroundGc) {
				if (gcThread == null) {
					gcThread = new GcThread();
				}
				gcThread.activate();
				return 0;
			}
			// System.out.println("Start GC, allocatedDelta=" + allocatedDelta +
			// ", header[" + currIndex + "].size=" + header.root[currIndex].size
			// + ", gcTreshold=" + gcThreshold);

			mark();
			return sweep();
		}
	}

	final long getBitmapUsedSpace(int from, final int till) {
		long allocated = 0;
		while (from < till) {
			final Page pg = getGCPage(from);
			for (int j = 0; j < Page.pageSize; j++) {
				int mask = pg.data[j] & 0xFF;
				while (mask != 0) {
					if ((mask & 1) != 0) {
						allocated += dbAllocationQuantum;
					}
					mask >>= 1;
				}
			}
			pool.unfix(pg);
			from += 1;
		}
		return allocated;
	}

	final int markObjectReference(final byte[] obj, int offs) {
		final int oid = ByteUtils.unpack4(obj, offs);
		offs += 4;
		if (oid < 0) {
			final int tid = -1 - oid;
			int len;
			switch (tid) {
			case ClassDescriptor.tpString:
			case ClassDescriptor.tpClass:
				offs = ByteUtils.skipString(obj, offs);
				break;
			case ClassDescriptor.tpArrayOfByte:
				len = ByteUtils.unpack4(obj, offs);
				offs += len + 4;
				break;
			case ClassDescriptor.tpArrayOfObject:
				len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				for (int i = 0; i < len; i++) {
					offs = markObjectReference(obj, offs);
				}
				break;
			case ClassDescriptor.tpArrayOfRaw:
				len = ByteUtils.unpack4(obj, offs);
				offs += 8;
				for (int i = 0; i < len; i++) {
					offs = markObjectReference(obj, offs);
				}
				break;
			case ClassDescriptor.tpCustom:
				try {
					final ByteArrayObjectInputStream in = new ByteArrayObjectInputStream(
							obj, offs, null, false, true);
					serializer.unpack(in);
					offs = in.getPosition();
					break;
				} catch (final IOException x) {
					throw new ODbException(ODbException.ACCESS_VIOLATION, x);
				}
			default:
				if (tid >= ClassDescriptor.tpValueTypeBias) {
					final int typeOid = -ClassDescriptor.tpValueTypeBias - oid;
					final ClassDescriptor desc = findClassDescriptor(typeOid);
					if (desc.isCollection) {
						len = ByteUtils.unpack4(obj, offs);
						offs += 4;
						for (int i = 0; i < len; i++) {
							offs = markObjectReference(obj, offs);
						}
					} else if (desc.isMap) {
						len = ByteUtils.unpack4(obj, offs);
						offs += 4;
						for (int i = 0; i < len; i++) {
							offs = markObjectReference(obj, offs);
							offs = markObjectReference(obj, offs);
						}
					} else {
						offs = markObject(obj, offs, desc);
					}
				} else {
					offs += ClassDescriptor.sizeof[tid];
				}
			}
		} else {
			markOid(oid);
		}
		return offs;
	}

	final int markObject(final byte[] obj, int offs, final ClassDescriptor desc) {
		final ClassDescriptor.FieldDescriptor[] all = desc.allFields;

		for (int i = 0, n = all.length; i < n; i++) {
			final ClassDescriptor.FieldDescriptor fd = all[i];
			switch (fd.type) {
			case ClassDescriptor.tpBoolean:
			case ClassDescriptor.tpByte:
				offs += 1;
				continue;
			case ClassDescriptor.tpChar:
			case ClassDescriptor.tpShort:
				offs += 2;
				continue;
			case ClassDescriptor.tpInt:
			case ClassDescriptor.tpEnum:
			case ClassDescriptor.tpFloat:
				offs += 4;
				continue;
			case ClassDescriptor.tpLong:
			case ClassDescriptor.tpDouble:
			case ClassDescriptor.tpDate:
				offs += 8;
				continue;
			case ClassDescriptor.tpString:
			case ClassDescriptor.tpClass:
				offs = ByteUtils.skipString(obj, offs);
				continue;
			case ClassDescriptor.tpObject:
				offs = markObjectReference(obj, offs);
				continue;
			case ClassDescriptor.tpValue:
				offs = markObject(obj, offs, fd.valueDesc);
				continue;
			case ClassDescriptor.tpRaw: {
				final int len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				if (len > 0) {
					offs += len;
				} else if (len == -2 - ClassDescriptor.tpObject) {
					markOid(ByteUtils.unpack4(obj, offs));
					offs += 4;
				} else if (len < -1) {
					offs += ClassDescriptor.sizeof[-2 - len];
				}
				continue;
			}
			case ClassDescriptor.tpCustom:
				try {
					final ByteArrayObjectInputStream in = new ByteArrayObjectInputStream(
							obj, offs, null, false, true);
					serializer.unpack(in);
					offs = in.getPosition();
				} catch (final IOException x) {
					throw new ODbException(ODbException.ACCESS_VIOLATION, x);
				}
				continue;
			case ClassDescriptor.tpArrayOfByte:
			case ClassDescriptor.tpArrayOfBoolean: {
				final int len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				if (len > 0) {
					offs += len;
				} else if (len < -1) {
					offs += ClassDescriptor.sizeof[-2 - len];
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfShort:
			case ClassDescriptor.tpArrayOfChar: {
				final int len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				if (len > 0) {
					offs += len * 2;
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfInt:
			case ClassDescriptor.tpArrayOfEnum:
			case ClassDescriptor.tpArrayOfFloat: {
				final int len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				if (len > 0) {
					offs += len * 4;
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfLong:
			case ClassDescriptor.tpArrayOfDouble:
			case ClassDescriptor.tpArrayOfDate: {
				final int len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				if (len > 0) {
					offs += len * 8;
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfString: {
				int len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				while (--len >= 0) {
					offs = ByteUtils.skipString(obj, offs);
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfObject: {
				int len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				while (--len >= 0) {
					offs = markObjectReference(obj, offs);
				}
				continue;
			}
			case ClassDescriptor.tpLink: {
				int len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				while (--len >= 0) {
					markOid(ByteUtils.unpack4(obj, offs));
					offs += 4;
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfValue: {
				int len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				final ClassDescriptor valueDesc = fd.valueDesc;
				while (--len >= 0) {
					offs = markObject(obj, offs, valueDesc);
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfRaw: {
				int len = ByteUtils.unpack4(obj, offs);
				offs += 8;
				while (--len >= 0) {
					offs = markObjectReference(obj, offs);
				}
				continue;
			}
			}
		}
		return offs;
	}

	/**
	 * This method is used internally by ODb to get transaction context
	 * associated with current thread. But it can be also used by application to
	 * get transaction context, store it in some variable and use in another
	 * thread. I will make it possible to share one transaction between multiple
	 * threads.
	 * 
	 * @return transaction context associated with current thread
	 */
	public ThreadTransactionContext getTransactionContext() {
		return (ThreadTransactionContext) transactionContext.get();
	}

	/**
	 * Associate transaction context with the thread This method can be used by
	 * application to share the same transaction between multiple threads
	 * 
	 * @param ctx
	 *            new transaction context
	 * @return transaction context previously associated with this thread
	 */
	public ThreadTransactionContext setTransactionContext(
			final ThreadTransactionContext ctx) {
		final ThreadTransactionContext oldCtx = (ThreadTransactionContext) transactionContext
				.get();
		transactionContext.set(ctx);
		return oldCtx;
	}

	public void beginSerializableTransaction() {
		if (multiclientSupport) {
			beginThreadTransaction(READ_WRITE_TRANSACTION);
		} else {
			beginThreadTransaction(SERIALIZABLE_TRANSACTION);
		}
	}

	public void commitSerializableTransaction() {
		if (!isInsideThreadTransaction()) {
			throw new ODbException(ODbException.NOT_IN_TRANSACTION);
		}
		endThreadTransaction(Integer.MAX_VALUE);
	}

	public void rollbackSerializableTransaction() {
		if (!isInsideThreadTransaction()) {
			throw new ODbException(ODbException.NOT_IN_TRANSACTION);
		}
		rollbackThreadTransaction();
	}

	public void beginThreadTransaction(final int mode) {
		switch (mode) {
		case SERIALIZABLE_TRANSACTION:
			if (multiclientSupport) {
				throw new IllegalArgumentException("Illegal transaction mode");
			}
			useSerializableTransactions = true;
			getTransactionContext().nested += 1;
			;
			break;
		case EXCLUSIVE_TRANSACTION:
		case COOPERATIVE_TRANSACTION:
			if (multiclientSupport) {
				if (mode == EXCLUSIVE_TRANSACTION) {
					transactionLock.exclusiveLock();
				} else {
					transactionLock.sharedLock();
				}
				synchronized (transactionMonitor) {
					if (nNestedTransactions++ == 0) {
						file.lock(mode == READ_ONLY_TRANSACTION);
						final byte[] buf = new byte[Header.sizeof];
						final int rc = file.read(0, buf);
						if (rc > 0 && rc < Header.sizeof) {
							throw new ODbException(
									ODbException.DATABASE_CORRUPTED);
						}
						header.unpack(buf);
						final int curr = header.curr;
						currIndex = curr;
						currIndexSize = header.root[1 - curr].indexUsed;
						committedIndexSize = currIndexSize;
						usedSize = header.root[curr].size;

						if (header.transactionId != transactionId) {
							if (bitmapPageAvailableSpace != null) {
								for (int i = 0; i < bitmapPageAvailableSpace.length; i++) {
									bitmapPageAvailableSpace[i] = Integer.MAX_VALUE;
								}
							}
							objectCache.clear();
							pool.clear();
							transactionId = header.transactionId;
						}
					}
				}
			} else {
				synchronized (transactionMonitor) {
					if (scheduledCommitTime != Long.MAX_VALUE) {
						nBlockedTransactions += 1;
						while (System.currentTimeMillis() >= scheduledCommitTime) {
							try {
								transactionMonitor.wait();
							} catch (final InterruptedException x) {
							}
						}
						nBlockedTransactions -= 1;
					}
					nNestedTransactions += 1;
				}
				if (mode == EXCLUSIVE_TRANSACTION) {
					transactionLock.exclusiveLock();
				} else {
					transactionLock.sharedLock();
				}
			}
			break;
		default:
			throw new IllegalArgumentException("Illegal transaction mode");
		}
	}

	public void endThreadTransaction() {
		endThreadTransaction(Integer.MAX_VALUE);
	}

	public void endThreadTransaction(final int maxDelay) {
		if (multiclientSupport) {
			if (maxDelay != Integer.MAX_VALUE) {
				throw new IllegalArgumentException(
						"Delay is not supported for global transactions");
			}
			synchronized (transactionMonitor) {
				transactionLock.unlock();
				if (nNestedTransactions != 0) { // may be everything is already
												// aborted
					if (nNestedTransactions == 1) {
						commit();
						pool.flush();
						file.unlock();
					}
					nNestedTransactions -= 1;
				}
			}
			return;
		}
		final ThreadTransactionContext ctx = getTransactionContext();
		if (ctx.nested != 0) { // serializable transaction
			if (--ctx.nested == 0) {
				final ArrayList modified = ctx.modified;
				final ArrayList deleted = ctx.deleted;
				final Map locked = ctx.locked;
				synchronized (backgroundGcMonitor) {
					synchronized (this) {
						synchronized (objectCache) {
							for (int i = modified.size(); --i >= 0;) {
								store(modified.get(i));
							}
							for (int i = deleted.size(); --i >= 0;) {
								deallocateObject0(deleted.get(i));
							}
							if (modified.size() + deleted.size() > 0) {
								commit0();
							}
						}
					}
				}
				final Iterator iterator = locked.values().iterator();
				while (iterator.hasNext()) {
					((Resource) iterator.next()).reset();
				}
				modified.clear();
				deleted.clear();
				locked.clear();
			}
		} else { // exclusive or cooperative transaction
			synchronized (transactionMonitor) {
				transactionLock.unlock();

				if (nNestedTransactions != 0) { // may be everything is already
												// aborted
					if (--nNestedTransactions == 0) {
						nCommittedTransactions += 1;
						commit();
						scheduledCommitTime = Long.MAX_VALUE;
						if (nBlockedTransactions != 0) {
							transactionMonitor.notifyAll();
						}
					} else {
						if (maxDelay != Integer.MAX_VALUE) {
							final long nextCommit = System.currentTimeMillis()
									+ maxDelay;
							if (nextCommit < scheduledCommitTime) {
								scheduledCommitTime = nextCommit;
							}
							if (maxDelay == 0) {
								final int n = nCommittedTransactions;
								nBlockedTransactions += 1;
								do {
									try {
										transactionMonitor.wait();
									} catch (final InterruptedException x) {
									}
								} while (nCommittedTransactions == n);
								nBlockedTransactions -= 1;
							}
						}
					}
				}
			}
		}
	}

	public boolean isInsideThreadTransaction() {
		return getTransactionContext().nested != 0 || nNestedTransactions != 0;
	}

	public void rollbackThreadTransaction() {
		if (multiclientSupport) {
			synchronized (transactionMonitor) {
				transactionLock.reset();
				rollback();
				file.unlock();
				nNestedTransactions = 0;
			}
			return;
		}
		final ThreadTransactionContext ctx = getTransactionContext();
		if (ctx.nested != 0) { // serializable transaction
			final ArrayList modified = ctx.modified;
			final Map locked = ctx.locked;
			synchronized (this) {
				synchronized (objectCache) {
					int i = modified.size();
					while (--i >= 0) {
						final Object obj = modified.get(i);
						final int oid = getOid(obj);
						Conditions.assertArgument(oid != 0);
						invalidate(obj);
						if (getPos(oid) == 0) {
							freeId(oid);
							objectCache.remove(oid);
						} else {
							loadStub(oid, obj, obj.getClass());
							objectCache.clearDirty(obj);
						}
					}
				}
			}
			final Iterator iterator = locked.values().iterator();
			while (iterator.hasNext()) {
				((Resource) iterator.next()).reset();
			}
			ctx.nested = 0;
			modified.clear();
			ctx.deleted.clear();
			locked.clear();
			if (listener != null) {
				listener.onTransactionRollback();
			}
		} else {
			synchronized (transactionMonitor) {
				transactionLock.reset();
				nNestedTransactions = 0;
				if (nBlockedTransactions != 0) {
					transactionMonitor.notifyAll();
				}
				rollback();
			}
		}
	}

	public/* protected */boolean lockObject(final Object obj) {
		if (useSerializableTransactions) {
			final ThreadTransactionContext ctx = getTransactionContext();
			if (ctx.nested != 0) { // serializable transaction
				return ctx.locked.put(obj, obj) == null;
			}
		}
		return true;
	}

	public void close() {
		synchronized (backgroundGcMonitor) {
			commit();
			opened = false;
		}
		if (gcThread != null) {
			gcThread.activate();
			try {
				gcThread.join();
			} catch (final InterruptedException x) {
			}
		}
		if (isDirty()) {
			final Page pg = pool.putPage(0);
			header.pack(pg.data);
			pool.flush();
			pool.modify(pg);
			header.dirty = false;
			header.pack(pg.data);
			pool.unfix(pg);
			pool.flush();
		}
		pool.close();
		// make GC easier
		pool = null;
		objectCache = null;
		classDescMap = null;
		bitmapPageAvailableSpace = null;
		dirtyPagesMap = null;
		descList = null;
	}

	private boolean getBooleanValue(final Object value) {
		if (value instanceof Boolean) {
			return ((Boolean) value).booleanValue();
		} else if (value instanceof String) {
			final String s = (String) value;
			if ("true".equalsIgnoreCase(s) || "t".equalsIgnoreCase(s)
					|| "1".equals(s)) {
				return true;
			} else if ("false".equalsIgnoreCase(s) || "f".equalsIgnoreCase(s)
					|| "0".equals(s)) {
				return false;
			}
		}
		throw new ODbException(ODbException.BAD_PROPERTY_VALUE);
	}

	private long getIntegerValue(final Object value) {
		if (value instanceof Number) {
			return ((Number) value).longValue();
		} else if (value instanceof String) {
			try {
				return Long.parseLong((String) value, 10);
			} catch (final NumberFormatException x) {
			}
		}
		throw new ODbException(ODbException.BAD_PROPERTY_VALUE);
	}

	public void setProperties(final Properties props) {
		String value;
		properties.putAll(props);
		if ((value = props.getProperty("implicit.values")) != null) {
			ClassDescriptor.treateAnyNonPersistentObjectAsValue = getBooleanValue(value);
		}
		if ((value = props.getProperty("serialize.transient.objects")) != null) {
			ClassDescriptor.serializeNonPersistentObjects = getBooleanValue(value);
		}
		if ((value = props.getProperty("object.cache.init.size")) != null) {
			objectCacheInitSize = (int) getIntegerValue(value);
			if (objectCacheInitSize <= 0) {
				throw new IllegalArgumentException(
						"Initial object cache size should be positive");
			}
		}
		if ((value = props.getProperty("object.cache.kind")) != null) {
			cacheKind = value;
		}
		if ((value = props.getProperty("object.index.init.size")) != null) {
			initIndexSize = (int) getIntegerValue(value);
		}
		if ((value = props.getProperty("extension.quantum")) != null) {
			extensionQuantum = getIntegerValue(value);
		}
		if ((value = props.getProperty("gc.threshold")) != null) {
			gcThreshold = getIntegerValue(value);
		}
		if ((value = props.getProperty("file.readonly")) != null) {
			readOnly = getBooleanValue(value);
		}
		if ((value = props.getProperty("file.noflush")) != null) {
			noFlush = getBooleanValue(value);
		}
		if ((value = props.getProperty("alternative.btree")) != null) {
			alternativeBtree = getBooleanValue(value);
		}
		if ((value = props.getProperty("background.gc")) != null) {
			backgroundGc = getBooleanValue(value);
		}
		if ((value = props.getProperty("string.encoding")) != null) {
			encoding = value;
		}
		if ((value = props.getProperty("lock.file")) != null) {
			lockFile = getBooleanValue(value);
		}
		if ((value = props.getProperty("replication.ack")) != null) {
			replicationAck = getBooleanValue(value);
		}
		if ((value = props.getProperty("concurrent.iterator")) != null) {
			concurrentIterator = getBooleanValue(value);
		}
		if ((value = props.getProperty("slave.connection.timeout")) != null) {
			slaveConnectionTimeout = (int) getIntegerValue(value);
		}
		if ((value = props.getProperty("force.store")) != null) {
			forceStore = getBooleanValue(value);
		}
		if ((value = props.getProperty("page.pool.lru.limit")) != null) {
			pagePoolLruLimit = getIntegerValue(value);
		}
		if ((value = props.getProperty("multiclient.support")) != null) {
			multiclientSupport = getBooleanValue(value);
		}
		if ((value = props.getProperty("reload.objects.on.rollback")) != null) {
			reloadObjectsOnRollback = getBooleanValue(value);
		}
		if ((value = props.getProperty("reuse.oid")) != null) {
			reuseOid = getBooleanValue(value);
		}
		if ((value = props.getProperty("serialize.system.collections")) != null) {
			serializeSystemCollections = getBooleanValue(value);
		}
		if ((value = props.getProperty("compatibility.mode")) != null) {
			compatibilityMode = (int) getIntegerValue(value);
		}
		if (multiclientSupport && backgroundGc) {
			throw new IllegalArgumentException(
					"In mutliclient access mode bachround GC is not supported");
		}
	}

	public void setProperty(final String name, final Object value) {
		properties.put(name, value);
		if (name.equals("implicit.values")) {
			ClassDescriptor.treateAnyNonPersistentObjectAsValue = getBooleanValue(value);
		} else if (name.equals("object.cache.kind")) {
			cacheKind = (String) value;
		} else if (name.equals("object.index.init.size")) {
			initIndexSize = (int) getIntegerValue(value);
		} else if (name.equals("extension.quantum")) {
			extensionQuantum = getIntegerValue(value);
		} else if (name.equals("gc.threshold")) {
			gcThreshold = getIntegerValue(value);
		} else if (name.equals("file.readonly")) {
			readOnly = getBooleanValue(value);
		} else if (name.equals("file.noflush")) {
			noFlush = getBooleanValue(value);
		} else if (name.equals("alternative.btree")) {
			alternativeBtree = getBooleanValue(value);
		} else if (name.equals("background.gc")) {
			backgroundGc = getBooleanValue(value);
		} else if (name.equals("string.encoding")) {
			encoding = (value == null) ? null : value.toString();
		} else if (name.equals("lock.file")) {
			lockFile = getBooleanValue(value);
		} else if (name.equals("replication.ack")) {
			replicationAck = getBooleanValue(value);
		} else if (name.equals("concurrent.iterator")) {
			concurrentIterator = getBooleanValue(value);
		} else if (name.equals("slave.connection.timeout")) {
			slaveConnectionTimeout = (int) getIntegerValue(value);
		} else if (name.equals("force.store")) {
			forceStore = getBooleanValue(value);
		} else if (name.equals("page.pool.lru.limit")) {
			pagePoolLruLimit = getIntegerValue(value);
		} else if (name.equals("multiclient.support")) {
			multiclientSupport = getBooleanValue(value);
		} else if (name.equals("reload.objects.on.rollback")) {
			reloadObjectsOnRollback = getBooleanValue(value);
		} else if (name.equals("reuse.oid")) {
			reuseOid = getBooleanValue(value);
		} else if (name.equals("compatibility.mode")) {
			compatibilityMode = (int) getIntegerValue(value);
		} else if (name.equals("serialize.system.collections")) {
			serializeSystemCollections = getBooleanValue(value);
		}
		if (multiclientSupport && backgroundGc) {
			throw new IllegalArgumentException(
					"In mutliclient access mode bachround GC is not supported");
		}
	}

	public Object getProperty(final String name) {
		return properties.get(name);
	}

	public Properties getProperties() {
		return properties;
	}

	public ODbListener setListener(final ODbListener listener) {
		final ODbListener prevListener = this.listener;
		this.listener = listener;
		return prevListener;
	}

	public ODbListener getOODBListener() {
		return listener;
	}

	public synchronized Object getObjectByOID(final int oid) {
		return oid == 0 ? null : lookupObject(oid, null);
	}

	public/* protected */synchronized void modifyObject(final Object obj) {
		synchronized (objectCache) {
			if (!isModified(obj)) {
				modified = true;
				if (useSerializableTransactions) {
					final ThreadTransactionContext ctx = getTransactionContext();
					if (ctx.nested != 0) { // serializable transaction
						ctx.modified.add(obj);
						return;
					}
				}
				objectCache.setDirty(obj);
			}
		}
	}

	public/* protected */synchronized void storeObject(final Object obj) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		if (useSerializableTransactions && getTransactionContext().nested != 0) {
			// Store should not be used in serializable transaction mode
			throw new ODbException(ODbException.INVALID_OPERATION,
					"store object");
		}
		synchronized (objectCache) {
			storeObject0(obj, false);
		}
	}

	public/* protected */void storeFinalizedObject(final Object obj) {
		if (opened) {
			synchronized (objectCache) {
				if (getOid(obj) != 0) {
					try {
						storeObject0(obj, true);
					} catch (final Throwable x) {
						x.printStackTrace();
						Runtime.getRuntime().halt(1);
					}
				}
			}
		}
	}

	public synchronized int makePersistent(final Object obj) {
		if (!opened) {
			throw new ODbException(ODbException.DATABASE_NOT_OPENED);
		}
		if (obj == null) {
			return 0;
		}
		int oid = getOid(obj);
		if (oid != 0) {
			return oid;
		}
		if (forceStore
				&& (!useSerializableTransactions || getTransactionContext().nested == 0)) {
			synchronized (objectCache) {
				storeObject0(obj, false);
			}
			return getOid(obj);
		} else {
			synchronized (objectCache) {
				oid = allocateId();
				assignOid(obj, oid, false);
				setPos(oid, 0);
				objectCache.put(oid, obj);
				modify(obj);
				return oid;
			}
		}
	}

	private final CustomAllocator getCustomAllocator(final Class cls) {
		final Object a = customAllocatorMap.get(cls);
		if (a != null) {
			return a == defaultAllocator ? null : (CustomAllocator) a;
		}
		final Class superclass = cls.getSuperclass();
		if (superclass != null) {
			final CustomAllocator alloc = getCustomAllocator(superclass);
			if (alloc != null) {
				customAllocatorMap.put(cls, alloc);
				return alloc;
			}
		}
		final Class[] interfaces = cls.getInterfaces();
		for (int i = 0; i < interfaces.length; i++) {
			final CustomAllocator alloc = getCustomAllocator(interfaces[i]);
			if (alloc != null) {
				customAllocatorMap.put(cls, alloc);
				return alloc;
			}
		}
		customAllocatorMap.put(cls, defaultAllocator);
		return null;
	}

	private final void storeObject0(final Object obj, final boolean finalized) {
		if (obj instanceof IStoreable) {
			((IODbObject) obj).onStore();
		}
		if (listener != null) {
			listener.onObjectStore(obj);
		}
		int oid = getOid(obj);
		boolean newObject = false;
		if (oid == 0) {
			oid = allocateId();
			if (!finalized) {
				objectCache.put(oid, obj);
			}
			assignOid(obj, oid, false);
			newObject = true;
		} else if (isModified(obj)) {
			objectCache.clearDirty(obj);
		}
		final byte[] data = packObject(obj, finalized);
		long pos;
		final int newSize = ObjectHeader.getSize(data, 0);
		final CustomAllocator allocator = (customAllocatorMap != null) ? getCustomAllocator(obj
				.getClass()) : null;
		if (newObject || (pos = getPos(oid)) == 0) {
			pos = allocator != null ? allocator.allocate(newSize) : allocate(
					newSize, 0);
			setPos(oid, pos | dbModifiedFlag);
		} else {
			final int offs = (int) pos & (Page.pageSize - 1);
			if ((offs & (dbFreeHandleFlag | dbPageObjectFlag)) != 0) {
				throw new ODbException(ODbException.DELETED_OBJECT);
			}
			final Page pg = pool.getPage(pos - offs);
			final int size = ObjectHeader.getSize(pg.data, offs & ~dbFlagsMask);
			pool.unfix(pg);
			if ((pos & dbModifiedFlag) == 0) {
				if (allocator != null) {
					allocator.free(pos & ~dbFlagsMask, size);
					pos = allocator.allocate(newSize);
				} else {
					cloneBitmap(pos & ~dbFlagsMask, size);
					pos = allocate(newSize, 0);
				}
				setPos(oid, pos | dbModifiedFlag);
			} else {
				pos &= ~dbFlagsMask;
				if (newSize != size) {
					if (allocator != null) {
						final long newPos = allocator.reallocate(pos, size,
								newSize);
						if (newPos != pos) {
							pos = newPos;
							setPos(oid, pos | dbModifiedFlag);
						} else if (newSize < size) {
							ObjectHeader.setSize(data, 0, size);
						}
					} else {
						if (((newSize + dbAllocationQuantum - 1) & ~(dbAllocationQuantum - 1)) > ((size
								+ dbAllocationQuantum - 1) & ~(dbAllocationQuantum - 1))) {
							final long newPos = allocate(newSize, 0);
							cloneBitmap(pos, size);
							free(pos, size);
							pos = newPos;
							setPos(oid, pos | dbModifiedFlag);
						} else if (newSize < size) {
							ObjectHeader.setSize(data, 0, size);
						}
					}
				}
			}
		}
		modified = true;
		pool.put(pos, data, newSize);
	}

	public/* protected */synchronized void loadObject(final Object obj) {
		if (isRaw(obj)) {
			loadStub(getOid(obj), obj, obj.getClass());
		}
	}

	final synchronized Object lookupObject(final int oid, final Class cls) {
		Object obj = objectCache.get(oid);
		if (obj == null || isRaw(obj)) {
			obj = loadStub(oid, obj, cls);
		}
		return obj;
	}

	protected int swizzle(final Object obj, final boolean finalized) {
		int oid = 0;
		if (obj != null) {
			if (!isPersistent(obj)) {
				storeObject0(obj, finalized);
			}
			oid = getOid(obj);
		}
		return oid;
	}

	final ClassDescriptor findClassDescriptor(final int oid) {
		return (ClassDescriptor) lookupObject(oid, ClassDescriptor.class);
	}

	class ByteArrayObjectInputStream extends ODbInputStream {
		private final byte[] buf;
		private final Object parent;
		private final boolean recursiveLoading;
		private final boolean markReferences;

		ByteArrayObjectInputStream(final byte[] buf, final int offs,
				final Object parent, final boolean resursiveLoading,
				final boolean markReferenes) {
			super(new ByteArrayInputStream(buf, offs, buf.length - offs));
			this.buf = buf;
			this.parent = parent;
			this.recursiveLoading = resursiveLoading;
			this.markReferences = markReferenes;
		}

		int getPosition() throws IOException {
			return buf.length - in.available();
		}

		public String readString() throws IOException {
			final int offs = getPosition();
			final ArrayPos pos = new ArrayPos(buf, offs);
			final String str = ByteUtils.unpackString(pos, encoding);
			in.skip(pos.offs - offs);
			return str;
		}

		public Object readObject() throws IOException {
			int offs = getPosition();
			Object obj = null;
			if (markReferences) {
				offs = markObjectReference(buf, offs) - offs;
			} else {
				try {
					final ArrayPos pos = new ArrayPos(buf, offs);
					obj = unswizzle(pos, Object.class, parent, recursiveLoading);
					offs = pos.offs - offs;
				} catch (final Exception x) {
					throw new ODbException(ODbException.ACCESS_VIOLATION, x);
				}
			}
			in.skip(offs);
			return obj;
		}
	}

	protected Object unswizzle(final int oid, final Class cls,
			final boolean recursiveLoading) {
		if (oid == 0) {
			return null;
		}
		if (recursiveLoading) {
			return lookupObject(oid, cls);
		}
		Object stub = objectCache.get(oid);
		if (stub != null) {
			return stub;
		}
		ClassDescriptor desc;
		if (cls == Object.class || (desc = findClassDescriptor(cls)) == null
				|| desc.hasSubclasses) {
			final long pos = getPos(oid);
			final int offs = (int) pos & (Page.pageSize - 1);
			if ((offs & (dbFreeHandleFlag | dbPageObjectFlag)) != 0) {
				throw new ODbException(ODbException.DELETED_OBJECT);
			}
			final Page pg = pool.getPage(pos - offs);
			final int typeOid = ObjectHeader.getType(pg.data, offs
					& ~dbFlagsMask);
			pool.unfix(pg);
			desc = findClassDescriptor(typeOid);
		}
		stub = desc.newInstance();
		assignOid(stub, oid, true);
		objectCache.put(oid, stub);
		return stub;
	}

	final Object loadStub(final int oid, Object obj, final Class cls) {
		final long pos = getPos(oid);
		if ((pos & (dbFreeHandleFlag | dbPageObjectFlag)) != 0) {
			throw new ODbException(ODbException.DELETED_OBJECT);
		}
		final byte[] body = pool.get(pos & ~dbFlagsMask);
		ClassDescriptor desc;
		final int typeOid = ObjectHeader.getType(body, 0);
		if (typeOid == 0) {
			desc = findClassDescriptor(cls);
		} else {
			desc = findClassDescriptor(typeOid);
		}
		// synchronized (objectCache)
		{
			if (obj == null) {
				obj = desc.customSerializable ? serializer.create(desc.cls)
						: desc.newInstance();
				objectCache.put(oid, obj);
			}
			assignOid(obj, oid, false);
			try {
				if (obj instanceof SelfSerializable) {
					((SelfSerializable) obj)
							.unpack(new ByteArrayObjectInputStream(body,
									ObjectHeader.sizeof, obj,
									recursiveLoading(obj), false));
				} else if (desc.customSerializable) {
					serializer.unpack(obj, new ByteArrayObjectInputStream(body,
							ObjectHeader.sizeof, obj, recursiveLoading(obj),
							false));
				} else {
					unpackObject(obj, desc, recursiveLoading(obj), body,
							ObjectHeader.sizeof, obj);
				}
			} catch (final Exception x) {
				throw new ODbException(ODbException.ACCESS_VIOLATION, x);
			}
		}
		if (obj instanceof Loadable) {
			((IODbObject) obj).onLoad();
		}
		if (listener != null) {
			listener.onObjectLoad(obj);
		}
		return obj;
	}

	class PersistentObjectInputStream extends ObjectInputStream {
		PersistentObjectInputStream(final InputStream in) throws IOException {
			super(in);
			enableResolveObject(true);
		}

		protected Object resolveObject(final Object obj) throws IOException {
			final int oid = getOid(obj);
			if (oid != 0) {
				return lookupObject(oid, obj.getClass());
			}
			return obj;
		}

		protected Class resolveClass(final ObjectStreamClass desc)
				throws IOException, ClassNotFoundException {
			String classLoaderName = null;
			if (loaderMap != null
					&& (compatibilityMode & CLASS_LOADER_SERIALIZATION_COMPATIBILITY_MODE) == 0) {
				classLoaderName = (String) readObject();
			}
			final ClassLoader cl = (classLoaderName != null) ? findClassLoader(classLoaderName)
					: loader;
			if (cl != null) {
				try {
					return Class.forName(desc.getName(), false, cl);
				} catch (final ClassNotFoundException x) {
				}
			}
			return super.resolveClass(desc);
		}
	}

	public class PersistentObjectOutputStream extends ObjectOutputStream {
		PersistentObjectOutputStream(final OutputStream out) throws IOException {
			super(out);
		}

		public ODb getStorage() {
			return ODbImpl.this;
		}
	}

	public class AnnotatedPersistentObjectOutputStream extends
			PersistentObjectOutputStream {
		AnnotatedPersistentObjectOutputStream(final OutputStream out)
				throws IOException {
			super(out);
		}

		protected void annotateClass(final Class cls) throws IOException {
			final ClassLoader loader = cls.getClassLoader();
			writeObject((loader instanceof NamedClassLoader) ? ((NamedClassLoader) loader)
					.getName() : null);
		}
	}

	final int skipObjectReference(final byte[] obj, int offs) throws Exception {
		final int oid = ByteUtils.unpack4(obj, offs);
		int len;
		offs += 4;
		if (oid < 0) {
			final int tid = -1 - oid;
			switch (tid) {
			case ClassDescriptor.tpString:
			case ClassDescriptor.tpClass:
				offs = ByteUtils.skipString(obj, offs);
				break;
			case ClassDescriptor.tpArrayOfByte:
				len = ByteUtils.unpack4(obj, offs);
				offs += len + 4;
				break;
			case ClassDescriptor.tpArrayOfObject:
				len = ByteUtils.unpack4(obj, offs);
				offs += 4;
				for (int i = 0; i < len; i++) {
					offs = skipObjectReference(obj, offs);
				}
				break;
			case ClassDescriptor.tpArrayOfRaw:
				len = ByteUtils.unpack4(obj, offs);
				offs += 8;
				for (int i = 0; i < len; i++) {
					offs = skipObjectReference(obj, offs);
				}
				break;
			default:
				if (tid >= ClassDescriptor.tpValueTypeBias) {
					final int typeOid = -ClassDescriptor.tpValueTypeBias - oid;
					final ClassDescriptor desc = findClassDescriptor(typeOid);
					if (desc.isCollection) {
						len = ByteUtils.unpack4(obj, offs);
						offs += 4;
						for (int i = 0; i < len; i++) {
							offs = skipObjectReference(obj, offs);
						}
					} else if (desc.isMap) {
						len = ByteUtils.unpack4(obj, offs);
						offs += 4;
						for (int i = 0; i < len; i++) {
							offs = skipObjectReference(obj, offs);
							offs = skipObjectReference(obj, offs);
						}
					} else {
						offs = unpackObject(null, findClassDescriptor(typeOid),
								false, obj, offs, null);
					}
				} else {
					offs += ClassDescriptor.sizeof[tid];
				}
			}
		}
		return offs;
	}

	final Object unswizzle(final ArrayPos obj, final Class cls,
			final Object parent, final boolean recursiveLoading)
			throws Exception {
		final byte[] body = obj.body;
		int offs = obj.offs;
		final int oid = ByteUtils.unpack4(body, offs);
		offs += 4;
		Object val;
		if (oid < 0) {
			switch (-1 - oid) {
			case ClassDescriptor.tpBoolean:
				val = body[offs++] != 0;
				break;
			case ClassDescriptor.tpByte:
				val = body[offs++];
				break;
			case ClassDescriptor.tpChar:
				val = (char) ByteUtils.unpack2(body, offs);
				offs += 2;
				break;
			case ClassDescriptor.tpShort:
				val = ByteUtils.unpack2(body, offs);
				offs += 2;
				break;
			case ClassDescriptor.tpInt:
				val = ByteUtils.unpack4(body, offs);
				offs += 4;
				break;
			case ClassDescriptor.tpLong:
				val = ByteUtils.unpack8(body, offs);
				offs += 8;
				break;
			case ClassDescriptor.tpFloat:
				val = ByteUtils.unpackF4(body, offs);
				offs += 4;
				break;
			case ClassDescriptor.tpDouble:
				val = ByteUtils.unpackF8(body, offs);
				offs += 8;
				break;
			case ClassDescriptor.tpDate:
				val = new Date(ByteUtils.unpack8(body, offs));
				offs += 8;
				break;
			case ClassDescriptor.tpString:
				obj.offs = offs;
				return ByteUtils.unpackString(obj, encoding);
			case ClassDescriptor.tpClass:
				obj.offs = offs;
				return ClassDescriptor.loadClass(this,
						ByteUtils.unpackString(obj, encoding));
			case ClassDescriptor.tpLink: {
				final int len = ByteUtils.unpack4(body, offs);
				offs += 4;
				final Object[] arr = new Object[len];
				for (int j = 0; j < len; j++) {
					final int elemOid = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (elemOid != 0) {
						arr[j] = new PersistentStub(this, elemOid);
					}
				}
				val = new LinkImpl(this, arr, parent);
				break;
			}
			case ClassDescriptor.tpArrayOfByte: {
				final int len = ByteUtils.unpack4(body, offs);
				offs += 4;
				final byte[] arr = new byte[len];
				System.arraycopy(body, offs, arr, 0, len);
				offs += len;
				val = arr;
				break;
			}
			case ClassDescriptor.tpArrayOfBoolean: {
				final int len = ByteUtils.unpack4(body, offs);
				offs += 4;
				final boolean[] arr = new boolean[len];
				for (int j = 0; j < len; j++) {
					arr[j] = body[offs++] != 0;
				}
				val = arr;
				break;
			}
			case ClassDescriptor.tpArrayOfShort: {
				final int len = ByteUtils.unpack4(body, offs);
				offs += 4;
				final short[] arr = new short[len];
				for (int j = 0; j < len; j++) {
					arr[j] = ByteUtils.unpack2(body, offs);
					offs += 2;
				}
				val = arr;
				break;
			}
			case ClassDescriptor.tpArrayOfChar: {
				final int len = ByteUtils.unpack4(body, offs);
				offs += 4;
				final char[] arr = new char[len];
				for (int j = 0; j < len; j++) {
					arr[j] = (char) ByteUtils.unpack2(body, offs);
					offs += 2;
				}
				val = arr;
				break;
			}
			case ClassDescriptor.tpArrayOfInt: {
				final int len = ByteUtils.unpack4(body, offs);
				offs += 4;
				final int[] arr = new int[len];
				for (int j = 0; j < len; j++) {
					arr[j] = ByteUtils.unpack4(body, offs);
					offs += 4;
				}
				val = arr;
				break;
			}
			case ClassDescriptor.tpArrayOfLong: {
				final int len = ByteUtils.unpack4(body, offs);
				offs += 4;
				final long[] arr = new long[len];
				for (int j = 0; j < len; j++) {
					arr[j] = ByteUtils.unpack8(body, offs);
					offs += 8;
				}
				val = arr;
				break;
			}
			case ClassDescriptor.tpArrayOfFloat: {
				final int len = ByteUtils.unpack4(body, offs);
				offs += 4;
				final float[] arr = new float[len];
				for (int j = 0; j < len; j++) {
					arr[j] = ByteUtils.unpackF4(body, offs);
					offs += 4;
				}
				val = arr;
				break;
			}
			case ClassDescriptor.tpArrayOfDouble: {
				final int len = ByteUtils.unpack4(body, offs);
				offs += 4;
				final double[] arr = new double[len];
				for (int j = 0; j < len; j++) {
					arr[j] = ByteUtils.unpackF8(body, offs);
					offs += 8;
				}
				val = arr;
				break;
			}
			case ClassDescriptor.tpArrayOfObject: {
				final int len = ByteUtils.unpack4(body, offs);
				obj.offs = offs + 4;
				final Object[] arr = new Object[len];
				for (int j = 0; j < len; j++) {
					arr[j] = unswizzle(obj, Object.class, parent,
							recursiveLoading);
				}
				return arr;
			}
			case ClassDescriptor.tpArrayOfRaw: {
				final int len = ByteUtils.unpack4(body, offs);
				final int typeOid = ByteUtils.unpack4(body, offs + 4);
				obj.offs = offs + 8;
				Class elemType;
				if (typeOid == -1) {
					elemType = Comparable.class;
				} else {
					final ClassDescriptor desc = findClassDescriptor(typeOid);
					elemType = desc.cls;
				}
				final Object arr = Array.newInstance(elemType, len);
				for (int j = 0; j < len; j++) {
					Array.set(arr, j,
							unswizzle(obj, elemType, parent, recursiveLoading));
				}
				return arr;
			}
			case ClassDescriptor.tpCustom: {
				final ByteArrayObjectInputStream in = new ByteArrayObjectInputStream(
						body, offs, parent, recursiveLoading, false);
				val = serializer.unpack(in);
				offs = in.getPosition();
				break;
			}
			default:
				if (oid < -ClassDescriptor.tpValueTypeBias) {
					final int typeOid = -ClassDescriptor.tpValueTypeBias - oid;
					final ClassDescriptor desc = findClassDescriptor(typeOid);
					val = desc.newInstance();
					if (desc.isCollection) {
						final int len = ByteUtils.unpack4(body, offs);
						obj.offs = offs + 4;
						final Collection collection = (Collection) val;
						for (int i = 0; i < len; i++) {
							collection.add(unswizzle(obj, Object.class, parent,
									recursiveLoading));
						}
						return collection;
					} else if (desc.isMap) {
						final int len = ByteUtils.unpack4(body, offs);
						obj.offs = offs + 4;
						final Map map = (Map) val;
						for (int i = 0; i < len; i++) {
							final Object key = unswizzle(obj, Object.class,
									parent, recursiveLoading);
							final Object value = unswizzle(obj, Object.class,
									parent, recursiveLoading);
							map.put(key, value);
						}
						return map;
					} else {
						offs = unpackObject(val, desc, recursiveLoading, body,
								offs, parent);
					}
				} else {
					throw new ODbException(ODbException.UNSUPPORTED_TYPE);
				}
			}
		} else {
			val = unswizzle(oid, cls, recursiveLoading);
		}
		obj.offs = offs;
		return val;
	}

	final int unpackObject(final Object obj, final ClassDescriptor desc,
			final boolean recursiveLoading, final byte[] body, int offs,
			final Object parent) throws Exception {
		final ClassDescriptor.FieldDescriptor[] all = desc.allFields;
		final ReflectionProvider provider = ClassDescriptor
				.getReflectionProvider();
		int len;

		for (int i = 0, n = all.length; i < n; i++) {
			final ClassDescriptor.FieldDescriptor fd = all[i];
			final Field f = fd.field;

			if (f == null || obj == null) {
				switch (fd.type) {
				case ClassDescriptor.tpBoolean:
				case ClassDescriptor.tpByte:
					offs += 1;
					continue;
				case ClassDescriptor.tpChar:
				case ClassDescriptor.tpShort:
					offs += 2;
					continue;
				case ClassDescriptor.tpInt:
				case ClassDescriptor.tpFloat:
				case ClassDescriptor.tpEnum:
					offs += 4;
					continue;
				case ClassDescriptor.tpObject:
					offs = skipObjectReference(body, offs);
					continue;
				case ClassDescriptor.tpLong:
				case ClassDescriptor.tpDouble:
				case ClassDescriptor.tpDate:
					offs += 8;
					continue;
				case ClassDescriptor.tpString:
				case ClassDescriptor.tpClass:
					offs = ByteUtils.skipString(body, offs);
					continue;
				case ClassDescriptor.tpValue:
					offs = unpackObject(null, fd.valueDesc, recursiveLoading,
							body, offs, parent);
					continue;
				case ClassDescriptor.tpRaw:
				case ClassDescriptor.tpArrayOfByte:
				case ClassDescriptor.tpArrayOfBoolean:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len > 0) {
						offs += len;
					} else if (len < -1) {
						offs += ClassDescriptor.sizeof[-2 - len];
					}
					continue;
				case ClassDescriptor.tpCustom: {
					final ByteArrayObjectInputStream in = new ByteArrayObjectInputStream(
							body, offs, parent, recursiveLoading, false);
					serializer.unpack(in);
					offs = in.getPosition();
					continue;
				}
				case ClassDescriptor.tpArrayOfShort:
				case ClassDescriptor.tpArrayOfChar:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len > 0) {
						offs += len * 2;
					}
					continue;
				case ClassDescriptor.tpArrayOfObject:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					for (int j = 0; j < len; j++) {
						offs = skipObjectReference(body, offs);
					}
					continue;
				case ClassDescriptor.tpArrayOfInt:
				case ClassDescriptor.tpArrayOfEnum:
				case ClassDescriptor.tpArrayOfFloat:
				case ClassDescriptor.tpLink:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len > 0) {
						offs += len * 4;
					}
					continue;
				case ClassDescriptor.tpArrayOfLong:
				case ClassDescriptor.tpArrayOfDouble:
				case ClassDescriptor.tpArrayOfDate:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len > 0) {
						offs += len * 8;
					}
					continue;
				case ClassDescriptor.tpArrayOfString:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len > 0) {
						for (int j = 0; j < len; j++) {
							offs = ByteUtils.skipString(body, offs);
						}
					}
					continue;
				case ClassDescriptor.tpArrayOfValue:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len > 0) {
						final ClassDescriptor valueDesc = fd.valueDesc;
						for (int j = 0; j < len; j++) {
							offs = unpackObject(null, valueDesc,
									recursiveLoading, body, offs, parent);
						}
					}
					continue;
				}
			} else if (offs < body.length) {
				switch (fd.type) {
				case ClassDescriptor.tpBoolean:
					provider.setBoolean(f, obj, body[offs++] != 0);
					continue;
				case ClassDescriptor.tpByte:
					provider.setByte(f, obj, body[offs++]);
					continue;
				case ClassDescriptor.tpChar:
					provider.setChar(f, obj,
							(char) ByteUtils.unpack2(body, offs));
					offs += 2;
					continue;
				case ClassDescriptor.tpShort:
					provider.setShort(f, obj, ByteUtils.unpack2(body, offs));
					offs += 2;
					continue;
				case ClassDescriptor.tpInt:
					provider.setInt(f, obj, ByteUtils.unpack4(body, offs));
					offs += 4;
					continue;
				case ClassDescriptor.tpLong:
					provider.setLong(f, obj, ByteUtils.unpack8(body, offs));
					offs += 8;
					continue;
				case ClassDescriptor.tpFloat:
					provider.setFloat(f, obj, ByteUtils.unpackF4(body, offs));
					offs += 4;
					continue;
				case ClassDescriptor.tpDouble:
					provider.setDouble(f, obj, ByteUtils.unpackF8(body, offs));
					offs += 8;
					continue;
				case ClassDescriptor.tpEnum: {
					final int index = ByteUtils.unpack4(body, offs);
					if (index >= 0) {
						provider.set(f, obj, fd.field.getType()
								.getEnumConstants()[index]);
					} else {
						provider.set(f, obj, null);
					}
					offs += 4;
					continue;
				}
				case ClassDescriptor.tpString: {
					final ArrayPos pos = new ArrayPos(body, offs);
					provider.set(f, obj, ByteUtils.unpackString(pos, encoding));
					offs = pos.offs;
					continue;
				}
				case ClassDescriptor.tpClass: {
					final ArrayPos pos = new ArrayPos(body, offs);
					final Class cls = ClassDescriptor.loadClass(this,
							ByteUtils.unpackString(pos, encoding));
					provider.set(f, obj, cls);
					offs = pos.offs;
					continue;
				}
				case ClassDescriptor.tpDate: {
					final long msec = ByteUtils.unpack8(body, offs);
					offs += 8;
					Date date = null;
					if (msec >= 0) {
						date = new Date(msec);
					}
					provider.set(f, obj, date);
					continue;
				}
				case ClassDescriptor.tpObject: {
					final ArrayPos pos = new ArrayPos(body, offs);
					provider.set(
							f,
							obj,
							unswizzle(pos, f.getType(), parent,
									recursiveLoading));
					offs = pos.offs;
					continue;
				}
				case ClassDescriptor.tpValue: {
					final Object value = fd.valueDesc.newInstance();
					offs = unpackObject(value, fd.valueDesc, recursiveLoading,
							body, offs, parent);
					provider.set(f, obj, value);
					continue;
				}
				case ClassDescriptor.tpRaw:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len >= 0) {
						final ByteArrayInputStream bin = new ByteArrayInputStream(
								body, offs, len);
						final ObjectInputStream in = new PersistentObjectInputStream(
								bin);
						provider.set(f, obj, in.readObject());
						in.close();
						offs += len;
					} else if (len < 0) {
						Object val = null;
						switch (-2 - len) {
						case ClassDescriptor.tpBoolean:
							val = Boolean.valueOf(body[offs++] != 0);
							break;
						case ClassDescriptor.tpByte:
							val = new Byte(body[offs++]);
							break;
						case ClassDescriptor.tpChar:
							val = new Character((char) ByteUtils.unpack2(body,
									offs));
							offs += 2;
							break;
						case ClassDescriptor.tpShort:
							val = new Short(ByteUtils.unpack2(body, offs));
							offs += 2;
							break;
						case ClassDescriptor.tpInt:
							val = new Integer(ByteUtils.unpack4(body, offs));
							offs += 4;
							break;
						case ClassDescriptor.tpLong:
							val = new Long(ByteUtils.unpack8(body, offs));
							offs += 8;
							break;
						case ClassDescriptor.tpFloat:
							val = new Float(Float.intBitsToFloat(ByteUtils
									.unpack4(body, offs)));
							offs += 4;
							break;
						case ClassDescriptor.tpDouble:
							val = new Double(Double.longBitsToDouble(ByteUtils
									.unpack8(body, offs)));
							offs += 8;
							break;
						case ClassDescriptor.tpDate:
							val = new Date(ByteUtils.unpack8(body, offs));
							offs += 8;
							break;
						case ClassDescriptor.tpObject:
							val = unswizzle(ByteUtils.unpack4(body, offs),
									ODbObject.class, recursiveLoading);
							offs += 4;
						}
						provider.set(f, obj, val);
					}
					continue;
				case ClassDescriptor.tpCustom: {
					final ByteArrayObjectInputStream in = new ByteArrayObjectInputStream(
							body, offs, parent, recursiveLoading, false);
					serializer.unpack(in);
					provider.set(f, obj, serializer.unpack(in));
					offs = in.getPosition();
					continue;
				}
				case ClassDescriptor.tpArrayOfByte:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final byte[] arr = new byte[len];
						System.arraycopy(body, offs, arr, 0, len);
						offs += len;
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfBoolean:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final boolean[] arr = new boolean[len];
						for (int j = 0; j < len; j++) {
							arr[j] = body[offs++] != 0;
						}
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfShort:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final short[] arr = new short[len];
						for (int j = 0; j < len; j++) {
							arr[j] = ByteUtils.unpack2(body, offs);
							offs += 2;
						}
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfChar:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final char[] arr = new char[len];
						for (int j = 0; j < len; j++) {
							arr[j] = (char) ByteUtils.unpack2(body, offs);
							offs += 2;
						}
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfInt:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final int[] arr = new int[len];
						for (int j = 0; j < len; j++) {
							arr[j] = ByteUtils.unpack4(body, offs);
							offs += 4;
						}
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfEnum:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						f.set(obj, null);
					} else {
						final Class elemType = f.getType().getComponentType();
						final Enum[] enumConstants = (Enum[]) elemType
								.getEnumConstants();
						final Enum[] arr = (Enum[]) Array.newInstance(elemType,
								len);
						for (int j = 0; j < len; j++) {
							final int index = ByteUtils.unpack4(body, offs);
							if (index >= 0) {
								arr[j] = enumConstants[index];
							}
							offs += 4;
						}
						f.set(obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfLong:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final long[] arr = new long[len];
						for (int j = 0; j < len; j++) {
							arr[j] = ByteUtils.unpack8(body, offs);
							offs += 8;
						}
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfFloat:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final float[] arr = new float[len];
						for (int j = 0; j < len; j++) {
							arr[j] = ByteUtils.unpackF4(body, offs);
							offs += 4;
						}
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfDouble:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final double[] arr = new double[len];
						for (int j = 0; j < len; j++) {
							arr[j] = ByteUtils.unpackF8(body, offs);
							offs += 8;
						}
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfDate:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final Date[] arr = new Date[len];
						for (int j = 0; j < len; j++) {
							final long msec = ByteUtils.unpack8(body, offs);
							offs += 8;
							if (msec >= 0) {
								arr[j] = new Date(msec);
							}
						}
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfString:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final String[] arr = new String[len];
						final ArrayPos pos = new ArrayPos(body, offs);
						for (int j = 0; j < len; j++) {
							arr[j] = ByteUtils.unpackString(pos, encoding);
						}
						offs = pos.offs;
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfObject:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final Class elemType = f.getType().getComponentType();
						final Object[] arr = (Object[]) Array.newInstance(
								elemType, len);
						final ArrayPos pos = new ArrayPos(body, offs);
						for (int j = 0; j < len; j++) {
							arr[j] = unswizzle(pos, elemType, parent,
									recursiveLoading);
						}
						offs = pos.offs;
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpArrayOfValue:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final Class elemType = f.getType().getComponentType();
						final Object[] arr = (Object[]) Array.newInstance(
								elemType, len);
						final ClassDescriptor valueDesc = fd.valueDesc;
						for (int j = 0; j < len; j++) {
							final Object value = valueDesc.newInstance();
							offs = unpackObject(value, valueDesc,
									recursiveLoading, body, offs, parent);
							arr[j] = value;
						}
						provider.set(f, obj, arr);
					}
					continue;
				case ClassDescriptor.tpLink:
					len = ByteUtils.unpack4(body, offs);
					offs += 4;
					if (len < 0) {
						provider.set(f, obj, null);
					} else {
						final Object[] arr = new Object[len];
						for (int j = 0; j < len; j++) {
							final int elemOid = ByteUtils.unpack4(body, offs);
							offs += 4;
							if (elemOid != 0) {
								arr[j] = new PersistentStub(this, elemOid);
							}
						}
						provider.set(f, obj, new LinkImpl(this, arr, parent));
					}
				}
			}
		}
		return offs;
	}

	final int packValue(final Object value, int offs, final ByteBuffer buf)
			throws Exception {
		if (value == null) {
			buf.extend(offs + 4);
			ByteUtils.pack4(buf.arr, offs, -1);
			offs += 4;
		} else if (value instanceof IODbObject) {
			buf.extend(offs + 8);
			ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpObject);
			ByteUtils.pack4(buf.arr, offs + 4, swizzle(value, buf.finalized));
			offs += 8;
		} else {
			final Class c = value.getClass();
			if (c == Boolean.class) {
				buf.extend(offs + 5);
				ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpBoolean);
				buf.arr[offs + 4] = (byte) (((Boolean) value).booleanValue() ? 1
						: 0);
				offs += 5;
			} else if (c == Character.class) {
				buf.extend(offs + 6);
				ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpChar);
				ByteUtils.pack2(buf.arr, offs + 4,
						(short) ((Character) value).charValue());
				offs += 6;
			} else if (c == Byte.class) {
				buf.extend(offs + 5);
				ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpByte);
				buf.arr[offs + 4] = ((Byte) value).byteValue();
				offs += 5;
			} else if (c == Short.class) {
				buf.extend(offs + 6);
				ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpShort);
				ByteUtils
						.pack2(buf.arr, offs + 4, ((Short) value).shortValue());
				offs += 6;
			} else if (c == Integer.class) {
				buf.extend(offs + 8);
				ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpInt);
				ByteUtils
						.pack4(buf.arr, offs + 4, ((Integer) value).intValue());
				offs += 8;
			} else if (c == Long.class) {
				buf.extend(offs + 12);
				ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpLong);
				ByteUtils.pack8(buf.arr, offs + 4, ((Long) value).longValue());
				offs += 12;
			} else if (c == Float.class) {
				buf.extend(offs + 8);
				ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpFloat);
				ByteUtils.pack4(buf.arr, offs + 4,
						Float.floatToIntBits(((Float) value).floatValue()));
				offs += 8;
			} else if (c == Double.class) {
				buf.extend(offs + 12);
				ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpDouble);
				ByteUtils
						.pack8(buf.arr, offs + 4, Double
								.doubleToLongBits(((Double) value)
										.doubleValue()));
				offs += 12;
			} else if (c == Date.class) {
				buf.extend(offs + 12);
				ByteUtils.pack4(buf.arr, offs, -2 - ClassDescriptor.tpDate);
				ByteUtils.pack8(buf.arr, offs + 4, ((Date) value).getTime());
				offs += 12;
			} else {
				final ByteArrayOutputStream bout = new ByteArrayOutputStream();
				final ObjectOutputStream out = loaderMap != null
						&& (compatibilityMode & CLASS_LOADER_SERIALIZATION_COMPATIBILITY_MODE) == 0 ? (ObjectOutputStream) new AnnotatedPersistentObjectOutputStream(
						bout)
						: (ObjectOutputStream) new PersistentObjectOutputStream(
								bout);
				out.writeObject(value);
				out.close();
				final byte[] arr = bout.toByteArray();
				final int len = arr.length;
				buf.extend(offs + 4 + len);
				ByteUtils.pack4(buf.arr, offs, len);
				offs += 4;
				System.arraycopy(arr, 0, buf.arr, offs, len);
				offs += len;
			}
		}
		return offs;
	}

	final byte[] packObject(final Object obj, final boolean finalized) {
		final ByteBuffer buf = new ByteBuffer(this, obj, finalized);
		int offs = ObjectHeader.sizeof;
		buf.extend(offs);
		final ClassDescriptor desc = getClassDescriptor(obj.getClass());
		try {
			if (obj instanceof SelfSerializable) {
				((SelfSerializable) obj).pack(buf.getOutputStream());
				offs = buf.used;
			} else if (desc.customSerializable) {
				serializer.pack(obj, buf.getOutputStream());
				offs = buf.used;
			} else {
				offs = packObject(obj, desc, offs, buf);
			}
		} catch (final Exception x) {
			throw new ODbException(ODbException.ACCESS_VIOLATION, x);
		}
		ObjectHeader.setSize(buf.arr, 0, offs);
		ObjectHeader.setType(buf.arr, 0, desc.getObjectId());
		return buf.arr;
	}

	final int swizzle(final ByteBuffer buf, int offs, final Object obj)
			throws Exception {
		if (obj instanceof IODbObject || obj == null) {
			offs = buf.packI4(offs, swizzle(obj, buf.finalized));
		} else {
			final Class t = obj.getClass();
			if (t == Boolean.class) {
				buf.extend(offs + 5);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpBoolean);
				buf.arr[offs + 4] = (byte) (((Boolean) obj).booleanValue() ? 1
						: 0);
				offs += 5;
			} else if (t == Character.class) {
				buf.extend(offs + 6);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpChar);
				ByteUtils.pack2(buf.arr, offs + 4,
						(short) ((Character) obj).charValue());
				offs += 6;
			} else if (t == Byte.class) {
				buf.extend(offs + 5);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpByte);
				buf.arr[offs + 4] = ((Byte) obj).byteValue();
				offs += 5;
			} else if (t == Short.class) {
				buf.extend(offs + 6);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpShort);
				ByteUtils.pack2(buf.arr, offs + 4, ((Short) obj).shortValue());
				offs += 6;
			} else if (t == Integer.class) {
				buf.extend(offs + 8);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpInt);
				ByteUtils.pack4(buf.arr, offs + 4, ((Integer) obj).intValue());
				offs += 8;
			} else if (t == Long.class) {
				buf.extend(offs + 12);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpLong);
				ByteUtils.pack8(buf.arr, offs + 4, ((Long) obj).longValue());
				offs += 12;
			} else if (t == Float.class) {
				buf.extend(offs + 8);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpFloat);
				ByteUtils.packF4(buf.arr, offs + 4, ((Float) obj).floatValue());
				offs += 8;
			} else if (t == Double.class) {
				buf.extend(offs + 12);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpDouble);
				ByteUtils.packF8(buf.arr, offs + 4,
						((Double) obj).doubleValue());
				offs += 12;
			} else if (t == Date.class) {
				buf.extend(offs + 12);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpDate);
				ByteUtils.pack8(buf.arr, offs + 4, ((Date) obj).getTime());
				offs += 12;
			} else if (t == String.class) {
				offs = buf.packI4(offs, -1 - ClassDescriptor.tpString);
				offs = buf.packString(offs, (String) obj);
			} else if (t == Class.class) {
				offs = buf.packI4(offs, -1 - ClassDescriptor.tpClass);
				offs = buf.packString(offs,
						ClassDescriptor.getClassName((Class) obj));
			} else if (obj instanceof LinkImpl) {
				final LinkImpl link = (LinkImpl) obj;
				link.owner = buf.parent;
				final int len = link.size();
				buf.extend(offs + 8 + len * 4);
				ByteUtils.pack4(buf.arr, offs, -1 - ClassDescriptor.tpLink);
				offs += 4;
				ByteUtils.pack4(buf.arr, offs, len);
				offs += 4;
				for (int j = 0; j < len; j++) {
					ByteUtils.pack4(buf.arr, offs,
							swizzle(link.getRaw(j), buf.finalized));
					offs += 4;
				}
				if (!buf.finalized) {
					link.unpin();
				}
			} else if (obj instanceof Collection
					&& (!serializeSystemCollections || t.getName().startsWith(
							"java.util."))) {
				final ClassDescriptor valueDesc = getClassDescriptor(obj
						.getClass());
				offs = buf.packI4(offs, -ClassDescriptor.tpValueTypeBias
						- valueDesc.getObjectId());
				final Collection c = (Collection) obj;
				offs = buf.packI4(offs, c.size());
				for (final Object elem : c) {
					offs = swizzle(buf, offs, elem);
				}
			} else if (obj instanceof Map
					&& (!serializeSystemCollections || t.getName().startsWith(
							"java.util."))) {
				final ClassDescriptor valueDesc = getClassDescriptor(obj
						.getClass());
				offs = buf.packI4(offs, -ClassDescriptor.tpValueTypeBias
						- valueDesc.getObjectId());
				final Map map = (Map) obj;
				offs = buf.packI4(offs, map.size());
				for (final Object entry : map.entrySet()) {
					final Map.Entry e = (Map.Entry) entry;
					offs = swizzle(buf, offs, e.getKey());
					offs = swizzle(buf, offs, e.getValue());
				}
			} else if (obj instanceof Value) {
				final ClassDescriptor valueDesc = getClassDescriptor(obj
						.getClass());
				offs = buf.packI4(offs, -ClassDescriptor.tpValueTypeBias
						- valueDesc.getObjectId());
				offs = packObject(obj, valueDesc, offs, buf);
			} else if (t.isArray()) {
				final Class elemType = t.getComponentType();
				if (elemType == byte.class) {
					final byte[] arr = (byte[]) obj;
					final int len = arr.length;
					buf.extend(offs + len + 8);
					ByteUtils.pack4(buf.arr, offs, -1
							- ClassDescriptor.tpArrayOfByte);
					ByteUtils.pack4(buf.arr, offs + 4, len);
					System.arraycopy(arr, 0, buf.arr, offs + 8, len);
					offs += 8 + len;
				} else if (elemType == boolean.class) {
					final boolean[] arr = (boolean[]) obj;
					final int len = arr.length;
					buf.extend(offs + len + 8);
					ByteUtils.pack4(buf.arr, offs, -1
							- ClassDescriptor.tpArrayOfBoolean);
					offs += 4;
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int i = 0; i < len; i++) {
						buf.arr[offs++] = (byte) (arr[i] ? 1 : 0);
					}
				} else if (elemType == char.class) {
					final char[] arr = (char[]) obj;
					final int len = arr.length;
					buf.extend(offs + len * 2 + 8);
					ByteUtils.pack4(buf.arr, offs, -1
							- ClassDescriptor.tpArrayOfChar);
					offs += 4;
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int i = 0; i < len; i++) {
						ByteUtils.pack2(buf.arr, offs, (short) arr[i]);
						offs += 2;
					}
				} else if (elemType == short.class) {
					final short[] arr = (short[]) obj;
					final int len = arr.length;
					buf.extend(offs + len * 2 + 8);
					ByteUtils.pack4(buf.arr, offs, -1
							- ClassDescriptor.tpArrayOfShort);
					offs += 4;
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int i = 0; i < len; i++) {
						ByteUtils.pack2(buf.arr, offs, arr[i]);
						offs += 2;
					}
				} else if (elemType == int.class) {
					final int[] arr = (int[]) obj;
					final int len = arr.length;
					buf.extend(offs + len * 4 + 8);
					ByteUtils.pack4(buf.arr, offs, -1
							- ClassDescriptor.tpArrayOfInt);
					offs += 4;
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int i = 0; i < len; i++) {
						ByteUtils.pack4(buf.arr, offs, arr[i]);
						offs += 4;
					}
				} else if (elemType == long.class) {
					final long[] arr = (long[]) obj;
					final int len = arr.length;
					buf.extend(offs + len * 8 + 8);
					ByteUtils.pack4(buf.arr, offs, -1
							- ClassDescriptor.tpArrayOfLong);
					offs += 4;
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int i = 0; i < len; i++) {
						ByteUtils.pack8(buf.arr, offs, arr[i]);
						offs += 8;
					}
				} else if (elemType == float.class) {
					final float[] arr = (float[]) obj;
					final int len = arr.length;
					buf.extend(offs + len * 4 + 8);
					ByteUtils.pack4(buf.arr, offs, -1
							- ClassDescriptor.tpArrayOfFloat);
					offs += 4;
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int i = 0; i < len; i++) {
						ByteUtils.packF4(buf.arr, offs, arr[i]);
						offs += 4;
					}
				} else if (elemType == double.class) {
					final double[] arr = (double[]) obj;
					final int len = arr.length;
					buf.extend(offs + len * 8 + 8);
					ByteUtils.pack4(buf.arr, offs, -1
							- ClassDescriptor.tpArrayOfLong);
					offs += 4;
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int i = 0; i < len; i++) {
						ByteUtils.packF8(buf.arr, offs, arr[i]);
						offs += 8;
					}
				} else if (elemType == Object.class) {
					offs = buf.packI4(offs, -1
							- ClassDescriptor.tpArrayOfObject);
					final Object[] arr = (Object[]) obj;
					final int len = arr.length;
					offs = buf.packI4(offs, len);
					for (int i = 0; i < len; i++) {
						offs = swizzle(buf, offs, arr[i]);
					}
				} else {
					offs = buf.packI4(offs, -1 - ClassDescriptor.tpArrayOfRaw);
					final int len = Array.getLength(obj);
					offs = buf.packI4(offs, len);
					if (elemType.equals(Comparable.class)) {
						offs = buf.packI4(offs, -1);
					} else {
						final ClassDescriptor desc = getClassDescriptor(elemType);
						offs = buf.packI4(offs, desc.getObjectId());
					}
					for (int i = 0; i < len; i++) {
						offs = swizzle(buf, offs, Array.get(obj, i));
					}
				}
			} else if (serializer != null && serializer.isEmbedded(obj)) {
				buf.packI4(offs, -1 - ClassDescriptor.tpCustom);
				serializer.pack(obj, buf.getOutputStream());
				offs = buf.used;
			} else {
				offs = buf.packI4(offs, swizzle(obj, buf.finalized));
			}
		}
		return offs;
	}

	final int packObject(final Object obj, final ClassDescriptor desc,
			int offs, final ByteBuffer buf) throws Exception {
		final ClassDescriptor.FieldDescriptor[] flds = desc.allFields;
		for (int i = 0, n = flds.length; i < n; i++) {
			final ClassDescriptor.FieldDescriptor fd = flds[i];
			final Field f = fd.field;
			switch (fd.type) {
			case ClassDescriptor.tpByte:
				buf.extend(offs + 1);
				buf.arr[offs++] = f.getByte(obj);
				continue;
			case ClassDescriptor.tpBoolean:
				buf.extend(offs + 1);
				buf.arr[offs++] = (byte) (f.getBoolean(obj) ? 1 : 0);
				continue;
			case ClassDescriptor.tpShort:
				buf.extend(offs + 2);
				ByteUtils.pack2(buf.arr, offs, f.getShort(obj));
				offs += 2;
				continue;
			case ClassDescriptor.tpChar:
				buf.extend(offs + 2);
				ByteUtils.pack2(buf.arr, offs, (short) f.getChar(obj));
				offs += 2;
				continue;
			case ClassDescriptor.tpInt:
				buf.extend(offs + 4);
				ByteUtils.pack4(buf.arr, offs, f.getInt(obj));
				offs += 4;
				continue;
			case ClassDescriptor.tpLong:
				buf.extend(offs + 8);
				ByteUtils.pack8(buf.arr, offs, f.getLong(obj));
				offs += 8;
				continue;
			case ClassDescriptor.tpFloat:
				buf.extend(offs + 4);
				ByteUtils.packF4(buf.arr, offs, f.getFloat(obj));
				offs += 4;
				continue;
			case ClassDescriptor.tpDouble:
				buf.extend(offs + 8);
				ByteUtils.packF8(buf.arr, offs, f.getDouble(obj));
				offs += 8;
				continue;
			case ClassDescriptor.tpEnum: {
				final Enum e = (Enum) f.get(obj);
				buf.extend(offs + 4);
				if (e == null) {
					ByteUtils.pack4(buf.arr, offs, -1);
				} else {
					ByteUtils.pack4(buf.arr, offs, e.ordinal());
				}
				offs += 4;
				continue;
			}
			case ClassDescriptor.tpDate: {
				buf.extend(offs + 8);
				final Date d = (Date) f.get(obj);
				final long msec = (d == null) ? -1 : d.getTime();
				ByteUtils.pack8(buf.arr, offs, msec);
				offs += 8;
				continue;
			}
			case ClassDescriptor.tpString:
				offs = buf.packString(offs, (String) f.get(obj));
				continue;
			case ClassDescriptor.tpClass:
				offs = buf.packString(offs,
						ClassDescriptor.getClassName((Class) f.get(obj)));
				continue;
			case ClassDescriptor.tpObject:
				offs = swizzle(buf, offs, f.get(obj));
				continue;
			case ClassDescriptor.tpValue: {
				final Object value = f.get(obj);
				if (value == null) {
					throw new ODbException(ODbException.NULL_VALUE,
							fd.fieldName);
				} else if (value instanceof IODbObject) {
					throw new ODbException(ODbException.SERIALIZE_PERSISTENT);
				}
				offs = packObject(value, fd.valueDesc, offs, buf);
				continue;
			}
			case ClassDescriptor.tpRaw:
				offs = packValue(f.get(obj), offs, buf);
				continue;
			case ClassDescriptor.tpCustom: {
				serializer.pack(f.get(obj), buf.getOutputStream());
				offs = buf.size();
				continue;
			}
			case ClassDescriptor.tpArrayOfByte: {
				final byte[] arr = (byte[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					System.arraycopy(arr, 0, buf.arr, offs, len);
					offs += len;
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfBoolean: {
				final boolean[] arr = (boolean[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++, offs++) {
						buf.arr[offs] = (byte) (arr[j] ? 1 : 0);
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfShort: {
				final short[] arr = (short[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len * 2);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						ByteUtils.pack2(buf.arr, offs, arr[j]);
						offs += 2;
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfChar: {
				final char[] arr = (char[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len * 2);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						ByteUtils.pack2(buf.arr, offs, (short) arr[j]);
						offs += 2;
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfInt: {
				final int[] arr = (int[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len * 4);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						ByteUtils.pack4(buf.arr, offs, arr[j]);
						offs += 4;
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfEnum: {
				final Enum[] arr = (Enum[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len * 4);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						if (arr[j] == null) {
							ByteUtils.pack4(buf.arr, offs, -1);
						} else {
							ByteUtils.pack4(buf.arr, offs, arr[j].ordinal());
						}
						offs += 4;
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfLong: {
				final long[] arr = (long[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len * 8);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						ByteUtils.pack8(buf.arr, offs, arr[j]);
						offs += 8;
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfFloat: {
				final float[] arr = (float[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len * 4);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						ByteUtils.packF4(buf.arr, offs, arr[j]);
						offs += 4;
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfDouble: {
				final double[] arr = (double[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len * 8);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						ByteUtils.packF8(buf.arr, offs, arr[j]);
						offs += 8;
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfDate: {
				final Date[] arr = (Date[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len * 8);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						final Date d = arr[j];
						final long msec = (d == null) ? -1 : d.getTime();
						ByteUtils.pack8(buf.arr, offs, msec);
						offs += 8;
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfString: {
				final String[] arr = (String[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						offs = buf.packString(offs, arr[j]);
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfObject: {
				final Object[] arr = (Object[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4 + len * 4);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						offs = swizzle(buf, offs, arr[j]);
					}
				}
				continue;
			}
			case ClassDescriptor.tpArrayOfValue: {
				final Object[] arr = (Object[]) f.get(obj);
				if (arr == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					final int len = arr.length;
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					final ClassDescriptor elemDesc = fd.valueDesc;
					for (int j = 0; j < len; j++) {
						final Object value = arr[j];
						if (value == null) {
							throw new ODbException(ODbException.NULL_VALUE,
									fd.fieldName);
						}
						offs = packObject(value, elemDesc, offs, buf);
					}
				}
				continue;
			}
			case ClassDescriptor.tpLink: {
				final LinkImpl link = (LinkImpl) f.get(obj);
				if (link == null) {
					buf.extend(offs + 4);
					ByteUtils.pack4(buf.arr, offs, -1);
					offs += 4;
				} else {
					link.owner = buf.parent;
					final int len = link.size();
					buf.extend(offs + 4 + len * 4);
					ByteUtils.pack4(buf.arr, offs, len);
					offs += 4;
					for (int j = 0; j < len; j++) {
						ByteUtils.pack4(buf.arr, offs,
								swizzle(link.getRaw(j), buf.finalized));
						offs += 4;
					}
					if (!buf.finalized) {
						link.unpin();
					}
				}
				continue;
			}
			}
		}
		return offs;
	}

	public ClassLoader setClassLoader(final ClassLoader loader) {
		final ClassLoader prev = loader;
		this.loader = loader;
		return prev;
	}

	public ClassLoader getClassLoader() {
		return loader;
	}

	public void registerClassLoader(final NamedClassLoader loader) {
		if (loaderMap == null) {
			loaderMap = new HashMap();
		}
		loaderMap.put(loader.getName(), loader);
	}

	public ClassLoader findClassLoader(final String name) {
		if (loaderMap == null) {
			return null;
		}
		return (ClassLoader) loaderMap.get(name);
	}

	public void setCustomSerializer(final CustomSerializer serializer) {
		this.serializer = serializer;
	}

	class HashIterator implements ODbObjectIterator, Iterator {
		Iterator oids;

		HashIterator(final HashSet result) {
			oids = result.iterator();
		}

		public Object next() {
			final int oid = ((Integer) oids.next()).intValue();
			return lookupObject(oid, null);
		}

		public int nextObjectid() {
			return oids.hasNext() ? ((Integer) oids.next()).intValue() : 0;
		}

		public boolean hasNext() {
			return oids.hasNext();
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	public Iterator merge(final Iterator[] selections) {
		HashSet result = null;
		for (int i = 0; i < selections.length; i++) {
			final ODbObjectIterator iterator = (ODbObjectIterator) selections[i];
			final HashSet newResult = new HashSet();
			int oid;
			while ((oid = iterator.nextObjectid()) != 0) {
				final Integer oidWrapper = new Integer(oid);
				if (result == null || result.contains(oidWrapper)) {
					newResult.add(oidWrapper);
				}
			}
			result = newResult;
			if (result.size() == 0) {
				break;
			}
		}
		if (result == null) {
			result = new HashSet();
		}
		return new HashIterator(result);
	}

	public Iterator join(final Iterator[] selections) {
		final HashSet result = new HashSet();
		for (int i = 0; i < selections.length; i++) {
			final ODbObjectIterator iterator = (ODbObjectIterator) selections[i];
			int oid;
			while ((oid = iterator.nextObjectid()) != 0) {
				result.add(new Integer(oid));
			}
		}
		return new HashIterator(result);
	}

	public synchronized void registerCustomAllocator(final Class cls,
			final CustomAllocator allocator) {
		synchronized (objectCache) {
			final ClassDescriptor desc = getClassDescriptor(cls);
			desc.allocator = allocator;
			storeObject0(desc, false);
			if (customAllocatorMap == null) {
				customAllocatorMap = new HashMap();
				customAllocatorList = new ArrayList();
			}
			customAllocatorMap.put(cls, allocator);
			customAllocatorList.add(allocator);
			reserveLocation(allocator.getSegmentBase(),
					allocator.getSegmentSize());
		}
	}

	public CustomAllocator createBitmapAllocator(final int quantum,
			final long base, final long extension, final long limit) {
		return new BitmapCustomAllocator(this, quantum, base, extension, limit);
	}

	public int getDatabaseFormatVersion() {
		return header.databaseFormatVersion;
	}

	public void store(final Object obj) {
		if (obj instanceof IODbObject) {
			((IODbObject) obj).store();
		} else {
			synchronized (this) {
				synchronized (objectCache) {
					synchronized (objMap) {
						final ObjectMap.Entry e = objMap.put(obj);
						if ((e.state & ODbObject.RAW) != 0) {
							throw new ODbException(ODbException.ACCESS_TO_STUB);
						}
						storeObject(obj);
						e.state &= ~ODbObject.DIRTY;
					}
				}
			}
		}
	}

	void unassignOid(final Object obj) {
		if (obj instanceof IODbObject) {
			((IODbObject) obj).unassignObjectId();
		} else {
			objMap.remove(obj);
		}
	}

	void assignOid(final Object obj, final int oid, final boolean raw) {
		if (obj instanceof IODbObject) {
			((IODbObject) obj).assignObjectId(this, oid, raw);
		} else {
			synchronized (objMap) {
				final ObjectMap.Entry e = objMap.put(obj);
				e.oid = oid;
				if (raw) {
					e.state = ODbObject.RAW;
				}
			}
		}
		if (listener != null) {
			listener.onObjectAssignOid(obj);
		}
	}

	public void modify(final Object obj) {
		if (obj instanceof IODbObject) {
			((IODbObject) obj).modify();
		} else {
			if (useSerializableTransactions) {
				final ThreadTransactionContext ctx = getTransactionContext();
				if (ctx.nested != 0) { // serializable transaction
					ctx.modified.add(obj);
					return;
				}
			}
			synchronized (this) {
				synchronized (objectCache) {
					synchronized (objMap) {
						final ObjectMap.Entry e = objMap.put(obj);
						if ((e.state & ODbObject.DIRTY) == 0 && e.oid != 0) {
							if ((e.state & ODbObject.RAW) != 0) {
								throw new ODbException(
										ODbException.ACCESS_TO_STUB);
							}
							Conditions
									.assertArgument((e.state & ODbObject.DELETED) == 0);
							storeObject(obj);
							e.state &= ~ODbObject.DIRTY;
						}
					}
				}
			}
		}
	}

	public void invalidate(final Object obj) {
		if (obj instanceof IODbObject) {
			((IODbObject) obj).invalidate();
		} else {
			synchronized (objMap) {
				final ObjectMap.Entry e = objMap.put(obj);
				e.state &= ~ODbObject.DIRTY;
				e.state |= ODbObject.RAW;
			}
		}
	}

	public void load(final Object obj) {
		if (obj instanceof IODbObject) {
			((IODbObject) obj).load();
		} else {
			synchronized (objMap) {
				final ObjectMap.Entry e = objMap.get(obj);
				if (e == null || (e.state & ODbObject.RAW) == 0 || e.oid == 0) {
					return;
				}
			}
			loadObject(obj);
		}
	}

	boolean isLoaded(final Object obj) {
		if (obj instanceof IODbObject) {
			final IODbObject po = (IODbObject) obj;
			return !po.isRaw() && po.isPersistent();
		} else {
			synchronized (objMap) {
				final ObjectMap.Entry e = objMap.get(obj);
				return e != null && (e.state & ODbObject.RAW) == 0
						&& e.oid != 0;
			}
		}
	}

	public int getOid(final Object obj) {
		return (obj instanceof IODbObject) ? ((IODbObject) obj).getObjectId()
				: obj == null ? 0 : objMap.getOid(obj);
	}

	boolean isPersistent(final Object obj) {
		return getOid(obj) != 0;
	}

	boolean isDeleted(final Object obj) {
		return (obj instanceof IODbObject) ? ((IODbObject) obj).isDeleted()
				: obj == null ? false
						: (objMap.getState(obj) & ODbObject.DELETED) != 0;
	}

	boolean recursiveLoading(final Object obj) {
		if (recursiveLoadingPolicyDefined) {
			synchronized (recursiveLoadingPolicy) {
				Class type = obj.getClass();
				do {
					final Object enabled = recursiveLoadingPolicy.get(type);
					if (enabled != null) {
						return ((Boolean) enabled).booleanValue();
					}
				} while ((type = type.getSuperclass()) != null);
			}
		}
		return (obj instanceof IODbObject) ? ((IODbObject) obj)
				.recursiveLoading() : true;
	}

	public boolean setRecursiveLoading(final Class type, final boolean enabled) {
		synchronized (recursiveLoadingPolicy) {
			final Object prevValue = recursiveLoadingPolicy.put(type, enabled);
			recursiveLoadingPolicyDefined = true;
			return prevValue == null ? true : ((Boolean) prevValue)
					.booleanValue();
		}
	}

	public SqlOptimizerParameters getSqlOptimizerParameters() {
		return sqlOptimizerParameters;
	}

	boolean isModified(final Object obj) {
		return (obj instanceof IODbObject) ? ((IODbObject) obj).isModified()
				: obj == null ? false
						: (objMap.getState(obj) & ODbObject.DIRTY) != 0;
	}

	boolean isRaw(final Object obj) {
		return (obj instanceof IODbObject) ? ((IODbObject) obj).isRaw()
				: obj == null ? false
						: (objMap.getState(obj) & ODbObject.RAW) != 0;
	}

	private ObjectMap objMap;

	protected int initIndexSize = dbDefaultInitIndexSize;
	protected int objectCacheInitSize = dbDefaultObjectCacheInitSize;
	protected long extensionQuantum = dbDefaultExtensionQuantum;
	protected String cacheKind = "default";
	protected boolean readOnly = false;
	protected boolean noFlush = false;
	protected boolean lockFile = false;
	protected boolean multiclientSupport = false;
	protected boolean alternativeBtree = false;
	protected boolean backgroundGc = false;
	protected boolean forceStore = false;
	protected boolean reloadObjectsOnRollback = false;
	protected boolean reuseOid = true;
	protected long pagePoolLruLimit = dbDefaultPagePoolLruLimit;
	protected int compatibilityMode = 0;
	protected boolean serializeSystemCollections = true;

	private HashMap customAllocatorMap;
	private ArrayList customAllocatorList;
	private CustomAllocator defaultAllocator;

	SqlOptimizerParameters sqlOptimizerParameters = new SqlOptimizerParameters();
	boolean replicationAck = false;
	boolean concurrentIterator = false;
	int slaveConnectionTimeout = 60; // seconds

	Properties properties = new Properties();

	String encoding = null;

	PagePool pool;
	Header header; // base address of database file mapping
	int dirtyPagesMap[]; // bitmap of changed pages in current index
	boolean modified;

	int currRBitmapPage;// current bitmap page for allocating records
	int currRBitmapOffs;// offset in current bitmap page for allocating
						// unaligned records
	int currPBitmapPage;// current bitmap page for allocating page objects
	int currPBitmapOffs;// offset in current bitmap page for allocating
						// page objects
	Location reservedChain;
	int reservedChainLength;

	CloneNode cloneList;
	boolean insideCloneBitmap;

	int committedIndexSize;
	int currIndexSize;

	int currIndex; // copy of header.root, used to allow read access to the
					// database
					// during transaction commit
	long usedSize; // total size of allocated objects since the beginning of the
					// session
	int[] bitmapPageAvailableSpace;
	boolean opened;

	int[] greyBitmap; // bitmap of visited during GC but not yet marked object
	int[] blackBitmap; // bitmap of objects marked during GC
	long gcThreshold;
	long allocatedDelta;
	boolean gcDone;
	boolean gcActive;
	Object backgroundGcMonitor;
	Object backgroundGcStartMonitor;
	GcThread gcThread;

	int bitmapExtentBase;

	ClassLoader loader;
	HashMap loaderMap;
	HashMap recursiveLoadingPolicy;
	boolean recursiveLoadingPolicyDefined;

	CustomSerializer serializer;

	ODbListener listener;

	long transactionId;
	ODbFile file;

	int nNestedTransactions;
	int nBlockedTransactions;
	int nCommittedTransactions;
	long scheduledCommitTime;
	Object transactionMonitor;
	ODbResource transactionLock;

	final ThreadLocal transactionContext = new ThreadLocal() {
		protected synchronized Object initialValue() {
			return new ThreadTransactionContext();
		}
	};
	boolean useSerializableTransactions;

	ObjectIdHashTable objectCache;
	HashMap classDescMap;
	ClassDescriptor descList;
}

class RootPage {
	long size; // database file size
	long index; // offset to object index
	long shadowIndex; // offset to shadow index
	long usedSize; // size used by objects
	int indexSize; // size of object index
	int shadowIndexSize; // size of object index
	int indexUsed; // userd part of the index
	int freeList; // L1 list of free descriptors
	int bitmapEnd; // index of last allocated bitmap page
	int rootObject; // OID of root object
	int classDescList; // List of class descriptors
	int bitmapExtent; // Offset of extended bitmap pages in object index

	final static int sizeof = 64;
}

class Header {
	int curr; // current root
	boolean dirty; // database was not closed normally
	byte databaseFormatVersion;

	RootPage root[];
	long transactionId;

	final static int sizeof = 3 + RootPage.sizeof * 2 + 8;

	final void pack(final byte[] rec) {
		int offs = 0;
		rec[offs++] = (byte) curr;
		rec[offs++] = (byte) (dirty ? 1 : 0);
		rec[offs++] = databaseFormatVersion;
		for (int i = 0; i < 2; i++) {
			ByteUtils.pack8(rec, offs, root[i].size);
			offs += 8;
			ByteUtils.pack8(rec, offs, root[i].index);
			offs += 8;
			ByteUtils.pack8(rec, offs, root[i].shadowIndex);
			offs += 8;
			ByteUtils.pack8(rec, offs, root[i].usedSize);
			offs += 8;
			ByteUtils.pack4(rec, offs, root[i].indexSize);
			offs += 4;
			ByteUtils.pack4(rec, offs, root[i].shadowIndexSize);
			offs += 4;
			ByteUtils.pack4(rec, offs, root[i].indexUsed);
			offs += 4;
			ByteUtils.pack4(rec, offs, root[i].freeList);
			offs += 4;
			ByteUtils.pack4(rec, offs, root[i].bitmapEnd);
			offs += 4;
			ByteUtils.pack4(rec, offs, root[i].rootObject);
			offs += 4;
			ByteUtils.pack4(rec, offs, root[i].classDescList);
			offs += 4;
			ByteUtils.pack4(rec, offs, root[i].bitmapExtent);
			offs += 4;
		}
		ByteUtils.pack8(rec, offs, transactionId);
		offs += 8;
		Conditions.assertArgument(offs == sizeof);
	}

	final void unpack(final byte[] rec) {
		int offs = 0;
		curr = rec[offs++];
		dirty = rec[offs++] != 0;
		databaseFormatVersion = rec[offs++];
		root = new RootPage[2];
		for (int i = 0; i < 2; i++) {
			root[i] = new RootPage();
			root[i].size = ByteUtils.unpack8(rec, offs);
			offs += 8;
			root[i].index = ByteUtils.unpack8(rec, offs);
			offs += 8;
			root[i].shadowIndex = ByteUtils.unpack8(rec, offs);
			offs += 8;
			root[i].usedSize = ByteUtils.unpack8(rec, offs);
			offs += 8;
			root[i].indexSize = ByteUtils.unpack4(rec, offs);
			offs += 4;
			root[i].shadowIndexSize = ByteUtils.unpack4(rec, offs);
			offs += 4;
			root[i].indexUsed = ByteUtils.unpack4(rec, offs);
			offs += 4;
			root[i].freeList = ByteUtils.unpack4(rec, offs);
			offs += 4;
			root[i].bitmapEnd = ByteUtils.unpack4(rec, offs);
			offs += 4;
			root[i].rootObject = ByteUtils.unpack4(rec, offs);
			offs += 4;
			root[i].classDescList = ByteUtils.unpack4(rec, offs);
			offs += 4;
			root[i].bitmapExtent = ByteUtils.unpack4(rec, offs);
			offs += 4;
		}
		transactionId = ByteUtils.unpack8(rec, offs);
		offs += 8;
		Conditions.assertArgument(offs == sizeof);
	}

}
