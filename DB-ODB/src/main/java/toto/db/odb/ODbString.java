package toto.db.odb;

/**
 * Class encapsulating native Java string. java.lang.String is not persistent
 * object so it can not be stored in ODb as independent persistent object. But
 * sometimes it is needed. This class sole this problem providing implicit
 * conversion operator from java.lang.String to PerisstentString. Also
 * PersistentString class is mutable, allowing to change it's values.
 */
class ODbString extends ODbResource {
	/**
	 * Consutrctor of perisstent string
	 * 
	 * @param str
	 *            Java string
	 */
	public ODbString(final String str) {
		this.str = str;
	}

	private ODbString() {
	}

	/**
	 * Get Java string
	 * 
	 * @return Java string
	 */
	public String toString() {
		return str;
	}

	/**
	 * Append string to the current string value of PersistentString
	 * 
	 * @param tail
	 *            appended string
	 */
	public void append(final String tail) {
		modify();
		str = str + tail;
	}

	/**
	 * Assign new string value to the PersistentString
	 * 
	 * @param str
	 *            new string value
	 */
	public void set(final String str) {
		modify();
		this.str = str;
	}

	/**
	 * Get current string value
	 * 
	 * @return Java string
	 */
	public String get() {
		return str;
	}

	private String str;
}
