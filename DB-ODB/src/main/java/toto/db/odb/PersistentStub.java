package toto.db.odb;

class PersistentStub implements IODbObject {
	public void load() {
		throw new ODbException(ODbException.ACCESS_TO_STUB);
	}

	public void loadAndModify() {
		load();
		modify();
	}

	public final boolean isRaw() {
		return true;
	}

	public final boolean isModified() {
		return false;
	}

	public final boolean isDeleted() {
		return false;
	}

	public final boolean isPersistent() {
		return true;
	}

	public void makePersistent(ODb storage) {
		throw new ODbException(ODbException.ACCESS_TO_STUB);
	}

	public void store() {
		throw new ODbException(ODbException.ACCESS_TO_STUB);
	}

	public void modify() {
		throw new ODbException(ODbException.ACCESS_TO_STUB);
	}

	public PersistentStub(ODb storage, int oid) {
		this.storage = storage;
		this.oid = oid;
	}

	public final int getObjectId() {
		return oid;
	}

	public void deallocate() {
		throw new ODbException(ODbException.ACCESS_TO_STUB);
	}

	public boolean recursiveLoading() {
		return true;
	}

	public final ODb getODB() {
		return storage;
	}

	public boolean equals(Object o) {
		return getODB().getOid(o) == oid;
	}

	public int hashCode() {
		return oid;
	}

	public void onLoad() {
	}

	public void onStore() {
	}

	public void invalidate() {
		throw new ODbException(ODbException.ACCESS_TO_STUB);
	}

	transient ODb storage;
	transient int oid;

	public void unassignObjectId() {
		throw new ODbException(ODbException.ACCESS_TO_STUB);
	}

	public void assignObjectId(ODb storage, int oid, boolean raw) {
		throw new ODbException(ODbException.ACCESS_TO_STUB);
	}

	public Object clone() throws CloneNotSupportedException {
		PersistentStub p = (PersistentStub) super.clone();
		p.oid = 0;
		return p;
	}

	public void readExternal(java.io.ObjectInput s) throws java.io.IOException,
			ClassNotFoundException {
		oid = s.readInt();
	}

	public void writeExternal(java.io.ObjectOutput s)
			throws java.io.IOException {
		s.writeInt(oid);
	}
}
