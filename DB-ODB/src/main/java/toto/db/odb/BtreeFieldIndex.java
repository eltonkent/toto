package toto.db.odb;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.mi.toto.Conditions;
import toto.annotations.DontObfuscate;

class FieldValue implements Comparable<FieldValue> {
	Comparable value;
	Object obj;

	public int compareTo(final FieldValue f) {
		return value.compareTo(f.value);
	}

	FieldValue(final Object obj, final Object value) {
		this.obj = obj;
		this.value = (Comparable) value;
	}
}

@DontObfuscate
class BtreeFieldIndex<T> extends Btree<T> implements FieldIndex<T> {
	String className;
	String fieldName;
	long autoincCount;
	transient Class cls;
	transient Field fld;

	BtreeFieldIndex() {
	}

	private final void locateField() {
		fld = ClassDescriptor.locateField(cls, fieldName);
		if (fld == null) {
			throw new ODbException(ODbException.INDEXED_FIELD_NOT_FOUND,
					className + "." + fieldName);
		}
	}

	public Class getIndexedClass() {
		return cls;
	}

	public Field[] getKeyFields() {
		return new Field[] { fld };
	}

	public void onLoad() {
		cls = ClassDescriptor.loadClass(getODB(), className);
		locateField();
	}

	BtreeFieldIndex(final Class cls, final String fieldName,
			final boolean unique) {
		this(cls, fieldName, unique, 0);
	}

	BtreeFieldIndex(final Class cls, final String fieldName,
			final boolean unique, final long autoincCount) {
		this.cls = cls;
		this.unique = unique;
		this.fieldName = fieldName;
		this.className = ClassDescriptor.getClassName(cls);
		this.autoincCount = autoincCount;
		locateField();
		type = checkType(fld.getType());
	}

	protected Object unpackEnum(final int val) {
		return fld.getType().getEnumConstants()[val];
	}

	private Key extractKey(final Object obj) {
		try {
			final Field f = fld;
			Key key = null;
			switch (type) {
			case ClassDescriptor.tpBoolean:
				key = new Key(f.getBoolean(obj));
				break;
			case ClassDescriptor.tpByte:
				key = new Key(f.getByte(obj));
				break;
			case ClassDescriptor.tpShort:
				key = new Key(f.getShort(obj));
				break;
			case ClassDescriptor.tpChar:
				key = new Key(f.getChar(obj));
				break;
			case ClassDescriptor.tpInt:
				key = new Key(f.getInt(obj));
				break;
			case ClassDescriptor.tpObject: {
				final Object val = f.get(obj);
				key = new Key(val, getODB().makePersistent(val), true);
				break;
			}
			case ClassDescriptor.tpLong:
				key = new Key(f.getLong(obj));
				break;
			case ClassDescriptor.tpDate:
				key = new Key((Date) f.get(obj));
				break;
			case ClassDescriptor.tpFloat:
				key = new Key(f.getFloat(obj));
				break;
			case ClassDescriptor.tpDouble:
				key = new Key(f.getDouble(obj));
				break;
			case ClassDescriptor.tpEnum:
				key = new Key((Enum) f.get(obj));
				break;
			case ClassDescriptor.tpString: {
				final Object val = f.get(obj);
				if (val != null) {
					key = new Key((String) val);
				}
			}
				break;
			default:
				Conditions.failed("Invalid type");
			}
			return key;
		} catch (final Exception x) {
			throw new ODbException(ODbException.ACCESS_VIOLATION, x);
		}
	}

	public boolean add(final T obj) {
		return put(obj);
	}

	public boolean put(final T obj) {
		final Key key = extractKey(obj);
		return key != null && super.insert(key, obj, false) >= 0;
	}

	public T set(final T obj) {
		final Key key = extractKey(obj);
		if (key == null) {
			throw new ODbException(ODbException.KEY_IS_NULL);
		}
		return super.set(key, obj);
	}

	public boolean addAll(final Collection<? extends T> c) {
		final FieldValue[] arr = new FieldValue[c.size()];
		final Iterator<? extends T> e = c.iterator();
		try {
			for (int i = 0; e.hasNext(); i++) {
				final T obj = e.next();
				arr[i] = new FieldValue(obj, fld.get(obj));
			}
		} catch (final Exception x) {
			throw new ODbException(ODbException.ACCESS_VIOLATION, x);
		}
		Arrays.sort(arr);
		for (int i = 0; i < arr.length; i++) {
			add((T) arr[i].obj);
		}
		return arr.length > 0;
	}

	public boolean remove(final Object obj) {
		final Key key = extractKey(obj);
		return key != null && super.removeIfExists(key, obj);
	}

	public boolean containsObject(final T obj) {
		final Key key = extractKey(obj);
		if (key == null) {
			return false;
		}
		if (unique) {
			return super.get(key) != null;
		} else {
			final Object[] mbrs = get(key, key);
			for (int i = 0; i < mbrs.length; i++) {
				if (mbrs[i] == obj) {
					return true;
				}
			}
			return false;
		}
	}

	public boolean contains(final Object obj) {
		final Key key = extractKey(obj);
		if (key == null) {
			return false;
		}
		if (unique) {
			return super.get(key) != null;
		} else {
			final Object[] mbrs = get(key, key);
			for (int i = 0; i < mbrs.length; i++) {
				if (mbrs[i].equals(obj)) {
					return true;
				}
			}
			return false;
		}
	}

	public synchronized void append(final T obj) {
		Key key;
		try {
			switch (type) {
			case ClassDescriptor.tpInt:
				key = new Key((int) autoincCount);
				fld.setInt(obj, (int) autoincCount);
				break;
			case ClassDescriptor.tpLong:
				key = new Key(autoincCount);
				fld.setLong(obj, autoincCount);
				break;
			default:
				throw new ODbException(ODbException.UNSUPPORTED_INDEX_TYPE,
						fld.getType());
			}
		} catch (final Exception x) {
			throw new ODbException(ODbException.ACCESS_VIOLATION, x);
		}
		autoincCount += 1;
		getODB().modify(obj);
		super.insert(key, obj, false);
	}

	public T[] getPrefix(final String prefix) {
		final ArrayList<T> list = getList(new Key(prefix, true), new Key(prefix
				+ Character.MAX_VALUE, false));
		return list.toArray((T[]) Array.newInstance(cls, list.size()));
	}

	public T[] prefixSearch(final String key) {
		final ArrayList<T> list = prefixSearchList(key);
		return list.toArray((T[]) Array.newInstance(cls, list.size()));
	}

	public T[] get(final Key from, final Key till) {
		final ArrayList list = new ArrayList();
		if (root != 0) {
			BtreePage.find((ODbImpl) getODB(), root, checkKey(from),
					checkKey(till), this, height, list);
		}
		return (T[]) list.toArray((T[]) Array.newInstance(cls, list.size()));
	}

	public T[] toArray() {
		final T[] arr = (T[]) Array.newInstance(cls, nElems);
		if (root != 0) {
			BtreePage.traverseForward((ODbImpl) getODB(), root, type, height,
					arr, 0);
		}
		return arr;
	}

	public IterableIterator<T> queryByExample(final T obj) {
		final Key key = extractKey(obj);
		return iterator(key, key, ASCENT_ORDER);
	}

	public IterableIterator<T> select(final String predicate) {
		final Query<T> query = new QueryImpl<T>(getODB());
		return query.select(cls, iterator(), predicate);
	}

	public boolean isCaseInsensitive() {
		return false;
	}
}

@DontObfuscate
class BtreeCaseInsensitiveFieldIndex<T> extends BtreeFieldIndex<T> {
	BtreeCaseInsensitiveFieldIndex() {
	}

	BtreeCaseInsensitiveFieldIndex(final Class cls, final String fieldName,
			final boolean unique) {
		super(cls, fieldName, unique);
	}

	BtreeCaseInsensitiveFieldIndex(final Class cls, final String fieldName,
			final boolean unique, final long autoincCount) {
		super(cls, fieldName, unique, autoincCount);
	}

	Key checkKey(Key key) {
		if (key != null && key.oval instanceof String) {
			key = new Key(((String) key.oval).toLowerCase(), key.inclusion != 0);
		}
		return super.checkKey(key);
	}

	public boolean isCaseInsensitive() {
		return true;
	}
}
