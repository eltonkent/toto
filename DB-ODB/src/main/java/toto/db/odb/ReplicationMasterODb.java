package toto.db.odb;

/**
 * Storage performing replication of changed pages to specified slave nodes.
 */
public interface ReplicationMasterODb extends ODb {
	/**
	 * Get number of currently available slave nodes
	 * 
	 * @return number of online replication slaves
	 */
	public int getNumberOfAvailableHosts();
}
