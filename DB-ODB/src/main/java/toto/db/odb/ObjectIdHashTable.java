package toto.db.odb;

interface ObjectIdHashTable {
	boolean remove(int oid);

	void put(int oid, Object obj);

	Object get(int oid);

	void flush();

	void invalidate();

	void reload();

	void clear();

	int size();

	void setDirty(Object obj);

	void clearDirty(Object obj);
}
