package toto.db.odb;

interface GenericSortArray {
	int size();

	int compare(int i, int j);

	void swap(int i, int j);
}