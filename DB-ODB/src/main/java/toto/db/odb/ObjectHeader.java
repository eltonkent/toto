package toto.db.odb;

import toto.lang.ByteUtils;

class ObjectHeader {
	static final int sizeof = 8;

	static int getSize(byte[] arr, int offs) {
		return ByteUtils.unpack4(arr, offs);
	}

	static void setSize(byte[] arr, int offs, int size) {
		ByteUtils.pack4(arr, offs, size);
	}

	static int getType(byte[] arr, int offs) {
		return ByteUtils.unpack4(arr, offs + 4);
	}

	static void setType(byte[] arr, int offs, int type) {
		ByteUtils.pack4(arr, offs + 4, type);
	}
}
