package toto.db.odb;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import toto.geom2d.RectangleD;

/**
 * Interface of object spatial index. Spatial index is used to allow fast
 * selection of spatial objects belonging to the specified rectangle. Spatial
 * index is implemented using Guttman R-Tree with quadratic split algorithm.
 */
public interface SpatialIndexR2<T> extends IODbObject, Resource, ODbTable<T> {
	/**
	 * Find all objects located in the selected rectangle
	 * 
	 * @param r
	 *            selected rectangle
	 * @return array of objects which enveloping rectangle intersects with
	 *         specified rectangle
	 */
	public Object[] get(RectangleD r);

	/**
	 * Find all objects located in the selected rectangle
	 * 
	 * @param r
	 *            selected rectangle
	 * @return array list of objects which enveloping rectangle intersects with
	 *         specified rectangle
	 */
	public ArrayList<T> getList(RectangleD r);

	/**
	 * Put new object in the index.
	 * 
	 * @param r
	 *            enveloping rectangle for the object
	 * @param obj
	 *            object associated with this rectangle. Object can be not yet
	 *            persistent, in this case its forced to become persistent by
	 *            assigning OID to it.
	 */
	public void put(RectangleD r, T obj);

	/**
	 * Remove object with specified enveloping rectangle from the tree.
	 * 
	 * @param r
	 *            enveloping rectangle for the object
	 * @param obj
	 *            object removed from the index
	 * @exception ODbException
	 *                (StorageError.KEY_NOT_FOUND) exception if there is no such
	 *                key in the index
	 */
	public void remove(RectangleD r, T obj);

	/**
	 * Get wrapping rectangle
	 * 
	 * @return minimal rectangle containing all rectangles in the index,
	 *         <code>null</code> if index is empty
	 */
	public RectangleD getWrappingRectangle();

	/**
	 * Get iterator through all members of the index This iterator doesn't
	 * support remove() method. It is not possible to modify spatial index
	 * during iteration.
	 * 
	 * @return iterator through all objects in the index
	 */
	public Iterator<T> iterator();

	/**
	 * Get entry iterator through all members of the index This iterator doesn't
	 * support remove() method. It is not possible to modify spatial index
	 * during iteration.
	 * 
	 * @return entry iterator which key specifies recrtangle and value -
	 *         correspondent object
	 */
	public IterableIterator<Map.Entry<RectangleD, T>> entryIterator();

	/**
	 * Get objects which rectangle intersects with specified rectangle This
	 * iterator doesn't support remove() method. It is not possible to modify
	 * spatial index during iteration.
	 * 
	 * @param r
	 *            selected rectangle
	 * @return iterator for objects which enveloping rectangle overlaps with
	 *         specified rectangle
	 */
	public IterableIterator<T> iterator(RectangleD r);

	/**
	 * Get entry iterator through objects which rectangle intersects with
	 * specified rectangle This iterator doesn't support remove() method. It is
	 * not possible to modify spatial index during iteration.
	 * 
	 * @param r
	 *            selected rectangle
	 * @return entry iterator for objects which enveloping rectangle overlaps
	 *         with specified rectangle
	 */
	public IterableIterator<Map.Entry<RectangleD, T>> entryIterator(RectangleD r);

	/**
	 * Get iterator through all neighbors of the specified point in the order of
	 * increasing distance from the specified point to the wrapper rectangle of
	 * the object
	 * 
	 * @param x
	 *            x coordinate of the point
	 * @param y
	 *            y coordinate of the point
	 * @return iterator through all objects in the index in the order of
	 *         increasing distance from the specified point
	 */
	public IterableIterator<T> neighborIterator(double x, double y);
}
