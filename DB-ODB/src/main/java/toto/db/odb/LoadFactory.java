package toto.db.odb;

interface LoadFactory {
	Object create(ClassDescriptor desc);
}