//-< JSQLRuntimeException.java >-------------------------------------*--------*
// JSQL                       Version 1.04       (c) 1999  GARRET    *     ?  *
// (Java SQL)                                                        *   /\|  *
//                                                                   *  /  \  *
//                          Created:      9-Dec-2002  K.A. Knizhnik  * / [] \ *
//                          Last update:  9-Dec-2002  K.A. Knizhnik  * GARRET *
//-------------------------------------------------------------------*--------*
// Exception thown by JSQL at runtime
//-------------------------------------------------------------------*--------*

package toto.db.odb;

/**
 * Exception thown by {@link ODbSQL} at runtime which should be ignored and
 * boolean expression caused this exption should be treated as false
 */
public class ODbSQLRuntimeException extends RuntimeException {
	/**
	 * Constructor of exception
	 * 
	 * @param target
	 *            class of the target object in which field was not found
	 * @param fieldName
	 *            name of the locate field
	 */
	public ODbSQLRuntimeException(final String message, final Class target,
			final String fieldName) {
		super(message);
		this.target = target;
		this.fieldName = fieldName;
	}

	/**
	 * Get class in which lookup was performed
	 */
	public Class getTarget() {
		return target;
	}

	/**
	 * Get name of the field
	 */
	public String getFieldName() {
		return fieldName;
	}

	String fieldName;
	Class target;
}
