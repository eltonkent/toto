package toto.db.odb.search;

import java.io.Reader;

import toto.db.odb.IODbObject;


/**
 * Interface for classes which are able to extract text and its language
 * themselves.
 */
public interface FullTextSearchable extends IODbObject {
	/**
	 * Get document text
	 */
	Reader getText();

	/**
	 * Get document language (null if unknown)
	 */
	String getLanguage();
}