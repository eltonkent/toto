package toto.db.odb;

import com.mi.toto.Conditions;
import toto.lang.ByteUtils;

class BtreeKey {
	Key key;
	int oid;
	int oldOid;

	BtreeKey(Key key, int oid) {
		this.key = key;
		this.oid = oid;
	}

	final void getStr(Page pg, int i) {
		int len = BtreePage.getKeyStrSize(pg, i);
		int offs = BtreePage.firstKeyOffs + BtreePage.getKeyStrOffs(pg, i);
		char[] sval = new char[len];
		for (int j = 0; j < len; j++) {
			sval[j] = (char) ByteUtils.unpack2(pg.data, offs);
			offs += 2;
		}
		key = new Key(sval);
	}

	final void getByteArray(Page pg, int i) {
		int len = BtreePage.getKeyStrSize(pg, i);
		int offs = BtreePage.firstKeyOffs + BtreePage.getKeyStrOffs(pg, i);
		byte[] bval = new byte[len];
		System.arraycopy(pg.data, offs, bval, 0, len);
		key = new Key(bval);
	}

	final void extract(Page pg, int offs, int type) {
		byte[] data = pg.data;

		switch (type) {
		case ClassDescriptor.tpBoolean:
			key = new Key(data[offs] != 0);
			break;
		case ClassDescriptor.tpByte:
			key = new Key(data[offs]);
			break;
		case ClassDescriptor.tpShort:
			key = new Key(ByteUtils.unpack2(data, offs));
			break;
		case ClassDescriptor.tpChar:
			key = new Key((char) ByteUtils.unpack2(data, offs));
			break;
		case ClassDescriptor.tpInt:
		case ClassDescriptor.tpObject:
		case ClassDescriptor.tpEnum:
			key = new Key(ByteUtils.unpack4(data, offs));
			break;
		case ClassDescriptor.tpLong:
		case ClassDescriptor.tpDate:
			key = new Key(ByteUtils.unpack8(data, offs));
			break;
		case ClassDescriptor.tpFloat:
			key = new Key(ByteUtils.unpackF4(data, offs));
			break;
		case ClassDescriptor.tpDouble:
			key = new Key(ByteUtils.unpackF8(data, offs));
			break;
		default:
			Conditions.failed("Invalid type");
		}
	}

	final void pack(Page pg, int i) {
		byte[] dst = pg.data;
		switch (key.type) {
		case ClassDescriptor.tpBoolean:
		case ClassDescriptor.tpByte:
			dst[BtreePage.firstKeyOffs + i] = (byte) key.ival;
			break;
		case ClassDescriptor.tpShort:
		case ClassDescriptor.tpChar:
			ByteUtils.pack2(dst, BtreePage.firstKeyOffs + i * 2,
					(short) key.ival);
			break;
		case ClassDescriptor.tpInt:
		case ClassDescriptor.tpObject:
		case ClassDescriptor.tpEnum:
			ByteUtils.pack4(dst, BtreePage.firstKeyOffs + i * 4, key.ival);
			break;
		case ClassDescriptor.tpLong:
		case ClassDescriptor.tpDate:
			ByteUtils.pack8(dst, BtreePage.firstKeyOffs + i * 8, key.lval);
			break;
		case ClassDescriptor.tpFloat:
			ByteUtils.pack4(dst, BtreePage.firstKeyOffs + i * 4,
					Float.floatToIntBits((float) key.dval));
			break;
		case ClassDescriptor.tpDouble:
			ByteUtils.pack8(dst, BtreePage.firstKeyOffs + i * 8,
					Double.doubleToLongBits(key.dval));
			break;
		default:
			Conditions.failed("Invalid type");
		}
		ByteUtils.pack4(dst, BtreePage.firstKeyOffs
				+ (BtreePage.maxItems - i - 1) * 4, oid);
	}
}
