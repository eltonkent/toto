package toto.db.odb;

class ReplicationMasterStorageImpl extends ODbImpl implements
		ReplicationMasterODb {
	public ReplicationMasterStorageImpl(int port, String[] hosts,
			int asyncBufSize, String pageTimestampFile) {
		this.port = port;
		this.hosts = hosts;
		this.asyncBufSize = asyncBufSize;
		this.pageTimestampFile = pageTimestampFile;
	}

	public void open(ODbFile file, long pagePoolSize) {
		super.open(
				asyncBufSize != 0 ? (ReplicationMasterFile) new AsyncReplicationMasterFile(
						this, file, asyncBufSize, pageTimestampFile)
						: new ReplicationMasterFile(this, file,
								pageTimestampFile), pagePoolSize);
	}

	public int getNumberOfAvailableHosts() {
		return ((ReplicationMasterFile) pool.file).getNumberOfAvailableHosts();
	}

	int port;
	String[] hosts;
	int asyncBufSize;
	String pageTimestampFile;
}
