package toto.db.odb;

import toto.db.ToToDBException;

/**
 * Generic Exception throw for common ODB errors.
 */
public class ODbException extends ToToDBException {
	static final int DATABASE_NOT_OPENED = 1;
	static final int DATABASE_ALREADY_OPENED = 2;
	static final int FILE_ACCESS_ERROR = 3;
	static final int KEY_NOT_UNIQUE = 4;
	static final int KEY_NOT_FOUND = 5;
	static final int SCHEMA_CHANGED = 6;
	static final int UNSUPPORTED_TYPE = 7;
	static final int UNSUPPORTED_INDEX_TYPE = 8;
	static final int INCOMPATIBLE_KEY_TYPE = 9;
	static final int NOT_ENOUGH_SPACE = 10;
	static final int DATABASE_CORRUPTED = 11;
	static final int CONSTRUCTOR_FAILURE = 12;
	static final int DESCRIPTOR_FAILURE = 13;
	static final int ACCESS_TO_STUB = 14;
	static final int INVALID_OID = 15;
	static final int DELETED_OBJECT = 16;
	static final int ACCESS_VIOLATION = 17;
	static final int CLASS_NOT_FOUND = 18;
	static final int NULL_VALUE = 19;
	static final int INDEXED_FIELD_NOT_FOUND = 20;
	static final int LOCK_FAILED = 21;
	static final int NO_SUCH_PROPERTY = 22;
	static final int BAD_PROPERTY_VALUE = 23;
	static final int SERIALIZE_PERSISTENT = 24;
	static final int EMPTY_VALUE = 25;
	static final int UNSUPPORTED_ENCODING = 26;
	static final int DATABASE_IS_USED = 27;
	static final int BAD_REPLICATION_PORT = 28;
	static final int CONNECTION_FAILURE = 29;
	static final int TOO_MUCH_OBJECTS = 30;
	static final int FULL_TEXT_INDEX_ERROR = 31;
	static final int KEY_IS_NULL = 32;
	static final int INVALID_OPERATION = 33;
	static final int READ_ONLY_DATABASE = 34;
	static final int NOT_IN_TRANSACTION = 35;
	static final int WRONG_CIPHER_KEY = 36;

	private static final String[] messageText = {
			"",
			"Database not opened",
			"Database already opened",
			"File access error",
			"Key not unique",
			"Key not found",
			"Database schema was changed for",
			"Unsupported type",
			"Unsupported index type",
			"Incompatible key type",
			"Not enough space",
			"Database file is corrupted",
			"Failed to instantiate the object of",
			"Could not build class descriptor. If this is a nested class, ensure its static with a public no-args constructor. for:",
			"Stub object is accessed", "Invalid object reference",
			"Access to the deleted object", "Object access violation",
			"Failed to locate", "Null value", "Could not find indexed field",
			"Lock could not be granted", "No such database property",
			"Bad property value",
			"Attempt to store persistent object as raw object",
			"Attempt to store java.lang.Object as value",
			"Unsupported encoding", "Database is used by other application",
			"Invalid replication node port", "Connection failure",
			"Too much objects", "Failed to insert document in full text index",
			"Index key is null", "Invalid operation",
			"Database is opened in read-only mode",
			"Not within serializable transaction context",
			"Wrong cipher key for the encrypted database" };

	/**
	 * Get exception error code (see definitions above)
	 */
	int getErrorCode() {
		return errorCode;
	}

	/**
	 * Get original exception if DatabaseError excepotion was thrown as the
	 * result of catching some other exception within Database implementation.
	 * DatabaseError is used as wrapper of other exceptions to avoid cascade
	 * propagation of throws and try/catch constructions.
	 * 
	 * @return original exception or <code>null</code> if there is no such
	 *         exception
	 */
	Exception getOriginalException() {
		return origEx;
	}

	ODbException(final int errorCode) {
		super(messageText[errorCode]);
		this.errorCode = errorCode;
	}

	ODbException(final int errorCode, final Exception x) {
		super(messageText[errorCode] + ": " + x, x);
		this.errorCode = errorCode;
		origEx = x;
	}

	ODbException(final int errorCode, final Object param) {
		super(messageText[errorCode] + " " + param);
		this.errorCode = errorCode;
	}

	ODbException(final String message) {
		super(message);
	}

	ODbException(final int errorCode, final Object param, final Exception x) {
		super(messageText[errorCode] + " " + param + ": " + x, x);
		this.errorCode = errorCode;
		origEx = x;
	}

	private int errorCode;
	private Exception origEx;
}
