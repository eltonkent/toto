/**
 * Simple, light weight Object Oriented database.
 * <p>
 * <div>
 * <h3>Using RAGE Object Database</h3>
 * ODB is many times faster than SQLite. It does not require SQL and works with just POJO's. Any java object (that has a publicly accessible no-args constructor) can be saved. It is recommended that  any save objects extend the {@link toto.db.odb.ODbObject} class.<br/>
 * <b>Note:</b>Its is recommended that all objects saved in the ODb not be declared as nested classes. If the need for using embedded classes arises, They should be declared static with a public no-args constructor.
 * 
 * <h4>Features</h4>
 * <ul>
 * <li><b>Automatic Schema adoption:</b> The lifetime of Objects in ODb exceed the lifetime of the application. If a class structure changes (fields added or removed), ODb automatically adopts to the new changes converting them when the object is loaded.</li>
 * <li><b>Utilities for Database back-up and restore</b></li>
 * <li><b>Utilities for Import/Export as XML</b></li>
 * </ul>
 * <h4>Benchmarking</h4>
 * The following table illustrates the performance of ODb against SQLite in milliseconds.
 * <table>
 * <tr>
 * <td><b>Operation</b></td>
 * <td><b>SQLite</b></td>
 * <td><b>RAGE ODb</b></td>
 * <td><b>Criteria</b></td>
 * </tr>
 * <tr>
 * <td>Delete</td>
 * <td>200,459</td>
 * <td>35,678</td>
 * <td>Locate and remove 10,000 records.</td>
 * </tr>
 * <tr>
 * <td>Insert</td>
 * <td>280,103</td>
 * <td>17,367</td>
 * <td>Loading 10,000 simple records into the database.</td>
 * </tr>
 * <tr>
 * <td>Search</td>
 * <td>93,605</td>
 * <td>12,382</td>
 * <td>Seek 10,000 records via indexes.</td>
 * </tr>
 * </table>
 * <h4>Creating/Opening a Object Database:</h4>
 *
 * Creating a database is as simple as invoking the ODbFactory and then providing the path to the embedded database.
 * <pre>
 * ODb db = ODbFactory.createOODB();
 * <span>//Create or open an existing database. providing null for the database path creates an in-memory database.</span>
 * db.open("\sdcard\myapp\mydb.odb");
 * </pre>
 * <h4>Setting a root object</h4>
 * After the ODb instance is opened, the database can access persistent objects from it.ODb uses a concept of Root object to make the application have reference to saved objects. ODb can have only one root object and is obtained using the {@link toto.db.odb.ODb#getRootObject()} method. ODb returns null for this method if there is no root object registered yet for a database. In that case, we should create a new root object using the {@link ODb#setRootObject(Object)} method. Once the root object is set, the {@link toto.db.odb.ODb#getRootObject()} will always return a reference to this object and it contains references to other objects saved in ODb.
 * <pre>
 * SimpleRootClass root=(SimpleRootClass)db.getRootObject();
 * if(root==null){
 * 		root=new SimpleRootClass();
 * 		db.setRootObject(root);
 * }
 * </pre>
 * You can also set a {@link toto.db.odb.Index} as a root object. In this instance, we use a String index key as a root index.
 * <pre>
 * Index stringIdx=(Index)db.getRootObject();
 * if(stringIdx==null){
 * 		<font color="green">//Here String is the key type and true indicates that its a unique key.</font>
 * 		stringIdx=db.createIndex(String.class,true);
 * 		db.setRootObject(stringIdx);
 * }
 * <span>//then save objects using string keys</span>
 * stringIdx.put("teamscore",new TeamScore());
 * </pre>
 * <h4>Setting a root object with multiple key indices</h4>
 * You can also set a root object with multiple key indices by specifying the fields to be used as indices.
 * <b>Sample Root Class</b><br/>
 * Its important (for performance reasons) for the root object to extend {@link toto.db.odb.ODbObject}
 * <pre>
 * 
 * <span>//Just any class with an integer that can be used as its local key</span>
 * class IntKey extends OdbObject{
 * 		public int <font color="red">intKey</font>; // integer key
 * 		.. some other field members
 * }
 * <span>//Just any class with a string that can be used as its local key</span>
 * class StringKey extends OdbObject{
 * 		public String <font color="red">strKey</font>;
 * 		.. some other field members
 * }
 * <span>//Just any class with an string that can be used as its foreign key</span>
 * class ExportKey extends OdbObject{
 * 		public String <font color="red">exportKey</font>;
 * 		.. some other field members
 * }
 * <span>// now create the root class</span>
 * class RootClass extends OdbObject{
 * 		<span>// index on IntKey.intKey</span>
 * 		public FieldIndex<IntKey> intKeyIndex;
 * 		<span>// index on StringKey.strKey</span>
 *		public FieldIndex<StringKey> strKeyIndex
 *		<span>// index on ExportKey, which key doesn�t
 *		// belong to the class</span>
 *		public Index<ExportKey> foreignIndex;
 * 		public RootClass(ODb db){
 * 			<span>//call super with ODb instance</span>
 * 			super(db);
 * 			<span>//create an index of the intkey class. Its important to note here that the name of field "intKey"
 * 			// should be the same as the one in the class. The true parameter indicates a unique index.</span>
 * 			intKeyIndex = db.<IntKey>createFieldIndex(IntKey.class, "<font color="red">intKey</font>",true); 
 * 			<span>//here false indicates that the index allows duplicates</span>
 * 			strKeyIndex = db.<StringKey>createFieldIndex(StringKey.class, "<font color="red">strKey</font>",false); 
 * 			foreignIndex = db.<ExportKey>createIndex(ExportKey.class,false);
 * 		}
 * }
 * 
 * <span><b>// now setting the root object and saving data</b></span>
 * ..
 * RootClass root=(RootClass)db.getRootObject();
 * if(root==null){
 * 		root=new RootClass(db);
 * 		db.setRootObject(root);
 * }
 * <span>//now save data to their corresponding indices.</span>
 * IntKey intData=new IntKey();
 * intData.intKey=10;
 * <span>
 * ..set any other data
 * ..now add it to the root index</span>
 * root.intKeyIndex.put(intData);
 * 
 * StringKey strData=new StringKey();
 * strData.strKey="this is test data";
 * <span>
 * ..set any other data
 * ..now add it to the root index</span>
 * root.strKeyIndex.put(strData);
 * root.foreignIndex.put(new Key(1001), new ExportKey());
 * <span>//close the database</span>
 * db.close();
 * </pre>
 * 
 * <h4>Iterating through an index</h4>
 *  In the above examples, we created both single and multiple indices root objects. once we set a root object index, we can iterate through them using the {@link java.util.Iterator} class.
 * <pre>
 * 	<span>//for single index</span>
 * 	Iterator i=root.iterator();
 *  <span>//for multiple indices</span>
 *  Iterator i = root.<font color="red">intIndex</font>.iterator();
 *  while (i.hasNext()) {
 *  	IntKey obj = (IntKey)i.next();
 *  	i.remove(); <span>// exclude object from index</span>
 *  	obj.deallocate(); <span>// deallocate object</span>
 *  }
 * </pre>
 * <h4>Searching through an Index</h4>
 * In order to search through a list of objects, we need to specify a range of keys to search through.
 * <pre>
 * <span>//the equivalent of "select * from ... where"</span>
 * List<RootClass> result=strKeyIndex.getList(""this is test data"",""this is test data"");
 * </pre>
 * <h4>Removing Objects</h4>
 * The {@link toto.db.odb.Index#remove(Object)} method is used to remove an object associated with an index.
 * <pre>
 *  <span>//removing objects associated with key "this is test data"</span>
 *  strKeyIndex.remove("this is test data");
 *  db.commit();
 * </pre>
 * <h4>Creating compound indices</h4>
 * Often applications require a compound index or a key that supports several values. ODb supports compound and multi-field indices.
 * <pre>
 *  <span>//Create the compound index and assign as root object.</span>
 *  Index<Person> compoundIndex=db.&lt;Person&gt;createIndex(new Class[]{String.class,String.class},true);
 *  <span>//set as root</span>
 *  db.setRootObject(compoundIndex);
 *  <span>//adding object to compound index</span>
 *  compoundIndex.put(new Key(new Object[]{"john","mayer"}),new Person());
 * </pre>
 * <h4>Creating multi-field indices</h4>
 * <pre>
 * FieldIndex&lt;Person&gt; multifieldIndex= db.&lt;Person&gt;createFieldIndex(Person.class, <span>// indexed class</span> new String[] { <span>// key consists of two string fields</span>"lastName", "firstName"},true); <span>// index is unique</span>
 * Person person = new Person("john", "mayer");
 * multifieldIndex.put(person);
 * </pre>
 * <h4>Performing multi/compound index iteration</h4>
 * Specify the values of all the key components.
 * <pre>
 * <span>// Get person by last and first name</span>
 * Person person = compoundIndex.get(new Key(new object[] { "john","mayer" }));
 * <span>// Locate all persons with specified last name</span>
 * Key lastName = new Key((new object[] { "mayer" });
 * Iterator<Person> iterator= multifieldIndex.iterator(lastName, <span>// low boundary(default is inclusive)</span>lastName, <span>// high boundary (also inclusive)</span>Index.ASCENT_ORDER);
 * while (iterator.hasNext()) {
 * 		person = iterator.next();
 * }
 * </pre>
 * <h4>multi/compound index search</h4>
 * There are two ways search through a multi/compound index.
 * <h5>1. Creating a Multi-Dimensional Comparator</h5>
 * Its as simple as extending the {@link toto.db.odb.MultidimensionalComparator} class and overriding {@link toto.db.odb.MultidimensionalComparator#compare(Object, Object, int)}.
 * <pre>
 * static class Stock extends ODbObject {
 * 		String symbol;
 * 		float price;
 * 		int volume;
 * }
 * 
 * static class StockComparator extends MultidimensionalComparator<Stock>{
 * public int compare(Stock s1, Stock s2, int component) {
 * switch (component) {
 * case 0: <span>// Stock.symbol</span>
 * if (s1.symbol == null && s2.symbol == null) {
 * 	return EQ;
 * } else if (s1.symbol == null) {
 * 	return LEFT_UNDEFINED;
 * } else if (s2.symbol == null) {
 * 	return RIGHT_UNDEFINED;
 * } else {
 * int diff = s1.symbol.compareTo(s2.symbol);
 * return diff < 0 ? LT : diff == 0 ? EQ : GT;
 * }
 * case 1: <span>// Stock.price</span>
 * return s1.price < s2.price ? LT : s1.price == s2.price ? EQ : GT;
 * case 1: <span>// Stock.price</span>
 * return s1.price < s2.price? LT : s1.price == s2.price ? EQ : GT;
 * case 2: <span>// Stock.volume</span>
 * return s1.volume < s2.volume? LT : s1.volume == s2.volume ? EQ : GT;
 * default:
 * throw new IllegalArgumentException();
 * }
 * }
 * public int getNumberOfDimensions() {
 * 	return 3;
 * }
 * public Stock cloneField(Stock src,int component) {
 * Stock clone = new Stock();
 * switch (component) {
 * case 0: <span>// Stock.symbol</span>
 * clone.symbol = src.symbol;
 * break;
 * case 1: <span>// Stock.price</span>
 * clone.price = src.price;
 * break;
 * case 2: <span>// Stock.volume</span>
 * clone.volume = src.volume;
 * break;
 * default:
 * throw new IllegalArgumentException();
 * }
 * return clone;
 * }
 * }
 * <b><span>Using the Comparator</span></b>
 * MultidimensionalIndex<Stock> index= db.<Stock>createMultidimensionalIndex(new StockComparator());
 * </pre>
 * <h5>1. Using Reflection to generate a comparator</h5>
 * <pre>
 * static class Quote extends ODbObject{
 * int timestamp;
 * float low;
 * float high;
 * float open;
 * float close;
 * int volume;
 * }
 * <span>...</span>
 * MultidimensionalIndex&lt;Quote&gt; index= db.&lt;Quote&gt;createMultidimensionalIndex(Quote.class, // class of index elementsnew String[] { <span>// list of searchable fields</span> "low", "high", "open", "close", "volume"},false); <span>// do not treat 0 as undefined value</span>
 * </pre>
 * <h4>Query by Example</h4>
 * <pre>
 * class Car {
 *  String model;
 *  String make;
 *  String color;
 *  int mileage;
 *  int price;
 *  int productionYear;
 *  boolean airCondition;
 *  boolean automatic;
 *  boolean navigationSystem;
 * }
 * <span>//the following code searches for green Fords</span>
 * Car pattern = new Car();
 * pattern.make = "Ford";
 * pattern.color = "green";
 * ArrayList<Car> cars = index.queryByExample(pattern);
 * </pre>
 * <h4>Transactions on ODb</h4>
 * Transactions are a fundamental mechanism of database systems, and serve two main goals: enforcing database consistency, and providing concurrent access to the database by multiple clients. The transaction body is the atomic sequence of logically related operations that should all be accepted together, or rejected together.
 * Odb uses a shadow object transaction mechanism. When the application modifies an object, this process does not rewrite the object directly; rather, a copy of the object is created and updated. During transaction commit, originals of the objects are replaced with their updated copies.
 * There are different types of transactions models
 * <h5>Synchronized Access</h5>
 * Using the Synchronized keyword to perform ODb operations
 * <h5>Cooperative transactions</h5>
 * Multiple threads can share a transaction, and all of them must �see� the others� changes. Committing a transaction in this mode stores to disk all changes made by the threads, and a rollback of the transaction undoes the work of all threads.
 * <h5>Exclusive per-thread transactions</h5>
 * This mode, in which each thread accesses the database exclusively, is the safest, because unintended interaction between threads is impossible. No locking is needed. But it represents the worst case for concurrency, since at any time, only one transaction can be executed (even if it is a read-only transaction).
 * 
 * Applications with few concurrent activities needing access to the persistent data.
 * <pre>
 * db.beginThreadTransaction(Odb.EXCLUSIVE_TRANSACTION);
 * try {
 *  <span>// do something
 *  // commit changes in case of normal completion</span>
 *  db.endThreadTransaction();
 * } catch (Exception x) {
 *  <span>// rollback transaction in case of exception</span>
 *  db.rollbackThreadTransaction();
 * }
 * </pre>
 * <h5>Serializable per-thread transactions</h5>
 * The notion of serializable transactions means that transactions executing in parallel do not overlap or affect each other� outcomes. In other words, it means that each transaction can work as if it is the only transaction accessing the database.
 * it is the programmer's responsibility to set proper locks.
 * Serializable transaction should be started using
 * <pre>
 * db.beginThreadTransaction(ODb.SERIALIZABLE_TRANSACTION)
 * </pre>
 * and finished either by
 * <pre>
 * db.endThreadTransaction() 
 * </pre>
 * or
 * <pre>
 * db.rollbackThreadTransaction();
 * </pre>
 * <h4>Using ODbSQL</h4>
 * The {@link toto.db.odb.ODbSQL} class can be termed a relational database wrapper because it provides associations between an RDBMS table and a Java class, as well as between row and object instances.it is possible to mark fields of the class
 * for which indexes should be created using the {@link toto.db.odb.Indexable} annotation.
 * 
 * <b>Converting {@link toto.db.odb.ODb} to {@link toto.db.odb.ODbSQL} is simple</b>
 * <pre>
 * ODbSQL sqlDb=new ODbSQL(db);
 * </pre>
 * 
 * <pre>
 * class Record extends Persistent {
 *  {@link toto.db.odb.Indexable}(unique=true, caseInsensitive=true)
 *  String key;
 *  String value;
 * }
 * </pre>
 * <h5>Creating a ODbSQL select query</h5>
 * <pre>
 * for (Record rec : sqlDb.<Record>select(Record.class,"key like 'ABC%'"))
 * {
 * 	System.out.println(rec);
 * }
 * </pre>
 * If a query must be executed multiple times, the application can prepare the query in order to reduce query parsing overhead for each execution. A prepared query can contain positioned parameters (specified by the '?' character)
 * <pre>
 * Query<Record> query = sqlDb.<Record>prepare(Record.class,"key=?");
 * <span>// '?' is parameter placeholder</span>
 * for (int i = 1; i < 1000; i++) {
 *  query.setIntParameter(1, i); <span>// bind parameter</span>
 *  Record rec = query.execute().next();
 *  rec.doSomething();
 * }
 * </pre>
 * <h5>Creating a Update query</h5>
 * To update or delete selected records, perform a �select for update� and pass true as the second optional parameter forUpdate of the select method.
 * <pre>
 * Iterator<Record> i = sqlDb.<Record>select(Record.class,<span> // target</span> table"key='1'", <span>// selection</span> predicatetrue); <span>// select for update</span>
 * if (i.hasNext()) { <span>// if record is found</span>
 *  Record rec = i.next();
 *  <span>// exclude if from index before update</span>
 *  sqlDb.excludeFromIndex(rec, "key");
 *  rec.key = "2"; <span>// update record</span>
 *  rec.modify(); <span>// mark record as modified</span>
 *  sqlDb.includeInIndex(rec, "key"); <span>// reinsert in index</span>
 * }
 * </pre>
 * <h5>Creating a Delete query</h5>
 * <pre>
 * Record rec = sqlDb.<Record>select(Record.class, "key='2'",true).next();
 * sqlDb.deleteRecord(rec);
 * </pre>
 * <h4>Using Text ObSQL</h4>
 * It provides almost the same syntax as standard SQL. The main difference is that a  {@link toto.db.odb.ODbSQL} query returns a set of objects, not tuples. In addition, {@link toto.db.odb.ODbSQL} doesn't support joins, nested selects, grouping and aggregate functions. When defining a ObSQL query, it is not necessary to specify a "select ... from ...
 * where" clause because, as mentioned above, {@link toto.db.odb.ODbSQL} always selects objects, and the relevant table is specified as a parameter of the select method.
 * 
 * <pre>
 * sqlDb.select(Person.class, "age > 30 and salary < 100000");
 * sqlDb.select(Detail.class, "color='grey' order by price");
 * sqlDb.select(Order.class, "detail.delivery between '01/01/2008'" + "and '02/01/2008'");
 * sqlDb.select(Book.class, "title like '%DBMS%'");
 * sqlDb.select(Company.class,"exists i: (contract[i].company.location = 'US')");
 * </pre>
 * <h4>Using ODb Collections</h4>
 * If the need to save collection based objects to ODb arises, the collection implementations of ODb should be used. The following collection types have been implemented in ODb.
 * <ul>
 * <li>{@link toto.db.odb.ODbList} - {@link toto.db.odb.ODb#createList()}</li>
 * <li>{@link toto.db.odb.ODbMap} - {@link toto.db.odb.ODb#createMap(Class)}</li>
 * <li>{@link toto.db.odb.ODbSet} - {@link toto.db.odb.ODb#createSet()}</li>
 * <li>{@link toto.db.odb.ODbTable}</li>
 * <li>{@link toto.db.odb.ODbBitIndex}</li>
 * <li>{@link toto.db.odb.OdbSortedCollection}- {@link toto.db.odb.ODb#createSortedCollection(ODbComparator, boolean)}</li>
 * </ul> 
 * <h5>Creating and saving an ODBList</h5>
 * <pre>
 * <span>//in this instance, we are saving an ODbList as the root object. </span>
 * ODbList<ListRecord> root = (ODbList) db.getRootObject();
 * if (root == null) {
 * 		root = db.<ListRecord>createList();
 * 		db.setRootObject(root);
 * 	}
 * <span>//Add some sample data </span>
 * 	for (i = 0; i < nRecords / 2; i++) {
 * 		ListRecord rec = new ListRecord();
 * 		rec.test = RandomString.randomAlphanumeric(100);
 * 		root.add(rec);
 * 	}
 * <span>//commit </span>
 * db.commit();
 * </pre>
 * </div>
 * </p>
 */
package toto.db.odb;

