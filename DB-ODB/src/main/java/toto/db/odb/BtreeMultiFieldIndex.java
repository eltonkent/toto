package toto.db.odb;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import com.mi.toto.Conditions;
import toto.annotations.DontObfuscate;
import toto.lang.ByteUtils;

class MultiFieldValue implements Comparable<MultiFieldValue> {
	Comparable[] values;
	Object obj;

	public int compareTo(final MultiFieldValue f) {
		for (int i = 0; i < values.length; i++) {
			final int diff = values[i].compareTo(f.values[i]);
			if (diff != 0) {
				return diff;
			}
		}
		return 0;
	}

	MultiFieldValue(final Object obj, final Comparable[] values) {
		this.obj = obj;
		this.values = values;
	}
}

class BtreeMultiFieldIndex<T> extends Btree<T> implements FieldIndex<T> {
	String className;
	String[] fieldName;
	int[] types;

	transient Class cls;
	transient Field[] fld;

	BtreeMultiFieldIndex() {
	}

	BtreeMultiFieldIndex(final Class cls, final String[] fieldName,
			final boolean unique) {
		this.cls = cls;
		this.unique = unique;
		this.fieldName = fieldName;
		this.className = ClassDescriptor.getClassName(cls);
		locateFields();
		type = ClassDescriptor.tpArrayOfByte;
		types = new int[fieldName.length];
		for (int i = 0; i < types.length; i++) {
			types[i] = checkType(fld[i].getType());
		}
	}

	private final void locateFields() {
		fld = new Field[fieldName.length];
		for (int i = 0; i < fieldName.length; i++) {
			fld[i] = ClassDescriptor.locateField(cls, fieldName[i]);
			if (fld[i] == null) {
				throw new ODbException(ODbException.INDEXED_FIELD_NOT_FOUND,
						className + "." + fieldName[i]);
			}
		}
	}

	public Class getIndexedClass() {
		return cls;
	}

	public Field[] getKeyFields() {
		return fld;
	}

	public void onLoad() {
		cls = ClassDescriptor.loadClass(getODB(), className);
		locateFields();
	}

	int compareByteArrays(final byte[] key, final byte[] item, final int offs,
			final int lengtn) {
		int o1 = 0;
		int o2 = offs;
		final byte[] a1 = key;
		final byte[] a2 = item;
		for (int i = 0; i < fld.length && o1 < key.length; i++) {
			int diff = 0;
			switch (types[i]) {
			case ClassDescriptor.tpBoolean:
			case ClassDescriptor.tpByte:
				diff = a1[o1++] - a2[o2++];
				break;
			case ClassDescriptor.tpShort:
				diff = ByteUtils.unpack2(a1, o1) - ByteUtils.unpack2(a2, o2);
				o1 += 2;
				o2 += 2;
				break;
			case ClassDescriptor.tpChar:
				diff = (char) ByteUtils.unpack2(a1, o1)
						- (char) ByteUtils.unpack2(a2, o2);
				o1 += 2;
				o2 += 2;
				break;
			case ClassDescriptor.tpInt:
			case ClassDescriptor.tpObject:
			case ClassDescriptor.tpEnum: {
				final int i1 = ByteUtils.unpack4(a1, o1);
				final int i2 = ByteUtils.unpack4(a2, o2);
				diff = i1 < i2 ? -1 : i1 == i2 ? 0 : 1;
				o1 += 4;
				o2 += 4;
				break;
			}
			case ClassDescriptor.tpLong:
			case ClassDescriptor.tpDate: {
				final long l1 = ByteUtils.unpack8(a1, o1);
				final long l2 = ByteUtils.unpack8(a2, o2);
				diff = l1 < l2 ? -1 : l1 == l2 ? 0 : 1;
				o1 += 8;
				o2 += 8;
				break;
			}
			case ClassDescriptor.tpFloat: {
				final float f1 = Float
						.intBitsToFloat(ByteUtils.unpack4(a1, o1));
				final float f2 = Float
						.intBitsToFloat(ByteUtils.unpack4(a2, o2));
				diff = f1 < f2 ? -1 : f1 == f2 ? 0 : 1;
				o1 += 4;
				o2 += 4;
				break;
			}
			case ClassDescriptor.tpDouble: {
				final double d1 = Double.longBitsToDouble(ByteUtils.unpack8(a1,
						o1));
				final double d2 = Double.longBitsToDouble(ByteUtils.unpack8(a2,
						o2));
				diff = d1 < d2 ? -1 : d1 == d2 ? 0 : 1;
				o1 += 8;
				o2 += 8;
				break;
			}
			case ClassDescriptor.tpString: {
				final int len1 = ByteUtils.unpack4(a1, o1);
				final int len2 = ByteUtils.unpack4(a2, o2);
				o1 += 4;
				o2 += 4;
				int len = len1 < len2 ? len1 : len2;
				while (--len >= 0) {
					diff = (char) ByteUtils.unpack2(a1, o1)
							- (char) ByteUtils.unpack2(a2, o2);
					if (diff != 0) {
						return diff;
					}
					o1 += 2;
					o2 += 2;
				}
				diff = len1 - len2;
				break;
			}
			case ClassDescriptor.tpArrayOfByte: {
				final int len1 = ByteUtils.unpack4(a1, o1);
				final int len2 = ByteUtils.unpack4(a2, o2);
				o1 += 4;
				o2 += 4;
				int len = len1 < len2 ? len1 : len2;
				while (--len >= 0) {
					diff = a1[o1++] - a2[o2++];
					if (diff != 0) {
						return diff;
					}
				}
				diff = len1 - len2;
				break;
			}
			default:
				Conditions.failed("Invalid type");
			}
			if (diff != 0) {
				return diff;
			}
		}
		return 0;
	}

	String convertString(final Object s) {
		return (String) s;
	}

	Object unpackByteArrayKey(final Page pg, final int pos) {
		int offs = BtreePage.firstKeyOffs + BtreePage.getKeyStrOffs(pg, pos);
		final byte[] data = pg.data;
		final Object values[] = new Object[fld.length];

		for (int i = 0; i < fld.length; i++) {
			Object v = null;
			switch (types[i]) {
			case ClassDescriptor.tpBoolean:
				v = Boolean.valueOf(data[offs++] != 0);
				break;
			case ClassDescriptor.tpByte:
				v = new Byte(data[offs++]);
				break;
			case ClassDescriptor.tpShort:
				v = new Short(ByteUtils.unpack2(data, offs));
				offs += 2;
				break;
			case ClassDescriptor.tpChar:
				v = new Character((char) ByteUtils.unpack2(data, offs));
				offs += 2;
				break;
			case ClassDescriptor.tpInt:
				v = new Integer(ByteUtils.unpack4(data, offs));
				offs += 4;
				break;
			case ClassDescriptor.tpObject: {
				final int oid = ByteUtils.unpack4(data, offs);
				v = oid == 0 ? null : ((ODbImpl) getODB()).lookupObject(oid,
						null);
				offs += 4;
				break;
			}
			case ClassDescriptor.tpLong:
				v = new Long(ByteUtils.unpack8(data, offs));
				offs += 8;
				break;
			case ClassDescriptor.tpEnum:
				v = fld[i].getType().getEnumConstants()[ByteUtils.unpack4(data,
						offs)];
				offs += 4;
				break;
			case ClassDescriptor.tpDate: {
				final long msec = ByteUtils.unpack8(data, offs);
				v = msec == -1 ? null : new Date(msec);
				offs += 8;
				break;
			}
			case ClassDescriptor.tpFloat:
				v = new Float(Float.intBitsToFloat(ByteUtils
						.unpack4(data, offs)));
				offs += 4;
				break;
			case ClassDescriptor.tpDouble:
				v = new Double(Double.longBitsToDouble(ByteUtils.unpack8(data,
						offs)));
				offs += 8;
				break;
			case ClassDescriptor.tpString: {
				final int len = ByteUtils.unpack4(data, offs);
				offs += 4;
				final char[] sval = new char[len];
				for (int j = 0; j < len; j++) {
					sval[j] = (char) ByteUtils.unpack2(data, offs);
					offs += 2;
				}
				v = new String(sval);
				break;
			}
			case ClassDescriptor.tpArrayOfByte: {
				final int len = ByteUtils.unpack4(data, offs);
				offs += 4;
				final byte[] bval = new byte[len];
				System.arraycopy(data, offs, bval, 0, len);
				offs += len;
				break;
			}
			default:
				Conditions.failed("Invalid type");
			}
			values[i] = v;
		}
		return values;
	}

	private Key extractKey(final Object obj) {
		try {
			final ByteBuffer buf = new ByteBuffer();
			int dst = 0;
			for (int i = 0; i < fld.length; i++) {
				final Field f = fld[i];
				switch (types[i]) {
				case ClassDescriptor.tpBoolean:
					buf.extend(dst + 1);
					buf.arr[dst++] = (byte) (f.getBoolean(obj) ? 1 : 0);
					break;
				case ClassDescriptor.tpByte:
					buf.extend(dst + 1);
					buf.arr[dst++] = f.getByte(obj);
					break;
				case ClassDescriptor.tpShort:
					buf.extend(dst + 2);
					ByteUtils.pack2(buf.arr, dst, f.getShort(obj));
					dst += 2;
					break;
				case ClassDescriptor.tpChar:
					buf.extend(dst + 2);
					ByteUtils.pack2(buf.arr, dst, (short) f.getChar(obj));
					dst += 2;
					break;
				case ClassDescriptor.tpInt:
					buf.extend(dst + 4);
					ByteUtils.pack4(buf.arr, dst, f.getInt(obj));
					dst += 4;
					break;
				case ClassDescriptor.tpObject: {
					final Object p = f.get(obj);
					buf.extend(dst + 4);
					ByteUtils.pack4(buf.arr, dst, getODB().makePersistent(p));
					dst += 4;
					break;
				}
				case ClassDescriptor.tpLong:
					buf.extend(dst + 8);
					ByteUtils.pack8(buf.arr, dst, f.getLong(obj));
					dst += 8;
					break;
				case ClassDescriptor.tpDate: {
					final Date d = (Date) f.get(obj);
					buf.extend(dst + 8);
					ByteUtils.pack8(buf.arr, dst, d == null ? -1 : d.getTime());
					dst += 8;
					break;
				}
				case ClassDescriptor.tpFloat:
					buf.extend(dst + 4);
					ByteUtils.pack4(buf.arr, dst,
							Float.floatToIntBits(f.getFloat(obj)));
					dst += 4;
					break;
				case ClassDescriptor.tpDouble:
					buf.extend(dst + 8);
					ByteUtils.pack8(buf.arr, dst,
							Double.doubleToLongBits(f.getDouble(obj)));
					dst += 8;
					break;
				case ClassDescriptor.tpEnum:
					buf.extend(dst + 4);
					ByteUtils
							.pack4(buf.arr, dst, ((Enum) f.get(obj)).ordinal());
					dst += 4;
					break;
				case ClassDescriptor.tpString: {
					buf.extend(dst + 4);
					final String str = convertString(f.get(obj));
					if (str != null) {
						final int len = str.length();
						ByteUtils.pack4(buf.arr, dst, len);
						dst += 4;
						buf.extend(dst + len * 2);
						for (int j = 0; j < len; j++) {
							ByteUtils
									.pack2(buf.arr, dst, (short) str.charAt(j));
							dst += 2;
						}
					} else {
						ByteUtils.pack4(buf.arr, dst, 0);
						dst += 4;
					}
					break;
				}
				case ClassDescriptor.tpArrayOfByte: {
					buf.extend(dst + 4);
					final byte[] arr = (byte[]) f.get(obj);
					if (arr != null) {
						final int len = arr.length;
						ByteUtils.pack4(buf.arr, dst, len);
						dst += 4;
						buf.extend(dst + len);
						System.arraycopy(arr, 0, buf.arr, dst, len);
						dst += len;
					} else {
						ByteUtils.pack4(buf.arr, dst, 0);
						dst += 4;
					}
					break;
				}
				default:
					Conditions.failed("Invalid type");
				}
			}
			return new Key(buf.toArray());
		} catch (final Exception x) {
			throw new ODbException(ODbException.ACCESS_VIOLATION, x);
		}
	}

	private Key convertKey(final Key key) {
		if (key == null) {
			return null;
		}
		if (key.type != ClassDescriptor.tpArrayOfObject) {
			throw new ODbException(ODbException.INCOMPATIBLE_KEY_TYPE);
		}
		final Object[] values = (Object[]) key.oval;
		final ByteBuffer buf = new ByteBuffer();
		int dst = 0;
		for (int i = 0; i < values.length; i++) {
			final Object v = values[i];
			switch (types[i]) {
			case ClassDescriptor.tpBoolean:
				buf.extend(dst + 1);
				buf.arr[dst++] = (byte) (((Boolean) v).booleanValue() ? 1 : 0);
				break;
			case ClassDescriptor.tpByte:
				buf.extend(dst + 1);
				buf.arr[dst++] = ((Number) v).byteValue();
				break;
			case ClassDescriptor.tpShort:
				buf.extend(dst + 2);
				ByteUtils.pack2(buf.arr, dst, ((Number) v).shortValue());
				dst += 2;
				break;
			case ClassDescriptor.tpChar:
				buf.extend(dst + 2);
				ByteUtils.pack2(buf.arr, dst,
						(v instanceof Number) ? ((Number) v).shortValue()
								: (short) ((Character) v).charValue());
				dst += 2;
				break;
			case ClassDescriptor.tpInt:
				buf.extend(dst + 4);
				ByteUtils.pack4(buf.arr, dst, ((Number) v).intValue());
				dst += 4;
				break;
			case ClassDescriptor.tpObject:
				buf.extend(dst + 4);
				ByteUtils.pack4(buf.arr, dst, getODB().getOid(v));
				dst += 4;
				break;
			case ClassDescriptor.tpLong:
				buf.extend(dst + 8);
				ByteUtils.pack8(buf.arr, dst, ((Number) v).longValue());
				dst += 8;
				break;
			case ClassDescriptor.tpDate:
				buf.extend(dst + 8);
				ByteUtils.pack8(buf.arr, dst,
						v == null ? -1 : ((Date) v).getTime());
				dst += 8;
				break;
			case ClassDescriptor.tpFloat:
				buf.extend(dst + 4);
				ByteUtils.pack4(buf.arr, dst,
						Float.floatToIntBits(((Number) v).floatValue()));
				dst += 4;
				break;
			case ClassDescriptor.tpDouble:
				buf.extend(dst + 8);
				ByteUtils.pack8(buf.arr, dst,
						Double.doubleToLongBits(((Number) v).doubleValue()));
				dst += 8;
				break;
			case ClassDescriptor.tpEnum:
				buf.extend(dst + 4);
				ByteUtils.pack4(buf.arr, dst, ((Enum) v).ordinal());
				dst += 4;
				break;
			case ClassDescriptor.tpString: {
				buf.extend(dst + 4);
				if (v != null) {
					final String str = convertString(v);
					final int len = str.length();
					ByteUtils.pack4(buf.arr, dst, len);
					dst += 4;
					buf.extend(dst + len * 2);
					for (int j = 0; j < len; j++) {
						ByteUtils.pack2(buf.arr, dst, (short) str.charAt(j));
						dst += 2;
					}
				} else {
					ByteUtils.pack4(buf.arr, dst, 0);
					dst += 4;
				}
				break;
			}
			case ClassDescriptor.tpArrayOfByte: {
				buf.extend(dst + 4);
				if (v != null) {
					final byte[] arr = (byte[]) v;
					final int len = arr.length;
					ByteUtils.pack4(buf.arr, dst, len);
					dst += 4;
					buf.extend(dst + len);
					System.arraycopy(arr, 0, buf.arr, dst, len);
					dst += len;
				} else {
					ByteUtils.pack4(buf.arr, dst, 0);
					dst += 4;
				}
				break;
			}
			default:
				Conditions.failed("Invalid type");
			}
		}
		return new Key(buf.toArray(), key.inclusion != 0);
	}

	public boolean add(final T obj) {
		return super.put(extractKey(obj), obj);
	}

	public boolean put(final T obj) {
		return super.put(extractKey(obj), obj);
	}

	public T set(final T obj) {
		return super.set(extractKey(obj), obj);
	}

	public boolean addAll(final Collection<? extends T> c) {
		final MultiFieldValue[] arr = new MultiFieldValue[c.size()];
		final Iterator<? extends T> e = c.iterator();
		try {
			for (int i = 0; e.hasNext(); i++) {
				final T obj = e.next();
				final Comparable[] values = new Comparable[fld.length];
				for (int j = 0; j < values.length; j++) {
					values[j] = (Comparable) fld[j].get(obj);
				}
				arr[i] = new MultiFieldValue(obj, values);
			}
		} catch (final Exception x) {
			throw new ODbException(ODbException.ACCESS_VIOLATION, x);
		}
		Arrays.sort(arr);
		for (int i = 0; i < arr.length; i++) {
			add((T) arr[i].obj);
		}
		return arr.length > 0;
	}

	public boolean remove(final Object obj) {
		return super.removeIfExists(extractKey(obj), obj);
	}

	public T remove(final Key key) {
		return super.remove(convertKey(key));
	}

	public boolean containsObject(final T obj) {
		final Key key = extractKey(obj);
		if (unique) {
			return super.get(key) != null;
		} else {
			final Object[] mbrs = get(key, key);
			for (int i = 0; i < mbrs.length; i++) {
				if (mbrs[i] == obj) {
					return true;
				}
			}
			return false;
		}
	}

	public boolean contains(final Object obj) {
		final Key key = extractKey(obj);
		if (unique) {
			return super.get(key) != null;
		} else {
			final Object[] mbrs = get(key, key);
			for (int i = 0; i < mbrs.length; i++) {
				if (mbrs[i].equals(obj)) {
					return true;
				}
			}
			return false;
		}
	}

	public void append(final T obj) {
		throw new ODbException(ODbException.UNSUPPORTED_INDEX_TYPE);
	}

	public T[] get(final Key from, final Key till) {
		final ArrayList list = new ArrayList();
		if (root != 0) {
			BtreePage.find((ODbImpl) getODB(), root, convertKey(from),
					convertKey(till), this, height, list);
		}
		return (T[]) list.toArray((T[]) Array.newInstance(cls, list.size()));
	}

	public T[] getPrefix(final String prefix) {
		throw new ODbException(ODbException.INCOMPATIBLE_KEY_TYPE);
	}

	public T[] prefixSearch(final String key) {
		throw new ODbException(ODbException.INCOMPATIBLE_KEY_TYPE);
	}

	public T[] toArray() {
		final T[] arr = (T[]) Array.newInstance(cls, nElems);
		if (root != 0) {
			BtreePage.traverseForward((ODbImpl) getODB(), root, type, height,
					arr, 0);
		}
		return arr;
	}

	public T get(final Key key) {
		return super.get(convertKey(key));
	}

	public IterableIterator<T> iterator(final Key from, final Key till,
			final int order) {
		return super.iterator(convertKey(from), convertKey(till), order);
	}

	public IterableIterator<Map.Entry<Object, T>> entryIterator(final Key from,
			final Key till, final int order) {
		return super.entryIterator(convertKey(from), convertKey(till), order);
	}

	public IterableIterator<T> queryByExample(final T obj) {
		final Key key = extractKey(obj);
		return iterator(key, key, ASCENT_ORDER);
	}

	public IterableIterator<T> select(final String predicate) {
		final Query<T> query = new QueryImpl<T>(getODB());
		return query.select(cls, iterator(), predicate);
	}

	public boolean isCaseInsensitive() {
		return false;
	}
}

@DontObfuscate
class BtreeCaseInsensitiveMultiFieldIndex<T> extends BtreeMultiFieldIndex<T> {
	BtreeCaseInsensitiveMultiFieldIndex() {
	}

	BtreeCaseInsensitiveMultiFieldIndex(final Class cls,
			final String[] fieldNames, final boolean unique) {
		super(cls, fieldNames, unique);
	}

	String convertString(final Object s) {
		return ((String) s).toLowerCase();
	}

	public boolean isCaseInsensitive() {
		return true;
	}
}
