package toto.db.odb;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.mi.toto.Conditions;
import toto.lang.ByteUtils;

/**
 * Utilities for working with default implementation of the Object Database
 * 
 * @see ODb
 */
public class ODBUtils {

	/**
	 * Compresses the ODb file at the given path It creates new file at the same
	 * location and with the same name but with with ".odbz" extension. ODb can
	 * work with compressed DB files, at reduced performance.
	 * 
	 * @param oDbPath
	 * @param compressionLevel
	 *            recommended:2
	 * @throws IOException
	 */
	public void compressDatabase(String oDbPath, int compressionLevel)
			throws IOException {
		String path = oDbPath;
		FileInputStream in = new FileInputStream(path);
		int ext = path.lastIndexOf('.');
		String zip = path.substring(0, ext) + ".odbz";
		FileOutputStream out = new FileOutputStream(zip);
		byte[] segment = new byte[CompressedFile.SEGMENT_LENGTH];
		ZipOutputStream zout = new ZipOutputStream(out);
		if (compressionLevel == 2) {
			zout.setLevel(compressionLevel);
		}
		long pos = 0;
		int rc = -1;
		do {
			int offs = 0;
			while (offs < segment.length
					&& (rc = in.read(segment, offs, segment.length - offs)) >= 0) {
				offs += rc;
			}
			if (offs > 0) {
				String name = "000000000000" + pos;
				ZipEntry entry = new ZipEntry(
						name.substring(name.length() - 12));
				entry.setSize(offs);
				zout.putNextEntry(entry);
				zout.write(segment, 0, offs);
				zout.closeEntry();
				pos += offs;
			}
		} while (rc >= 0);

		zout.finish();
		zout.close();
	}

	/**
	 * Get total size of all allocated objects in the database
	 */
	public long getUsedSize(ODb database) {
		if (database instanceof ODbImpl) {
			ODbImpl db = (ODbImpl) database;
			return db.usedSize;
		}
		return 0;
	}

	/**
	 * Get database memory dump. This function returns hashmap which key is
	 * classes of stored objects and value - MemoryUsage object which specifies
	 * number of instances of particular class in the storage and total size of
	 * memory used by these instance. Size of internal database structures
	 * (object index,* memory allocation bitmap) is associated with
	 * <code>Storage</code> class. Size of class descriptors - with
	 * <code>java.lang.Class</code> class.
	 * <p>
	 * This method traverse the storage as garbage collection do - starting from
	 * the root object and recursively visiting all reachable objects. So it
	 * reports statistic only for visible objects. If total database size is
	 * significantly larger than total size of all instances reported by this
	 * method, it means that there is garbage in the database. You can
	 * explicitly invoke garbage collector in this case.
	 * </p>
	 */
	public static synchronized HashMap<Class, MemoryUsage> getMemoryDump(
			ODb database) {
		if (database instanceof ODbImpl) {
			ODbImpl db = (ODbImpl) database;
			synchronized (db.objectCache) {
				if (!db.opened) {
					throw new ODbException(ODbException.DATABASE_NOT_OPENED);
				}
				int bitmapSize = (int) (db.header.root[db.currIndex].size >>> (db.dbAllocationQuantumBits + 5)) + 1;
				boolean existsNotMarkedObjects;
				long pos;
				int i, j, n;

				// mark
				db.greyBitmap = new int[bitmapSize];
				db.blackBitmap = new int[bitmapSize];
				int rootOid = db.header.root[db.currIndex].rootObject;
				HashMap<Class, MemoryUsage> map = new HashMap<Class, MemoryUsage>();

				if (rootOid != 0) {
					MemoryUsage indexUsage = new MemoryUsage(Index.class);
					MemoryUsage fieldIndexUsage = new MemoryUsage(
							FieldIndex.class);
					MemoryUsage classUsage = new MemoryUsage(Class.class);

					db.markOid(rootOid);
					do {
						existsNotMarkedObjects = false;
						for (i = 0; i < bitmapSize; i++) {
							if (db.greyBitmap[i] != 0) {
								existsNotMarkedObjects = true;
								for (j = 0; j < 32; j++) {
									if ((db.greyBitmap[i] & (1 << j)) != 0) {
										pos = (((long) i << 5) + j) << db.dbAllocationQuantumBits;
										db.greyBitmap[i] &= ~(1 << j);
										db.blackBitmap[i] |= 1 << j;
										int offs = (int) pos
												& (Page.pageSize - 1);
										Page pg = db.pool.getPage(pos - offs);
										int typeOid = ObjectHeader.getType(
												pg.data, offs);
										int objSize = ObjectHeader.getSize(
												pg.data, offs);
										int alignedSize = (objSize
												+ db.dbAllocationQuantum - 1)
												& ~(db.dbAllocationQuantum - 1);
										if (typeOid != 0) {
											db.markOid(typeOid);
											ClassDescriptor desc = db
													.findClassDescriptor(typeOid);
											if (Btree.class
													.isAssignableFrom(desc.cls)) {
												Btree btree = new Btree(
														pg.data,
														ObjectHeader.sizeof
																+ offs);
												btree.assignObjectId(db, 0,
														false);
												int nPages = btree.markTree();
												if (FieldIndex.class
														.isAssignableFrom(desc.cls)) {
													fieldIndexUsage.nInstances += 1;
													fieldIndexUsage.totalSize += (long) nPages
															* Page.pageSize
															+ objSize;
													fieldIndexUsage.allocatedSize += (long) nPages
															* Page.pageSize
															+ alignedSize;
												} else {
													indexUsage.nInstances += 1;
													indexUsage.totalSize += (long) nPages
															* Page.pageSize
															+ objSize;
													indexUsage.allocatedSize += (long) nPages
															* Page.pageSize
															+ alignedSize;
												}
											} else {
												MemoryUsage usage = (MemoryUsage) map
														.get(desc.cls);
												if (usage == null) {
													usage = new MemoryUsage(
															desc.cls);
													map.put(desc.cls, usage);
												}
												usage.nInstances += 1;
												usage.totalSize += objSize;
												usage.allocatedSize += alignedSize;

												if (desc.hasReferences) {
													db.markObject(
															db.pool.get(pos),
															ObjectHeader.sizeof,
															desc);
												}
											}
										} else {
											classUsage.nInstances += 1;
											classUsage.totalSize += objSize;
											classUsage.allocatedSize += alignedSize;
										}
										db.pool.unfix(pg);
									}
								}
							}
						}
					} while (existsNotMarkedObjects);

					if (indexUsage.nInstances != 0) {
						map.put(Index.class, indexUsage);
					}
					if (fieldIndexUsage.nInstances != 0) {
						map.put(FieldIndex.class, fieldIndexUsage);
					}
					if (classUsage.nInstances != 0) {
						map.put(Class.class, classUsage);
					}
					MemoryUsage system = new MemoryUsage(ODb.class);
					system.totalSize += db.header.root[0].indexSize * 8L;
					system.totalSize += db.header.root[1].indexSize * 8L;
					system.totalSize += (long) (db.header.root[db.currIndex].bitmapEnd - db.dbBitmapId)
							* Page.pageSize;
					system.totalSize += Page.pageSize; // root page

					if (db.header.root[db.currIndex].bitmapExtent != 0) {
						system.allocatedSize = db
								.getBitmapUsedSpace(db.dbBitmapId,
										db.dbBitmapId + db.dbBitmapPages)
								+ db.getBitmapUsedSpace(
										db.header.root[db.currIndex].bitmapExtent
												+ db.dbBitmapPages
												- db.bitmapExtentBase,
										db.header.root[db.currIndex].bitmapExtent
												+ db.header.root[db.currIndex].bitmapEnd
												- db.dbBitmapId
												- db.bitmapExtentBase);
					} else {
						system.allocatedSize = db.getBitmapUsedSpace(
								db.dbBitmapId,
								db.header.root[db.currIndex].bitmapEnd);
					}
					system.nInstances = db.header.root[db.currIndex].indexSize;
					map.put(ODb.class, system);
				}
				return map;
			}
		}
		return null;
	}

	/**
	 * Get size of the database
	 */
	public static long getDatabaseSize(ODb database) {
		if (database instanceof ODbImpl) {
			ODbImpl db = (ODbImpl) database;
			return db.header.root[1 - db.currIndex].size;
		}
		return 0;
	}

	/**
	 * Export database in XML format
	 * 
	 * @param writer
	 *            writer for generated XML document
	 */
	public static synchronized void exportXML(ODb database,
			java.io.Writer writer) throws java.io.IOException {
		if (database instanceof ODbImpl) {
			ODbImpl db = (ODbImpl) database;
			if (!db.opened) {
				throw new ODbException(ODbException.DATABASE_NOT_OPENED);
			}
			db.objectCache.flush();
			int rootOid = db.header.root[1 - db.currIndex].rootObject;
			if (rootOid != 0) {
				XMLExporter xmlExporter = new XMLExporter(db, writer);
				xmlExporter.exportDatabase(rootOid);
			}
		}
	}

	/**
	 * Import data from XML file
	 * 
	 * @param reader
	 *            XML document reader
	 */
	public synchronized void importXML(ODb database, java.io.Reader reader)
			throws ODbException {
		if (database instanceof ODbImpl) {
			ODbImpl db = (ODbImpl) database;
			if (!db.opened) {
				throw new ODbException(ODbException.DATABASE_NOT_OPENED);
			}
			XMLImporter xmlImporter = new XMLImporter(db, reader);
			xmlImporter.importDatabase();
		}
	}

	public static void backup(ODb database, String filePath, String cryptKey)
			throws java.io.IOException {
		backup(database, new IFileOutputStream(
				cryptKey != null ? (ODbFile) new Rc4File(filePath, false,
						false, cryptKey) : (ODbFile) new OSFile(filePath,
						false, false)));
	}

	/**
	 * Backup current state of database
	 * 
	 * @param out
	 *            output stream to which backup is done
	 */
	public static/* synchronized */void backup(ODb database, OutputStream out)
			throws java.io.IOException {
		if (database instanceof ODbImpl) {
			ODbImpl db = (ODbImpl) database;

			if (!db.opened) {
				throw new ODbException(ODbException.DATABASE_NOT_OPENED);
			}
			synchronized (database) {
				db.objectCache.flush();
			}

			int curr = 1 - db.currIndex;
			final int nObjects = db.header.root[curr].indexUsed;
			long indexOffs = db.header.root[curr].index;
			int i, j, k;
			int nUsedIndexPages = (nObjects + db.dbHandlesPerPage - 1)
					/ db.dbHandlesPerPage;
			int nIndexPages = (int) ((db.header.root[curr].indexSize
					+ db.dbHandlesPerPage - 1) / db.dbHandlesPerPage);
			long totalRecordsSize = 0;
			long nPagedObjects = 0;
			int bitmapExtent = db.header.root[curr].bitmapExtent;
			final long[] index = new long[nObjects];
			final int[] oids = new int[nObjects];

			if (bitmapExtent == 0) {
				bitmapExtent = Integer.MAX_VALUE;
			}
			for (i = 0, j = 0; i < nUsedIndexPages; i++) {
				Page pg = db.pool.getPage(indexOffs + (long) i * Page.pageSize);
				for (k = 0; k < db.dbHandlesPerPage && j < nObjects; k++, j++) {
					long pos = ByteUtils.unpack8(pg.data, k * 8);
					index[j] = pos;
					oids[j] = j;
					if ((pos & db.dbFreeHandleFlag) == 0) {
						if ((pos & db.dbPageObjectFlag) != 0) {
							nPagedObjects += 1;
						} else if (pos != 0) {
							int offs = (int) pos & (Page.pageSize - 1);
							Page op = db.pool.getPage(pos - offs);
							int size = ObjectHeader.getSize(op.data, offs
									& ~db.dbFlagsMask);
							size = (size + db.dbAllocationQuantum - 1)
									& ~(db.dbAllocationQuantum - 1);
							totalRecordsSize += size;
							db.pool.unfix(op);
						}
					}
				}
				db.pool.unfix(pg);

			}
			Header newHeader = new Header();
			newHeader.curr = 0;
			newHeader.dirty = false;
			newHeader.databaseFormatVersion = db.header.databaseFormatVersion;
			long newFileSize = (long) (nPagedObjects + nIndexPages * 2 + 1)
					* Page.pageSize + totalRecordsSize;
			newFileSize = (newFileSize + Page.pageSize - 1)
					& ~(Page.pageSize - 1);
			newHeader.root = new RootPage[2];
			newHeader.root[0] = new RootPage();
			newHeader.root[1] = new RootPage();
			newHeader.root[0].size = newHeader.root[1].size = newFileSize;
			newHeader.root[0].index = newHeader.root[1].shadowIndex = Page.pageSize;
			newHeader.root[0].shadowIndex = newHeader.root[1].index = Page.pageSize
					+ (long) nIndexPages * Page.pageSize;
			newHeader.root[0].shadowIndexSize = newHeader.root[0].indexSize = newHeader.root[1].shadowIndexSize = newHeader.root[1].indexSize = nIndexPages
					* db.dbHandlesPerPage;
			newHeader.root[0].indexUsed = newHeader.root[1].indexUsed = nObjects;
			newHeader.root[0].freeList = newHeader.root[1].freeList = db.header.root[curr].freeList;
			newHeader.root[0].bitmapEnd = newHeader.root[1].bitmapEnd = db.header.root[curr].bitmapEnd;

			newHeader.root[0].rootObject = newHeader.root[1].rootObject = db.header.root[curr].rootObject;
			newHeader.root[0].classDescList = newHeader.root[1].classDescList = db.header.root[curr].classDescList;
			newHeader.root[0].bitmapExtent = newHeader.root[1].bitmapExtent = db.header.root[curr].bitmapExtent;
			byte[] page = new byte[Page.pageSize];
			newHeader.pack(page);
			out.write(page);

			long pageOffs = (long) (nIndexPages * 2 + 1) * Page.pageSize;
			long recOffs = (long) (nPagedObjects + nIndexPages * 2 + 1)
					* Page.pageSize;

			GenericSort.sort(new GenericSortArray() {
				public int size() {
					return nObjects;
				}

				public int compare(int i, int j) {
					return index[i] < index[j] ? -1 : index[i] == index[j] ? 0
							: 1;
				}

				public void swap(int i, int j) {
					long t1 = index[i];
					index[i] = index[j];
					index[j] = t1;
					int t2 = oids[i];
					oids[i] = oids[j];
					oids[j] = t2;
				}
			});
			byte[] newIndex = new byte[nIndexPages * db.dbHandlesPerPage * 8];
			for (i = 0; i < nObjects; i++) {
				long pos = index[i];
				int oid = oids[i];
				if ((pos & db.dbFreeHandleFlag) == 0) {
					if ((pos & db.dbPageObjectFlag) != 0) {
						ByteUtils.pack8(newIndex, oid * 8, pageOffs
								| db.dbPageObjectFlag);
						pageOffs += Page.pageSize;
					} else if (pos != 0) {
						ByteUtils.pack8(newIndex, oid * 8, recOffs);
						int offs = (int) pos & (Page.pageSize - 1);
						Page op = db.pool.getPage(pos - offs);
						int size = ObjectHeader.getSize(op.data, offs
								& ~db.dbFlagsMask);
						size = (size + db.dbAllocationQuantum - 1)
								& ~(db.dbAllocationQuantum - 1);
						recOffs += size;
						db.pool.unfix(op);
					}
				} else {
					ByteUtils.pack8(newIndex, oid * 8, pos);
				}
			}
			out.write(newIndex);
			out.write(newIndex);

			for (i = 0; i < nObjects; i++) {
				long pos = index[i];
				if (((int) pos & (db.dbFreeHandleFlag | db.dbPageObjectFlag)) == db.dbPageObjectFlag) {
					if (oids[i] < db.dbBitmapId + db.dbBitmapPages
							|| (oids[i] >= bitmapExtent && oids[i] < bitmapExtent
									+ db.dbLargeBitmapPages - db.dbBitmapPages)) {
						int pageId = oids[i] < db.dbBitmapId + db.dbBitmapPages ? oids[i]
								- db.dbBitmapId
								: oids[i] - bitmapExtent + db.bitmapExtentBase;
						long mappedSpace = (long) pageId * Page.pageSize * 8
								* db.dbAllocationQuantum;
						if (mappedSpace >= newFileSize) {
							Arrays.fill(page, (byte) 0);
						} else if (mappedSpace + Page.pageSize * 8
								* db.dbAllocationQuantum <= newFileSize) {
							Arrays.fill(page, (byte) -1);
						} else {
							int nBits = (int) ((newFileSize - mappedSpace) >> db.dbAllocationQuantumBits);
							Arrays.fill(page, 0, nBits >> 3, (byte) -1);
							page[nBits >> 3] = (byte) ((1 << (nBits & 7)) - 1);
							Arrays.fill(page, (nBits >> 3) + 1, Page.pageSize,
									(byte) 0);
						}
						out.write(page);
					} else {
						Page pg = db.pool.getPage(pos & ~db.dbFlagsMask);
						out.write(pg.data);
						db.pool.unfix(pg);
					}
				}
			}
			for (i = 0; i < nObjects; i++) {
				long pos = index[i];
				if (pos != 0
						&& ((int) pos & (db.dbFreeHandleFlag | db.dbPageObjectFlag)) == 0) {
					pos &= ~db.dbFlagsMask;
					int offs = (int) pos & (Page.pageSize - 1);
					Page pg = db.pool.getPage(pos - offs);
					int size = ObjectHeader.getSize(pg.data, offs);
					size = (size + db.dbAllocationQuantum - 1)
							& ~(db.dbAllocationQuantum - 1);

					while (true) {
						if (Page.pageSize - offs >= size) {
							out.write(pg.data, offs, size);
							break;
						}
						out.write(pg.data, offs, Page.pageSize - offs);
						size -= Page.pageSize - offs;
						pos += Page.pageSize - offs;
						offs = 0;
						db.pool.unfix(pg);
						pg = db.pool.getPage(pos);
					}
					db.pool.unfix(pg);
				}
			}
			if (recOffs != newFileSize) {
				Conditions
						.assertArgument(newFileSize - recOffs < Page.pageSize);
				int align = (int) (newFileSize - recOffs);
				Arrays.fill(page, 0, align, (byte) 0);
				out.write(page, 0, align);
			}
		}
	}

}
