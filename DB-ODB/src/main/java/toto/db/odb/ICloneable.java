package toto.db.odb;

/**
 * This interface allows to clone its implementor. It is needed because
 * Object.clone is protected and java.lang.Cloneable interface contains no
 * method defintion
 */
public interface ICloneable extends java.lang.Cloneable {
	Object clone() throws CloneNotSupportedException;
}
