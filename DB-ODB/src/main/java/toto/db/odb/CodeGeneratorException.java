package toto.db.odb;

/**
 * Exception thrown by code generator
 */
class CodeGeneratorException extends RuntimeException {
	CodeGeneratorException(String msg) {
		super(msg);
	}
}
