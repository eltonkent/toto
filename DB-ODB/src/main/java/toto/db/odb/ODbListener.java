package toto.db.odb;

/**
 * Listener of database events. Programmer should derive his own subclass and
 * register it using Storage.setListener method.
 */
public abstract class ODbListener {
	/**
	 * This metod is called during database open when database was not close
	 * normally and has to be recovered
	 */
	public void databaseCorrupted() {
	}

	/**
	 * This method is called after completion of recovery
	 */
	public void recoveryCompleted() {
	}

	/**
	 * Method invoked by ODb affter object is loaded from the database
	 * 
	 * @param obj
	 *            loaded object
	 */
	public void onObjectLoad(final Object obj) {
	}

	/**
	 * Method invoked by ODb before object is written to the database
	 * 
	 * @param obj
	 *            stored object
	 */
	public void onObjectStore(final Object obj) {
	}

	/**
	 * Method invoked by ODb before object is deallocated
	 * 
	 * @param obj
	 *            deallocated object
	 */
	public void onObjectDelete(final Object obj) {
	}

	/**
	 * Method invoked by ODb after object is assigned OID (becomes persisistent)
	 * 
	 * @param obj
	 *            object which is made persistent
	 */
	public void onObjectAssignOid(final Object obj) {
	}

	/**
	 * Method invoked by ODb when slave node receive updates from master
	 */
	public void onMasterDatabaseUpdate() {
	}

	/**
	 * Method invoked by ODb when transaction is committed
	 */
	public void onTransactionCommit() {
	}

	/**
	 * Method invoked by ODb when transaction is aborted
	 */
	public void onTransactionRollback() {
	}

	/**
	 * This method is called when garbage collection is started (ether
	 * explicitly by invocation of Storage.gc() method, either implicitly after
	 * allocation of some amount of memory)).
	 */
	public void gcStarted() {
	}

	/**
	 * This method is called when unreferenced object is deallocated from
	 * database during garbage collection. It is possible to get instance of the
	 * object using <code>Storage.getObjectByOid()</code> method.
	 * 
	 * @param cls
	 *            class of deallocated object
	 * @param oid
	 *            object identifier of deallocated object
	 */
	public void deallocateObject(final Class cls, final int oid) {
	}

	/**
	 * This method is called when garbage collection is completed
	 * 
	 * @param nDeallocatedObjects
	 *            number of deallocated objects
	 */
	public void gcCompleted(final int nDeallocatedObjects) {
	}

	/**
	 * Handle replication error
	 * 
	 * @param host
	 *            address of host replication to which is failed (null if error
	 *            jappens at slave node)
	 * @return <code>true</code> if host should be reconnected and attempt to
	 *         send data to it should be repeated, <code>false</code> if no more
	 *         attmpts to communicate with this host should be performed
	 */
	public boolean replicationError(final String host) {
		return false;
	}

	/**
	 * This method is called when runtime error happen during execution of JSQL
	 * query
	 */
	public void JSQLRuntimeError(final ODbSQLRuntimeException x) {
	}

	/**
	 * This method is called when query is executed
	 * 
	 * @param table
	 *            table in which search is perfomed.
	 * @param query
	 *            executed query
	 * @param elapsedTime
	 *            time in milliseconds of query execution (please notice that
	 *            many queries are executed incrementally, so the time passed to
	 *            this method is not always complete time of query execution
	 * @param sequentialSearch
	 *            true if sequential scan in used for query execution, false if
	 *            some index is used
	 */
	public void queryExecution(final Class table, final String query,
			final long elapsedTime, final boolean sequentialSearch) {
	}

	/**
	 * Sequential search is performed for query execution
	 * 
	 * @param table
	 *            table in which sequential search is perfomed.
	 * @param query
	 *            executed query
	 */
	public void sequentialSearchPerformed(final Class table, final String query) {
	}

	/**
	 * Sort of the selected result set is performed for query execution
	 * 
	 * @param table
	 *            table in which sort is perfomed.
	 * @param query
	 *            executed query
	 */
	public void sortResultSetPerformed(final Class table, final String query) {
	}

	/**
	 * This method is called by XML exporter if database correuption or some
	 * other reasons makes export of the object not possible
	 * 
	 * @param oid
	 *            object identified
	 * @param x
	 *            catched exception
	 */
	public void objectNotExported(final int oid, final ODbException x) {
	}

	/**
	 * Index is automaticaslly created by Database class when query is executed
	 * and autoIndices is anabled
	 * 
	 * @param table
	 *            table for which index is created
	 * @param field
	 *            index key
	 */
	public void indexCreated(final Class table, final String field) {
	}
}
