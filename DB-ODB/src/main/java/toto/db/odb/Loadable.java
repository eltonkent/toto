package toto.db.odb;

/**
 * Interface for classes which need onLoad callback to be invoked when OODB
 * loads object from the storage
 */
interface Loadable {
	/**
	 * Method called by the database after loading of the object. It can be used
	 * to initialize transient fields of the object. Default implementation of
	 * this method do nothing
	 */
	public void onLoad();
}
