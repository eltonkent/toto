package toto.db.odb;

/**
 * Base class for all ODb capable objects.
 * <p>
 * Its not mandatory for all object that are stored in ODb to extend
 * {@link ODbObject}. But extending
 * </p>
 * 
 * @see IODbObject
 */
public class ODbObject extends PinnedODbObject {
	public ODbObject() {
	}

	public ODbObject(final ODb storage) {
		super(storage);
	}

	protected void finalize() {
		if ((state & DIRTY) != 0 && oid != 0) {
			storage.storeFinalizedObject(this);
		}
		state = DELETED;
	}
}
