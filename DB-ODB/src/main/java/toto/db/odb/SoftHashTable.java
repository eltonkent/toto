package toto.db.odb;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;

class SoftHashTable extends WeakHashTable {
	SoftHashTable(ODbImpl db, int initialCapacity) {
		super(db, initialCapacity);
	}

	protected Reference createReference(Object obj) {
		return new SoftReference(obj);
	}
}
