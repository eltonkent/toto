package toto.db.odb;

import java.io.File;
import java.util.Map;

import toto.cache.SerializedStorageConverter;
import toto.cache.storage.impl.DiskStorage;
import android.util.Log;

/**
 * ODb based storage provider for {@link toto.cache.TCache} (BETA)
 * 
 * @author Mobifluence Interactive
 * 
 * @param <K>
 * @param <V>
 */
public class ODbStorage<K, V> extends DiskStorage<K, V> {

	private final ODb db;
	private Index<ODbEntry> idx;

	static class ODbEntry extends ODbObject {
		byte[] entry;

		public ODbEntry() {
		}

		ODbEntry(final byte[] entry) {
			this.entry = entry;
		}
	}

	public ODbStorage(final SerializedStorageConverter<K> keyConverter,
			final SerializedStorageConverter<V> valueConverter,
			final File cacheDirectory) {
		super(keyConverter, valueConverter, cacheDirectory);
		db = ODbFactory.createOODB();
		db.open(cacheDirectory.toString() + File.separator + "TCache.odb");
		idx = db.getRootObject();
		if (idx == null) {
			idx = db.createIndex(ODbEntry.class, true);
			db.setRootObject(idx);
		}

	}

	@Override
	protected void clear() {
		idx.clear();
	}

	@Override
	protected void destroy() {
		db.close();
		new File(cacheDirectory, "TCache.odb").delete();
	}

	@Override
	protected void close() {
		db.commit();
		db.close();
	}

	@Override
	protected boolean save(final byte[] key, final byte[] value) {
		final ODbEntry valEntry = new ODbEntry(value);
		final ODbEntry keyEntry = new ODbEntry(key);
		final boolean ret = idx.put(new Key(keyEntry), valEntry);
		db.commit();
		Log.d("ToTo", "Number of entries: " + idx.size());
		return ret;
	}

	@Override
	protected boolean delete(final byte[] key) {
		final ODbEntry keyEntry = new ODbEntry(key);
		final ODbEntry tst = idx.remove(new Key(keyEntry));
		return tst == null;
		// TODO fix this
		// return idx.remove(key);
		// return false;
	}

	@Override
	protected byte[] fetch(final byte[] key) {
		final ODbEntry keyEntry = new ODbEntry(key);
		final ODbEntry value = idx.get(keyEntry);
		if (value != null) {
			return value.entry;
		}
		return null;
	}

	@Override
	protected int size() {
		return idx.size();
		// TODO query to find the number of entries in the database
	}

	@Override
	protected toto.cache.Storage<K, V>.EntryMetaInfo getEntryMetaInfo(
			final K key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Map<K, toto.cache.Storage<K, V>.EntryMetaInfo> getAllEntriesMetaInfo() {
		// TODO Auto-generated method stub
		return null;
	}
}
