package toto.db.odb;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import test.toto.ToToTestCase;
import test.toto.db.odb.ODbCollectionsTest.IndexRecord;
import test.toto.db.odb.ODbCollectionsTest.TestIndexIndices;
import toto.db.odb.FieldIndex;
import toto.db.odb.IODbObject;
import toto.db.odb.Index;
import toto.db.odb.Key;
import toto.db.odb.ODb;
import toto.db.odb.ODbFactory;
import toto.db.odb.ODbObject;
import toto.io.file.FileUtils;
import toto.io.file.SDCardUtils;

public class ODbIndicesTest extends ToToTestCase {

	String path = SDCardUtils.getDirectory().toString() + "/ODB/";
	String dbName;
	final static int nRecords = 100;
	static int pagePoolSize = 32 * 1024 * 1024;

	public ODbIndicesTest() {
		dbName = path + "ODbUnitTest.odb";
		File file = new File(path);
		if (!file.exists()) {
			file.mkdir();
		}
		file = new File(dbName);
		if (file.exists()) {
			file.delete();
		}
	}

	public void testIndex() {
		ODb db = ODbFactory.createOODB();
		db.open(dbName);
		boolean serializableTransaction = true;
		TestIndexIndices root = (TestIndexIndices) db.getRootObject();
		if (root == null) {
			root = new TestIndexIndices();
			root.strIndex = db.createIndex(String.class, true);
			root.intIndex = db.createIndex(long.class, true);
			db.setRootObject(root);
		}
		Index intIndex = root.intIndex;
		Index strIndex = root.strIndex;
		long start = System.currentTimeMillis();
		long key = 1999;
		int i;
		for (i = 0; i < nRecords; i++) {
			IndexRecord rec = new IndexRecord();
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			rec.intKey = key;
			rec.strKey = Long.toString(key);
			intIndex.put(new Key(rec.intKey), rec);
			strIndex.put(new Key(rec.strKey), rec);
			/*
			 * if (i % 100000 == 0) { System.out.print("Insert " + i +
			 * " records\r"); db.commit(); }
			 */
		}

		if (serializableTransaction) {
			db.endThreadTransaction();
		} else {
			db.commit();
		}
		// db.gc();
		logI("Elapsed time for inserting " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		key = 1999;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			IndexRecord rec1 = (IndexRecord) intIndex.get(new Key(key));
			IndexRecord rec2 = (IndexRecord) strIndex.get(new Key(Long
					.toString(key)));
			assert (rec1 != null && rec1 == rec2);
		}
		logI("Elapsed time for performing " + nRecords * 2
				+ " index searches: " + (System.currentTimeMillis() - start)
				+ " milliseconds");

		start = System.currentTimeMillis();
		Iterator iterator = intIndex.iterator();
		key = Long.MIN_VALUE;
		for (i = 0; iterator.hasNext(); i++) {
			IndexRecord rec = (IndexRecord) iterator.next();
			assert (rec.intKey >= key);
			key = rec.intKey;
		}
		assert (i == nRecords);
		iterator = strIndex.iterator();
		String strKey = "";
		for (i = 0; iterator.hasNext(); i++) {
			IndexRecord rec = (IndexRecord) iterator.next();
			assert (rec.strKey.compareTo(strKey) >= 0);
			strKey = rec.strKey;
		}
		assert (i == nRecords);
		logI("Elapsed time for iterating through " + (nRecords * 2)
				+ " records: " + (System.currentTimeMillis() - start)
				+ " milliseconds");
		// HashMap map = db.getMemoryDump();
		// iterator = map.values().iterator();
		// logI("Memory usage");
		// start = System.currentTimeMillis();
		// while (iterator.hasNext()) {
		// MemoryUsage usage = (MemoryUsage) iterator.next();
		// logI(" " + usage.cls.getName() + ": instances="
		// + usage.nInstances + ", total size=" + usage.totalSize
		// + ", allocated size=" + usage.allocatedSize);
		// }
		// logI("Elapsed time for memory dump: "
		// + (System.currentTimeMillis() - start) + " milliseconds");
		start = System.currentTimeMillis();
		key = 1999;
		if (serializableTransaction) {
			db.beginThreadTransaction(ODb.SERIALIZABLE_TRANSACTION);
		}
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			IndexRecord rec = (IndexRecord) intIndex.get(new Key(key));
			IndexRecord removed = (IndexRecord) intIndex.remove(new Key(key));
			assert (removed == rec);
			// strIndex.remove(new Key(Long.toString(key)), rec);
			strIndex.remove(new Key(Long.toString(key)));
			rec.deallocate();
		}
		if (serializableTransaction) {
			db.endThreadTransaction();
		}
		assert (!intIndex.iterator().hasNext());
		assert (!strIndex.iterator().hasNext());
		assert (!intIndex.iterator(null, null, Index.DESCENT_ORDER).hasNext());
		assert (!strIndex.iterator(null, null, Index.DESCENT_ORDER).hasNext());
		assert (!intIndex.iterator(null, null, Index.ASCENT_ORDER).hasNext());
		assert (!strIndex.iterator(null, null, Index.ASCENT_ORDER).hasNext());
		logI("Elapsed time for deleting " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");
		db.close();
		try {
			FileUtils.emptyDirectory(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void testRandomAccessFieldIndex() {
		final int nRecords = 1003; // should be prime
		int pagePoolSize = 32 * 1024 * 1024;
		final int step = 5;
		ODb db = ODbFactory.createOODB();
		db.open(dbName, pagePoolSize);
		FieldIndex root = (FieldIndex) db.getRootObject();
		if (root == null) {
			root = db.createRandomAccessFieldIndex(ListRecord.class, "i", true);
			db.setRootObject(root);
		}
		long start = System.currentTimeMillis();
		int i, j;
		for (i = 0, j = 0; i < nRecords; i++, j += step) {
			ListRecord rec = new ListRecord();
			rec.i = j % nRecords;
			root.put(rec);
		}
		db.commit();
		// db.gc();
		logI("Elapsed time for inserting " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		for (i = 0; i < nRecords; i++) {
			ListRecord rec = (ListRecord) root.get(new Key(i));
			assert (rec.i == i);
			assert (root.indexOf(new Key(i)) == i);
		}
		logI("Elapsed time for performing " + nRecords + " get operations: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		for (i = 0; i < nRecords; i++) {
			ListRecord rec = (ListRecord) root.getAt(i);
			assert (rec.i == i);
		}
		logI("Elapsed time for performing " + nRecords + " getAt operations: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		Iterator iterator = root
				.entryIterator(nRecords / 2, Index.ASCENT_ORDER);
		for (i = nRecords / 2; iterator.hasNext(); i++) {
			Map.Entry e = (Map.Entry) iterator.next();
			assert (((Integer) e.getKey()).intValue() == i && ((ListRecord) e
					.getValue()).i == i);
		}
		assert (i == nRecords);
		iterator = root.entryIterator(nRecords / 2 - 1, Index.DESCENT_ORDER);
		for (i = nRecords / 2 - 1; iterator.hasNext(); i--) {
			Map.Entry e = (Map.Entry) iterator.next();
			assert (((Integer) e.getKey()).intValue() == i && ((ListRecord) e
					.getValue()).i == i);
		}
		assert (i == -1);
		logI("Elapsed time for iterating through " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		for (i = 0, j = 0; i < nRecords; i += step, j++) {
			ListRecord rec = (ListRecord) root.getAt(i - j);
			assert (rec.i == i);
			root.remove(rec);
			rec.deallocate();
		}
		logI("Elapsed time for deleting " + nRecords / step + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");
		root.clear();
		assert (!root.iterator().hasNext());
		db.close();
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	static class ListRecord extends ODbObject {
		private int i;
		private String test;
	};

	public void testThickIndex() {
		final int maxDuplicates = 100;
		ODb db = ODbFactory.createOODB();
		db.open(dbName, pagePoolSize);
		Indices root = (Indices) db.getRootObject();
		if (root == null) {
			root = new Indices();
			root.strIndex = db.createThickIndex(String.class);
			root.intIndex = db.createThickIndex(long.class);
			db.setRootObject(root);
		}
		Index<IODbObject> intIndex = root.intIndex;
		Index<IODbObject> strIndex = root.strIndex;
		long start = System.currentTimeMillis();
		long key = 1999;
		int i;
		int n = 0;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			int d = (int) (key % maxDuplicates);
			for (int j = 0; j < d; j++) {
				Record rec = new Record();
				rec.intKey = key;
				rec.strKey = Long.toString(key);
				intIndex.put(new Key(rec.intKey), rec);
				strIndex.put(new Key(rec.strKey), rec);
				n += 1;
			}
		}

		db.commit();
		logI("Elapsed time for inserting " + n + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		key = 1999;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			Object[] res1 = intIndex.get(new Key(key), new Key(key));
			Object[] res2 = strIndex.get(new Key(Long.toString(key)), new Key(
					Long.toString(key)));
			int d = (int) (key % maxDuplicates);
			assert (res1.length == res2.length && res1.length == d);
			for (int j = 0; j < d; j++) {
				assert (((Record) res1[j]).intKey == key && ((Record) res2[j]).intKey == key);
			}
		}
		logI("Elapsed time for performing " + nRecords * 2
				+ " index searches: " + (System.currentTimeMillis() - start)
				+ " milliseconds");

		start = System.currentTimeMillis();
		Iterator iterator = intIndex.iterator();
		key = Long.MIN_VALUE;
		for (i = 0; iterator.hasNext(); i++) {
			Record rec = (Record) iterator.next();
			assert (rec.intKey >= key);
			key = rec.intKey;
		}
		assert (i == n);
		iterator = strIndex.iterator();
		String strKey = "";
		for (i = 0; iterator.hasNext(); i++) {
			Record rec = (Record) iterator.next();
			assert (rec.strKey.compareTo(strKey) >= 0);
			strKey = rec.strKey;
		}
		assert (i == n);
		logI("Elapsed time for iterating through " + n + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		key = 1999;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			Object[] res = intIndex.get(new Key(key), new Key(key));
			int d = (int) (key % maxDuplicates);
			assert (res.length == d);
			for (int j = 0; j < d; j++) {
				intIndex.remove(new Key(key), (Record) res[j]);
				strIndex.remove(new Key(Long.toString(key)), (Record) res[j]);
				((Record) res[j]).deallocate();
			}
		}
		assert (!intIndex.iterator().hasNext());
		assert (!strIndex.iterator().hasNext());
		assert (!intIndex.iterator(null, null, Index.DESCENT_ORDER).hasNext());
		assert (!strIndex.iterator(null, null, Index.DESCENT_ORDER).hasNext());
		assert (!intIndex.iterator(null, null, Index.ASCENT_ORDER).hasNext());
		assert (!strIndex.iterator(null, null, Index.ASCENT_ORDER).hasNext());
		logI("Elapsed time for deleting " + n + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");
		db.close();
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	static class Record extends ODbObject {
		String strKey;
		long intKey;
	}

	static class Indices extends ODbObject {
		Index strIndex;
		Index intIndex;
	}

	public void testCompoundIndex() {
		ODb db = ODbFactory.createOODB();
		db.open(dbName, pagePoolSize);
		// for (int i = 0; i < args.length; i++) {
		// if ("altbtree".equals(args[i])) {
		// db.setProperty("perst.alternative.btree", Boolean.TRUE);
		// }
		// }
		FieldIndex root = (FieldIndex) db.getRootObject();
		if (root == null) {
			root = db.createFieldIndex(Record2.class, new String[] { "intKey",
					"strKey" }, true);
			db.setRootObject(root);
		}
		long start = System.currentTimeMillis();
		long key = 1999;
		int i;
		for (i = 0; i < nRecords; i++) {
			Record2 rec = new Record2();
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			rec.intKey = (int) (key >>> 32);
			rec.strKey = Integer.toString((int) key);
			root.put(rec);
		}
		db.commit();
		logI("Elapsed time for inserting " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		key = 1999;
		int minKey = Integer.MAX_VALUE;
		int maxKey = Integer.MIN_VALUE;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			int intKey = (int) (key >>> 32);
			String strKey = Integer.toString((int) key);
			Record2 rec = (Record2) root.get(new Key(new Object[] {
					new Integer(intKey), strKey }));
			assert (rec != null && rec.intKey == intKey && rec.strKey
					.equals(strKey));
			if (intKey < minKey) {
				minKey = intKey;
			}
			if (intKey > maxKey) {
				maxKey = intKey;
			}
		}
		logI("Elapsed time for performing " + nRecords + " index searches: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		Iterator iterator = root.iterator(new Key(new Integer(minKey), ""),
				new Key(new Integer(maxKey + 1), "???"),
				FieldIndex.ASCENT_ORDER);
		int n = 0;
		String prevStr = "";
		int prevInt = minKey;
		while (iterator.hasNext()) {
			Record2 rec = (Record2) iterator.next();
			assert (rec.intKey > prevInt || rec.intKey == prevInt
					&& rec.strKey.compareTo(prevStr) > 0);
			prevStr = rec.strKey;
			prevInt = rec.intKey;
			n += 1;
		}
		assert (n == nRecords);

		iterator = root.iterator(new Key(new Integer(minKey), "", false),
				new Key(new Integer(maxKey + 1), "???", false),
				FieldIndex.DESCENT_ORDER);
		n = 0;
		prevInt = maxKey + 1;
		while (iterator.hasNext()) {
			Record2 rec = (Record2) iterator.next();
			assert (rec.intKey < prevInt || rec.intKey == prevInt
					&& rec.strKey.compareTo(prevStr) < 0);
			prevStr = rec.strKey;
			prevInt = rec.intKey;
			n += 1;
		}
		assert (n == nRecords);
		logI("Elapsed time for iterating through " + (nRecords * 2)
				+ " records: " + (System.currentTimeMillis() - start)
				+ " milliseconds");
		start = System.currentTimeMillis();
		key = 1999;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			int intKey = (int) (key >>> 32);
			String strKey = Integer.toString((int) key);
			Record2 rec = (Record2) root.get(new Key(new Object[] {
					new Integer(intKey), strKey }));
			assert (rec != null && rec.intKey == intKey && rec.strKey
					.equals(strKey));
			assert (root.contains(rec));
			root.remove(rec);
			rec.deallocate();
		}
		assert (!root.iterator().hasNext());
		assert (!root.iterator(null, null, Index.DESCENT_ORDER).hasNext());
		assert (!root.iterator(null, null, Index.ASCENT_ORDER).hasNext());
		logI("Elapsed time for deleting " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");
		db.close();
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	static class Record2 extends ODbObject {
		String strKey;
		int intKey;
	};
}
