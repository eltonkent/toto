package toto.db.odb;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedMap;

import test.toto.ToToTestCase;
import toto.db.odb.FieldIndex;
import toto.db.odb.IODbObject;
import toto.db.odb.Index;
import toto.db.odb.Key;
import toto.db.odb.MultidimensionalComparator;
import toto.db.odb.MultidimensionalIndex;
import toto.db.odb.ODb;
import toto.db.odb.ODbBitIndex;
import toto.db.odb.ODbComparator;
import toto.db.odb.ODbFactory;
import toto.db.odb.ODbList;
import toto.db.odb.ODbMap;
import toto.db.odb.ODbObject;
import toto.db.odb.ODbSet;
import toto.db.odb.OdbSortedCollection;
import toto.io.file.FileUtils;
import toto.io.file.SDCardUtils;


public class ODbCollectionsTest extends ToToTestCase {
	static class IndexRecord extends ODbObject {
		String strKey;
		long intKey;
	};

	static class TestIndexIndices extends ODbObject {
		Index strIndex;
		Index intIndex;
	}

	final static int nRecords = 100;
	static int pagePoolSize = 32 * 1024 * 1024;
	String path = SDCardUtils.getDirectory().toString() + "/ODB/";
	String dbName;

	public ODbCollectionsTest() {
		dbName = path + "ODbUnitTest.odb";
		File file = new File(path);
		if (!file.exists()) {
			file.mkdir();
		}
		file = new File(dbName);
		if (file.exists()) {
			file.delete();
		}
	}



	public void testODbList() {
		ODb db = ODbFactory.createOODB();
		db.open(dbName);

		ODbList<ListRecord> root = (ODbList) db.getRootObject();
		if (root == null) {
			root = db.<ListRecord> createList();
			db.setRootObject(root);
		}
		long start = System.currentTimeMillis();
		int i;
		for (i = 0; i < nRecords / 2; i++) {
			ListRecord rec = new ListRecord();
			rec.i = i * 2;
			rec.test = RandomString.randomAlphanumeric(100);
			root.add(rec);
		}
		for (i = 1; i < nRecords; i += 2) {
			ListRecord rec = new ListRecord();
			rec.i = i;
			rec.test = RandomString.randomAlphanumeric(100);
			root.add(i, rec);
		}

		db.commit();
		// db.gc();
		logI("Elapsed time for inserting " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		root = db.getRootObject();
		for (i = 0; i < nRecords; i++) {
			ListRecord rec = (ListRecord) root.get(i);
			assert (rec.i == i);
		}
		logI("Elapsed time for performing " + nRecords + " gets: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		Iterator iterator = root.iterator();
		for (i = 0; iterator.hasNext(); i++) {
			ListRecord rec = (ListRecord) iterator.next();
			assert (rec.i == i);
		}
		assert (i == nRecords);
		logI("Elapsed time for iterating through " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		/*
		 * for (i = nRecords; --i >= 0;) { Record rec = (Record)root.remove(i);
		 * assert(rec.i == i); rec.deallocate(); } /*
		 */
		for (i = 0; i < nRecords; i++) {
			ListRecord rec = (ListRecord) root.remove(0);
			assert (rec.i == i);
			rec.deallocate();
		}
		/**/
		assert (!root.iterator().hasNext());
		logI("Elapsed time for deleting " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");
		db.close();
		try {
			FileUtils.emptyDirectory(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static class ListRecord extends ODbObject {
		private int i;
		private String test;
	};

	public void testODbMap() {
		ODb db = ODbFactory.createOODB();
		db.open(dbName);

		boolean populate = true;
		MapIndices root = (MapIndices) db.getRootObject();
		if (root == null) {
			root = new MapIndices();
			root.strMap = db.createMap(String.class);
			root.intMap = db.createMap(long.class);
			root.objMap = db.createMap(MapRecordKey.class);
			db.setRootObject(root);
		}
		SortedMap intMap = root.intMap;
		SortedMap strMap = root.strMap;
		ODbMap objMap = root.objMap;
		long start = System.currentTimeMillis();
		long key = 1999;
		int i;
		if (objMap.size() == 0) {
			for (i = 0; i < nRecords; i++) {
				MapRecord rec = new MapRecord();
				key = (3141592621L * key + 2718281829L) % 1000000007L;
				rec.intKey = key;
				rec.strKey = Long.toString(key);
				intMap.put(new Long(rec.intKey), rec);
				strMap.put(rec.strKey, rec);
				MapRecordKey rc = new MapRecordKey();
				rc.intKey = key;
				objMap.put(rc, rec);
				assert (intMap.get(new Long(rec.intKey)) == rec);
				assert (strMap.get(rec.strKey) == rec);
				assert (objMap.get(rc) == rec);
			}

			db.commit();
			logI("Elapsed time for inserting " + nRecords + " records: "
					+ (System.currentTimeMillis() - start) + " milliseconds");
		}
		start = System.currentTimeMillis();
		key = 1999;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			MapRecord rec1 = (MapRecord) intMap.get(new Long(key));
			MapRecord rec2 = (MapRecord) strMap.get(Long.toString(key));
			MapRecord rec3 = (MapRecord) objMap.get(new MapRecordKey(key));
			assert (rec1 != null && rec1 == rec2 && rec1 == rec3);
		}
		logI("Elapsed time for performing " + nRecords * 3 + " map searches: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		Iterator iterator = intMap.values().iterator();
		key = Long.MIN_VALUE;
		for (i = 0; iterator.hasNext(); i++) {
			MapRecord rec = (MapRecord) iterator.next();
			assert (rec.intKey >= key);
			key = rec.intKey;
		}
		assert (i == nRecords);
		iterator = intMap.keySet().iterator();
		key = Long.MIN_VALUE;
		for (i = 0; iterator.hasNext(); i++) {
			long v = ((Long) iterator.next()).longValue();
			assert (v >= key);
			key = v;
		}
		assert (i == nRecords);
		iterator = strMap.values().iterator();
		String strKey = "";
		for (i = 0; iterator.hasNext(); i++) {
			MapRecord rec = (MapRecord) iterator.next();
			assert (rec.strKey.compareTo(strKey) >= 0);
			strKey = rec.strKey;
		}
		assert (i == nRecords);
		iterator = strMap.keySet().iterator();
		strKey = "";
		for (i = 0; iterator.hasNext(); i++) {
			String v = (String) iterator.next();
			assert (v.compareTo(strKey) >= 0);
			strKey = v;
		}
		assert (i == nRecords);
		iterator = objMap.entrySet().iterator();
		key = Long.MIN_VALUE;
		for (i = 0; iterator.hasNext(); i++) {
			Map.Entry entry = (Map.Entry) iterator.next();
			MapRecord v = (MapRecord) entry.getValue();
			MapRecordKey k = (MapRecordKey) entry.getKey();
			assert (k.intKey == v.intKey);
			assert (k.intKey >= key);
			key = k.intKey;
		}
		assert (i == nRecords);
		logI("Elapsed time for 5 iterations through " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		if (!populate) {
			start = System.currentTimeMillis();
			key = 1999;
			for (i = 0; i < nRecords; i++) {
				key = (3141592621L * key + 2718281829L) % 1000000007L;
				MapRecord rem1 = (MapRecord) intMap.remove(new Long(key));
				MapRecord rem2 = (MapRecord) strMap.remove(Long.toString(key));
				Map.Entry e = objMap.getEntry(new MapRecordKey(key));
				MapRecord rem3 = (MapRecord) objMap.remove(e.getKey());
				assert (rem1 != null && rem1 == rem2 && rem1 == rem3 && rem3 == e
						.getValue());
				rem1.deallocate();
				((MapRecordKey) e.getKey()).deallocate();
			}
			assert (intMap.entrySet().size() == 0);
			assert (!intMap.entrySet().iterator().hasNext());
			assert (strMap.entrySet().size() == 0);
			assert (!strMap.entrySet().iterator().hasNext());
			assert (objMap.entrySet().size() == 0);
			assert (!objMap.entrySet().iterator().hasNext());
			logI("Elapsed time for deleting " + nRecords + " records: "
					+ (System.currentTimeMillis() - start) + " milliseconds");
		}
		db.close();
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	static class MapRecord extends ODbObject {
		String strKey;
		long intKey;

		public int compare(Object o) {
			long key = ((MapRecord) o).intKey;
			return intKey < key ? -1 : intKey == key ? 0 : 1;
		}
	};

	static class MapRecordKey extends ODbObject implements Comparable {
		long intKey;

		MapRecordKey() {
		}

		MapRecordKey(long val) {
			intKey = val;
		}

		public int compareTo(Object o) {
			long key = ((MapRecordKey) o).intKey;
			return intKey < key ? -1 : intKey == key ? 0 : 1;
		}
	};

	static class MapIndices extends ODbObject {
		ODbMap strMap;
		ODbMap intMap;
		ODbMap objMap;
	}

	public void testODbSet() {
		ODb db = ODbFactory.createOODB();
		db.open(dbName);
		SetIndices root = (SetIndices) db.getRootObject();
		if (root == null) {
			root = new SetIndices();
			root.set = db.createSet();
			root.index = db.createIndex(long.class, true);
			db.setRootObject(root);
		}
		int i, n, m;
		long key = 1999;
		long start = System.currentTimeMillis();
		for (i = 0, n = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			int r = (int) (key % maxInitSize);
			ODbSet ps = db.createScalableSet(r);
			for (int j = 0; j < r; j++) {
				ps.add(new SetRecord(j));
				n += 1;
			}
			root.set.add(ps);
			root.index.put(new Key(key), ps);
		}
		db.commit();
		logI("Elapsed time for inserting " + n + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		key = 1999;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			int r = (int) (key % maxInitSize);
			ODbSet ps = (ODbSet) root.index.get(new Key(key));
			assert (root.set.contains(ps));
			assert (ps.size() == r);
		}
		logI("Elapsed time for performing " + nRecords * 2
				+ " index searches: " + (System.currentTimeMillis() - start)
				+ " milliseconds");

		start = System.currentTimeMillis();
		Iterator iterator = root.set.iterator();
		for (i = 0; iterator.hasNext();) {
			ODbSet ps = (ODbSet) iterator.next();
			Iterator si = ps.iterator();
			int sum = 0;
			while (si.hasNext()) {
				sum += ((SetRecord) si.next()).id;
			}
			assert (ps.size() * (ps.size() - 1) / 2 == sum);
			i += ps.size();
		}
		assert (i == n);
		logI("Elapsed time for iterating through " + n + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		key = 1999;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			int r = (int) (key % maxInitSize);
			ODbSet ps = (ODbSet) root.index.get(new Key(key));
			assert (ps.size() == r);
			for (int j = r; j < r * 2; j++) {
				SetRecord rec = new SetRecord(j);
				ps.add(rec);
				ps.add(rec);
			}
		}
		db.commit();
		logI("Elapsed time for adding " + n + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		iterator = root.set.iterator();
		for (i = 0; iterator.hasNext();) {
			ODbSet ps = (ODbSet) iterator.next();
			Iterator si = ps.iterator();
			int sum = 0;
			while (si.hasNext()) {
				sum += ((SetRecord) si.next()).id;
			}
			assert (ps.size() * (ps.size() - 1) / 2 == sum);
			i += ps.size();
		}
		assert (i == n * 2);
		logI("Elapsed time for iterating through " + n * 2 + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		key = 1999;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			ODbSet ps = (ODbSet) root.index.remove(new Key(key));
			int r = (int) (key % maxInitSize) * 2;
			assert (ps.size() == r);
			root.set.remove(ps);
			for (iterator = ps.iterator(); iterator.hasNext(); ((IODbObject) iterator
					.next()).deallocate())
				;
			ps.deallocate();
		}
		assert (root.set.size() == 0);
		assert (root.index.size() == 0);
		assert (!root.set.iterator().hasNext());
		assert (!root.index.iterator().hasNext());
		logI("Elapsed time for deleting " + n * 2 + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");
		db.close();
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	final static int maxInitSize = 100;

	static class SetIndices extends ODbObject {
		ODbSet set;
		Index index;
	}

	static class SetRecord extends ODbObject {
		private int id;

		SetRecord() {
		}

		SetRecord(int id) {
			this.id = id;
		}
	}

	public void testOdbTree() {
		ODb db = ODbFactory.createOODB();
		db.open(dbName, pagePoolSize);
		TreeRecordList root = (TreeRecordList) db.getRootObject();
		if (root == null) {
			root = new TreeRecordList();
			root.list = db.createSortedCollection(new TreeNameComparator(),
					true);
			db.setRootObject(root);
		}
		OdbSortedCollection list = root.list;
		long key = 1999;
		int i;
		long start = System.currentTimeMillis();
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			String str = Long.toString(key);
			int m = str.length() / 2;
			String firstName = str.substring(0, m);
			String lastName = str.substring(m);
			int age = (int) key % 100;
			TreeRecord p = new TreeRecord(firstName, lastName, age);
			list.add(p);
		}
		db.commit();
		logI("Elapsed time for inserting " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		key = 1999;
		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			String str = Long.toString(key);
			int m = str.length() / 2;
			TreeNameRecord name = new TreeNameRecord();
			int age = (int) key % 100;
			name.first = str.substring(0, m);
			name.last = str.substring(m);

			TreeRecord p = (TreeRecord) list.get(name);
			assert (p != null);
			assert (list.contains(p));
			assert (p.age == age);
		}
		logI("Elapsed time for performing " + nRecords + " index searches: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		Iterator iterator = list.iterator();
		TreeNameRecord name = new TreeNameRecord();
		name.first = name.last = "";
		ODbComparator comparator = list.getComparator();
		for (i = 0; iterator.hasNext(); i++) {
			TreeRecord p = (TreeRecord) iterator.next();
			assert (comparator.compareMemberWithKey(p, name) > 0);
			name.first = p.firstName;
			name.last = p.lastName;
			iterator.remove();
		}
		assert (i == nRecords);
		logI("Elapsed time for removing " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");
		assert (!list.iterator().hasNext());
		db.close();
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	class TreeNameRecord {
		private String first;
		String last;
	}

	static class TreeRecord extends ODbObject {
		String firstName;
		String lastName;
		int age;

		public TreeRecord() {
		}

		TreeRecord(String firstName, String lastName, int age) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.age = age;
		}
	}

	static class TreeRecordList extends ODbObject {
		public TreeRecordList() {

		}

		OdbSortedCollection list;
	}

	static class TreeNameComparator extends ODbComparator {
		public TreeNameComparator() {

		}

		public int compareMembers(Object m1, Object m2) {
			TreeRecord p1 = (TreeRecord) m1;
			TreeRecord p2 = (TreeRecord) m2;
			int diff = p1.firstName.compareTo(p2.firstName);
			if (diff != 0) {
				return diff;
			}
			return p1.lastName.compareTo(p2.lastName);
		}

		public int compareMemberWithKey(Object mbr, Object key) {
			TreeRecord p = (TreeRecord) mbr;
			TreeNameRecord name = (TreeNameRecord) key;
			int diff = p.firstName.compareTo(name.first);
			if (diff != 0) {
				return diff;
			}
			return p.lastName.compareTo(name.last);
		}

	}

	public void testKDTree() {
		ODb db = ODbFactory.createOODB();
		db.open(dbName, pagePoolSize);
		boolean populate = true;

		MultidimensionalIndex<IODbObject> index = (MultidimensionalIndex<IODbObject>) db
				.getRootObject();
		if (index == null) {
			index = db.createMultidimensionalIndex(new StockComparator());
			db.setRootObject(index);
		}
		Random r;
		long start;
		if (index.size() == 0) {
			r = new Random(2007);
			start = System.currentTimeMillis();
			for (int i = 0; i < nRecords; i++) {
				Stock s = getRandomStock(r);
				index.add(s);
			}
			db.commit();
			logI("Elapsed time for inserting " + nRecords + " records: "
					+ (System.currentTimeMillis() - start) + " milliseconds");
			logI("Tree height: " + index.getHeight());
			if (index.size() > 1
					&& index.getHeight() / Math.log(index.size())
							* Math.log(2.0) > KD_TREE_OPTIMIZATION_THRESHOLD) {
				start = System.currentTimeMillis();
				index.optimize();
				logI("New tree height: " + index.getHeight());
				logI("Elapsed time for tree optimization: "
						+ (System.currentTimeMillis() - start)
						+ " milliseconds");
			}
		}
		start = System.currentTimeMillis();
		r = new Random(2007);
		stockCount = FIRST_STOCK_ID;
		for (int i = 0; i < nRecords; i++) {
			Stock s = getRandomStock(r);
			List<IODbObject> result = index.queryByExample(s);
			assert (result.size() >= 1);
			for (int j = 0; j < result.size(); j++) {
				assert (s.eq((Stock) result.get(j)));
			}
		}
		logI("Elapsed time for performing " + nRecords
				+ " query by example searches: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		r = new Random(2007);
		Random r2 = new Random(2008);
		long total = 0;
		for (int i = 0; i < nRecords; i++) {
			Stock s = getRandomStock(r);
			Stock min = new Stock();
			Stock max = new Stock();

			min.price = s.price - (float) MAX_PRICE / EPSILON;
			min.volume = s.volume - MAX_VOLUME / EPSILON;

			max.price = s.price + (float) MAX_PRICE / EPSILON;
			max.volume = s.volume + MAX_VOLUME / EPSILON;

			Iterator iterator = index.iterator(min, max);
			int n = 0;
			while (iterator.hasNext()) {
				s = (Stock) iterator.next();
				assert (min.le(s));
				assert (s.le(max));
				n += 1;
			}
			assert (n >= 1);
			total += n;
		}
		logI("Elapsed time for performing " + nRecords
				+ " range query by example searches: "
				+ (System.currentTimeMillis() - start)
				+ " milliseconds, select " + total + " objects");
		start = System.currentTimeMillis();
		Iterator iterator = index.iterator();
		int n = 0;
		while (iterator.hasNext()) {
			iterator.next();
			n += 1;
		}
		assert (n == nRecords);
		logI("Elapsed time for iterating through " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		if (!populate) {
			start = System.currentTimeMillis();
			iterator = index.iterator();
			n = 0;
			while (iterator.hasNext()) {
				Stock s = (Stock) iterator.next();
				iterator.remove();
				s.deallocate();
				n += 1;
			}
			db.commit();
			assert (n == nRecords);
			logI("Elapsed time for removing " + nRecords + " records: "
					+ (System.currentTimeMillis() - start) + " milliseconds");

			assert (!index.iterator().hasNext());
			index.clear();
			assert (index.size() == 0);
		}
		db.close();
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	static class Stock extends ODbObject {
		String symbol;
		float price;
		int volume;

		boolean eq(Stock s) {
			return symbol.equals(s.symbol) && price == s.price
					&& volume == s.volume;
		}

		boolean le(Stock s) {
			return price <= s.price && volume <= s.volume;
		}
	};

	static class StockComparator extends MultidimensionalComparator<IODbObject> {
		public int compare(IODbObject m1, IODbObject m2, int i) {
			Stock s1 = (Stock) m1;
			Stock s2 = (Stock) m2;
			switch (i) {
			case 0:
				if (s1.symbol == null && s2.symbol == null) {
					return EQ;
				} else if (s1.symbol == null) {
					return LEFT_UNDEFINED;
				} else if (s2.symbol == null) {
					return RIGHT_UNDEFINED;
				} else {
					int diff = s1.symbol.compareTo(s2.symbol);
					return diff < 0 ? LT : diff == 0 ? EQ : GT;
				}
			case 1:
				return s1.price < s2.price ? LT : s1.price == s2.price ? EQ
						: GT;
			case 2:
				return s1.volume < s2.volume ? LT : s1.volume == s2.volume ? EQ
						: GT;
			default:
				throw new IllegalArgumentException();
			}
		}

		public int getNumberOfDimensions() {
			return 3;
		}

		public IODbObject cloneField(IODbObject obj, int i) {
			Stock src = (Stock) obj;
			Stock clone = new Stock();
			switch (i) {
			case 0:
				clone.symbol = src.symbol;
				break;
			case 1:
				clone.price = src.price;
				break;
			case 2:
				clone.volume = src.volume;
				break;
			default:
				throw new IllegalArgumentException();
			}
			return clone;
		}
	}

	public static Stock getRandomStock(Random r) {
		Stock s = new Stock();
		// s.symbol = Integer.toHexString(++stockCount);
		s.symbol = Integer.toHexString(r.nextInt(MAX_SYMBOLS));
		s.price = (float) r.nextInt(MAX_PRICE * 10) / 10;
		s.volume = r.nextInt(MAX_VOLUME);
		return s;
	}

	static final int MAX_SYMBOLS = 100;
	static final int MAX_PRICE = 100;
	static final int MAX_VOLUME = 100;
	static final int EPSILON = 100;
	static final int KD_TREE_OPTIMIZATION_THRESHOLD = 3;

	static final int FIRST_STOCK_ID = 0xA0000;
	static int stockCount = FIRST_STOCK_ID;

	public void testBitIndex() {
		ODb db = ODbFactory.createOODB();
		db.open(dbName, pagePoolSize);
		Catalogue root = (Catalogue) db.getRootObject();
		if (root == null) {
			root = new Catalogue();
			root.optionIndex = db.createBitIndex();
			root.modelIndex = db.createFieldIndex(Car.class, "model", true);
			db.setRootObject(root);
		}
		ODbBitIndex index = root.optionIndex;
		long start = System.currentTimeMillis();
		long rnd = 1999;
		int i, n;

		int selectedOptions = Car.TURBO | Car.DISEL | Car.FWD | Car.ABS
				| Car.EBD | Car.ESP | Car.AIR_COND | Car.HATCHBACK
				| Car.CLASS_C;
		int unselectedOptions = Car.AUTOMATIC;

		for (i = 0, n = 0; i < nRecords; i++) {
			rnd = (3141592621L * rnd + 2718281829L) % 1000000007L;
			int options = (int) rnd;
			Car car = new Car();
			car.model = Long.toString(rnd);
			car.options = options;
			root.modelIndex.put(car);
			root.optionIndex.put(car, options);
			if ((options & selectedOptions) == selectedOptions
					&& (options & unselectedOptions) == 0) {
				n += 1;
			}
		}
		logI("Elapsed time for inserting " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		Iterator iterator = root.optionIndex.iterator(selectedOptions,
				unselectedOptions);
		for (i = 0; iterator.hasNext(); i++) {
			Car car = (Car) iterator.next();
			assert ((car.options & selectedOptions) == selectedOptions);
			assert ((car.options & unselectedOptions) == 0);
		}
		logI("Number of selected cars: " + i);
		assert (i == n);
		logI("Elapsed time for bit search through " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		iterator = root.modelIndex.iterator();
		for (i = 0, n = 0; iterator.hasNext(); i++) {
			Car car = (Car) iterator.next();
			root.optionIndex.remove(car);
			car.deallocate();
		}
		root.optionIndex.clear();
		logI("Elapsed time for removing " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		db.close();
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	static class Car extends ODbObject {
		int hps;
		int maxSpeed;
		int timeTo100;
		int options;
		String model;
		String vendor;
		String specification;

		static final int CLASS_A = 0x00000001;
		static final int CLASS_B = 0x00000002;
		static final int CLASS_C = 0x00000004;
		static final int CLASS_D = 0x00000008;

		static final int UNIVERAL = 0x00000010;
		static final int SEDAN = 0x00000020;
		static final int HATCHBACK = 0x00000040;
		static final int MINIWAN = 0x00000080;

		static final int AIR_COND = 0x00000100;
		static final int CLIMANT_CONTROL = 0x00000200;
		static final int SEAT_HEATING = 0x00000400;
		static final int MIRROR_HEATING = 0x00000800;

		static final int ABS = 0x00001000;
		static final int ESP = 0x00002000;
		static final int EBD = 0x00004000;
		static final int TC = 0x00008000;

		static final int FWD = 0x00010000;
		static final int REAR_DRIVE = 0x00020000;
		static final int FRONT_DRIVE = 0x00040000;

		static final int GPS_NAVIGATION = 0x00100000;
		static final int CD_RADIO = 0x00200000;
		static final int CASSETTE_RADIO = 0x00400000;
		static final int LEATHER = 0x00800000;

		static final int XEON_LIGHTS = 0x01000000;
		static final int LOW_PROFILE_TIRES = 0x02000000;
		static final int AUTOMATIC = 0x04000000;

		static final int DISEL = 0x10000000;
		static final int TURBO = 0x20000000;
		static final int GASOLINE = 0x40000000;
	};

	static class Catalogue extends ODbObject {
		// public Catalogue(){
		//
		// }
		FieldIndex modelIndex;
		ODbBitIndex optionIndex;
	};


}
