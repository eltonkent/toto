package toto.db.odb;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import test.toto.ToToTestCase;
import test.toto.xc.xml.TestDAO;
import toto.db.odb.FieldIndex;
import toto.db.odb.Index;
import toto.db.odb.Indexable;
import toto.db.odb.ODb;
import toto.db.odb.ODbBlob;
import toto.db.odb.ODbFactory;
import toto.db.odb.ODbListener;
import toto.db.odb.ODbObject;
import toto.db.odb.ODbSQL;
import toto.db.odb.Query;
import toto.db.odb.RandomAccessInputStream;
import toto.db.odb.RandomAccessOutputStream;
import toto.io.file.FileUtils;
import toto.io.file.SDCardUtils;

public class ODbTest extends ToToTestCase {

	String path = getTestStorageLocation()+File.separator+"ODB/";
	String dbName;

	public ODbTest() {
		dbName = path + "ODbUnitTest.odb";
		File file = new File(path);
		if (!file.exists()) {
			file.mkdir();
		}
		file = new File(dbName);
		if (file.exists()) {
			file.delete();
		}
	}

	public void testCustomCollections() {
		ODb odb = ODbFactory.createOODB();
		odb.open(dbName);
		TestCustomCol root = odb.getRootObject();
		if (root == null) {
			root = new TestCustomCol();
			root.data = odb.<TestDAO> createList();
			odb.setRootObject(root);
		}
		TestDAO dao = new TestDAO();
		dao.setAddress("Mobifluence Interactive");
		dao.setAge(3);
		dao.setName("Rage Toolkit");
		// root.data=new ArrayList<String>();
		root.data.add(dao);
		odb.commit();
		odb.close();
		odb.open(dbName);
		root = odb.getRootObject();
		assertEquals(3, root.data.get(0).getAge());
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	public static class TestCustomCol {
		List<TestDAO> data;

		public TestCustomCol() {

		}
	}

	/**
	 * ODb insert
	 */
	public void testInsert() {
		ODb odb = ODbFactory.createOODB();
		odb.open(dbName);
		Index<TestDAO> idx = odb.getRootObject();
		if (idx == null) {
			idx = odb.createIndex(String.class, true);
			odb.setRootObject(idx);
		}
		TestDAO test = new TestDAO();
		test.setAddress("rage, mobifluence interactive");
		test.setAge(3);
		test.setName("Rage Toolkit");
		idx.set("rageobj", test);

		test = new TestDAO();
		test.setAddress("appaddle, mobifluence interactive");
		test.setAge(1);
		test.setName("appaddle");
		idx.set("appaddleobj", test);

		logH("ODb", "Object Inserted");
		odb.commit();
		odb.close();
		try {
			FileUtils.emptyDirectory(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void testBlob() throws IOException {
		ODb storage = ODbFactory.createOODB();
		storage.open(dbName);
		ODbBlob blob = storage.createBlob();
		storage.setRootObject(blob);
		OutputStream os = blob.getOutputStream();
		byte[] arr = new byte[1024 * 1024];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = (byte) (i % 256);
		}
		// test target
		os.write(arr);
		blob = (ODbBlob) storage.getRootObject();
		InputStream is = blob.getInputStream();
		byte[] arr2 = arr.clone();
		is.read(arr2);
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != arr2[i]) {
				fail();
			}
		}
		FileUtils.emptyDirectory(new File(path));
	}

	public void testODbSQL() {
		ODb db = ODbFactory.createOODB();
		db.open(dbName);
		Indices root = (Indices) db.getRootObject();
		if (root == null) {
			root = new Indices();
			root.strIndex = db.createFieldIndex(Record.class, "strKey", true);
			root.intIndex = db.createFieldIndex(Record.class, "intKey", true);
			root.dateIndex = db
					.createFieldIndex(Record.class, "dateKey", false);
			db.setRootObject(root);
		}
		FieldIndex intIndex = root.intIndex;
		FieldIndex strIndex = root.strIndex;
		FieldIndex dateIndex = root.dateIndex;
		long start = System.currentTimeMillis();
		long begin = start;
		long key = 1999;
		Iterator iterator;
		int i;
		for (i = 0; i < nRecords; i++) {
			Record rec = new Record();
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			rec.intKey = key;
			rec.strKey = Long.toString(key);
			rec.dateKey = new Date();
			intIndex.put(rec);
			strIndex.put(rec);
			dateIndex.put(rec);
		}
		db.commit();
		long end = System.currentTimeMillis();
		logI("Elapsed time for inserting " + nRecords + " records: "
				+ (end - start) + " milliseconds");

		start = System.currentTimeMillis();
		key = 1999;
		Query q1 = db.createQuery();
		q1.prepare(Record.class, "strKey=?");
		q1.addIndex("strKey", strIndex);

		Query q2 = db.createQuery();
		q2.addIndex("intKey", intIndex);
		q2.prepare(Record.class, "intKey=?");

		Query q3 = db.createQuery();
		q3.addIndex("dateKey", dateIndex);
		q3.prepare(Record.class, "dateKey between ? and ?");

		for (i = 0; i < nRecords; i++) {
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			q1.setParameter(1, Long.toString(key));
			iterator = q1.execute(intIndex.iterator());
			Record rec1 = (Record) iterator.next();
			assert (!iterator.hasNext());

			q2.setIntParameter(1, key);
			iterator = q2.execute(strIndex.iterator());
			Record rec2 = (Record) iterator.next();
			assert (!iterator.hasNext());

			assert (rec1 != null && rec1 == rec2);
		}
		logI("Elapsed time for performing " + nRecords * 2
				+ " index searches: " + (System.currentTimeMillis() - start)
				+ " milliseconds");

		start = System.currentTimeMillis();
		iterator = intIndex.select("strKey=string(intKey)");
		key = Long.MIN_VALUE;
		for (i = 0; iterator.hasNext(); i++) {
			Record rec = (Record) iterator.next();
			assert (rec.intKey >= key);
			key = rec.intKey;
		}
		assert (i == nRecords);
		logI("Elapsed time for iterating through " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		iterator = strIndex.select("(intKey and 1023) = 0 order by intKey");
		key = Long.MIN_VALUE;
		for (i = 0; iterator.hasNext(); i++) {
			Record rec = (Record) iterator.next();
			assert (rec.intKey >= key);
			key = rec.intKey;
		}
		logI("Elapsed time for ordering " + i + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		iterator = intIndex.select("strKey=string(intKey)");
		key = Long.MIN_VALUE;
		for (i = 0; iterator.hasNext(); i++) {
			Record rec = (Record) iterator.next();
			assert (rec.intKey >= key);
			key = rec.intKey;
		}
		assert (i == nRecords);
		logI("Elapsed time for iterating through " + nRecords + " records: "
				+ (System.currentTimeMillis() - start) + " milliseconds");

		start = System.currentTimeMillis();
		q3.setParameter(1, new Date(begin));
		q3.setParameter(2, new Date(end));
		iterator = q3.execute(dateIndex.iterator());
		long curr = begin;
		i = 0;
		while (iterator.hasNext()) {
			Record rec = (Record) iterator.next();
			assert (rec.dateKey.getTime() >= curr);
			curr = rec.dateKey.getTime();
			i += 1;
		}
		logI("Elapsed time for iterating through date index for " + i
				+ " records: " + (System.currentTimeMillis() - start)
				+ " milliseconds");
		assert (i == nRecords);

		start = System.currentTimeMillis();
		iterator = intIndex.iterator(null, null, Index.ASCENT_ORDER);
		while (iterator.hasNext()) {
			Record rec = (Record) iterator.next();
			rec.deallocate();
		}
		intIndex.deallocate();
		strIndex.deallocate();
		dateIndex.deallocate();
		logI("Elapsed time for deleting all data: "
				+ (System.currentTimeMillis() - start) + " milliseconds");
		db.close();
		try {
			FileUtils.emptyDirectory(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testODbSQLJoin() {
		ODb storage = ODbFactory.createOODB();
		storage.open(dbName);
		storage.getSqlOptimizerParameters().enableCostBasedOptimization = true;
		ODbSQL db = new ODbSQL(storage);

		long start = System.currentTimeMillis();

		for (int i = 0; i < nLabels; i++) {
			RecordLabel label = new RecordLabel();
			label.name = "Label" + i;
			label.email = "contact@" + label.name + ".com";
			label.address = "Country, City, Street";
			label.phone = "+1 123-456-7890";
			db.addRecord(label);
		}

		for (int i = 0; i < nAlbums; i++) {
			Album album = new Album();
			album.name = "Album" + i;
			album.label = db.<RecordLabel> select(RecordLabel.class,
					"name='Label" + (i % nLabels) + "'").first();
			album.genre = GENRES[i % GENRES.length];
			album.release = new Date();
			db.addRecord(album);

			for (int j = 0; j < nTracksPerAlbum; j++) {
				Track track = new Track();
				track.no = j + 1;
				track.name = "Track" + j;
				track.album = album;
				track.duration = 3.5f;
				db.addRecord(track);
			}
		}
		storage.commit();
		logI("Elapsed time for database initialization: "
				+ (System.currentTimeMillis() - start));

		QueryExecutionListener listener = new QueryExecutionListener();
		storage.setListener(listener);
		Query<Track> query = db.<Track> prepare(Track.class,
				"no > 0 and album.label.name=?");
		start = System.currentTimeMillis();
		int nTracks = 0;
		for (int i = 0; i < nLabels; i++) {
			query.setParameter(1, "Label" + i);
			for (Track t : query) {
				nTracks += 1;
			}
		}
		logI("Elapsed time for searching of " + nTracks + " tracks: "
				+ (System.currentTimeMillis() - start));
		assert (nTracks == nAlbums * nTracksPerAlbum);

		String prev = "";
		int n = 0;
		for (RecordLabel label : db.<RecordLabel> select(RecordLabel.class,
				"order by name")) {
			assert (prev.compareTo(label.name) < 0);
			prev = label.name;
			n += 1;
		}
		assert (n == nLabels);

		n = 0;
		prev = "zzz";
		for (RecordLabel label : db.<RecordLabel> select(RecordLabel.class,
				"name like 'Label%' order by name desc")) {
			assert (prev.compareTo(label.name) > 0);
			prev = label.name;
			n += 1;
		}
		assert (n == nLabels);

		n = 0;
		for (RecordLabel label : db.<RecordLabel> select(RecordLabel.class,
				"name in ('Label1', 'Label2', 'Label3')")) {
			n += 1;
		}
		assert (n == 3);

		n = 0;
		for (RecordLabel label : db
				.<RecordLabel> select(
						RecordLabel.class,
						"(name = 'Label1' or name = 'Label2' or name = 'Label3') and email like 'contact@%'")) {
			n += 1;
		}
		assert (n == 3);

		Query<RecordLabel> query2 = db.<RecordLabel> prepare(RecordLabel.class,
				"phone like '+1%' and name in ?");
		ArrayList<String> list = new ArrayList<String>(nLabels);
		for (int i = 0; i < nLabels; i++) {
			list.add("Label" + i);
		}
		n = 0;
		query2.setParameter(1, list);
		for (RecordLabel label : query2) {
			assert (label.name.equals("Label" + n++));
		}
		assert (n == nLabels);

		n = 0;
		String labelName = "Label2";
		for (Track track : db
				.<Track> select(
						Track.class,
						"album.label.name='Label1' or album.label.name='Label2' order by album.label.name desc")) {
			assert (track.album.label.name.equals(labelName) || track.album.label.name
					.equals(labelName = "Label1"));
			n += 1;
		}
		assert (n == nAlbums * nTracksPerAlbum * 2 / nLabels);

		Query<Album> query3 = db.<Album> prepare(Album.class, "label=?");
		n = 0;
		for (RecordLabel label : db.<RecordLabel> getRecords(RecordLabel.class)) {
			query3.setParameter(1, label);
			for (Album album : query3) {
				n += 1;
			}
		}
		assert (n == nAlbums);

		n = 0;
		for (Album album : db.<Album> select(Album.class,
				"name in ('Album1', 'Album2', 'Album3', 'Album4', 'Album5')")) {
			n += 1;
			assert (album.name.equals("Album" + n));
		}
		assert (n == 5);

		Query<Album> query4 = db.<Album> prepare(Album.class, "genre in ?");
		ArrayList<String> genres = new ArrayList<String>();
		for (String genre : GENRES) {
			genres.add(genre.toLowerCase());
		}
		query4.setParameter(1, genres);
		n = 0;
		for (Album album : query4) {
			n += 1;
		}
		assert (n == nAlbums);

		assert (listener.nSequentialSearches == 0);
		assert (listener.nSorts == 1);

		db.dropTable(Track.class);
		db.dropTable(Album.class);
		db.dropTable(RecordLabel.class);
		storage.close();
		try {
			FileUtils.emptyDirectory(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	class QueryExecutionListener extends ODbListener {
		int nSequentialSearches;
		int nSorts;

		public void sequentialSearchPerformed(Class table, String query) {
			nSequentialSearches += 1;
		}

		public void sortResultSetPerformed(Class table, String query) {
			nSorts += 1;
		}
	}


	static class Record extends ODbObject {
		String strKey;
		long intKey;
		Date dateKey;
	};



	static class Indices extends ODbObject {
		FieldIndex strIndex;
		FieldIndex intIndex;
		FieldIndex dateIndex;
	}

	final static int nRecords = 100;
	static int pagePoolSize = 32 * 1024 * 1024;

	static class Track {
		// public Track() {
		//
		// }

		int no;

		@Indexable
		Album album;

		@Indexable
		String name;

		float duration;
	}

	static class Album extends ODbObject {
		// public Album() {
		//
		// }

		@Indexable(unique = true, caseInsensitive = true)
		String name;

		@Indexable
		RecordLabel label;

		@Indexable(thick = true, caseInsensitive = true)
		String genre;

		Date release;
	}

	static class RecordLabel extends ODbObject {
		// public RecordLabel() {
		// }

		@Indexable(unique = true)
		String name;

		String email;
		String phone;
		String address;
	}

	static final int nLabels = 100;
	static final int nAlbums = 100;
	static final int nTracksPerAlbum = 10;

	static final String GENRES[] = { "Rock", "Pop", "Jazz", "R&B", "Folk",
			"Classic" };

	static class Access implements Comparable {
		long pos;
		int size;

		public int compareTo(Object o) {
			long diff = pos - ((Access) o).pos;
			return diff < 0 ? -1 : diff == 0 ? 0 : 1;
		}
	}

	public void testRandomBlob() throws Exception {
		ODb db = ODbFactory.createOODB();
		db.open(dbName, pagePoolSize);
		Access[] map = initializeRandomAccessMap();
		ODbBlob blob = (ODbBlob) db.getRootObject();
		if (blob == null) {
			blob = db.createRandomAccessBlob();
			db.setRootObject(blob);
			storeContent(map, blob);
		}
		inspectContent(map, blob);
		db.close();
		FileUtils.emptyDirectoryQuietly(new File(path));
	}

	final static int nIterations = 100;
	final static long fileSize = 8L * 1024 * 1024 * 1024;
	final static int maxRecordSize = 1000;

	Access[] initializeRandomAccessMap() {
		Access[] map = new Access[nIterations];
		Access[] sortedMap = new Access[nIterations];
		long key = 1999;
		for (int i = 0; i < nIterations; i++) {
			map[i] = new Access();
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			map[i].pos = key % fileSize;
			key = (3141592621L * key + 2718281829L) % 1000000007L;
			map[i].size = (int) (key % maxRecordSize);
			sortedMap[i] = map[i];
		}
		Arrays.sort(sortedMap);
		for (int i = 1; i < sortedMap.length; i++) {
			if (sortedMap[i - 1].pos + sortedMap[i - 1].size > sortedMap[i].pos) {
				sortedMap[i - 1].size = (int) (sortedMap[i].pos - sortedMap[i - 1].pos);
			}
		}
		return map;
	}

	void inspectContent(Access[] map, ODbBlob blob) throws Exception {
		RandomAccessInputStream in = blob.getInputStream();
		for (int i = 0; i < nIterations; i++) {
			int size = map[i].size;
			long pos = map[i].pos;
			byte filler = (byte) (pos & 0xFF);
			byte[] content = new byte[size];
			in.setPosition(pos);
			int rc = in.read(content, 0, size);
			assert (rc == size);
			for (int j = 0; j < size; j++) {
				if (content[j] != filler) {
					logI(i + ": filler = " + filler + ", content[" + j + "]="
							+ content[j]);
				}
				assert (content[j] == filler);
			}
		}
	}

	void storeContent(Access[] map, ODbBlob blob) throws Exception {
		RandomAccessOutputStream out = blob.getOutputStream(0);
		for (int i = 0; i < nIterations; i++) {
			int size = map[i].size;
			long pos = map[i].pos;
			byte filler = (byte) (pos & 0xFF);
			byte[] content = new byte[size];
			for (int j = 0; j < size; j++) {
				content[j] = filler;
			}
			out.setPosition(pos);
			out.write(content, 0, size);
		}
	}
}
