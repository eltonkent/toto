LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_LDLIBS +=  -llog 
LOCAL_MODULE    := IO
LOCAL_SRC_FILES := NativeBufferUtils.cpp \
		
include $(BUILD_SHARED_LIBRARY)
