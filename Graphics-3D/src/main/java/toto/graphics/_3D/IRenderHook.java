package toto.graphics._3D;

public abstract interface IRenderHook {
	public abstract void beforeRendering(int paramInt);

	public abstract void afterRendering(int paramInt);

	public abstract void setCurrentObject3D(T3DObject paramObject3D);

	public abstract void setCurrentShader(GLSLShader paramGLSLShader);

	public abstract void setTransparency(float paramFloat);

	public abstract void onDispose();

	public abstract boolean repeatRendering();
}
