package toto.graphics._3D;

abstract interface BufferUtil {
	public abstract void copy(float[] paramArrayOfFloat,
			FloatBufferWrapper paramFloatBufferWrapper);
}
