package toto.graphics._3D;

public abstract interface ITextureEffect {
	public abstract void init(Texture paramTexture);

	public abstract void apply(int[] paramArrayOfInt1, int[] paramArrayOfInt2);

	public abstract boolean containsAlpha();
}
