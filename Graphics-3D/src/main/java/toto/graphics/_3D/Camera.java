package toto.graphics._3D;

import java.io.Serializable;

/**
 * The Camera represents the position and direction of the camera/viewer in the
 * current scene. It also contains information about the current field of view
 * (FOV). One should keep in mind that the rotation matrix of the camera is
 * actually a rotation matrix which will be aplied to all objects in the world.
 * This is important when chosing the rotation angle for the camera: A (virtual)
 * rotation of the camera around an axis using the angle w leads to the same
 * results as if the world would rotate around this axis using -w. So the angle
 * given to the rotate?()-methods is actually the angle by which the world
 * rotates around the camera when the camera is viewed as static. If you dislike
 * this behaviour, you may use the rotateCamera?()-methods instead which will
 * take care of it.
 */
public class Camera implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int CAMERA_MOVEIN = 1;
	public static final int CAMERA_MOVEOUT = 2;
	public static final int CAMERA_MOVEDOWN = 3;
	public static final int CAMERA_MOVEUP = 4;
	public static final int CAMERA_MOVELEFT = 6;
	public static final int CAMERA_MOVERIGHT = 5;
	public static final int CAMERA_DONT_MOVE = 7;
	public static final boolean SLIDE = true;
	public static final boolean DONT_SLIDE = false;
	public static final int ELLIPSOID_ALIGNED = 0;
	public static final int ELLIPSOID_TRANSFORMED = 1;
	float scaleX;
	float scaleY;
	float divx;
	float divy;
	protected Matrix backMatrix = new Matrix();

	protected Matrix lookAtTmp = null;
	protected float backBx;
	protected float backBy;
	protected float backBz;
	private int ellipsoidMode = 0;
	private float cameraFOV;
	private float yFOV;
	private float lowerLimit;
	private float higherLimit;
	private float[] workMatrix = new float[16];
	private Matrix projectionMatrix = new Matrix();

	public Camera() {
		this.cameraFOV = Config.defaultCameraFOV;
		this.yFOV = -1.0F;
		this.scaleX = 1.0F;
		this.scaleY = 1.0F;
		this.divx = 1.0F;
		this.divy = 1.0F;
		this.lowerLimit = 0.5F;
		this.higherLimit = 1.5F;
	}

	void calcFOV(int width, int height) {
		float fov = this.cameraFOV / 2.0F;

		if ((!Config.autoMaintainAspectRatio) || (this.yFOV != -1.0F)) {
			this.divx = fov;
			this.divy = fov;
			if (this.yFOV != -1.0F)
				this.divy = (this.yFOV / 2.0F);
		} else {
			this.divx = fov;
			this.divy = (fov * (height / width));
		}
		this.scaleX = (width / (2.0F * this.divx));
		this.scaleY = (height / (2.0F * this.divy));

		if (!Config.glIgnoreNearPlane) {
			this.divx /= Config.nearPlane;
			this.divy /= Config.nearPlane;
		}
	}

	public SimpleVector getPosition() {
		return SimpleVector.create(this.backBx, this.backBy, this.backBz);
	}

	public SimpleVector getPosition(SimpleVector toFill) {
		toFill.set(this.backBx, this.backBy, this.backBz);
		return toFill;
	}

	public SimpleVector getXAxis() {
		return this.backMatrix.getXAxis();
	}

	public SimpleVector getYAxis() {
		return this.backMatrix.getYAxis();
	}

	public SimpleVector getZAxis() {
		return this.backMatrix.getZAxis();
	}

	public SimpleVector getDirection() {
		return getVector(2);
	}

	public SimpleVector getDirection(SimpleVector toFill) {
		return getVector(2, toFill);
	}

	public SimpleVector getUpVector() {
		SimpleVector s = getVector(1);
		s.scalarMul(-1.0F);
		return s;
	}

	public SimpleVector getSideVector() {
		return getVector(0);
	}

	private SimpleVector getVector(int pos) {
		return getVector(pos, SimpleVector.create());
	}

	private SimpleVector getVector(int pos, SimpleVector toFill) {
		float x1 = this.backMatrix.mat[0][pos];
		float y1 = this.backMatrix.mat[1][pos];
		float z1 = this.backMatrix.mat[2][pos];
		float n = 1.0F / (float) Math.sqrt(x1 * x1 + y1 * y1 + z1 * z1);
		toFill.set(x1 * n, y1 * n, z1 * n);
		return toFill;
	}

	public float convertRADAngleIntoFOV(float angle) {
		angle = (float) (Math.tan(angle / 2.0D) * 2.0D);
		return angle;
	}

	public float convertDEGAngleIntoFOV(float angle) {
		double a = angle / 180.0D * 3.141592653589793D;
		angle = (float) (Math.tan(a / 2.0D) * 2.0D);
		return angle;
	}

	public void setFOVLimits(float lower, float higher) {
		this.lowerLimit = lower;
		this.higherLimit = higher;
	}

	public float getMaxFOV()
	/*     */{
		/* 305 */
		return this.higherLimit;
		/*     */
	}

	public float getMinFOV()
	/*     */{
		/* 314 */
		return this.lowerLimit;
		/*     */
	}

	public void setFOV(float tanFOV) {
		if (tanFOV > this.higherLimit) {
			tanFOV = this.higherLimit;
		} else if (tanFOV < this.lowerLimit) {
			tanFOV = this.lowerLimit;
		}

		this.cameraFOV = tanFOV;
	}

	public void setYFOV(float tanFOV) {
		if (tanFOV != -1.0F) {
			if (tanFOV > this.higherLimit) {
				tanFOV = this.higherLimit;
			} else if (tanFOV < this.lowerLimit) {
				tanFOV = this.lowerLimit;
			}
		}

		this.yFOV = tanFOV;
	}

	public float getFOV()
	/*     */{
		/* 363 */
		return this.cameraFOV;
		/*     */
	}

	public float getYFOV()
	/*     */{
		/* 375 */
		return this.yFOV;
		/*     */
	}

	public void adjustFovToNearPlane() {
		if (!Config.glIgnoreNearPlane) {
			this.cameraFOV *= Config.nearPlane;
			if (this.yFOV != -1.0F)
				this.yFOV *= Config.nearPlane;
		}
	}

	public void increaseFOV(float inc) {
		this.cameraFOV += inc;
		if (this.cameraFOV > this.higherLimit) {
			this.cameraFOV = this.higherLimit;
		} else if (this.cameraFOV < this.lowerLimit)
			this.cameraFOV = this.lowerLimit;
	}

	public void decreaseFOV(float dec) {
		this.cameraFOV -= dec;
		if (this.cameraFOV > this.higherLimit) {
			this.cameraFOV = this.higherLimit;
		} else if (this.cameraFOV < this.lowerLimit)
			this.cameraFOV = this.lowerLimit;
	}

	public void setFOVtoDefault() {
		this.cameraFOV = Config.defaultCameraFOV;
	}

	public void lookAt(SimpleVector lookAt) {
		if (this.lookAtTmp == null) {
			this.lookAtTmp = new Matrix();
		}

		float lavx = lookAt.x - this.backBx;
		float lavy = lookAt.y - this.backBy;
		float lavz = lookAt.z - this.backBz;

		float FIXER = 1.0E-020F;

		if ((lavx == 0.0F) && (lavz == 0.0F)) {
			lavx += 1.0E-020F;
		}

		float n = (float) Math.sqrt(lavx * lavx + lavy * lavy + lavz * lavz);
		if (n != 0.0F) {
			lavx /= n;
			lavy /= n;
			lavz /= n;
		}

		Matrix cameraMatMAT = this.lookAtTmp;
		this.lookAtTmp.setIdentity();
		float[][] cameraMat = cameraMatMAT.mat;

		cameraMat[0][1] = 0.0F;
		cameraMat[1][1] = 1.0F;
		cameraMat[2][1] = 0.0F;

		cameraMat[0][2] = lavx;
		cameraMat[1][2] = lavy;
		cameraMat[2][2] = lavz;

		float x1 = 0.0F;
		float y1 = 1.0F;
		float z1 = 0.0F;

		float vx1 = lavx;
		float vy1 = lavy;
		float vz1 = lavz;

		float resx = y1 * vz1 - z1 * vy1;
		float resy = z1 * vx1 - x1 * vz1;
		float resz = x1 * vy1 - y1 * vx1;

		n = (float) Math.sqrt(resx * resx + resy * resy + resz * resz);
		if (n != 0.0F) {
			resx /= n;
			resy /= n;
			resz /= n;
		}

		float resx2 = vy1 * resz - vz1 * resy;
		float resy2 = vz1 * resx - vx1 * resz;
		float resz2 = vx1 * resy - vy1 * resx;

		n = (float) Math.sqrt(resx2 * resx2 + resy2 * resy2 + resz2 * resz2);
		if (n != 0.0F) {
			resx2 /= n;
			resy2 /= n;
			resz2 /= n;
		}

		cameraMat[0][0] = resx;
		cameraMat[1][0] = resy;
		cameraMat[2][0] = resz;

		cameraMat[0][1] = resx2;
		cameraMat[1][1] = resy2;
		cameraMat[2][1] = resz2;

		cameraMatMAT.orthonormalize();
		this.backMatrix.setTo(cameraMatMAT);
	}

	public void align(T3DObject object) {
		Matrix m = object.getRotationMatrix().cloneMatrix();
		m.scalarMul(1.0F / object.getScale());
		this.backMatrix = m.invert3x3();
	}

	public void setPositionToCenter(T3DObject object) {
		SimpleVector center = object.getTransformedCenter();
		this.backBx = center.x;
		this.backBy = center.y;
		this.backBz = center.z;
	}

	public void setPosition(SimpleVector pos) {
		this.backBx = pos.x;
		this.backBy = pos.y;
		this.backBz = pos.z;
	}

	public void setPosition(float x, float y, float z) {
		this.backBx = x;
		this.backBy = y;
		this.backBz = z;
	}

	public void setOrientation(SimpleVector dir, SimpleVector up) {
		up.scalarMul(-1.0F);
		this.backMatrix.setOrientation(dir, up, false);
		up.scalarMul(-1.0F);
	}

	public SimpleVector transform(SimpleVector vertex) {
		Matrix mat2 = this.backMatrix;
		Matrix mat3 = new Matrix();

		mat3.mat[3][0] = (-this.backBx);
		mat3.mat[3][1] = (-this.backBy);
		mat3.mat[3][2] = (-this.backBz);

		mat3.matMul(mat2);

		float s22 = mat3.mat[2][2];
		float s12 = mat3.mat[1][2];
		float s02 = mat3.mat[0][2];
		float s00 = mat3.mat[0][0];
		float s10 = mat3.mat[1][0];
		float s11 = mat3.mat[1][1];
		float s21 = mat3.mat[2][1];
		float s20 = mat3.mat[2][0];
		float s01 = mat3.mat[0][1];

		float bx = mat3.mat[3][0];
		float by = mat3.mat[3][1];
		float bz = mat3.mat[3][2];

		float x1 = vertex.x;
		float y1 = vertex.y;
		float z1 = vertex.z;

		float p1x = x1 * s00 + y1 * s10 + z1 * s20 + bx;
		float p1y = x1 * s01 + y1 * s11 + z1 * s21 + by;
		float p1z = x1 * s02 + y1 * s12 + z1 * s22 + bz;

		return SimpleVector.create(p1x, p1y, p1z);
	}

	public void moveCamera(int mode, float speed) {
		float mul = -1.0F;
		if ((mode & 0x1) == 1) {
			mul = 1.0F;
		}
		mul *= speed;
		int pos = 2 - ((mode + 1) / 2 - 1);
		this.backBx += this.backMatrix.mat[0][pos] * mul;
		this.backBy += this.backMatrix.mat[1][pos] * mul;
		this.backBz += this.backMatrix.mat[2][pos] * mul;
	}

	public void moveCamera(SimpleVector direction, float speed) {
		this.backBx += direction.x * speed;
		this.backBy += direction.y * speed;
		this.backBz += direction.z * speed;
	}

	public void rotateCameraAxis(SimpleVector axis, float angle) {
		this.backMatrix.rotateAxis(axis, -angle);
	}

	public void rotateCameraX(float angle) {
		this.backMatrix.rotateX(-angle);
	}

	public void rotateCameraY(float angle) {
		this.backMatrix.rotateY(-angle);
	}

	public void rotateCameraZ(float angle) {
		this.backMatrix.rotateZ(-angle);
	}

	public void rotateAxis(SimpleVector axis, float angle) {
		this.backMatrix.rotateAxis(axis, angle);
	}

	public void rotateX(float angle) {
		this.backMatrix.rotateX(angle);
	}

	public void rotateY(float angle) {
		this.backMatrix.rotateY(angle);
	}

	public void rotateZ(float angle) {
		this.backMatrix.rotateZ(angle);
	}

	public Matrix getBack()
	/*     */{
		/* 781 */
		return this.backMatrix;
		/*     */
	}

	public void setBack(Matrix mat)
	/*     */{
		/* 791 */
		this.backMatrix = mat;
		/*     */
	}

	public void setEllipsoidMode(int mode)
	/*     */{
		/* 807 */
		this.ellipsoidMode = mode;
		/*     */
	}

	public int getEllipsoidMode()
	/*     */{
		/* 818 */
		return this.ellipsoidMode;
		/*     */
	}

	public Matrix getProjectionMatrix(FrameBuffer buffer) {
		return getProjectionMatrix(buffer, Config.glIgnoreNearPlane ? 1.0F
				: Config.nearPlane, Config.farPlane);
	}

	public Matrix getProjectionMatrix(FrameBuffer buffer, float nearPlane,
			float farPlane) {
		float h = buffer.getHeight();
		float w = buffer.getWidth();

		if (buffer.renderTarget != null) {
			if ((buffer.virtualHeight > 0) && (buffer.virtualWidth > 0)) {
				h = buffer.virtualHeight;
				w = buffer.virtualWidth;
			} else {
				h = buffer.renderTarget.height;
				w = buffer.renderTarget.width;
			}
		}

		Camera cam = this;
		float fov = cam.getFOV();
		float fovy = 0.0F;

		if (Config.autoMaintainAspectRatio)
			fovy = fov * (h / w);
		else {
			fovy = fov;
		}

		if (cam.getYFOV() != -1.0F) {
			fovy = cam.getYFOV();
		}

		float far = farPlane;
		float near = nearPlane;
		fov *= near;
		fovy *= near;

		float top = fovy * 0.5F;
		float bottom = -fovy * 0.5F;
		float left = -fov * 0.5F;
		float right = fov * 0.5F;

		frustum(this.workMatrix, left, right, bottom, top, near, far);
		this.projectionMatrix.setDump(this.workMatrix);
		this.projectionMatrix.transformToGL();

		return this.projectionMatrix;
	}

	private void frustum(float[] matrix, float left, float right, float bottom,
			float top, float near, float far) {
		float rWidth = 1.0F / (right - left);
		float rHeight = 1.0F / (top - bottom);
		float rDepth = 1.0F / (near - far);
		float x = 2.0F * (near * rWidth);
		float y = 2.0F * (near * rHeight);
		float a = 2.0F * ((right + left) * rWidth);
		float b = (top + bottom) * rHeight;
		float c = (far + near) * rDepth;
		float d = 2.0F * (far * near * rDepth);
		matrix[0] = x;
		matrix[1] = 0.0F;
		matrix[2] = 0.0F;
		matrix[3] = 0.0F;
		matrix[4] = 0.0F;
		matrix[5] = y;
		matrix[6] = 0.0F;
		matrix[7] = 0.0F;
		matrix[8] = a;
		matrix[9] = b;
		matrix[10] = c;
		matrix[11] = -1.0F;
		matrix[12] = 0.0F;
		matrix[13] = 0.0F;
		matrix[14] = d;
		matrix[15] = 0.0F;
	}
}
