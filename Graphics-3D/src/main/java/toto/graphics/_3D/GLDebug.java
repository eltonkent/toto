package toto.graphics._3D;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

class GLDebug implements InvocationHandler {
	private GL gl = null;
	private StringBuilder sb = new StringBuilder();

	public static GL create(GL gl) {
		if (gl == null) {
			return null;
		}
		Logger.log("Creating dynamic proxy for gl!");
		return (GL) Proxy.newProxyInstance(gl.getClass().getClassLoader(), gl
				.getClass().getInterfaces(), new GLDebug(gl));
	}

	private GLDebug(GL gl) {
		this.gl = gl;
	}

	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		this.sb.setLength(0);
		this.sb.append(method.getName()).append('(');
		boolean first = true;
		for (Object o : args) {
			if (!first) {
				this.sb.append(", ");
			}
			first = false;
			this.sb.append(o.toString());
		}
		this.sb.append(')');

		long time = System.nanoTime();
		Object obj = method.invoke(this.gl, args);
		this.sb.append(" took ").append(System.nanoTime() - time).append("ns");
		Logger.log(this.sb.toString());
		if (obj != null) {
			Logger.log("return value: " + obj.toString());
		}
		int error = ((GL10) this.gl).glGetError();
		if (error != 0) {
			Logger.log("!! ERROR !! - " + error, 1);
		}
		return obj;
	}
}
