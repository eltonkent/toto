package toto.graphics._3D;

final class CollisionInfo {
	SimpleVector eRadius;
	SimpleVector r3Velocity;
	SimpleVector r3Pos;
	SimpleVector intersectionPoint;
	SimpleVector eSpaceVelocity;
	SimpleVector eSpaceBasePoint;
	T3DObject collisionObject = null;

	boolean foundCollision = false;

	boolean collision = false;

	boolean isPartOfCollision = false;
	float nearestDistance;
	Matrix addTransMat = null;

	Matrix addRotMat = null;
	float r3Destx;
	float r3Desty;
	float r3Destz;
	float invERadiusx;
	float invERadiusy;
	float invERadiusz;
	private float invERadiusOrgx;
	private float invERadiusOrgy;
	private float invERadiusOrgz;

	float getMaxRadius() {
		return Math.max(Math.max(this.eRadius.x, this.eRadius.y),
				this.eRadius.z);
	}

	void setScale(float scale) {
		this.invERadiusx = (this.invERadiusOrgx * scale);
		this.invERadiusy = (this.invERadiusOrgy * scale);
		this.invERadiusz = (this.invERadiusOrgz * scale);
	}

	void calculateInverseAndDest() {
		if (this.eRadius != null) {
			this.invERadiusOrgx = (1.0F / this.eRadius.x);
			this.invERadiusOrgy = (1.0F / this.eRadius.y);
			this.invERadiusOrgz = (1.0F / this.eRadius.z);

			this.invERadiusx = this.invERadiusOrgx;
			this.invERadiusy = this.invERadiusOrgy;
			this.invERadiusz = this.invERadiusOrgz;
		}
		recalcDest();
	}

	void recalcDest() {
		if ((this.r3Pos != null) && (this.r3Velocity != null)) {
			this.r3Destx = (this.r3Pos.x + this.r3Velocity.x);
			this.r3Desty = (this.r3Pos.y + this.r3Velocity.y);
			this.r3Destz = (this.r3Pos.z + this.r3Velocity.z);
		}
	}

	void setIntersectionPoint(SimpleVector sv) {
		if (this.intersectionPoint == null) {
			this.intersectionPoint = new SimpleVector();
		}
		this.intersectionPoint.set(sv);
	}
}
