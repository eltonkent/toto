package toto.graphics._3D;

public abstract interface IVertexController {
	public static final boolean ALTER_SOURCE_MESH = true;
	public static final boolean PRESERVE_SOURCE_MESH = false;

	public abstract boolean init(Mesh paramMesh, boolean paramBoolean);

	public abstract boolean setup();

	public abstract void apply();

	public abstract SimpleVector[] getSourceMesh();

	public abstract SimpleVector[] getSourceNormals();

	public abstract SimpleVector[] getDestinationMesh();

	public abstract SimpleVector[] getDestinationNormals();

	public abstract int getMeshSize();

	public abstract void updateMesh();

	public abstract void refreshMeshData();

	public abstract void destroy();

	public abstract void cleanup();

	public abstract int[] getPolygonIDs(int paramInt1, int paramInt2);
}
