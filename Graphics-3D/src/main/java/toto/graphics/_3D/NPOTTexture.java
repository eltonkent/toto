package toto.graphics._3D;

import toto.graphics.color.RGBColor;

public class NPOTTexture extends Texture {
	private static final long serialVersionUID = 1L;

	public NPOTTexture(int width, int height, RGBColor col) {
		super(width, height, col, true);
		this.nPot = true;
	}
}
