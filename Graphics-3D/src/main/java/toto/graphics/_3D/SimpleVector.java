package toto.graphics._3D;

import java.io.Serializable;

public class SimpleVector implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Matrix globalTmpMat = new Matrix();

	public static final SimpleVector ORIGIN = new SimpleVector(0.0F, 0.0F, 0.0F);
	public float x;
	public float y;
	public float z;
	private static final SimpleVector DOWN = new SimpleVector(0.0F, 1.0F, 0.0F);

	private float[] array = null;

	public SimpleVector(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public SimpleVector(double x, double y, double z) {
		this.x = ((float) x);
		this.y = ((float) y);
		this.z = ((float) z);
	}

	public SimpleVector(SimpleVector s) {
		this.x = s.x;
		this.y = s.y;
		this.z = s.z;
	}

	public SimpleVector() {
		this.x = 0.0F;
		this.y = 0.0F;
		this.z = 0.0F;
	}

	public SimpleVector(float[] vector) {
		if (vector.length == 3) {
			this.x = vector[0];
			this.y = vector[1];
			this.z = vector[2];
		} else {
			this.x = 0.0F;
			this.y = 0.0F;
			this.z = 0.0F;
			Logger.log("Source-array needs to have a length of 3", 0);
		}
	}

	public static SimpleVector create() {
		return new SimpleVector(0.0F, 0.0F, 0.0F);
	}

	public static SimpleVector create(SimpleVector sv) {
		return new SimpleVector(sv.x, sv.y, sv.z);
	}

	public static SimpleVector create(float x, float y, float z) {
		return new SimpleVector(x, y, z);
	}

	public void set(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void set(SimpleVector s) {
		this.x = s.x;
		this.y = s.y;
		this.z = s.z;
	}

	public float[] toArray() {
		if (this.array == null) {
			this.array = new float[] { this.x, this.y, this.z };
		} else {
			this.array[0] = this.x;
			this.array[1] = this.y;
			this.array[2] = this.z;
		}

		return this.array;
	}

	public String toString() {
		return "(" + this.x + "," + this.y + "," + this.z + ")";
	}

	public boolean equals(Object obj) {
		if ((obj instanceof SimpleVector)) {
			SimpleVector sv = (SimpleVector) obj;
			return (sv.x == this.x) && (sv.y == this.y) && (sv.z == this.z);
		}
		return false;
	}

	public int hashCode() {
		return (int) (this.x * 100.0F + this.y * 10.0F + this.z);
	}

	public SimpleVector rotate(SimpleVector rotVector) {
		float oldX = this.x;
		float oldY = this.y;
		float oldZ = this.z;

		float sinX = (float) Math.sin(rotVector.x);
		float cosX = (float) Math.cos(rotVector.x);
		float sinY = (float) Math.sin(rotVector.y);
		float cosY = (float) Math.cos(rotVector.y);
		float sinZ = (float) Math.sin(rotVector.z);
		float cosZ = (float) Math.cos(rotVector.z);

		float rotatedX = oldX * cosZ - oldY * sinZ;
		float rotatedY = oldX * sinZ + oldY * cosZ;

		oldX = rotatedX;
		oldY = rotatedY;

		rotatedX = oldX * cosY - oldZ * sinY;
		float rotatedZ = oldX * sinY + oldZ * cosY;

		oldZ = rotatedZ;

		rotatedY = oldY * cosX - oldZ * sinX;
		rotatedZ = oldY * sinX + oldZ * cosX;
		return create(rotatedX, rotatedY, rotatedZ);
	}

	public void rotate(Matrix mat) {
		float[][] matmat = mat.mat;
		float xr = this.x * matmat[0][0] + this.y * matmat[1][0] + this.z
				* matmat[2][0];
		float yr = this.x * matmat[0][1] + this.y * matmat[1][1] + this.z
				* matmat[2][1];
		float zr = this.x * matmat[0][2] + this.y * matmat[1][2] + this.z
				* matmat[2][2];
		this.x = xr;
		this.y = yr;
		this.z = zr;
	}

	public void rotateAxis(SimpleVector axis, float angle) {
		synchronized (globalTmpMat) {
			globalTmpMat.setIdentity();
			globalTmpMat.rotateAxis(axis, angle);
			matMul(globalTmpMat);
		}
	}

	public SimpleVector reflect(SimpleVector faceNormal) {
		SimpleVector cross = calcCross(faceNormal);
		cross = faceNormal.calcCross(cross);
		cross.scalarMul(2.0F);
		cross = cross.calcSub(this);
		return cross;
	}

	public void rotateX(float angle) {
		float oldY = this.y;
		float oldZ = this.z;

		float sinX = (float) Math.sin(angle);
		float cosX = (float) Math.cos(angle);

		this.y = (oldY * cosX - oldZ * sinX);
		this.z = (oldY * sinX + oldZ * cosX);
	}

	public void rotateY(float angle) {
		float oldX = this.x;
		float oldZ = this.z;

		float sinY = (float) Math.sin(angle);
		float cosY = (float) Math.cos(angle);

		this.x = (oldX * cosY - oldZ * sinY);
		this.z = (oldX * sinY + oldZ * cosY);
	}

	public void rotateZ(float angle) {
		float oldY = this.y;
		float oldX = this.x;

		float sinZ = (float) Math.sin(angle);
		float cosZ = (float) Math.cos(angle);

		this.x = (oldX * cosZ - oldY * sinZ);
		this.y = (oldX * sinZ + oldY * cosZ);
	}

	public SimpleVector normalize() {
		float n = (float) Math.sqrt(this.x * this.x + this.y * this.y + this.z
				* this.z);
		if (n != 0.0F) {
			float dn = 1.0F / n;
			return create(this.x * dn, this.y * dn, this.z * dn);
		}
		return create(0.0F, 0.0F, 0.0F);
	}

	public SimpleVector normalize(SimpleVector sv) {
		if (sv == null) {
			sv = create();
		}
		float n = (float) Math.sqrt(this.x * this.x + this.y * this.y + this.z
				* this.z);
		if (n != 0.0F) {
			float dn = 1.0F / n;
			sv.set(this.x * dn, this.y * dn, this.z * dn);
		} else {
			sv.set(0.0F, 0.0F, 0.0F);
		}
		return sv;
	}

	public float length() {
		return (float) Math.sqrt(this.x * this.x + this.y * this.y + this.z
				* this.z);
	}

	public SimpleVector calcCross(SimpleVector vec) {
		float vx1 = vec.x;
		float vy1 = vec.y;
		float vz1 = vec.z;

		float resx = this.y * vz1 - this.z * vy1;
		float resy = this.z * vx1 - this.x * vz1;
		float resz = this.x * vy1 - this.y * vx1;
		return create(resx, resy, resz);
	}

	public float calcDot(SimpleVector vec) {
		return this.x * vec.x + this.y * vec.y + this.z * vec.z;
	}

	public SimpleVector calcSub(SimpleVector vec) {
		return create(this.x - vec.x, this.y - vec.y, this.z - vec.z);
	}

	public SimpleVector calcAdd(SimpleVector vec) {
		return new SimpleVector(this.x + vec.x, this.y + vec.y, this.z + vec.z);
	}

	public float distance(SimpleVector pos) {
		return lengthBetween(pos);
	}

	float lengthBetween(SimpleVector vec) {
		float x1 = this.x - vec.x;
		float y1 = this.y - vec.y;
		float z1 = this.z - vec.z;
		return (float) Math.sqrt(x1 * x1 + y1 * y1 + z1 * z1);
	}

	public float calcAngleFast(SimpleVector vec) {
		return acosFast(_calcAngle(vec));
	}

	public float calcAngle(SimpleVector vec) {
		return (float) Math.acos(_calcAngle(vec));
	}

	private float _calcAngle(SimpleVector vec) {
		float dot = this.x * vec.x + this.y * vec.y + this.z * vec.z;
		float lt = this.x * this.x + this.y * this.y + this.z * this.z;
		float lv = vec.x * vec.x + vec.y * vec.y + vec.z * vec.z;

		dot /= (float) Math.sqrt(lt * lv);

		if (dot < -1.0F) {
			dot = -1.0F;
		} else if (dot > 1.0F) {
			dot = 1.0F;
		}

		return dot;
	}

	private float acosFast(float value) {
		return (-0.6981317F * value * value - 0.8726646F) * value + 1.570796F;
	}

	public void scalarMul(float scalar) {
		this.x *= scalar;
		this.y *= scalar;
		this.z *= scalar;
	}

	public void matMul(Matrix mat) {
		float[][] matmat = mat.mat;
		float[] matmat0 = matmat[0];
		float[] matmat1 = matmat[1];
		float[] matmat2 = matmat[2];
		float[] matmat3 = matmat[3];

		float xr = this.x * matmat0[0] + this.y * matmat1[0] + this.z
				* matmat2[0] + matmat3[0];
		float yr = this.x * matmat0[1] + this.y * matmat1[1] + this.z
				* matmat2[1] + matmat3[1];
		float zr = this.x * matmat0[2] + this.y * matmat1[2] + this.z
				* matmat2[2] + matmat3[2];

		this.x = xr;
		this.y = yr;
		this.z = zr;
	}

	public void add(SimpleVector vec) {
		this.x += vec.x;
		this.y += vec.y;
		this.z += vec.z;
	}

	void add(float vecx, float vecy, float vecz) {
		this.x += vecx;
		this.y += vecy;
		this.z += vecz;
	}

	public void sub(SimpleVector vec) {
		this.x -= vec.x;
		this.y -= vec.y;
		this.z -= vec.z;
	}

	public void makeEqualLength(SimpleVector vec) {
		float lVec = vec.length();
		float lThis = length();
		if (lThis > lVec) {
			SimpleVector r = normalize();
			r.scalarMul(lVec);
			this.x = r.x;
			this.y = r.y;
			this.z = r.z;
		}
	}

	public Matrix getRotationMatrix() {
		return getRotationMatrix(new Matrix(), DOWN);
	}

	public Matrix getRotationMatrix(SimpleVector up) {
		return getRotationMatrix(new Matrix(), up);
	}

	public Matrix getRotationMatrix(Matrix mat) {
		return getRotationMatrix(mat, DOWN);
	}

	public Matrix getRotationMatrix(Matrix mat, SimpleVector up) {
		float lavx = this.x;
		float lavy = this.y;
		float lavz = this.z;

		float FIXER = 1.0E-020F;

		if ((lavx == 0.0F) && (lavz == 0.0F)) {
			lavx += 1.0E-020F;
		}

		float n = (float) Math.sqrt(lavx * lavx + lavy * lavy + lavz * lavz);
		if (n != 0.0F) {
			lavx /= n;
			lavy /= n;
			lavz /= n;
		}

		float[][] cameraMat = mat.mat;

		cameraMat[1][0] = 0.0F;
		cameraMat[1][1] = 1.0F;
		cameraMat[1][2] = 0.0F;

		cameraMat[2][0] = lavx;
		cameraMat[2][1] = lavy;
		cameraMat[2][2] = lavz;

		float x1 = up.x;
		float y1 = up.y;
		float z1 = up.z;

		float vx1 = lavx;
		float vy1 = lavy;
		float vz1 = lavz;

		float resx = y1 * vz1 - z1 * vy1;
		float resy = z1 * vx1 - x1 * vz1;
		float resz = x1 * vy1 - y1 * vx1;

		float resx2 = 0.0F;
		float resy2 = 0.0F;
		float resz2 = 0.0F;

		n = (float) Math.sqrt(resx * resx + resy * resy + resz * resz);
		if (n != 0.0F) {
			resx /= n;
			resy /= n;
			resz /= n;
		}

		resx2 = vy1 * resz - vz1 * resy;
		resy2 = vz1 * resx - vx1 * resz;
		resz2 = vx1 * resy - vy1 * resx;

		n = (float) Math.sqrt(resx2 * resx2 + resy2 * resy2 + resz2 * resz2);
		if (n != 0.0F) {
			resx2 /= n;
			resy2 /= n;
			resz2 /= n;
		}

		cameraMat[0][0] = resx;
		cameraMat[0][1] = resy;
		cameraMat[0][2] = resz;

		cameraMat[1][0] = resx2;
		cameraMat[1][1] = resy2;
		cameraMat[1][2] = resz2;

		mat.orthonormalize();
		return mat;
	}
}
