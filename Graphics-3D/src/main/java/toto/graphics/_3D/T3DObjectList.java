package toto.graphics._3D;

import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.NoSuchElementException;

final class T3DObjectList implements Serializable {
	private static final long serialVersionUID = 1L;
	private T3DObject[] objList = null;

	private int SIZE = 100;

	private int count = 0;

	T3DObjectList() {
		this.objList = new T3DObject[this.SIZE];
	}

	T3DObjectList(int size) {
		this.SIZE = size;
		this.objList = new T3DObject[size];
	}

	int size() {
		return this.count;
	}

	void clear() {
		for (int i = 0; i < this.count; i++) {
			this.objList[i] = null;
		}
		if (this.objList.length > 1000) {
			this.objList = new T3DObject[this.SIZE];
		}
		this.count = 0;
	}

	T3DObject[] toArray() {
		T3DObject[] res = new T3DObject[this.count];
		System.arraycopy(this.objList, 0, res, 0, this.count);
		return res;
	}

	void addElement(T3DObject obj) {
		if (this.count >= this.objList.length) {
			T3DObject[] tmp = new T3DObject[this.SIZE
					+ this.objList.length];
			System.arraycopy(this.objList, 0, tmp, 0, this.objList.length);
			this.objList = tmp;
		}
		this.objList[this.count] = obj;
		this.count += 1;
	}

	T3DObject[] getInternalArray() {
		/* 65 */
		return this.objList;
		/*     */
	}

	T3DObject elementAt(int i) {
		return this.objList[i];
	}

	void removeElementAt(int i) {
		if (i + 1 < this.count) {
			System.arraycopy(this.objList, i + 1, this.objList, i, this.count
					- i - 1);
		}
		this.count -= 1;
		this.objList[this.count] = null;
	}

	boolean contains(T3DObject obj) {
		int end = size();
		for (int i = 0; i < end; i++) {
			if (this.objList[i].equals(obj)) {
				return true;
			}
		}
		return false;
	}

	boolean removeElement(T3DObject obj) {
		int end = size();
		for (int i = 0; i < end; i++) {
			if (((Object) this.objList[i]).equals(obj)) {
				removeElementAt(i);
				return true;
			}
		}
		return false;
	}

	Enumeration<T3DObject> elements() {
		return new Enumeration() {
			int cnt = 0;
			int sc = T3DObjectList.this.count;

			public boolean hasMoreElements() {
				if (T3DObjectList.this.count != this.sc) {
					throw new ConcurrentModificationException();
				}

				return this.cnt < T3DObjectList.this.count;
			}

			public T3DObject nextElement() {
				if (T3DObjectList.this.count != this.sc) {
					throw new ConcurrentModificationException();
				}

				if (this.cnt < T3DObjectList.this.count) {
					return T3DObjectList.this.objList[(this.cnt++)];
				}
				throw new NoSuchElementException("ObjList Enumeration");
			}
		};
	}
}
