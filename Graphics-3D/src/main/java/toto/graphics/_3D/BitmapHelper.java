/*    */
package toto.graphics._3D;

/*    */
/*    */

import java.io.BufferedInputStream;
import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;

/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */

/*    */
/*    */public class BitmapHelper
/*    */{
	/* 20 */private static int callCount = 0;

	/*    */
	/*    */
	public static Bitmap loadImage(InputStream is)
	/*    */throws Exception
	/*    */{
		/* 32 */
		if (!(is instanceof BufferedInputStream)) {
			/* 33 */
			is = new BufferedInputStream(is, 2048);
			/*    */
		}
		/* 35 */
		cleanUp();
		/* 36 */
		Bitmap image = BitmapFactory.decodeStream(is);
		/* 37 */
		is.close();
		/* 38 */
		return image;
		/*    */
	}

	/*    */
	/*    */
	public static Bitmap convert(Drawable dable)
	/*    */{
		/* 52 */
		cleanUp();
		/* 53 */
		int width = dable.getIntrinsicWidth();
		/* 54 */
		int height = dable.getIntrinsicHeight();
		/* 55 */
		Bitmap bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		/* 56 */
		Canvas canvas = new Canvas(bitmap);
		/* 57 */
		dable.setBounds(0, 0, width, height);
		/* 58 */
		dable.draw(canvas);
		/* 59 */
		return bitmap;
		/*    */
	}

	/*    */
	/*    */
	public static Bitmap rescale(Bitmap bitmap, int width, int height)
	/*    */{
		/* 74 */
		Matrix matrix = new Matrix();
		/* 75 */
		matrix.postScale(width / bitmap.getWidth(), height / bitmap.getHeight());
		/* 76 */
		return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
				bitmap.getHeight(), matrix, true);
		/*    */
	}

	/*    */
	/*    */
	private static void cleanUp() {
		/* 80 */
		callCount += 1;
		/* 81 */
		if (callCount >= 10)
		/*    */{
			/* 84 */
			System.gc();
			/* 85 */
			System.runFinalization();
			/* 86 */
			System.gc();
			/* 87 */
			callCount = 0;
			/*    */
		}
		/*    */
	}
	/*    */
}
