package toto.graphics._3D;

//import toto.io.NativeBufferUtils;

class BufferUtilFactory {
	private static boolean nativeSupport = false;
	private static BufferUtil util = null;

	static {
//		if (NativeBufferUtils.isAPIAvailable()) {
//			util = new BufferUtilNative();
//			nativeSupport = true;
//		} else {
			util = new BufferUtilVM();
//		}
	}

	static BufferUtil getBufferUtil()
	/*    */{
		/* 41 */
		return util;
		/*    */
	}

	static boolean hasNativeSupport() {
		return nativeSupport;
	}
}
