package toto.graphics._3D;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.EGLConfigChooser;

public class AAConfigChooser implements EGLConfigChooser {
	private GLSurfaceView view = null;
	private boolean withAlpha = false;
	private int depth = 16;

	public AAConfigChooser(GLSurfaceView view) {
		this.view = view;
	}

	public AAConfigChooser(GLSurfaceView view, boolean withAlpha) {
		this.view = view;
		this.withAlpha = withAlpha;
	}

	private AAConfigChooser(GLSurfaceView view, boolean withAlpha, int depth) {
		this.view = view;
		this.withAlpha = withAlpha;
		this.depth = depth;
	}

	public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
		int greenSize = this.withAlpha ? 5 : 6;
		int alphaSize = this.withAlpha ? 1 : 0;
		int redSize = 5;
		int blueSize = 5;

		if (this.depth > 24) {
			redSize = 8;
			blueSize = 8;
			greenSize = 8;
			alphaSize = this.withAlpha ? 8 : 0;
		}
		try {
			this.view.setEGLContextClientVersion(2);
		} catch (IllegalStateException localIllegalStateException) {
		} catch (Throwable t) {
			Logger.log("Couldn't initialize OpenGL ES 2.0", 0);
			return null;
		}

		int[] val = new int[1];

		int[] configSpec = { 12324, redSize, 12323, greenSize, 12322, blueSize,
				12321, alphaSize, 12325, 24, 12352, 4, 12338, 1, 12337, 2,
				12344 };

		if (!egl.eglChooseConfig(display, configSpec, null, 0, val)) {
			configSpec = new int[] { 12324, redSize, 12323, greenSize, 12322,
					blueSize, 12321, alphaSize, 12325, 16, 12352, 4, 12338, 1,
					12337, 2, 12344 };
			if (!egl.eglChooseConfig(display, configSpec, null, 0, val)) {
				error();
			}
		}
		int numConfigs = val[0];

		if (numConfigs <= 0) {
			int EGL_COVERAGE_BUFFERS_NV = 12512;
			int EGL_COVERAGE_SAMPLES_NV = 12513;
			int EGL_DEPTH_ENCODING_NV = 12514;
			int EGL_DEPTH_ENCODING_NONLINEAR_NV = 12515;

			configSpec = new int[] { 12324, redSize, 12323, greenSize, 12322,
					blueSize, 12321, alphaSize, 12325, this.depth, 12352, 4,
					12514, 12515, 12512, 1, 12513, 2, 12344 };

			if (!egl.eglChooseConfig(display, configSpec, null, 0, val)) {
				error();
			}
			numConfigs = val[0];

			if (numConfigs <= 0) {
				Logger.log("No AA config found...defaulting to non-AA modes!");
				configSpec = new int[] { 12324, redSize, 12323, greenSize,
						12322, blueSize, 12321, alphaSize, 12325, this.depth,
						12352, 4, 12344 };

				if (!egl.eglChooseConfig(display, configSpec, null, 0, val)) {
					error();
				}
				numConfigs = val[0];

				if (numConfigs <= 0) {
					error();
				}
				Config.aaMode = 0;
				Logger.log("No AA enabled!");
			} else {
				Config.aaMode = 2;
				Logger.log("CSAA enabled!");
			}
		} else {
			Config.aaMode = 1;
			Logger.log("MSAA enabled with 2 samples!");
		}

		EGLConfig[] configs = new EGLConfig[numConfigs];
		if (!egl.eglChooseConfig(display, configSpec, configs, numConfigs, val)) {
			error();
		}

		int index = -1;
		for (int i = 0; i < configs.length; i++) {
			if (findConfigAttrib(egl, display, configs[i], 12324, 0) == 5) {
				index = i;
				break;
			}
		}
		if (index == -1) {
			Logger.log("Unable to find a matching config...using default!");
			index = 0;
		}
		EGLConfig config = configs.length > 0 ? configs[index] : null;
		if (config == null) {
			error();
		}
		return config;
	}

	private void error() {
		Config.aaMode = 0;
		Logger.log("Failed to choose config!", 0);
	}

	private int findConfigAttrib(EGL10 egl, EGLDisplay display,
			EGLConfig config, int attribute, int defaultValue) {
		int[] val = new int[1];
		if (egl.eglGetConfigAttrib(display, config, attribute, val)) {
			return val[0];
		}
		return defaultValue;
	}
}
