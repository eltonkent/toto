package toto.graphics._3D;

import java.io.Serializable;


public class SkyBox implements Serializable {
	private static final long serialVersionUID = 1L;
	private T3DScene rage3DScene = null;

	private T3DObject box = null;

	private boolean disposed = false;

	private float size = 0.0F;

	public SkyBox(float size) {
		this("left", "front", "right", "back", "up", "down", size);
	}

	public SkyBox(String left, String front, String right, String back,
			String up, String down, float size) {
		this.size = size;

		this.rage3DScene = new T3DScene();
		this.box = T3DObject.createDummyObj();

		TextureCache texMan = TextureCache.getInstance();

		Texture lt = texMan.getTexture(left);
		Texture ft = texMan.getTexture(front);
		Texture bt = texMan.getTexture(back);
		Texture rt = texMan.getTexture(right);
		Texture ut = texMan.getTexture(up);
		Texture dt = texMan.getTexture(down);

		if ((lt == null) || (rt == null) || (ft == null) || (bt == null)
				|| (ut == null) || (dt == null)) {
			Logger.log("Skybox textures not found!", 1);
			return;
		}

		lt.setClamping(true);
		ft.setClamping(true);
		bt.setClamping(true);
		rt.setClamping(true);
		ut.setClamping(true);
		dt.setClamping(true);

		T3DObject frontObj = T3DPrimitives.getPlane(1, size);
		T3DObject leftObj = T3DPrimitives.getPlane(1, size);
		T3DObject rightObj = T3DPrimitives.getPlane(1, size);
		T3DObject backObj = T3DPrimitives.getPlane(1, size);
		T3DObject downObj = T3DPrimitives.getPlane(1, size);
		T3DObject upObj = T3DPrimitives.getPlane(1, size);

		downObj.rotateX(1.570796F);
		upObj.rotateX(-1.570796F);
		upObj.rotateY(-1.570796F);
		leftObj.rotateY(-1.570796F);
		rightObj.rotateY(1.570796F);
		backObj.rotateX(-3.141593F);
		backObj.rotateZ(-3.141593F);
		downObj.rotateY(-1.570796F);

		downObj.rotateMesh();
		upObj.rotateMesh();
		leftObj.rotateMesh();
		rightObj.rotateMesh();
		backObj.rotateMesh();

		frontObj.clearRotation();
		upObj.clearRotation();
		downObj.clearRotation();
		leftObj.clearRotation();
		rightObj.clearRotation();
		backObj.clearRotation();

		float halfSize = size / 2.0F;

		frontObj.translate(0.0F, 0.0F, halfSize);
		backObj.translate(0.0F, 0.0F, -halfSize);
		leftObj.translate(-halfSize, 0.0F, 0.0F);
		rightObj.translate(halfSize, 0.0F, 0.0F);
		upObj.translate(0.0F, -halfSize, 0.0F);
		downObj.translate(0.0F, halfSize, 0.0F);

		frontObj.translateMesh();
		upObj.translateMesh();
		downObj.translateMesh();
		leftObj.translateMesh();
		rightObj.translateMesh();
		backObj.translateMesh();

		frontObj.setTexture(front);
		upObj.setTexture(up);
		leftObj.setTexture(left);
		rightObj.setTexture(right);
		backObj.setTexture(back);
		downObj.setTexture(down);

		this.box = T3DObject.mergeAll(new T3DObject[] { frontObj, upObj,
				leftObj, rightObj, backObj, downObj });
		this.box.build();
		this.rage3DScene.setAmbientLight(255, 255, 255);
		this.box.setLighting(Lighting.NO_LIGHTS);

		this.rage3DScene.addObject(this.box);
		this.rage3DScene.setClippingPlanes(1.0F, size);
	}

	public synchronized void dispose() {
		if (!this.disposed) {
			this.rage3DScene.removeAllObjects();
			this.box = null;
			this.rage3DScene = null;
			this.disposed = true;
		}
	}

	protected void finalize() {
		dispose();
	}

	public T3DScene getRage3DScene()
	/*     */{
		/* 177 */
		return this.rage3DScene;
		/*     */
	}

	public void compile() {
		this.box.compile();
	}

	public void setCenter(SimpleVector trans) {
		this.rage3DScene.getCamera().setPosition(trans.x * -1.0F,
				trans.y * -1.0F, trans.z * -1.0F);
		float add = Math.max(Math.max(Math.abs(trans.x), Math.abs(trans.y)),
				Math.abs(trans.z));
		this.rage3DScene.setClippingPlanes(100.0F, this.size + add);
	}

	public void render(T3DScene rage3DScene, FrameBuffer buffer) {
		if (this.disposed) {
			return;
		}
		this.rage3DScene.getCamera().getBack()
				.setTo(rage3DScene.getCamera().getBack());
		this.rage3DScene.renderScene(buffer);
		this.rage3DScene.draw(buffer);
	}
}
