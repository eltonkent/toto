package toto.graphics._3D;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/*    */
/*    */

class VisListManager implements Serializable {
	private static final long serialVersionUID = 2L;
	private HashMap<Long, ArrayList<VisList>> buf2Vis = new HashMap();
	boolean isDisposed = false;

	void dispose() {
		this.isDisposed = true;
	}

	protected void finalize() {
		dispose();
	}

	VisList getVisList(FrameBuffer buffer, VisList current) {
		ArrayList lists = (ArrayList) this.buf2Vis.get(buffer.getID());
		if (lists == null) {
			buffer.register(this);
			lists = new ArrayList(3);
			lists.add(current);

			this.buf2Vis.put(buffer.getID(), lists);
			current.lastCycle = buffer.displayCycle;
			return current;
		}
		VisList use = null;
		for (int i = 0; i < lists.size(); i++) {
			VisList vl = (VisList) lists.get(i);
			if (vl.lastCycle != buffer.displayCycle) {
				use = vl;
				break;
			}
		}
		if (use == null) {
			use = new VisList(current.getMaxSize());
			lists.add(use);
			Logger.log("Additional visibility list (" + lists.size()
					+ ") created with size: " + current.getMaxSize(), 2);
		}
		use.lastCycle = buffer.displayCycle;
		return use;
	}

	void remove(FrameBuffer buffer) {
		if (this.buf2Vis.remove(buffer.getID()) != null)
			Logger.log("Visibility lists disposed!", 2);
	}
}
