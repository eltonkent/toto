package toto.graphics._3D;

import java.io.Serializable;

final class OcTreeNode implements Serializable {
	private static final long serialVersionUID = 1L;
	private static int nodeID = 0;
	OcTreeNode[] children;
	OcTreeNode parent = null;
	float xLow;
	float yLow;
	float zLow;
	float xHigh;
	float yHigh;
	float zHigh;
	int childCnt;
	int id = -1;

	int polyCnt = 0;

	int pointCnt = 0;

	int[] polyList = null;

	int[] pointList = null;
	SimpleVector[] pList;

	OcTreeNode() {
		this.childCnt = 0;
		this.children = null;
		this.pList = new SimpleVector[8];
		for (int i = 0; i < 8; i++) {
			this.pList[i] = SimpleVector.create(0.0F, 0.0F, 0.0F);
		}
		this.parent = null;
		this.id = nodeID;
		nodeID += 1;
		this.polyCnt = 0;
		this.pointCnt = 0;
	}

	boolean isLeaf() {
		return getChildCount() == 0;
	}

	int isVisible(Matrix transform, float divX, float divY) {
		float[][] mat = transform.mat;

		float s00 = mat[0][0];
		float s10 = mat[1][0];
		float s11 = mat[1][1];
		float s21 = mat[2][1];
		float s22 = mat[2][2];
		float s12 = mat[1][2];
		float s20 = mat[2][0];
		float s02 = mat[0][2];
		float s01 = mat[0][1];

		float bx = mat[3][0];
		float by = mat[3][1];
		float bz = mat[3][2];

		int obbZClipped = 0;
		int obbZFarClipped = 0;
		int obbXnClipped = 0;
		int obbXpClipped = 0;
		int obbYnClipped = 0;
		int obbYpClipped = 0;

		float divXD = divX;
		float divYD = divY;

		boolean oneMissed = false;
		boolean oneFound = false;

		for (int p = 0; p < 8; p++) {
			SimpleVector ps = this.pList[p];

			float x1 = ps.x;
			float y1 = ps.y;
			float z1 = ps.z;

			float p1x = x1 * s00 + y1 * s10 + z1 * s20 + bx;
			float p1z = x1 * s02 + y1 * s12 + z1 * s22 + bz;

			boolean zMissed = false;

			if (p1z < Config.nearPlane) {
				obbZClipped++;
				oneFound = true;
			} else if (p1z > Config.farPlane) {
				obbZFarClipped++;
				oneFound = true;
			} else {
				zMissed = true;
			}

			float p1zh = p1z * divXD;
			if (p1x < -p1zh) {
				obbXnClipped++;
				oneFound = true;
			} else if (p1x > p1zh) {
				obbXpClipped++;
				oneFound = true;
			} else {
				p1zh = p1z * divYD;
				float p1y = x1 * s01 + y1 * s11 + z1 * s21 + by;
				if (p1y < -p1zh) {
					obbYnClipped++;
					oneFound = true;
				} else if (p1y > p1zh) {
					obbYpClipped++;
					oneFound = true;
				} else if (zMissed) {
					oneMissed = true;
				}

			}

			if ((oneFound) && (oneMissed)) {
				return 1;
			}

		}

		boolean invisible = (obbYpClipped == 8) || (obbXpClipped == 8)
				|| (obbYnClipped == 8) || (obbXnClipped == 8)
				|| (obbZClipped == 8) || (obbZFarClipped == 8);

		if (!invisible) {
			if ((obbYpClipped == 0) && (obbXpClipped == 0)
					&& (obbYnClipped == 0) && (obbXnClipped == 0)
					&& (obbZClipped == 0) && (obbZFarClipped == 0)) {
				return 999;
			}
			return 1;
		}
		return 0;
	}

	void addTriangle(int max, int number, int p0, int p1, int p2) {
		if (this.polyList == null) {
			this.polyList = new int[max];
			this.pointList = new int[max * 3];
		}
		this.polyList[this.polyCnt] = number;

		if (notInList(this.pointList, this.pointCnt, p0)) {
			this.pointList[this.pointCnt] = p0;
			this.pointCnt += 1;
		}
		if (notInList(this.pointList, this.pointCnt, p1)) {
			this.pointList[this.pointCnt] = p1;
			this.pointCnt += 1;
		}
		if (notInList(this.pointList, this.pointCnt, p2)) {
			this.pointList[this.pointCnt] = p2;
			this.pointCnt += 1;
		}
		this.polyCnt += 1;
	}

	void packPoints() {
		if (this.pointCnt != this.polyCnt * 3) {
			int[] tmpPoints = new int[this.pointCnt];
			System.arraycopy(this.pointList, 0, tmpPoints, 0, this.pointCnt);
			this.pointList = tmpPoints;
		}
	}

	private boolean notInList(int[] list, int size, int element) {
		for (int i = 0; i < size; i++) {
			if (element == list[i]) {
				return false;
			}
		}
		return true;
	}

	static void resetNodeID() {
		nodeID = 0;
	}

	int getID() {
		/* 198 */
		return this.id;
		/*     */
	}

	void addChild(OcTreeNode node) {
		if (this.children == null) {
			this.children = new OcTreeNode[8];
		}
		if (this.childCnt < 8) {
			this.children[this.childCnt] = node;
			this.childCnt += 1;
			node.setParent(this);
		}
	}

	void removeChild(OcTreeNode node) {
		int nodePos = -1;
		for (int i = 0; i < this.childCnt; i++) {
			if (((Object) this.children[i]).equals(node)) {
				nodePos = i;
				break;
			}
		}
		if (nodePos != -1) {
			for (int i = nodePos; i < this.childCnt - 1; i++) {
				this.children[i] = this.children[(i + 1)];
			}
			this.childCnt -= 1;
		}
	}

	void setParent(OcTreeNode parent) {
		/* 229 */
		this.parent = parent;
		/*     */
	}

	OcTreeNode getParent() {
		/* 233 */
		return this.parent;
		/*     */
	}

	OcTreeNode[] getChildren() {
		/* 237 */
		return this.children;
		/*     */
	}

	int getChildCount() {
		/* 241 */
		return this.childCnt;
		/*     */
	}

	int getPolyCount() {
		/* 245 */
		return this.polyCnt;
		/*     */
	}

	int getPointCount() {
		/* 249 */
		return this.pointCnt;
		/*     */
	}

	int[] getPolygons() {
		/* 253 */
		return this.polyList;
		/*     */
	}

	int[] getPoints() {
		/* 257 */
		return this.pointList;
		/*     */
	}

	boolean completeFit(float x1, float y1, float z1, float x2, float y2,
			float z2, float x3, float y3, float z3) {
		if ((x1 >= this.xLow) && (x1 <= this.xHigh) && (x2 >= this.xLow)
				&& (x2 <= this.xHigh) && (x3 >= this.xLow)
				&& (x3 <= this.xHigh) && (y1 >= this.yLow)
				&& (y1 <= this.yHigh) && (y2 >= this.yLow)
				&& (y2 <= this.yHigh) && (y3 >= this.yLow)
				&& (y3 <= this.yHigh) && (z1 >= this.zLow)
				&& (z1 <= this.zHigh) && (z2 >= this.zLow)
				&& (z2 <= this.zHigh) && (z3 >= this.zLow)
				&& (z3 <= this.zHigh)) {
			return true;
		}

		return false;
	}

	boolean partialFit(float x1, float y1, float z1, float x2, float y2,
			float z2, float x3, float y3, float z3) {
		if ((x1 >= this.xLow) && (x1 <= this.xHigh) && (y1 >= this.yLow)
				&& (y1 <= this.yHigh) && (z1 >= this.zLow)
				&& (z1 <= this.zHigh)) {
			return true;
		}
		if ((x2 >= this.xLow) && (x2 <= this.xHigh) && (y2 >= this.yLow)
				&& (y2 <= this.yHigh) && (z2 >= this.zLow)
				&& (z2 <= this.zHigh)) {
			return true;
		}
		if ((x3 >= this.xLow) && (x3 <= this.xHigh) && (y3 >= this.yLow)
				&& (y3 <= this.yHigh) && (z3 >= this.zLow)
				&& (z3 <= this.zHigh)) {
			return true;
		}
		return false;
	}

	boolean sphereIntersectsNode(float x, float y, float z, float rad) {
		return (x + rad >= this.xLow) && (x - rad <= this.xHigh)
				&& (y + rad >= this.yLow) && (y - rad <= this.yHigh)
				&& (z + rad >= this.zLow) && (z - rad <= this.zHigh);
	}

	void extendDimensions(float x1, float y1, float z1, float x2, float y2,
			float z2, float x3, float y3, float z3) {
		if (x1 < this.xLow) {
			this.xLow = x1;
		}
		if (x2 < this.xLow) {
			this.xLow = x2;
		}
		if (x3 < this.xLow) {
			this.xLow = x3;
		}

		if (y1 < this.yLow) {
			this.yLow = y1;
		}
		if (y2 < this.yLow) {
			this.yLow = y2;
		}
		if (y3 < this.yLow) {
			this.yLow = y3;
		}

		if (z1 < this.zLow) {
			this.zLow = z1;
		}
		if (z2 < this.zLow) {
			this.zLow = z2;
		}
		if (z3 < this.zLow) {
			this.zLow = z3;
		}

		if (x1 > this.xHigh) {
			this.xHigh = x1;
		}
		if (x2 > this.xHigh) {
			this.xHigh = x2;
		}
		if (x3 > this.xHigh) {
			this.xHigh = x3;
		}

		if (y1 > this.yHigh) {
			this.yHigh = y1;
		}
		if (y2 > this.yHigh) {
			this.yHigh = y2;
		}
		if (y3 > this.yHigh) {
			this.yHigh = y3;
		}

		if (z1 > this.zHigh) {
			this.zHigh = z1;
		}
		if (z2 > this.zHigh) {
			this.zHigh = z2;
		}
		if (z3 > this.zHigh) {
			this.zHigh = z3;
		}

		setDimensions(this.xLow, this.yLow, this.zLow, this.xHigh, this.yHigh,
				this.zHigh);

		if (this.parent != null)
			this.parent.extendDimensions(x1, y1, z1, x2, y2, z2, x3, y3, z3);
	}

	void setDimensions(float xLow, float yLow, float zLow, float xHigh,
			float yHigh, float zHigh) {
		this.xLow = xLow;
		this.yLow = yLow;
		this.zLow = zLow;
		this.xHigh = xHigh;
		this.yHigh = yHigh;
		this.zHigh = zHigh;

		this.pList[4].x = xLow;
		this.pList[4].y = yLow;
		this.pList[4].z = zHigh;

		this.pList[5].x = xLow;
		this.pList[5].y = yLow;
		this.pList[5].z = zLow;

		this.pList[6].x = xHigh;
		this.pList[6].y = yLow;
		this.pList[6].z = zLow;

		this.pList[7].x = xHigh;
		this.pList[7].y = yLow;
		this.pList[7].z = zHigh;

		this.pList[0].x = xLow;
		this.pList[0].y = yHigh;
		this.pList[0].z = zHigh;

		this.pList[1].x = xLow;
		this.pList[1].y = yHigh;
		this.pList[1].z = zLow;

		this.pList[2].x = xHigh;
		this.pList[2].y = yHigh;
		this.pList[2].z = zLow;

		this.pList[3].x = xHigh;
		this.pList[3].y = yHigh;
		this.pList[3].z = zHigh;
	}
}
