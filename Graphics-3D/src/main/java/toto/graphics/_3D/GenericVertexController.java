package toto.graphics._3D;

import java.io.Serializable;

public abstract class GenericVertexController implements IVertexController,
		Serializable {
	private static final long serialVersionUID = 1L;
	private SimpleVector[] meshData;
	private SimpleVector[] normalData;
	private SimpleVector[] meshTarget;
	private SimpleVector[] normalTarget;
	private Mesh mesh;
	private int size = 0;

	private boolean initialized = false;
	private float x;
	private float y;
	private float z;
	private float[] meshxOrg = null;
	private float[] meshyOrg = null;
	private float[] meshzOrg = null;

	private float[] meshnxOrg = null;
	private float[] meshnyOrg = null;
	private float[] meshnzOrg = null;

	public final boolean init(Mesh mesh, boolean modify) {
		boolean ok = true;

		if (!this.initialized) {
			this.mesh = mesh;

			this.meshxOrg = mesh.xOrg;
			this.meshyOrg = mesh.yOrg;
			this.meshzOrg = mesh.zOrg;

			this.meshnxOrg = mesh.nxOrg;
			this.meshnyOrg = mesh.nyOrg;
			this.meshnzOrg = mesh.nzOrg;

			if (!mesh.normalsCalculated) {
				Logger.log(
						"No normals have been calculated for this mesh yet!", 1);
			}

			int size = mesh.obbStart;
			if ((size == 0) || (mesh.obbEnd + 1 != mesh.anzCoords)) {
				size = mesh.anzCoords;
			}

			this.size = size;

			this.meshData = new SimpleVector[size];
			this.normalData = new SimpleVector[size];

			if (modify) {
				this.meshTarget = this.meshData;
				this.normalTarget = this.normalData;
			} else {
				this.meshTarget = new SimpleVector[size];
				this.normalTarget = new SimpleVector[size];
			}

			for (int i = 0; i < size; i++) {
				float x = this.meshxOrg[i];
				float y = this.meshyOrg[i];
				float z = this.meshzOrg[i];
				float nx = this.meshnxOrg[i];
				float ny = this.meshnyOrg[i];
				float nz = this.meshnzOrg[i];
				if (!modify) {
					this.meshTarget[i] = new SimpleVector(x, y, z);
					this.normalTarget[i] = new SimpleVector(nx, ny, nz);
				}
				this.meshData[i] = new SimpleVector(x, y, z);
				this.normalData[i] = new SimpleVector(nx, ny, nz);
			}
			ok &= setup();
			this.initialized = ok;
		} else {
			ok = false;
			Logger.log(
					"This instance has already been assigned to another Mesh!",
					0);
		}
		return ok;
	}

	public boolean setup() {
		return true;
	}

	public final SimpleVector[] getSourceMesh() {
		/* 118 */
		return this.meshData;
		/*     */
	}

	public final SimpleVector[] getSourceNormals() {
		/* 122 */
		return this.normalData;
		/*     */
	}

	public final SimpleVector[] getDestinationMesh() {
		/* 126 */
		return this.meshTarget;
		/*     */
	}

	public final SimpleVector[] getDestinationNormals() {
		/* 130 */
		return this.normalTarget;
		/*     */
	}

	public final int getMeshSize() {
		/* 134 */
		return this.size;
		/*     */
	}

	public void refreshMeshData() {
		float[] meshxOrg = this.mesh.xOrg;
		float[] meshyOrg = this.mesh.yOrg;
		float[] meshzOrg = this.mesh.zOrg;

		float[] meshnxOrg = this.mesh.nxOrg;
		float[] meshnyOrg = this.mesh.nyOrg;
		float[] meshnzOrg = this.mesh.nzOrg;

		for (int i = 0; i < this.size; i++) {
			SimpleVector meshTargeti = this.meshTarget[i];
			SimpleVector normalDatai = this.normalData[i];

			meshTargeti.x = meshxOrg[i];
			meshTargeti.y = meshyOrg[i];
			meshTargeti.z = meshzOrg[i];

			normalDatai.x = meshnxOrg[i];
			normalDatai.y = meshnyOrg[i];
			normalDatai.z = meshnzOrg[i];
		}
	}

	public final void updateMesh() {
		if (this.size == 0) {
			return;
		}

		this.x = this.meshTarget[0].x;
		this.y = this.meshTarget[0].y;
		this.z = this.meshTarget[0].z;

		float minx = this.x;
		float maxx = this.x;
		float miny = this.y;
		float maxy = this.y;
		float minz = this.z;
		float maxz = this.z;

		SimpleVector[] meshTarget = this.meshTarget;
		SimpleVector[] normalTarget = this.normalTarget;

		float[] meshxOrg = this.meshxOrg;
		float[] meshyOrg = this.meshyOrg;
		float[] meshzOrg = this.meshzOrg;

		float[] meshnxOrg = this.meshnxOrg;
		float[] meshnyOrg = this.meshnyOrg;
		float[] meshnzOrg = this.meshnzOrg;

		Mesh mesh = this.mesh;

		for (int i = 0; i < this.size; i++) {
			SimpleVector meshTargeti = meshTarget[i];
			SimpleVector normalTargeti = normalTarget[i];

			this.x = meshTargeti.x;
			this.y = meshTargeti.y;
			this.z = meshTargeti.z;

			meshxOrg[i] = this.x;
			meshyOrg[i] = this.y;
			meshzOrg[i] = this.z;
			meshnxOrg[i] = normalTargeti.x;
			meshnyOrg[i] = normalTargeti.y;
			meshnzOrg[i] = normalTargeti.z;

			if (this.x < minx)
				minx = this.x;
			else if (this.x > maxx) {
				maxx = this.x;
			}
			if (this.y < miny)
				miny = this.y;
			else if (this.y > maxy) {
				maxy = this.y;
			}
			if (this.z < minz)
				minz = this.z;
			else if (this.z > maxz) {
				maxz = this.z;
			}
		}

		if (mesh.obbStart == 0) {
			mesh.obbStart = mesh.anzCoords;
			mesh.obbEnd = (this.size + 7);
			mesh.anzCoords += 8;
		}

		int pos = mesh.obbStart;

		meshxOrg[pos] = minx;
		meshyOrg[pos] = miny;
		meshzOrg[(pos++)] = minz;

		meshxOrg[pos] = minx;
		meshyOrg[pos] = miny;
		meshzOrg[(pos++)] = maxz;

		meshxOrg[pos] = maxx;
		meshyOrg[pos] = miny;
		meshzOrg[(pos++)] = minz;

		meshxOrg[pos] = maxx;
		meshyOrg[pos] = miny;
		meshzOrg[(pos++)] = maxz;

		meshxOrg[pos] = maxx;
		meshyOrg[pos] = maxy;
		meshzOrg[(pos++)] = minz;

		meshxOrg[pos] = maxx;
		meshyOrg[pos] = maxy;
		meshzOrg[(pos++)] = maxz;

		meshxOrg[pos] = minx;
		meshyOrg[pos] = maxy;
		meshzOrg[(pos++)] = minz;

		meshxOrg[pos] = minx;
		meshyOrg[pos] = maxy;
		meshzOrg[(pos++)] = maxz;
	}

	public final void destroy() {
		cleanup();
		this.initialized = false;
	}

	public void cleanup() {
	}

	public int[] getPolygonIDs(int vertex, int max) {
		int[] ids = new int[max];
		int cnt = 0;
		for (int i = 0; (i < this.mesh.anzTri) && (cnt < max); i++) {
			if ((this.mesh.coords[this.mesh.points[i][0]] == vertex)
					|| (this.mesh.coords[this.mesh.points[i][1]] == vertex)
					|| (this.mesh.coords[this.mesh.points[i][2]] == vertex)) {
				ids[cnt] = i;
				cnt++;
			}
		}

		int[] ids2 = new int[cnt];
		System.arraycopy(ids, 0, ids2, 0, cnt);
		return ids2;
	}
}
