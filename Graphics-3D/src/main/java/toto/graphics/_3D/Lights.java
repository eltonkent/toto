package toto.graphics._3D;

import java.io.Serializable;

class Lights implements Serializable {
	private static final long serialVersionUID = -9015402026145033787L;
	int rgbScale;
	int lightCnt;
	int maxLights;
	float[] xOrg;
	float[] yOrg;
	float[] zOrg;
	float[] xTr;
	float[] yTr;
	float[] zTr;
	float[] bOrg;
	float[] rOrg;
	float[] gOrg;
	Matrix[] transform;
	boolean[] isVisible;
	boolean[] isRemoved;
	float[] attenuation;
	float[] discardDistance;
	float[] distanceOverride;
	private Matrix mat5 = new Matrix();

	Lights(int max) {
		this.rgbScale = 1;

		this.maxLights = max;
		this.lightCnt = 0;

		this.xOrg = new float[this.maxLights];
		this.yOrg = new float[this.maxLights];
		this.zOrg = new float[this.maxLights];

		this.xTr = new float[this.maxLights];
		this.yTr = new float[this.maxLights];
		this.zTr = new float[this.maxLights];

		this.bOrg = new float[this.maxLights];
		this.gOrg = new float[this.maxLights];
		this.rOrg = new float[this.maxLights];

		this.transform = new Matrix[this.maxLights];

		this.isVisible = new boolean[this.maxLights];

		this.attenuation = new float[this.maxLights];
		this.discardDistance = new float[this.maxLights];

		this.distanceOverride = new float[this.maxLights];

		this.isRemoved = new boolean[this.maxLights];

		for (int i = 0; i < this.maxLights; i++) {
			this.attenuation[i] = -2.0F;
			this.discardDistance[i] = -2.0F;
			this.distanceOverride[i] = -1.0F;
			this.isRemoved[i] = false;
		}
	}

	void setRGBScale(int scale) {
		if ((scale > 0) && (scale < 5) && (scale != 3))
			this.rgbScale = scale;
		else
			Logger.log("This RGB scaling (" + scale + ") is not supported!", 1);
	}

	float getAttenuation(int number) {
		if (!isFine(number)) {
			return 0.0F;
		}
		return this.attenuation[number];
	}

	float getDiscardDistance(int number) {
		if (!isFine(number)) {
			return 0.0F;
		}
		return this.discardDistance[number];
	}

	SimpleVector getPosition(int number, SimpleVector s) {
		SimpleVector res = s;
		if (isFine(number)) {
			res.z = this.zOrg[number];
			res.x = this.xOrg[number];
			res.y = this.yOrg[number];
		}
		return res;
	}

	SimpleVector getIntensity(int number) {
		if (!isFine(number)) {
			return SimpleVector.create();
		}
		return SimpleVector.create(this.rOrg[number], this.gOrg[number],
				this.bOrg[number]);
	}

	void setAttenuation(int number, float ld) {
		if (isFine(number)) {
			if (ld < -1.0F) {
				ld = -1.0F;
			}
			if (ld == 0.0F) {
				ld = 1.0F;
			}
			this.attenuation[number] = ld;
		}
	}

	void setDiscardDistance(int number, float dd) {
		if (isFine(number)) {
			if (dd < -1.0F) {
				dd = -2.0F;
			}
			this.discardDistance[number] = dd;
		}
	}

	void setDistanceOverride(int number, float dd) {
		if (isFine(number))
			this.distanceOverride[number] = dd;
	}

	float getDistanceOverride(int number) {
		return this.distanceOverride[number];
	}

	int addLight(float x, float y, float z, float r, float g, float b) {
		int lc = this.lightCnt;

		if (lc >= this.maxLights) {
			for (int i = 0; i < this.maxLights; i++) {
				if (this.isRemoved[i]) {
					lc = i;
					this.isRemoved[lc] = false;
					this.attenuation[lc] = -2.0F;
					this.discardDistance[lc] = -2.0F;
					this.distanceOverride[lc] = -1.0F;
					break;
				}
			}
		}

		if (lc < this.maxLights) {
			this.xOrg[lc] = x;
			this.yOrg[lc] = y;
			this.zOrg[lc] = z;

			this.rOrg[lc] = r;
			this.gOrg[lc] = g;
			this.bOrg[lc] = b;

			this.isVisible[lc] = true;

			if (this.transform[lc] == null)
				this.transform[lc] = new Matrix();
			else {
				this.transform[lc].setIdentity();
			}

			Logger.log("Adding Lightsource: " + lc, 2);

			if (lc == this.lightCnt)
				this.lightCnt += 1;
		} else {
			Logger.log(
					"Maximum number of Lightsources reached...ignoring additional ones!",
					1);
		}

		return lc;
	}

	void transformLights(Camera cam) {
		Matrix mat2 = cam.backMatrix;

		for (int i = 0; i < this.lightCnt; i++)
			if ((this.isVisible[i]) && (!this.isRemoved[i])) {
				this.mat5.setIdentity();
				this.mat5.translate(-cam.backBx, -cam.backBy, -cam.backBz);
				this.mat5.matMul(mat2);

				float s00 = this.mat5.mat[0][0];
				float s10 = this.mat5.mat[1][0];
				float s11 = this.mat5.mat[1][1];
				float s21 = this.mat5.mat[2][1];
				float s22 = this.mat5.mat[2][2];
				float s12 = this.mat5.mat[1][2];
				float s20 = this.mat5.mat[2][0];
				float s02 = this.mat5.mat[0][2];
				float s01 = this.mat5.mat[0][1];

				float bx = this.mat5.mat[3][0];
				float by = this.mat5.mat[3][1];
				float bz = this.mat5.mat[3][2];

				float x1 = this.xOrg[i];
				float y1 = this.yOrg[i];
				float z1 = this.zOrg[i];

				this.xTr[i] = (x1 * s00 + y1 * s10 + z1 * s20 + bx);
				this.yTr[i] = (x1 * s01 + y1 * s11 + z1 * s21 + by);
				this.zTr[i] = (x1 * s02 + y1 * s12 + z1 * s22 + bz);
			}
	}

	void setVisibility(int ln, boolean mode) {
		if (isFine(ln))
			this.isVisible[ln] = mode;
	}

	void remove(int ln) {
		if (isFine(ln)) {
			this.isRemoved[ln] = true;
			this.isVisible[ln] = false;
		}
	}

	void setLightIntensity(int ln, float r, float g, float b) {
		if (isFine(ln)) {
			this.rOrg[ln] = r;
			this.gOrg[ln] = g;
			this.bOrg[ln] = b;
		}
	}

	void setPosition(int ln, float x, float y, float z) {
		if (isFine(ln)) {
			this.zOrg[ln] = z;
			this.xOrg[ln] = x;
			this.yOrg[ln] = y;
		}
	}

	private boolean isFine(int ln) {
		if (ln < 0) {
			return false;
		}
		if ((ln < this.lightCnt) && (this.isRemoved[ln])) {
			return false;
		}
		if (ln < this.lightCnt) {
			return true;
		}
		Logger.log("Tried to modify an undefined light source!", 1);
		return false;
	}
}
