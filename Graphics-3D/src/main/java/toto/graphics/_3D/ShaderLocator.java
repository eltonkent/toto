package toto.graphics._3D;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.content.res.AssetManager;
import android.content.res.Resources;

public class ShaderLocator {
	private AssetManager assets = null;
	private Integer rawResourceId = null;
	private Resources res = null;

	public ShaderLocator() {
		Logger.log("Accessing shaders via JAR!");
	}

	public ShaderLocator(AssetManager assets) {
		this.assets = assets;
		Logger.log("Accessing shaders via assets directory!");
	}

	public ShaderLocator(Resources res, int rawResourceId) {
		this.rawResourceId = Integer.valueOf(rawResourceId);
		this.res = res;
		Logger.log("Accessing shaders via res/raw directory!");
	}

	public String getShaderCode(String name) throws IOException {
		if ((this.assets == null) && (this.rawResourceId == null)) {
			return loadFromJar(name);
		}
		if (this.assets != null) {
			return loadFromAssets(name);
		}
		if (this.rawResourceId != null) {
			return loadFromRaw(name);
		}
		return null;
	}

	private String loadFromRaw(String name) throws IOException {
		InputStream is = null;
		try {
			is = this.res.openRawResource(this.rawResourceId.intValue());
			return getFileFromZip(name, is);
		} finally {
			if (is != null)
				is.close();
		}
	}

	private String loadFromAssets(String name) throws IOException {
		InputStream is = null;
		try {
			is = this.assets.open("jpct_shaders.zip", 3);
			return getFileFromZip(name, is);
		} finally {
			if (is != null)
				is.close();
		}
	}

	private String loadFromJar(String name) throws IOException {
		InputStream is = null;
		try {
			is = getClass().getResourceAsStream(name);
			return ModelLoader.loadTextFile(is);
		} finally {
			if (is != null)
				is.close();
		}
	}

	private String getFileFromZip(String name, InputStream is)
			throws IOException {
		ZipInputStream zis = new ZipInputStream(is);
		ZipEntry ze = null;
		name = name.replace("/", "");
		do {
			ze = zis.getNextEntry();
			if ((ze != null) && (ze.getName().endsWith(name))) {
				Logger.log("Loading " + name + " from zip file!");
				return ModelLoader.loadTextFile(zis);
			}
		} while (ze != null);
		throw new FileNotFoundException();
	}
}
