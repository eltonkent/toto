package toto.graphics._3D;

import java.io.Serializable;

public abstract interface IPaintListener extends Serializable {
	public abstract void startPainting();

	public abstract void finishedPainting();
}
