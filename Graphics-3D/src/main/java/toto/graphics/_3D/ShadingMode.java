package toto.graphics._3D;

/**
 * Rage 3D object Shading Mode
 * 
 * @see toto.graphics._3D.T3DObject#setShadingMode(int)
 * 
 */
public enum ShadingMode {
	/**
	 * Indicates that gouraud shading should be used (default)
	 */
	SHADING_GOURAUD, //
	/**
	 * Indicates that (faked) flat shading should be used. Faked, because it
	 * sits on top of gouraud, i.e. it's not faster than using gouraud shading.
	 * In fact it may be slower because it hinders the usage of triangle strips.
	 */
	SHADING_FAKED_FLAT // 1;
}
