package toto.graphics._3D;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

import toto.graphics.color.RGBColor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;

public class Texture implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final int[] TEXTURE_SIZES = { 1, 2, 4, 8, 16, 32, 64, 128,
			256, 512, 1024, 2048 };
	public static final int DEFAULT_WIDTH = 16;
	public static final int DEFAULT_HEIGHT = 16;
	private static boolean defaultTo4bpp = false;

	private static boolean defaultToMipmapping = false;

	private static boolean defaultToKeepPixels = true;

	static int MARKER_NOTHING = 0;

	static int MARKER_DELETE_AND_UPLOAD = 1;
	int width;
	int height;
	int[] texels;
	byte[] zippedTexels = null;

	boolean alpha = false;

	boolean isUnicolor = false;

	boolean repeat = true;

	boolean bilinear = true;

	boolean mipmap = true;

	boolean enabled = true;

	boolean convertTo4444 = false;

	boolean etc1 = false;

	boolean isShadowMap = false;

	transient ITextureEffect myEffect = null;

	transient int fbo = -1;

	transient int renderBuffer = -1;

	transient int lastHandlerId = -1;

	boolean nPot = false;
	private boolean isLoaded;
	private int[] storeTexels = null;

	private boolean isConverted = false;

	private int openGLID = 0;

	private int markerGL = 0;

	private int lastRenderer = -1;

	private int lastRendererMarker = -1;

	private boolean keepPixels = true;

	private HashMap<Integer, Integer> glIDs = new HashMap();

	private HashSet<Integer> marker = new HashSet();

	private DepthBuffer depthBuffer = null;

	Texture() {
		this.width = 16;
		this.height = 16;

		this.isLoaded = true;
		int end = this.width * this.height;
		this.texels = new int[end];
		this.isConverted = false;
		resetIDs();
		for (int i = 0; i < end; i++) {
			this.texels[i] = -1;
		}
		this.convertTo4444 = defaultTo4bpp;
		setMipmap(defaultToMipmapping);
		this.keepPixels = defaultToKeepPixels;
	}

	public Texture(InputStream is) {
		loadTexture(is, null);
		this.isConverted = false;
	}

	public Texture(InputStream is, boolean useAlpha) {
		loadTexture(is, null, useAlpha);
		this.isConverted = false;
	}

	public Texture(Bitmap image) {
		loadTexture(null, image);
		this.isConverted = false;
	}

	public Texture(Bitmap image, boolean useAlpha) {
		loadTexture(null, image, useAlpha);
		this.isConverted = false;
	}

	public Texture(Drawable image) {
		this(BitmapHelper.convert(image));
	}

	public Texture(Drawable image, boolean useAlpha) {
		this(BitmapHelper.convert(image), useAlpha);
	}

	public Texture(int width, int height) {
		this(width, height, RGBColor.BLACK);
	}

	public Texture(int width, int height, int alpha) {
		this(width, height, RGBColor.BLACK);
		int end = this.texels.length;
		this.alpha = true;
		int al = (alpha & 0xFF) << 24;
		for (int i = 0; i < end; i++)
			this.texels[i] |= al;
	}

	public Texture(int width, int height, RGBColor col) {
		this(createIntArray(width, height, col, false), adjustSize(width),
				adjustSize(height), false);
		if ((col != null) && (col.getAlpha() != 0))
			this.alpha = true;
	}

	Texture(int width, int height, RGBColor col, boolean npot) {
		this(createIntArray(width, height, col, npot), width, height, false);
		if ((col != null) && (col.getAlpha() != 0))
			this.alpha = true;
	}

	private static int[] createIntArray(int width, int height, RGBColor col,
			boolean npot) {
		if (col == null) {
			return null;
		}

		int w = width;
		int h = height;

		if (!npot) {
			w = adjustSize(w);
			h = adjustSize(h);
		}

		int[] pixels = new int[w * h];
		int color = col.getAlpha() << 24 | col.getRed() << 16
				| col.getGreen() << 8 | col.getBlue();
		if (color != 0) {
			for (int i = 0; i < pixels.length; i++) {
				pixels[i] = color;
			}
		}

		if (Logger.isDebugEnabled()) {
			Logger.log("Created a " + w + "*" + h + " texture using "
					+ pixels.length * 4 + " bytes!", 3);
		}

		return pixels;
	}

	public void add(Texture ta, float weight) {
		if ((ta.texels == null) || (this.texels == null)) {
			Logger.log("Textures contains no texel data!", 0);
			return;
		}

		if (ta.getArraySize() != getArraySize()) {
			Logger.log("Texture sizes don't match", 0);
			return;
		}
		int[] tatex = ta.texels;
		for (int i = 0; i < tatex.length; i++) {
			int col2 = tatex[i];
			int col1 = this.texels[i];
			int a1 = col1 >> 24;
			int r1 = (col1 & 0xFF0000) >> 16;
			int g1 = (col1 & 0xFF00) >> 8;
			int b1 = col1 & 0xFF;
			int a2 = col2 >> 24;
			int r2 = (col2 & 0xFF0000) >> 16;
			int g2 = (col2 & 0xFF00) >> 8;
			int b2 = col2 & 0xFF;

			a1 = clip(a1 + (int) (a2 * weight));
			r1 = clip(r1 + (int) (r2 * weight));
			g1 = clip(g1 + (int) (g2 * weight));
			b1 = clip(b1 + (int) (b2 * weight));
			this.texels[i] = (a1 << 24 | r1 << 16 | g1 << 8 | b1);
		}
		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	Texture(int[] pixels, int pw, int ph, boolean blitMode) {
		if (blitMode) {
			if ((pw <= 2048) && (ph <= 2048)) {
				this.width = adjustSize(pw);
				this.height = adjustSize(ph);

				this.texels = new int[this.width * this.height];
				refill(pixels, pw, ph);
			} else {
				Logger.log("Unsupported bitmap size for blitting!", 0);
			}
		} else {
			this.width = pw;
			this.height = ph;
			this.texels = pixels;
		}

		this.isLoaded = true;
		this.convertTo4444 = defaultTo4bpp;
		setMipmap(defaultToMipmapping);
		if (this.texels == null) {
			this.mipmap = false;
		}
		resetIDs();
	}

	public void setEffect(ITextureEffect effect) {
		if (this.storeTexels == null) {
			if (this.texels == null) {
				Logger.log(
						"Can't setKey a texture effect for a compressed texture!",
						0);
				return;
			}
			this.storeTexels = new int[this.texels.length];
			System.arraycopy(this.texels, 0, this.storeTexels, 0,
					this.texels.length);
		}
		this.myEffect = effect;
		this.myEffect.init(this);
	}

	public void removeEffect() {
		this.myEffect = null;
		this.storeTexels = null;
	}

	public void applyEffect() {
		if (this.myEffect != null) {
			this.myEffect.apply(this.texels, this.storeTexels);
			if ((this.myEffect.containsAlpha()) && (!this.alpha)) {
				this.alpha = true;
			}

			setMarker(MARKER_DELETE_AND_UPLOAD);
		} else {
			Logger.log("The texture doesn't have an effect assigned to it!", 0);
		}
	}

	public void setEnabled(boolean isEnabled)
	/*     */{
		/* 451 */
		this.enabled = isEnabled;
		/*     */
	}

	public boolean isEnabled()
	/*     */{
		/* 461 */
		return this.enabled;
		/*     */
	}

	public void enable4bpp(boolean doit) {
		this.convertTo4444 = doit;
		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	public static void defaultTo4bpp(boolean doit) {
		defaultTo4bpp = doit;
	}

	public static void defaultToKeepPixels(boolean doit) {
		defaultToKeepPixels = doit;
	}

	public static void defaultToMipmapping(boolean doit) {
		defaultToMipmapping = doit;
	}

	public void setTextureCompression(boolean enabled) {
		this.etc1 = enabled;
		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	public void compress() {
		if ((this.zippedTexels != null) || (this.myEffect != null)
				|| (this.texels == null)) {
			return;
		}
		this.zippedTexels = ZipHelper.zip(this.texels);

		float ratio = this.zippedTexels.length / (this.texels.length * 4);

		if (ratio <= 0.95F) {
			Logger.log("Texture compressed to " + (int) (ratio * 100.0F)
					+ "% (" + this.zippedTexels.length + "/"
					+ this.texels.length * 4 + ") of uncompressed size!", 3);
			this.texels = null;
		} else {
			Logger.log("Texture not compressed, because compressed size was "
					+ (int) (ratio * 100.0F) + "% (" + this.zippedTexels.length
					+ "/" + this.texels.length * 4 + ") of uncompressed size!",
					3);
			this.zippedTexels = null;
		}
	}

	public int getArraySize() {
		if (this.texels == null) {
			return 0;
		}
		return this.texels.length;
	}

	public int getWidth()
	/*     */{
		/* 566 */
		return this.width;
		/*     */
	}

	public int getHeight()
	/*     */{
		/* 575 */
		return this.height;
		/*     */
	}

	/**
	 * @deprecated
	 */
	public void enableClamping() {
		this.repeat = false;
		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	public void setClamping(boolean clamping) {
		this.repeat = (!clamping);
		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	public void setFiltering(boolean filter) {
		this.bilinear = filter;
		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	public void setMipmap(boolean mipmap) {
		this.mipmap = mipmap;
		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	public void removeAlpha() {
		this.alpha = true;
		for (int i = 0; i < this.texels.length; i++) {
			this.texels[i] |= -16777216;
		}

		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	public void removePixels() {
		this.texels = null;
		this.mipmap = false;
	}

	public void keepPixelData(boolean keepData) {
		this.keepPixels = keepData;
	}

	public void setAsShadowMap(boolean isShadowMap) {
		this.isShadowMap = isShadowMap;
		setMipmap(false);
		this.convertTo4444 = false;
		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	public void setDepthBuffer(DepthBuffer depthBuffer) {
		if ((depthBuffer != null)
				&& ((depthBuffer.getWidth() != getWidth()) || (depthBuffer
						.getHeight() != getHeight()))) {
			Logger.log("Size of depth buffer doesn't match texture size!", 0);
			return;
		}

		this.depthBuffer = depthBuffer;
		setMarker(MARKER_DELETE_AND_UPLOAD);
	}

	public DepthBuffer getDepthBuffer() {
		/* 697 */
		return this.depthBuffer;
		/*     */
	}

	final int getOpenGLID(int renderer) {
		if (this.isConverted) {
			if (renderer == this.lastRenderer) {
				return this.openGLID;
			}
			Integer id = (Integer) this.glIDs.get(IntegerC.valueOf(renderer));
			if (id != null) {
				this.openGLID = id.intValue();
				this.lastRenderer = renderer;
				return this.openGLID;
			}
		}

		return 0;
	}

	final void clearIDs(int renderer) {
		this.openGLID = 0;
		this.markerGL = -999;
		this.lastRenderer = -1;
		this.lastRendererMarker = -1;
		this.glIDs.remove(IntegerC.valueOf(renderer));
		this.marker.remove(IntegerC.valueOf(renderer));
	}

	final void setOpenGLID(int renderer, int id) {
		this.openGLID = id;
		if (id != 0) {
			this.lastRenderer = renderer;
			this.glIDs.put(IntegerC.valueOf(renderer), IntegerC.valueOf(id));
			this.isConverted = true;
			if ((this.myEffect == null) && (!this.keepPixels)) {
				this.texels = null;
				this.zippedTexels = null;
			}
		} else {
			resetIDs();
		}
	}

	final int getMarker(int renderer) {
		if ((this.markerGL == -999) || (this.lastRendererMarker != renderer)) {
			this.lastRendererMarker = renderer;
			if (this.marker.contains(IntegerC.valueOf(renderer)))
				this.markerGL = MARKER_NOTHING;
			else {
				this.markerGL = MARKER_DELETE_AND_UPLOAD;
			}
		}
		return this.markerGL;
	}

	private final void setMarker(int mark) {
		setMarker(-1, mark);
	}

	final void setMarker(int renderer, int mark) {
		if (mark == MARKER_NOTHING) {
			this.marker.add(IntegerC.valueOf(renderer));
			this.markerGL = MARKER_NOTHING;
			this.lastRendererMarker = renderer;
		} else if (mark == MARKER_DELETE_AND_UPLOAD) {
			this.marker.clear();
			this.markerGL = -999;
			this.lastRendererMarker = -1;
		}
	}

	final void refill(int[] pixels, int pw, int ph) {
		for (int y = 0; y < ph; y++) {
			int y1 = y * this.width;
			int y2 = pw * y;
			for (int x = 0; x < pw; x++) {
				this.texels[(y1 + x)] = pixels[(y2 + x)];
			}
		}
		this.isConverted = false;
	}

	private void loadTexture(InputStream is, Bitmap img) {
		loadTexture(is, img, false);
	}

	private void loadTexture(InputStream is, Bitmap img, boolean alpha) {
		this.isLoaded = false;
		Logger.log("Loading Texture...", 2);
		try {
			Bitmap image = img;
			boolean recycleMe = false;
			if (image == null) {
				image = BitmapHelper.loadImage(is);
				recycleMe = true;
			}

			if ((image != null) && (image.getWidth() > 0)) {
				int h = image.getHeight();
				int w = image.getWidth();

				this.width = w;
				this.height = h;
				boolean faulty = false;

				if ((w != 1) && (w != 2) && (w != 4) && (w != 8) && (w != 16)
						&& (w != 32) && (w != 64) && (w != 128) && (w != 256)
						&& (w != 512) && (w != 1024) && (w != 2048)
						&& (w != 4096) && (w != 8192)) {
					faulty = true;
					Logger.log("Unsupported Texture width: " + w, 0);
				}

				if ((h != 1) && (h != 2) && (h != 4) && (h != 8) && (h != 16)
						&& (h != 32) && (h != 64) && (h != 128) && (h != 256)
						&& (h != 512) && (h != 1024) && (h != 2048)
						&& (h != 4096) && (h != 8192)) {
					faulty = true;
					Logger.log("Unsupported Texture height: " + h, 0);
				}

				if (faulty) {
					this.width = 16;
					this.height = 16;
					image = Bitmap.createBitmap(16, 16, Config.ARGB_8888);
				}

				this.texels = new int[this.width * this.height];

				Logger.log("Texture loaded..." + this.texels.length * 4
						+ " bytes/" + w + "*" + h + " pixels!", 2);

				image.getPixels(this.texels, 0, this.width, 0, 0, this.width,
						this.height);
				if (recycleMe) {
					image.recycle();
				}
				this.isLoaded = true;
			} else {
				Logger.log(
						"File not found - replacement texture used instead!", 0);
				this.texels = new int[8];
				this.isLoaded = true;
				this.width = 2;
				this.height = 2;
			}
			this.convertTo4444 = defaultTo4bpp;
			setMipmap(defaultToMipmapping);
			this.keepPixels = defaultToKeepPixels;
		} catch (Exception e) {
			Logger.log(e, 0);
		}

		if (this.isLoaded)
			this.alpha = alpha;
	}

	private static int adjustSize(int size) {
		for (int i = 0; i < TEXTURE_SIZES.length; i++) {
			if (size <= TEXTURE_SIZES[i]) {
				return TEXTURE_SIZES[i];
			}
		}
		return size;
	}

	private void resetIDs() {
		this.openGLID = 0;
		this.markerGL = -999;
		this.lastRenderer = -1;
		this.lastRendererMarker = -1;
		this.glIDs.clear();
		this.marker.clear();
	}

	static Texture createSingleColoredTexture(RGBColor col) {
		int[] pixels = new int[256];
		int color = col.getRed() << 16 | col.getGreen() << 8 | col.getBlue();
		for (int i = 0; i < 256; i++) {
			pixels[i] = color;
		}
		Texture t = new Texture(pixels, 16, 16, false);
		t.isUnicolor = true;
		return t;
	}

	private int clip(int col) {
		if (col < 0) {
			col = 0;
		}
		if (col > 255) {
			col = 255;
		}
		return col;
	}
}
