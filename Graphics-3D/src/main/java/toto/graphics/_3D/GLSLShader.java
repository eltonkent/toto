package toto.graphics._3D;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.opengl.GLES20;

public class GLSLShader {
	private static Map<String, String> REPLACEMENTS = new HashMap();

	private static int NEXT = 0;

	int colorHandle = -1;
	int vertexHandle = -1;
	int normalHandle = -1;
	int tangentHandle = -1;
	int[] textureHandle = new int[4];
	int[] textureUnitHandle = new int[4];
	int mvpMatrixHandle = -1;
	int pMatrixHandle = -1;
	int mvMatrixHandle = -1;
	int texMatrixHandle = -1;
	int textureCountHandle = -1;
	int blendingModeHandle = -1;
	int useColorsHandle = -1;
	int additionalColorHandle = -1;
	int ambientColorHandle = -1;
	int lightPositionsHandle = -1;
	int diffuseColorsHandle = -1;
	int specularColorsHandle = -1;
	int attenuationHandle = -1;
	int shininessHandle = -1;
	int lightCountHandle = -1;
	int fogStartHandle = -1;
	int fogEndHandle = -1;
	int fogColorHandle = -1;
	int alphaHandle = -1;

	boolean needsTangents = false;
	boolean init = false;

	int id = NEXT++;

	boolean directMode = false;

	private int lastContext = -1;

	private int program = 0;

	private String frgSource = null;
	private String vtxSource = null;

	private boolean newUniforms = false;
	private UniformMap uniforms = new UniformMap(null);
	private List<String> toRemove = null;
	private List<String> keyTemp = null;

	private static ShaderLocator locator = null;

	private Matrix tmp = new Matrix();
	private float[] tmpFloats = new float[16];
	private Uniform tmpUniform = new Uniform();
	private Map<String, Integer> attributes = null;

	public GLSLShader(String vertexShaderSource, String fragmentShaderSource) {
		this.frgSource = fragmentShaderSource;
		this.vtxSource = vertexShaderSource;

		this.needsTangents = (vertexShaderSource
				.indexOf("attribute vec4 tangent") != -1);

		this.init = false;
	}

	public static void addReplacement(String key, String shaderSrc) {
		REPLACEMENTS.put(key, shaderSrc);
	}

	public static void setShaderLocator(ShaderLocator locator)
	/*      */{
		/* 217 */
		locator = locator;
		/*      */
	}

	GLSLShader(String appendix) {
		if (locator == null) {
			locator = new ShaderLocator();
		}
		check20();
		try {
			Logger.log("Loading default shaders "
					+ (appendix != null ? "(" + appendix + ")" : "") + "!");
			Logger.log(REPLACEMENTS.size() + " shaders in replacement map!");

			String name = GL20CompLayer.getFragmentShaderName();
			if (appendix != null) {
				int pos = name.indexOf('.');
				name = name.substring(0, pos) + appendix + name.substring(pos);
			}

			String repName = name.replace("/", "");

			String fs = null;
			if (REPLACEMENTS.containsKey(repName)) {
				Logger.log("Using replacement fragment shader instead of default one!");
				fs = (String) REPLACEMENTS.get(repName);
			} else {
				fs = locator.getShaderCode(name);
			}

			name = GL20CompLayer.getVertexShaderName();
			if (appendix != null) {
				int pos = name.indexOf('.');
				name = name.substring(0, pos) + appendix + name.substring(pos);
			}

			repName = name.replace("/", "");

			String vs = null;
			if (REPLACEMENTS.containsKey(repName)) {
				Logger.log("Using replacement vertex shader instead of default one!");
				vs = (String) REPLACEMENTS.get(repName);
			} else {
				vs = locator.getShaderCode(name);
			}

			loadProgram(vs, fs);
		} catch (Exception e) {
			Logger.log("Unable to load shader!", 1);
			Logger.log(e, 0);
		}

		init();
		this.init = true;
	}

	private void check20() {
		if (FrameBuffer.versionHint == 1)
			Logger.log("Shaders are not supported when using OpenGL ES 1.x!", 0);
	}

	void preInit() {
		if (!this.init) {
			check20();
			loadProgram(this.vtxSource, this.frgSource);
			init();
			this.init = true;
		}
	}

	private void init() {
		this.vertexHandle = GLES20
				.glGetAttribLocation(this.program, "position");
		this.mvpMatrixHandle = GLES20.glGetUniformLocation(this.program,
				"modelViewProjectionMatrix");
		this.pMatrixHandle = GLES20.glGetUniformLocation(this.program,
				"projectionMatrix");

		this.colorHandle = GLES20.glGetAttribLocation(this.program, "color");
		this.tangentHandle = GLES20
				.glGetAttribLocation(this.program, "tangent");
		this.normalHandle = GLES20.glGetAttribLocation(this.program, "normal");

		for (int i = 0; i < this.textureHandle.length; i++) {
			this.textureHandle[i] = GLES20.glGetAttribLocation(this.program,
					"texture" + i);
			this.textureUnitHandle[i] = GLES20.glGetUniformLocation(
					this.program, "textureUnit" + i);
		}

		this.mvMatrixHandle = GLES20.glGetUniformLocation(this.program,
				"modelViewMatrix");
		this.texMatrixHandle = GLES20.glGetUniformLocation(this.program,
				"textureMatrix");
		this.alphaHandle = GLES20.glGetUniformLocation(this.program, "alpha");
		this.useColorsHandle = GLES20.glGetUniformLocation(this.program,
				"useColors");
		this.additionalColorHandle = GLES20.glGetUniformLocation(this.program,
				"additionalColor");
		this.ambientColorHandle = GLES20.glGetUniformLocation(this.program,
				"ambientColor");

		this.lightCountHandle = GLES20.glGetUniformLocation(this.program,
				"lightCount");
		this.lightPositionsHandle = GLES20.glGetUniformLocation(this.program,
				"lightPositions");
		this.diffuseColorsHandle = GLES20.glGetUniformLocation(this.program,
				"diffuseColors");
		this.specularColorsHandle = GLES20.glGetUniformLocation(this.program,
				"specularColors");
		this.attenuationHandle = GLES20.glGetUniformLocation(this.program,
				"attenuation");

		this.fogColorHandle = GLES20.glGetUniformLocation(this.program,
				"fogColor");
		this.fogStartHandle = GLES20.glGetUniformLocation(this.program,
				"fogStart");
		this.fogEndHandle = GLES20.glGetUniformLocation(this.program, "fogEnd");

		this.textureCountHandle = GLES20.glGetUniformLocation(this.program,
				"textureCount");
		this.blendingModeHandle = GLES20.glGetUniformLocation(this.program,
				"blendingMode");

		this.shininessHandle = GLES20.glGetUniformLocation(this.program,
				"shininess");

		if (this.vertexHandle == -1) {
			Logger.log("Missing attribute 'position' in vertex shader", 0);
		}

		Logger.log("Handles of " + this.program + ": " + this.vertexHandle
				+ "/" + this.mvpMatrixHandle + "/" + this.normalHandle);

		activate();

		for (int i = 0; i < this.textureUnitHandle.length; i++)
			if (this.textureUnitHandle[i] != -1)
				GLES20.glUniform1i(this.textureUnitHandle[i], i);
	}

	int getAttributeHandle(String name) {
		if (this.attributes == null) {
			Logger.log("Creating attribute/handle mapping!");
			this.attributes = new HashMap();
		}
		Integer handle = (Integer) this.attributes.get(name);
		if (handle == null) {
			int handi = GLES20.glGetAttribLocation(this.program, name);
			if (handi < 0) {
				handi = -1;
			}
			handle = IntegerC.valueOf(handi);
			this.attributes.put(name, handle);
			Logger.log("Handle for attribute " + name + " is " + handi, 3);
		}
		return handle.intValue();
	}

	private int getHandle(String name) {
		if (name.equals("modelViewMatrix")) {
			return this.mvMatrixHandle;
		}
		if (name.equals("modelViewProjectionMatrix")) {
			return this.mvpMatrixHandle;
		}
		if (name.equals("projectionMatrix")) {
			return this.pMatrixHandle;
		}

		if (name.equals("textureMatrix")) {
			return this.texMatrixHandle;
		}
		if (name.equals("alpha")) {
			return this.alphaHandle;
		}
		if (name.equals("useColors")) {
			return this.useColorsHandle;
		}
		if (name.equals("additionalColor")) {
			return this.additionalColorHandle;
		}
		if (name.equals("ambientColor")) {
			return this.ambientColorHandle;
		}
		if (name.equals("lightCount")) {
			return this.lightCountHandle;
		}
		if (name.equals("lightPositions")) {
			return this.lightPositionsHandle;
		}
		if (name.equals("diffuseColors")) {
			return this.diffuseColorsHandle;
		}
		if (name.equals("specularColors")) {
			return this.specularColorsHandle;
		}
		if (name.equals("attenuation")) {
			return this.attenuationHandle;
		}
		if (name.equals("fogColor")) {
			return this.fogColorHandle;
		}
		if (name.equals("fogStart")) {
			return this.fogStartHandle;
		}
		if (name.equals("fogEnd")) {
			return this.fogEndHandle;
		}
		if (name.equals("textureCount")) {
			return this.textureCountHandle;
		}
		if (name.equals("blendingMode")) {
			return this.blendingModeHandle;
		}
		if (name.equals("shininess")) {
			return this.shininessHandle;
		}
		return -1;
	}

	public int getProgram()
	/*      */{
		/* 430 */
		return this.program;
		/*      */
	}

	public void setStaticUniform(String name, int val) {
		set(name, val, true);
	}

	public void setStaticUniform(String name, float val) {
		set(name, val, true);
	}

	public void setStaticUniform(String name, float[] val) {
		set(name, val, true);
	}

	public void setStaticFloatArrayUniform(String name, float[] val) {
		set(name, val, true, true);
	}

	public void setStaticUniform(String name, SimpleVector[] vals) {
		set(name, vals, true);
	}

	public void setStaticUniform(String name, SimpleVector val) {
		set(name, val, true);
	}

	public void setStaticUniform(String name, Matrix m) {
		set(name, toFloatBuffer(m, null), true);
	}

	public void setUniform(String name, int val) {
		set(name, val, false);
	}

	public void setUniform(String name, float val) {
		set(name, val, false);
	}

	public void setUniform(String name, SimpleVector val) {
		set(name, val, false);
	}

	public void setUniform(String name, SimpleVector[] val) {
		set(name, val, false);
	}

	public void setUniform(String name, float[] val) {
		set(name, val, false);
	}

	public void setFloatArrayUniform(String name, float[] val) {
		set(name, val, false, true);
	}

	public void setUniform(String name, Matrix m) {
		Uniform v = this.uniforms.get(name);
		if ((v != null) && (v.getType() == 3))
			set(name, toFloatBuffer(m, v.getMatrixValue()), false);
		else
			set(name, toFloatBuffer(m, null), false);
	}

	void activate(int id) {
		checkContext(id);
		activate();
	}

	void activate() {
		GLES20.glUseProgram(this.program);
	}

	public void dispose() {
		this.program = 0;
	}

	void recompile() {
		Logger.log("Recompiling shader because of context change!");
		loadProgram(this.vtxSource, this.frgSource);
		init();
	}

	void checkContext(int contextId) {
		if ((this.lastContext != -1) && (this.lastContext != contextId)) {
			recompile();
		}
		this.lastContext = contextId;
	}

	void update() {
		if (this.program <= 0) {
			return;
		}

		if (this.newUniforms) {
			if (this.keyTemp == null) {
				this.keyTemp = new ArrayList();
			}
			this.keyTemp.clear();
			this.keyTemp.addAll(this.uniforms.keySet());
			for (int i = 0; i < this.keyTemp.size(); i++) {
				String key = (String) this.keyTemp.get(i);
				Uniform v = this.uniforms.get(key);
				if (!v.hasHandle()) {
					v.setHandle(getLocation(key));
				}
			}
			this.newUniforms = false;
		}

		List values = this.uniforms.values();
		int end = values.size();

		for (int i = 0; i < end; i++) {
			Uniform v = (Uniform) values.get(i);
			setUniform(v);

			if (v.isStaticUniform()) {
				if (this.toRemove == null) {
					this.toRemove = new ArrayList();
				}
				this.toRemove.add(v.getName());
			}
		}

		if (this.toRemove != null) {
			for (int i = 0; i < this.toRemove.size(); i++) {
				this.uniforms.remove((String) this.toRemove.get(i));
			}
			this.toRemove = null;
		}
	}

	private void setUniform(Uniform v) {
		int location = v.getHandle();
		if (location == -1) {
			return;
		}
		int type = v.getType();

		switch (type) {
		case 0:
			GLES20.glUniform1i(location, v.getIntValue());
			break;
		case 1:
			GLES20.glUniform1f(location, v.getFloatValue());
			break;
		case 2:
			float[] f = v.getFloatArrayValue();
			switch (f.length) {
			case 1:
				GLES20.glUniform1f(location, f[0]);
				break;
			case 2:
				GLES20.glUniform2f(location, f[0], f[1]);
				break;
			case 3:
				GLES20.glUniform3f(location, f[0], f[1], f[2]);
				break;
			case 4:
				GLES20.glUniform4f(location, f[0], f[1], f[2], f[3]);
			}

			break;
		case 4:
			SimpleVector vs = v.getVectorValue();
			GLES20.glUniform3f(location, vs.x, vs.y, vs.z);
			break;
		case 3:
			FloatBuffer m = v.getMatrixValue();
			m.rewind();
			GLES20.glUniformMatrix4fv(location, 1, false, m);
			break;
		case 5:
			float[] data = v.getVectorArrayValue();
			GLES20.glUniform3fv(location, data.length / 3, data, 0);
			break;
		case 6:
			float[] fa = v.getFloatArrayValue();
			GLES20.glUniform1fv(location, fa.length, fa, 0);
		}
	}

	private int getLocation(String n) {
		return GLES20.glGetUniformLocation(this.program, n);
	}

	private FloatBuffer toFloatBuffer(Matrix m, FloatBuffer floatBuffer64) {
		if (floatBuffer64 == null)
			floatBuffer64 = ByteBuffer.allocateDirect(64)
					.order(ByteOrder.nativeOrder()).asFloatBuffer();
		else {
			floatBuffer64.rewind();
		}
		this.tmp.setTo(m);
		this.tmp.transformToGL();
		float[] dump = this.tmp.fillDump(this.tmpFloats);
		floatBuffer64.put(dump);
		floatBuffer64.rewind();
		return floatBuffer64;
	}

	private void set(String name, int value, boolean staticy) {
		Uniform v = this.uniforms.get(name);

		if ((v == null) && (this.directMode)) {
			int handle = getHandle(name);
			if (handle != -1) {
				this.tmpUniform.setType(0);
				this.tmpUniform.setHandle(handle);
				v = this.tmpUniform;
			}
		}

		if (v == null) {
			v = new Uniform(0, name);
			this.newUniforms = true;
			this.uniforms.put(name, v);
		}
		v.setValue(value);
		v.setStaticUniform(staticy);

		if (this.directMode)
			setUniform(v);
	}

	private void set(String name, float value, boolean staticy) {
		Uniform v = this.uniforms.get(name);

		if ((v == null) && (this.directMode)) {
			int handle = getHandle(name);
			if (handle != -1) {
				this.tmpUniform.setType(1);
				this.tmpUniform.setHandle(handle);
				v = this.tmpUniform;
			}
		}

		if (v == null) {
			v = new Uniform(1, name);
			this.newUniforms = true;
			this.uniforms.put(name, v);
		}
		v.setValue(value);
		v.setStaticUniform(staticy);

		if (this.directMode)
			setUniform(v);
	}

	private void set(String name, float[] value, boolean staticy) {
		set(name, value, staticy, false);
	}

	private void set(String name, float[] value, boolean staticy,
			boolean asFloatArray) {
		Uniform v = this.uniforms.get(name);

		if ((v == null) && (this.directMode)) {
			int handle = getHandle(name);
			if (handle != -1) {
				this.tmpUniform.setType(asFloatArray ? 6 : 2);
				this.tmpUniform.setHandle(handle);
				v = this.tmpUniform;
			}
		}

		if (v == null) {
			v = new Uniform(asFloatArray ? 6 : 2, name);
			this.newUniforms = true;
			this.uniforms.put(name, v);
		}
		v.setValue(value);
		v.setStaticUniform(staticy);

		if (this.directMode)
			setUniform(v);
	}

	private void set(String name, SimpleVector value, boolean staticy) {
		Uniform v = this.uniforms.get(name);

		if ((v == null) && (this.directMode)) {
			int handle = getHandle(name);
			if (handle != -1) {
				this.tmpUniform.setType(4);
				this.tmpUniform.setHandle(handle);
				v = this.tmpUniform;
			}
		}

		if (v == null) {
			v = new Uniform(4, name);
			this.newUniforms = true;
			this.uniforms.put(name, v);
		}
		v.setValue(value);
		v.setStaticUniform(staticy);

		if (this.directMode)
			setUniform(v);
	}

	private void set(String name, SimpleVector[] values, boolean staticy) {
		Uniform v = this.uniforms.get(name);

		if ((v == null) && (this.directMode)) {
			int handle = getHandle(name);
			if (handle != -1) {
				this.tmpUniform.setType(5);
				this.tmpUniform.setHandle(handle);
				v = this.tmpUniform;
			}
		}

		if (v == null) {
			v = new Uniform(5, name);
			this.newUniforms = true;
			this.uniforms.put(name, v);
		}
		v.setValue(values);
		v.setStaticUniform(staticy);

		if (this.directMode)
			setUniform(v);
	}

	private void set(String name, FloatBuffer value, boolean staticy) {
		Uniform v = this.uniforms.get(name);

		if ((v == null) && (this.directMode)) {
			int handle = getHandle(name);
			if (handle != -1) {
				this.tmpUniform.setType(3);
				this.tmpUniform.setHandle(handle);
				v = this.tmpUniform;
			}
		}

		if (v == null) {
			v = new Uniform(3, name);
			this.newUniforms = true;
			this.uniforms.put(name, v);
		}
		v.setValue(value);
		v.setStaticUniform(staticy);

		if (this.directMode)
			setUniform(v);
	}

	private void loadProgram(String vs, String fs) {
		if (Config.glForceHighPrecision) {
			String ofs = fs;
			fs = fs.replace("precision mediump", "precision highp").replace(
					"precision lowp", "precision highp");
			if (!ofs.equals(fs)) {
				Logger.log("Shader forced to use high precision!");
			}
		}

		this.frgSource = fs;
		this.vtxSource = vs;

		Logger.log("Compiling shader program!");

		int vertexShader = loadShader(35633, vs);

		if (vertexShader == 0) {
			checkError("load shader");
			Logger.log("Failed to load and compile vertex shaders!", 0);
		}

		int fragmentShader = loadShader(35632, fs);

		if (fragmentShader == 0) {
			checkError("load shader");
			Logger.log("Failed to load and compile fragment shaders!", 0);
		}
		this.program = createProgram(vertexShader, fragmentShader);
	}

	private int createProgram(int vertexShader2, int fragmentShader2) {
		int program = GLES20.glCreateProgram();
		if (program != 0) {
			GLES20.glAttachShader(program, vertexShader2);
			checkError("glAttachShader - vertex shader");
			GLES20.glAttachShader(program, fragmentShader2);
			checkError("glAttachShader -  fragment shader");
			GLES20.glLinkProgram(program);
			int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(program, 35714, linkStatus, 0);
			if (linkStatus[0] != 1) {
				Logger.log(
						"Could not link shader program: "
								+ GLES20.glGetProgramInfoLog(program), 0);
				GLES20.glDeleteProgram(program);
				program = 0;
			}
		}
		return program;
	}

	private int loadShader(int shaderType, String source) {
		int shader = GLES20.glCreateShader(shaderType);
		if (shader != 0) {
			GLES20.glShaderSource(shader, source);
			GLES20.glCompileShader(shader);
			int[] compiled = new int[1];
			GLES20.glGetShaderiv(shader, 35713, compiled, 0);
			if (compiled[0] == 0) {
				Logger.log("Could not compile shader " + shaderType + ": "
						+ GLES20.glGetShaderInfoLog(shader));
				GLES20.glDeleteShader(shader);
				shader = 0;
			}
		}
		return shader;
	}

	private void checkError(String op) {
		int error;
		while ((error = GLES20.glGetError()) != 0) {

			Logger.log(op + ": glError " + error, 0);
		}
	}

	public void finalize() {
		dispose();
	}

	private static class Uniform {
		public static final int INT = 0;
		public static final int FLOAT = 1;
		public static final int FLOAT_ARRAY = 2;
		public static final int MATRIX = 3;
		public static final int VECTOR = 4;
		public static final int VECTOR_ARRAY = 5;
		public static final int SINGLE_FLOAT_ARRAY = 6;
		private int type = 0;
		private int iValue = 0;
		private float fValue = 0.0F;
		private float[] faValue = null;
		private FloatBuffer mValue = null;
		private SimpleVector sValue = null;
		private float[] saData = null;
		private int handle = -1;
		private boolean staticUniform = false;
		private String name = null;

		public Uniform() {
			this(0, "--tmp--");
		}

		public Uniform(int type, String name) {
			this.type = type;
			this.name = name;
		}

		public String getName() {
			/* 1100 */
			return this.name;
			/*      */
		}

		public int getHandle() {
			/* 1104 */
			return this.handle;
			/*      */
		}

		public void setHandle(int handle) {
			/* 1108 */
			this.handle = handle;
			/*      */
		}

		public boolean hasHandle() {
			return this.handle != -1;
		}

		public int getType() {
			/* 1116 */
			return this.type;
			/*      */
		}

		public void setType(int type) {
			/* 1120 */
			this.type = type;
			/*      */
		}

		public int getIntValue() {
			/* 1124 */
			return this.iValue;
			/*      */
		}

		public float getFloatValue() {
			/* 1128 */
			return this.fValue;
			/*      */
		}

		public float[] getFloatArrayValue() {
			/* 1132 */
			return this.faValue;
			/*      */
		}

		public FloatBuffer getMatrixValue() {
			/* 1136 */
			return this.mValue;
			/*      */
		}

		public SimpleVector getVectorValue() {
			/* 1140 */
			return this.sValue;
			/*      */
		}

		public float[] getVectorArrayValue() {
			/* 1144 */
			return this.saData;
			/*      */
		}

		public void setValue(int value) {
			/* 1148 */
			this.iValue = value;
			/*      */
		}

		public void setValue(float value) {
			/* 1152 */
			this.fValue = value;
			/*      */
		}

		public void setValue(float[] value) {
			/* 1156 */
			this.faValue = value;
			/*      */
		}

		public void setValue(FloatBuffer value) {
			/* 1160 */
			this.mValue = value;
			/*      */
		}

		public void setValue(SimpleVector value) {
			/* 1164 */
			this.sValue = value;
			/*      */
		}

		public void setValue(SimpleVector[] values) {
			if (this.saData == null) {
				this.saData = new float[3 * values.length];
			}
			int end = values.length;
			int c = 0;
			for (int i = 0; i < end; i++) {
				SimpleVector v = values[i];
				this.saData[(c++)] = v.x;
				this.saData[(c++)] = v.y;
				this.saData[(c++)] = v.z;
			}
		}

		public void setStaticUniform(boolean staticUniform) {
			/* 1182 */
			this.staticUniform = staticUniform;
			/*      */
		}

		public boolean isStaticUniform() {
			/* 1186 */
			return this.staticUniform;
			/*      */
		}
	}

	private static class UniformMap {

		private UniformMap() {
			uniforms = new ArrayList(4);
			keys = new HashSet();
			lastUniform = null;
			lastName = null;
		}

		UniformMap(UniformMap uniformmap) {
			this();
		}

		private List<Uniform> uniforms = new ArrayList(4);
		private Set<String> keys = new HashSet();
		private Uniform lastUniform = null;
		private String lastName = null;

		public Uniform get(String name) {
			if ((name.equals(this.lastName)) && (this.lastUniform != null)) {
				return this.lastUniform;
			}
			int end = this.uniforms.size();
			for (int i = 0; i < end; i++) {
				Uniform u = (Uniform) this.uniforms.get(i);
				if (u.name.equals(name)) {
					this.lastUniform = u;
					this.lastName = name;
					return u;
				}
			}
			return null;
		}

		public void put(String name, Uniform uniform) {
			int end = this.uniforms.size();
			this.keys.add(name);
			for (int i = 0; i < end; i++) {
				Uniform u = (Uniform) this.uniforms.get(i);
				if (u.name.equals(name)) {
					this.uniforms.set(i, uniform);
					return;
				}
			}
			this.uniforms.add(uniform);
		}

		public void remove(String name) {
			int end = this.uniforms.size();
			for (int i = 0; i < end; i++) {
				Uniform u = (Uniform) this.uniforms.get(i);
				if (u.name.equals(name)) {
					this.uniforms.remove(i);
					this.keys.remove(name);
					return;
				}
			}
		}

		public List<Uniform> values() {
			return this.uniforms;
		}

		public Set<String> keySet() {
			return this.keys;
		}
	}
}
