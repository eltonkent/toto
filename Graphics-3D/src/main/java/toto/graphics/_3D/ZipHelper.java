package toto.graphics._3D;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/*     */
/*     */
/*     */

class ZipHelper {
	private static byte[] buffer = new byte[1024];
	private static ByteArrayOutputStream myOutput = null;

	static synchronized int[] unzip(byte[] zipped) {
		byte[] data = zipped;
		long start = System.currentTimeMillis();
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		checkBuffer();
		ByteArrayOutputStream bos = myOutput;
		bos.reset();
		try {
			GZIPInputStream zis = new GZIPInputStream(bis);
			int cnt = 0;
			do {
				cnt = zis.read(buffer);
				if (cnt != -1)
					bos.write(buffer, 0, cnt);
			} while (cnt >= 0);
			zis.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		if (Logger.isDebugEnabled()) {
			Logger.log(
					"Uncompressed " + zipped.length + " bytes to " + bos.size()
							+ " bytes in "
							+ (System.currentTimeMillis() - start) + "ms!", 3);
		}

		if (bos.size() > Config.byteBufferLimit) {
			myOutput = null;
		}
		return byteArrayToInt(bos.toByteArray());
	}

	private static void checkBuffer() {
		if (myOutput == null)
			myOutput = new ByteArrayOutputStream(16384);
	}

	static synchronized byte[] zip(int[] unzipped) {
		checkBuffer();
		ByteArrayOutputStream bos = myOutput;
		bos.reset();
		try {
			GZIPOutputStream zos = new GZIPOutputStream(bos);
			int pos = 0;
			int cur = 0;
			while ((cur = buffer(unzipped, pos)) > 0) {
				zos.write(buffer, 0, cur << 2);
				pos += cur;
			}
			zos.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		if (Logger.isDebugEnabled()) {
			Logger.log("Compressed " + (unzipped.length << 2) + " bytes to "
					+ bos.size() + " bytes!", 3);
		}

		if (bos.size() > Config.byteBufferLimit) {
			myOutput = null;
		}
		return bos.toByteArray();
	}

	private static int buffer(int[] data, int pos) {
		int cnt = 0;
		int end = Math.min(buffer.length >> 2, data.length - pos) + pos;
		int ii = 0;
		for (int i = pos; i < end; i++) {
			int vali = data[i];
			buffer[(cnt++)] = ((byte) (vali >>> 24));
			buffer[(cnt++)] = ((byte) (vali >>> 16));
			buffer[(cnt++)] = ((byte) (vali >>> 8));
			buffer[(cnt++)] = ((byte) vali);
			ii++;
		}
		return ii;
	}

	public static byte[] intToByteArray(int[] value) {
		byte[] data = new byte[value.length << 2];
		int cnt = 0;
		int end = value.length;
		for (int i = 0; i < end; i++) {
			int vali = value[i];
			data[(cnt++)] = ((byte) (vali >>> 24));
			data[(cnt++)] = ((byte) (vali >>> 16));
			data[(cnt++)] = ((byte) (vali >>> 8));
			data[(cnt++)] = ((byte) vali);
		}
		return data;
	}

	public static int[] byteArrayToInt(byte[] b) {
		int[] data = new int[b.length + 3 >> 2];
		int cnt = 0;
		int end = b.length;
		for (int i = 0; i < end; i++) {
			int val = b[i] << 24;
			i++;
			if (i < end) {
				val |= (b[i] & 0xFF) << 16;
			}
			i++;
			if (i < end) {
				val |= (b[i] & 0xFF) << 8;
			}
			i++;
			if (i < end) {
				val |= b[i] & 0xFF;
			}
			data[(cnt++)] = val;
		}
		return data;
	}
}
