package toto.graphics._3D;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.BitSet;

import javax.microedition.khronos.opengles.GL11;

import android.opengl.ETC1Util;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Build;

class GL20 implements GL11, GL20Handler {
	private static int NEXT = 0;

	private GLSLShader defaultShader = null;
	private GLSLShader defaultShaderTex0 = null;
	private GLSLShader defaultShaderTex1 = null;
	private GLSLShader defaultShaderTex0Light0 = null;
	private GLSLShader defaultShaderFog = null;
	private GLSLShader defaultShaderFogLight0 = null;
	private GLSLShader defaultShaderTex0Amb = null;
	private GLSLShader defaultShaderDepth = null;
	private GLSLShader userShader = null;

	private final float[] curColor = new float[4];

	private int currentTextureStage = 0;

	private final BitSet textureStates = new BitSet(64);
	private final int[] textureModes = new int[4];

	private int textureCombineRgb = 0;
	private int textureRgbScale = 1;

	private boolean flatShading = false;

	private float[] currentMatrixPointer = null;
	private final float[] projectionMatrix = new float[16];
	private final float[] modelviewMatrix = new float[16];
	private final float[] textureMatrix = new float[16];
	private final float[] tempMatrix = new float[16];
	private boolean textureMatrixUsed = false;

	private GLSLShader activeShader = null;

	private float alpha = 1.0F;

	private boolean useColors = false;

	private final float[] additionalColor = new float[4];

	private final float[] ambientColor = new float[4];

	private final float[] lightPositions = new float[24];

	private final float[] diffuseColors = new float[24];

	private final float[] specularColors = new float[24];

	private final float[] attenuation = new float[8];

	private float shininess = 0.0F;

	private int lightCount = 0;

	private boolean fogEnabled = false;
	private float fogStart = -1.0F;
	private float fogEnd = -1.0F;
	private final float[] fogColor = new float[3];

	private boolean lightsEnabled = false;
	private final BitSet lightsState = new BitSet(64);

	private final int id = NEXT++;

	private int fbo = -1;

	private int fboTexture = -1;
	private boolean depthMode = false;
	private GLSLShader storedShader = null;
	private GLSLShader storedUserShader = null;
	private final Texture fboColorStorage = null;
	private final Texture fboDepthStorage = null;

	private boolean loggedShaderMsg = false;

	private final IntBuffer buffy4 = ByteBuffer.allocateDirect(4)
			.order(ByteOrder.nativeOrder()).asIntBuffer();

	public GL20() {
		Logger.log("Initializing GL20 render pipeline...");
		if ((Config.useVBO) && (Build.VERSION.SDK_INT == 8)) {
			Config.useVBO = false;
			Logger.log("Support for VBO when running OpenGL ES 2.0 is faulty in Android 2.2...VBO will be disabled!");
		}

		this.defaultShader = new GLSLShader(null);
		this.defaultShaderTex0Amb = new GLSLShader("Tex0Amb");

		if (!Config.skipDefaultShaders) {
			this.defaultShaderTex0 = new GLSLShader("Tex0");
			this.defaultShaderTex1 = new GLSLShader("Tex1");
			this.defaultShaderTex0Light0 = new GLSLShader("Tex0Light0");
			this.defaultShaderFog = new GLSLShader("Fog");
			this.defaultShaderFogLight0 = new GLSLShader("FogLight0");
			this.defaultShaderDepth = new GLSLShader("Depth");
		}

		setIdentity(this.projectionMatrix);
		setIdentity(this.modelviewMatrix);
		setIdentity(this.textureMatrix);
		setIdentity(this.tempMatrix);

		this.currentMatrixPointer = this.projectionMatrix;

		resetShaderData();
		updateShaderData();

		Logger.log("GL20 render pipeline initialized!");
	}

	private void checkError(final String op) {
		int error;
		while ((error = GLES20.glGetError()) != 0) {
			Logger.log(op + ": glError " + error, 0);
			System.exit(error);
		}
	}

	public void reset() {
		if (this.depthMode) {
			return;
		}
		this.activeShader = null;
	}

	public GLSLShader updateShaderData() {
		if ((this.activeShader != this.defaultShaderDepth) && (this.depthMode)) {
			Logger.log(
					"Can't render into a depth map without the shader being setKey!",
					0);
		}

		if (this.activeShader == null) {
			this.activeShader = this.defaultShader;
			this.activeShader.activate(this.id);
		}

		this.lightCount = this.lightsState.length();

		int lights = this.lightCount;
		if (!this.lightsEnabled) {
			lights = 0;
		}

		if ((this.userShader == null)
				&& (this.activeShader != this.defaultShaderDepth)) {
			final int texCount = this.textureStates.length();
			GLSLShader tmp = this.activeShader;

			if (texCount > 1) {
				if (texCount == 2)
					tmp = this.defaultShaderTex1;
				else
					tmp = this.defaultShader;
			} else {
				tmp = this.defaultShaderTex0;
				if (this.fogEnabled) {
					if (lights != 1)
						tmp = this.defaultShaderFog;
					else {
						tmp = this.defaultShaderFogLight0;
					}
				} else if (!this.textureMatrixUsed) {
					if (lights == 0) {
						tmp = this.defaultShaderTex0Amb;
					} else if (lights == 1) {
						tmp = this.defaultShaderTex0Light0;
					}

				}

			}

			if (tmp == null) {
				tmp = this.defaultShader;
				if (!this.loggedShaderMsg) {
					this.loggedShaderMsg = true;
					Logger.log(
							"Your application uses default shader, but they have been disabled in Config. Consider to enable them or performance will suffer!",
							1);
				}
			}

			if (tmp != this.activeShader) {
				tmp.activate(this.id);
				this.activeShader = tmp;
			}
		}

		final GLSLShader newShader = this.activeShader;

		newShader.checkContext(this.id);

		if (this.userShader == this.activeShader) {
			this.userShader.update();
		}

		if (newShader.mvpMatrixHandle != -1) {
			Matrix.multiplyMM(this.tempMatrix, 0, this.projectionMatrix, 0,
					this.modelviewMatrix, 0);
			GLES20.glUniformMatrix4fv(newShader.mvpMatrixHandle, 1, false,
					this.tempMatrix, 0);
		}

		if (newShader.mvMatrixHandle != -1) {
			GLES20.glUniformMatrix4fv(newShader.mvMatrixHandle, 1, false,
					this.modelviewMatrix, 0);
		}

		if (newShader.texMatrixHandle != -1) {
			GLES20.glUniformMatrix4fv(newShader.texMatrixHandle, 1, false,
					this.textureMatrix, 0);
		}

		if (newShader.pMatrixHandle != -1) {
			GLES20.glUniformMatrix4fv(newShader.pMatrixHandle, 1, false,
					this.projectionMatrix, 0);
		}

		if (newShader.alphaHandle != -1) {
			GLES20.glUniform1f(newShader.alphaHandle, this.alpha);
		}

		if (newShader.useColorsHandle != -1) {
			GLES20.glUniform1i(newShader.useColorsHandle, this.useColors ? 1
					: 0);
		}

		if (newShader.additionalColorHandle != -1) {
			GLES20.glUniform4fv(newShader.additionalColorHandle, 1,
					this.additionalColor, 0);
		}

		if (newShader.ambientColorHandle != -1) {
			GLES20.glUniform4fv(newShader.ambientColorHandle, 1,
					this.ambientColor, 0);
		}

		if ((this.lightsEnabled) && (this.lightCount > 0)) {
			int pos = lights * 3;
			if (pos < this.diffuseColors.length) {
				this.diffuseColors[(pos++)] = -999.0F;
				this.diffuseColors[(pos++)] = -999.0F;
				this.diffuseColors[pos] = -999.0F;
			}

			if (newShader.lightPositionsHandle != -1) {
				GLES20.glUniform3fv(newShader.lightPositionsHandle,
						this.lightCount, this.lightPositions, 0);
			}

			if (newShader.attenuationHandle != -1) {
				GLES20.glUniform1fv(newShader.attenuationHandle,
						this.lightCount, this.attenuation, 0);
			}

			if (newShader.diffuseColorsHandle != -1) {
				GLES20.glUniform3fv(newShader.diffuseColorsHandle,
						this.lightCount, this.diffuseColors, 0);
			}

			if (newShader.specularColorsHandle != -1) {
				GLES20.glUniform3fv(newShader.specularColorsHandle,
						this.lightCount, this.specularColors, 0);
			}
		}

		if (newShader.lightCountHandle != -1) {
			GLES20.glUniform1i(newShader.lightCountHandle, lights);
		}

		if (newShader.shininessHandle != 0) {
			GLES20.glUniform1f(newShader.shininessHandle, this.shininess);
		}

		if (this.fogEnabled) {
			if (newShader.fogStartHandle != -1) {
				GLES20.glUniform1f(newShader.fogStartHandle, this.fogStart);
			}
			if (newShader.fogEndHandle != -1) {
				GLES20.glUniform1f(newShader.fogEndHandle, this.fogEnd);
			}
			if (newShader.fogColorHandle != -1) {
				GLES20.glUniform3fv(newShader.fogColorHandle, 1, this.fogColor,
						0);
			}
		} else if (newShader.fogStartHandle != -1) {
			GLES20.glUniform1f(newShader.fogStartHandle, -1.0F);
		}

		final int textureCount = this.textureStates.length();

		if (newShader.textureCountHandle != -1) {
			GLES20.glUniform1i(newShader.textureCountHandle, textureCount);
		}

		if (newShader.blendingModeHandle != -1) {
			GLES20.glUniform1iv(newShader.blendingModeHandle, textureCount,
					this.textureModes, 0);
		}

		return newShader;
	}

	public void resetShaderData() {
		this.alpha = 1.0F;
		this.useColors = false;
		this.shininess = 0.0F;

		for (int i = 0; i < 4; i++) {
			this.additionalColor[i] = 0.0F;
			this.ambientColor[i] = 1.0F;
		}
	}

	public void setShader(final GLSLShader shader) {
		if (this.depthMode) {
			return;
		}

		this.userShader = shader;

		if (shader != null) {
			shader.preInit();
		}

		if (this.activeShader != this.userShader)
			if (this.userShader != null) {
				this.userShader.activate(this.id);
				this.activeShader = this.userShader;
			} else {
				this.defaultShader.activate(this.id);
				this.activeShader = this.defaultShader;
			}
	}

	public void clearShader() {
		setShader(null);
	}

	public void glActiveTexture(final int texture) {
		GLES20.glActiveTexture(texture);
		this.currentTextureStage = texture;
	}

	public void glAlphaFunc(final int func, final float ref) {
		throw new RuntimeException("Not implemented!");
	}

	public void glAlphaFuncx(final int func, final int ref) {
		throw new RuntimeException("Not implemented!");
	}

	public void glBindTexture(final int target, final int texture) {
		GLES20.glBindTexture(target, texture);
	}

	public void glBlendFunc(final int sfactor, final int dfactor) {
		GLES20.glBlendFunc(sfactor, dfactor);
	}

	public void glClear(final int mask) {
		GLES20.glClear(mask);
	}

	public void glClearColor(final float red, final float green,
			final float blue, final float alpha) {
		GLES20.glClearColor(red, green, blue, alpha);
	}

	public void glClearColorx(final int red, final int green, final int blue,
			final int alpha) {
		GLES20.glClearColor((int) (red * 255.0F), (int) (green * 255.0F),
				(int) (blue * 255.0F), (int) (alpha * 255.0F));
	}

	public void glClearDepthf(final float depth) {
		GLES20.glClearDepthf(depth);
	}

	public void glClearDepthx(final int depth) {
		throw new RuntimeException("Not implemented!");
	}

	public void glClearStencil(final int s) {
		GLES20.glClearStencil(s);
	}

	public void glClientActiveTexture(final int texture) {
		GLES20.glActiveTexture(texture);
		this.currentTextureStage = texture;
	}

	public void glColor4f(final float red, final float green, final float blue,
			final float alpha) {
		this.curColor[0] = red;
		this.curColor[1] = green;
		this.curColor[2] = blue;
		this.curColor[3] = alpha;
	}

	public void glColor4x(final int red, final int green, final int blue,
			final int alpha) {
		this.curColor[0] = ((int) (red * 255.0F));
		this.curColor[1] = ((int) (green * 255.0F));
		this.curColor[2] = ((int) (blue * 255.0F));
		this.curColor[3] = ((int) (alpha * 255.0F));
	}

	public void glColorMask(final boolean red, final boolean green,
			final boolean blue, final boolean alpha) {
		GLES20.glColorMask(red, green, blue, alpha);
	}

	public void glColorPointer(final int size, final int type,
			final int stride, final Buffer pointer) {
		if (this.activeShader.colorHandle != -1) {
			GLES20.glVertexAttribPointer(this.activeShader.colorHandle, size,
					type, false, stride, pointer);

			if (pointer == null)
				this.useColors = false;
		}
	}

	public void glCompressedTexImage2D(final int target, final int level,
			final int internalformat, final int width, final int height,
			final int border, final int imageSize, final Buffer data) {
		GLES20.glCompressedTexImage2D(target, level, internalformat, width,
				height, border, imageSize, data);
	}

	public void glCompressedTexSubImage2D(final int target, final int level,
			final int xoffset, final int yoffset, final int width,
			final int height, final int format, final int imageSize,
			final Buffer data) {
		GLES20.glCompressedTexSubImage2D(target, level, xoffset, yoffset,
				width, height, format, imageSize, data);
	}

	public void glCopyTexImage2D(final int target, final int level,
			final int internalformat, final int x, final int y,
			final int width, final int height, final int border) {
		GLES20.glCopyTexImage2D(target, level, internalformat, x, y, width,
				height, border);
	}

	public void glCopyTexSubImage2D(final int target, final int level,
			final int xoffset, final int yoffset, final int x, final int y,
			final int width, final int height) {
		GLES20.glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y,
				width, height);
	}

	public void glCullFace(final int mode) {
		GLES20.glCullFace(mode);
	}

	public void glDeleteTextures(final int n, final IntBuffer textures) {
		GLES20.glDeleteTextures(n, textures);
	}

	public void glDeleteTextures(final int n, final int[] textures,
			final int offset) {
		GLES20.glDeleteTextures(n, textures, offset);
	}

	public void glDepthFunc(final int func) {
		GLES20.glDepthFunc(func);
	}

	public void glDepthMask(final boolean flag) {
		GLES20.glDepthMask(flag);
	}

	public void glDepthRangef(final float zNear, final float zFar) {
		GLES20.glDepthRangef(zNear, zFar);
	}

	public void glDepthRangex(final int zNear, final int zFar) {
		throw new RuntimeException("Not implemented!");
	}

	public void glDisable(final int cap) {
		if (hasCap(cap)) {
			if (cap == 3553) {
				this.textureStates.set(getTextureStageIndex(), false);
			} else
				GLES20.glDisable(cap);

		} else if (cap == 2896)
			this.lightsEnabled = false;
		else if ((cap >= 16384) && (cap <= 16391))
			this.lightsState.set(cap - 16384, false);
		else if (cap == 2912)
			this.fogEnabled = false;
	}

	public void glDisableClientState(final int array) {
		if ((array == 32886) && (this.activeShader.colorHandle != -1)) {
			GLES20.glDisableVertexAttribArray(this.activeShader.colorHandle);
			if (this.activeShader.useColorsHandle != -1) {
				GLES20.glUniform1i(this.activeShader.useColorsHandle, 0);
			}
			this.useColors = false;
		}

		if ((array == 32884) && (this.activeShader.vertexHandle != -1)) {
			GLES20.glDisableVertexAttribArray(this.activeShader.vertexHandle);
		}

		if ((array == 32885) && (this.activeShader.normalHandle != -1)) {
			GLES20.glDisableVertexAttribArray(this.activeShader.normalHandle);
		}

		final int textureIndex = getTextureStageIndex();
		if ((array == 32888)
				&& (this.activeShader.textureHandle[textureIndex] != -1))
			GLES20.glDisableVertexAttribArray(this.activeShader.textureHandle[textureIndex]);
	}

	public void glDrawArrays(final int mode, final int first, final int count) {
		GLES20.glDrawArrays(mode, first, count);
	}

	public void glDrawElements(final int mode, final int count, final int type,
			final Buffer indices) {
		GLES20.glDrawElements(mode, count, type, indices);
	}

	public void glEnable(final int cap) {
		if (hasCap(cap)) {
			if (cap == 3553) {
				this.textureStates.set(getTextureStageIndex(), true);
			} else {
				GLES20.glEnable(cap);
			}
		} else if (cap == 2896)
			this.lightsEnabled = true;
		else if ((cap >= 16384) && (cap <= 16391))
			this.lightsState.set(cap - 16384, true);
		else if (cap == 2912)
			this.fogEnabled = true;
	}

	public void glEnableClientState(final int array) {
		if ((array == 32886) && (this.activeShader.colorHandle != -1)) {
			GLES20.glEnableVertexAttribArray(this.activeShader.colorHandle);
			if (this.activeShader.useColorsHandle != -1) {
				GLES20.glUniform1i(this.activeShader.useColorsHandle, 1);
			}
			this.useColors = true;
		}

		if ((array == 32884) && (this.activeShader.vertexHandle != -1)) {
			GLES20.glEnableVertexAttribArray(this.activeShader.vertexHandle);
		}

		if ((array == 32885) && (this.activeShader.normalHandle != -1)) {
			GLES20.glEnableVertexAttribArray(this.activeShader.normalHandle);
		}

		final int textureIndex = getTextureStageIndex();
		if ((array == 32888)
				&& (this.activeShader.textureHandle[textureIndex] != -1))
			GLES20.glEnableVertexAttribArray(this.activeShader.textureHandle[textureIndex]);
	}

	public void glFinish() {
		GLES20.glFinish();
	}

	public void glFlush() {
		GLES20.glFlush();
	}

	public void glFogf(final int pname, final float param) {
		if (pname == 2915)
			this.fogStart = param;
		else if (pname == 2916)
			this.fogEnd = param;
	}

	public void glFogfv(final int pname, final FloatBuffer params) {
		if (pname == 2918) {
			this.fogColor[0] = params.get(0);
			this.fogColor[1] = params.get(1);
			this.fogColor[2] = params.get(2);
		}
	}

	public void glFogfv(final int pname, final float[] params, final int offset) {
	}

	public void glFogx(final int pname, final int param) {
	}

	public void glFogxv(final int pname, final IntBuffer params) {
	}

	public void glFogxv(final int pname, final int[] params, final int offset) {
	}

	public void glFrontFace(final int mode) {
		GLES20.glFrontFace(mode);
	}

	public void glFrustumf(final float left, final float right,
			final float bottom, final float top, final float near,
			final float far) {
		Matrix.frustumM(this.currentMatrixPointer, 0, left, right, bottom, top,
				near, far);
	}

	public void glFrustumx(final int left, final int right, final int bottom,
			final int top, final int near, final int far) {
		Matrix.frustumM(this.currentMatrixPointer, 0, left, right, bottom, top,
				near, far);
	}

	public void glGenTextures(final int n, final IntBuffer textures) {
		GLES20.glGenTextures(n, textures);
	}

	public void glGenTextures(final int n, final int[] textures,
			final int offset) {
		GLES20.glGenTextures(n, textures, offset);
	}

	public int glGetError() {
		return GLES20.glGetError();
	}

	public void glGetIntegerv(final int pname, final IntBuffer params) {
		GLES20.glGetIntegerv(pname, params);
	}

	public void glGetIntegerv(final int pname, final int[] params,
			final int offset) {
		GLES20.glGetIntegerv(pname, params, offset);
	}

	public String glGetString(final int name) {
		return GLES20.glGetString(name);
	}

	public void glHint(final int target, final int mode) {
		GLES20.glHint(target, mode);
	}

	public void glLightModelf(final int pname, final float param) {
	}

	public void glLightModelfv(final int pname, final FloatBuffer params) {
	}

	public void glLightModelfv(final int pname, final float[] params,
			final int offset) {
		if (pname == 2899) {
			this.ambientColor[0] = params[0];
			this.ambientColor[1] = params[1];
			this.ambientColor[2] = params[2];
			this.ambientColor[3] = params[3];
		}
	}

	public void glLightModelx(final int pname, final int param) {
	}

	public void glLightModelxv(final int pname, final IntBuffer params) {
	}

	public void glLightModelxv(final int pname, final int[] params,
			final int offset) {
	}

	public void glLightf(final int light, final int pname, final float param) {
		final int pos = light - 16384;

		if (pname == 4616)
			this.attenuation[pos] = param;
	}

	public void glLightfv(final int light, final int pname,
			final FloatBuffer params) {
	}

	public void glLightfv(final int light, final int pname,
			final float[] params, final int offset) {
		final int pos = light - 16384;
		int index = pos * 3;

		if (pname == 4611) {
			this.lightPositions[(index++)] = params[0];
			this.lightPositions[(index++)] = params[1];
			this.lightPositions[index] = params[2];
		} else if (pname == 4609) {
			this.diffuseColors[(index++)] = params[0];
			this.diffuseColors[(index++)] = params[1];
			this.diffuseColors[index] = params[2];
		} else if (pname == 4610) {
			this.specularColors[(index++)] = params[0];
			this.specularColors[(index++)] = params[1];
			this.specularColors[index] = params[2];
		}
	}

	public void glLightx(final int light, final int pname, final int param) {
	}

	public void glLightxv(final int light, final int pname,
			final IntBuffer params) {
	}

	public void glLightxv(final int light, final int pname, final int[] params,
			final int offset) {
	}

	public void glLineWidth(final float width) {
		GLES20.glLineWidth(width);
	}

	public void glLineWidthx(final int width) {
		GLES20.glLineWidth(width);
	}

	public void glLoadIdentity() {
		setIdentity(this.currentMatrixPointer);
		if (this.currentMatrixPointer == this.textureMatrix)
			this.textureMatrixUsed = false;
	}

	public void glLoadMatrixf(final FloatBuffer m) {
		System.arraycopy(m.array(), 0, this.currentMatrixPointer, 0, 16);
		if (this.currentMatrixPointer == this.textureMatrix)
			this.textureMatrixUsed = true;
	}

	public void glLoadMatrixf(final float[] m, final int offset) {
		System.arraycopy(m, offset, this.currentMatrixPointer, 0, 16);
		if (this.currentMatrixPointer == this.textureMatrix)
			this.textureMatrixUsed = true;
	}

	public void glLoadMatrixx(final IntBuffer m) {
		for (int i = 0; i < 16; i++) {
			this.currentMatrixPointer[i] = m.get(i);
		}
		if (this.currentMatrixPointer == this.textureMatrix)
			this.textureMatrixUsed = true;
	}

	public void glLoadMatrixx(final int[] m, final int offset) {
		for (int i = 0; i < 16; i++) {
			this.currentMatrixPointer[i] = m[(i + offset)];
		}
		if (this.currentMatrixPointer == this.textureMatrix)
			this.textureMatrixUsed = true;
	}

	public void glLogicOp(final int opcode) {
		throw new RuntimeException("Not implemented!");
	}

	public void glMaterialf(final int face, final int pname, final float param) {
		if ((face == 1032) && (pname == 5632))
			this.shininess = param;
	}

	public void glMaterialfv(final int face, final int pname,
			final FloatBuffer params) {
	}

	public void glMaterialfv(final int face, final int pname,
			final float[] params, final int offset) {
		if ((face == 1032) && (pname == 5632)) {
			this.alpha = params[3];
			this.additionalColor[0] = params[0];
			this.additionalColor[1] = params[1];
			this.additionalColor[2] = params[2];
		}
	}

	public void glMaterialx(final int face, final int pname, final int param) {
	}

	public void glMaterialxv(final int face, final int pname,
			final IntBuffer params) {
	}

	public void glMaterialxv(final int face, final int pname,
			final int[] params, final int offset) {
	}

	public void glMatrixMode(final int mode) {
		if (mode == 5888)
			this.currentMatrixPointer = this.modelviewMatrix;
		else if (mode == 5890)
			this.currentMatrixPointer = this.textureMatrix;
		else if (mode == 5889)
			this.currentMatrixPointer = this.projectionMatrix;
		else
			throw new RuntimeException("Unsupported matrix mode: " + mode);
	}

	public void glMultMatrixf(final FloatBuffer m) {
		Matrix.multiplyMM(this.tempMatrix, 0, this.currentMatrixPointer, 0,
				m.array(), 0);
		System.arraycopy(this.tempMatrix, 0, this.currentMatrixPointer, 0, 16);
	}

	public void glMultMatrixf(final float[] m, final int offset) {
		Matrix.multiplyMM(this.tempMatrix, 0, this.currentMatrixPointer, 0, m,
				0);
		System.arraycopy(this.tempMatrix, 0, this.currentMatrixPointer, 0, 16);
	}

	public void glMultMatrixx(final IntBuffer m) {
		throw new RuntimeException("Not implemented!");
	}

	public void glMultMatrixx(final int[] m, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glMultiTexCoord4f(final int target, final float s,
			final float t, final float r, final float q) {
		throw new RuntimeException("Not implemented!");
	}

	public void glMultiTexCoord4x(final int target, final int s, final int t,
			final int r, final int q) {
		throw new RuntimeException("Not implemented!");
	}

	public void glNormal3f(final float nx, final float ny, final float nz) {
		throw new RuntimeException("Not implemented!");
	}

	public void glNormal3x(final int nx, final int ny, final int nz) {
		throw new RuntimeException("Not implemented!");
	}

	public void glNormalPointer(final int type, final int stride,
			final Buffer pointer) {
		if (this.activeShader.normalHandle != -1)
			GLES20.glVertexAttribPointer(this.activeShader.normalHandle, 3,
					type, false, stride, pointer);
	}

	public void glOrthof(final float left, final float right,
			final float bottom, final float top, final float near,
			final float far) {
		Matrix.orthoM(this.currentMatrixPointer, 0, left, right, bottom, top,
				near, far);
	}

	public void glOrthox(final int left, final int right, final int bottom,
			final int top, final int near, final int far) {
		Matrix.orthoM(this.currentMatrixPointer, 0, left, right, bottom, top,
				near, far);
	}

	public void glPixelStorei(final int pname, final int param) {
		GLES20.glPixelStorei(pname, param);
	}

	public void glPointSize(final float size) {
		throw new RuntimeException("Not implemented!");
	}

	public void glPointSizex(final int size) {
		throw new RuntimeException("Not implemented!");
	}

	public void glPolygonOffset(final float factor, final float units) {
		GLES20.glPolygonOffset(factor, units);
	}

	public void glPolygonOffsetx(final int factor, final int units) {
		GLES20.glPolygonOffset(factor, units);
	}

	public void glPopMatrix() {
		setIdentity(this.currentMatrixPointer);
		if (this.currentMatrixPointer == this.textureMatrix)
			this.textureMatrixUsed = false;
	}

	public void glPushMatrix() {
	}

	public void glReadPixels(final int x, final int y, final int width,
			final int height, final int format, final int type,
			final Buffer pixels) {
		GLES20.glReadPixels(x, y, width, height, format, type, pixels);
	}

	public void glRotatef(final float angle, final float x, final float y,
			final float z) {
		Matrix.rotateM(this.currentMatrixPointer, 0, angle, x, y, z);
	}

	public void glRotatex(final int angle, final int x, final int y, final int z) {
		Matrix.rotateM(this.currentMatrixPointer, 0, angle, x, y, z);
	}

	public void glSampleCoverage(final float value, final boolean invert) {
		GLES20.glSampleCoverage(value, invert);
	}

	public void glSampleCoveragex(final int value, final boolean invert) {
		GLES20.glSampleCoverage(value, invert);
	}

	public void glScalef(final float x, final float y, final float z) {
		Matrix.scaleM(this.currentMatrixPointer, 0, x, y, z);
	}

	public void glScalex(final int x, final int y, final int z) {
		Matrix.scaleM(this.currentMatrixPointer, 0, x, y, z);
	}

	public void glScissor(final int x, final int y, final int width,
			final int height) {
		GLES20.glScissor(x, y, width, height);
	}

	public void glShadeModel(final int mode) {
		if (mode == 7424)
			this.flatShading = true;
		else
			this.flatShading = false;
	}

	public void glStencilFunc(final int func, final int ref, final int mask) {
		GLES20.glStencilFunc(func, ref, mask);
	}

	public void glStencilMask(final int mask) {
		GLES20.glStencilMask(mask);
	}

	public void glStencilOp(final int fail, final int zfail, final int zpass) {
		GLES20.glStencilOp(fail, zfail, zpass);
	}

	public void glTexCoordPointer(final int size, final int type,
			final int stride, final Buffer pointer) {
		final int index = getTextureStageIndex();
		if (this.activeShader.textureHandle[index] != -1)
			GLES20.glVertexAttribPointer(
					this.activeShader.textureHandle[index], size, type, false,
					stride, pointer);
	}

	public void glTexEnvf(final int target, final int pname, final float param) {
		glTexEnvx(target, pname, (int) param);
	}

	public void glTexEnvfv(final int target, final int pname,
			final FloatBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glTexEnvfv(final int target, final int pname,
			final float[] params, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glTexEnvx(final int target, final int pname, final int param) {
		if (target != 8960) {
			throw new RuntimeException("Not implemented: " + target);
		}
		if (pname == 8704) {
			int p = 0;
			if (param == 260)
				p = 1;
			else if (param == 7681)
				p = 2;
			else if (param == 3042) {
				p = 3;
			}

			this.textureModes[getTextureStageIndex()] = p;
		} else if (pname == 34161) {
			this.textureCombineRgb = param;
		} else if (pname == 34163) {
			this.textureRgbScale = param;
		} else {
			throw new RuntimeException("Parameter not supported: " + pname);
		}
	}

	public void glTexEnvxv(final int target, final int pname,
			final IntBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glTexEnvxv(final int target, final int pname,
			final int[] params, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glTexImage2D(final int target, final int level,
			final int internalformat, final int width, final int height,
			final int border, final int format, final int type,
			final Buffer pixels) {
		GLES20.glTexImage2D(target, level, internalformat, width, height,
				border, format, type, pixels);
	}

	public void glTexParameterf(final int target, final int pname,
			final float param) {
		GLES20.glTexParameterf(target, pname, param);
	}

	public void glTexParameterx(final int target, final int pname,
			final int param) {
		GLES20.glTexParameteri(target, pname, param);
	}

	public void glTexSubImage2D(final int target, final int level,
			final int xoffset, final int yoffset, final int width,
			final int height, final int format, final int type,
			final Buffer pixels) {
		GLES20.glTexSubImage2D(target, level, xoffset, yoffset, width, height,
				format, type, pixels);
	}

	public void glTranslatef(final float x, final float y, final float z) {
		Matrix.translateM(this.currentMatrixPointer, 0, x, y, z);
	}

	public void glTranslatex(final int x, final int y, final int z) {
		Matrix.translateM(this.currentMatrixPointer, 0, x, y, z);
	}

	public void glVertexPointer(final int size, final int type,
			final int stride, final Buffer pointer) {
		if (this.activeShader.vertexHandle != -1)
			GLES20.glVertexAttribPointer(this.activeShader.vertexHandle, size,
					type, false, stride, pointer);
	}

	public void glViewport(final int x, final int y, final int width,
			final int height) {
		GLES20.glViewport(x, y, width, height);
	}

	public void glBindBuffer(final int target, final int buffer) {
		GLES20.glBindBuffer(target, buffer);
	}

	public void glBufferData(final int target, final int size,
			final Buffer data, final int usage) {
		GLES20.glBufferData(target, size, data, usage);
	}

	public void glBufferSubData(final int target, final int offset,
			final int size, final Buffer data) {
		GLES20.glBufferSubData(target, offset, size, data);
	}

	public void glClipPlanef(final int plane, final FloatBuffer equation) {
		throw new RuntimeException("Not implemented!");
	}

	public void glClipPlanef(final int plane, final float[] equation,
			final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glClipPlanex(final int plane, final IntBuffer equation) {
		throw new RuntimeException("Not implemented!");
	}

	public void glClipPlanex(final int plane, final int[] equation,
			final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glColor4ub(final byte red, final byte green, final byte blue,
			final byte alpha) {
		throw new RuntimeException("Not implemented!");
	}

	public void glColorPointer(final int size, final int type,
			final int stride, final int offset) {
		if (this.activeShader.colorHandle != -1)
			GLES20.glVertexAttribPointer(this.activeShader.colorHandle, size,
					type, false, stride, offset);
	}

	public void glDeleteBuffers(final int n, final IntBuffer buffers) {
		GLES20.glDeleteBuffers(n, buffers);
	}

	public void glDeleteBuffers(final int n, final int[] buffers,
			final int offset) {
		GLES20.glDeleteBuffers(n, buffers, offset);
	}

	public void glDrawElements(final int mode, final int count, final int type,
			final int offset) {

		GLES20.glDrawElements(mode, count, type, offset);
	}

	public void glGenBuffers(final int n, final IntBuffer buffers) {
		GLES20.glGenBuffers(n, buffers);
	}

	public void glGenBuffers(final int n, final int[] buffers, final int offset) {
		checkError("before");
		GLES20.glGenBuffers(n, buffers, offset);
	}

	public void glGetBooleanv(final int pname, final IntBuffer params) {
		GLES20.glGetBooleanv(pname, params);
	}

	public void glGetBooleanv(final int pname, final boolean[] params,
			final int offset) {
		GLES20.glGetBooleanv(pname, params, offset);
	}

	public void glGetBufferParameteriv(final int target, final int pname,
			final IntBuffer params) {
		GLES20.glGetBufferParameteriv(target, pname, params);
	}

	public void glGetBufferParameteriv(final int target, final int pname,
			final int[] params, final int offset) {
		GLES20.glGetBufferParameteriv(target, pname, params, offset);
	}

	public void glGetClipPlanef(final int pname, final FloatBuffer eqn) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetClipPlanef(final int pname, final float[] eqn,
			final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetClipPlanex(final int pname, final IntBuffer eqn) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetClipPlanex(final int pname, final int[] eqn,
			final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetFixedv(final int pname, final IntBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetFixedv(final int pname, final int[] params,
			final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetFloatv(final int pname, final FloatBuffer params) {
		GLES20.glGetFloatv(pname, params);
	}

	public void glGetFloatv(final int pname, final float[] params,
			final int offset) {
		GLES20.glGetFloatv(pname, params, offset);
	}

	public void glGetLightfv(final int light, final int pname,
			final FloatBuffer params) {
	}

	public void glGetLightfv(final int light, final int pname,
			final float[] params, final int offset) {
	}

	public void glGetLightxv(final int light, final int pname,
			final IntBuffer params) {
	}

	public void glGetLightxv(final int light, final int pname,
			final int[] params, final int offset) {
	}

	public void glGetMaterialfv(final int face, final int pname,
			final FloatBuffer params) {
	}

	public void glGetMaterialfv(final int face, final int pname,
			final float[] params, final int offset) {
	}

	public void glGetMaterialxv(final int face, final int pname,
			final IntBuffer params) {
	}

	public void glGetMaterialxv(final int face, final int pname,
			final int[] params, final int offset) {
	}

	public void glGetPointerv(final int pname, final Buffer[] params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexEnviv(final int env, final int pname,
			final IntBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexEnviv(final int env, final int pname,
			final int[] params, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexEnvxv(final int env, final int pname,
			final IntBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexEnvxv(final int env, final int pname,
			final int[] params, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexParameterfv(final int target, final int pname,
			final FloatBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexParameterfv(final int target, final int pname,
			final float[] params, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexParameteriv(final int target, final int pname,
			final IntBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexParameteriv(final int target, final int pname,
			final int[] params, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexParameterxv(final int target, final int pname,
			final IntBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glGetTexParameterxv(final int target, final int pname,
			final int[] params, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public boolean glIsBuffer(final int buffer) {
		return GLES20.glIsBuffer(buffer);
	}

	public boolean glIsEnabled(final int cap) {
		if (hasCap(cap)) {
			return GLES20.glIsEnabled(cap);
		}
		return false;
	}

	public boolean glIsTexture(final int texture) {
		return GLES20.glIsTexture(texture);
	}

	public void glNormalPointer(final int type, final int stride,
			final int offset) {
		if (this.activeShader.normalHandle != -1)
			GLES20.glVertexAttribPointer(this.activeShader.normalHandle, 3,
					type, false, stride, offset);
	}

	public void glPointParameterf(final int pname, final float param) {
		throw new RuntimeException("Not implemented!");
	}

	public void glPointParameterfv(final int pname, final FloatBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glPointParameterfv(final int pname, final float[] params,
			final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glPointParameterx(final int pname, final int param) {
		throw new RuntimeException("Not implemented!");
	}

	public void glPointParameterxv(final int pname, final IntBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glPointParameterxv(final int pname, final int[] params,
			final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glPointSizePointerOES(final int type, final int stride,
			final Buffer pointer) {
		throw new RuntimeException("Not implemented!");
	}

	public void glTexCoordPointer(final int size, final int type,
			final int stride, final int offset) {
		final int index = getTextureStageIndex();
		if (this.activeShader.textureHandle[index] != -1)
			GLES20.glVertexAttribPointer(
					this.activeShader.textureHandle[index], size, type, false,
					stride, offset);
	}

	public void glTexEnvi(final int target, final int pname, final int param) {
		glTexEnvx(target, pname, param);
	}

	public void glTexEnviv(final int target, final int pname,
			final IntBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glTexEnviv(final int target, final int pname,
			final int[] params, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glTexParameterfv(final int target, final int pname,
			final FloatBuffer params) {
		GLES20.glTexParameterfv(target, pname, params);
	}

	public void glTexParameterfv(final int target, final int pname,
			final float[] params, final int offset) {
		GLES20.glTexParameterfv(target, pname, params, offset);
	}

	public void glTexParameteri(final int target, final int pname,
			final int param) {
		GLES20.glTexParameteri(target, pname, param);
	}

	public void glTexParameteriv(final int target, final int pname,
			final IntBuffer params) {
		GLES20.glTexParameteriv(target, pname, params);
	}

	public void glTexParameteriv(final int target, final int pname,
			final int[] params, final int offset) {
		GLES20.glTexParameteriv(target, pname, params, offset);
	}

	public void glTexParameterxv(final int target, final int pname,
			final IntBuffer params) {
		throw new RuntimeException("Not implemented!");
	}

	public void glTexParameterxv(final int target, final int pname,
			final int[] params, final int offset) {
		throw new RuntimeException("Not implemented!");
	}

	public void glVertexPointer(final int size, final int type,
			final int stride, final int offset) {
		if (this.activeShader.vertexHandle != -1)
			GLES20.glVertexAttribPointer(this.activeShader.vertexHandle, size,
					type, false, stride, offset);
	}

	private int getTextureStageIndex() {
		if (this.currentTextureStage != -1) {
			return this.currentTextureStage - 33984;
		}
		return 0;
	}

	private void clearErrors() {
		while (GLES20.glGetError() != 0)
			;
	}

	private boolean hasCap(final int cap) {
		if (cap == 3553) {
			return true;
		}
		if ((cap >= 16384) && (cap <= 16391))
			return false;
		if (cap == 2896)
			return false;
		if (cap == 2903)
			return false;
		if (cap == 3008)
			return false;
		if (cap == 2977)
			return false;
		if (cap == 2912) {
			return false;
		}
		return true;
	}

	public boolean uploadTexture(final int w, final int h, final int mode,
			final int level, final boolean bpp4, final ByteBuffer texture) {
		if (ETC1Util.isETC1Supported()) {
			final Virtualizer texVirt = TextureCache.getInstance()
					.getVirtualizer();
			final boolean useCache = texVirt != null
					&& Config.cacheCompressedTextures;
			final long s = System.currentTimeMillis();
			Logger.log("Compressing texture to ETC1...");
			final ByteBuffer tmpBuf = ByteBuffer.allocateDirect(w * h * 3);
			final int end = texture.limit();
			final byte res[] = new byte[15];
			long chkSum = 111 * end + 1781 * level;
			for (int i = 0; i < end;) {
				int cnt = 0;
				texture.get(res, 0, 3);
				texture.get();
				i += 4;
				cnt += 3;
				if (i + 4 < end) {
					texture.get(res, cnt, 3);
					texture.get();
					i += 4;
					cnt += 3;
				}
				if (i + 4 < end) {
					texture.get(res, cnt, 3);
					texture.get();
					i += 4;
					cnt += 3;
				}
				if (i + 4 < end) {
					texture.get(res, cnt, 3);
					texture.get();
					i += 4;
					cnt += 3;
				}
				if (i + 4 < end) {
					texture.get(res, cnt, 3);
					texture.get();
					i += 4;
					cnt += 3;
				}
				tmpBuf.put(res, 0, cnt);
				int mul = 1;
				byte abyte0[];
				final int k = (abyte0 = res).length;
				for (int j = 0; j < k; j++) {
					final byte re = abyte0[j];
					mul = (i & 1) != 1 ? -1 : 1;
					chkSum += i * 10 + (long) re * (long) mul;
				}

			}

			tmpBuf.rewind();
			final long s2 = System.currentTimeMillis();
			final String name = (new StringBuilder("etc1_")).append(level)
					.append("_").append(chkSum).append(".tex").toString();
			android.opengl.ETC1Util.ETC1Texture tex = null;
			if (useCache) {
				final ByteBuffer bb = texVirt.readFromCache(name);
				if (bb != null)
					tex = new android.opengl.ETC1Util.ETC1Texture(w, h, bb);
			}
			if (tex == null) {
				tex = ETC1Util.compressTexture(tmpBuf, w, h, 3, w * 3);
				if (useCache)
					texVirt.storeInCache(tex.getData(), name);
			}
			ETC1Util.loadTexture(3553, level, 0, 6407, 5121, tex);
			final long time = System.currentTimeMillis();
			Logger.log((new StringBuilder("...done in ")).append(time - s)
					.append("/").append(time - s2).append("ms").toString());
			return true;
		} else {
			return false;
		}
	}

	public void bindVertexAttributes(final String name, final int type,
			final Buffer attributes) {
		final int handle = this.activeShader.getAttributeHandle(name);
		if (handle != -1) {
			GLES20.glEnableVertexAttribArray(handle);
			GLES20.glVertexAttribPointer(handle, type, 5132, false, type * 4,
					attributes);
		}
	}

	public void bindVertexAttributes(final String name, final int type,
			final int attributesId) {
		final int handle = this.activeShader.getAttributeHandle(name);
		if (handle != -1) {
			GLES20.glBindBuffer(34962, attributesId);
			GLES20.glEnableVertexAttribArray(handle);
			GLES20.glVertexAttribPointer(handle, type, 5132, false, type * 4, 0);
		}
	}

	public void unbindVertexAttributes(final String name, final int type,
			final Buffer attributes) {
		final int handle = this.activeShader.getAttributeHandle(name);
		if (handle != -1)
			GLES20.glDisableVertexAttribArray(handle);
	}

	public void unbindVertexAttributes(final String name, final int type,
			final int attributesId) {
		final int handle = this.activeShader.getAttributeHandle(name);
		if (handle != -1) {
			GLES20.glDisableVertexAttribArray(handle);
			if (attributesId > 0)
				GLES20.glBindBuffer(34962, 0);
		}
	}

	public void setTangents(final Buffer tangents) {
		if (this.activeShader.tangentHandle != -1) {
			GLES20.glEnableVertexAttribArray(this.activeShader.tangentHandle);
			GLES20.glVertexAttribPointer(this.activeShader.tangentHandle, 4,
					5132, false, 16, tangents);
		}
	}

	public void setTangents(final int tangentsId) {
		if (this.activeShader.tangentHandle != -1) {
			GLES20.glBindBuffer(34962, tangentsId);
			GLES20.glEnableVertexAttribArray(this.activeShader.tangentHandle);
			GLES20.glVertexAttribPointer(this.activeShader.tangentHandle, 4,
					5132, false, 16, 0);
		}
	}

	public void clearTangents(final int tangentsId) {
		if (this.activeShader.tangentHandle != -1) {
			GLES20.glDisableVertexAttribArray(this.activeShader.tangentHandle);
			if (tangentsId > 0)
				GLES20.glBindBuffer(34962, 0);
		}
	}

	public void clearTangents() {
		if (this.activeShader.tangentHandle != -1)
			GLES20.glDisableVertexAttribArray(this.activeShader.tangentHandle);
	}

	public void setRenderTarget(final Texture renderTarget,
			final GLRenderer renderer, final FrameBuffer buffer) {
		final int myID = renderer.myID;

		if (renderTarget == null) {
			GLES20.glColorMask(true, true, true, true);
			if (this.fbo != -1) {
				if (Logger.isDebugEnabled()) {
					Logger.log("Unbinding buffers (" + this.fbo + ")!", 3);
				}

				GLES20.glBindFramebuffer(36160, 0);
				GLES20.glBindRenderbuffer(36161, 0);
				renderer.resetViewport(buffer);
			}
			if ((this.storedShader != null) && (this.depthMode)) {
				this.depthMode = false;
				if (this.activeShader != this.defaultShaderDepth) {
					Logger.log(
							"Exiting from depth mode without the shader being setKey...strange...",
							1);
				}

				this.activeShader = this.storedShader;
				this.userShader = this.storedUserShader;
			}
			this.storedUserShader = null;
			this.storedShader = null;
		} else {
			GLES20.glViewport(0, 0, renderTarget.getWidth(),
					renderTarget.getHeight());
			final IntBuffer buffy = this.buffy4;
			buffy.rewind();

			this.depthMode = renderTarget.isShadowMap;

			this.fbo = renderTarget.fbo;
			int renderBuffer = renderTarget.renderBuffer;

			if ((renderTarget.lastHandlerId != this.id)
					&& (renderTarget.lastHandlerId != -1)) {
				this.fbo = -1;

				if (renderTarget.getDepthBuffer() != null) {
					if ((renderTarget.getDepthBuffer().lastHandlerId != this.id)
							&& (renderTarget.getDepthBuffer().lastHandlerId != -1))
						renderTarget.getDepthBuffer().renderBuffer = -1;
				} else {
					renderBuffer = -1;
				}
			}

			renderTarget.lastHandlerId = this.id;

			boolean buffersBuffer = false;
			if (renderTarget.getDepthBuffer() != null) {
				renderTarget.getDepthBuffer().lastHandlerId = this.id;
				renderBuffer = renderTarget.getDepthBuffer().renderBuffer;
				buffersBuffer = true;
			}

			if (this.fbo == -1) {
				GLES20.glGenFramebuffers(1, buffy);
				this.fbo = buffy.get(0);

				if (renderTarget.getOpenGLID(myID) == 0) {
					renderTarget.setMarker(myID, Texture.MARKER_NOTHING);
					renderer.convertTexture(renderTarget);
					renderer.lastTextures[0] = -1;
				}

				final int tid = renderTarget.getOpenGLID(myID);
				renderer.bindTexture(0, tid);

				this.fboTexture = renderTarget.getOpenGLID(myID);

				final int channel = 36064;

				GLES20.glBindFramebuffer(36160, this.fbo);
				GLES20.glFramebufferTexture2D(36160, channel, 3553, tid, 0);
				checkError("Failed to create frame buffer (" + this.fbo + ")");

				if ((!buffersBuffer) || (renderBuffer == -1)) {
					if (Logger.isDebugEnabled()) {
						Logger.log("Creating render buffer "
								+ (buffersBuffer ? "for depth buffer!"
										: "in depth mode!"), 3);
					}
					buffy.rewind();
					GLES20.glGenRenderbuffers(1, buffy);
					renderBuffer = buffy.get(0);
					GLES20.glBindRenderbuffer(36161, renderBuffer);
					GLES20.glRenderbufferStorage(36161, 33189,
							renderTarget.getWidth(), renderTarget.getHeight());
					GLES20.glFramebufferRenderbuffer(36160, 36096, 36161,
							renderBuffer);
					if (buffersBuffer)
						renderTarget.getDepthBuffer().renderBuffer = renderBuffer;
					else {
						renderTarget.renderBuffer = renderBuffer;
					}
					if (Logger.isDebugEnabled())
						Logger.log("Render buffer created: " + renderBuffer, 3);
				} else {
					if (Logger.isDebugEnabled()) {
						Logger.log("Using depth buffer's render buffer "
								+ renderBuffer + "!", 3);
					}
					GLES20.glBindRenderbuffer(36161, renderBuffer);
					GLES20.glRenderbufferStorage(36161, 33189,
							renderTarget.getWidth(), renderTarget.getHeight());
					GLES20.glFramebufferRenderbuffer(36160, 36096, 36161,
							renderBuffer);
				}
				checkFrameBufferObject();

				renderTarget.fbo = this.fbo;
			} else {
				if (Logger.isDebugEnabled()) {
					Logger.log("Binding buffers (" + this.fbo + "/"
							+ renderBuffer + ")!", 3);
				}
				GLES20.glBindFramebuffer(36160, this.fbo);
				GLES20.glBindRenderbuffer(36161, renderBuffer);
			}

			if ((this.activeShader != this.defaultShaderDepth)
					&& (this.depthMode)) {
				this.storedShader = this.activeShader;
				this.storedUserShader = this.userShader;
				this.activeShader = this.defaultShaderDepth;
				this.userShader = null;
				this.activeShader.activate(this.id);
			}
		}
	}

	private void checkFrameBufferObject() {
		final int state = GLES20.glCheckFramebufferStatus(36160);
		switch (state) {
		case 36053:
			break;
		case 36054:
			Logger.log(
					"FrameBuffer: "
							+ this.fbo
							+ " has caused a GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT exception",
					0);
			break;
		case 36055:
			Logger.log(
					"FrameBuffer: "
							+ this.fbo
							+ ", has caused a GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT exception",
					0);
			break;
		case 36057:
			Logger.log(
					"FrameBuffer: "
							+ this.fbo
							+ ", has caused a GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS exception",
					0);
			break;
		case 36056:
		default:
			Logger.log("Unexpected reply from glCheckFramebufferStatus: "
					+ state, 0);
		}

		if (state != 36053)
			this.fbo = -1;
	}

	private void setIdentity(final float[] matrix) {
		matrix[0] = 1.0F;
		matrix[1] = 0.0F;
		matrix[2] = 0.0F;
		matrix[3] = 0.0F;
		matrix[4] = 0.0F;
		matrix[5] = 1.0F;
		matrix[6] = 0.0F;
		matrix[7] = 0.0F;
		matrix[8] = 0.0F;
		matrix[9] = 0.0F;
		matrix[10] = 1.0F;
		matrix[11] = 0.0F;
		matrix[12] = 0.0F;
		matrix[13] = 0.0F;
		matrix[14] = 0.0F;
		matrix[15] = 1.0F;
	}

	public void unloadRenderTarget(final Texture texture) {
		final IntBuffer buffy = this.buffy4;

		final int fbo = texture.fbo;
		final int renderBuffer = texture.renderBuffer;

		if (fbo == -1) {
			GLES20.glBindFramebuffer(36160, 0);

			buffy.rewind();
			if (fbo != -1) {
				buffy.put(fbo);
				buffy.rewind();
				GLES20.glDeleteFramebuffers(1, buffy);
				buffy.rewind();
				texture.fbo = -1;
			}

			if (renderBuffer != -1) {
				GLES20.glBindRenderbuffer(36161, 0);
				if (renderBuffer != -1) {
					buffy.put(renderBuffer);
					buffy.rewind();
					GLES20.glDeleteRenderbuffers(1, buffy);
					buffy.rewind();
					texture.renderBuffer = -1;
				}
			}
		}
	}

	public int getTextureStagesRaw() {
		final IntBuffer sm = ByteBuffer.allocateDirect(64)
				.order(ByteOrder.nativeOrder()).asIntBuffer();
		GLES20.glGetIntegerv(35660, sm);
		final int max = sm.get(0);
		return max;
	}
}