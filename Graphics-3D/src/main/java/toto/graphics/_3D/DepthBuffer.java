package toto.graphics._3D;

public class DepthBuffer {
	int renderBuffer = -1;

	int width = 0;

	int height = 0;

	int lastHandlerId = -1;

	public DepthBuffer(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getHeight()
	/*    */{
		/* 37 */
		return this.height;
		/*    */
	}

	public int getWidth()
	/*    */{
		/* 45 */
		return this.width;
		/*    */
	}
}
