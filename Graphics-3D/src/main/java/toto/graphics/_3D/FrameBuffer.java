/*     */
package toto.graphics._3D;

/*     */
/*     */

import java.io.Serializable;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import toto.graphics.color.RGBColor;

/*     */
/*     */

/*     */
/*     */public class FrameBuffer
/*     */implements Serializable
/*     */{
	/*     */private static final long serialVersionUID = 1L;
	/*     */public static final boolean OPAQUE_BLITTING = false;
	/*     */public static final boolean TRANSPARENT_BLITTING = true;
	/* 30 */static int versionHint = 0;
	/*     */
	/* 32 */private static long sid = 0L;
	/*     */float middleX;
	/*     */float middleY;
	/*     */int width;
	/*     */int height;
	/* 38 */transient GLRenderer glRend = null;
	/*     */
	/* 40 */transient boolean hasRenderTarget = false;
	/*     */
	/* 42 */transient Texture renderTarget = null;
	/*     */
	/* 44 */transient long displayCycle = 0L;
	/*     */
	/* 46 */private transient ArrayList<VisListManager> usedBy = new ArrayList(
			2);
	/*     */
	/* 48 */private transient ArrayList<IPostProcessor> postProcessors = new ArrayList(
			1);
	/*     */
	/* 50 */private Long id = null;
	/*     */
	/* 52 */private int openGlVersion = 0;
	/*     */
	/* 54 */boolean initialized = false;
	/*     */
	/* 56 */int virtualHeight = -1;
	/*     */
	/* 58 */int virtualWidth = -1;
	/*     */int length;
	/* 62 */private RGBColor tmpColor = new RGBColor();

	/*     */
	/*     */
	public FrameBuffer(GL10 glContext, int x, int y)
	/*     */{
		/* 78 */
		this.id = Long.valueOf(sid);
		/* 79 */
		sid += 1L;
		/*     */
		/* 81 */
		this.initialized = true;
		/*     */
		/* 83 */
		this.length = (x * y);
		/* 84 */
		this.width = x;
		/* 85 */
		this.height = y;
		/*     */
		/* 87 */
		this.middleX = (this.width / 2.0F);
		/* 88 */
		this.middleY = (this.height / 2.0F);
		/*     */
		try
		/*     */{
			/* 91 */
			versionHint = 0;
			/* 92 */
			this.openGlVersion = 0;
			/* 93 */
			this.glRend = new GLRenderer();
			/* 94 */
			this.glRend.init(glContext, this.width, this.height);
			/*     */
		} catch (Exception e) {
			/* 96 */
			Logger.log(e, 0);
			/*     */
		}
		/*     */
		/* 99 */
		if (glContext != null) {
			/* 100 */
			this.openGlVersion = 1;
			/* 101 */
			versionHint = 1;
			/*     */
		} else {
			/* 103 */
			this.openGlVersion = 2;
			/* 104 */
			versionHint = 2;
			/*     */
		}
		/*     */
	}

	/*     */
	/*     */
	public FrameBuffer(int x, int y)
	/*     */{
		/* 119 */
		this(null, x, y);
		/*     */
	}

	/*     */
	/*     */
	public Long getID()
	/*     */{
		/* 133 */
		return this.id;
		/*     */
	}

	/*     */
	/*     */
	public int getOpenGLMajorVersion()
	/*     */{
		/* 142 */
		return this.openGlVersion;
		/*     */
	}

	/*     */
	/*     */
	public boolean isInitialized()
	/*     */{
		/* 152 */
		return this.glRend.isInitialized();
		/*     */
	}

	/*     */
	/*     */
	public void setRenderTarget(int texID)
	/*     */{
		/* 166 */
		if (texID == -1)
			/* 167 */setRenderTarget(null);
		/*     */
		else
			/* 169 */setRenderTarget(TextureCache.getInstance().getTextureByID(
					texID));
		/*     */
	}

	/*     */
	/*     */
	public void setRenderTarget(int texID, int left, int top, int right,
			int bottom, boolean clearAll)
	/*     */{
		/* 199 */
		if (texID == -1)
			/* 200 */setRenderTarget(null);
		/*     */
		else
			/* 202 */
			setRenderTarget(TextureCache.getInstance().getTextureByID(texID),
					left, top, right, bottom, clearAll);
		/*     */
	}

	/*     */
	/*     */
	public void setRenderTarget(Texture tex)
	/*     */{
		/* 219 */
		setRenderTarget(tex, -1, -1, -1, -1, true);
		/*     */
	}

	/*     */
	/*     */
	public void setRenderTarget(Texture tex, int left, int top, int right,
			int bottom, boolean clearAll)
	/*     */{
		/* 248 */
		if ((tex != null)
				&& ((tex.getWidth() > getWidth()) || (tex.getHeight() > getHeight()))
				&& ((!Config.useFBO) || (!this.glRend.gl20))) {
			/* 249 */
			Logger.log(
					"Can't render into a texture larger than the current framebuffer!",
					0);
			/* 250 */
			return;
			/*     */
		}
		/*     */
		/* 253 */
		if (this.glRend != null) {
			/* 254 */
			if ((tex != null) && (tex.mipmap)) {
				/* 255 */
				tex.setMipmap(false);
				/*     */
			}
			/* 257 */
			this.glRend.setRenderTarget(tex, this, left, top, right, bottom,
					clearAll);
			/*     */
		}
		/*     */
		/* 260 */
		if (tex == null)
			/* 261 */this.hasRenderTarget = false;
		/*     */
		else {
			/* 263 */
			this.hasRenderTarget = true;
			/*     */
		}
		/* 265 */
		this.renderTarget = tex;
		/*     */
	}

	/*     */
	/*     */
	public void removeRenderTarget()
	/*     */{
		/* 274 */
		if (this.hasRenderTarget)
			/* 275 */setRenderTarget(null);
		/*     */
	}

	/*     */
	/*     */
	public void setVirtualDimensions(int width, int height)
	/*     */{
		/* 290 */
		this.virtualHeight = height;
		/* 291 */
		this.virtualWidth = width;
		/*     */
	}

	/*     */
	/*     */
	public void addPostProcessor(IPostProcessor proc)
	/*     */{
		/* 307 */
		if (proc.isInitialized())
			/* 308 */Logger.log("Post processor has already been initialized!",
					0);
		/*     */
		else
			/* 310 */this.postProcessors.add(proc);
		/*     */
	}

	/*     */
	/*     */
	public void removePostProcessor(IPostProcessor proc)
	/*     */{
		/* 323 */
		this.postProcessors.remove(proc);
		/* 324 */
		this.glRend.disposeProcessor(proc);
		/*     */
	}

	/*     */
	/*     */
	public void removeAllPostProcessors()
	/*     */{
		/* 334 */
		for (int i = 0; i < this.postProcessors.size(); i++)
			/* 335 */
			removePostProcessor((IPostProcessor) this.postProcessors.get(i));
		/*     */
	}

	/*     */
	/*     */
	public void runPostProcessors()
	/*     */{
		/* 349 */
		if (this.postProcessors == null) {
			/* 350 */
			this.postProcessors = new ArrayList(1);
			/*     */
		}
		/* 352 */
		if (this.postProcessors.size() > 0)
			/* 353 */for (int i = 0; i < this.postProcessors.size(); i++) {
				/* 354 */
				IPostProcessor pp = (IPostProcessor) this.postProcessors.get(i);
				/* 355 */
				this.glRend.postProcess(this, pp);
				/*     */
			}
		/*     */
	}

	/*     */
	/*     */
	public void setPaintListener(IPaintListener listener)
	/*     */{
		/* 368 */
		this.glRend.setPaintListener(listener);
		/*     */
	}

	/*     */
	/*     */
	public void dispose()
	/*     */{
		/* 380 */
		checkListeners();
		/* 381 */
		removeListeners();
		/* 382 */
		removeAllPostProcessors();
		/* 383 */
		if (this.glRend != null) {
			/* 384 */
			this.glRend.dispose();
			/* 385 */
			this.glRend = null;
			/*     */
		}
		/*     */
	}

	/*     */
	/*     */
	public void freeMemory()
	/*     */{
		/* 395 */
		if (this.glRend != null)
			/* 396 */this.glRend.unloadKnownTextures();
		/*     */
	}

	/*     */
	/*     */
	public int getWidth()
	/*     */{
		/* 406 */
		return this.width;
		/*     */
	}

	/*     */
	/*     */
	public int getHeight()
	/*     */{
		/* 415 */
		return this.height;
		/*     */
	}

	/*     */
	/*     */
	public float getCenterX()
	/*     */{
		/* 425 */
		return this.middleX;
		/*     */
	}

	/*     */
	/*     */
	public float getCenterY()
	/*     */{
		/* 435 */
		return this.middleY;
		/*     */
	}

	/*     */
	/*     */
	public void resize(int width, int height)
	/*     */{
		/* 449 */
		if ((height == this.height) && (width == this.width)) {
			/* 450 */
			return;
			/*     */
		}
		/*     */
		/* 453 */
		if (this.glRend != null) {
			/* 454 */
			this.glRend.revalidate(width, height);
			/* 455 */
			this.width = width;
			/* 456 */
			this.height = height;
			/* 457 */
			this.middleX = (width / 2.0F);
			/* 458 */
			this.middleY = (height / 2.0F);
			/* 459 */
			this.length = (width * height);
			/*     */
		}
		/*     */
	}

	/*     */
	/*     */
	public void clear()
	/*     */{
		/* 468 */
		clear(null);
		/*     */
	}

	/*     */
	/*     */
	public void clearZBufferOnly()
	/*     */{
		/* 478 */
		this.glRend.clearZBufferOnly();
		/*     */
	}

	/*     */
	/*     */
	public void clearColorBufferOnly(RGBColor col)
	/*     */{
		/* 488 */
		this.glRend.clearColorBufferOnly(col);
		/*     */
	}

	/*     */
	/*     */
	public void clear(RGBColor col)
	/*     */{
		/* 499 */
		this.glRend.clear(col);
		/*     */
	}

	/*     */
	/*     */
	public void clear(int rgb)
	/*     */{
		/* 510 */
		this.tmpColor.setTo(rgb >> 16 & 0xFF, rgb >> 8 & 0xFF, rgb & 0xFF,
				rgb >> 24 & 0xFF);
		/* 511 */
		clear(this.tmpColor);
		/*     */
	}

	/*     */
	/*     */
	public int[] getPixels()
	/*     */{
		/* 532 */
		return getPixels(new int[this.width * this.height]);
		/*     */
	}

	/*     */
	/*     */
	public int[] getPixels(int[] toFill)
	/*     */{
		/* 557 */
		if ((toFill == null) || (toFill.length != this.width * this.height)) {
			/* 558 */
			Logger.log("The int[]-array has to have a size of width*height!", 0);
			/* 559 */
			return null;
			/*     */
		}
		/* 561 */
		this.glRend.grabScreen(this, toFill);
		/* 562 */
		return toFill;
		/*     */
	}

	/*     */
	/*     */
	public void blit(Texture src, int srcX, int srcY, int destX, int destY,
			int width, int height, boolean transparent)
	/*     */{
		/* 602 */
		this.glRend.blitTexture(src, this, srcX, srcY, destX, destY, width,
				height, transparent);
		/*     */
	}

	/*     */
	/*     */
	public void blit(Texture src, int srcX, int srcY, int destX, int destY,
			int sourceWidth, int sourceHeight, int destWidth, int destHeight,
			int transValue, boolean additive, RGBColor addColor)
	/*     */{
		/* 650 */
		int red = 255;
		int green = 255;
		int blue = 255;
		/*     */
		/* 652 */
		if (addColor != null) {
			/* 653 */
			red = addColor.getRed();
			/* 654 */
			green = addColor.getGreen();
			/* 655 */
			blue = addColor.getBlue();
			/*     */
		}
		/* 657 */
		this.glRend.blitTexture(src, this, srcX, srcY, destX, destY,
				sourceWidth, sourceHeight, additive, addColor != null,
				destWidth, destHeight, transValue, red, green, blue);
		/*     */
	}

	/*     */
	/*     */
	public void blit(Texture src, int srcX, int srcY, int destX, int destY,
			int sourceWidth, int sourceHeight, int destWidth, int destHeight,
			int transValue, boolean additive)
	/*     */{
		/* 702 */
		blit(src, srcX, srcY, destX, destY, sourceWidth, sourceHeight,
				destWidth, destHeight, transValue, additive, null);
		/*     */
	}

	/*     */
	/*     */
	public void blit(int[] src, int srcWidth, int srcHeight, int srcX,
			int srcY, int destX, int destY, int width, int height,
			boolean transparent)
	/*     */{
		/* 750 */
		if ((srcX >= 0) && (srcX < srcWidth) && (srcY >= 0)
				&& (srcY < srcHeight) && (srcX + width <= srcWidth)
				&& (srcY + height <= srcHeight))
			/* 751 */
			this.glRend.blitIntArray(src, this, srcX, srcY, destX, destY,
					width, height, transparent, srcWidth, srcHeight);
		/*     */
		else
			/* 753 */Logger.log("Blitting region out of bounds", 0);
		/*     */
	}

	/*     */
	/*     */
	public void display()
	/*     */{
		/* 761 */
		incCounter();
		/* 762 */
		this.glRend.swapBuffers();
		/*     */
	}

	/*     */
	/*     */
	public void sync()
	/*     */{
		/* 770 */
		this.glRend.sync();
		/*     */
	}

	/*     */
	/*     */
	public void flush()
	/*     */{
		/* 778 */
		this.glRend.flush();
		/*     */
	}

	/*     */
	/*     */
	private void incCounter() {
		/* 782 */
		if (!this.hasRenderTarget)
			/* 783 */this.displayCycle += 1L;
		/*     */
	}

	/*     */
	/*     */
	final void register(VisListManager vlm)
	/*     */{
		/* 789 */
		checkListeners();
		/* 790 */
		if (!this.usedBy.contains(vlm))
			/* 791 */this.usedBy.add(vlm);
		/*     */
	}

	/*     */
	/*     */
	private void checkListeners()
	/*     */{
		/* 796 */
		if (this.usedBy == null) {
			/* 797 */
			this.usedBy = new ArrayList(2);
			/*     */
		}
		/*     */
		/* 800 */
		ArrayList toRemove = null;
		/*     */
		/* 802 */
		for (int i = 0; i < this.usedBy.size(); i++) {
			/* 803 */
			VisListManager vlm = (VisListManager) this.usedBy.get(i);
			/* 804 */
			if (vlm.isDisposed) {
				/* 805 */
				if (toRemove == null) {
					/* 806 */
					toRemove = new ArrayList();
					/*     */
				}
				/* 808 */
				toRemove.add(vlm);
				/*     */
			}
			/*     */
		}
		/*     */
		/* 812 */
		if (toRemove != null)
			/* 813 */for (int i = 0; i < toRemove.size(); i++)
				/* 814 */
				this.usedBy.remove(toRemove.get(i));
		/*     */
	}

	/*     */
	/*     */
	private void removeListeners()
	/*     */{
		/*     */
		try
		/*     */{
			/* 821 */
			for (int i = 0; i < this.usedBy.size(); i++) {
				/* 822 */
				VisListManager vlm = (VisListManager) this.usedBy.get(i);
				/* 823 */
				vlm.remove(this);
				/*     */
			}
			/*     */
		} catch (Exception e) {
			/* 826 */
			Logger.log("Couldn't unregister visibility list!", 1);
			/*     */
		}
		/*     */
	}

	/*     */
	/*     */
	protected void finalize() {
		/* 831 */
		checkListeners();
		/* 832 */
		removeListeners();
		/*     */
	}
	/*     */
}
