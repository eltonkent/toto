package toto.graphics._3D;

/**
 * Helper class to create Rage3D Objects.
 * 
 * @see T3DObject
 */
public class T3DPrimitives {
	private static final float HRT = (float) Math.sqrt(2.0D) / 2.0F;

	public static T3DObject getPlane(int quads, float scale) {
		float startx = -scale * quads / 2.0F;
		float starty = startx;
		float tx = 0.0F;
		float ty = 0.0F;
		float dtex = 1.0F / quads;
		T3DObject obj = new T3DObject(quads * quads * 2 + 8);
		for (int i = 0; i < quads; i++) {
			for (int p = 0; p < quads; p++) {
				float dtx = tx + dtex;
				float dty = ty + dtex;
				if (dtx > 1.0F) {
					dtx = 1.0F;
				}
				if (dty > 1.0F) {
					dty = 1.0F;
				}
				obj.addTriangle(startx, starty, 0.0F, tx, ty, startx, starty
						+ scale, 0.0F, tx, dty, startx + scale, starty, 0.0F,
						dtx, ty);
				obj.addTriangle(startx, starty + scale, 0.0F, tx, dty, startx
						+ scale, starty + scale, 0.0F, dtx, dty,
						startx + scale, starty, 0.0F, dtx, ty);
				startx += scale;
				tx += dtex;
			}
			starty += scale;
			startx = -scale * quads / 2.0F;
			tx = 0.0F;
			ty += dtex;
		}
		return obj;
	}

	public static T3DObject getCone(float scale) {
		return getCone(90, scale);
	}

	public static T3DObject getCone(int faces, float scale) {
		scale *= 2.0F;
		return createLatheObject(faces,
				new SimpleVector[] { SimpleVector.create(0.5F, 0.5F, 0.0F) },
				scale);
	}

	public static T3DObject getCone(int faces, float scale, float scaleHeight) {
		scale *= 2.0F;
		return createLatheObject(faces,
				new SimpleVector[] { SimpleVector.create(0.5F,
						0.5F * scaleHeight, 0.0F) }, scale, scaleHeight);
	}

	public static T3DObject getCylinder(float scale) {
		return getCylinder(90, scale);
	}

	public static T3DObject getCylinder(int faces, float scale) {
		scale *= 2.0F;
		return createLatheObject(faces,
				new SimpleVector[] { SimpleVector.create(0.5F, 0.5F, 0.0F),
						SimpleVector.create(0.5F, -0.5F, 0.0F) }, scale);
	}

	public static T3DObject getCylinder(int faces, float scale,
			float scaleHeight) {
		scale *= 2.0F;
		return createLatheObject(
				faces,
				new SimpleVector[] {
						SimpleVector.create(0.5F, 0.5F * scaleHeight, 0.0F),
						SimpleVector.create(0.5F, -0.5F * scaleHeight, 0.0F) },
				scale, scaleHeight);
	}

	public static T3DObject getPyramide(float scale) {
		scale *= 2.0F;
		return createLatheObject(4,
				new SimpleVector[] { SimpleVector.create(HRT, 0.5F, 0.0F) },
				scale);
	}

	public static T3DObject getPyramide(float scale, float scaleHeight) {
		scale *= 2.0F;
		return createLatheObject(4, new SimpleVector[] { SimpleVector.create(
				HRT, 0.5F * scaleHeight, 0.0F) }, scale, scaleHeight);
	}

	public static T3DObject getDoubleCone(float scale) {
		return getDoubleCone(90, scale);
	}

	public static T3DObject getDoubleCone(int faces, float scale) {
		scale *= 2.0F;
		return createLatheObject(faces,
				new SimpleVector[] { SimpleVector.create(0.5F, 0.0F, 0.0F) },
				scale);
	}

	public static T3DObject getCube(float scale) {
		scale *= 2.0F;
		return createLatheObject(4,
				new SimpleVector[] { SimpleVector.create(HRT, 0.5F, 0.0F),
						SimpleVector.create(HRT, -0.5F, 0.0F) }, scale);
	}

	public static T3DObject getBox(float scale, float scaleHeight) {
		scale *= 2.0F;
		return createLatheObject(
				4,
				new SimpleVector[] {
						SimpleVector.create(HRT, 0.5F * scaleHeight, 0.0F),
						SimpleVector.create(HRT, -0.5F * scaleHeight, 0.0F) },
				scale, scaleHeight);
	}

	public static T3DObject getSphere(float scale) {
		return getSphere(20, scale);
	}

	public static T3DObject getSphere(int faces, float scale) {
		return getEllipsoid(faces, scale, 1.0F);
	}

	public static T3DObject getEllipsoid(float scale, float scaleHeight) {
		return getEllipsoid(20, scale, scaleHeight);
	}

	public static T3DObject getEllipsoid(int faces, float scale,
			float scaleHeight) {
		scale *= 2.0F;
		SimpleVector[] vertexList = new SimpleVector[faces];
		float pi = 3.141593F;
		for (int i = 0; i < vertexList.length; i++) {
			float stage = (i + 1.0F) / (vertexList.length + 1.0F);
			vertexList[i] = SimpleVector.create(
					(float) Math.sin(stage * pi) * 0.5F,
					(float) Math.cos(stage * pi) * 0.5F * scaleHeight, 0.0F);
		}
		return createLatheObject(faces, vertexList, scale, scaleHeight);
	}

	private static final T3DObject createLatheObject(int numRotations,
			SimpleVector[] latheVertexList, float scale) {
		return createLatheObject(numRotations, latheVertexList, scale, 1.0F);
	}

	private static final T3DObject createLatheObject(int numRotations,
			SimpleVector[] latheVertexList, float scale, float scaleHeight) {
		int TOP = 0;
		int BOTTOM = 1;

		T3DObject obj = new T3DObject(numRotations * 2 + numRotations * 2
				* (latheVertexList.length - 1) + 1);
		SimpleVector[] vertexList = new SimpleVector[numRotations
				* latheVertexList.length + 2];

		vertexList[0] = SimpleVector.create(0.0F, 0.5F * scaleHeight, 0.0F);
		vertexList[1] = SimpleVector.create(0.0F, -0.5F * scaleHeight, 0.0F);

		int currentVertex = 2;
		for (int i = 0; i < numRotations; i++) {
			for (int p = 0; p < latheVertexList.length; p++) {
				float stage = i / numRotations;
				vertexList[(currentVertex++)] = latheVertexList[p]
						.rotate(SimpleVector.create(0.0F,
								stage * 2.0F * 3.141593F, 0.0F));
			}
		}

		for (int i = 0; i < numRotations; i++) {
			int a = (i + 0) % numRotations * latheVertexList.length + 2;
			int c = (i + 1) % numRotations * latheVertexList.length + 2;

			obj.addTriangle(scale * vertexList[0].x, scale * vertexList[0].y,
					scale * vertexList[0].z, scale * vertexList[c].x, scale
							* vertexList[c].y, scale * vertexList[c].z, scale
							* vertexList[a].x, scale * vertexList[a].y, scale
							* vertexList[a].z);

			int b = a + latheVertexList.length - 1;
			int d = c + latheVertexList.length - 1;

			obj.addTriangle(scale * vertexList[1].x, scale * vertexList[1].y,
					scale * vertexList[1].z, scale * vertexList[b].x, scale
							* vertexList[b].y, scale * vertexList[b].z, scale
							* vertexList[d].x, scale * vertexList[d].y, scale
							* vertexList[d].z);
		}

		for (int j = 0; j < latheVertexList.length - 1; j++) {
			for (int i = 0; i < numRotations; i++) {
				int a = (i + 0) % numRotations * latheVertexList.length + j + 2;
				int b = a + 1;
				int c = (i + 1) % numRotations * latheVertexList.length + j + 2;
				int d = c + 1;

				obj.addTriangle(scale * vertexList[a].x, scale
						* vertexList[a].y, scale * vertexList[a].z, scale
						* vertexList[d].x, scale * vertexList[d].y, scale
						* vertexList[d].z, scale * vertexList[b].x, scale
						* vertexList[b].y, scale * vertexList[b].z);
				obj.addTriangle(scale * vertexList[a].x, scale
						* vertexList[a].y, scale * vertexList[a].z, scale
						* vertexList[c].x, scale * vertexList[c].y, scale
						* vertexList[c].z, scale * vertexList[d].x, scale
						* vertexList[d].y, scale * vertexList[d].z);
			}
		}
		return obj;
	}
}
