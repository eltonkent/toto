package toto.graphics._3D;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */

public final class Mesh implements Serializable {
	private static final long serialVersionUID = 2L;
	public static final boolean COMPRESS = true;
	public static final boolean DONT_COMPRESS = false;
	public static final int SERIALIZE_ALL = 0;
	public static final int SERIALIZE_VERTICES_ONLY = 1;
	public static final int SERIALIZE_LOW_PRECISION = 2;
	private int serializeMethod = 0;

	IVertexController myController = null;

	boolean normalsCalculated = false;

	boolean tangentsCalculated = false;
	int anzVectors;
	int maxVectors;
	int anzCoords;
	int anzTri;
	int[][] points;
	float[] xOrg;
	float[] yOrg;
	float[] zOrg;
	float[] nxOrg;
	float[] nyOrg;
	float[] nzOrg;
	transient int[] nxiOrg;
	transient int[] nyiOrg;
	transient int[] nziOrg;
	int[] coords;
	int obbStart;
	int obbEnd;
	short[] sxOrg = null;

	short[] syOrg = null;

	short[] szOrg = null;

	short[] snxOrg = null;

	short[] snyOrg = null;

	short[] snzOrg = null;
	float[][] tangentVectors;
	List<VertexAttributes> attrList = null;
	private boolean locked;
	private static HashMap<GenericContainer, ArrayList<Integer>> verts = null;

	Mesh(int max) {
		this.maxVectors = max;
		this.anzVectors = 0;
		this.anzCoords = 0;
		this.anzTri = 0;
		this.obbStart = 0;
		this.obbEnd = 0;
		this.normalsCalculated = false;

		this.points = new int[this.maxVectors / 3 + 1][3];

		this.coords = new int[this.maxVectors];

		this.xOrg = new float[this.maxVectors];
		this.yOrg = new float[this.maxVectors];
		this.zOrg = new float[this.maxVectors];

		this.nxOrg = new float[this.maxVectors];
		this.nyOrg = new float[this.maxVectors];
		this.nzOrg = new float[this.maxVectors];
	}

	public boolean setVertexController(IVertexController controller,
			boolean modify) {
		if (this.myController != null) {
			this.myController.destroy();
		}
		if (controller.init(this, modify)) {
			this.myController = controller;
			return true;
		}
		return false;
	}

	public void applyVertexController() {
		if (this.myController != null) {
			this.myController.apply();
			this.myController.updateMesh();
		} else {
			Logger.log("No controller has been assigned to this mesh", 1);
		}
	}

	public void removeVertexController() {
		if (this.myController != null) {
			this.myController.destroy();
			this.myController = null;
		}
	}

	public void strip() {
		if (this.locked) {
			return;
		}
		this.points = null;
		this.coords = null;
	}

	public void setLocked(boolean locked)
	/*      */{
		/* 216 */
		this.locked = locked;
		/*      */
	}

	public boolean isLocked()
	/*      */{
		/* 225 */
		return this.locked;
		/*      */
	}

	void strongStrip(T3DScene rage3DScene, T3DObject self) {
		if ((rage3DScene == null) || (this.locked)) {
			return;
		}

		int end = rage3DScene.objectList.size();
		for (int i = 0; i < end; i++) {
			T3DObject obj = rage3DScene.objectList.elementAt(i);
			if ((obj != self) && (obj.objMesh == this)
					&& ((!obj.isCompiled()) || (obj.dynamic))) {
				return;
			}

		}

		this.nxOrg = null;
		this.nyOrg = null;
		this.nzOrg = null;
	}

	public void compress() {
		int size = this.anzCoords;
		int sizev = this.anzVectors + 8;

		if (this.obbEnd == 0) {
			size += 8;
		}

		float[] xT = new float[size];
		float[] yT = new float[size];
		float[] zT = new float[size];

		float[] nxT = new float[size];
		float[] nyT = new float[size];
		float[] nzT = new float[size];

		for (int i = 0; i < this.anzCoords; i++) {
			xT[i] = this.xOrg[i];
			yT[i] = this.yOrg[i];
			zT[i] = this.zOrg[i];

			nxT[i] = this.nxOrg[i];
			nyT[i] = this.nyOrg[i];
			nzT[i] = this.nzOrg[i];
		}

		this.xOrg = xT;
		this.yOrg = yT;
		this.zOrg = zT;

		this.nxOrg = nxT;
		this.nyOrg = nyT;
		this.nzOrg = nzT;

		if (sizev < this.maxVectors) {
			int pcnt = sizev / 3 + 1;

			int[][] tpoints = new int[pcnt][3];
			int[] tcoords = new int[sizev];

			for (int i = 0; i < sizev; i++) {
				tcoords[i] = this.coords[i];
			}

			for (int i = 0; i < pcnt; i++) {
				tpoints[i] = this.points[i];
			}

			this.coords = tcoords;
			this.points = tpoints;

			this.maxVectors = sizev;
		}
	}

	public Mesh cloneMesh(boolean compressed) {
		Mesh tmpMesh = new Mesh(this.maxVectors);
		tmpMesh.anzCoords = this.anzCoords;
		tmpMesh.anzVectors = this.anzVectors;
		tmpMesh.anzTri = this.anzTri;
		tmpMesh.obbStart = this.obbStart;
		tmpMesh.obbEnd = this.obbEnd;
		for (int i = 0; i < this.maxVectors; i++) {
			tmpMesh.coords[i] = this.coords[i];
			if (i < this.anzCoords) {
				tmpMesh.xOrg[i] = this.xOrg[i];
				tmpMesh.yOrg[i] = this.yOrg[i];
				tmpMesh.zOrg[i] = this.zOrg[i];
				tmpMesh.nxOrg[i] = this.nxOrg[i];
				tmpMesh.nyOrg[i] = this.nyOrg[i];
				tmpMesh.nzOrg[i] = this.nzOrg[i];
			}
		}

		int end = this.maxVectors / 3 + 1;

		for (int i = 0; i < end; i++) {
			for (int p = 0; p < 3; p++) {
				tmpMesh.points[i][p] = this.points[i][p];
			}
		}
		if (compressed) {
			tmpMesh.compress();
		}
		return tmpMesh;
	}

	public int getVertexCount()
	/*      */{
		/* 378 */
		return this.anzVectors;
		/*      */
	}

	public int getUniqueVertexCount()
	/*      */{
		/* 390 */
		return this.anzCoords;
		/*      */
	}

	public int getTriangleCount()
	/*      */{
		/* 399 */
		return this.anzTri;
		/*      */
	}

	public float[] getBoundingBox() {
		return calcBoundingBox();
	}

	public void addVertexAttributes(VertexAttributes attrs) {
		if (attrs.matches(this.anzCoords)) {
			if (this.attrList == null) {
				this.attrList = new ArrayList(1);
			}

			if (!this.attrList.contains(attrs))
				this.attrList.add(attrs);
		} else {
			Logger.log(
					"Number of elements in vertex attributes doesn't match mesh size!",
					0);
		}
	}

	void translateMesh(Matrix translationMatrix, Matrix originMatrix) {
		for (int i = 0; i < this.anzCoords; i++) {
			this.zOrg[i] += translationMatrix.mat[3][2]
					+ originMatrix.mat[3][2];
			this.xOrg[i] += translationMatrix.mat[3][0]
					+ originMatrix.mat[3][0];
			this.yOrg[i] += translationMatrix.mat[3][1]
					+ originMatrix.mat[3][1];
		}
	}

	final float getLargestCoveredDistance() {
		float largest = -1.0F;
		for (int i2 = 0; i2 < this.anzTri; i2++) {
			int p0 = this.coords[this.points[i2][0]];
			int p1 = this.coords[this.points[i2][1]];
			int p2 = this.coords[this.points[i2][2]];

			float x0 = this.xOrg[p0];
			float y0 = this.yOrg[p0];
			float z0 = this.zOrg[p0];

			float x1 = this.xOrg[p1];
			float y1 = this.yOrg[p1];
			float z1 = this.zOrg[p1];

			float x2 = this.xOrg[p2];
			float y2 = this.yOrg[p2];
			float z2 = this.zOrg[p2];

			float dx0x1 = Math.abs(x0 - x1);
			float dx1x2 = Math.abs(x1 - x2);
			float dx0x2 = Math.abs(x0 - x2);

			float dy0y1 = Math.abs(y0 - y1);
			float dy1y2 = Math.abs(y1 - y2);
			float dy0y2 = Math.abs(y0 - y2);

			float dz0z1 = Math.abs(z0 - z1);
			float dz1z2 = Math.abs(z1 - z2);
			float dz0z2 = Math.abs(z0 - z2);

			if (dx0x1 > largest) {
				largest = dx0x1;
			}
			if (dx1x2 > largest) {
				largest = dx1x2;
			}
			if (dx0x2 > largest) {
				largest = dx0x2;
			}

			if (dy0y1 > largest) {
				largest = dy0y1;
			}
			if (dy1y2 > largest) {
				largest = dy1y2;
			}
			if (dy0y2 > largest) {
				largest = dy0y2;
			}

			if (dz0z1 > largest) {
				largest = dz0z1;
			}
			if (dz1z2 > largest) {
				largest = dz1z2;
			}
			if (dz0z2 > largest) {
				largest = dz0z2;
			}
		}
		return largest;
	}

	void calcNormals() {
		long s = System.currentTimeMillis();
		calcNormalsFast();
		if (Logger.getLogLevel() >= 2) {
			Logger.log("Normal vectors calculated in "
					+ (System.currentTimeMillis() - s) + "ms!");
		}
		this.normalsCalculated = true;
	}

	final Vector[] calcNormalsMD2(Vector[] adjacentList) {
		Vector[] tmpList = (Vector[]) null;
		if (adjacentList == null) {
			tmpList = new Vector[this.anzCoords];
		}

		for (int i = 0; i < this.anzCoords; i++) {
			if (adjacentList == null) {
				tmpList[i] = new Vector();
			}

			int adjacent = 0;

			float nx = 0.0F;
			float ny = 0.0F;
			float nz = 0.0F;

			float x1 = this.xOrg[i];
			float y1 = this.yOrg[i];
			float z1 = this.zOrg[i];

			if (adjacentList == null) {
				for (int i2 = 0; i2 < this.anzTri; i2++) {
					int p0 = this.coords[this.points[i2][0]];
					int p1 = this.coords[this.points[i2][1]];
					int p2 = this.coords[this.points[i2][2]];

					float xn2 = this.xOrg[p2];
					float yn2 = this.yOrg[p2];
					float zn2 = this.zOrg[p2];

					float xn0 = this.xOrg[p0];
					float yn0 = this.yOrg[p0];
					float zn0 = this.zOrg[p0];

					float xn1 = this.xOrg[p1];
					float yn1 = this.yOrg[p1];
					float zn1 = this.zOrg[p1];

					if (((x1 == xn2) && (y1 == yn2) && (z1 == zn2))
							|| ((x1 == xn1) && (y1 == yn1) && (z1 == zn1))
							|| ((x1 == xn0) && (y1 == yn0) && (z1 == zn0))) {
						tmpList[i].addElement(IntegerC.valueOf(i2));

						adjacent++;

						float vx = xn0 - xn2;
						float vy = yn0 - yn2;
						float vz = zn0 - zn2;

						float wx = xn1 - xn2;
						float wy = yn1 - yn2;
						float wz = zn1 - zn2;

						nx += vy * wz - vz * wy;
						ny += vz * wx - vx * wz;
						nz += vx * wy - vy * wx;
					}
				}
			} else {
				int end = adjacentList[i].size();
				for (int i3 = 0; i3 < end; i3++) {
					int i2 = ((Integer) adjacentList[i].elementAt(i3))
							.intValue();

					int p0 = this.coords[this.points[i2][0]];
					int p1 = this.coords[this.points[i2][1]];
					int p2 = this.coords[this.points[i2][2]];

					float xn2 = this.xOrg[p2];
					float yn2 = this.yOrg[p2];
					float zn2 = this.zOrg[p2];

					float xn0 = this.xOrg[p0];
					float yn0 = this.yOrg[p0];
					float zn0 = this.zOrg[p0];

					float xn1 = this.xOrg[p1];
					float yn1 = this.yOrg[p1];
					float zn1 = this.zOrg[p1];

					if (((x1 == xn2) && (y1 == yn2) && (z1 == zn2))
							|| ((x1 == xn1) && (y1 == yn1) && (z1 == zn1))
							|| ((x1 == xn0) && (y1 == yn0) && (z1 == zn0))) {
						adjacent++;

						float vx = xn0 - xn2;
						float vy = yn0 - yn2;
						float vz = zn0 - zn2;

						float wx = xn1 - xn2;
						float wy = yn1 - yn2;
						float wz = zn1 - zn2;

						nx += vy * wz - vz * wy;
						ny += vz * wx - vx * wz;
						nz += vx * wy - vy * wx;
					}
				}
			}

			if (adjacent != 0) {
				float n = (float) Math.sqrt(nx * nx + ny * ny + nz * nz);

				if (n == 0.0F) {
					n = 1.0E-012F;
				}
				n = 1.0F / n;

				this.nxOrg[i] = (nx * n);
				this.nyOrg[i] = (ny * n);
				this.nzOrg[i] = (nz * n);
			}
		}

		if (adjacentList == null) {
			return tmpList;
		}
		return adjacentList;
	}

	SimpleVector calcCenter() {
		float xm = 0.0F;
		float zm = 0.0F;
		float ym = 0.0F;
		int anz = 0;

		for (int i = 0; i < this.anzTri; i++) {
			int[] pp = this.points[i];
			for (int z = 0; z < 3; z++) {
				int p = this.coords[pp[z]];
				xm += this.xOrg[p];
				ym += this.yOrg[p];
				zm += this.zOrg[p];
				anz++;
			}
		}

		if (anz != 0) {
			float fanz = anz;
			return SimpleVector.create(xm / fanz, ym / fanz, zm / fanz);
		}
		return SimpleVector.create(0.0F, 0.0F, 0.0F);
	}

	void rotateMesh(Matrix rot, float xRot, float yRot, float zRot, float scale) {
		float s00 = rot.mat[0][0];
		float s10 = rot.mat[1][0];
		float s20 = rot.mat[2][0];

		float s01 = rot.mat[0][1];
		float s11 = rot.mat[1][1];
		float s21 = rot.mat[2][1];

		float s02 = rot.mat[0][2];
		float s12 = rot.mat[1][2];
		float s22 = rot.mat[2][2];

		float bx = xRot;
		float by = yRot;
		float bz = zRot;

		for (int i = 0; i < this.anzCoords; i++) {
			float z1 = this.zOrg[i] - bz;
			float x1 = this.xOrg[i] - bx;
			float y1 = this.yOrg[i] - by;

			float x2 = x1 * s00 + y1 * s10 + z1 * s20 + bx;
			float y2 = x1 * s01 + y1 * s11 + z1 * s21 + by;
			float z2 = x1 * s02 + y1 * s12 + z1 * s22 + bz;

			this.xOrg[i] = x2;
			this.yOrg[i] = y2;
			this.zOrg[i] = z2;

			x1 = this.nxOrg[i];
			y1 = this.nyOrg[i];
			z1 = this.nzOrg[i];

			x2 = x1 * s00 + y1 * s10 + z1 * s20;
			y2 = x1 * s01 + y1 * s11 + z1 * s21;
			z2 = x1 * s02 + y1 * s12 + z1 * s22;

			this.nxOrg[i] = (x2 / scale);
			this.nyOrg[i] = (y2 / scale);
			this.nzOrg[i] = (z2 / scale);
		}
	}

	float[] calcBoundingBox() {
		float[] result = new float[6];

		float minx = 1.0E+011F;
		float maxx = -1.0E+011F;
		float miny = 1.0E+011F;
		float maxy = -1.0E+011F;
		float minz = 1.0E+011F;
		float maxz = -1.0E+011F;

		int end = this.anzCoords;
		if (this.obbStart != 0) {
			end = this.obbStart;
		}

		for (int i = 0; i < end; i++) {
			float x1 = this.xOrg[i];
			float y1 = this.yOrg[i];
			float z1 = this.zOrg[i];

			if (x1 < minx) {
				minx = x1;
			}
			if (x1 > maxx) {
				maxx = x1;
			}
			if (y1 < miny) {
				miny = y1;
			}
			if (y1 > maxy) {
				maxy = y1;
			}
			if (z1 < minz) {
				minz = z1;
			}
			if (z1 > maxz) {
				maxz = z1;
			}
		}

		result[0] = minx;
		result[1] = maxx;
		result[2] = miny;
		result[3] = maxy;
		result[4] = minz;
		result[5] = maxz;

		return result;
	}

	void calculateTangentVectors(Vectors vecs) {
		long s = System.currentTimeMillis();

		if ((this.tangentVectors == null)
				|| (this.tangentVectors.length != this.anzCoords)) {
			this.tangentVectors = new float[this.anzCoords][];
		}

		SimpleVector[] tan1 = new SimpleVector[this.anzCoords];
		SimpleVector[] tan2 = new SimpleVector[this.anzCoords];

		int end = tan1.length;

		for (int i = 0; i < end; i++) {
			tan1[i] = new SimpleVector(0.0F, 0.0F, 0.0F);
			tan2[i] = new SimpleVector(0.0F, 0.0F, 0.0F);
		}

		end = this.anzTri;

		for (int i = 0; i < end; i++) {
			int[] pp = this.points[i];

			int p0 = this.coords[pp[0]];
			int p1 = this.coords[pp[1]];
			int p2 = this.coords[pp[2]];

			float vx0 = this.xOrg[p0];
			float vy0 = this.yOrg[p0];
			float vz0 = this.zOrg[p0];

			float vx1 = this.xOrg[p1];
			float vy1 = this.yOrg[p1];
			float vz1 = this.zOrg[p1];

			float vx2 = this.xOrg[p2];
			float vy2 = this.yOrg[p2];
			float vz2 = this.zOrg[p2];

			float u0 = vecs.nuOrg[pp[0]];
			float v0 = vecs.nvOrg[pp[0]];
			float u1 = vecs.nuOrg[pp[1]];
			float v1 = vecs.nvOrg[pp[1]];
			float u2 = vecs.nuOrg[pp[2]];
			float v2 = vecs.nvOrg[pp[2]];

			float x0 = vx1 - vx0;
			float x1 = vx2 - vx0;
			float y0 = vy1 - vy0;
			float y1 = vy2 - vy0;
			float z0 = vz1 - vz0;
			float z1 = vz2 - vz0;

			float s0 = u1 - u0;
			float s1 = u2 - u0;
			float t0 = v1 - v0;
			float t1 = v2 - v0;

			if (s0 == 0.0F) {
				s0 = 1.0E-005F;
			}

			if (s1 == 0.0F) {
				s1 = 1.0E-005F;
			}

			if (t0 == 0.0F) {
				t0 = 1.0E-005F;
			}

			if (t1 == 0.0F) {
				t1 = 1.0E-005F;
			}

			float d = s0 * t1 - s1 * t0;
			if (d == 0.0F) {
				d = 1.0E-005F;
			}
			float r = 1.0F / d;

			SimpleVector sdir = new SimpleVector((t1 * x0 - t0 * x1) * r, (t1
					* y0 - t0 * y1)
					* r, (t1 * z0 - t0 * z1) * r);
			SimpleVector tdir = new SimpleVector((s0 * x1 - s1 * x0) * r, (s0
					* y1 - s1 * y0)
					* r, (s0 * z1 - s1 * z0) * r);

			tan1[p0].add(sdir);
			tan1[p1].add(sdir);
			tan1[p2].add(sdir);

			tan2[p0].add(tdir);
			tan2[p1].add(tdir);
			tan2[p2].add(tdir);
		}

		SimpleVector n = new SimpleVector();
		SimpleVector n2 = new SimpleVector();
		SimpleVector t = new SimpleVector();

		end = this.anzCoords;

		for (int coord = 0; coord < end; coord++) {
			n.set(this.nxOrg[coord], this.nyOrg[coord], this.nzOrg[coord]);
			t.set(tan1[coord]);
			n2.set(n);

			float dot = n.calcDot(t);
			n2.scalarMul(dot);
			t.sub(n2);
			t = t.normalize();

			float[] tv = this.tangentVectors[coord];
			if (tv == null) {
				tv = new float[4];
				this.tangentVectors[coord] = tv;
			}
			tv[0] = t.x;
			tv[1] = t.y;
			tv[2] = t.z;

			SimpleVector cr = n.calcCross(t);
			dot = cr.calcDot(tan2[coord]);

			tv[3] = (dot < 0.0F ? -1.0F : 1.0F);
		}

		this.tangentsCalculated = true;
		if (Logger.getLogLevel() >= 2)
			Logger.log("Tangent vectors calculated in "
					+ (System.currentTimeMillis() - s) + "ms!");
	}

	private final void calcNormalsFast() {
		if (verts == null) {
			verts = new HashMap();
		}

		for (int i = 0; i < this.anzTri; i++) {
			int[] pp = this.points[i];
			for (int p = 0; p < 3; p++) {
				int p0 = this.coords[pp[p]];

				float x = this.xOrg[p0];
				float y = this.yOrg[p0];
				float z = this.zOrg[p0];

				GenericContainer sb = new GenericContainer(3);
				sb.add(x);
				sb.add(y);
				sb.add(z);

				ArrayList obj = (ArrayList) verts.get(sb);
				if (obj == null) {
					obj = new ArrayList(3);
					verts.put(sb, obj);
				}
				obj.add(IntegerC.valueOf(i));
			}
		}

		for (int i = 0; i < this.anzCoords; i++) {
			int adjacent = 0;

			float nx = 0.0F;
			float ny = 0.0F;
			float nz = 0.0F;

			float x1 = this.xOrg[i];
			float y1 = this.yOrg[i];
			float z1 = this.zOrg[i];

			GenericContainer sb = new GenericContainer(3);
			sb.add(x1);
			sb.add(y1);
			sb.add(z1);

			List tri = (List) verts.get(sb);
			if (tri != null) {
				int end = tri.size();

				for (int p = 0; p < end; p++) {
					int i2 = ((Integer) tri.get(p)).intValue();

					int p0 = this.coords[this.points[i2][0]];
					int p1 = this.coords[this.points[i2][1]];
					int p2 = this.coords[this.points[i2][2]];

					float xn2 = this.xOrg[p2];
					float yn2 = this.yOrg[p2];
					float zn2 = this.zOrg[p2];

					float xn0 = this.xOrg[p0];
					float yn0 = this.yOrg[p0];
					float zn0 = this.zOrg[p0];

					float xn1 = this.xOrg[p1];
					float yn1 = this.yOrg[p1];
					float zn1 = this.zOrg[p1];

					adjacent++;

					float vx = xn0 - xn2;
					float vy = yn0 - yn2;
					float vz = zn0 - zn2;

					float wx = xn1 - xn2;
					float wy = yn1 - yn2;
					float wz = zn1 - zn2;

					nx += vy * wz - vz * wy;
					ny += vz * wx - vx * wz;
					nz += vx * wy - vy * wx;
				}
			}

			if (adjacent != 0) {
				float n = (float) Math.sqrt(nx * nx + ny * ny + nz * nz);

				if (n == 0.0F) {
					n = 1.0E-012F;
				}
				n = 1.0F / n;

				this.nxOrg[i] = (nx * n);
				this.nyOrg[i] = (ny * n);
				this.nzOrg[i] = (nz * n);
			}
		}
		verts.clear();
	}

	public void setSerializeMethod(int method)
	/*      */{
		/* 1014 */
		this.serializeMethod = method;
		/*      */
	}

	private void readObject(ObjectInputStream stream) throws IOException,
			ClassNotFoundException {
		stream.defaultReadObject();

		if ((this.serializeMethod & 0x2) != 0) {
			this.xOrg = shortToFloat(this.sxOrg);
			this.sxOrg = null;
			this.yOrg = shortToFloat(this.syOrg);
			this.syOrg = null;
			this.zOrg = shortToFloat(this.szOrg);
			this.szOrg = null;
			this.nxOrg = shortToFloat(this.snxOrg);
			this.snxOrg = null;
			this.nyOrg = shortToFloat(this.snyOrg);
			this.snyOrg = null;
			this.nzOrg = shortToFloat(this.snzOrg);
			this.snzOrg = null;
		}
		if ((this.serializeMethod & 0x1) != 0) {
			this.nxOrg = new float[this.xOrg.length];
			this.nyOrg = new float[this.yOrg.length];
			this.nzOrg = new float[this.zOrg.length];
			if (this.points != null)
				calcNormals();
		}
	}

	private void writeObject(ObjectOutputStream stream) throws IOException {
		float[] cxOrg = this.xOrg;
		float[] cyOrg = this.yOrg;
		float[] czOrg = this.zOrg;
		float[] cnxOrg = this.nxOrg;
		float[] cnyOrg = this.nyOrg;
		float[] cnzOrg = this.nzOrg;

		if ((this.serializeMethod & 0x1) != 0) {
			this.nxOrg = null;
			this.nyOrg = null;
			this.nzOrg = null;
		}
		if ((this.serializeMethod & 0x2) != 0) {
			this.sxOrg = floatToShort(this.xOrg);
			this.xOrg = null;
			this.syOrg = floatToShort(this.yOrg);
			this.yOrg = null;
			this.szOrg = floatToShort(this.zOrg);
			this.zOrg = null;
			this.snxOrg = floatToShort(this.nxOrg);
			this.nxOrg = null;
			this.snyOrg = floatToShort(this.nyOrg);
			this.nyOrg = null;
			this.snzOrg = floatToShort(this.nzOrg);
			this.nzOrg = null;
		}
		stream.defaultWriteObject();

		this.xOrg = cxOrg;
		this.yOrg = cyOrg;
		this.zOrg = czOrg;
		this.nxOrg = cnxOrg;
		this.nyOrg = cnyOrg;
		this.nzOrg = cnzOrg;
	}

	private short[] floatToShort(float[] data) {
		if (data == null) {
			return null;
		}
		short[] result = new short[data.length];
		for (int i = 0; i < data.length; i++)
			result[i] = ((short) (Float.floatToRawIntBits(data[i]) >> 16));
		return result;
	}

	private float[] shortToFloat(short[] data) {
		if (data == null) {
			return null;
		}
		float[] result = new float[data.length];
		for (int i = 0; i < data.length; i++)
			result[i] = Float.intBitsToFloat(data[i] << 16);
		return result;
	}
}
