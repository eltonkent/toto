/*     */
package toto.graphics._3D;

/*     */
/*     */

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import toto.graphics.color.RGBColor;

/*     */
/*     */
/*     */
/*     */

/*     */
/*     */class DeSerializer
/*     */{
	/*     */private static final int INTS = 0;
	/*     */private static final int FLOATS = 1;
	/*     */private static final int INT_INTS = 2;
	/*     */private static final int FLOAT_FLOATS = 3;
	/*     */private static final int VERSION = 6;
	/* 21 */private byte[] buffer = new byte[512];
	/*     */
	/* 23 */private int index = 0;
	/*     */
	/* 25 */private int length = 0;

	/*     */
	/*     */
	public T3DObject[] deserialize(InputStream is)
	/*     */{
		/*     */
		try
		/*     */{
			/* 32 */
			int version = readInt(is);
			/* 33 */
			if (version > 6) {
				/* 34 */
				Logger.log("Unsupported version: " + version, 0);
				/* 35 */
				return null;
				/*     */
			}
			/* 38 */
			int length = 1;
			/* 39 */
			if (version >= 2)
			/*     */{
				/* 41 */
				length = readInt(is);
				/*     */
			}
			/*     */
			/* 44 */
			T3DObject[] result = new T3DObject[length];
			/*     */
			/* 46 */
			for (int cnt = 0; cnt < length; cnt++)
			/*     */{
				/* 48 */
				boolean reduced = readBoolean(is);
				/*     */
				/* 50 */
				String oname = null;
				/* 51 */
				if (version >= 4) {
					/* 52 */
					oname = readString(is);
					/*     */
				}
				/*     */
				/* 55 */
				SimpleVector rotPiv = null;
				/* 56 */
				if (version >= 5) {
					/* 57 */
					rotPiv = new SimpleVector(readFloatArray(is));
					/*     */
				}
				/*     */
				/* 60 */
				Mesh m = readMesh(is);
				/* 61 */
				int anzTri = m.anzTri;
				/*     */
				/* 63 */
				T3DObject obj = new T3DObject(10);
				/*     */
				/* 65 */
				if (oname != null) {
					/* 66 */
					obj.setName(oname);
					/*     */
				}
				/*     */
				/* 69 */
				if (rotPiv != null) {
					/* 70 */
					obj.setRotationPivot(rotPiv);
					/* 71 */
					obj.skipPivot = true;
					/*     */
				}
				/*     */
				/* 74 */
				Mesh mesh = obj.getMesh();
				/* 75 */
				Vectors vecs = obj.objVectors;
				/*     */
				/* 77 */
				mesh.anzCoords = m.anzCoords;
				/* 78 */
				mesh.anzTri = anzTri;
				/* 79 */
				mesh.anzVectors = m.anzVectors;
				/* 80 */
				mesh.maxVectors = m.maxVectors;
				/*     */
				/* 82 */
				mesh.normalsCalculated = m.normalsCalculated;
				/* 83 */
				mesh.xOrg = m.xOrg;
				/* 84 */
				mesh.yOrg = m.yOrg;
				/* 85 */
				mesh.zOrg = m.zOrg;
				/* 86 */
				mesh.nxOrg = m.nxOrg;
				/* 87 */
				mesh.nyOrg = m.nyOrg;
				/* 88 */
				mesh.nzOrg = m.nzOrg;
				/* 89 */
				mesh.coords = m.coords;
				/* 90 */
				mesh.obbStart = m.obbStart;
				/* 91 */
				mesh.obbEnd = m.obbEnd;
				/* 92 */
				mesh.points = m.points;
				/*     */
				/* 94 */
				vecs.nuOrg = readFloatArray(is);
				/* 95 */
				vecs.nvOrg = readFloatArray(is);
				/* 96 */
				vecs.uMul = readFloatFloatArray(is);
				/* 97 */
				vecs.vMul = readFloatFloatArray(is);
				/* 98 */
				vecs.alpha = readFloatArray(is);
				/* 99 */
				vecs.maxVectors = readInt(is);
				/* 100 */
				readBoolean(is);
				/*     */
				/* 102 */
				if (!reduced) {
					/* 103 */
					readFloatArray(is);
					/* 104 */
					readFloatArray(is);
					/* 105 */
					readFloatArray(is);
					/* 106 */
					readFloatArray(is);
					/* 107 */
					readFloatArray(is);
					/* 108 */
					readFloatArray(is);
					/* 109 */
					readFloatArray(is);
					/* 110 */
					readIntArray(is);
					/*     */
				}
				/*     */
				/* 113 */
				SimpleVector center = readVector(is);
				/* 114 */
				SimpleVector pivot = readVector(is);
				/*     */
				/* 116 */
				obj.centerX = center.x;
				/* 117 */
				obj.centerY = center.y;
				/* 118 */
				obj.centerZ = center.z;
				/*     */
				/* 120 */
				obj.xRotationCenter = pivot.x;
				/* 121 */
				obj.yRotationCenter = pivot.y;
				/* 122 */
				obj.zRotationCenter = pivot.z;
				/*     */
				/* 124 */
				obj.hasBeenBuild = readBoolean(is);
				/* 125 */
				obj.texture = readIntArray(is);
				/* 126 */
				obj.multiMode = readIntIntArray(is);
				/* 127 */
				obj.multiTex = readIntIntArray(is);
				/*     */
				/* 129 */
				int tLen = readInt(is);
				/*     */
				/* 131 */
				HashMap ids = new HashMap();
				/* 132 */
				HashMap names = new HashMap();
				/*     */
				/* 134 */
				TextureCache tm = TextureCache.getInstance();
				/*     */
				/* 136 */
				HashSet<String> nam = tm.getNames();
				/* 137 */
				for (String name : nam) {
					/* 138 */
					int id = tm.getTextureID(name);
					/* 139 */
					names.put(name, IntegerC.valueOf(id));
					/*     */
				}
				/*     */
				/* 142 */
				for (int i = 0; i < tLen; i++) {
					/* 143 */
					int id = readInt(is);
					/* 144 */
					String name = readString(is);
					/* 145 */
					ids.put(IntegerC.valueOf(id), name);
					/*     */
				}
				/*     */
				/* 149 */
				int lastOrg = -12345789;
				/* 150 */
				int lastTid = -12345789;
				/*     */
				/* 152 */
				int end = obj.texture.length;
				/*     */
				/* 154 */
				for (int i = 0; i < end; i++)
				/*     */{
					/* 156 */
					int[] objtexture = obj.texture;
					/*     */
					/* 158 */
					int org = objtexture[i];
					/*     */
					/* 160 */
					if (lastOrg == org) {
						/* 161 */
						objtexture[i] = lastTid;
						/*     */
					} else {
						/* 163 */
						String key = (String) ids.get(IntegerC.valueOf(org));
						/* 164 */
						if (key != null) {
							/* 165 */
							Integer tid = (Integer) names.get(key);
							/* 166 */
							if (tid != null) {
								/* 167 */
								objtexture[i] = tid.intValue();
								/*     */
							} else {
								/* 169 */
								int id = createTexture(key);
								/* 170 */
								if (id != -1)
									/* 171 */objtexture[i] = id;
								/*     */
								else {
									/* 173 */
									objtexture[i] = tm
											.getTextureID("--dummy--");
									/*     */
								}
								/*     */
							}
							/* 176 */
							lastOrg = org;
							/* 177 */
							lastTid = objtexture[i];
							/*     */
						}
						/*     */
					}
					/*     */
				}
				/*     */
				/* 182 */
				lastOrg = -12345789;
				/* 183 */
				lastTid = -12345789;
				/*     */
				/* 185 */
				if (obj.multiTex != null)
				/*     */{
					/* 187 */
					end = obj.multiTex.length;
					/*     */
					/* 189 */
					for (int p = 0; p < end; p++)
					/*     */{
						/* 191 */
						int[] objmultiTexp = obj.multiTex[p];
						/* 192 */
						int len = objmultiTexp.length;
						/*     */
						/* 194 */
						for (int i = 0; i < len; i++)
						/*     */{
							/* 196 */
							int org = objmultiTexp[i];
							/*     */
							/* 198 */
							if (lastOrg == org) {
								/* 199 */
								objmultiTexp[i] = lastTid;
								/*     */
							} else {
								/* 201 */
								String key = (String) ids.get(IntegerC
										.valueOf(org));
								/* 202 */
								if (key != null) {
									/* 203 */
									Integer tid = (Integer) names.get(key);
									/* 204 */
									if (tid != null) {
										/* 205 */
										objmultiTexp[i] = tid.intValue();
										/*     */
									} else {
										/* 207 */
										int id = createTexture(key);
										/* 208 */
										if (id > -1)
											/* 209 */objmultiTexp[i] = id;
										/*     */
										else {
											/* 211 */
											objmultiTexp[i] = -1;
											/*     */
										}
										/*     */
									}
									/*     */
								}
								/* 215 */
								lastOrg = org;
								/* 216 */
								lastTid = objmultiTexp[i];
								/*     */
							}
							/*     */
						}
						/*     */
					}
					/*     */
				}
				/*     */
				/* 222 */
				obj.usesMultiTexturing = readBoolean(is);
				/* 223 */
				obj.maxStagesUsed = Math.min(readInt(is),
						Config.maxTextureLayers);
				/*     */
				/* 225 */
				Animation anim = readAnimation(obj, is);
				/* 226 */
				if (anim != null) {
					/* 227 */
					obj.setAnimationSequence(anim);
					/*     */
				}
				/*     */
				/* 230 */
				if (version >= 3) {
					/* 231 */
					OcTree oc = readOcTree(is);
					/* 232 */
					if (oc != null) {
						/* 233 */
						oc.postConstruct();
						/* 234 */
						obj.setOcTree(oc);
						/*     */
					}
					/*     */
				}
				/*     */
				/* 238 */
				if (version >= 6) {
					/* 239 */
					VertexAttributes[] vass = readVertexAttributes(is);
					/* 240 */
					if (vass != null) {
						/* 241 */
						for (int i = 0; i < vass.length; i++) {
							/* 242 */
							obj.getMesh().addVertexAttributes(vass[i]);
							/*     */
						}
						/*     */
					}
					/*     */
				}
				/*     */
				/* 247 */
				result[cnt] = obj;
				/*     */
			}
			/*     */
			/* 250 */
			return result;
			/*     */
		} catch (Exception e) {
			/* 252 */
			e.printStackTrace();
			/* 253 */
			Logger.log("Can't deserialize object: " + e.getMessage(), 0);
			/*     */
		} finally {
			/*     */
			try {
				/* 256 */
				is.close();
				/*     */
			}
			/*     */catch (IOException localIOException3) {
				/*     */
			}
			/*     */
		}
		/* 261 */
		return null;
		/*     */
	}

	/*     */
	/*     */
	private VertexAttributes[] readVertexAttributes(InputStream is)
			throws Exception {
		/* 265 */
		int cnt = readInt(is);
		/* 266 */
		if (cnt == 0) {
			/* 267 */
			return null;
			/*     */
		}
		/*     */
		/* 270 */
		VertexAttributes[] ret = new VertexAttributes[cnt];
		/*     */
		/* 272 */
		for (int i = 0; i < cnt; i++) {
			/* 273 */
			float[] data = readFloatArray(is);
			/* 274 */
			int type = readInt(is);
			/* 275 */
			String name = readString(is);
			/* 276 */
			boolean dynamic = readBoolean(is);
			/* 277 */
			VertexAttributes vas = new VertexAttributes(name, data, type);
			/* 278 */
			vas.setDynamic(dynamic);
			/* 279 */
			ret[i] = vas;
			/*     */
		}
		/* 281 */
		return ret;
		/*     */
	}

	/*     */
	/*     */
	private int createTexture(String key) {
		/* 285 */
		int pos = key.indexOf("__obj-Color:");
		/* 286 */
		int pos2 = key.indexOf("__3ds-Color:");
		/* 287 */
		if ((pos != 0) && (pos2 != 0)) {
			/* 288 */
			return -1;
			/*     */
		}
		/* 290 */
		String[] parts = key.substring(12).split("/");
		/* 291 */
		if (parts.length != 3) {
			/* 292 */
			return -1;
			/*     */
		}
		/*     */
		/* 295 */
		TextureCache tm = TextureCache.getInstance();
		/*     */
		/* 297 */
		if (tm.containsTexture(key)) {
			/* 298 */
			return tm.getTextureID(key);
			/*     */
		}
		/*     */
		/* 301 */
		int r = Integer.parseInt(parts[0]);
		/* 302 */
		int g = Integer.parseInt(parts[1]);
		/* 303 */
		int b = Integer.parseInt(parts[2]);
		/* 304 */
		Texture t = new Texture(8, 8, new RGBColor(r, g, b));
		/* 305 */
		tm.addTexture(key, t);
		/* 306 */
		return tm.getTextureID(key);
		/*     */
	}

	/*     */
	/*     */
	private SimpleVector readVector(InputStream is) throws Exception {
		/* 310 */
		return new SimpleVector(readFloat(is), readFloat(is), readFloat(is));
		/*     */
	}

	/*     */
	/*     */
	private String readString(InputStream is) throws Exception {
		/* 314 */
		int[] cs = readIntArray(is);
		/* 315 */
		byte[] ca = new byte[cs.length];
		/* 316 */
		for (int i = 0; i < cs.length; i++) {
			/* 317 */
			ca[i] = ((byte) cs[i]);
			/*     */
		}
		/* 319 */
		String txt = new String(ca, "UTF-8");
		/* 320 */
		if (txt.equals("*null*")) {
			/* 321 */
			return null;
			/*     */
		}
		/* 323 */
		return txt;
		/*     */
	}

	/*     */
	/*     */
	private OcTree readOcTree(InputStream is) throws Exception {
		/* 327 */
		boolean hasOc = readBoolean(is);
		/* 328 */
		if (!hasOc) {
			/* 329 */
			Logger.log("No octree found in serialized data!");
			/* 330 */
			return null;
			/*     */
		}
		/* 332 */
		Logger.log("Octree found in serialized data!");
		/*     */
		/* 334 */
		OcTree oc = new OcTree();
		/* 335 */
		oc.leafs = readInt(is);
		/* 336 */
		oc.nodes = readInt(is);
		/* 337 */
		oc.maxPoly = readInt(is);
		/* 338 */
		oc.maxDepth = readInt(is);
		/* 339 */
		oc.totalPolys = readInt(is);
		/* 340 */
		oc.useForCollision = readBoolean(is);
		/* 341 */
		oc.useForRendering = readBoolean(is);
		/* 342 */
		oc.mode = readInt(is);
		/* 343 */
		oc.tris = readIntArray(is);
		/*     */
		/* 345 */
		Map nodes = new HashMap();
		/* 346 */
		readOcTreeNodes(is, oc, nodes);
		/* 347 */
		return oc;
		/*     */
	}

	/*     */
	/*     */
	private OcTreeNode readOcTreeNodes(InputStream is, OcTree oc,
			Map<Integer, OcTreeNode> nodes) throws Exception {
		/* 351 */
		OcTreeNode node = new OcTreeNode();
		/* 352 */
		if (nodes.size() == 0) {
			/* 353 */
			oc.root = node;
			/*     */
		}
		/* 355 */
		node.id = readInt(is);
		/*     */
		/* 357 */
		nodes.put(IntegerC.valueOf(node.id), node);
		/*     */
		/* 359 */
		int parentId = readInt(is);
		/* 360 */
		node.parent = ((OcTreeNode) nodes.get(Integer.valueOf(parentId)));
		/*     */
		/* 362 */
		node.xLow = readFloat(is);
		/* 363 */
		node.yLow = readFloat(is);
		/* 364 */
		node.zLow = readFloat(is);
		/* 365 */
		node.xHigh = readFloat(is);
		/* 366 */
		node.yHigh = readFloat(is);
		/* 367 */
		node.zHigh = readFloat(is);
		/* 368 */
		node.childCnt = readInt(is);
		/* 369 */
		node.polyCnt = readInt(is);
		/* 370 */
		node.pointCnt = readInt(is);
		/* 371 */
		node.polyList = readIntArray(is);
		/* 372 */
		node.pointList = readIntArray(is);
		/*     */
		/* 374 */
		for (int i = 0; i < 8; i++) {
			/* 375 */
			node.pList[i] = readVector(is);
			/*     */
		}
		/*     */
		/* 378 */
		node.children = new OcTreeNode[node.childCnt];
		/*     */
		/* 380 */
		for (int i = 0; i < node.childCnt; i++) {
			/* 381 */
			OcTreeNode on = readOcTreeNodes(is, oc, nodes);
			/* 382 */
			node.children[i] = on;
			/*     */
		}
		/*     */
		/* 385 */
		return node;
		/*     */
	}

	/*     */
	/*     */
	private Animation readAnimation(T3DObject obj, InputStream is)
			throws Exception
	/*     */{
		/* 390 */
		Animation anim = null;
		/*     */
		/* 392 */
		int max = 0;
		/*     */
		try {
			/* 394 */
			max = readInt(is);
			/*     */
		}
		/*     */catch (Exception localException)
		/*     */{
			/*     */
		}
		/* 399 */
		if (max == 0) {
			/* 400 */
			return null;
			/*     */
		}
		/*     */
		/* 403 */
		anim = new Animation(max);
		/*     */
		/* 405 */
		anim.aktFrames = readInt(is);
		/*     */
		/* 407 */
		for (int i = 0; i < anim.aktFrames; i++) {
			/* 408 */
			anim.keyFrames[i] = readMesh(is);
			/* 409 */
			if (anim.keyFrames[i] != null) {
				/* 410 */
				anim.keyFrames[i].strip();
				/*     */
			}
			/*     */
		}
		/*     */
		/* 414 */
		anim.endFrame = readInt(is);
		/* 415 */
		anim.mode = readInt(is);
		/* 416 */
		anim.anzAnim = readInt(is);
		/* 417 */
		anim.wrapMode = readInt(is);
		/* 418 */
		anim.startFrames = readIntArray(is);
		/* 419 */
		anim.endFrames = readIntArray(is);
		/*     */
		/* 421 */
		for (int i = 0; i < anim.startFrames.length; i++) {
			/* 422 */
			anim.seqNames[i] = readString(is);
			/*     */
		}
		/* 424 */
		return anim;
		/*     */
	}

	/*     */
	/*     */
	private Mesh readMesh(InputStream is) throws Exception
	/*     */{
		/* 429 */
		int anzCoords = readInt(is);
		/* 430 */
		if (anzCoords == 0) {
			/* 431 */
			return null;
			/*     */
		}
		/*     */
		/* 434 */
		int anzTri = readInt(is);
		/* 435 */
		int anzVectors = readInt(is);
		/* 436 */
		int maxVectors = readInt(is);
		/*     */
		/* 438 */
		Mesh mesh = new Mesh(1);
		/*     */
		/* 440 */
		mesh.anzCoords = anzCoords;
		/* 441 */
		mesh.anzTri = anzTri;
		/* 442 */
		mesh.anzVectors = anzVectors;
		/* 443 */
		mesh.maxVectors = maxVectors;
		/*     */
		/* 445 */
		mesh.normalsCalculated = readBoolean(is);
		/* 446 */
		mesh.xOrg = readFloatArray(is);
		/* 447 */
		mesh.yOrg = readFloatArray(is);
		/* 448 */
		mesh.zOrg = readFloatArray(is);
		/* 449 */
		mesh.nxOrg = readFloatArray(is);
		/* 450 */
		mesh.nyOrg = readFloatArray(is);
		/* 451 */
		mesh.nzOrg = readFloatArray(is);
		/* 452 */
		mesh.coords = readIntArray(is);
		/* 453 */
		mesh.obbStart = readInt(is);
		/* 454 */
		mesh.obbEnd = readInt(is);
		/* 455 */
		mesh.points = readIntIntArray(is);
		/*     */
		/* 457 */
		return mesh;
		/*     */
	}

	/*     */
	/*     */
	private float[] readFloatArray(InputStream is) throws Exception {
		/* 461 */
		int type = readInt(is);
		/* 462 */
		if (type != 1) {
			/* 463 */
			throw new Exception("float[] array expected (" + type + ")!");
			/*     */
		}
		/*     */
		/* 466 */
		int len = readInt(is);
		/* 467 */
		if (len == -1) {
			/* 468 */
			return null;
			/*     */
		}
		/*     */
		/* 471 */
		if (len < 0) {
			/* 472 */
			float[] res = new float[-len];
			/* 473 */
			return res;
			/*     */
		}
		/*     */
		/* 476 */
		float[] res = new float[len];
		/*     */
		/* 478 */
		for (int i = 0; i < len; i++) {
			/* 479 */
			res[i] = readFloat(is);
			/*     */
		}
		/*     */
		/* 482 */
		return res;
		/*     */
	}

	/*     */
	/*     */
	private int[] readIntArray(InputStream is) throws Exception {
		/* 486 */
		int type = readInt(is);
		/* 487 */
		if (type != 0) {
			/* 488 */
			throw new Exception("int[] array expected (" + type + ")!");
			/*     */
		}
		/*     */
		/* 491 */
		int len = readInt(is);
		/* 492 */
		if (len == -1) {
			/* 493 */
			return null;
			/*     */
		}
		/*     */
		/* 496 */
		if (len < 0) {
			/* 497 */
			int[] res = new int[-len];
			/* 498 */
			return res;
			/*     */
		}
		/*     */
		/* 501 */
		int[] res = new int[len];
		/*     */
		/* 503 */
		for (int i = 0; i < len; i++) {
			/* 504 */
			res[i] = readInt(is);
			/*     */
		}
		/*     */
		/* 507 */
		return res;
		/*     */
	}

	/*     */
	/*     */
	private int[][] readIntIntArray(InputStream is) throws Exception {
		/* 511 */
		return readIntIntArray(is, 999999);
		/*     */
	}

	/*     */
	/*     */
	private int[][] readIntIntArray(InputStream is, int max) throws Exception {
		/* 515 */
		int type = readInt(is);
		/* 516 */
		if (type != 2) {
			/* 517 */
			throw new Exception("int[][] array expected (" + type + ")!");
			/*     */
		}
		/*     */
		/* 520 */
		int len = readInt(is);
		/* 521 */
		if (len == -1) {
			/* 522 */
			return null;
			/*     */
		}
		/*     */
		/* 525 */
		len = Math.min(max, len);
		/*     */
		/* 527 */
		int[][] res = new int[len][];
		/*     */
		/* 529 */
		for (int i = 0; i < len; i++) {
			/* 530 */
			int l2 = readInt(is);
			/* 531 */
			int[] res2 = new int[l2];
			/* 532 */
			for (int p = 0; p < l2; p++) {
				/* 533 */
				res2[p] = readInt(is);
				/*     */
			}
			/* 535 */
			res[i] = res2;
			/*     */
		}
		/*     */
		/* 538 */
		return res;
		/*     */
	}

	/*     */
	/*     */
	private float[][] readFloatFloatArray(InputStream is) throws Exception {
		/* 542 */
		return readFloatFloatArray(is, 999999);
		/*     */
	}

	/*     */
	/*     */
	private float[][] readFloatFloatArray(InputStream is, int max)
			throws Exception {
		/* 546 */
		int type = readInt(is);
		/* 547 */
		if (type != 3) {
			/* 548 */
			throw new Exception("float[][] array expected (" + type + ")!");
			/*     */
		}
		/*     */
		/* 551 */
		int len = readInt(is);
		/* 552 */
		if (len == -1) {
			/* 553 */
			return null;
			/*     */
		}
		/*     */
		/* 556 */
		len = Math.min(max, len);
		/*     */
		/* 558 */
		float[][] res = new float[len][];
		/*     */
		/* 560 */
		for (int i = 0; i < len; i++) {
			/* 561 */
			int l2 = readInt(is);
			/* 562 */
			float[] res2 = new float[l2];
			/* 563 */
			for (int p = 0; p < l2; p++) {
				/* 564 */
				res2[p] = readFloat(is);
				/*     */
			}
			/* 566 */
			res[i] = res2;
			/*     */
		}
		/*     */
		/* 569 */
		return res;
		/*     */
	}

	/*     */
	/*     */
	private boolean readBoolean(InputStream is) throws Exception {
		/* 573 */
		int v = read(is);
		/* 574 */
		if (v != 0) {
			/* 575 */
			return true;
			/*     */
		}
		/* 577 */
		return false;
		/*     */
	}

	/*     */
	/*     */
	private int readInt(InputStream is) throws Exception
	/*     */{
		/* 582 */
		return read(is);
		/*     */
	}

	/*     */
	/*     */
	private float readFloat(InputStream is) throws Exception {
		/* 586 */
		return Float.intBitsToFloat(read(is));
		/*     */
	}

	/*     */
	/*     */
	private int read(InputStream is) throws Exception {
		/* 590 */
		if ((this.index >= this.length) || (this.length == 0)) {
			/* 591 */
			int start = 0;
			/* 592 */
			this.length = 0;
			/* 593 */
			while ((this.length % 4 != 0) || (start == 0)) {
				/* 594 */
				this.index = 0;
				/* 595 */
				if (is.available() == 0) {
					/* 596 */
					throw new Exception("Premature end of file!");
					/*     */
				}
				/* 598 */
				this.length += is.read(this.buffer, start, this.buffer.length
						- start);
				/*     */
				/* 600 */
				start = this.length;
				/*     */
			}
			/*     */
		}
		/* 603 */
		int ret = ((this.buffer[this.index] & 0xFF) << 24)
				+ ((this.buffer[(this.index + 1)] & 0xFF) << 16)
				+ ((this.buffer[(this.index + 2)] & 0xFF) << 8)
				+ (this.buffer[(this.index + 3)] & 0xFF);
		/* 604 */
		this.index += 4;
		/* 605 */
		return ret;
		/*     */
	}
	/*     */
}
