package toto.graphics._3D;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

class CompiledInstance {
	protected static final long serialVersionUID = 1L;
	protected static final int BUFFER_SIZE = Config.vertexBufferSize;
	protected static final int[] smallBufferOne = new int[BUFFER_SIZE];
	protected static final int[] smallBufferTwo = new int[BUFFER_SIZE];
	protected static final int[][] smallBufferMT = new int[4][BUFFER_SIZE];
	protected static final int[] sbPosMT = new int[4];
	protected static final int[] smallBufferThree = new int[BUFFER_SIZE];
	protected static final int[] smallBuffer = new int[4];

	protected static final int[] stageMap = { 33984, 33985, 33986, 33987 };
	protected static final float[] ALL_ONES = { 1.0F, 1.0F, 1.0F, 1.0F };
	protected static final float[] ALL_ONES_3 = { 1.0F, 1.0F, 1.0F, 0.0F };
	protected static final float[] ZEROS_ONE = { 0.0F, 0.0F, 0.0F, 1.0F };
	protected static final float[] ALL_ZEROS = { 0.0F, 0.0F, 0.0F, 0.0F };
	protected static final float[] allOnes3 = { 1.0F, 1.0F, 1.0F, 0.0F };
	protected static final float[] lights4 = new float[4];
	protected static final float[] STATIC_DUMPY = new float[16];

	protected static int sbPosOne = 0;
	protected static int sbPosTwo = 0;
	protected static int sbPosThree = 0;

	protected int[][] smallBufferAttrs = null;
	protected int[] sbPosAttrs = null;

	protected static final int[] LIGHTS = { 16384, 16385, 16386, 16387, 16388,
			16389, 16390, 16391 };
	protected static final float COORD_SCALE = 1.0F;
	protected static Matrix mat = new Matrix();
	protected static Matrix mo = new Matrix();

	public static T3DObject lastObj = null;
	public static float chkSum = 0.0F;

	public static int lastLightCnt = 0;
	public static Buffer lastVertexBuffer = null;

	protected boolean dynamic = false;

	protected T3DObject obj = null;
	protected IntBuffer normals = null;
	protected IntBuffer vertices = null;
	protected IntBuffer colors = null;
	protected IntBuffer tangents = null;
	protected ShortBuffer indices = null;
	protected IntBuffer[] multiTextures = null;

	protected IntBuffer[] attributes = null;

	protected int normalsId = 0;
	protected int verticesId = 0;
	protected int colorsId = 0;
	protected int indicesId = 0;
	protected int tangentsId = 0;
	protected int[] multiTexturesIds = null;

	protected int[] attributesIds = null;

	protected IntList tris = new IntList();
	protected int cnt = 0;
	protected int polyIndex = 0;
	protected int endStage = 0;
	protected boolean indexed = true;
	protected boolean staticUV = true;
	protected int treeID = -1;
	protected String key = null;
	protected int tex0 = -1;
	protected int tex1 = -1;
	protected int lastTex0 = -99;
	protected int lastTex1 = -99;
	protected HashMap<GenericContainer, Integer> vertex2index = null;
	protected int rendererID = -999999999;
	protected GLRenderer lastRenderer = null;
	protected boolean filled = false;
	protected float[] dumpy = null;

	protected boolean needsCoordMapper = true;
	protected int[] coordMapper = null;
	protected int[] vcoords = null;
	protected int[] ncoords = null;

	protected boolean buffersCreated = false;
	protected boolean vboPossible = true;

	protected int useStrip = Config.glTriangleStrips ? 0 : 2;
	protected int primitiveType = 4;
	protected int indexCount;
	protected boolean firstCompile = true;

	protected boolean hasBeenRefilled = false;

	protected boolean hasBeenVirtualized = false;

	CompiledInstance(T3DObject obj, int polyIndex, int treeID) {
		this.obj = obj;
		this.polyIndex = polyIndex;
		this.treeID = treeID;
		this.dynamic = obj.dynamic;
		this.indexed = obj.indexed;
		this.staticUV = obj.staticUV;
	}

	void setKey(String k) {
		/* 129 */
		this.key = k;
		/*      */
	}

	public String getKey() {
		/* 133 */
		return this.key;
		/*      */
	}

	public int getTreeID() {
		/* 137 */
		return this.treeID;
		/*      */
	}

	public int getPolyIndex() {
		/* 141 */
		return this.polyIndex;
		/*      */
	}

	public int getStageCount() {
		/* 145 */
		return this.endStage;
		/*      */
	}

	protected void dispose(GLRenderer renderer) {
		if ((this.buffersCreated) && (this.vboPossible)
				&& (this.obj.shareWith == null) && (this.lastRenderer != null)
				&& (!this.lastRenderer.disposed)) {
			renderer.deleteBuffer(this.normalsId);
			renderer.deleteBuffer(this.verticesId);
			renderer.deleteBuffer(this.colorsId);
			renderer.deleteBuffer(this.indicesId);
			renderer.deleteBuffer(this.tangentsId);
			for (int i = 0; i < this.endStage; i++) {
				renderer.deleteBuffer(this.multiTexturesIds[i]);
			}
			if (this.attributesIds != null) {
				for (int i = 0; i < this.attributesIds.length; i++) {
					renderer.deleteBuffer(this.attributesIds[i]);
				}
			}
			Logger.log("VBO disposed for object '" + this.obj.getName() + "'");
		}

		if (this.obj.renderHook != null)
			this.obj.renderHook.onDispose();
	}

	synchronized void render(int myID, GLRenderer rendy, float[] ambient,
			float[] cols, boolean intoDepthMap, Camera cam, float[][] lights,
			int wireFrameMode) {
		GL10 gl10 = rendy.gl10;
		GL11 gl11 = rendy.gl11;

		int backType = this.primitiveType;
		boolean wireFrame = wireFrameMode != 0;

		if (wireFrame) {
			this.primitiveType = 3;
			if (wireFrameMode == 2) {
				this.primitiveType = 0;
			}
		}

		float[] lights4 = new float[4];
		T3DObject obj = this.obj;

		rendy.setShader(obj.getShaderInternal());

		int objCompiledSize = obj.compiled.size();

		if ((obj.shareWith != null) && (!obj.sharing)) {
			obj.sharing = true;
			if (objCompiledSize != obj.shareWith.compiled.size()) {
				Logger.log(
						"Number of compiled instances don't match...can't share data!",
						0);
				return;
			}

			for (int i = 0; i < objCompiledSize; i++) {
				CompiledInstance dcis = (CompiledInstance) obj.compiled.get(i);
				CompiledInstance scis = (CompiledInstance) obj.shareWith.compiled
						.get(i);

				if ((Config.useVBO) && (gl11 != null) && (scis.vboPossible)) {
					if ((scis.rendererID != myID)
							&& (scis.rendererID != -999999999)) {
						if (Logger.getLogLevel() >= 2) {
							Logger.log("OpenGL context has changed(1)...recovering for renderer "
									+ myID + "/" + this.rendererID + "!");
						}
						scis.buffersCreated = false;
					}
					scis.compileToVBO(gl11, rendy);
					scis.rendererID = myID;
				}

				dcis.copy(scis);
				dcis.filled = true;
			}
			compileToVBO(gl11, rendy);
			this.rendererID = myID;
			if (Logger.getLogLevel() >= 2) {
				Logger.log("Object '" + obj.getName()
						+ "' shares compiled data with object '"
						+ obj.shareWith.getName() + "'");
			}
		}

		if (!this.filled) {
			Logger.log(
					"render() called on an uncompiled object (" + obj.getName()
							+ ")!", 0);
			return;
		}

		T3DScene rage3DScene = obj.myRage3DScene;
		if (rage3DScene == null) {
			return;
		}
		int lightCnt = rage3DScene.lights.lightCnt;

		boolean vertexAlpha = (this.colors != null) || (this.colorsId != 0);
		boolean hasTangents = (this.tangents != null) || (this.tangentsId != 0);

		if (this.rendererID == -999999999) {
			this.rendererID = myID;
		} else if (this.rendererID != myID) {
			if (this.buffersCreated) {
				try {
					if (Logger.getLogLevel() >= 2) {
						Logger.log("OpenGL context has changed(2)...recovering for renderer "
								+ myID + "/" + this.rendererID + "!");
					}
					this.buffersCreated = false;
					if (obj.shareWith != null) {
						obj.sharing = true;
						for (int i = 0; i < objCompiledSize; i++) {
							CompiledInstance dcis = (CompiledInstance) obj.compiled
									.get(i);
							CompiledInstance scis = (CompiledInstance) obj.shareWith.compiled
									.get(i);

							if ((Config.useVBO) && (gl11 != null)
									&& (scis.vboPossible)
									&& (scis.rendererID != myID)) {
								Logger.log("Uploading data for parent object "
										+ obj.shareWith.getName() + "!");
								scis.buffersCreated = false;
								scis.compileToVBO(gl11, rendy);
								scis.rendererID = myID;
							}

							dcis.copy(scis);
						}
					}
					compileToVBO(gl11, rendy);
					if (this.dynamic)
						fill();
				} catch (Exception e) {
					Logger.log(
							"Unable to recover to use VBO...using normal vertex arrays instead!",
							1);
					e.printStackTrace();
				}
			}
			this.rendererID = myID;
		}

		this.lastRenderer = rendy;

		float[] dump = (float[]) null;
		boolean multi = objCompiledSize > 1;

		if (this.dumpy == null) {
			if (multi)
				this.dumpy = new float[16];
			else {
				this.dumpy = STATIC_DUMPY;
			}
		}

		if (multi) {
			dump = (float[]) rendy.matrixCache.get(obj);
		}
		if (dump == null) {
			mo.setTo(obj.transBuffer);

			mat.setTo(cam.getBack());
			mat.transformToGL();

			mo.translate(-cam.backBx, -cam.backBy, -cam.backBz);
			mo.matMul(mat);
			dump = mo.fillDump(this.dumpy);

			if (multi) {
				rendy.matrixCache.put(obj, dump);
			}

		}

		gl10.glMatrixMode(5888);
		gl10.glPushMatrix();
		gl10.glLoadIdentity();

		boolean resetCulling = false;

		if (obj.doCulling) {
			if (obj.reverseCulling) {
				gl10.glCullFace(1028);
				resetCulling = true;
			}
		} else
			gl10.glDisable(2884);

		if (!wireFrame) {
			if (vertexAlpha) {
				cols[3] = 1.0F;
			}

			gl10.glLightModelfv(2899, ambient, 0);
			gl10.glMaterialfv(1032, 5632, cols, 0);

			allOnes3[3] = cols[3];

			float nSum = 0.0F;
			if (objCompiledSize > 1) {
				for (int i = 0; (i < lightCnt) && (i < 8); i++) {
					float fi = i;
					float[] lightsi = lights[i];
					if (lightsi[0] != -9999.0F) {
						nSum += (fi + 0.7181F) * lightsi[0];
						nSum += (fi + 1.1F) * lightsi[1];
						nSum += (fi + 2.24F) * lightsi[2];
						nSum += (fi + 3.567F) * lightsi[3];
						nSum += (i + 9.7272F)
								* (lightsi[4] + 1.4F * lightsi[5] + 4.56F * lightsi[6]);
					}
				}
			}

			if (vertexAlpha) {
				gl10.glEnable(2903);
			}

			if ((chkSum != nSum) || (lastObj != obj)) {
				if (!rendy.gl20) {
					gl10.glMaterialfv(1032, 4608, ALL_ONES_3, 0);
					gl10.glMaterialfv(1032, 4609, allOnes3, 0);
					gl10.glMaterialfv(1032, 4610, allOnes3, 0);
				}

				if (lightCnt < lastLightCnt) {
					for (int i = 0; i < lastLightCnt; i++) {
						gl10.glDisable(LIGHTS[i]);
					}
				}

				lastLightCnt = 0;

				for (int i = 0; (i < lightCnt) && (i < 8); i++) {
					float[] lightsi = lights[i];
					int lin = LIGHTS[i];
					if (lightsi[0] == -9999.0F) {
						gl10.glDisable(lin);
					} else {
						lastLightCnt += 1;
						boolean set = (chkSum != nSum) || (lastObj != obj);

						if (set) {
							gl10.glEnable(lin);

							lights4[0] = lightsi[1];
							lights4[1] = lightsi[2];
							lights4[2] = lightsi[3];
							lights4[3] = 1.0F;
							gl10.glLightfv(lin, 4611, lights4, 0);

							lights4[0] = lightsi[4];
							lights4[1] = lightsi[5];
							lights4[2] = lightsi[6];
							lights4[3] = 0.0F;

							gl10.glLightfv(lin, 4609, lights4, 0);
							gl10.glLightfv(lin, 4608, ALL_ZEROS, 0);

							if (lightsi[0] >= 0.0F) {
								float f = lightsi[0];
								if (f == 0.0F) {
									f = 0.001F;
								}
								gl10.glLightf(lin, 4616, 4.0F / f);
							} else {
								gl10.glLightf(lin, 4616, 0.0F);
							}
						}

						if (!obj.doSpecularLighting) {
							gl10.glLightfv(lin, 4610, ALL_ZEROS, 0);
						} else {
							gl10.glMaterialf(1032, 5633, Config.specPow / 2.0F);
							if (!set) {
								lights4[0] = lightsi[4];
								lights4[1] = lightsi[5];
								lights4[2] = lightsi[6];
								lights4[3] = 0.0F;
							}
							gl10.glLightfv(lin, 4610, lights4, 0);
						}
					}
				}

				lastObj = obj;
				chkSum = nSum;
			}

			if (obj.isFlatShaded)
				gl10.glShadeModel(7424);
		} else {
			gl10.glDisable(2896);
		}

		gl10.glLoadMatrixf(dump, 0);

		IRenderHook hook = obj.renderHook;

		if (hook != null) {
			hook.setTransparency(cols != null ? cols[3] : 1.0F);
			hook.setCurrentObject3D(obj);
			hook.beforeRendering(this.polyIndex);
		}

		GLSLShader shader = rendy.initShader();
		if (shader != null) {
			shader.directMode = true;
		}
		if (hook != null) {
			hook.setCurrentShader(shader);
		}

		if ((!Config.useVBO) || (gl11 == null) || (!this.vboPossible)) {
			if (lastVertexBuffer != this.vertices) {
				lastVertexBuffer = this.vertices;
				gl10.glNormalPointer(5132, 12, this.normals);
				gl10.glVertexPointer(3, 5132, 12, this.vertices);

				gl10.glEnableClientState(32885);
				gl10.glEnableClientState(32884);
				if (!vertexAlpha) {
					gl10.glDisableClientState(32886);
				} else {
					gl10.glColorPointer(4, 5132, 16, this.colors);
					gl10.glEnableClientState(32886);
				}

				if (!hasTangents)
					rendy.clearTangents();
				else {
					rendy.setTangents(this.tangents);
				}

				if (this.attributes != null) {
					for (int i = 0; i < this.attributes.length; i++) {
						rendy.bindVertexAttributes(
								(VertexAttributes) obj.objMesh.attrList.get(i),
								this.attributes[i]);
					}
				}

				rendy.clearStageFlag();
				for (int i = 0; i < this.endStage; i++) {
					gl10.glClientActiveTexture(stageMap[i]);
					gl10.glEnableClientState(32888);
					gl10.glTexCoordPointer(2, 5132, 8, this.multiTextures[i]);
				}
			} else {
				if (!vertexAlpha)
					gl10.glDisableClientState(32886);
				else {
					gl10.glEnableClientState(32886);
				}

				if (!hasTangents)
					rendy.clearTangents();
				else {
					rendy.setTangents(this.tangents);
				}

				if (this.attributes != null) {
					for (int i = 0; i < this.attributes.length; i++) {
						rendy.bindVertexAttributes(
								(VertexAttributes) obj.objMesh.attrList.get(i),
								this.attributes[i]);
					}
				}
			}

			if ((!wireFrame)
					&& (obj.isEnvmapped)
					&& ((!Config.glForceEnvMapToSecondStage) || (this.endStage > 1))) {
				TextureCache tm = TextureCache.getInstance();
				if ((this.tex0 == -1)
						|| (obj.texture[this.polyIndex] != this.lastTex0)) {
					this.lastTex0 = obj.texture[this.polyIndex];
					this.tex0 = tm.getTextureByID(this.lastTex0).getOpenGLID(
							myID);
				}
				if ((Config.glForceEnvMapToSecondStage)
						&& ((this.tex1 == -1) || (obj.multiTex[0][this.polyIndex] != this.lastTex1))
						&& (this.endStage > 1)) {
					this.lastTex1 = obj.multiTex[0][this.polyIndex];
					this.tex1 = tm.getTextureByID(this.lastTex1).getOpenGLID(
							myID);
				}

				int tex = this.tex0;
				int s = 0;
				if (Config.glForceEnvMapToSecondStage) {
					tex = this.tex1;
					s = 1;
				}

				if (rendy.lastTextures[s] != tex) {
					rendy.bindTexture(s, tex);
				}

				gl10.glMatrixMode(5890);
				gl10.glPushMatrix();
				gl10.glLoadIdentity();
				gl10.glTranslatef(0.5F, 0.5F, 0.5F);
				gl10.glScalef(0.5F, 0.5F, 0.5F);

				gl10.glMultMatrixf(dump, 0);
				gl10.glMatrixMode(5888);
			}
			do {
				if (this.indexed)
					gl10.glDrawElements(this.primitiveType, this.indexCount,
							5123, this.indices);
				else {
					gl10.glDrawArrays(this.primitiveType, 0, this.cnt);
				}
			} while ((hook != null) && (hook.repeatRendering()));

			if (hasTangents) {
				rendy.clearTangents();
			}

			if (this.attributes != null) {
				for (int i = 0; i < this.attributes.length; i++) {
					rendy.unbindVertexAttributes(
							(VertexAttributes) obj.objMesh.attrList.get(i),
							this.attributes[i]);
				}
			}

			if (hook != null) {
				hook.afterRendering(this.polyIndex);
			}
		} else {
			compileToVBO(gl11, rendy);

			if (lastVertexBuffer != this.vertices) {
				lastVertexBuffer = this.vertices;

				gl11.glEnableClientState(32885);
				gl11.glBindBuffer(34962, this.normalsId);
				gl11.glNormalPointer(5132, 12, 0);

				gl11.glEnableClientState(32884);
				gl11.glBindBuffer(34962, this.verticesId);
				gl11.glVertexPointer(3, 5132, 12, 0);

				if (!vertexAlpha) {
					gl10.glDisableClientState(32886);
				} else {
					gl11.glBindBuffer(34962, this.colorsId);
					gl10.glEnableClientState(32886);
					gl11.glColorPointer(4, 5132, 16, 0);
				}

				if (!hasTangents)
					rendy.clearTangents();
				else {
					rendy.setTangents(this.tangentsId);
				}

				if (this.attributesIds != null) {
					for (int i = 0; i < this.attributesIds.length; i++) {
						rendy.bindVertexAttributes(
								(VertexAttributes) obj.objMesh.attrList.get(i),
								this.attributesIds[i]);
					}
				}

				rendy.clearStageFlag();
				for (int i = 0; i < this.endStage; i++) {
					gl10.glClientActiveTexture(stageMap[i]);
					gl10.glEnableClientState(32888);
					gl11.glBindBuffer(34962, this.multiTexturesIds[i]);
					gl11.glTexCoordPointer(2, 5132, 8, 0);
				}

				gl11.glBindBuffer(34962, 0);
			} else {
				if (!vertexAlpha) {
					gl10.glDisableClientState(32886);
				} else {
					gl11.glBindBuffer(34962, this.colorsId);
					gl10.glEnableClientState(32886);
					gl11.glColorPointer(4, 5132, 16, 0);

					gl11.glBindBuffer(34962, 0);
				}

				if (!hasTangents)
					rendy.clearTangents();
				else {
					rendy.setTangents(this.tangentsId);
				}

				if (this.attributesIds != null) {
					for (int i = 0; i < this.attributesIds.length; i++) {
						rendy.bindVertexAttributes(
								(VertexAttributes) obj.objMesh.attrList.get(i),
								this.attributesIds[i]);
					}
				}
			}

			if ((!wireFrame)
					&& (obj.isEnvmapped)
					&& ((!Config.glForceEnvMapToSecondStage) || (this.endStage > 1))) {
				TextureCache tm = TextureCache.getInstance();
				if (this.tex0 == -1) {
					this.tex0 = tm.getTextureByID(obj.texture[this.polyIndex])
							.getOpenGLID(myID);
				}
				if ((Config.glForceEnvMapToSecondStage) && (this.tex1 == -1)
						&& (this.endStage > 1)) {
					this.tex1 = tm.getTextureByID(
							obj.multiTex[0][this.polyIndex]).getOpenGLID(myID);
				}

				int tex = this.tex0;
				int s = 0;
				if (Config.glForceEnvMapToSecondStage) {
					tex = this.tex1;
					s = 1;
				}

				rendy.bindTexture(s, tex);

				gl10.glMatrixMode(5890);
				gl10.glPushMatrix();
				gl10.glLoadIdentity();
				gl10.glTranslatef(0.5F, 0.5F, 0.5F);
				gl10.glScalef(0.5F, 0.5F, 0.5F);

				gl10.glMultMatrixf(dump, 0);
				gl10.glMatrixMode(5888);
			}
			do {
				if (this.indexed) {
					gl11.glBindBuffer(34963, this.indicesId);
					gl11.glDrawElements(this.primitiveType, this.indexCount,
							5123, 0);
					gl11.glBindBuffer(34963, 0);
				} else {
					gl10.glDrawArrays(this.primitiveType, 0, this.cnt);
				}
			}

			while ((hook != null) && (hook.repeatRendering()));

			if (hasTangents) {
				rendy.clearTangents(this.tangentsId);
			}

			if (this.attributesIds != null) {
				for (int i = 0; i < this.attributesIds.length; i++) {
					rendy.unbindVertexAttributes(
							(VertexAttributes) obj.objMesh.attrList.get(i),
							this.attributesIds[i]);
				}
			}

			if (hook != null) {
				hook.afterRendering(this.polyIndex);
			}
		}

		if (shader != null) {
			shader.directMode = false;
		}

		if (obj.isEnvmapped) {
			gl10.glMatrixMode(5890);
			gl10.glPopMatrix();
		}

		gl10.glMatrixMode(5888);
		gl10.glPopMatrix();

		if (vertexAlpha) {
			gl10.glDisable(2903);
		}

		if (resetCulling) {
			gl10.glCullFace(1029);
		}

		if (obj.isFlatShaded) {
			gl10.glShadeModel(7425);
		}

		if (wireFrame) {
			gl10.glEnable(2896);
		}

		if (!obj.doCulling) {
			gl10.glEnable(2884);
		}

		rendy.closeShader();

		this.primitiveType = backType;
	}

	void copy(CompiledInstance cis) {
		this.colors = cis.colors;
		this.tris = cis.tris;
		this.normals = cis.normals;
		this.tangents = cis.tangents;
		this.attributes = cis.attributes;
		this.sbPosAttrs = cis.sbPosAttrs;
		this.indices = cis.indices;
		this.vertices = cis.vertices;
		this.multiTextures = cis.multiTextures;
		this.vertex2index = cis.vertex2index;
		this.polyIndex = cis.polyIndex;
		this.endStage = cis.endStage;
		this.dynamic = cis.dynamic;
		this.cnt = cis.cnt;
		this.indexed = cis.indexed;
		this.staticUV = cis.staticUV;
		this.treeID = cis.treeID;
		this.key = cis.key;
		this.indexCount = cis.indexCount;
		this.primitiveType = cis.primitiveType;

		this.needsCoordMapper = cis.needsCoordMapper;
		this.coordMapper = cis.coordMapper;
		this.vcoords = cis.vcoords;
		this.ncoords = cis.ncoords;

		this.buffersCreated = cis.buffersCreated;
		this.verticesId = cis.verticesId;
		this.normalsId = cis.normalsId;
		this.indicesId = cis.indicesId;
		this.tangentsId = cis.tangentsId;
		this.colorsId = cis.colorsId;
		this.vboPossible = cis.vboPossible;

		if (cis.multiTexturesIds != null) {
			this.multiTexturesIds = new int[cis.multiTexturesIds.length];
			int c = 0;
			for (int i : cis.multiTexturesIds) {
				this.multiTexturesIds[c] = i;
				c++;
			}
		}

		if (cis.attributesIds != null) {
			this.attributesIds = new int[cis.attributesIds.length];
			int c = 0;
			for (int i : cis.attributesIds) {
				this.attributesIds[c] = i;
				c++;
			}
		}

		if ((this.buffersCreated) && (Logger.getLogLevel() >= 2))
			Logger.log("Object '" + this.obj.getName() + "' shares VBOs ("
					+ this.verticesId + "/" + this.normalsId
					+ ") with object '" + this.obj.shareWith.getName() + "'");
	}

	void add(int triIndex) {
		this.tris.add(triIndex);
	}

	public void error(Exception e) {
		Logger.log("Error while compiling instance!", 0);
		e.printStackTrace();
	}

	public synchronized void fill() {
		synchronized (sbPosMT) {
			try {
				_fill();
			} catch (ArrayIndexOutOfBoundsException e) {
				String msg = "State: ";
				msg = msg + sbPosMT[0] + "/" + sbPosMT[1] + "/" + sbPosMT[2]
						+ "/" + sbPosMT[3] + "/" + sbPosOne + "/" + sbPosTwo
						+ "/" + sbPosThree;
				Logger.log(msg, 1);
				throw e;
			}
		}
	}

	public void _fill() {
		sbPosOne = 0;
		sbPosTwo = 0;
		sbPosThree = 0;

		if (this.obj.shareWith != null) {
			return;
		}

		if (this.useStrip == 0) {
			checkStrips();
		}

		long time = 0L;
		if (Logger.isDebugEnabled()) {
			time = System.currentTimeMillis();
		}

		this.hasBeenRefilled = true;

		long s = 0L;

		if (this.tris != null) {
			int verts = this.tris.size() * 3;
			boolean init = false;

			if (this.normals == null) {
				s = System.currentTimeMillis();
				init = true;
				this.normals = ByteBuffer.allocateDirect(verts * 3 * 4)
						.order(ByteOrder.nativeOrder()).asIntBuffer();
				this.vertices = ByteBuffer.allocateDirect(verts * 3 * 4)
						.order(ByteOrder.nativeOrder()).asIntBuffer();

				if (this.obj.hasVertexAlpha()) {
					this.colors = ByteBuffer.allocateDirect(verts * 4 * 4)
							.order(ByteOrder.nativeOrder()).asIntBuffer();
				}

				if (this.obj.objMesh.tangentVectors != null) {
					this.tangents = ByteBuffer.allocateDirect(verts * 4 * 4)
							.order(ByteOrder.nativeOrder()).asIntBuffer();
				}

				if (this.obj.objMesh.attrList != null) {
					int len = this.obj.objMesh.attrList.size();
					this.attributes = new IntBuffer[len];
					this.smallBufferAttrs = new int[len][BUFFER_SIZE];
					this.sbPosAttrs = new int[len];
					for (int i = 0; i < len; i++) {
						this.attributes[i] = ByteBuffer
								.allocateDirect(
										verts
												* ((VertexAttributes) this.obj.objMesh.attrList
														.get(i)).type * 4)
								.order(ByteOrder.nativeOrder()).asIntBuffer();
					}
				}

				if (this.indexed) {
					this.indices = ByteBuffer.allocateDirect(verts * 4)
							.order(ByteOrder.nativeOrder()).asShortBuffer();
					this.vertex2index = new HashMap();
				}

				int max = 1;
				if (this.obj.multiTex != null) {
					for (int i = 0; i < this.obj.multiTex.length; i++) {
						if (this.obj.multiTex[i][this.polyIndex] == -1) {
							break;
						}
						max++;
					}
				}
				this.endStage = max;
				if (this.endStage > Config.glStageCount) {
					this.endStage = Config.glStageCount;
				}

				this.multiTextures = new IntBuffer[this.endStage];

				for (int i = 0; i < this.endStage; i++) {
					this.multiTextures[i] = ByteBuffer
							.allocateDirect(verts * 2 * 4)
							.order(ByteOrder.nativeOrder()).asIntBuffer();
				}
			}

			Mesh objMesh = this.obj.objMesh;

			float[] x = objMesh.xOrg;
			float[] y = objMesh.yOrg;
			float[] z = objMesh.zOrg;

			float[] nx = objMesh.nxOrg;
			float[] ny = objMesh.nyOrg;
			float[] nz = objMesh.nzOrg;

			if (nx == null) {
				Logger.log(
						"Can't compile a mesh that has already been stripped!",
						0);
				return;
			}

			this.cnt = 0;
			if (this.vertex2index != null) {
				this.vertex2index.clear();
			}

			int endII = this.tris.size();
			int i = 0;

			IntList cm = null;

			if ((this.dynamic) && (init) && (this.colors == null)
					&& (this.coordMapper == null) && (objMesh.attrList == null)) {
				cm = new IntList();
			}

			if ((this.coordMapper != null) && (this.staticUV)) {
				endII = this.coordMapper.length;

				if (this.vcoords != null) {
					int ix = 0;
					int c = 0;

					int[] vcoords = this.vcoords;
					int[] ncoords = this.ncoords;

					if (this.needsCoordMapper) {
						int[] coordMapper = this.coordMapper;

						for (int ii = 0; ii < endII; ii++) {
							int tmp714_713 = coordMapper[ii];
							c = tmp714_713;
							vcoords[ix] = ((int) (x[tmp714_713] * 65536.0F));
							ncoords[(ix++)] = ((int) (nx[c] * 65536.0F));
							vcoords[ix] = ((int) (y[c] * 65536.0F));
							ncoords[(ix++)] = ((int) (ny[c] * 65536.0F));
							vcoords[ix] = ((int) (z[c] * 65536.0F));
							ncoords[(ix++)] = ((int) (nz[c] * 65536.0F));
						}
						this.cnt = endII;
					} else {
						for (c = 0; c < endII; c++) {
							vcoords[ix] = ((int) (x[c] * 65536.0F));
							ncoords[(ix++)] = ((int) (nx[c] * 65536.0F));
							vcoords[ix] = ((int) (y[c] * 65536.0F));
							ncoords[(ix++)] = ((int) (ny[c] * 65536.0F));
							vcoords[ix] = ((int) (z[c] * 65536.0F));
							ncoords[(ix++)] = ((int) (nz[c] * 65536.0F));
						}
						this.cnt = endII;
					}

					this.vertices.put(vcoords);
					this.normals.put(ncoords);
				} else {
					for (int ii = 0; ii < endII; ii++) {
						int c = this.coordMapper[ii];

						this.vertices.put((int) (x[c] * 65536.0F));
						this.vertices.put((int) (y[c] * 65536.0F));
						this.vertices.put((int) (z[c] * 65536.0F));

						this.normals.put((int) (nx[c] * 65536.0F));
						this.normals.put((int) (ny[c] * 65536.0F));
						this.normals.put((int) (nz[c] * 65536.0F));
					}
					this.cnt = endII;
				}
			} else {
				Vectors objVectors = this.obj.objVectors;
				float[] u = objVectors.nuOrg;
				float[] v = objVectors.nvOrg;
				int[][] points = objMesh.points;

				for (int iu = 0; iu < 4; iu++) {
					sbPosMT[iu] = 0;
				}

				if (this.sbPosAttrs != null) {
					for (int iu = 0; iu < this.sbPosAttrs.length; iu++) {
						this.sbPosAttrs[iu] = 0;
					}
				}

				float[][] tangentVectors = this.obj.objMesh.tangentVectors;

				for (int ii = 0; ii < endII; ii++) {
					i = this.tris.get(ii);

					int start = 0;
					if ((ii > 0) && (this.useStrip == 1)) {
						start = 2;
					}

					GenericContainer key = null;

					for (int p = start; p < 3; p++) {
						int pind = points[i][p];
						int coord = objMesh.coords[pind];

						if (!this.indexed) {
							smallBufferOne[(sbPosOne++)] = ((int) (x[coord] * 65536.0F));
							smallBufferOne[(sbPosOne++)] = ((int) (y[coord] * 65536.0F));
							smallBufferOne[(sbPosOne++)] = ((int) (z[coord] * 65536.0F));

							boolean end = (ii == endII - 1) && (p == 2);
							if ((sbPosOne == BUFFER_SIZE) || (end)) {
								this.vertices.put(smallBufferOne, 0, sbPosOne);
								sbPosOne = 0;
							}

							smallBufferTwo[(sbPosTwo++)] = ((int) (nx[coord] * 65536.0F));
							smallBufferTwo[(sbPosTwo++)] = ((int) (ny[coord] * 65536.0F));
							smallBufferTwo[(sbPosTwo++)] = ((int) (nz[coord] * 65536.0F));

							if ((sbPosTwo == BUFFER_SIZE) || (end)) {
								this.normals.put(smallBufferTwo, 0, sbPosTwo);
								sbPosTwo = 0;
							}

							if (cm != null) {
								cm.add(coord);
							}

							fillAttributes(init, objVectors, tangentVectors,
									pind, coord, end);
						} else {
							if (key == null)
								key = new GenericContainer(11);
							else {
								key.clear();
							}
							key.add(x[coord]);
							key.add(y[coord]);
							key.add(z[coord]);

							key.add(nx[coord]);
							key.add(ny[coord]);
							key.add(nz[coord]);

							for (int k = 0; k < this.endStage; k++) {
								if (k == 0) {
									u = objVectors.nuOrg;
									v = objVectors.nvOrg;
									key.add(u[pind]);
									key.add(v[pind]);
								} else if (this.obj.maxStagesUsed > 1) {
									u = objVectors.uMul[(k - 1)];
									v = objVectors.vMul[(k - 1)];
									key.add(u[pind]);
									key.add(v[pind]);
								}

							}

							if (this.colors != null) {
								key.add(objVectors.alpha[pind]);
							}

							boolean end = (ii == endII - 1) && (p == 2);

							Integer pos = (Integer) this.vertex2index.get(key);
							if (pos == null) {
								smallBufferOne[(sbPosOne++)] = ((int) (x[coord] * 65536.0F));
								smallBufferOne[(sbPosOne++)] = ((int) (y[coord] * 65536.0F));
								smallBufferOne[(sbPosOne++)] = ((int) (z[coord] * 65536.0F));

								if ((sbPosOne == BUFFER_SIZE) || (end)) {
									this.vertices.put(smallBufferOne, 0,
											sbPosOne);
									sbPosOne = 0;
								}

								smallBufferTwo[(sbPosTwo++)] = ((int) (nx[coord] * 65536.0F));
								smallBufferTwo[(sbPosTwo++)] = ((int) (ny[coord] * 65536.0F));
								smallBufferTwo[(sbPosTwo++)] = ((int) (nz[coord] * 65536.0F));

								if ((sbPosTwo == BUFFER_SIZE) || (end)) {
									this.normals.put(smallBufferTwo, 0,
											sbPosTwo);
									sbPosTwo = 0;
								}
								if (cm != null) {
									cm.add(coord);
								}

								fillAttributes(init, objVectors,
										tangentVectors, pind, coord, end);

								int pi = (this.vertices.position() + sbPosOne - 3) / 3;
								this.vertex2index
										.put(key, IntegerC.valueOf(pi));
								this.indices.put((short) pi);
								key = null;
							} else {
								this.indices.put((short) pos.intValue());
								if (end) {
									if (sbPosOne != 0) {
										this.vertices.put(smallBufferOne, 0,
												sbPosOne);
										sbPosOne = 0;
									}
									if (sbPosTwo != 0) {
										this.normals.put(smallBufferTwo, 0,
												sbPosTwo);
										sbPosTwo = 0;
									}
									if (sbPosThree != 0) {
										this.tangents.put(smallBufferThree, 0,
												sbPosThree);
										sbPosThree = 0;
									}
									if (this.sbPosAttrs != null) {
										for (int ui = 0; ui < this.sbPosAttrs.length; ui++) {
											if (this.sbPosAttrs[ui] != 0) {
												this.attributes[ui]
														.put(this.smallBufferAttrs[ui],
																0,
																this.sbPosAttrs[ui]);
												this.sbPosAttrs[ui] = 0;
											}
										}
									}
									for (int ui = 0; ui < 4; ui++) {
										if (sbPosMT[ui] != 0) {
											this.multiTextures[ui].put(
													smallBufferMT[ui], 0,
													sbPosMT[ui]);
											sbPosMT[ui] = 0;
										}
									}
								}
							}
						}
					}
					if ((ii == 0) || (this.useStrip == 2))
						this.cnt += 3;
					else {
						this.cnt += 1;
					}
				}

				if (this.indices != null) {
					this.indexCount = this.indices.position();
					if (this.indexCount != this.indices.limit()) {
						this.indices = ((ShortBuffer) this.indices.flip());
					}
				}

				if (cm != null) {
					int cs = cm.size();
					if (Logger.getLogLevel() >= 2) {
						Logger.log("Remapping " + cs + " vertex indices!");
					}
					this.coordMapper = new int[cs];

					if ((this.dynamic)
							&& ((this.obj.anim == null) || (this.obj.anim.cacheIndices))) {
						if (Logger.getLogLevel() >= 2) {
							Logger.log("Creating vertex cache (" + cs * 24
									+ " bytes)!");
						}
						this.vcoords = new int[cs * 3];
						this.ncoords = new int[cs * 3];
					}
					int cnt = 0;
					this.needsCoordMapper = false;
					int ei = cm.size();
					for (int ie = 0; ie < ei; ie++) {
						this.coordMapper[cnt] = cm.get(ie);
						if (this.coordMapper[cnt] != cnt) {
							this.needsCoordMapper = true;
						}
						cnt++;
					}
					if (Logger.getLogLevel() >= 2) {
						if (this.needsCoordMapper)
							Logger.log("Vertex indices will be mapped!");
						else {
							Logger.log("Vertex indices will be accessed directly!");
						}
					}
					cm = null;
				}
			}

			int length = this.vertices.position();
			int tLength = this.multiTextures[0].position();

			int cLength = 0;
			if (this.colors != null) {
				cLength = this.colors.position();
				this.colors.rewind();
			}

			int tanLength = 0;
			if (this.tangents != null) {
				tanLength = this.tangents.position();
				this.tangents.rewind();
			}

			this.vertices.rewind();
			this.normals.rewind();

			for (int k = 0; k < this.endStage; k++) {
				this.multiTextures[k].rewind();
			}

			if (this.attributes != null) {
				for (int k = 0; k < this.attributes.length; k++) {
					this.sbPosAttrs[k] = this.attributes[k].position();
					this.attributes[k].rewind();
				}
			}

			if (this.indices != null) {
				this.indices.rewind();
			}

			if (init) {
				if (!this.dynamic) {
					this.tris = null;
					this.vertex2index = null;
					if (this.indexed) {
						this.normals = ((IntBuffer) flip(this.normals, length));
						this.vertices = ((IntBuffer) flip(this.vertices, length));
						if (this.colors != null) {
							this.colors = ((IntBuffer) flip(this.colors,
									cLength));
						}

						if (this.tangents != null) {
							this.tangents = ((IntBuffer) flip(this.tangents,
									tanLength));
						}
						for (int it = 0; it < this.endStage; it++) {
							this.multiTextures[it] = ((IntBuffer) flip(
									this.multiTextures[it], tLength));
						}
						if (this.attributes != null) {
							for (int k = 0; k < this.attributes.length; k++) {
								this.attributes[k] = ((IntBuffer) flip(
										this.attributes[k], this.sbPosAttrs[k]));
							}
						}
					}
				}
				if (Logger.getLogLevel() >= 2) {
					Logger.log("Subobject of object " + this.obj.getID() + "/"
							+ this.obj.getName() + " compiled to "
							+ (this.obj.indexed ? "indexed" : "flat")
							+ " fixed point data using " + this.cnt + "/"
							+ this.vertices.limit() / 3 + " vertices "
							+ (this.useStrip == 1 ? "in a strip " : "") + "in "
							+ (System.currentTimeMillis() - s) + "ms!");
				}
			}
		}

		if (Logger.isDebugEnabled()) {
			Logger.log(
					"Processing and uploading vertices of subobject of object "
							+ this.obj.getID() + "/" + this.obj.getName()
							+ " took " + (System.currentTimeMillis() - time)
							+ "ms", 3);
		}

		this.filled = true;
	}

	protected void fillAttributes(boolean init, Vectors objVectors,
			float[][] tangentVectors, int pind, int coord, boolean end) {
		if (this.colors != null) {
			smallBuffer[0] = 65536;
			smallBuffer[1] = 65536;
			smallBuffer[2] = 65536;
			smallBuffer[3] = ((int) (objVectors.alpha[pind] * 65536.0F));

			this.colors.put(smallBuffer);
		}

		if ((init) && (this.tangents != null)) {
			float[] tangentVectorsCoord = tangentVectors[coord];

			smallBufferThree[(sbPosThree++)] = ((int) (tangentVectorsCoord[0] * 65536.0F));
			smallBufferThree[(sbPosThree++)] = ((int) (tangentVectorsCoord[1] * 65536.0F));
			smallBufferThree[(sbPosThree++)] = ((int) (tangentVectorsCoord[2] * 65536.0F));
			smallBufferThree[(sbPosThree++)] = ((int) (tangentVectorsCoord[3] * 65536.0F));

			if ((sbPosThree == BUFFER_SIZE) || (end)) {
				this.tangents.put(smallBufferThree, 0, sbPosThree);
				sbPosThree = 0;
			}
		}

		if (this.obj.objMesh.attrList != null) {
			List attrs = this.obj.objMesh.attrList;
			int endy = attrs.size();

			for (int k = 0; k < endy; k++) {
				VertexAttributes vas = (VertexAttributes) attrs.get(k);
				int posi = coord * vas.type;

				float[] vasdata = vas.data;
				int[] smallBufferAttrsk = this.smallBufferAttrs[k];

				if (vas.type == 1) {
					int tmp294_292 = k;
					int[] tmp294_289 = this.sbPosAttrs;
					int tmp296_295 = tmp294_289[tmp294_292];
					tmp294_289[tmp294_292] = (tmp296_295 + 1);
					smallBufferAttrsk[tmp296_295] = ((int) (vasdata[posi] * 65536.0F));
				} else if (vas.type == 2) {
					int tmp331_329 = k;
					int[] tmp331_326 = this.sbPosAttrs;
					int tmp333_332 = tmp331_326[tmp331_329];
					tmp331_326[tmp331_329] = (tmp333_332 + 1);
					smallBufferAttrsk[tmp333_332] = ((int) (vasdata[(posi++)] * 65536.0F));
					int tmp359_357 = k;
					int[] tmp359_354 = this.sbPosAttrs;
					int tmp361_360 = tmp359_354[tmp359_357];
					tmp359_354[tmp359_357] = (tmp361_360 + 1);
					smallBufferAttrsk[tmp361_360] = ((int) (vasdata[posi] * 65536.0F));
				} else if (vas.type == 3) {
					int tmp396_394 = k;
					int[] tmp396_391 = this.sbPosAttrs;
					int tmp398_397 = tmp396_391[tmp396_394];
					tmp396_391[tmp396_394] = (tmp398_397 + 1);
					smallBufferAttrsk[tmp398_397] = ((int) (vasdata[(posi++)] * 65536.0F));
					int tmp424_422 = k;
					int[] tmp424_419 = this.sbPosAttrs;
					int tmp426_425 = tmp424_419[tmp424_422];
					tmp424_419[tmp424_422] = (tmp426_425 + 1);
					smallBufferAttrsk[tmp426_425] = ((int) (vasdata[(posi++)] * 65536.0F));
					int tmp452_450 = k;
					int[] tmp452_447 = this.sbPosAttrs;
					int tmp454_453 = tmp452_447[tmp452_450];
					tmp452_447[tmp452_450] = (tmp454_453 + 1);
					smallBufferAttrsk[tmp454_453] = ((int) (vasdata[posi] * 65536.0F));
				} else if (vas.type == 4) {
					int tmp489_487 = k;
					int[] tmp489_484 = this.sbPosAttrs;
					int tmp491_490 = tmp489_484[tmp489_487];
					tmp489_484[tmp489_487] = (tmp491_490 + 1);
					smallBufferAttrsk[tmp491_490] = ((int) (vasdata[(posi++)] * 65536.0F));
					int tmp517_515 = k;
					int[] tmp517_512 = this.sbPosAttrs;
					int tmp519_518 = tmp517_512[tmp517_515];
					tmp517_512[tmp517_515] = (tmp519_518 + 1);
					smallBufferAttrsk[tmp519_518] = ((int) (vasdata[(posi++)] * 65536.0F));
					int tmp545_543 = k;
					int[] tmp545_540 = this.sbPosAttrs;
					int tmp547_546 = tmp545_540[tmp545_543];
					tmp545_540[tmp545_543] = (tmp547_546 + 1);
					smallBufferAttrsk[tmp547_546] = ((int) (vasdata[(posi++)] * 65536.0F));
					int tmp573_571 = k;
					int[] tmp573_568 = this.sbPosAttrs;
					int tmp575_574 = tmp573_568[tmp573_571];
					tmp573_568[tmp573_571] = (tmp575_574 + 1);
					smallBufferAttrsk[tmp575_574] = ((int) (vasdata[posi] * 65536.0F));
				}

				if ((this.sbPosAttrs[k] == BUFFER_SIZE) || (end)) {
					this.attributes[k].put(smallBufferAttrsk, 0,
							this.sbPosAttrs[k]);
					this.sbPosAttrs[k] = 0;
				}
			}
		}

		if ((init) || (!this.staticUV))
			for (int k = 0; k < this.endStage; k++)
				if (k == 0) {
					float[] u = objVectors.nuOrg;
					float[] v = objVectors.nvOrg;
					int tmp690_689 = 0;
					int[] tmp690_686 = sbPosMT;
					int tmp692_691 = tmp690_686[tmp690_689];
					tmp690_686[tmp690_689] = (tmp692_691 + 1);
					smallBufferMT[0][tmp692_691] = ((int) (u[pind] * 65536.0F));
					int tmp716_715 = 0;
					int[] tmp716_712 = sbPosMT;
					int tmp718_717 = tmp716_712[tmp716_715];
					tmp716_712[tmp716_715] = (tmp718_717 + 1);
					smallBufferMT[0][tmp718_717] = ((int) (v[pind] * 65536.0F));

					if ((sbPosMT[0] == BUFFER_SIZE) || (end)) {
						this.multiTextures[k].put(smallBufferMT[0], 0,
								sbPosMT[0]);
						sbPosMT[0] = 0;
					}
				} else if (this.obj.maxStagesUsed > 1) {
					float[] u = objVectors.uMul[(k - 1)];
					float[] v = objVectors.vMul[(k - 1)];
					int tmp824_822 = k;
					int[] tmp824_819 = sbPosMT;
					int tmp826_825 = tmp824_819[tmp824_822];
					tmp824_819[tmp824_822] = (tmp826_825 + 1);
					smallBufferMT[k][tmp826_825] = ((int) (u[pind] * 65536.0F));
					int tmp852_850 = k;
					int[] tmp852_847 = sbPosMT;
					int tmp854_853 = tmp852_847[tmp852_850];
					tmp852_847[tmp852_850] = (tmp854_853 + 1);
					smallBufferMT[k][tmp854_853] = ((int) (v[pind] * 65536.0F));

					if ((sbPosMT[k] == BUFFER_SIZE) || (end)) {
						this.multiTextures[k].put(smallBufferMT[k], 0,
								sbPosMT[k]);
						sbPosMT[k] = 0;
					}
				}
	}

	public synchronized boolean isFilled()
	/*      */{
		/* 1368 */
		return this.filled;
		/*      */
	}

	protected void compileToVBO(GL11 gl11, GLRenderer renderer) {
		if ((this.dynamic) && (this.obj.sharing)) {
			ArrayList cis = this.obj.shareWith.compiled;
			int end = cis.size();
			for (int i = 0; i < end; i++) {
				CompiledInstance ci = (CompiledInstance) cis.get(i);
				if (ci.hasBeenRefilled) {
					ci.compileToVBO(gl11, renderer);
				}
			}
			return;
		}

		if ((!this.dynamic)
				&& ((this.buffersCreated) || (gl11 == null) || (!Config.useVBO))) {
			return;
		}

		if ((this.dynamic) && (!this.hasBeenRefilled)) {
			return;
		}

		if ((!this.dynamic) && (this.hasBeenVirtualized)
				&& (this.obj.virtualizer != null)) {
			this.vertices = ((IntBuffer) this.obj.virtualizer.restore(this,
					IntBuffer.class, "vertices"));
			this.normals = ((IntBuffer) this.obj.virtualizer.restore(this,
					IntBuffer.class, "normals"));
			this.colors = ((IntBuffer) this.obj.virtualizer.restore(this,
					IntBuffer.class, "colors"));
			this.tangents = ((IntBuffer) this.obj.virtualizer.restore(this,
					IntBuffer.class, "tangents"));
			this.indices = ((ShortBuffer) this.obj.virtualizer.restore(this,
					ShortBuffer.class, "indices"));

			for (int i = 0; i < this.endStage; i++) {
				this.multiTextures[i] = ((IntBuffer) this.obj.virtualizer
						.restore(this, IntBuffer.class, "multiTextures" + i));
			}
		}

		this.vertices.rewind();
		this.normals.rewind();
		if (this.colors != null) {
			this.colors.rewind();
		}
		if (this.tangents != null) {
			this.tangents.rewind();
		}
		if (this.indices != null) {
			this.indices.rewind();
		}
		for (int i = 0; i < this.endStage; i++) {
			if (this.multiTextures[i] != null) {
				this.multiTextures[i].rewind();
			}
		}

		if (this.attributes != null) {
			for (int k = 0; k < this.attributes.length; k++) {
				this.attributes[k].rewind();
			}
		}

		this.hasBeenRefilled = false;

		this.lastRenderer = renderer;

		int mode = 35044;
		if (this.dynamic) {
			mode = 35048;
		}

		if ((this.normalsId == 0) || (!this.buffersCreated)) {
			if (Logger.getLogLevel() >= 2) {
				Logger.log("Creating buffers...");
			}

			if (this.attributes != null) {
				this.attributesIds = new int[this.attributes.length];
			}

			int[] buffer = new int[1];

			gl11.glGenBuffers(1, buffer, 0);
			this.normalsId = buffer[0];

			gl11.glGenBuffers(1, buffer, 0);
			this.verticesId = buffer[0];

			if ((this.normalsId == 0) || (this.verticesId == 0)) {
				bufferError();
			} else {
				renderer.registerVBO(this.normalsId);
				renderer.registerVBO(this.verticesId);
			}

			if (this.colors != null) {
				gl11.glGenBuffers(1, buffer, 0);
				this.colorsId = buffer[0];
				if (this.colorsId == 0) {
					bufferError();
				} else
					renderer.registerVBO(this.colorsId);

			}

			if (this.tangents != null) {
				gl11.glGenBuffers(1, buffer, 0);
				this.tangentsId = buffer[0];
				if (this.tangentsId == 0) {
					bufferError();
				} else
					renderer.registerVBO(this.tangentsId);

			}

			if (this.attributes != null) {
				for (int i = 0; i < this.attributesIds.length; i++) {
					gl11.glGenBuffers(1, buffer, 0);
					this.attributesIds[i] = buffer[0];
					if (this.attributesIds[i] == 0) {
						bufferError();
					} else
						renderer.registerVBO(this.attributesIds[i]);
				}

			}

			this.multiTexturesIds = new int[this.endStage];
			for (int i = 0; i < this.endStage; i++) {
				gl11.glGenBuffers(1, buffer, 0);
				this.multiTexturesIds[i] = buffer[0];
				if (this.multiTexturesIds[i] == 0) {
					bufferError();
				} else
					renderer.registerVBO(this.multiTexturesIds[i]);

			}

			if (this.indexed) {
				gl11.glGenBuffers(1, buffer, 0);
				this.indicesId = buffer[0];
				if (this.indicesId == 0) {
					bufferError();
				} else
					renderer.registerVBO(this.indicesId);
			}

		}

		this.buffersCreated = true;
		gl11.glBindBuffer(34962, this.normalsId);
		int vertexSize = this.normals.capacity() * 4;
		gl11.glBufferData(34962, vertexSize, this.normals, mode);

		gl11.glBindBuffer(34962, this.verticesId);
		vertexSize = this.vertices.capacity() * 4;
		gl11.glBufferData(34962, vertexSize, this.vertices, mode);

		if (this.colors != null) {
			gl11.glBindBuffer(34962, this.colorsId);
			vertexSize = this.colors.capacity() * 4;
			gl11.glBufferData(34962, vertexSize, this.colors, mode);
		}

		if (this.tangents != null) {
			gl11.glBindBuffer(34962, this.tangentsId);
			vertexSize = this.tangents.capacity() * 4;
			gl11.glBufferData(34962, vertexSize, this.tangents, mode);
		}

		for (int i = 0; i < this.endStage; i++) {
			gl11.glBindBuffer(34962, this.multiTexturesIds[i]);
			vertexSize = this.multiTextures[i].capacity() * 4;
			gl11.glBufferData(34962, vertexSize, this.multiTextures[i], mode);
		}

		gl11.glBindBuffer(34962, 0);

		if (this.attributes != null) {
			for (int i = 0; i < this.attributes.length; i++) {
				gl11.glBindBuffer(34962, this.attributesIds[i]);
				vertexSize = this.attributes[i].capacity() * 4;
				gl11.glBufferData(34962, vertexSize, this.attributes[i], mode);
			}
			gl11.glBindBuffer(34962, 0);
		}

		if (this.indexed) {
			gl11.glBindBuffer(34963, this.indicesId);
			this.indices.rewind();
			vertexSize = this.indices.remaining() * 2;
			gl11.glBufferData(34963, vertexSize, this.indices, mode);
			gl11.glBindBuffer(34963, 0);
		}

		if (this.firstCompile) {
			if (Logger.getLogLevel() >= 2) {
				Logger.log("VBO created for object '" + this.obj.getName()
						+ "'");
			}
			this.firstCompile = false;
		}

		if ((!this.dynamic) && (this.obj.virtualizer != null)
				&& (!this.obj.virtualizer.isFull())) {
			if (this.hasBeenVirtualized) {
				this.normals = null;
				this.colors = null;
				this.tangents = null;
				this.indices = null;
				for (int i = 0; i < this.endStage; i++)
					this.multiTextures[i] = null;
			} else {
				this.hasBeenVirtualized = true;
				this.obj.virtualizer.store(this, this.vertices, "vertices");
				this.obj.virtualizer.store(this, this.normals, "normals");
				this.normals = null;
				this.obj.virtualizer.store(this, this.colors, "colors");
				this.colors = null;
				this.obj.virtualizer.store(this, this.tangents, "tangents");
				this.tangents = null;
				this.obj.virtualizer.store(this, this.indices, "indices");
				this.indices = null;
				for (int i = 0; i < this.endStage; i++) {
					this.obj.virtualizer.store(this, this.multiTextures[i],
							"multiTextures" + i);
					this.multiTextures[i] = null;
				}

			}

			this.vertices = ByteBuffer.allocateDirect(1)
					.order(ByteOrder.nativeOrder()).asIntBuffer();
		}
	}

	protected void bufferError() {
		Config.useVBO = false;
		Logger.log("Unable to initialize VBO!", 1);
	}

	protected Buffer flip(Buffer sr, int length) {
		if ((sr instanceof IntBuffer)) {
			IntBuffer src = (IntBuffer) sr;
			IntBuffer dest = ByteBuffer.allocateDirect(length * 4)
					.order(ByteOrder.nativeOrder()).asIntBuffer();
			src.rewind();
			int[] tmp = new int[length];
			src.get(tmp);
			dest.put(tmp);
			dest.rewind();
			return dest;
		}

		FloatBuffer src = (FloatBuffer) sr;
		FloatBuffer dest = ByteBuffer.allocateDirect(length * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		src.rewind();
		float[] tmp = new float[length];
		src.get(tmp);
		dest.put(tmp);
		dest.rewind();
		return dest;
	}

	protected void checkStrips() {
		if (this.useStrip != 0) {
			Logger.log(
					"Triangle strip checker has been called multiple times!", 1);
			return;
		}

		int endII = this.tris.size();
		Vectors objVectors = this.obj.objVectors;
		Mesh objMesh = this.obj.objMesh;
		int[][] points = objMesh.points;

		float[] x = objMesh.xOrg;
		float[] y = objMesh.yOrg;
		float[] z = objMesh.zOrg;

		float[] u = objVectors.nuOrg;
		float[] v = objVectors.nvOrg;

		Logger.log("Checking for triangle strip...", 3);

		float[][] bucket = new float[3][];
		float[][] cbucket = new float[3][];

		if (endII > 0) {
			int i = this.tris.get(0);
			for (int p = 0; p < 3; p++) {
				int c = objMesh.coords[points[i][p]];
				bucket[p] = new float[] { x[c], y[c], z[c], u[c], v[c] };
				cbucket[p] = new float[] { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F };
			}
		} else {
			this.useStrip = 2;
			return;
		}

		int cnt = 0;

		for (int ii = 1; ii < endII; ii++) {
			int i = this.tris.get(ii);
			for (int p = 0; p < 3; p++) {
				int pind = points[i][p];
				int coord = objMesh.coords[pind];

				cbucket[p][0] = x[coord];
				cbucket[p][1] = y[coord];
				cbucket[p][2] = z[coord];
				cbucket[p][3] = u[coord];
				cbucket[p][4] = v[coord];
			}

			if (cnt == 0) {
				boolean res = (compare(bucket[1], cbucket[1]))
						&& (compare(bucket[2], cbucket[0]));
				if (!res) {
					this.useStrip = 2;
					if (Logger.isDebugEnabled()) {
						Logger.log("Not a triangle strip at position " + ii
								+ "!", 3);
					}
					return;
				}
				cnt = 1;
			} else {
				boolean res = (compare(bucket[0], cbucket[0]))
						&& (compare(bucket[2], cbucket[1]));
				if (!res) {
					this.useStrip = 2;
					if (Logger.isDebugEnabled()) {
						Logger.log("Not a triangle strip at position " + ii
								+ "!", 3);
					}
					return;
				}
				cnt = 0;
			}
			float[][] swap = bucket;
			bucket = cbucket;
			cbucket = swap;
		}
		this.useStrip = 1;
		this.primitiveType = 5;
		if (Logger.getLogLevel() >= 2)
			Logger.log("Triangle strip found!", 2);
	}

	private boolean compare(float[] a, float[] b) {
		for (int i = 0; i < 5; i++) {
			if (a[i] != b[i]) {
				return false;
			}
		}
		return true;
	}

	public void finalize() {
		if (this.lastRenderer != null)
			dispose(this.lastRenderer);
	}
}
