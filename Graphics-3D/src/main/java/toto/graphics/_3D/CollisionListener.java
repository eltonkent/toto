package toto.graphics._3D;

import java.io.Serializable;

public abstract interface CollisionListener extends Serializable {
	public abstract void collision(CollisionEvent paramCollisionEvent);

	public abstract boolean requiresPolygonIDs();
}
