package toto.graphics._3D;

import java.io.Serializable;

import toto.graphics.color.RGBColor;

public class Overlay implements Serializable {
	private static final long serialVersionUID = 2L;
	private T3DScene rage3DScene;
	private T3DObject plane;
	private MyController adjuster = null;
	private int upperLeftX;
	private int upperLeftY;
	private int lowerRightX;
	private int lowerRightY;
	private int upperLeftU;
	private int upperLeftV;
	private int lowerRightU;
	private int lowerRightV;
	private int pivotX = -999999999;
	private int pivotY = -999999999;
	private float depth = Config.nearPlane + 5.0F;
	private boolean disposed = false;
	private boolean uvChange = false;
	private float rotation = 0.0F;
	private boolean rotMode = false;
	private static int cnt = 0;

	private SimpleVector tmp1 = new SimpleVector();
	private SimpleVector tmp2 = new SimpleVector();
	private SimpleVector tmp3 = new SimpleVector();
	private SimpleVector tmp4 = new SimpleVector();
	private SimpleVector tmp5 = new SimpleVector();
	private SimpleVector tmp6 = new SimpleVector();
	private Matrix tmpMat = new Matrix();

	public Overlay(T3DScene rage3DScene, FrameBuffer buffer,
			String textureName) {
		this(rage3DScene, 0, 0, buffer.getWidth(), buffer.getHeight(),
				textureName);
	}

	public Overlay(T3DScene rage3DScene, int upperLeftX, int upperLeftY,
			int lowerRightX, int lowerRightY, String textureName) {
		this(rage3DScene, upperLeftX, upperLeftY, lowerRightX, lowerRightY,
				textureName, false);
	}

	public Overlay(T3DScene rage3DScene, int upperLeftX, int upperLeftY,
			int lowerRightX, int lowerRightY, String textureName,
			boolean modifyUV) {
		this.rage3DScene = rage3DScene;
		this.upperLeftX = upperLeftX;
		this.upperLeftY = upperLeftY;
		this.lowerRightX = lowerRightX;
		this.lowerRightY = lowerRightY;
		this.plane = T3DPrimitives.getPlane(1, 1.0F);
		if (textureName != null) {
			this.plane.setTexture(textureName);
			TextureCache.getInstance().getTexture(textureName).setMipmap(false);
		}
		this.plane.setName("__overlay plane " + cnt++ + "__");
		this.plane.setAdditionalColor(RGBColor.WHITE);
		this.plane.setLighting(Lighting.NO_LIGHTS);
		this.plane.setUserObject(this);
		rage3DScene.addObject(this.plane);
		this.adjuster = new MyController(null);
		this.plane.getMesh().setVertexController(this.adjuster, false);
		this.plane.build(!modifyUV);
	}

	public void setDepth(float depth) {
		if (depth < Config.nearPlane) {
			depth = Config.nearPlane + 1.0F;
		}
		this.depth = depth;
	}

	public void setTransparency(int trans) {
		this.plane.setTransparency(trans);
	}

	public void setColor(RGBColor color) {
		this.plane.setAdditionalColor(color);
	}

	public void setTexture(String textureName) {
		this.plane.setTexture(textureName);
	}

	public void setTexture(TextureInfo textureInfo) {
		this.plane.setTexture(textureInfo);
	}

	public void setVisibility(boolean visible) {
		this.plane.setVisibility(visible);
	}

	public void setTransparencyMode(TransparencyMode mode) {
		this.plane.setTransparencyMode(mode);
	}

	public void setNewCoordinates(int upperLeftX, int upperLeftY,
			int lowerRightX, int lowerRightY) {
		this.upperLeftX = upperLeftX;
		this.upperLeftY = upperLeftY;
		this.lowerRightX = lowerRightX;
		this.lowerRightY = lowerRightY;
	}

	public void setSourceCoordinates(int upperLeftX, int upperLeftY,
			int lowerRightX, int lowerRightY) {
		this.upperLeftU = upperLeftX;
		this.upperLeftV = upperLeftY;
		this.lowerRightU = lowerRightX;
		this.lowerRightV = lowerRightY;
		this.uvChange = true;
	}

	public void setRotation(float angle) {
		this.rotation = angle;
		if (angle != 0.0F)
			this.rotMode = true;
	}

	public void setRotationPivot(int x, int y) {
		this.pivotX = x;
		this.pivotY = y;
	}

	public T3DObject getObject3D()
	/*     */{
		/* 291 */
		return this.plane;
		/*     */
	}

	public synchronized void dispose() {
		if (!this.disposed) {
			this.disposed = true;
			this.rage3DScene.removeObject(this.plane);
			this.plane.setUserObject(null);
			this.rage3DScene = null;
			this.plane = null;
			this.adjuster = null;
		}
	}

	public void unlink() {
		this.plane.setUserObject(null);
	}

	public void update(FrameBuffer buffer) {
		if ((this.plane.getVisibility()) && (!this.disposed)) {
			if (this.uvChange) {
				PolygonManager pm = this.plane.getPolygonManager();
				int tid = pm.getPolygonTexture(0);
				Texture t = TextureCache.getInstance().getTextureByID(tid);
				float u1 = this.upperLeftU / t.getWidth();
				float v1 = this.upperLeftV / t.getHeight();
				float u2 = this.lowerRightU / t.getWidth();
				float v2 = this.lowerRightV / t.getHeight();
				TextureInfo ti = new TextureInfo(tid, u1, v1, u1, v2, u2, v1);
				pm.setPolygonTexture(0, ti);

				ti = new TextureInfo(tid, u1, v2, u2, v2, u2, v1);
				pm.setPolygonTexture(1, ti);
			}
			Camera cam = this.rage3DScene.getCamera();

			boolean pivot = this.pivotX != -999999999;

			SimpleVector upperLeft = Interact2D.reproject2D3D(cam, buffer,
					this.upperLeftX, this.upperLeftY, this.depth, this.tmp1);
			SimpleVector lowerLeft = Interact2D.reproject2D3D(cam, buffer,
					this.upperLeftX, this.lowerRightY, this.depth, this.tmp2);
			SimpleVector lowerRight = Interact2D.reproject2D3D(cam, buffer,
					this.lowerRightX, this.lowerRightY, this.depth, this.tmp3);
			SimpleVector upperRight = Interact2D.reproject2D3D(cam, buffer,
					this.lowerRightX, this.upperLeftY, this.depth, this.tmp4);

			SimpleVector pivotPoint = null;
			if (pivot) {
				pivotPoint = Interact2D.reproject2D3D(cam, buffer, this.pivotX,
						this.pivotY, this.depth, this.tmp6);
			}

			Matrix camMat = this.rage3DScene.getCamera().getBack();
			SimpleVector camPos = this.rage3DScene.getCamera().getPosition(
					this.tmp5);
			camPos.matMul(camMat);
			upperLeft.add(camPos);
			lowerLeft.add(camPos);
			lowerRight.add(camPos);
			upperRight.add(camPos);
			Matrix resMat = camMat.invert(this.tmpMat);
			upperLeft.matMul(resMat);
			lowerLeft.matMul(resMat);
			lowerRight.matMul(resMat);
			upperRight.matMul(resMat);

			if (pivot) {
				pivotPoint.add(camPos);
				pivotPoint.matMul(resMat);
			}

			this.adjuster.setNewBounds(upperLeft, lowerLeft, upperRight,
					lowerRight);
			this.plane.getMesh().applyVertexController();

			if (this.rotMode) {
				this.plane.getRotationMatrix().setIdentity();
				SimpleVector s = this.tmp1;
				if (!pivot) {
					this.tmp1.set(upperLeft);
					s.add(lowerLeft);
					s.add(upperRight);
					s.add(lowerRight);
					s.scalarMul(0.25F);
				} else {
					s.set(pivotPoint);
				}
				this.plane.setRotationPivot(s);
				this.plane.rotateAxis(resMat.getZAxis(), this.rotation);
			}
			this.plane.touch();
		}
	}

	protected void finalize() {
		dispose();
	}

	private static class MyController extends GenericVertexController {

		private MyController() {
			poss = new SimpleVector[4];
		}

		MyController(MyController mycontroller) {
			this();
		}

		private static final long serialVersionUID = 1L;
		private SimpleVector[] poss = new SimpleVector[4];

		public void apply() {
			SimpleVector[] dstMesh = getDestinationMesh();
			for (int i = 0; i < 4; i++)
				dstMesh[i] = this.poss[i];
		}

		public void setNewBounds(SimpleVector ul, SimpleVector ll,
				SimpleVector ur, SimpleVector lr) {
			this.poss[0] = ul;
			this.poss[1] = ll;
			this.poss[2] = ur;
			this.poss[3] = lr;
		}
	}
}
