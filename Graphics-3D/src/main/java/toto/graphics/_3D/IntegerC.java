package toto.graphics._3D;

class IntegerC {
	private static final Integer[] CACHE = new Integer[4000];
	private static final int offset = 2000;

	static {
		for (int i = 0; i < CACHE.length; i++)
			CACHE[i] = Integer.valueOf(i - 2000);
	}

	public static Integer valueOf(int i) {
		if ((i >= -2000) && (i <= 1999)) {
			return CACHE[(i + 2000)];
		}
		return Integer.valueOf(i);
	}
}
