package toto.graphics._3D;

class MathUtils {
	static final void calcCross(float[] res, float[] one, float[] two) {
		res[0] = (one[1] * two[2] - one[2] * two[1]);
		res[1] = (one[2] * two[0] - one[0] * two[2]);
		res[2] = (one[0] * two[1] - one[1] * two[0]);
	}

	static final void calcCross(float[] res, float oneX, float oneY,
			float oneZ, float twoX, float twoY, float twoZ) {
		res[0] = (oneY * twoZ - oneZ * twoY);
		res[1] = (oneZ * twoX - oneX * twoZ);
		res[2] = (oneX * twoY - oneY * twoX);
	}

	static final float calcDot(float[] one, float[] two) {
		return one[0] * two[0] + one[1] * two[1] + one[2] * two[2];
	}

	static final float calcDot(float oneX, float oneY, float oneZ, float[] two) {
		return oneX * two[0] + oneY * two[1] + oneZ * two[2];
	}

	static final float calcDot(float oneX, float oneY, float oneZ, float twoX,
			float twoY, float twoZ) {
		return oneX * twoX + oneY * twoY + oneZ * twoZ;
	}
}
