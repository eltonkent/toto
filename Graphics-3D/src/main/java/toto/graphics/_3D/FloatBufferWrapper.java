/*     */
package toto.graphics._3D;

/*     */
/*     */

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/*     */
/*     */
/*     */

/*     */
/*     */class FloatBufferWrapper
/*     */{
	/*     */ByteBuffer bytes;
	/*     */FloatBuffer floats;
	/*     */private IntBuffer ints;
	/* 21 */private static int[] intArray = new int[0];

	/*     */
	/*     */
	public FloatBufferWrapper(ByteBuffer buffer) {
		/* 24 */
		this.bytes = buffer;
		/* 25 */
		this.floats = this.bytes.asFloatBuffer();
		/* 26 */
		this.ints = this.bytes.asIntBuffer();
		/*     */
	}

	/*     */
	/*     */
	public FloatBufferWrapper(int capacity) {
		/* 30 */
		this.bytes = ByteBuffer.allocateDirect(capacity * 4).order(
				ByteOrder.nativeOrder());
		/* 31 */
		this.floats = this.bytes.asFloatBuffer();
		/* 32 */
		this.ints = this.bytes.asIntBuffer();
		/*     */
	}

	/*     */
	/*     */
	public void flip() {
		/* 36 */
		this.bytes.flip();
		/* 37 */
		this.floats.flip();
		/* 38 */
		this.ints.flip();
		/*     */
	}

	/*     */
	/*     */
	public void put(float f) {
		/* 42 */
		this.bytes.position(this.bytes.position() + 4);
		/* 43 */
		this.floats.put(f);
		/* 44 */
		this.ints.position(this.ints.position() + 1);
		/*     */
	}

	/*     */
	/*     */
	public void put(float[] data, int offset, int len) {
		/* 48 */
		if (intArray.length < len) {
			/* 49 */
			intArray = new int[len];
			/*     */
		}
		/*     */
		/* 52 */
		int end = offset + len;
		/* 53 */
		for (int i = offset; i < end; i++) {
			/* 54 */
			intArray[(i - offset)] = Float.floatToRawIntBits(data[i]);
			/*     */
		}
		/*     */
		/* 57 */
		this.bytes.position(this.bytes.position() + 4 * len);
		/* 58 */
		this.floats.position(this.floats.position() + len);
		/* 59 */
		this.ints.put(intArray, 0, len);
		/*     */
	}

	/*     */
	/*     */
	public void put(float[] data) {
		/* 63 */
		if (intArray.length < data.length) {
			/* 64 */
			intArray = new int[data.length];
			/*     */
		}
		/*     */
		/* 67 */
		int end = data.length;
		/* 68 */
		for (int i = 0; i < end; i++) {
			/* 69 */
			intArray[i] = Float.floatToRawIntBits(data[i]);
			/*     */
		}
		/*     */
		/* 72 */
		this.bytes.position(this.bytes.position() + 4 * end);
		/* 73 */
		this.floats.position(this.floats.position() + end);
		/* 74 */
		this.ints.put(intArray, 0, data.length);
		/*     */
	}

	/*     */
	/*     */
	public void put(int[] data) {
		/* 78 */
		this.bytes.position(this.bytes.position() + 4 * data.length);
		/* 79 */
		this.floats.position(this.floats.position() + data.length);
		/* 80 */
		this.ints.put(data, 0, data.length);
		/*     */
	}

	/*     */
	/*     */
	public void put(FloatBufferWrapper b) {
		/* 84 */
		this.bytes.put(b.bytes);
		/* 85 */
		this.floats.position(this.bytes.position() >> 2);
		/* 86 */
		this.ints.position(this.bytes.position() >> 2);
		/*     */
	}

	/*     */
	/*     */
	public int capacity() {
		/* 90 */
		return this.floats.capacity();
		/*     */
	}

	/*     */
	/*     */
	public int position() {
		/* 94 */
		return this.floats.position();
		/*     */
	}

	/*     */
	/*     */
	public void position(int p) {
		/* 98 */
		this.bytes.position(4 * p);
		/* 99 */
		this.floats.position(p);
		/* 100 */
		this.ints.position(p);
		/*     */
	}

	/*     */
	/*     */
	public FloatBuffer slice() {
		/* 104 */
		return this.floats.slice();
		/*     */
	}

	/*     */
	/*     */
	public int remaining() {
		/* 108 */
		return this.floats.remaining();
		/*     */
	}

	/*     */
	/*     */
	public int limit() {
		/* 112 */
		return this.floats.limit();
		/*     */
	}

	/*     */
	/*     */
	public void clear() {
		/* 116 */
		this.bytes.clear();
		/* 117 */
		this.floats.clear();
		/* 118 */
		this.ints.clear();
		/*     */
	}

	/*     */
	/*     */
	public void rewind() {
		/* 122 */
		this.bytes.rewind();
		/* 123 */
		this.floats.rewind();
		/* 124 */
		this.ints.rewind();
		/*     */
	}

	/*     */
	/*     */
	public ByteBuffer getByteBuffer() {
		/* 128 */
		return this.bytes;
		/*     */
	}
	/*     */
}
