package toto.graphics._3D;

import java.nio.Buffer;
import java.nio.ByteBuffer;

abstract interface GL20Handler {
	public abstract GLSLShader updateShaderData();

	public abstract void resetShaderData();

	public abstract void setShader(GLSLShader paramGLSLShader);

	public abstract void clearShader();

	public abstract void reset();

	public abstract boolean uploadTexture(int paramInt1, int paramInt2,
			int paramInt3, int paramInt4, boolean paramBoolean,
			ByteBuffer paramByteBuffer);

	public abstract void setTangents(Buffer paramBuffer);

	public abstract void setTangents(int paramInt);

	public abstract void clearTangents();

	public abstract void clearTangents(int paramInt);

	public abstract void setRenderTarget(Texture paramTexture,
			GLRenderer paramGLRenderer, FrameBuffer paramFrameBuffer);

	public abstract void unloadRenderTarget(Texture paramTexture);

	public abstract void bindVertexAttributes(String paramString, int paramInt,
			Buffer paramBuffer);

	public abstract void bindVertexAttributes(String paramString,
			int paramInt1, int paramInt2);

	public abstract void unbindVertexAttributes(String paramString,
			int paramInt, Buffer paramBuffer);

	public abstract void unbindVertexAttributes(String paramString,
			int paramInt1, int paramInt2);

	public abstract int getTextureStagesRaw();
}
