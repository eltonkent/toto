package toto.graphics._3D;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/*     */
/*     */
/*     */

class Object3DCompiler implements Serializable {
	private static final long serialVersionUID = 1L;

	void compile(T3DObject obj) {
		if ((obj.compiled == null) || (obj.compiled.size() != 0)) {
			return;
		}

		if (obj.shareWith != null) {
			if (obj.shareWith.shareWith != null) {
				Logger.log(
						"Can't share data with an object that shares data itself. Share data with the source object instead!",
						0);
				return;
			}

			int cnt = obj.shareWith.compiled.size();
			boolean fp = false;
			if (cnt > 0) {
				fp = obj.shareWith.compiled.get(0) instanceof CompiledInstanceFP;
			}
			for (int i = 0; i < cnt; i++) {
				int polyIndex = ((CompiledInstance) obj.shareWith.compiled
						.get(i)).polyIndex;
				if (fp)
					obj.addCompiled(new CompiledInstanceFP(obj, polyIndex, -1));
				else {
					obj.addCompiled(new CompiledInstance(obj, polyIndex, -1));
				}
			}
			if (Logger.getLogLevel() >= 2) {
				Logger.log("Object " + obj.getID() + "/" + obj.getName()
						+ " precompiled!", 2);
			}
			return;
		}

		long s = System.currentTimeMillis();

		if ((!obj.isCompiled()) && (obj.compiled != null)) {
			HashMap instances = new HashMap();
			int batchSize = Config.glBatchSize;
			if (obj.batchSize != -1) {
				batchSize = obj.batchSize;
			}
			batchSize = Math.min(16000, batchSize);
			int end = obj.getMesh().anzTri;
			HashMap count = new HashMap();
			ArrayList leafs = null;
			boolean hoc = false;
			if ((obj.getOcTree() != null)
					&& (obj.getOcTree().getRenderingUse())) {
				leafs = obj.getOcTree().getFilledLeafs();
				hoc = true;
			}

			int lastNode = -1;

			StringBuilder key = new StringBuilder();
			int[] objtexture = obj.texture;
			int[][] objmultiTex = obj.multiTex;
			int[][] objmultiMode = obj.multiMode;
			boolean objfixedPointMode = (obj.fixedPointMode)
					&& ((!obj.dynamic) || (!BufferUtilFactory
							.hasNativeSupport()));
			for (int i = 0; i < end; i++) {
				key.setLength(0);
				append(key, objtexture[i]);
				if (objmultiTex != null) {
					int endLoop = objmultiTex.length;
					for (int p = 0; p < endLoop; p++) {
						int[] objmultiTexp = objmultiTex[p];
						int[] objmultiModep = objmultiMode[p];
						key.append('_');
						append(key, objmultiTexp[i]);
						key.append('/');
						append(key, objmultiModep[i]);
					}
				}

				int id = -1;
				if (hoc) {
					if (lastNode != -1) {
						int[] polys = ((OcTreeNode) leafs.get(lastNode))
								.getPolygons();
						int endLoop = polys.length;
						for (int x = 0; x < endLoop; x++) {
							if (i == polys[x]) {
								id = ((OcTreeNode) leafs.get(lastNode)).getID();
								break;
							}
						}
					}

					if (id == -1) {
						for (int p = 0; p < leafs.size(); p++) {
							int[] polys = ((OcTreeNode) leafs.get(p))
									.getPolygons();
							int endLoop = polys.length;
							for (int x = 0; x < endLoop; x++) {
								if (i == polys[x]) {
									id = ((OcTreeNode) leafs.get(p)).getID();
									lastNode = p;
									p = leafs.size();
									break;
								}
							}
						}
					}
					key.append("_oc_");
					append(key, id);
				}

				String keyStr = key.toString();
				int[] cs = (int[]) count.get(keyStr);
				if (cs == null) {
					cs = new int[1];
					count.put(keyStr, cs);
				}
				cs[0] += 1;
				key.append('_');
				append(key, cs[0] / batchSize);

				keyStr = key.toString();

				CompiledInstance ci = (CompiledInstance) instances.get(keyStr);
				if (ci == null) {
					if (objfixedPointMode)
						ci = new CompiledInstance(obj, i, id);
					else {
						ci = new CompiledInstanceFP(obj, i, id);
					}
					instances.put(keyStr, ci);
				}

				ci.add(i);
			}

			count.clear();
			ArrayList keys = new ArrayList(instances.keySet());
			Collections.sort(keys);

			int size = instances.size();
			for (int i = 0; i < keys.size(); i++) {
				String keyo = (String) keys.get(i);
				CompiledInstance ci = (CompiledInstance) instances.remove(keyo);
				ci.fill();
				ci.setKey(keyo);
				obj.addCompiled(ci);
			}
			if (Logger.getLogLevel() >= 2) {
				Logger.log(
						"Object " + obj.getID() + "/" + obj.getName()
								+ " compiled to " + size + " subobjects in "
								+ (System.currentTimeMillis() - s) + "ms!", 2);
			}
		} else if ((Logger.getLogLevel() >= 2) && (obj.compiled != null)) {
			Logger.log("Object " + obj.getID() + "/" + obj.getName()
					+ " already compiled!", 0);
		}
	}

	private void append(StringBuilder sb, int val) {
		val += 1000;
		if (val > 65536) {
			int high = val >> 16;
			int low = val - (high << 16);
			sb.append(low);
			sb.append(high);
		} else {
			sb.append((char) val);
		}
	}
}
