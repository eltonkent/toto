package toto.graphics._3D;

import java.io.IOException;
import java.io.Writer;

import android.util.Log;

class Logger {
	public static final int ERROR = 0;
	static final int WARNING = 1;
	static final int MESSAGE = 2;
	static final int DEBUG = 3;
	static final int ON_ERROR_THROW_EXCEPTION = 2;
	static final int ON_ERROR_EXIT = 1;
	static final int ON_ERROR_RESUME_NEXT = 0;
	static final int LL_ONLY_ERRORS = 0;
	static final int LL_ERRORS_AND_WARNINGS = 1;
	static final int LL_VERBOSE = 2;
	static final int LL_DEBUG = 3;
	private static int mode = 2;

	private static int logLevel = 2;

	private static LogHandler logHandler = null;

	private static final String[] HEAD = { "ERROR: ", "WARNING: ", "MESSAGE: " };

	static void setOnError(int mode)
	/*     */{
		/* 90 */
		mode = mode;
		/*     */
	}

	static void setLogLevel(int level)
	/*     */{
		/* 104 */
		logLevel = level;
		/*     */
	}

	static int getLogLevel()
	/*     */{
		/* 118 */
		return logLevel;
		/*     */
	}

	static void log(String msg) {
		log(msg, 2);
	}

	static void log(Throwable tw) {
		log(Log.getStackTraceString(tw), 0);
	}

	static void log(Throwable tw, int type) {
		log(Log.getStackTraceString(tw), type);
	}

	static boolean isDebugEnabled() {
		return logLevel == 3;
	}

	static void log(String msg, int type) {
		if (type <= logLevel) {
			if ((type < 0) || (type > 3)) {
				type = 3;
			}
			if (type < 2) {
				msg = "[ " + System.currentTimeMillis() + " ] - " + HEAD[type]
						+ msg;
			}

			boolean logThis = (logHandler == null)
					|| (logHandler.log(msg, type));
			if (logThis) {
				Log.i("Rage3D", msg);

				if (type == 0)
					if (mode == 1) {
						System.exit(-99);
					} else if (mode == 2)
						throw new RuntimeException(msg);
			}
		}
	}

	static Writer getWriter() {
		return new Writer() {
			private StringBuilder sb = new StringBuilder();

			public void close() throws IOException {
				flush();
				this.sb = null;
			}

			public void flush() throws IOException {
				if (this.sb == null) {
					return;
				}
				Logger.log(this.sb.toString());
				this.sb.setLength(0);
			}

			public void write(char[] arg0, int arg1, int arg2)
					throws IOException {
				if (this.sb == null) {
					return;
				}

				if ((arg1 != 0) || (arg2 + arg1 != arg0.length)) {
					char[] tmp = new char[arg2];
					System.arraycopy(arg0, arg1, tmp, 0, arg2);
				}

				String str = String.valueOf(arg0);
				int pos = str.indexOf('\n');
				if (pos != -1) {
					String p1 = str.substring(0, pos);
					this.sb.append(p1);
					flush();
					if (pos < str.length() - 1) {
						String p2 = str.substring(pos + 1);
						this.sb.append(p2);
					}
				} else {
					this.sb.append(str);
				}
			}
		};
	}

	static void setLogHandler(LogHandler logHandler)
	/*     */{
		/* 261 */
		logHandler = logHandler;
		/*     */
	}

	static LogHandler getLogHandler()
	/*     */{
		/* 270 */
		return logHandler;
		/*     */
	}
}
