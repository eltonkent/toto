package toto.graphics._3D;

public class VertexAttributes {
	public static final int TYPE_SINGLE_FLOATS = 1;
	public static final int TYPE_TWO_FLOATS = 2;
	public static final int TYPE_THREE_FLOATS = 3;
	public static final int TYPE_FOUR_FLOATS = 4;
	float[] data;
	int type;
	boolean updated = false;
	String name;
	boolean dynamic = false;

	public VertexAttributes(String name, float[] data, int type) {
		this.data = new float[data.length];
		System.arraycopy(data, 0, this.data, 0, data.length);
		this.type = type;
		this.updated = true;
		this.name = name;
	}

	public boolean isDynamic()
	/*    */{
		/* 61 */
		return this.dynamic;
		/*    */
	}

	public void setDynamic(boolean dynamic)
	/*    */{
		/* 69 */
		this.dynamic = dynamic;
		/*    */
	}

	public void update(float[] data, int start) {
		if (!this.dynamic) {
			Logger.log("VertexAttributes haven't been defined as dynamic!", 0);
			return;
		}
		int len = data.length;
		if ((start >= 0) && (start + len <= data.length)) {
			System.arraycopy(data, 0, this.data, start, len);
			this.updated = true;
		} else {
			Logger.log("Invalid size: " + start + "/" + len, 0);
		}
	}

	public boolean matches(int vertexCount) {
		return this.data.length / this.type == vertexCount;
	}
}
