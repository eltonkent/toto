package toto.graphics._3D;

/**
 * When doing ellipsoid collision detection with this object, the ellipsoid can
 * be transformed according to the objects's transformation and in the source's
 * object space or it remains static in the target's object space (i.e.
 * "axis aligned"). The later is faster, but not suitable for all kinds of
 * ellipsoids
 * 
 * @see toto.graphics._3D.T3DObject#setEllipsoidMode(EllipsoidMode)
 */
public enum EllipsoidMode {

	/**
	 * The object's ellipsoid won't be transformed when performing collision
	 * detection (default).
	 */
	ELLIPSOID_ALIGNED, // 0;
	/**
	 * The object's ellipsoid will be transformed when performing collision
	 * detection.
	 */
	ELLIPSOID_TRANSFORMED// = 1;
}
