package toto.graphics._3D;

import java.io.Serializable;

import toto.graphics.color.RGBColor;

public class LensFlare implements Serializable {
	private static final long serialVersionUID = 1L;
	private Texture burst = null;
	private Texture halo1 = null;
	private Texture halo2 = null;
	private Texture halo3 = null;
	private SimpleVector sunPos = null;

	private Texture[] types = new Texture[7];
	private float[][] scales = new float[7][2];
	private float globalScale = 1.0F;
	private int trans = 1;
	private boolean cover = true;
	private float maxDistance = -1.0F;
	private boolean revertDirection = false;

	private boolean visible = false;
	private SimpleVector light2D = null;

	private SimpleVector tmp1 = new SimpleVector();
	private SimpleVector tmp2 = new SimpleVector();
	private SimpleVector tmp3 = new SimpleVector();

	public LensFlare(SimpleVector lightPos, String burst, String halo1,
			String halo2, String halo3) {
		TextureCache tm = TextureCache.getInstance();
		this.burst = tm.getTexture(burst);
		this.halo1 = tm.getTexture(halo1);
		this.halo2 = tm.getTexture(halo2);
		this.halo3 = tm.getTexture(halo3);
		this.sunPos = new SimpleVector(lightPos);

		this.types[0] = this.burst;
		this.types[1] = this.halo1;
		this.types[2] = this.burst;
		this.types[3] = this.halo2;
		this.types[4] = this.burst;
		this.types[5] = this.halo3;
		this.types[6] = this.burst;

		this.scales[0][0] = 1.0F;
		this.scales[0][1] = 1.0F;
		this.scales[1][0] = 2.0F;
		this.scales[1][1] = 0.5F;
		this.scales[2][0] = 3.0F;
		this.scales[2][1] = 0.25F;
		this.scales[3][0] = 8.0F;
		this.scales[3][1] = 1.0F;
		this.scales[4][0] = -2.0F;
		this.scales[4][1] = 0.5F;
		this.scales[5][0] = -4.0F;
		this.scales[5][1] = 0.25F;
		this.scales[6][0] = -5.5F;
		this.scales[6][1] = 0.25F;
	}

	public void setTransparency(int trans)
	/*     */{
		/* 88 */
		this.trans = trans;
		/*     */
	}

	public void setLightPosition(SimpleVector lightPos) {
		this.sunPos.set(lightPos);
	}

	public void setGlobalScale(float scale)
	/*     */{
		/* 104 */
		this.globalScale = scale;
		/*     */
	}

	public void setHiding(boolean hides)
	/*     */{
		/* 113 */
		this.cover = hides;
		/*     */
	}

	public void setMaximumDistance(float distance)
	/*     */{
		/* 123 */
		this.maxDistance = distance;
		/*     */
	}

	public void setDirection(boolean lightToCam)
	/*     */{
		/* 132 */
		this.revertDirection = lightToCam;
		/*     */
	}

	public void update(FrameBuffer buffer, T3DScene rage3DScene) {
		Camera cam = rage3DScene.getCamera();
		this.light2D = Interact2D.project3D2D(cam, buffer, this.sunPos,
				this.tmp3);
		this.visible = true;
		if (this.cover) {
			SimpleVector camPos = cam.getPosition(this.tmp1);
			if (!this.revertDirection) {
				SimpleVector delta = camPos;
				this.tmp2.set(camPos);
				delta.scalarMul(-1.0F);
				delta.add(this.sunPos);
				float dlen = delta.length();
				float dist = rage3DScene.calcMinDistance(
						this.tmp2,
						delta.normalize(delta),
						this.maxDistance != -1.0F ? Math.min(this.maxDistance,
								dlen * 1.05F) : dlen * 1.05F);

				this.visible = ((dist == 1.0E+012F) || (dist > dlen - 5.0F));
			} else {
				this.tmp2.set(this.sunPos);
				this.tmp2.scalarMul(-1.0F);
				SimpleVector delta = camPos;
				delta.add(this.tmp2);
				float dlen = delta.length();
				float dist = rage3DScene.calcMinDistance(
						this.sunPos,
						delta.normalize(delta),
						this.maxDistance != -1.0F ? Math.min(this.maxDistance,
								dlen * 1.05F) : dlen * 1.05F);
				this.visible = ((dist == 1.0E+012F) || (dist > dlen - 5.0F));
			}
		}
	}

	public void render(FrameBuffer buffer) {
		if ((this.light2D != null) && (this.visible)) {
			SimpleVector lp = this.tmp1;
			lp.set(this.light2D);
			float mx = buffer.getCenterX();
			float my = buffer.getCenterY();
			lp.z = 0.0F;
			SimpleVector cp = this.tmp2;
			cp.set(mx, my, 0.0F);
			cp.scalarMul(-1.0F);
			lp.add(cp);
			SimpleVector dir = lp;
			float len = dir.length();
			dir = dir.normalize(dir);
			SimpleVector d = this.tmp2;
			d.set(0.0F, 0.0F, 0.0F);

			for (int i = 0; i < this.types.length; i++) {
				d.set(dir);
				Texture t = this.types[i];
				float l = this.scales[i][0];
				float scale = this.scales[i][1] * this.globalScale;
				d.scalarMul(1.0F / l * len);
				int tw = t.getWidth();
				int th = t.getHeight();
				int x = (int) (d.x - (tw >> 1) * scale);
				int y = (int) (d.y - (th >> 1) * scale);
				buffer.blit(t, 0, 0, x + (int) mx, y + (int) my, tw, th,
						(int) (tw * scale), (int) (th * scale), this.trans,
						true, RGBColor.WHITE);
			}
		}
	}
}
