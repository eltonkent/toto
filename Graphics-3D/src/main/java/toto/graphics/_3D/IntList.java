package toto.graphics._3D;

class IntList {
	private int[] store = new int[10];
	private int cnt = 0;

	void add(int i) {
		if (this.cnt >= this.store.length) {
			int[] n = new int[this.cnt + Math.min(32768, this.cnt)];
			System.arraycopy(this.store, 0, n, 0, this.cnt);
			this.store = n;
		}
		this.store[this.cnt] = i;
		this.cnt += 1;
	}

	void clear() {
		this.cnt = 0;
		this.store = new int[10];
	}

	int get(int pos) {
		if (pos >= this.cnt) {
			throw new ArrayIndexOutOfBoundsException(pos);
		}
		return this.store[pos];
	}

	int size() {
		return this.cnt;
	}

	void compact() {
		int[] n = new int[this.cnt];
		System.arraycopy(this.store, 0, n, 0, this.cnt);
		this.store = n;
	}
}
