package toto.graphics._3D;

class MemoryHelper {
	static void compact() {
		Logger.log("Memory usage before compacting: " + getMem());
		System.gc();
		System.runFinalization();
		System.gc();
		try {
			Thread.sleep(150L);
		} catch (Exception localException) {
		}
		Logger.log("Memory usage after compacting: " + getMem());
	}

	static void printMemory() {
		Logger.log("Memory usage: " + getMem());
	}

	private static String getMem() {
		return (Runtime.getRuntime().totalMemory() - Runtime.getRuntime()
				.freeMemory())
				/ 1024L
				+ " KB used out of "
				+ Runtime.getRuntime().totalMemory()
				/ 1024L
				+ " KB. Max. memory available to the VM is "
				+ Runtime.getRuntime().maxMemory() / 1024L + " KB.";
	}
}
