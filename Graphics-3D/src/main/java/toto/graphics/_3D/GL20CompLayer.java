package toto.graphics._3D;

class GL20CompLayer {
	static String getVertexShaderName() {
		String name = "/defaultVertexShader.src";
		Logger.log("Default vertex shader is: " + name);
		return name;
	}

	static String getFragmentShaderName() {
		String name = "/defaultFragmentShader.src";
		Logger.log("Default fragment shader is: " + name);
		return name;
	}
}
