package toto.graphics._3D;

import java.io.Serializable;

public class Matrix implements Serializable {
	private static Matrix globalTmpMat = new Matrix();
	private static final long serialVersionUID = 2L;
	private static final float pi = 3.141593F;
	private static final float mpi = -3.141593F;
	private static final float pih = 1.570796F;
	private static final float mpih = -1.570796F;
	private static final float spi = (float) Math.sin(3.141592741012573D);

	private static final float mspi = (float) Math.sin(-3.141592741012573D);

	private static final float cpi = (float) Math.cos(3.141592741012573D);

	private static final float mcpi = (float) Math.cos(-3.141592741012573D);

	private static final float spih = (float) Math.sin(1.570796370506287D);

	private static final float mspih = (float) Math.sin(-1.570796370506287D);

	private static final float cpih = (float) Math.cos(1.570796370506287D);

	private static final float mcpih = (float) Math.cos(-1.570796370506287D);
	float[][] mat;
	private float[] mat0;
	private float[] mat1;
	private float[] mat2;
	private float[] mat3;
	private float lastRot = 0.0F;
	private float lastSin = 0.0F;
	private float lastCos = 1.0F;

	public Matrix() {
		this.mat = new float[4][4];

		this.mat0 = this.mat[0];
		this.mat1 = this.mat[1];
		this.mat2 = this.mat[2];
		this.mat3 = this.mat[3];

		this.mat0[0] = 1.0F;
		this.mat1[1] = 1.0F;
		this.mat2[2] = 1.0F;
		this.mat3[3] = 1.0F;
	}

	public Matrix(Matrix m) {
		this.mat = new float[4][4];
		this.mat0 = this.mat[0];
		this.mat1 = this.mat[1];
		this.mat2 = this.mat[2];
		this.mat3 = this.mat[3];
		setTo(m);
	}

	public final boolean isIdentity() {
		return (this.mat0[0] == 1.0F) && (this.mat1[1] == 1.0F)
				&& (this.mat2[2] == 1.0F) && (this.mat3[3] == 1.0F)
				&& (this.mat0[1] == 0.0F) && (this.mat0[2] == 0.0F)
				&& (this.mat0[3] == 0.0F) && (this.mat1[0] == 0.0F)
				&& (this.mat1[2] == 0.0F) && (this.mat1[3] == 0.0F)
				&& (this.mat2[1] == 0.0F) && (this.mat2[0] == 0.0F)
				&& (this.mat2[3] == 0.0F) && (this.mat3[1] == 0.0F)
				&& (this.mat3[2] == 0.0F) && (this.mat3[0] == 0.0F);
	}

	public final void setIdentity() {
		this.mat0[0] = 1.0F;
		this.mat1[1] = 1.0F;
		this.mat2[2] = 1.0F;
		this.mat3[3] = 1.0F;

		this.mat0[1] = 0.0F;
		this.mat0[2] = 0.0F;
		this.mat0[3] = 0.0F;

		this.mat1[0] = 0.0F;
		this.mat1[2] = 0.0F;
		this.mat1[3] = 0.0F;

		this.mat2[1] = 0.0F;
		this.mat2[0] = 0.0F;
		this.mat2[3] = 0.0F;

		this.mat3[1] = 0.0F;
		this.mat3[2] = 0.0F;
		this.mat3[0] = 0.0F;
	}

	public final void setOrientation(SimpleVector dir, SimpleVector up) {
		setOrientation(dir, up, true);
	}

	final void setOrientation(SimpleVector dir, SimpleVector up, boolean invert) {
		up = up.normalize();
		dir = dir.normalize();

		SimpleVector right = up.calcCross(dir).normalize();

		if (!invert) {
			this.mat[0][0] = right.x;
			this.mat[1][0] = right.y;
			this.mat[2][0] = right.z;
			this.mat[3][0] = 0.0F;

			this.mat[0][1] = up.x;
			this.mat[1][1] = up.y;
			this.mat[2][1] = up.z;
			this.mat[3][1] = 0.0F;

			this.mat[0][2] = dir.x;
			this.mat[1][2] = dir.y;
			this.mat[2][2] = dir.z;
			this.mat[3][2] = 0.0F;

			this.mat[0][3] = 0.0F;
			this.mat[1][3] = 0.0F;
			this.mat[2][3] = 0.0F;
			this.mat[3][3] = 1.0F;
		} else {
			this.mat[0][0] = right.x;
			this.mat[0][1] = right.y;
			this.mat[0][2] = right.z;
			this.mat[0][3] = 0.0F;

			this.mat[1][0] = up.x;
			this.mat[1][1] = up.y;
			this.mat[1][2] = up.z;
			this.mat[1][3] = 0.0F;

			this.mat[2][0] = dir.x;
			this.mat[2][1] = dir.y;
			this.mat[2][2] = dir.z;
			this.mat[2][3] = 0.0F;

			this.mat[3][0] = 0.0F;
			this.mat[3][1] = 0.0F;
			this.mat[3][2] = 0.0F;
			this.mat[3][3] = 1.0F;
		}
	}

	public final void scalarMul(float scalar) {
		this.mat[0][0] *= scalar;
		this.mat[0][1] *= scalar;
		this.mat[0][2] *= scalar;

		this.mat[1][0] *= scalar;
		this.mat[1][1] *= scalar;
		this.mat[1][2] *= scalar;

		this.mat[2][0] *= scalar;
		this.mat[2][1] *= scalar;
		this.mat[2][2] *= scalar;
	}

	public final void matMul(Matrix maty) {
		if (isIdentity()) {
			setTo(maty);
			return;
		}

		if (maty.isIdentity()) {
			return;
		}

		float matTemp00 = this.mat0[0];
		float matTemp01 = this.mat0[1];
		float matTemp02 = this.mat0[2];
		float matTemp03 = this.mat0[3];
		float matTemp10 = this.mat1[0];
		float matTemp11 = this.mat1[1];
		float matTemp12 = this.mat1[2];
		float matTemp13 = this.mat1[3];
		float matTemp20 = this.mat2[0];
		float matTemp21 = this.mat2[1];
		float matTemp22 = this.mat2[2];
		float matTemp23 = this.mat2[3];
		float matTemp30 = this.mat3[0];
		float matTemp31 = this.mat3[1];
		float matTemp32 = this.mat3[2];
		float matTemp33 = this.mat3[3];

		float[] matymat0 = maty.mat0;
		float[] matymat1 = maty.mat1;
		float[] matymat2 = maty.mat2;
		float[] matymat3 = maty.mat3;

		float mat200 = matymat0[0];
		float mat201 = matymat0[1];
		float mat202 = matymat0[2];
		float mat203 = matymat0[3];
		float mat210 = matymat1[0];
		float mat211 = matymat1[1];
		float mat212 = matymat1[2];
		float mat213 = matymat1[3];
		float mat220 = matymat2[0];
		float mat221 = matymat2[1];
		float mat222 = matymat2[2];
		float mat223 = matymat2[3];
		float mat230 = matymat3[0];
		float mat231 = matymat3[1];
		float mat232 = matymat3[2];
		float mat233 = matymat3[3];

		float[] mat0 = this.mat0;
		float[] mat1 = this.mat1;
		float[] mat2 = this.mat2;
		float[] mat3 = this.mat3;

		mat0[0] = (matTemp00 * mat200 + matTemp01 * mat210 + matTemp02 * mat220 + matTemp03
				* mat230);
		mat0[1] = (matTemp00 * mat201 + matTemp01 * mat211 + matTemp02 * mat221 + matTemp03
				* mat231);
		mat0[2] = (matTemp00 * mat202 + matTemp01 * mat212 + matTemp02 * mat222 + matTemp03
				* mat232);
		mat0[3] = (matTemp00 * mat203 + matTemp01 * mat213 + matTemp02 * mat223 + matTemp03
				* mat233);
		mat1[0] = (matTemp10 * mat200 + matTemp11 * mat210 + matTemp12 * mat220 + matTemp13
				* mat230);
		mat1[1] = (matTemp10 * mat201 + matTemp11 * mat211 + matTemp12 * mat221 + matTemp13
				* mat231);
		mat1[2] = (matTemp10 * mat202 + matTemp11 * mat212 + matTemp12 * mat222 + matTemp13
				* mat232);
		mat1[3] = (matTemp10 * mat203 + matTemp11 * mat213 + matTemp12 * mat223 + matTemp13
				* mat233);
		mat2[0] = (matTemp20 * mat200 + matTemp21 * mat210 + matTemp22 * mat220 + matTemp23
				* mat230);
		mat2[1] = (matTemp20 * mat201 + matTemp21 * mat211 + matTemp22 * mat221 + matTemp23
				* mat231);
		mat2[2] = (matTemp20 * mat202 + matTemp21 * mat212 + matTemp22 * mat222 + matTemp23
				* mat232);
		mat2[3] = (matTemp20 * mat203 + matTemp21 * mat213 + matTemp22 * mat223 + matTemp23
				* mat233);
		mat3[0] = (matTemp30 * mat200 + matTemp31 * mat210 + matTemp32 * mat220 + matTemp33
				* mat230);
		mat3[1] = (matTemp30 * mat201 + matTemp31 * mat211 + matTemp32 * mat221 + matTemp33
				* mat231);
		mat3[2] = (matTemp30 * mat202 + matTemp31 * mat212 + matTemp32 * mat222 + matTemp33
				* mat232);
		mat3[3] = (matTemp30 * mat203 + matTemp31 * mat213 + matTemp32 * mat223 + matTemp33
				* mat233);
	}

	public final void rotateX(float w) {
		float sw;
		float cw;

		if (w == 3.141593F) {
			cw = cpi;
			sw = spi;
		} else {

			if (w == -3.141593F) {
				cw = mcpi;
				sw = mspi;
			} else {

				if (w == 1.570796F) {
					cw = cpih;
					sw = spih;
				} else {

					if (w == -1.570796F) {
						cw = mcpih;
						sw = mspih;
					} else {
						if (w != this.lastRot) {
							this.lastRot = w;
							this.lastSin = ((float) Math.sin(w));
							this.lastCos = ((float) Math.cos(w));
						}

						cw = this.lastCos;
						sw = this.lastSin;
					}
				}
			}
		}

		float s01 = this.mat0[1];
		float s02 = this.mat0[2];
		float s11 = this.mat1[1];
		float s12 = this.mat1[2];
		float s21 = this.mat2[1];
		float s22 = this.mat2[2];
		float s31 = this.mat3[1];
		float s32 = this.mat3[2];

		this.mat0[1] = (s01 * cw + s02 * sw);
		this.mat0[2] = (s01 * -sw + s02 * cw);
		this.mat1[1] = (s11 * cw + s12 * sw);
		this.mat1[2] = (s11 * -sw + s12 * cw);
		this.mat2[1] = (s21 * cw + s22 * sw);
		this.mat2[2] = (s21 * -sw + s22 * cw);
		this.mat3[1] = (s31 * cw + s32 * sw);
		this.mat3[2] = (s31 * -sw + s32 * cw);
	}

	public final void rotateY(float w) {
		float sw;
		float cw;

		if (w == 3.141593F) {
			cw = cpi;
			sw = spi;
		} else {

			if (w == -3.141593F) {
				cw = mcpi;
				sw = mspi;
			} else {

				if (w == 1.570796F) {
					cw = cpih;
					sw = spih;
				} else {

					if (w == -1.570796F) {
						cw = mcpih;
						sw = mspih;
					} else {
						if (w != this.lastRot) {
							this.lastRot = w;
							this.lastSin = ((float) Math.sin(w));
							this.lastCos = ((float) Math.cos(w));
						}

						cw = this.lastCos;
						sw = this.lastSin;
					}
				}
			}
		}

		float s00 = this.mat0[0];
		float s02 = this.mat0[2];
		float s10 = this.mat1[0];
		float s12 = this.mat1[2];
		float s20 = this.mat2[0];
		float s22 = this.mat2[2];
		float s30 = this.mat3[0];
		float s32 = this.mat3[2];

		this.mat0[0] = (s00 * cw + s02 * sw);
		this.mat0[2] = (s00 * -sw + s02 * cw);
		this.mat1[0] = (s10 * cw + s12 * sw);
		this.mat1[2] = (s10 * -sw + s12 * cw);
		this.mat2[0] = (s20 * cw + s22 * sw);
		this.mat2[2] = (s20 * -sw + s22 * cw);
		this.mat3[0] = (s30 * cw + s32 * sw);
		this.mat3[2] = (s30 * -sw + s32 * cw);
	}

	public final void rotateZ(float w) {
		float sw;
		float cw;

		if (w == 3.141593F) {
			cw = cpi;
			sw = spi;
		} else {

			if (w == -3.141593F) {
				cw = mcpi;
				sw = mspi;
			} else {

				if (w == 1.570796F) {
					cw = cpih;
					sw = spih;
				} else {

					if (w == -1.570796F) {
						cw = mcpih;
						sw = mspih;
					} else {
						if (w != this.lastRot) {
							this.lastRot = w;
							this.lastSin = ((float) Math.sin(w));
							this.lastCos = ((float) Math.cos(w));
						}

						cw = this.lastCos;
						sw = this.lastSin;
					}
				}
			}
		}

		float s00 = this.mat0[0];
		float s01 = this.mat0[1];
		float s10 = this.mat1[0];
		float s11 = this.mat1[1];
		float s20 = this.mat2[0];
		float s21 = this.mat2[1];
		float s30 = this.mat3[0];
		float s31 = this.mat3[1];

		this.mat0[0] = (s00 * cw + s01 * sw);
		this.mat0[1] = (s00 * -sw + s01 * cw);
		this.mat1[0] = (s10 * cw + s11 * sw);
		this.mat1[1] = (s10 * -sw + s11 * cw);
		this.mat2[0] = (s20 * cw + s21 * sw);
		this.mat2[1] = (s20 * -sw + s21 * cw);
		this.mat3[0] = (s30 * cw + s31 * sw);
		this.mat3[1] = (s30 * -sw + s31 * cw);
	}

	public final void rotateAxis(SimpleVector axis, float angle) {
		if (angle != this.lastRot) {
			this.lastRot = angle;
			this.lastSin = ((float) Math.sin(angle));
			this.lastCos = ((float) Math.cos(angle));
		}

		float c = this.lastCos;
		float s = this.lastSin;
		float t = 1.0F - c;

		axis = axis.normalize(axis);

		float x = axis.x;
		float y = axis.y;
		float z = axis.z;

		synchronized (globalTmpMat) {
			Matrix mat = globalTmpMat;
			mat.setIdentity();

			float sy = s * y;
			float sx = s * x;
			float sz = s * z;

			float txy = t * x * y;
			float txz = t * x * z;
			float tyz = t * y * z;

			mat.mat0[0] = (t * x * x + c);
			mat.mat1[0] = (txy + sz);
			mat.mat2[0] = (txz - sy);

			mat.mat0[1] = (txy - sz);
			mat.mat1[1] = (t * y * y + c);
			mat.mat2[1] = (tyz + sx);

			mat.mat0[2] = (txz + sy);
			mat.mat1[2] = (tyz - sx);
			mat.mat2[2] = (t * z * z + c);

			mat.orthonormalize();
			matMul(mat);
		}
	}

	public final void interpolate(Matrix source, Matrix dest, float weight) {
		if (weight > 1.0F) {
			weight = 1.0F;
		} else if (weight < 0.0F) {
			weight = 0.0F;
		}

		float antiWeight = 1.0F - weight;
		for (int i = 0; i < 4; i++) {
			this.mat[i][0] = (source.mat[i][0] * antiWeight + dest.mat[i][0]
					* weight);
			this.mat[i][1] = (source.mat[i][1] * antiWeight + dest.mat[i][1]
					* weight);
			this.mat[i][2] = (source.mat[i][2] * antiWeight + dest.mat[i][2]
					* weight);
			this.mat[i][3] = (source.mat[i][3] * antiWeight + dest.mat[i][3]
					* weight);
		}
		orthonormalize();
	}

	public final SimpleVector getTranslation() {
		return SimpleVector.create(this.mat3[0], this.mat3[1], this.mat3[2]);
	}

	public final SimpleVector getXAxis() {
		return SimpleVector.create(this.mat0[0], this.mat0[1], this.mat0[2]);
	}

	public final SimpleVector getYAxis() {
		return SimpleVector.create(this.mat1[0], this.mat1[1], this.mat1[2]);
	}

	public final SimpleVector getZAxis() {
		return SimpleVector.create(this.mat2[0], this.mat2[1], this.mat2[2]);
	}

	public final SimpleVector getXAxis(SimpleVector toFill) {
		toFill.set(this.mat0[0], this.mat0[1], this.mat0[2]);
		return toFill;
	}

	public final SimpleVector getYAxis(SimpleVector toFill) {
		toFill.set(this.mat1[0], this.mat1[1], this.mat1[2]);
		return toFill;
	}

	public final SimpleVector getZAxis(SimpleVector toFill) {
		toFill.set(this.mat2[0], this.mat2[1], this.mat2[2]);
		return toFill;
	}

	public final float get(int row, int col) {
		if ((row >= 0) && (row <= 4) && (col >= 0) && (col <= 4)) {
			return this.mat[row][col];
		}
		return 0.0F;
	}

	public final void translate(SimpleVector trans) {
		this.mat3[0] += trans.x;
		this.mat3[1] += trans.y;
		this.mat3[2] += trans.z;
	}

	public final void translate(float x, float y, float z) {
		this.mat3[0] += x;
		this.mat3[1] += y;
		this.mat3[2] += z;
	}

	public final Matrix cloneMatrix() {
		Matrix tmpMat = new Matrix();
		tmpMat.setTo(this);
		return tmpMat;
	}

	public final Matrix invert() {
		return invert(new Matrix());
	}

	public final Matrix invert(Matrix dst) {
		float srcmat00 = this.mat0[0];
		float srcmat10 = this.mat0[1];
		float srcmat20 = this.mat0[2];
		float srcmat30 = this.mat0[3];
		float srcmat01 = this.mat1[0];
		float srcmat11 = this.mat1[1];
		float srcmat21 = this.mat1[2];
		float srcmat31 = this.mat1[3];
		float srcmat02 = this.mat2[0];
		float srcmat12 = this.mat2[1];
		float srcmat22 = this.mat2[2];
		float srcmat32 = this.mat2[3];
		float srcmat03 = this.mat3[0];
		float srcmat13 = this.mat3[1];
		float srcmat23 = this.mat3[2];
		float srcmat33 = this.mat3[3];

		float tmpmat00 = srcmat22 * srcmat33;
		float tmpmat01 = srcmat23 * srcmat32;
		float tmpmat02 = srcmat21 * srcmat33;
		float tmpmat03 = srcmat23 * srcmat31;
		float tmpmat10 = srcmat21 * srcmat32;
		float tmpmat11 = srcmat22 * srcmat31;
		float tmpmat12 = srcmat20 * srcmat33;
		float tmpmat13 = srcmat23 * srcmat30;
		float tmpmat20 = srcmat20 * srcmat32;
		float tmpmat21 = srcmat22 * srcmat30;
		float tmpmat22 = srcmat20 * srcmat31;
		float tmpmat23 = srcmat21 * srcmat30;

		float dstmat00 = tmpmat00
				* srcmat11
				+ tmpmat03
				* srcmat12
				+ tmpmat10
				* srcmat13
				- (tmpmat01 * srcmat11 + tmpmat02 * srcmat12 + tmpmat11
						* srcmat13);
		float dstmat01 = tmpmat01
				* srcmat10
				+ tmpmat12
				* srcmat12
				+ tmpmat21
				* srcmat13
				- (tmpmat00 * srcmat10 + tmpmat13 * srcmat12 + tmpmat20
						* srcmat13);
		float dstmat02 = tmpmat02
				* srcmat10
				+ tmpmat13
				* srcmat11
				+ tmpmat22
				* srcmat13
				- (tmpmat03 * srcmat10 + tmpmat12 * srcmat11 + tmpmat23
						* srcmat13);
		float dstmat03 = tmpmat11
				* srcmat10
				+ tmpmat20
				* srcmat11
				+ tmpmat23
				* srcmat12
				- (tmpmat10 * srcmat10 + tmpmat21 * srcmat11 + tmpmat22
						* srcmat12);
		float dstmat10 = tmpmat01
				* srcmat01
				+ tmpmat02
				* srcmat02
				+ tmpmat11
				* srcmat03
				- (tmpmat00 * srcmat01 + tmpmat03 * srcmat02 + tmpmat10
						* srcmat03);
		float dstmat11 = tmpmat00
				* srcmat00
				+ tmpmat13
				* srcmat02
				+ tmpmat20
				* srcmat03
				- (tmpmat01 * srcmat00 + tmpmat12 * srcmat02 + tmpmat21
						* srcmat03);
		float dstmat12 = tmpmat03
				* srcmat00
				+ tmpmat12
				* srcmat01
				+ tmpmat23
				* srcmat03
				- (tmpmat02 * srcmat00 + tmpmat13 * srcmat01 + tmpmat22
						* srcmat03);
		float dstmat13 = tmpmat10
				* srcmat00
				+ tmpmat21
				* srcmat01
				+ tmpmat22
				* srcmat02
				- (tmpmat11 * srcmat00 + tmpmat20 * srcmat01 + tmpmat23
						* srcmat02);

		tmpmat00 = srcmat02 * srcmat13;
		tmpmat01 = srcmat03 * srcmat12;
		tmpmat02 = srcmat01 * srcmat13;
		tmpmat03 = srcmat03 * srcmat11;
		tmpmat10 = srcmat01 * srcmat12;
		tmpmat11 = srcmat02 * srcmat11;
		tmpmat12 = srcmat00 * srcmat13;
		tmpmat13 = srcmat03 * srcmat10;
		tmpmat20 = srcmat00 * srcmat12;
		tmpmat21 = srcmat02 * srcmat10;
		tmpmat22 = srcmat00 * srcmat11;
		tmpmat23 = srcmat01 * srcmat10;

		float dstmat20 = tmpmat00
				* srcmat31
				+ tmpmat03
				* srcmat32
				+ tmpmat10
				* srcmat33
				- (tmpmat01 * srcmat31 + tmpmat02 * srcmat32 + tmpmat11
						* srcmat33);
		float dstmat21 = tmpmat01
				* srcmat30
				+ tmpmat12
				* srcmat32
				+ tmpmat21
				* srcmat33
				- (tmpmat00 * srcmat30 + tmpmat13 * srcmat32 + tmpmat20
						* srcmat33);
		float dstmat22 = tmpmat02
				* srcmat30
				+ tmpmat13
				* srcmat31
				+ tmpmat22
				* srcmat33
				- (tmpmat03 * srcmat30 + tmpmat12 * srcmat31 + tmpmat23
						* srcmat33);
		float dstmat23 = tmpmat11
				* srcmat30
				+ tmpmat20
				* srcmat31
				+ tmpmat23
				* srcmat32
				- (tmpmat10 * srcmat30 + tmpmat21 * srcmat31 + tmpmat22
						* srcmat32);
		float dstmat30 = tmpmat02
				* srcmat22
				+ tmpmat11
				* srcmat23
				+ tmpmat01
				* srcmat21
				- (tmpmat10 * srcmat23 + tmpmat00 * srcmat21 + tmpmat03
						* srcmat22);
		float dstmat31 = tmpmat20
				* srcmat23
				+ tmpmat00
				* srcmat20
				+ tmpmat13
				* srcmat22
				- (tmpmat12 * srcmat22 + tmpmat21 * srcmat23 + tmpmat01
						* srcmat20);
		float dstmat32 = tmpmat12
				* srcmat21
				+ tmpmat23
				* srcmat23
				+ tmpmat03
				* srcmat20
				- (tmpmat22 * srcmat23 + tmpmat02 * srcmat20 + tmpmat13
						* srcmat21);
		float dstmat33 = tmpmat22
				* srcmat22
				+ tmpmat10
				* srcmat20
				+ tmpmat21
				* srcmat21
				- (tmpmat20 * srcmat21 + tmpmat23 * srcmat22 + tmpmat11
						* srcmat20);

		float det = 1.0F / (srcmat00 * dstmat00 + srcmat01 * dstmat01
				+ srcmat02 * dstmat02 + srcmat03 * dstmat03);

		dst.mat0[0] = (dstmat00 * det);
		dst.mat0[1] = (dstmat01 * det);
		dst.mat0[2] = (dstmat02 * det);
		dst.mat0[3] = (dstmat03 * det);
		dst.mat1[0] = (dstmat10 * det);
		dst.mat1[1] = (dstmat11 * det);
		dst.mat1[2] = (dstmat12 * det);
		dst.mat1[3] = (dstmat13 * det);
		dst.mat2[0] = (dstmat20 * det);
		dst.mat2[1] = (dstmat21 * det);
		dst.mat2[2] = (dstmat22 * det);
		dst.mat2[3] = (dstmat23 * det);
		dst.mat3[0] = (dstmat30 * det);
		dst.mat3[1] = (dstmat31 * det);
		dst.mat3[2] = (dstmat32 * det);
		dst.mat3[3] = (dstmat33 * det);

		return dst;
	}

	public final Matrix invert3x3() {
		return invert3x3(new Matrix());
	}

	public final Matrix invert3x3(Matrix toFill) {
		toFill.mat0[1] = this.mat1[0];
		toFill.mat0[2] = this.mat2[0];
		toFill.mat1[0] = this.mat0[1];
		toFill.mat1[2] = this.mat2[1];
		toFill.mat2[0] = this.mat0[2];
		toFill.mat2[1] = this.mat1[2];
		toFill.mat0[0] = this.mat0[0];
		toFill.mat1[1] = this.mat1[1];
		toFill.mat2[2] = this.mat2[2];
		return toFill;
	}

	public final void orthonormalize() {
		float ux = 0.0F;
		float uy = 0.0F;
		float uz = 0.0F;

		float vx = 0.0F;
		float vy = 0.0F;
		float vz = 0.0F;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < i; j++) {
				ux = this.mat0[i];
				uy = this.mat1[i];
				uz = this.mat2[i];

				vx = this.mat0[j];
				vy = this.mat1[j];
				vz = this.mat2[j];

				float ut = ux * vx + uy * vy + uz * vz;
				this.mat0[j] -= ux * ut;
				this.mat1[j] -= uy * ut;
				this.mat2[j] -= uz * ut;
			}

			ux = this.mat0[i];
			uy = this.mat1[i];
			uz = this.mat2[i];

			float vt = 1.0F / (float) Math.sqrt(ux * ux + uy * uy + uz * uz);
			this.mat0[i] *= vt;
			this.mat1[i] *= vt;
			this.mat2[i] *= vt;
		}
	}

	public final float[] getDump() {
		return fillDump(null);
	}

	public final float[] fillDump(float[] dump) {
		if (dump == null) {
			dump = new float[16];
		} else if (dump.length != 16) {
			Logger.log("Dump array has to have a length of 16!", 0);
		}

		int cnt = 0;
		dump[(cnt++)] = this.mat0[0];
		dump[(cnt++)] = this.mat0[1];
		dump[(cnt++)] = this.mat0[2];
		dump[(cnt++)] = this.mat0[3];
		dump[(cnt++)] = this.mat1[0];
		dump[(cnt++)] = this.mat1[1];
		dump[(cnt++)] = this.mat1[2];
		dump[(cnt++)] = this.mat1[3];
		dump[(cnt++)] = this.mat2[0];
		dump[(cnt++)] = this.mat2[1];
		dump[(cnt++)] = this.mat2[2];
		dump[(cnt++)] = this.mat2[3];
		dump[(cnt++)] = this.mat3[0];
		dump[(cnt++)] = this.mat3[1];
		dump[(cnt++)] = this.mat3[2];
		dump[cnt] = this.mat3[3];

		return dump;
	}

	public final void setDump(float[] dump) {
		if (dump.length == 16) {
			int cnt = 0;
			this.mat0[0] = dump[(cnt++)];
			this.mat0[1] = dump[(cnt++)];
			this.mat0[2] = dump[(cnt++)];
			this.mat0[3] = dump[(cnt++)];
			this.mat1[0] = dump[(cnt++)];
			this.mat1[1] = dump[(cnt++)];
			this.mat1[2] = dump[(cnt++)];
			this.mat1[3] = dump[(cnt++)];
			this.mat2[0] = dump[(cnt++)];
			this.mat2[1] = dump[(cnt++)];
			this.mat2[2] = dump[(cnt++)];
			this.mat2[3] = dump[(cnt++)];
			this.mat3[0] = dump[(cnt++)];
			this.mat3[1] = dump[(cnt++)];
			this.mat3[2] = dump[(cnt++)];
			this.mat3[3] = dump[cnt];
		} else {
			Logger.log("Not a valid matrix dump!", 0);
		}
	}

	public final void setTo(Matrix source) {
		float[] thismat = this.mat[0];
		float[] sourcemat = source.mat[0];
		thismat[0] = sourcemat[0];
		thismat[1] = sourcemat[1];
		thismat[2] = sourcemat[2];
		thismat[3] = sourcemat[3];

		thismat = this.mat[1];
		sourcemat = source.mat[1];
		thismat[0] = sourcemat[0];
		thismat[1] = sourcemat[1];
		thismat[2] = sourcemat[2];
		thismat[3] = sourcemat[3];

		thismat = this.mat[2];
		sourcemat = source.mat[2];
		thismat[0] = sourcemat[0];
		thismat[1] = sourcemat[1];
		thismat[2] = sourcemat[2];
		thismat[3] = sourcemat[3];

		thismat = this.mat[3];
		sourcemat = source.mat[3];
		thismat[0] = sourcemat[0];
		thismat[1] = sourcemat[1];
		thismat[2] = sourcemat[2];
		thismat[3] = sourcemat[3];
	}

	public final void set(int row, int col, float value) {
		if ((row >= 0) && (row <= 4) && (col >= 0) && (col <= 4))
			this.mat[row][col] = value;
	}

	public final void setRow(int row, float v1, float v2, float v3, float v4) {
		if ((row >= 0) && (row <= 4)) {
			this.mat[row][0] = v1;
			this.mat[row][1] = v2;
			this.mat[row][2] = v3;
			this.mat[row][3] = v4;
		}
	}

	public final void setColumn(int col, float v1, float v2, float v3, float v4) {
		if ((col >= 0) && (col <= 4)) {
			this.mat0[col] = v1;
			this.mat1[col] = v2;
			this.mat2[col] = v3;
			this.mat3[col] = v4;
		}
	}

	public String toString() {
		String m = "(\n";
		for (int i = 0; i < 4; i++) {
			m = m + "\t" + this.mat[i][0];
			m = m + "\t" + this.mat[i][1];
			m = m + "\t" + this.mat[i][2];
			m = m + "\t" + this.mat[i][3] + "\n";
		}
		m = m + ")\n";
		return m;
	}

	public boolean equals(Object obj) {
		if ((obj instanceof Matrix)) {
			Matrix m2 = (Matrix) obj;
			for (int i = 0; i < 4; i++) {
				if (m2.mat[i][0] != this.mat[i][0]) {
					return false;
				}
				if (m2.mat[i][1] != this.mat[i][1]) {
					return false;
				}
				if (m2.mat[i][2] != this.mat[i][2]) {
					return false;
				}
				if (m2.mat[i][3] != this.mat[i][3]) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public final Matrix transpose() {
		Matrix dst = new Matrix();
		dst.mat0[1] = this.mat1[0];
		dst.mat0[2] = this.mat2[0];
		dst.mat0[3] = this.mat3[0];
		dst.mat1[0] = this.mat0[1];
		dst.mat1[2] = this.mat2[1];
		dst.mat1[3] = this.mat3[1];
		dst.mat2[0] = this.mat0[2];
		dst.mat2[1] = this.mat1[2];
		dst.mat2[3] = this.mat3[2];
		dst.mat3[0] = this.mat0[3];
		dst.mat3[1] = this.mat1[3];
		dst.mat3[2] = this.mat2[3];
		dst.mat0[0] = this.mat0[0];
		dst.mat1[1] = this.mat1[1];
		dst.mat2[2] = this.mat2[2];
		dst.mat3[3] = this.mat3[3];
		return dst;
	}

	public void transformToGL() {
		this.mat0[1] *= -1.0F;
		this.mat0[2] *= -1.0F;

		this.mat1[1] *= -1.0F;
		this.mat1[2] *= -1.0F;

		this.mat2[1] *= -1.0F;
		this.mat2[2] *= -1.0F;

		this.mat3[1] *= -1.0F;
		this.mat3[2] *= -1.0F;
	}
}
