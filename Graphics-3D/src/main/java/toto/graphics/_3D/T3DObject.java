package toto.graphics._3D;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import toto.graphics.color.RGBColor;

/**
 * A class for holding information about a 3D object.
 * <p>
 * Rage3D objects can be connected by defining their parents to build
 * hierarchies. A Rage3DObject once created, can be added to a
 * {@link toto.graphics._3D.T3DScene}
 * </p>
 * 
 * @see toto.graphics._3D.T3DScene
 * @see #build()
 */
public class T3DObject implements Serializable {

	private static final long serialVersionUID = 3L;

	public static final boolean ENVMAP_ENABLED = true;
	public static final boolean ENVMAP_DISABLED = false;
	public static final boolean BILLBOARDING_ENABLED = true;
	public static final boolean BILLBOARDING_DISABLED = false;
	public static final boolean CULLING_ENABLED = true;
	public static final boolean CULLING_DISABLED = false;
	public static final boolean SPECULAR_ENABLED = true;
	public static final boolean SPECULAR_DISABLED = false;
	public static final boolean OBJ_VISIBLE = true;
	public static final boolean OBJ_INVISIBLE = false;
	public static final int COLLISION_MODE_NONE = 0;
	public static final int COLLISION_MODE_OTHERS = 1;
	public static final int COLLISION_MODE_SELF = 2;
	public static final float COLLISION_NONE = 1.0E+012F;
	public static final float RAY_MISSES_BOX = 1.0E+012F;
	public static final boolean COLLISION_DETECTION_OPTIMIZED = true;
	public static final boolean COLLISION_DETECTION_NOT_OPTIMIZED = false;

	public static final int ELLIPSOID_ALIGNED = 0;
	public static final int ELLIPSOID_TRANSFORMED = 1;
	public static final int UNKNOWN_OBJECTSIZE = -1;
	public static final int NO_OBJECT = -100;
	private static final Lights DUMMY_LIGHTS = new Lights(0);
	private static final float INSIDE_POLYGON_CONST = 6.220354F;
	private static final float EPSILON = 1.0E-009F;
	private static int nextID = 0;

	static int globalListenerCount = 0;
	private static ArrayList<float[]> lightsList = new ArrayList();
	private static float[][] litData = new float[8][2];
	private static transient Matrix[] matrixArray = null;
	private static transient HashMap<Long, Matrix[]> matrixThreadCache = null;
	private static SimpleVector tempTC = new SimpleVector();
	private static Matrix mat7 = new Matrix();
	private static Matrix mat71 = new Matrix();
	private static Matrix matBill = null;
	boolean isTrans;
	TransparencyMode transMode = TransparencyMode.DEFAULT;
	boolean isEnvmapped;
	boolean isVisible;
	boolean isLit;
	boolean isPotentialCollider;
	boolean mayCollide;
	boolean wasCollider;
	float sortOffset = 0.0F;
	boolean isStatic;
	Vectors objVectors;
	Mesh objMesh;
	T3DScene myRage3DScene;
	int number = 0;

	String name = "";
	int[] texture;
	int[][] multiTex;
	int[][] multiMode;
	int maxStagesUsed = 0;

	boolean usesMultiTexturing = false;
	T3DObject[] parent;
	int parentCnt = 0;
	int transValue;
	float centerX;
	float centerY;
	float centerZ;
	boolean hasBoundingBox;
	boolean isFlatShaded;
	boolean object3DRendered;
	transient ArrayList<CompiledInstance> compiled = null;
	boolean dynamic = false;
	boolean modified = false;
	boolean indexed = true;
	boolean forcedIndexed = false;
	int batchSize = -1;
	boolean staticUV = true;
	transient float[][] nearestLights = null;
	boolean toStrip = false;
	transient IRenderHook renderHook = null;

	boolean sharing = false;
	T3DObject shareWith = null;

	Matrix transBuffer = new Matrix();

	ArrayList<CollisionListener> collisionListener = null;

	boolean disableListeners = false;

	private int[] polygonIDs = null;

	private int pIDCount = 0;

	private int lastAddedID = -1;
	private int lowestPos;
	private int highestPos;
	float xRotationCenter;
	float yRotationCenter;
	float zRotationCenter;
	transient GLSLShader shader;
	private int addColorR;
	private int addColorG;
	private int addColorB;
	private Matrix rotationMatrix = new Matrix();

	private Matrix translationMatrix = new Matrix();

	private Matrix originMatrix = new Matrix();

	private Matrix mat2 = new Matrix();

	private Matrix mat5 = new Matrix();

	private Matrix mat6 = new Matrix();
	boolean doCulling;
	boolean doSpecularLighting;
	Animation anim;
	boolean lazyTransforms;
	private boolean neverOptimize = false;
	private float scaleFactor;
	private boolean isBillBoard;
	private OcTree ocTree;
	private Matrix transCache = null;

	private Matrix invCache = null;

	private Matrix transCacheDump = null;
	private Matrix invCacheDump = null;
	private boolean optimizeColDet = false;
	private float largestPolygonSize = -1.0F;
	private transient PolygonManager polyManager = null;
	private EllipsoidMode ellipsoidMode = EllipsoidMode.ELLIPSOID_ALIGNED;
	private Object userObj = null;
	boolean reverseCulling = false;
	boolean hasBeenBuild = false;
	boolean hasBeenStripped = false;
	boolean fixedPointMode = true;
	boolean oneTextureSet = false;
	private RGBColor addColorInstance = new RGBColor();
	private transient HashSet<Integer> sectors = null;
	private transient int maxLights = 8;
	Matrix textureMatrix = null;
	boolean skipPivot = false;
	transient Virtualizer virtualizer = null;

	private static transient float[] dirColParam = null;
	private static transient float[] origColParam = null;
	private transient SimpleVector t0Vel = null;
	private transient SimpleVector fromBaseToIntersection = null;
	private transient SimpleVector planeIntersectionPoint = null;
	private transient Plane trianglePlane = null;
	private transient float[] newT = null;
	private RGBColor internalRGBColor = null;

	private void createCollisionArrays() {
		if (dirColParam == null) {
			dirColParam = new float[3];
			origColParam = new float[3];
		}
		if (this.t0Vel == null) {
			this.t0Vel = new SimpleVector();
			this.fromBaseToIntersection = new SimpleVector();
			this.planeIntersectionPoint = new SimpleVector();
			this.trianglePlane = new Plane();
			this.newT = new float[1];
		}
	}

	public static T3DObject createDummyObj() {
		return new T3DObject(0);
	}

	public void setVirtualizer(Virtualizer virtualizer) {
		this.virtualizer = virtualizer;

	}

	public Virtualizer getVirtualizer() {
		return this.virtualizer;

	}

	public void setUserObject(Object obj) {
		this.userObj = obj;

	}

	public Object getUserObject() {
		return this.userObj;

	}

	public void setSortOffset(float offset) {
		this.sortOffset = offset;
	}

	/**
	 * Merge to 3D Objects
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static T3DObject mergeObjects(T3DObject first,
			T3DObject second) {
		if ((first != null) && (second != null)) {
			int triCnt = Math.max(first.objMesh.anzTri + second.objMesh.anzTri,
					(first.objMesh.anzVectors + second.objMesh.anzVectors) / 3);
			T3DObject obj = new T3DObject(triCnt);
			first.appendToObject(obj);
			second.appendToObject(obj);
			obj.objMesh.normalsCalculated = ((first.objMesh.normalsCalculated) && (second.objMesh.normalsCalculated));
			return obj;

		}
		Logger.log("Can't merge null Rage3D Object!", 0);
		return null;
	}

	public static T3DObject mergeAll(T3DObject[] objs) {
		int anzs = 0;
		int anvs = 0;
		for (int i = 0; i < objs.length; i++) {
			T3DObject obj = objs[i];
			anzs += obj.objMesh.anzTri;
			anvs += obj.objMesh.anzVectors;
		}
		anvs /= 3;
		int triCnt = Math.max(anzs, anvs);
		T3DObject target = new T3DObject(triCnt);
		boolean nc = true;
		for (int i = 0; i < objs.length; i++) {
			nc &= objs[i].objMesh.normalsCalculated;
			objs[i].appendToObject(target);
			objs[i] = null;

		}
		target.objMesh.normalsCalculated = nc;
		return target;
	}

	public static void resetNextID() {
		nextID = 0;
	}

	public static int getNextID() {
		return nextID;
	}

	public static void setNextID(int next) {
		if (next >= nextID)
			/* 566 */nextID = next;

		else
			/* 568 */Logger.log(
					"The next ID can't be lower than the current one", 0);

	}

	public T3DObject(float[] coordinates, float[] uvs, int[] indices,
			int textureId) {
		if ((indices == null) && (coordinates.length % 9 != 0)) {
			Logger.log(
					"Without indices, coordinates' length have to be a multiple of 9!",
					0);

		}

		if (indices == null) {
			indices = new int[coordinates.length / 3];
			for (int i = 0; i < indices.length; i++) {
				indices[i] = i;

			}

		}

		int triangleCount = (indices.length + 3) / 2;
		int oldCount = triangleCount;
		if (coordinates.length / 3 > triangleCount) {
			triangleCount = coordinates.length / 3;
			if (oldCount + 100 < triangleCount) {
				Logger.log(
						"Coordinate array is sparsely populated! Consider to pre-process it to save memory!",
						1);
			}
		}

		init(triangleCount);
		this.objMesh.anzCoords = 0;
		for (int i = 0; i < coordinates.length; i += 3) {
			int ii = i / 3;
			this.objMesh.xOrg[ii] = coordinates[i];
			this.objMesh.yOrg[ii] = coordinates[(i + 1)];
			this.objMesh.zOrg[ii] = coordinates[(i + 2)];
		}
		this.objMesh.anzCoords = (coordinates.length / 3);

		if (textureId == -1) {
			textureId = 0;
		}
		int anzVec = 0;
		int posTri = 0;
		for (int i = 0; i < indices.length; i += 3) {
			for (int p = 0; p < 3; p++) {
				if (uvs != null) {
					float u = uvs[(2 * indices[(i + p)])];
					float v = uvs[(2 * indices[(i + p)] + 1)];
					this.objVectors.nuOrg[anzVec] = u;
					this.objVectors.nvOrg[anzVec] = v;
				}

				this.objMesh.coords[anzVec] = indices[(i + p)];
				this.objMesh.points[posTri][p] = anzVec;
				anzVec++;
				this.objMesh.anzVectors += 1;
			}

			this.texture[posTri] = textureId;

			posTri++;
			this.objMesh.anzTri += 1;

		}
		this.objMesh.compress();

	}

	public T3DObject(int maxTriangles) {
		init(maxTriangles);
	}

	public T3DObject(T3DObject obj) {
		this(obj, false);

	}

	public T3DObject(T3DObject obj, boolean reuseMesh) {
		int p = 0;
		if ((obj != null) && (obj.texture != null)) {
			p = obj.texture.length;

		}

		init(p + 8);
		if (p != 0)
			/* 698 */copy(obj, reuseMesh);

	}

	private void copy(T3DObject obj, boolean reuseMesh) {
		if (reuseMesh) {
			if (obj.objMesh.nxOrg == null) {
				Logger.log(
						"Can't use a mesh for a new object that has already been stripped!",
						0);
				return;

			}
			this.objMesh = obj.objMesh;

		} else {
			this.objMesh = obj.objMesh.cloneMesh(true);

		}

		if (obj.hasVertexAlpha()) {
			this.objVectors.createAlpha();
			System.arraycopy(obj.objVectors.alpha, 0, this.objVectors.alpha, 0,
					obj.objVectors.alpha.length);

		}

		int end = obj.objVectors.maxVectors;
		Vectors objobjVectors = obj.objVectors;

		for (int i = 0; i < end; i++) {
			this.objVectors.nuOrg[i] = objobjVectors.nuOrg[i];
			this.objVectors.nvOrg[i] = objobjVectors.nvOrg[i];

		}

		if (obj.objVectors.uMul != null) {
			this.objVectors.createMultiCoords();
			int min = Math.min(objobjVectors.uMul.length,
					this.objVectors.uMul.length);
			for (int i = 0; i < min; i++) {
				for (int p = 0; p < obj.objVectors.maxVectors; p++) {
					this.objVectors.uMul[i][p] = objobjVectors.uMul[i][p];
					this.objVectors.vMul[i][p] = objobjVectors.vMul[i][p];

				}

			}

		}

		this.maxStagesUsed = obj.maxStagesUsed;
		this.usesMultiTexturing = obj.usesMultiTexturing;
		this.objMesh.normalsCalculated = obj.objMesh.normalsCalculated;
		this.objVectors.setMesh(this.objMesh);
		this.isTrans = obj.isTrans;
		this.transMode = obj.transMode;
		this.isEnvmapped = obj.isEnvmapped;
		this.isVisible = obj.isVisible;
		this.isStatic = obj.isStatic;
		this.myRage3DScene = obj.myRage3DScene;
		this.lowestPos = obj.lowestPos;
		this.highestPos = obj.highestPos;
		this.lazyTransforms = obj.lazyTransforms;
		this.optimizeColDet = obj.optimizeColDet;
		this.largestPolygonSize = obj.largestPolygonSize;
		this.isBillBoard = obj.isBillBoard;
		this.skipPivot = obj.skipPivot;
		if (obj.multiTex != null) {
			if (this.multiTex == null) {
				this.multiTex = new int[Config.maxTextureLayers - 1][this.texture.length];
				this.multiMode = new int[Config.maxTextureLayers - 1][this.texture.length];

			}

			int min = Math.min(obj.multiTex.length, this.multiTex.length);

			for (int i = 0; i < min; i++) {
				System.arraycopy(obj.multiTex[i], 0, this.multiTex[i], 0,
						obj.multiTex[i].length);
				System.arraycopy(obj.multiMode[i], 0, this.multiMode[i], 0,
						obj.multiMode[i].length);
			}

			for (int i = min; i < this.multiTex.length; i++) {
				int e = this.multiTex[i].length;
				for (int ii = 0; ii < e; ii++) {
					this.multiTex[i][ii] = -1;

				}

			}

		}

		System.arraycopy(obj.texture, 0, this.texture, 0, obj.texture.length);
		System.arraycopy(obj.parent, 0, this.parent, 0, obj.parent.length);
		this.transValue = obj.transValue;
		this.xRotationCenter = obj.xRotationCenter;
		this.yRotationCenter = obj.yRotationCenter;
		this.zRotationCenter = obj.zRotationCenter;
		this.centerX = obj.centerX;
		this.centerY = obj.centerY;
		this.centerZ = obj.centerZ;
		this.hasBoundingBox = obj.hasBoundingBox;
		this.addColorR = obj.addColorR;
		this.addColorG = obj.addColorG;
		this.addColorB = obj.addColorB;
		this.addColorInstance = obj.addColorInstance;
		this.rotationMatrix = obj.rotationMatrix.cloneMatrix();
		this.translationMatrix = obj.translationMatrix.cloneMatrix();
		this.originMatrix = obj.originMatrix.cloneMatrix();
		this.doCulling = obj.doCulling;
		this.anim = obj.anim;
		this.userObj = obj.userObj;
		this.oneTextureSet = obj.oneTextureSet;
	}

	private void init(int maxTriangles) {
		if ((maxTriangles != -1) && (maxTriangles != 0)) {
			int cnt = 3 * maxTriangles + 8;
			this.objMesh = new Mesh(cnt);
			this.objVectors = new Vectors(cnt, this.objMesh);
			this.texture = new int[maxTriangles];

		} else {
			this.objMesh = new Mesh(1);
		}

		this.parent = new T3DObject[Config.maxParentObjects];
		this.parentCnt = 0;
		this.object3DRendered = false;
		this.rotationMatrix.setIdentity();
		this.translationMatrix.setIdentity();
		this.originMatrix.setIdentity();
		this.xRotationCenter = 0.0F;
		this.yRotationCenter = 0.0F;
		this.zRotationCenter = 0.0F;
		this.centerX = 0.0F;
		this.centerY = 0.0F;
		this.centerZ = 0.0F;
		this.number = nextID;
		nextID += 1;
		this.name = ("object" + this.number);
		this.objMesh.anzTri = 0;
		this.isPotentialCollider = false;
		this.mayCollide = false;
		this.isBillBoard = false;
		this.isFlatShaded = false;
		this.isLit = true;
		this.lazyTransforms = false;

		this.wasCollider = false;

		this.anim = null;
		this.ocTree = null;
		if (maxTriangles != 0) {
			this.isEnvmapped = false;
			this.transValue = 0;
			this.isTrans = false;
			this.isStatic = false;
			this.isVisible = true;
			this.doCulling = true;
			this.doSpecularLighting = false;
			this.addColorR = 0;
			this.addColorG = 0;
			this.addColorB = 0;
			this.addColorInstance = new RGBColor();
			this.hasBoundingBox = false;

		}
		this.scaleFactor = 1.0F;

	}

	public void compile() {
		/* 883 */
		compile(false, true);

	}

	public void compile(boolean dynamic) {
		/* 895 */
		compile(dynamic, true);

	}

	public void compile(boolean dynamic, boolean staticUV) {
		/* 909 */
		if (this.compiled != null) {
			/* 910 */
			return;

		}
		/* 912 */
		this.dynamic = dynamic;
		/* 913 */
		this.indexed = (((dynamic) || (FrameBuffer.versionHint >= 2) || (this.objMesh.anzTri >= 1000)) && (((staticUV) && ((this.objMesh.attrList == null) || (this.objMesh.attrList
				.size() == 0))) ||
		/* 914 */(this.forcedIndexed)));
		/* 915 */
		this.staticUV = staticUV;
		/* 916 */
		this.batchSize = -1;

		/* 920 */
		if ((Config.fixCollapsingVertices) && (this.anim != null)
				&& (this.anim.realFirstCall)) {
			/* 921 */
			this.anim.doAnimation(this, 0, 0.0F);

		}

		/* 924 */
		compileInternal();

	}

	boolean isCompiled() {
		/* 928 */
		if (this.compiled == null) {
			/* 929 */
			return false;

		}
		/* 931 */
		return (this.compiled != null) && (this.compiled.size() > 0);

	}

	public void shareCompiledData(T3DObject withObj) {
		/* 950 */
		if (withObj.shareWith != null) {
			/* 951 */
			Logger.log(
					"Can't enable share data with an object that shares data itself! Use the source object instead!",
					0);
			/* 952 */
			return;

		}

		/* 959 */
		if (this.sharing) {
			/* 960 */
			Logger.log(
					"This object already shares data with '"
							+ withObj.getName() + "'", 0);
			/* 961 */
			return;

		}
		/* 963 */
		if ((withObj.ocTree != null) || (this.ocTree != null)) {
			/* 964 */
			Logger.log("No data sharing with octrees supported!", 0);
			/* 965 */
			return;

		}
		/* 967 */
		if (withObj.objMesh != this.objMesh) {
			/* 968 */
			Logger.log("Can't share data from different meshes!", 0);
			/* 969 */
			return;

		}
		/* 971 */
		this.shareWith = withObj;

	}

	public void touch() {
		/* 982 */
		if (this.dynamic) {
			/* 983 */
			this.modified = true;

		}
		/* 985 */
		if (this.lazyTransforms)
			/* 986 */enableLazyTransformations();

	}

	public void strip() {
		/* 995 */
		if ((this.compiled != null) && (this.compiled.size() > 0)) {
			/* 997 */
			reallyStrip();

		}

		/* 1000 */
		this.toStrip = true;

	}

	public void forceGeometryIndices(boolean force) {
		/* 1015 */
		this.forcedIndexed = force;

	}

	public void setAnimationSequence(Animation anim) {
		/* 1032 */
		if (anim == null) {
			/* 1033 */
			this.anim = null;
			/* 1034 */
			return;

		}

		/* 1037 */
		if ((this.hasBeenBuild) && (!this.dynamic)) {
			/* 1038 */
			Logger.log(
					/* 1039 */"You are adding an Animation to an Rage3DObject that has already been build in static mode. Consider to use { calcNormals(); calcBoundingBox(); } instead of build() and call build() only after the animation has been setKey.",
					/* 1040 */1);

		}

		/* 1043 */
		if (anim.aktFrames != 0) {
			/* 1044 */
			if (anim.keyFrames[0].anzCoords == this.objMesh.anzCoords) {
				/* 1045 */
				this.anim = anim;
				/* 1046 */
				this.anim.validate(this);

			} else {
				/* 1048 */
				Logger.log("The sizes of the Animation's Meshes ("
						+ anim.keyFrames[0].anzCoords
						+ ") and the object's Mesh (" + this.objMesh.anzCoords
						+ ") don't match!",
				/* 1049 */0);

			}

		}
		/* 1052 */
		else
			Logger.log("This Animation is empty!", 0);

	}

	public void clearAnimation() {
		/* 1061 */
		this.anim = null;

	}

	public Animation getAnimationSequence() {
		/* 1070 */
		return this.anim;

	}

	public void animate(float index, int seq) {
		/* 1086 */
		if ((this.dynamic) &&
		/* 1087 */(this.anim != null))
			/* 1088 */this.anim.doAnimation(this, seq, index);

	}

	public void animate(float index) {
		/* 1103 */
		animate(index, 0);

	}

	/**
	 * Sets if and how the object will respond to collisions. Setting mode to
	 * COLLISION_MODE_NONE (which is default) means, that the object can't be
	 * partner in a collision with other objects. Setting it to
	 * COLLISION_MODE_OTHERS means that other objects may collide with this
	 * object and setting it to COLLISION_MODE_SELF means, that the object
	 * itself may collide with other objects. The modes may be combined by using
	 * the or-operator | .
	 * 
	 * @param mode
	 *            The desired mode {@link #COLLISION_MODE_SELF},
	 *            {@link #COLLISION_MODE_NONE},@{link #COLLISION_MODE_OTHERS} or
	 *            combinations)
	 * @see #COLLISION_MODE_NONE
	 * @see #COLLISION_MODE_OTHERS
	 * @see #COLLISION_MODE_SELF
	 */
	public void setCollisionMode(int mode) {
		if (mode == 0) {
			this.isPotentialCollider = false;
			this.mayCollide = false;

		} else {
			if ((mode & 0x1) == 1)
				this.isPotentialCollider = true;

			else {
				this.isPotentialCollider = false;
			}
			if ((mode & 0x2) == 2)
				this.mayCollide = true;

			else
				this.mayCollide = false;

		}

	}

	/**
	 * Sets an optimization for collision detection to be used/not used. This
	 * optimization may cause problems on dynamically updated geometry from an
	 * animation or an IVertexController. Therefor, it's disabled by default.
	 * 
	 * @param optimized
	 */
	public void setCollisionOptimization(boolean optimized) {
		if (this.largestPolygonSize == -1.0F) {
			this.largestPolygonSize = this.objMesh.getLargestCoveredDistance();

		}
		this.optimizeColDet = optimized;
	}

	/**
	 * Sets the object to visible/invisible. Invisible objects won't be
	 * processed/rendered at all.
	 * 
	 * @param isVisible
	 */
	public void setVisibility(boolean isVisible) {
		this.isVisible = isVisible;

	}

	public boolean getVisibility() {
		return this.isVisible;

	}

	/**
	 * Calculates an AABB (Axis Aligned Bounding Box) for this object in
	 * object-space. The box will then be transformed together with the object
	 * so that it becomes an OBB (oriented bounding box) when the object will be
	 * transformed. Normally, there is no need to call this method directly,
	 * because it will already be called from the build() method. Exceptions
	 * could be the use of objects meshes for animation and similar tasks
	 */
	public void calcBoundingBox() {
		synchronized (this.objMesh) {
			float[] res = this.objMesh.calcBoundingBox();
			setBoundingBox(res[0], res[1], res[2], res[3], res[4], res[5]);

		}

	}

	/**
	 * Initializes basic object properties that are needed for almost all
	 * further processing. build() has to be called if the object is
	 * "ready to render" (loaded, Textures assigned, placed, rendering modes
	 * setKey, animations and vertex controllers assigned).
	 */
	public void build() {
		build(true);

	}

	/**
	 * 
	 * @param staticUV
	 */
	public void build(boolean staticUV) {

		try {
			this.hasBeenBuild = true;
			if (!this.objMesh.normalsCalculated) {
				calcCenter();
				calcBoundingBox();
				calcNormals();

			} else {
				calcCenter();
				if (this.objMesh.obbStart != 0)
					this.hasBoundingBox = true;

				else {
					calcBoundingBox();

				}

			}

			if ((this.shader != null) && (this.shader.needsTangents)
					&& (!this.objMesh.tangentsCalculated)) {
				this.objMesh.calculateTangentVectors(this.objVectors);
			}

			boolean dyna = (this.anim != null)
					|| (this.objMesh.myController != null);
			if ((!dyna) && (this.objMesh.attrList != null)) {
				for (int i = 0; (i < this.objMesh.attrList.size()) && (!dyna); i++) {
					dyna |= ((VertexAttributes) this.objMesh.attrList.get(i))
							.isDynamic();
				}

			}

			/* 1261 */
			if ((dyna) || (!staticUV))
				/* 1262 */compile(true, staticUV);

			else
				/* 1264 */compile();

		} catch (Exception e) {
			/* 1267 */
			e.printStackTrace();
			/* 1268 */
			Logger.log(
					"Couldn't build() object ("
							+ this.name
							+ ")! Check if the object is assigned to a world and if the TextureCache has all required textures loaded.",
					/* 1269 */0);

		}

	}

	public void disableVertexSharing() {
		/* 1280 */
		this.neverOptimize = true;

	}

	public boolean hasChild(T3DObject obj) {
		/* 1304 */
		if (obj != null) {
			/* 1305 */
			return obj.hasParent(this);

		}
		/* 1307 */
		Logger.log(
				"Testing a null-Rage3DObject for being a child is rather senseless!",
				1);
		/* 1308 */
		return false;

	}

	public boolean hasParent(T3DObject obj) {
		/* 1323 */
		boolean found = false;
		/* 1324 */
		if (obj != null) {
			/* 1325 */
			for (int i = 0; i < this.parentCnt; i++) {
				/* 1326 */
				if (this.parent[i].number == obj.number) {
					/* 1327 */
					found = true;
					/* 1328 */
					break;

				}

			}
			/* 1331 */
			return found;

		}
		/* 1333 */
		Logger.log(
				"Testing a null-Rage3DObject for being a parent is rather senseless!",
				1);
		/* 1334 */
		return false;

	}

	public void addChild(T3DObject obj) {
		if (obj != null)
			/* 1353 */obj.addParent(this);
		else
			/* 1355 */Logger.log(
					"Tried to assign a non-existent Rage3DObject as child!", 0);
	}

	public void removeChild(T3DObject obj) {
		if (obj != null)
			/* 1370 */obj.removeParent(this);
		else
			Logger.log(
					"Tried to remove a non-existent Rage3DObject from the child collection!",
					0);
	}

	public void removeParent(T3DObject obj) {
		if (obj != null) {
			boolean found = false;
			for (int i = 0; i < this.parentCnt; i++) {
				if (this.parent[i].number == obj.number) {
					found = true;
					if (i != this.parentCnt - 1) {
						for (int p = i; p < this.parentCnt - 1; p++) {
							this.parent[p] = this.parent[(p + 1)];
						}
					}
					this.parentCnt -= 1;
				}
			}
			if (!found)
				Logger.log(
						"Tried to remove an object from the parent collection that isn't part of it!",
						0);
		} else {
			Logger.log(
					"Tried to remove a non-existent object from the parent collection!",
					0);
		}
	}

	public void addParent(T3DObject obj) {
		if (obj == this) {
			Logger.log("An object can't be its own parent!", 1);
			return;
		}
		if (obj != null) {
			if (this.parentCnt < Config.maxParentObjects) {
				this.parent[this.parentCnt] = obj;
				this.parentCnt += 1;
			} else {
				Logger.log(
						"Can't assign more than "
								+ Config.maxParentObjects
								+ " objects as parent objects in the current Configuration!",
						0);
			}
		} else
			Logger.log("Tried to assign a nonexistent object as parent!", 0);
	}

	public T3DObject[] getParents() {
		T3DObject[] par = new T3DObject[this.parentCnt];
		if (this.parentCnt != 0) {
			System.arraycopy(this.parent, 0, par, 0, this.parentCnt);
		}
		return par;
	}

	public int getID() {
		return this.number - 2;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String n) {
		if ((this.myRage3DScene != null)
				&& (this.myRage3DScene.getInternalObjectByName(n) != null))
			/* 1488 */Logger.log(
					"Object with name '" + n + "' already exists!", 0);
		else
			/* 1490 */this.name = n;
	}

	public boolean wasVisible() {
		return this.object3DRendered;
	}

	public void setCulling(boolean mode) {
		this.doCulling = mode;
	}

	public boolean getCulling() {
		return this.doCulling;
	}

	public void setShadingMode(ShadingMode mode) {
		this.isFlatShaded = false;
		if (mode == ShadingMode.SHADING_FAKED_FLAT)
			this.isFlatShaded = true;
	}

	public void setLighting(Lighting mode) {
		if (mode == Lighting.ALL_LIGHTS) {
			this.isLit = true;
		}
		if (mode == Lighting.NO_LIGHTS)
			this.isLit = false;
	}

	public Lighting getLighting() {
		if (this.isLit) {
			return Lighting.ALL_LIGHTS;
		}
		return Lighting.NO_LIGHTS;
	}

	public void setMaxLights(int lightCount) {
		this.maxLights = Math.min(Math.max(lightCount, 0), 8);
	}

	public int getMaxLights() {
		return this.maxLights;
	}

	public void setSpecularLighting(boolean mode) {
		this.doSpecularLighting = mode;
	}

	public boolean getSpecularLighting() {
		return this.doSpecularLighting;
	}

	public void setTransparency(int trans) {
		this.transValue = trans;
		if (trans >= 0)
			/* 1651 */this.isTrans = true;
		else
			/* 1653 */this.isTrans = false;
	}

	public void setShader(GLSLShader shader) {
		/* 1665 */
		this.shader = shader;

	}

	public GLSLShader getShader() {
		/* 1675 */
		return this.shader;

	}

	GLSLShader getShaderInternal() {
		/* 1679 */
		if ((this.shader == null) && (this.myRage3DScene != null)
				&& (this.myRage3DScene.globalShader != null)) {
			/* 1680 */
			return this.myRage3DScene.globalShader;

		}
		/* 1682 */
		return this.shader;

	}

	public void calcTangentVectors() {
		/* 1694 */
		this.objMesh.calculateTangentVectors(this.objVectors);

	}

	public void clearShader() {
		/* 1702 */
		this.shader = null;

	}

	public int getTransparency() {
		/* 1711 */
		if (!this.isTrans) {
			/* 1712 */
			return -1;

		}
		/* 1714 */
		return this.transValue;

	}

	public boolean isTransparent() {
		/* 1723 */
		return this.isTrans;

	}

	public void setTransparencyMode(TransparencyMode mode) {
		this.transMode = mode;

	}

	public TransparencyMode getTransparencyMode() {
		/* 1746 */
		return this.transMode;

	}

	public boolean hasVertexAlpha() {
		/* 1756 */
		return this.objVectors.alpha != null;

	}

	public void setAdditionalColor(RGBColor col) {
		/* 1774 */
		int r = col.getRed();
		/* 1775 */
		int g = col.getGreen();
		/* 1776 */
		int b = col.getBlue();

		/* 1778 */
		this.addColorInstance = col;

		/* 1780 */
		if ((r >= 0) && (r < 256) && (g >= 0) && (g < 256) && (b >= 0)
				&& (b < 256)) {
			/* 1781 */
			this.addColorR = r;
			/* 1782 */
			this.addColorG = g;
			/* 1783 */
			this.addColorB = b;

		} else {
			/* 1785 */
			Logger.log("Color values need to be in the range of [0..255]!", 0);

		}

	}

	public void setAdditionalColor(int red, int green, int blue) {
		/* 1802 */
		if (this.internalRGBColor == null) {
			/* 1803 */
			this.internalRGBColor = new RGBColor();

		}
		/* 1805 */
		this.internalRGBColor.setTo(red, green, blue, 255);
		/* 1806 */
		setAdditionalColor(this.internalRGBColor);

	}

	public RGBColor getAdditionalColor() {
		/* 1815 */
		return this.addColorInstance;

	}

	public void clearAdditionalColor() {
		/* 1823 */
		this.addColorR = 0;
		/* 1824 */
		this.addColorG = 0;
		/* 1825 */
		this.addColorB = 0;
		/* 1826 */
		this.addColorInstance = RGBColor.BLACK;

	}

	public void clearObject() {
		/* 1839 */
		this.objMesh.obbStart = 0;
		/* 1840 */
		this.objMesh.obbEnd = 0;
		/* 1841 */
		this.objMesh.anzTri = 0;
		/* 1842 */
		this.objMesh.anzCoords = 0;
		/* 1843 */
		this.objMesh.anzVectors = 0;

	}

	public void decoupleMesh() {
		/* 1856 */
		this.objMesh = new Mesh(0);
		/* 1857 */
		this.objVectors.setMesh(this.objMesh);

	}

	public void setBillboarding(boolean mode) {
		/* 1870 */
		this.isBillBoard = mode;

	}

	public boolean isEnvmapped() {
		/* 1882 */
		return this.isEnvmapped;

	}

	public void setEnvmapped(boolean mode) {
		/* 1897 */
		this.isEnvmapped = mode;

	}

	public void setFixedPointMode(boolean useFixedPoint) {
		/* 1909 */
		this.fixedPointMode = useFixedPoint;

	}

	public void rotateX(float w) {
		/* 1922 */
		this.rotationMatrix.rotateX(w);

	}

	public void rotateY(float w) {
		/* 1935 */
		this.rotationMatrix.rotateY(w);

	}

	public void rotateZ(float w) {
		/* 1948 */
		this.rotationMatrix.rotateZ(w);

	}

	public void rotateAxis(SimpleVector axis, float angle) {
		/* 1964 */
		this.rotationMatrix.rotateAxis(axis, angle);

	}

	public void translateMesh() {
		/* 1976 */
		this.objMesh.translateMesh(this.translationMatrix, this.originMatrix);
		/* 1977 */
		if (this.anim != null) {
			/* 1978 */
			this.anim.translateMesh(this.translationMatrix, this.originMatrix);

		}
		/* 1980 */
		this.skipPivot = false;
		/* 1981 */
		calcBoundingBox();

	}

	public void translate(SimpleVector trans) {
		/* 1994 */
		this.translationMatrix.translate(trans);

	}

	public void translate(float x, float y, float z) {
		/* 2014 */
		this.translationMatrix.translate(x, y, z);

	}

	public void align(Camera camera) {
		/* 2031 */
		float scale = getScale();
		/* 2032 */
		setScale(1.0F);
		/* 2033 */
		this.rotationMatrix = camera.backMatrix.invert3x3(this.rotationMatrix);
		/* 2034 */
		setScale(scale);

	}

	public void align(T3DObject object) {
		/* 2048 */
		float scale = getScale();
		/* 2049 */
		setScale(1.0F);
		/* 2050 */
		this.rotationMatrix.setTo(object.rotationMatrix);
		/* 2051 */
		setScale(scale);

	}

	public void setOrientation(SimpleVector dir, SimpleVector up) {
		/* 2065 */
		float scale = getScale();
		/* 2066 */
		setScale(1.0F);
		/* 2067 */
		this.rotationMatrix.setOrientation(dir, up);
		/* 2068 */
		setScale(scale);

	}

	public void enableLazyTransformations() {
		/* 2088 */
		this.lazyTransforms = true;

		/* 2090 */
		if (this.transCacheDump == null) {
			/* 2091 */
			this.transCacheDump = this.transCache;

		}
		/* 2093 */
		if (this.invCacheDump == null) {
			/* 2094 */
			this.invCacheDump = this.invCache;

		}
		/* 2096 */
		this.transCache = null;
		/* 2097 */
		this.invCache = null;

	}

	public void disableLazyTransformations() {
		/* 2107 */
		this.lazyTransforms = false;

		/* 2109 */
		if (this.transCacheDump == null) {
			/* 2110 */
			this.transCacheDump = this.transCache;

		}
		/* 2112 */
		if (this.invCacheDump == null) {
			/* 2113 */
			this.invCacheDump = this.invCache;

		}

		/* 2116 */
		this.transCache = null;
		/* 2117 */
		this.invCache = null;

	}

	public void scale(float scale) {
		/* 2133 */
		if (scale > 0.0F) {
			/* 2134 */
			this.scaleFactor *= scale;
			/* 2135 */
			this.rotationMatrix.scalarMul(scale);

		} else {
			/* 2137 */
			Logger.log("Scale has to be greater than zero!", 0);

		}

	}

	public void setScale(float absScale) {
		/* 2152 */
		if ((this.scaleFactor != 0.0F) && (absScale > 0.0F)) {
			/* 2153 */
			float scale = absScale / this.scaleFactor;
			/* 2154 */
			if (scale < 1.0E-004F) {
				/* 2155 */
				scale = 1.0E-004F;

			}
			/* 2157 */
			scale(scale);

		} else {
			/* 2159 */
			Logger.log("Invalid scale!", 0);

		}

	}

	public float getScale() {
		/* 2169 */
		return this.scaleFactor;

	}

	public SimpleVector getTranslation() {
		/* 2179 */
		return this.translationMatrix.getTranslation();

	}

	public SimpleVector getTranslation(SimpleVector trns) {
		/* 2193 */
		if (trns == null) {
			/* 2194 */
			trns = new SimpleVector();

		}
		/* 2196 */
		float[] mat3 = this.translationMatrix.mat[3];
		/* 2197 */
		trns.x = mat3[0];
		/* 2198 */
		trns.y = mat3[1];
		/* 2199 */
		trns.z = mat3[2];
		/* 2200 */
		return trns;

	}

	public SimpleVector getOrigin() {
		/* 2209 */
		return this.originMatrix.getTranslation();

	}

	public SimpleVector getXAxis() {
		/* 2220 */
		SimpleVector sv = this.rotationMatrix.getXAxis();
		/* 2221 */
		sv.scalarMul(1.0F / this.scaleFactor);
		/* 2222 */
		return sv;

	}

	public SimpleVector getYAxis() {
		/* 2233 */
		SimpleVector sv = this.rotationMatrix.getYAxis();
		/* 2234 */
		sv.scalarMul(1.0F / this.scaleFactor);
		/* 2235 */
		return sv;

	}

	public SimpleVector getZAxis() {
		SimpleVector sv = this.rotationMatrix.getZAxis();
		sv.scalarMul(1.0F / this.scaleFactor);
		return sv;

	}

	public SimpleVector getXAxis(SimpleVector toFill) {
		SimpleVector sv = this.rotationMatrix.getXAxis(toFill);
		sv.scalarMul(1.0F / this.scaleFactor);
		return sv;

	}

	public SimpleVector getYAxis(SimpleVector toFill) {
		SimpleVector sv = this.rotationMatrix.getYAxis(toFill);
		sv.scalarMul(1.0F / this.scaleFactor);
		return sv;

	}

	/**
	 * 
	 * @param toFill
	 * @return
	 */
	public SimpleVector getZAxis(SimpleVector toFill) {
		SimpleVector sv = this.rotationMatrix.getZAxis(toFill);
		sv.scalarMul(1.0F / this.scaleFactor);
		return sv;

	}

	public Matrix getRotationMatrix() {
		/* 2308 */
		return this.rotationMatrix;

	}

	public Matrix getTranslationMatrix() {
		/* 2317 */
		return this.translationMatrix;

	}

	public Matrix getOriginMatrix() {
		/* 2336 */
		return this.originMatrix;

	}

	public int getLightCount() {
		/* 2347 */
		if (this.nearestLights == null) {
			/* 2348 */
			return 0;

		}
		/* 2350 */
		for (int i = 0; i < this.nearestLights.length; i++) {
			/* 2351 */
			if (this.nearestLights[i][0] == -9999.0F) {
				/* 2352 */
				return i;

			}

		}
		/* 2355 */
		return this.nearestLights.length;

	}

	public void setRotationMatrix(Matrix mat) {
		/* 2371 */
		this.rotationMatrix = mat;

	}

	public void clearRotation() {
		/* 2379 */
		setScale(1.0F);
		/* 2380 */
		this.rotationMatrix.setIdentity();

	}

	public void clearTranslation() {
		/* 2388 */
		this.translationMatrix.setIdentity();

	}

	public void setTextureMatrix(Matrix mat) {
		/* 2401 */
		this.textureMatrix = mat;

	}

	public Matrix getTextureMatrix() {
		/* 2410 */
		return this.textureMatrix;

	}

	public void rotateMesh() {
		/* 2423 */
		this.objMesh.rotateMesh(this.rotationMatrix, this.xRotationCenter,
				this.yRotationCenter, this.zRotationCenter, this.scaleFactor);
		/* 2424 */
		if (this.anim != null) {
			/* 2425 */
			this.anim.rotateMesh(this.rotationMatrix, this.xRotationCenter,
					this.yRotationCenter, this.zRotationCenter,
					this.scaleFactor);

		}
		/* 2427 */
		this.skipPivot = false;
		/* 2428 */
		calcBoundingBox();

	}

	public void setTranslationMatrix(Matrix mat) {
		/* 2441 */
		this.translationMatrix = mat;

	}

	public void setMesh(Mesh mesh) {
		/* 2455 */
		this.objMesh = mesh;
		/* 2456 */
		if (this.objVectors != null)
			/* 2457 */this.objVectors.setMesh(this.objMesh);

	}

	public Mesh getMesh() {
		/* 2477 */
		return this.objMesh;

	}

	public PolygonManager getPolygonManager() {
		/* 2489 */
		if (this.polyManager == null) {
			/* 2491 */
			this.polyManager = new PolygonManager(this);

		}
		/* 2493 */
		return this.polyManager;

	}

	public void setBoundingBox(float minx, float maxx, float miny, float maxy,
			float minz, float maxz) {
		/* 2517 */
		if (this.objMesh.obbStart != 0) {
			/* 2518 */
			this.objMesh.anzCoords = this.objMesh.obbStart;
			/* 2519 */
			this.objMesh.obbStart = 0;
			/* 2520 */
			this.objMesh.obbEnd = 0;

		}

		/* 2523 */
		if (this.objVectors != null) {
			/* 2524 */
			this.objMesh.obbStart = this.objVectors.addVertex(minx, miny, minz);
			/* 2525 */
			this.objVectors.addVertex(minx, miny, maxz);
			/* 2526 */
			this.objVectors.addVertex(maxx, miny, minz);
			/* 2527 */
			this.objVectors.addVertex(maxx, miny, maxz);
			/* 2528 */
			this.objVectors.addVertex(maxx, maxy, minz);
			/* 2529 */
			this.objVectors.addVertex(maxx, maxy, maxz);
			/* 2530 */
			this.objVectors.addVertex(minx, maxy, minz);
			/* 2531 */
			this.objMesh.obbEnd = this.objVectors.addVertex(minx, maxy, maxz);
			/* 2532 */
			this.hasBoundingBox = true;

		}

	}

	public T3DObject cloneObject() {
		/* 2553 */
		return new T3DObject(this, true);

	}

	void enlarge(int byTriangles) {
		/* 2558 */
		int newMaxTriangles = this.texture.length + byTriangles;

		/* 2560 */
		if (newMaxTriangles <= this.texture.length) {
			/* 2561 */
			return;

		}

		/* 2564 */
		T3DObject tmpObj = new T3DObject(newMaxTriangles);
		/* 2565 */
		Vectors tmpVectors = tmpObj.objVectors;
		/* 2566 */
		Mesh tmpMesh = tmpObj.objMesh;

		/* 2568 */
		for (int i = 0; i < this.objVectors.maxVectors; i++) {
			/* 2569 */
			tmpVectors.nuOrg[i] = this.objVectors.nuOrg[i];
			/* 2570 */
			tmpVectors.nvOrg[i] = this.objVectors.nvOrg[i];

		}
		/* 2572 */
		this.objVectors.nuOrg = tmpVectors.nuOrg;
		/* 2573 */
		this.objVectors.nvOrg = tmpVectors.nvOrg;
		/* 2574 */
		this.objVectors.maxVectors = tmpVectors.maxVectors;

		/* 2576 */
		this.objVectors.setMesh(this.objMesh);
		/* 2577 */
		this.objMesh.maxVectors = tmpMesh.maxVectors;

		/* 2579 */
		for (int i = 0; i < this.objMesh.points.length; i++) {
			/* 2580 */
			tmpMesh.points[i][0] = this.objMesh.points[i][0];
			/* 2581 */
			tmpMesh.points[i][1] = this.objMesh.points[i][1];
			/* 2582 */
			tmpMesh.points[i][2] = this.objMesh.points[i][2];

		}
		/* 2584 */
		this.objMesh.points = tmpMesh.points;

		/* 2586 */
		for (int i = 0; i < this.objMesh.xOrg.length; i++) {
			/* 2587 */
			tmpMesh.xOrg[i] = this.objMesh.xOrg[i];
			/* 2588 */
			tmpMesh.yOrg[i] = this.objMesh.yOrg[i];
			/* 2589 */
			tmpMesh.zOrg[i] = this.objMesh.zOrg[i];

			/* 2591 */
			tmpMesh.nxOrg[i] = this.objMesh.nxOrg[i];
			/* 2592 */
			tmpMesh.nyOrg[i] = this.objMesh.nyOrg[i];
			/* 2593 */
			tmpMesh.nzOrg[i] = this.objMesh.nzOrg[i];

			/* 2595 */
			tmpMesh.coords[i] = this.objMesh.coords[i];

		}

		/* 2598 */
		this.objMesh.xOrg = tmpMesh.xOrg;
		/* 2599 */
		this.objMesh.yOrg = tmpMesh.yOrg;
		/* 2600 */
		this.objMesh.zOrg = tmpMesh.zOrg;
		/* 2601 */
		this.objMesh.nxOrg = tmpMesh.nxOrg;
		/* 2602 */
		this.objMesh.nyOrg = tmpMesh.nyOrg;
		/* 2603 */
		this.objMesh.nzOrg = tmpMesh.nzOrg;
		/* 2604 */
		this.objMesh.coords = tmpMesh.coords;

		/* 2606 */
		System.arraycopy(this.texture, 0, tmpObj.texture, 0,
				this.texture.length);

		/* 2608 */
		this.texture = tmpObj.texture;

	}

	public Matrix getWorldTransformation() {

		Matrix mat5;
		/* 2622 */
		if ((!this.lazyTransforms) || (this.transCache == null)) {
			/* 2623 */
			mat5 = new Matrix();

			/* 2625 */
			float[] matmat3 = mat5.mat[3];
			/* 2626 */
			float[] translationMatrixmat = this.translationMatrix.mat[3];
			/* 2627 */
			float[] originMatrixmat = this.originMatrix.mat[3];

			/* 2629 */
			matmat3[0] = (-this.xRotationCenter);
			/* 2630 */
			matmat3[1] = (-this.yRotationCenter);
			/* 2631 */
			matmat3[2] = (-this.zRotationCenter);

			/* 2633 */
			if (!this.isBillBoard) {
				/* 2634 */
				mat5.matMul(this.rotationMatrix);

			} else {
				/* 2636 */
				if (this.myRage3DScene == null) {
					/* 2637 */
					return new Matrix();

				}
				/* 2639 */
				this.mat2 = this.myRage3DScene.camera.backMatrix;
				/* 2640 */
				Matrix m = getTmpMatrix(1);
				/* 2641 */
				this.mat2.invert(m);
				/* 2642 */
				m.scalarMul(this.scaleFactor);
				/* 2643 */
				mat5.matMul(m);

			}
			/* 2645 */
			mat5.translate(this.xRotationCenter + translationMatrixmat[0]
					+ originMatrixmat[0], this.yRotationCenter
					+ translationMatrixmat[1] + originMatrixmat[1],
					this.zRotationCenter +
					/* 2646 */translationMatrixmat[2] + originMatrixmat[2]);

			/* 2648 */
			if (this.parentCnt != 0) {
				/* 2649 */
				if (this.isBillBoard)
					/* 2650 */mat5 = recurseObjectsBillboarded(mat5);

				else {
					/* 2652 */
					mat5 = recurseObjects(mat5);

				}

			}

			/* 2656 */
			if (this.lazyTransforms)
				/* 2657 */if (this.transCacheDump != null) {
					/* 2658 */
					this.transCache = this.transCacheDump;
					/* 2659 */
					this.transCacheDump = null;
					/* 2660 */
					this.transCache.setTo(mat5);

				} else {
					/* 2662 */
					this.transCache = mat5.cloneMatrix();

				}

		}

		else {
			/* 2667 */
			mat5 = this.transCache.cloneMatrix();

		}
		/* 2669 */
		return mat5;

	}

	public Matrix getWorldTransformation(Matrix mat) {
		/* 2683 */
		if (mat == null) {
			/* 2684 */
			mat = new Matrix();

		}

		/* 2687 */
		if ((!this.lazyTransforms) || (this.transCache == null)) {
			/* 2688 */
			Matrix mat5 = mat;
			/* 2689 */
			mat5.setIdentity();

			/* 2691 */
			Matrix mat6 = getTmpMatrix(0);
			/* 2692 */
			mat6.setIdentity();

			/* 2694 */
			float[] matmat3 = mat5.mat[3];
			/* 2695 */
			float[] translationMatrixmat = this.translationMatrix.mat[3];
			/* 2696 */
			float[] originMatrixmat = this.originMatrix.mat[3];

			/* 2698 */
			matmat3[0] = (-this.xRotationCenter);
			/* 2699 */
			matmat3[1] = (-this.yRotationCenter);
			/* 2700 */
			matmat3[2] = (-this.zRotationCenter);

			/* 2702 */
			matmat3 = mat6.mat[3];

			/* 2704 */
			matmat3[0] = (this.xRotationCenter + translationMatrixmat[0] + originMatrixmat[0]);

			/* 2707 */
			matmat3[1] = (this.yRotationCenter + translationMatrixmat[1] + originMatrixmat[1]);
			/* 2708 */
			matmat3[2] = (this.zRotationCenter + translationMatrixmat[2] + originMatrixmat[2]);

			/* 2710 */
			if (!this.isBillBoard) {
				/* 2711 */
				mat5.matMul(this.rotationMatrix);

			} else {
				/* 2713 */
				Matrix m = getTmpMatrix(1);
				/* 2714 */
				this.mat2.invert(m);
				/* 2715 */
				m.scalarMul(this.scaleFactor);
				/* 2716 */
				mat5.matMul(m);

			}
			/* 2718 */
			mat5.matMul(mat6);

			/* 2720 */
			if (this.parentCnt != 0) {
				/* 2721 */
				if (this.isBillBoard)
					/* 2722 */mat5 = recurseObjectsBillboarded(mat5);

				else {
					/* 2724 */
					mat5 = recurseObjects(mat5);

				}

			}

			/* 2728 */
			if (this.lazyTransforms) {
				/* 2729 */
				if (this.transCacheDump != null) {
					/* 2730 */
					this.transCache = this.transCacheDump;
					/* 2731 */
					this.transCacheDump = null;
					/* 2732 */
					this.transCache.setTo(mat5);

				} else {
					/* 2734 */
					this.transCache = mat5.cloneMatrix();

				}

			}

			/* 2738 */
			if (mat != mat5)
				/* 2739 */mat.setTo(mat5);

		}

		else {
			/* 2742 */
			mat.setTo(this.transCache);

		}
		/* 2744 */
		return mat;

	}

	public synchronized void addCollisionListener(CollisionListener listener) {
		/* 2757 */
		if (this.collisionListener == null) {
			/* 2758 */
			this.collisionListener = new ArrayList(1);

		}
		/* 2760 */
		this.collisionListener.add(listener);
		/* 2761 */
		globalListenerCount += 1;

	}

	public synchronized void removeCollisionListener(CollisionListener listener) {
		/* 2771 */
		if (this.collisionListener != null) {
			/* 2772 */
			this.collisionListener.remove(listener);
			/* 2773 */
			globalListenerCount -= 1;
			/* 2774 */
			if (this.collisionListener.size() == 0)
				/* 2775 */this.collisionListener = null;

		}

	}

	public void disableCollisionListeners() {
		/* 2785 */
		this.disableListeners = true;

	}

	public void enableCollisionListeners() {
		/* 2792 */
		this.disableListeners = false;

	}

	public Iterator<CollisionListener> getCollisionListeners() {
		/* 2802 */
		if (this.collisionListener != null) {
			/* 2803 */
			return this.collisionListener.iterator();

		}
		/* 2805 */
		return new ArrayList(0).iterator();

	}

	public void setRenderHook(IRenderHook hook) {
		/* 2818 */
		this.renderHook = hook;

	}

	public IRenderHook getRenderHook() {
		/* 2827 */
		return this.renderHook;

	}

	public int checkForCollision(SimpleVector dirVec, float step) {
		checkWorld();
		return this.myRage3DScene.checkObjCollision(this, dirVec, step);

	}

	public SimpleVector checkForCollisionSpherical(SimpleVector translation,
			float radius) {
		checkWorld();
		return this.myRage3DScene.checkObjCollisionSpherical(this, translation,
				radius);

	}

	public SimpleVector checkForCollisionEllipsoid(SimpleVector translation,
			SimpleVector ellipsoid, int recursionDepth) {
		if (recursionDepth < 1) {
			recursionDepth = 1;

		}
		checkWorld();
		return this.myRage3DScene.checkObjCollisionEllipsoid(this, translation,
				ellipsoid, recursionDepth);

	}

	private void checkWorld() {
		if (this.myRage3DScene == null)
			Logger.log(
					"Object has to be assigned to a world for doing collision detection!",
					0);

	}

	/**
	 * When doing ellipsoid collision detection with this object, the ellipsoid
	 * can be transformed according to the objects's transformation and in the
	 * source's object space or it remains static in the target's object space
	 * (i.e. "axis aligned"). The later is faster, but not suitable for all
	 * kinds of ellipsoids.
	 * 
	 * @param mode
	 */
	public void setEllipsoidMode(EllipsoidMode mode) {
		this.ellipsoidMode = mode;
	}

	public EllipsoidMode getEllipsoidMode() {
		return this.ellipsoidMode;

	}

	public boolean wasTargetOfLastCollision() {
		return this.wasCollider;

	}

	public void resetCollisionStatus() {
		this.wasCollider = false;
	}

	public float calcMinDistance(SimpleVector org, SimpleVector dr) {
		this.wasCollider = false;
		resetPolygonIDCount();
		createCollisionArrays();

		origColParam[0] = org.x;
		origColParam[1] = org.y;
		origColParam[2] = org.z;

		dirColParam[0] = dr.x;
		dirColParam[1] = dr.y;
		dirColParam[2] = dr.z;

		float dist = collide(origColParam, dirColParam, 0.0F, 1.0E+012F, false);
		if (dist != 1.0E+012F) {
			if (this.collisionListener != null) {
				SimpleVector pos = SimpleVector.create(dr.x, dr.y, dr.z);
				pos.scalarMul(dist);
				pos.add(org.x, org.y, org.z);
				notifyCollisionListeners(0, 0, new T3DObject[] { this }, pos);
			}
			this.wasCollider = true;
		}
		return dist;
	}

	public float calcMinDistance(SimpleVector org, SimpleVector dr,
			float ignoreIfLarger) {
		return calcMinDistance(org, dr, ignoreIfLarger, true);
	}

	float calcMinDistance(SimpleVector org, SimpleVector dr,
			float ignoreIfLarger, boolean notify) {
		this.wasCollider = false;
		resetPolygonIDCount();
		float store = Config.collideOffset;
		Config.collideOffset = ignoreIfLarger;
		createCollisionArrays();

		origColParam[0] = org.x;
		origColParam[1] = org.y;
		origColParam[2] = org.z;

		dirColParam[0] = dr.x;
		dirColParam[1] = dr.y;
		dirColParam[2] = dr.z;

		float dist = collide(origColParam, dirColParam, 0.0F, ignoreIfLarger,
				false);
		Config.collideOffset = store;
		if ((notify) && (dist != 1.0E+012F)) {
			if (this.collisionListener != null) {
				SimpleVector pos = SimpleVector.create(dr.x, dr.y, dr.z);
				pos.scalarMul(dist);
				pos.add(org.x, org.y, org.z);
				notifyCollisionListeners(0, 0, new T3DObject[] { this }, pos);
			}
			this.wasCollider = true;
		}
		return dist;
	}

	public void setCenter(SimpleVector center) {
		this.centerX = center.x;
		this.centerY = center.y;
		this.centerZ = center.z;
	}

	public SimpleVector getCenter() {
		return SimpleVector.create(this.centerX, this.centerY, this.centerZ);
	}

	public SimpleVector getTransformedCenter() {
		return getTransformedCenter(new SimpleVector());
	}

	public SimpleVector getTransformedCenter(SimpleVector toFill) {
		if (toFill == null) {
			toFill = new SimpleVector();
		}
		getProjectedPoint(this.centerX, this.centerY, this.centerZ, toFill,
				null, mat7);
		return toFill;
	}

	private void fillTransformedCenter(SimpleVector fillMe) {
		getProjectedPoint(this.centerX, this.centerY, this.centerZ, fillMe,
				null, mat71);
	}

	public void setRotationPivot(SimpleVector pivot) {
		this.xRotationCenter = pivot.x;
		this.yRotationCenter = pivot.y;
		this.zRotationCenter = pivot.z;
	}

	public SimpleVector getRotationPivot() {
		return SimpleVector.create(this.xRotationCenter, this.yRotationCenter,
				this.zRotationCenter);
	}

	public void calcCenter() {
		SimpleVector c = this.objMesh.calcCenter();
		if (!this.skipPivot) {
			this.xRotationCenter = c.x;
			this.yRotationCenter = c.y;
			this.zRotationCenter = c.z;
		}
		this.centerX = c.x;
		this.centerY = c.y;
		this.centerZ = c.z;
	}

	public void setOcTree(OcTree ocTree) {

		this.ocTree = ocTree;

	}

	public OcTree getOcTree() {

		return this.ocTree;

	}

	public void setOrigin(SimpleVector origin) {
		this.originMatrix.setIdentity();
		this.originMatrix.translate(origin.x, origin.y, origin.z);
	}

	public void invert() {
		for (int i = 0; i < this.objMesh.anzTri; i++) {
			int pt1 = this.objMesh.points[i][0];
			int pt2 = this.objMesh.points[i][2];

			int p0 = this.objMesh.coords[pt1];
			int p2 = this.objMesh.coords[pt2];

			this.objMesh.coords[pt1] = p2;
			this.objMesh.coords[pt2] = p0;

			float f0 = this.objVectors.nuOrg[pt1];
			float f2 = this.objVectors.nuOrg[pt2];

			this.objVectors.nuOrg[pt1] = f2;
			this.objVectors.nuOrg[pt2] = f0;

			f0 = this.objVectors.nvOrg[pt1];
			f2 = this.objVectors.nvOrg[pt2];

			this.objVectors.nvOrg[pt1] = f2;
			this.objVectors.nvOrg[pt2] = f0;
		}
	}

	public void invertCulling(boolean inv) {
		this.reverseCulling = inv;
	}

	public boolean cullingIsInverted() {
		return this.reverseCulling;
	}

	public void calcNormals() {
		this.objMesh.calcNormals();
	}

	public void calcTextureWrap() {
		int texX = 256;
		int texY = 256;
		int texYB = 256;
		int texXB = 256;

		int texXh = texX >> 1;
		int texYh = texY >> 1;
		texX--;
		texY--;

		int texXBh = texXB >> 1;
		int texYBh = texYB >> 1;
		texXB--;
		texYB--;

		HashMap ht = new HashMap();
		for (int p = 0; p < this.objMesh.anzVectors; p++) {
			Integer pi = IntegerC.valueOf(this.objMesh.coords[p]);
			ArrayList points = (ArrayList) ht.get(pi);
			if (points == null) {
				points = new ArrayList();
				ht.put(pi, points);
			}
			points.add(IntegerC.valueOf(p));
		}

		for (int i = 0; i < this.objMesh.anzCoords; i++) {
			float px = this.objMesh.nxOrg[i];
			float py = this.objMesh.nyOrg[i];

			float su = texXh + px * texXh;
			float sv = texYh + py * texYh;

			float suB = texXBh + px * texXBh;
			float svB = texYBh + py * texYBh;

			while ((su > texX) || (su < 0.0F) || (sv > texY) || (sv < 0.0F)) {
				if (su > texX) {
					su = -texX;
				} else if (su < 0.0F) {
					su = texX;
				}

				if (sv > texY) {
					sv = -texY;
				} else if (sv < 0.0F) {
					sv = texY;
				}

			}

			while ((suB > texXB) || (suB < 0.0F) || (svB > texYB)
					|| (svB < 0.0F)) {
				if (suB > texXB) {
					suB = -texXB;
				} else if (suB < 0.0F) {
					suB = texXB;
				}

				if (svB > texYB) {
					svB = -texYB;
				} else if (svB < 0.0F) {
					svB = texYB;
				}

			}
			ArrayList points = (ArrayList) ht.get(IntegerC.valueOf(i));
			if (points != null)
				for (int i2 = 0; i2 < points.size(); i2++) {
					int p = ((Integer) points.get(i2)).intValue();
					this.objVectors.nuOrg[p] = (su / texX);
					this.objVectors.nvOrg[p] = (sv / texY);
				}
		}
	}

	public void calcTextureWrapSpherical() {
		int texX = 256;
		int texY = 256;
		int texYB = 256;
		int texXB = 256;

		calcCenter();
		double uTile = 1.0D;
		double vTile = 1.0D;

		HashMap ht = new HashMap();
		for (int p = 0; p < this.objMesh.anzVectors; p++) {
			Integer pi = IntegerC.valueOf(this.objMesh.coords[p]);
			ArrayList points = (ArrayList) ht.get(pi);
			if (points == null) {
				points = new ArrayList();
				ht.put(pi, points);
			}
			points.add(IntegerC.valueOf(p));
		}

		for (int i = 0; i < this.objMesh.anzCoords; i++) {
			float px = this.objMesh.xOrg[i] - this.centerX;
			float py = this.objMesh.yOrg[i] - this.centerY;
			float pz = this.objMesh.zOrg[i] - this.centerZ;

			float n = (float) Math.sqrt(px * px + py * py + pz * pz);
			px /= n;
			py /= n;

			float su = (float) ((Math.asin(px) / 3.141592653589793D + 0.5D) * uTile);
			float sv = (float) ((Math.asin(py) / 3.141592653589793D + 0.5D) * vTile);

			float suB = su * texXB;
			float svB = sv * texYB;

			su *= texX;
			sv *= texY;

			while ((su > texX) || (su < 0.0F) || (sv > texY) || (sv < 0.0F)) {
				if (su > texX) {
					su = -texX;
				} else if (su < 0.0F) {
					su = texX;
				}

				if (sv > texY) {
					sv = -texY;
				} else if (sv < 0.0F) {
					sv = texY;
				}

			}

			while ((suB > texXB) || (suB < 0.0F) || (svB > texYB)
					|| (svB < 0.0F)) {
				if (suB > texXB) {
					suB = -texXB;
				} else if (suB < 0.0F) {
					suB = texXB;
				}

				if (svB > texYB) {
					svB = -texYB;
				} else if (svB < 0.0F) {
					svB = texYB;
				}

			}

			ArrayList points = (ArrayList) ht.get(IntegerC.valueOf(i));
			if (points != null)
				for (int i2 = 0; i2 < points.size(); i2++) {
					int p = ((Integer) points.get(i2)).intValue();
					this.objVectors.nuOrg[p] = (su / texX);
					this.objVectors.nvOrg[p] = (sv / texY);
				}
		}
	}

	public void setTexture(String texname) {
		TextureCache texMan = TextureCache.getInstance();

		int number = texMan.getTextureID(texname);
		if (number != -1) {
			int end = this.texture.length;
			for (int i = 0; i < end; i++)
				this.texture[i] = number;
		} else {
			Logger.log("Tried to setKey an undefined texture!", 0);
		}
	}

	public void setTexture(TextureInfo tInf) {
		if (this.texture != null) {
			if (tInf.stageCnt > 1) {
				if (this.multiTex == null) {
					int end = Config.maxTextureLayers - 1;
					this.multiTex = new int[end][this.texture.length];
					this.multiMode = new int[end][this.texture.length];
					int tl = this.texture.length;
					for (int i = 0; i < tl; i++) {
						for (int ii = 0; ii < end; ii++) {
							this.multiTex[ii][i] = -1;
						}
					}
				}
				this.objVectors.createMultiCoords();
				this.usesMultiTexturing = true;
			} else {
				this.usesMultiTexturing = false;
			}

			int number = tInf.textures[0];
			if (number != -1) {
				int tl = this.texture.length;
				for (int i = 0; i < tl; i++)
					this.texture[i] = number;
			} else {
				Logger.log("Tried to setKey an undefined texture!", 0);
			}

			for (int t = 1; t < tInf.stageCnt; t++) {
				int tid = tInf.textures[t];
				int tm = tInf.mode[t];
				int toff = t - 1;

				int[] multiModeToff = this.multiMode[toff];
				int[] multiTexToff = this.multiTex[toff];

				if (this.multiMode.length != this.multiTex.length) {
					int tl = this.texture.length;
					for (int i = 0; i < this.objMesh.anzTri; i++) {
						multiModeToff[i] = tm;
					}
					for (int i = 0; i < tl; i++)
						multiTexToff[i] = tid;
				} else {
					int tl = this.objMesh.anzTri;
					for (int i = 0; i < tl; i++) {
						multiModeToff[i] = tm;
						multiTexToff[i] = tid;
					}
				}
				float[] objVectorsuMultoff = this.objVectors.uMul[toff];
				float[] objVectorsvMultoff = this.objVectors.vMul[toff];

				for (int i = 0; i < this.objVectors.nuOrg.length; i++) {
					objVectorsuMultoff[i] = this.objVectors.nuOrg[i];
					objVectorsvMultoff[i] = this.objVectors.nvOrg[i];
				}
			}
			this.maxStagesUsed = tInf.stageCnt;
		}
	}

	public void shareTextureData(T3DObject source) {
		this.texture = source.texture;
		this.multiTex = source.multiTex;
		this.multiMode = source.multiMode;
	}

	public float rayIntersectsAABB(SimpleVector org, SimpleVector dr,
			boolean isNormalized) {
		return rayIntersectsAABB(org.x, org.y, org.z, dr.x, dr.y, dr.z,
				isNormalized);
	}

	public float rayIntersectsAABB(SimpleVector org, SimpleVector dr) {
		return rayIntersectsAABB(org.x, org.y, org.z, dr.x, dr.y, dr.z, false);
	}

	float rayIntersectsAABB(float[] org, float[] dr) {
		return rayIntersectsAABB(org[0], org[1], org[2], dr[0], dr[1], dr[2],
				false);
	}

	float rayIntersectsAABB(float[] org, float[] dr, boolean normalize) {
		return rayIntersectsAABB(org[0], org[1], org[2], dr[0], dr[1], dr[2],
				normalize);
	}

	final float rayIntersectsAABB(float org0, float org1, float org2,
			float dr0, float dr1, float dr2, boolean isNormalized) {
		if (!this.hasBoundingBox) {
			return 1.0E+012F;
		}

		float[][] invTransmat = getInverseWorldTransformation(this.mat6).mat;

		float s00Ws = invTransmat[0][0];
		float s10Ws = invTransmat[1][0];
		float s11Ws = invTransmat[1][1];
		float s21Ws = invTransmat[2][1];
		float s20Ws = invTransmat[2][0];
		float s01Ws = invTransmat[0][1];
		float s22Ws = invTransmat[2][2];
		float s12Ws = invTransmat[1][2];
		float s02Ws = invTransmat[0][2];
		float bxWs = invTransmat[3][0];
		float byWs = invTransmat[3][1];
		float bzWs = invTransmat[3][2];

		float dx = dr0 * s00Ws + dr1 * s10Ws + dr2 * s20Ws;
		float dy = dr0 * s01Ws + dr1 * s11Ws + dr2 * s21Ws;
		float dz = dr0 * s02Ws + dr1 * s12Ws + dr2 * s22Ws;

		float bx = org0 * s00Ws + org1 * s10Ws + org2 * s20Ws + bxWs;
		float by = org0 * s01Ws + org1 * s11Ws + org2 * s21Ws + byWs;
		float bz = org0 * s02Ws + org1 * s12Ws + org2 * s22Ws + bzWs;

		float tminX = -1.0E+011F;
		float tminY = -1.0E+011F;
		float tminZ = -1.0E+011F;

		float tmaxX = 1.0E+011F;
		float tmaxY = 1.0E+011F;
		float tmaxZ = 1.0E+011F;

		if (!isNormalized) {
			float dn = (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
			dx /= dn;
			dy /= dn;
			dz /= dn;
		}

		int start = this.objMesh.obbStart;

		float[] objMeshxOrg = this.objMesh.xOrg;
		float[] objMeshyOrg = this.objMesh.yOrg;
		float[] objMeshzOrg = this.objMesh.zOrg;

		float minX = objMeshxOrg[start];
		float minY = objMeshyOrg[start];
		float minZ = objMeshzOrg[start];
		float maxX = minX;
		float maxY = minY;
		float maxZ = minZ;

		for (int i = 1; i < 8; i++) {
			float x = objMeshxOrg[(i + start)];
			float z = objMeshzOrg[(i + start)];
			float y = objMeshyOrg[(i + start)];
			if (x < minX) {
				minX = x;
			} else if (x > maxX) {
				maxX = x;
			}

			if (y < minY) {
				minY = y;
			} else if (y > maxY) {
				maxY = y;
			}

			if (z < minZ) {
				minZ = z;
			} else if (z > maxZ) {
				maxZ = z;
			}

		}

		if (Math.abs(dx) > 1.0E-009F) {
			tminX = (minX - bx) / dx;
			tmaxX = (maxX - bx) / dx;
			if (tminX > tmaxX) {
				float t = tminX;
				tminX = tmaxX;
				tmaxX = t;
			}
		}

		if (Math.abs(dy) > 1.0E-009F) {
			tminY = (minY - by) / dy;
			tmaxY = (maxY - by) / dy;
			if (tminY > tmaxY) {
				float t = tminY;
				tminY = tmaxY;
				tmaxY = t;
			}
		}

		if (Math.abs(dz) > 1.0E-009F) {
			tminZ = (minZ - bz) / dz;
			tmaxZ = (maxZ - bz) / dz;
			if (tminZ > tmaxZ) {
				float t = tminZ;
				tminZ = tmaxZ;
				tmaxZ = t;
			}
		}

		float tmin = tminX;
		if (tmin < tminY) {
			tmin = tminY;
		}
		if (tmin < tminZ) {
			tmin = tminZ;
		}

		float tmax = tmaxX;
		if (tmax > tmaxY) {
			tmax = tmaxY;
		}
		if (tmax > tmaxZ) {
			tmax = tmaxZ;
		}
		if ((tmin <= tmax) && (tmax > 0.0F)) {
			return tmin;
		}
		return 1.0E+012F;
	}

	public boolean ellipsoidIntersectsAABB(SimpleVector org,
			SimpleVector ellipsoid) {
		return ellipsoidIntersectsAABB(org.x, org.y, org.z, ellipsoid);
	}

	public boolean ellipsoidIntersectsAABB(float orgx, float orgy, float orgz,
			SimpleVector ellipsoid) {
		if (!this.hasBoundingBox) {
			return false;
		}

		boolean intersects = true;

		Matrix invTrans = getInverseWorldTransformation(this.mat6);

		float[][] invTransmat = invTrans.mat;

		float s00Ws = invTransmat[0][0];
		float s10Ws = invTransmat[1][0];
		float s11Ws = invTransmat[1][1];
		float s21Ws = invTransmat[2][1];
		float s20Ws = invTransmat[2][0];
		float s01Ws = invTransmat[0][1];
		float s22Ws = invTransmat[2][2];
		float s12Ws = invTransmat[1][2];
		float s02Ws = invTransmat[0][2];
		float bxWs = invTransmat[3][0];
		float byWs = invTransmat[3][1];
		float bzWs = invTransmat[3][2];

		float bx = orgx * s00Ws + orgy * s10Ws + orgz * s20Ws + bxWs;
		float by = orgx * s01Ws + orgy * s11Ws + orgz * s21Ws + byWs;
		float bz = orgx * s02Ws + orgy * s12Ws + orgz * s22Ws + bzWs;

		float ellipsoidx = ellipsoid.x;
		float ellipsoidy = ellipsoid.y;
		float ellipsoidz = ellipsoid.z;

		float exx = Math.abs(ellipsoidx * s00Ws + ellipsoidy * s10Ws
				+ ellipsoidz * s20Ws);
		float eyy = Math.abs(ellipsoidx * s01Ws + ellipsoidy * s11Ws
				+ ellipsoidz * s21Ws);
		float ezz = Math.abs(ellipsoidx * s02Ws + ellipsoidy * s12Ws
				+ ellipsoidz * s22Ws);

		bx /= exx;
		by /= eyy;
		bz /= ezz;

		int start = this.objMesh.obbStart;

		float ex = 1.0F / exx;
		float ey = 1.0F / eyy;
		float ez = 1.0F / ezz;

		float[] objMeshxOrg = this.objMesh.xOrg;
		float[] objMeshyOrg = this.objMesh.yOrg;
		float[] objMeshzOrg = this.objMesh.zOrg;

		float minX = objMeshxOrg[start] * ex;
		float minY = objMeshyOrg[start] * ey;
		float minZ = objMeshzOrg[start] * ez;
		float maxX = minX;
		float maxY = minY;
		float maxZ = minZ;

		int s = 1 + start;
		int e = 8 + start;
		for (int i = s; i < e; i++) {
			float x = objMeshxOrg[i] * ex;
			float y = objMeshyOrg[i] * ey;
			float z = objMeshzOrg[i] * ez;
			if (x < minX) {
				minX = x;
			} else if (x > maxX) {
				maxX = x;
			}

			if (y < minY) {
				minY = y;
			} else if (y > maxY) {
				maxY = y;
			}

			if (z < minZ) {
				minZ = z;
			} else if (z > maxZ) {
				maxZ = z;
			}

		}

		if ((bx + 1.0F < minX) || (bx - 1.0F > maxX) || (by + 1.0F < minY)
				|| (by - 1.0F > maxY) || (bz + 1.0F < minZ)
				|| (bz - 1.0F > maxZ)) {
			intersects = false;
		}

		return intersects;
	}

	public boolean sphereIntersectsAABB(SimpleVector org, float radius) {
		return sphereIntersectsAABB(org.toArray(), radius);
	}

	final boolean sphereIntersectsAABB(float[] org, float radius) {
		if (!this.hasBoundingBox) {
			return false;
		}

		boolean intersects = true;

		Matrix invTrans = getInverseWorldTransformation(this.mat6);

		float s00Ws = invTrans.mat[0][0];
		float s10Ws = invTrans.mat[1][0];
		float s11Ws = invTrans.mat[1][1];
		float s21Ws = invTrans.mat[2][1];
		float s20Ws = invTrans.mat[2][0];
		float s01Ws = invTrans.mat[0][1];
		float s22Ws = invTrans.mat[2][2];
		float s12Ws = invTrans.mat[1][2];
		float s02Ws = invTrans.mat[0][2];
		float bxWs = invTrans.mat[3][0];
		float byWs = invTrans.mat[3][1];
		float bzWs = invTrans.mat[3][2];

		float bx = org[0] * s00Ws + org[1] * s10Ws + org[2] * s20Ws + bxWs;
		float by = org[0] * s01Ws + org[1] * s11Ws + org[2] * s21Ws + byWs;
		float bz = org[0] * s02Ws + org[1] * s12Ws + org[2] * s22Ws + bzWs;

		int start = this.objMesh.obbStart;

		float minX = this.objMesh.xOrg[start];
		float minY = this.objMesh.yOrg[start];
		float minZ = this.objMesh.zOrg[start];
		float maxX = minX;
		float maxY = minY;
		float maxZ = minZ;

		for (int i = 1; i < 8; i++) {
			float x = this.objMesh.xOrg[(i + start)];
			float z = this.objMesh.zOrg[(i + start)];
			float y = this.objMesh.yOrg[(i + start)];
			if (x < minX) {
				minX = x;
			} else if (x > maxX) {
				maxX = x;
			}

			if (y < minY) {
				minY = y;
			} else if (y > maxY) {
				maxY = y;
			}

			if (z < minZ) {
				minZ = z;
			} else if (z > maxZ) {
				maxZ = z;
			}

		}

		if ((bx + radius < minX) || (bx - radius > maxX)
				|| (by + radius < minY) || (by - radius > maxY)
				|| (bz + radius < minZ) || (bz - radius > maxZ)) {
			intersects = false;
		}

		return intersects;
	}

	final float collide(float[] org, float[] dr, float breakRange,
			float spanRange) {
		return collide(org, dr, breakRange, spanRange, true);
	}

	private final float collide(float[] org, float[] dr, float breakRange,
			float spanRange, boolean mayOptimize) {
		createCollisionArrays();

		float collideOffset = Config.collideOffset;
		if ((this.optimizeColDet) && (this.largestPolygonSize != -1.0F)
				&& (mayOptimize)) {
			float newOffset = this.largestPolygonSize + 2.0F + breakRange;
			if (newOffset < collideOffset) {
				collideOffset = newOffset;
			}

		}

		float[][] invTransmat = getInverseWorldTransformation(this.mat6).mat;

		float s00Ws = invTransmat[0][0];
		float s10Ws = invTransmat[1][0];
		float s11Ws = invTransmat[1][1];
		float s21Ws = invTransmat[2][1];
		float s20Ws = invTransmat[2][0];
		float s01Ws = invTransmat[0][1];
		float s22Ws = invTransmat[2][2];
		float s12Ws = invTransmat[1][2];
		float s02Ws = invTransmat[0][2];
		float bxWs = invTransmat[3][0];
		float byWs = invTransmat[3][1];
		float bzWs = invTransmat[3][2];

		float dr0 = dr[0];
		float dr1 = dr[1];
		float dr2 = dr[2];

		float org0 = org[0];
		float org1 = org[1];
		float org2 = org[2];

		float dirCol0 = dr0 * s00Ws + dr1 * s10Ws + dr2 * s20Ws;
		float dirCol1 = dr0 * s01Ws + dr1 * s11Ws + dr2 * s21Ws;
		float dirCol2 = dr0 * s02Ws + dr1 * s12Ws + dr2 * s22Ws;

		float bx = org0 * s00Ws + org1 * s10Ws + org2 * s20Ws + bxWs;
		float by = org0 * s01Ws + org1 * s11Ws + org2 * s21Ws + byWs;
		float bz = org0 * s02Ws + org1 * s12Ws + org2 * s22Ws + bzWs;

		float origCol0 = bx;
		float origCol1 = by;
		float origCol2 = bz;

		int touchedLeafs = 0;

		Object[] leafArray = (Object[]) null;

		if ((this.ocTree != null) && (this.ocTree.getCollisionUse())) {
			float radius = spanRange + breakRange;
			leafArray = this.ocTree.getColliderLeafs(origCol0, origCol1,
					origCol2, radius * this.ocTree.getRadiusMultiplier());

			touchedLeafs = ((Integer) leafArray[0]).intValue();

			if (touchedLeafs == 0) {
				return 1.0E+012F;
			}

		}

		int taken = 0;
		int kicked = 0;
		int tested = 0;

		boolean exitLoop = false;

		float lastT = 1.0E+012F;
		int lastPoly = -1;

		int start = 0;
		int end = 0;

		start = 0;
		end = this.objMesh.anzTri;

		boolean useOcTree = false;
		OcTreeNode[] leafList = (OcTreeNode[]) null;
		int[] polyList = (int[]) null;
		int nodeCnt = 0;

		if ((this.ocTree != null) && (this.ocTree.getCollisionUse())) {
			leafList = (OcTreeNode[]) leafArray[1];
			useOcTree = true;
		}

		int[] objMeshcoords = this.objMesh.coords;
		float[] objMeshxOrg = this.objMesh.xOrg;
		float[] objMeshyOrg = this.objMesh.yOrg;
		float[] objMeshzOrg = this.objMesh.zOrg;
		do {
			if (useOcTree) {
				polyList = leafList[nodeCnt].getPolygons();
				start = 0;
				end = leafList[nodeCnt].getPolyCount();
				nodeCnt++;
			}

			int[][] objMeshpoints = this.objMesh.points;
			float collideOffsetMul = collideOffset * collideOffset;

			for (int k = start; k < end; k++) {
				int p = k;
				if (useOcTree) {
					p = polyList[k];
				}

				int[] objMeshpointsp = objMeshpoints[p];
				int p0 = objMeshcoords[objMeshpointsp[0]];

				float v1x = objMeshxOrg[p0];
				float v1y = objMeshyOrg[p0];
				float v1z = objMeshzOrg[p0];

				float dx = v1x - bx;
				float dy = v1y - by;
				float dz = v1z - bz;

				if ((spanRange == 1.0E+012F)
						|| ((dx * dx <= collideOffsetMul)
								&& (dy * dy <= collideOffsetMul) && (dz * dz <= collideOffsetMul))) {
					int p2 = objMeshcoords[objMeshpointsp[2]];
					int p1 = objMeshcoords[objMeshpointsp[1]];

					float edge1Col0 = objMeshxOrg[p1] - v1x;
					float edge1Col1 = objMeshyOrg[p1] - v1y;
					float edge1Col2 = objMeshzOrg[p1] - v1z;

					float edge2Col0 = objMeshxOrg[p2] - v1x;
					float edge2Col1 = objMeshyOrg[p2] - v1y;
					float edge2Col2 = objMeshzOrg[p2] - v1z;

					float pvecCol0 = dirCol1 * edge2Col2 - dirCol2 * edge2Col1;
					float pvecCol1 = dirCol2 * edge2Col0 - dirCol0 * edge2Col2;
					float pvecCol2 = dirCol0 * edge2Col1 - dirCol1 * edge2Col0;

					float det = edge1Col0 * pvecCol0 + edge1Col1 * pvecCol1
							+ edge1Col2 * pvecCol2;

					if (det >= 1.0E-009F) {
						float invDet = 1.0F / det;

						float tvecCol0 = bx - v1x;
						float tvecCol1 = by - v1y;
						float tvecCol2 = bz - v1z;

						float u = (tvecCol0 * pvecCol0 + tvecCol1 * pvecCol1 + tvecCol2
								* pvecCol2)
								* invDet;

						if ((u >= 0.0D) && (u <= 1.0F)) {
							float qvecCol0 = tvecCol1 * edge1Col2 - tvecCol2
									* edge1Col1;
							float qvecCol1 = tvecCol2 * edge1Col0 - tvecCol0
									* edge1Col2;
							float qvecCol2 = tvecCol0 * edge1Col1 - tvecCol1
									* edge1Col0;

							float v = (dirCol0 * qvecCol0 + dirCol1 * qvecCol1 + dirCol2
									* qvecCol2)
									* invDet;

							if ((v >= 0.0D) && (u + v <= 1.0D)) {
								float t = (edge2Col0 * qvecCol0 + edge2Col1
										* qvecCol1 + edge2Col2 * qvecCol2)
										* invDet;

								if ((t < breakRange) && (t >= 0.0F)) {
									exitLoop = true;
									lastT = t;
									lastPoly = p;
								} else {
									exitLoop = false;
									if ((t < lastT) && (t >= 0.0F)) {
										lastT = t;
										lastPoly = p;
									}
								}

								taken++;
							}
						}
					}
				} else {
					kicked++;
				}
				tested++;

				if (exitLoop) {
					p = end;
					break;
				}
			}
		} while ((useOcTree) && (nodeCnt < touchedLeafs));

		if (lastPoly != -1) {
			addPolygonID(lastPoly);
		}

		return lastT;
	}

	final float[] collideSpherical(float[] org, float radius, float spanRange,
			boolean[] colli, boolean camMode) {
		float edge10x = 0.0F;
		float edge10y = 0.0F;
		float edge10z = 0.0F;
		float edge20x = 0.0F;
		float edge20y = 0.0F;
		float edge20z = 0.0F;
		float origx = 0.0F;
		float origy = 0.0F;
		float origz = 0.0F;

		Matrix trans = null;

		float collideOffset = Config.collideOffset;
		if ((this.optimizeColDet) && (this.largestPolygonSize != -1.0F)) {
			float newOffset = this.largestPolygonSize + radius + 1.0F;
			if (newOffset < collideOffset) {
				collideOffset = newOffset;
			}

		}

		trans = getWorldTransformation(this.mat5);
		Matrix invTrans = getInverseWorldTransformation(this.mat6);

		float[][] invTransmat = invTrans.mat;

		float s00Ws = invTransmat[0][0];
		float s10Ws = invTransmat[1][0];
		float s11Ws = invTransmat[1][1];
		float s21Ws = invTransmat[2][1];
		float s20Ws = invTransmat[2][0];
		float s01Ws = invTransmat[0][1];
		float s22Ws = invTransmat[2][2];
		float s12Ws = invTransmat[1][2];
		float s02Ws = invTransmat[0][2];
		float bxWs = invTransmat[3][0];
		float byWs = invTransmat[3][1];
		float bzWs = invTransmat[3][2];

		float bx = org[0] * s00Ws + org[1] * s10Ws + org[2] * s20Ws + bxWs;
		float by = org[0] * s01Ws + org[1] * s11Ws + org[2] * s21Ws + byWs;
		float bz = org[0] * s02Ws + org[1] * s12Ws + org[2] * s22Ws + bzWs;

		origx = bx;
		origy = by;
		origz = bz;

		int touchedLeafs = 0;

		Object[] leafArray = (Object[]) null;

		if ((this.ocTree != null) && (this.ocTree.getCollisionUse())) {
			leafArray = this.ocTree.getColliderLeafs(origx, origy, origz,
					radius * this.ocTree.getRadiusMultiplier());

			touchedLeafs = ((Integer) leafArray[0]).intValue();

			if (touchedLeafs == 0) {
				return org;
			}

		}

		boolean col = false;

		int start = 0;
		int end = this.objMesh.anzTri;

		float dist = 0.0F;

		boolean useOcTree = false;
		OcTreeNode[] leafList = (OcTreeNode[]) null;
		int[] polyList = (int[]) null;
		int nodeCnt = 0;

		if ((this.ocTree != null) && (this.ocTree.getCollisionUse())) {
			leafList = (OcTreeNode[]) leafArray[1];
			useOcTree = true;
		}

		int[] objMeshcoords = this.objMesh.coords;
		float[] objMeshxOrg = this.objMesh.xOrg;
		float[] objMeshyOrg = this.objMesh.yOrg;
		float[] objMeshzOrg = this.objMesh.zOrg;

		int checks = 0;
		do {
			if (useOcTree) {
				polyList = leafList[nodeCnt].getPolygons();
				start = 0;
				end = leafList[nodeCnt].getPolyCount();
				nodeCnt++;
			}

			int[][] objMeshpoints = this.objMesh.points;
			float collideOffsetMul = collideOffset * collideOffset;

			for (int k = start; k < end; k++) {
				checks++;

				int p = k;
				if (useOcTree) {
					p = polyList[k];
				}

				int[] objMeshpointsp = objMeshpoints[p];
				int p0 = objMeshcoords[objMeshpointsp[0]];

				float v1x = this.objMesh.xOrg[p0];
				float v1y = this.objMesh.yOrg[p0];
				float v1z = this.objMesh.zOrg[p0];

				boolean collision = false;
				float dx = v1x - bx;
				float dy = v1y - by;
				float dz = v1z - bz;

				if ((spanRange == 1.0E+012F)
						|| ((dx * dx <= collideOffsetMul)
								&& (dy * dy <= collideOffsetMul) && (dz * dz <= collideOffsetMul))) {
					int p2 = objMeshcoords[objMeshpointsp[2]];
					int p1 = objMeshcoords[objMeshpointsp[1]];

					float edge1x = objMeshxOrg[p1] - v1x;
					float edge1y = objMeshyOrg[p1] - v1y;
					float edge1z = objMeshzOrg[p1] - v1z;

					float edge2x = objMeshxOrg[p2] - v1x;
					float edge2y = objMeshyOrg[p2] - v1y;
					float edge2z = objMeshzOrg[p2] - v1z;

					float nx = edge1y * edge2z - edge1z * edge2y;
					float ny = edge1z * edge2x - edge1x * edge2z;
					float nz = edge1x * edge2y - edge1y * edge2x;

					float nn = (float) Math.sqrt(nx * nx + ny * ny + nz * nz);
					nx /= nn;
					ny /= nn;
					nz /= nn;

					float camDist = nx * origx + ny * origy + nz * origz
							- (nx * v1x + ny * v1y + nz * v1z);

					if (Math.abs(camDist) < radius) {
						float posx = origx - nx * camDist;
						float posy = origy - ny * camDist;
						float posz = origz - nz * camDist;

						float allAngles = 0.0F;

						for (int j = 0; j < 3; j++) {
							int tp = objMeshcoords[objMeshpointsp[j]];

							float x = objMeshxOrg[tp];
							float y = objMeshyOrg[tp];
							float z = objMeshzOrg[tp];

							edge10x = x - posx;
							edge10y = y - posy;
							edge10z = z - posz;

							int nj = (j + 1) % 3;

							tp = objMeshcoords[objMeshpointsp[nj]];

							x = objMeshxOrg[tp];
							y = objMeshyOrg[tp];
							z = objMeshzOrg[tp];

							edge20x = x - posx;
							edge20y = y - posy;
							edge20z = z - posz;

							float dot = MathUtils.calcDot(edge10x, edge10y,
									edge10z, edge20x, edge20y, edge20z);

							float len1 = (float) Math.sqrt(edge10x * edge10x
									+ edge10y * edge10y + edge10z * edge10z);
							float len2 = (float) Math.sqrt(edge20x * edge20x
									+ edge20y * edge20y + edge20z * edge20z);

							float magMul = len1 * len2;
							float angle = (float) Math.acos(dot / magMul);

							if (Float.isNaN(angle)) {
								angle = 0.0F;
							}

							allAngles += angle;

							if (allAngles >= 6.220354F) {
								break;
							}
						}
						if (allAngles >= 6.220354F)
							collision = true;
						else {
							for (int j = 0; j < 3; j++) {
								int tp = objMeshcoords[objMeshpointsp[j]];

								float x1 = objMeshxOrg[tp];
								float y1 = objMeshyOrg[tp];
								float z1 = objMeshzOrg[tp];

								edge10x = origx - x1;
								edge10y = origy - y1;
								edge10z = origz - z1;

								int nj = (j + 1) % 3;

								tp = objMeshcoords[objMeshpointsp[nj]];

								float x2 = objMeshxOrg[tp];
								float y2 = objMeshyOrg[tp];
								float z2 = objMeshzOrg[tp];

								edge20x = x2 - x1;
								edge20y = y2 - y1;
								edge20z = z2 - z1;

								nn = (float) Math
										.sqrt(edge20x * edge20x + edge20y
												* edge20y + edge20z * edge20z);

								edge20x /= nn;
								edge20y /= nn;
								edge20z /= nn;

								float dot = MathUtils.calcDot(edge10x, edge10y,
										edge10z, edge20x, edge20y, edge20z);

								if (dot <= 0.0F) {
									posx = x1;
									posy = y1;
									posz = z1;
								} else {
									dist = (float) Math.sqrt((x1 - x2)
											* (x1 - x2) + (y1 - y2) * (y1 - y2)
											+ (z1 - z2) * (z1 - z2));
									if (dot >= dist) {
										posx = x2;
										posy = y2;
										posz = z2;
									} else {
										edge10x = edge20x * dot;
										edge10y = edge20y * dot;
										edge10z = edge20z * dot;

										posx = x1 + edge10x;
										posy = y1 + edge10y;
										posz = z1 + edge10z;
									}
								}

								dist = (float) Math.sqrt((posx - origx)
										* (posx - origx) + (posy - origy)
										* (posy - origy) + (posz - origz)
										* (posz - origz));

								float rad = radius;
								if (camMode) {
									rad *= Config.collideEdgeMul;
								}

								if (dist < rad) {
									collision = true;
									break;
								}
							}

						}

						if (collision) {
							addPolygonID(p);
							float back = radius - camDist;
							col = true;
							origx += nx * back;
							origy += ny * back;
							origz += nz * back;
						}
					}
				}
			}
		} while ((useOcTree) && (nodeCnt < touchedLeafs));

		float[][] transmat = trans.mat;

		if (Logger.isDebugEnabled()) {
			Logger.log("Polygons checked: " + checks, 3);
		}

		s00Ws = transmat[0][0];
		s10Ws = transmat[1][0];
		s11Ws = transmat[1][1];
		s21Ws = transmat[2][1];
		s20Ws = transmat[2][0];
		s01Ws = transmat[0][1];
		s22Ws = transmat[2][2];
		s12Ws = transmat[1][2];
		s02Ws = transmat[0][2];
		bxWs = transmat[3][0];
		byWs = transmat[3][1];
		bzWs = transmat[3][2];

		bx = origx * s00Ws + origy * s10Ws + origz * s20Ws + bxWs;
		by = origx * s01Ws + origy * s11Ws + origz * s21Ws + byWs;
		bz = origx * s02Ws + origy * s12Ws + origz * s22Ws + bzWs;

		org[0] = bx;
		org[1] = by;
		org[2] = bz;

		colli[0] |= col;
		return org;
	}

	final void collideEllipsoid(CollisionInfo cInf, float spanRange) {
		cInf.setScale(this.scaleFactor);

		createCollisionArrays();
		Matrix invAddTrans = null;
		Matrix invAddRot = null;

		boolean transRequired = false;
		float r00 = 0.0F;
		float r10 = 0.0F;
		float r11 = 0.0F;
		float r21 = 0.0F;
		float r20 = 0.0F;
		float r01 = 0.0F;
		float r22 = 0.0F;
		float r12 = 0.0F;
		float r02 = 0.0F;
		float rbx = 0.0F;
		float rby = 0.0F;
		float rbz = 0.0F;

		if (cInf.addTransMat != null) {
			transRequired = true;
			invAddTrans = cInf.addTransMat.invert();
			Matrix inv = getWorldTransformation();

			inv.matMul(invAddTrans);
			invAddRot = cInf.addRotMat.invert3x3();

			float[][] invmat = inv.mat;

			r00 = invmat[0][0];
			r10 = invmat[1][0];
			r11 = invmat[1][1];
			r21 = invmat[2][1];
			r20 = invmat[2][0];
			r01 = invmat[0][1];
			r22 = invmat[2][2];
			r12 = invmat[1][2];
			r02 = invmat[0][2];
			rbx = invmat[3][0];
			rby = invmat[3][1];
			rbz = invmat[3][2];
		}

		SimpleVector cInfr3Pos = cInf.r3Pos;
		SimpleVector cInfr3Velocity = cInf.r3Velocity;

		float orgx = cInfr3Pos.x;
		float orgy = cInfr3Pos.y;
		float orgz = cInfr3Pos.z;

		float origx = orgx;
		float origy = orgy;
		float origz = orgz;

		float origOcx = orgx;
		float origOcy = orgy;
		float origOcz = orgz;

		float vOcx = cInfr3Velocity.x;
		float vOcy = cInfr3Velocity.y;
		float vOcz = cInfr3Velocity.z;

		float vOrgx = vOcx;
		float vOrgy = vOcy;
		float vOrgz = vOcz;

		SimpleVector velocity = new SimpleVector();
		SimpleVector basePoint = new SimpleVector();
		SimpleVector collisionPoint = new SimpleVector();

		float distyx = 0.0F;
		float distyy = 0.0F;
		float distyz = 0.0F;

		boolean useOcTree = (this.ocTree != null)
				&& (this.ocTree.getCollisionUse());
		Matrix invTrans = null;

		if ((!transRequired) || (useOcTree)) {
			invTrans = getInverseWorldTransformation(this.mat6);

			float[][] invTransmat = invTrans.mat;

			float s00Ws = invTransmat[0][0];
			float s10Ws = invTransmat[1][0];
			float s11Ws = invTransmat[1][1];
			float s21Ws = invTransmat[2][1];
			float s20Ws = invTransmat[2][0];
			float s01Ws = invTransmat[0][1];
			float s22Ws = invTransmat[2][2];
			float s12Ws = invTransmat[1][2];
			float s02Ws = invTransmat[0][2];
			float bxWs = invTransmat[3][0];
			float byWs = invTransmat[3][1];
			float bzWs = invTransmat[3][2];

			origx = orgx * s00Ws + orgy * s10Ws + orgz * s20Ws + bxWs;
			origy = orgx * s01Ws + orgy * s11Ws + orgz * s21Ws + byWs;
			origz = orgx * s02Ws + orgy * s12Ws + orgz * s22Ws + bzWs;

			float vx = vOrgx * s00Ws + vOrgy * s10Ws + vOrgz * s20Ws;
			float vy = vOrgx * s01Ws + vOrgy * s11Ws + vOrgz * s21Ws;
			float vz = vOrgx * s02Ws + vOrgy * s12Ws + vOrgz * s22Ws;

			vOrgx = vx;
			vOrgy = vy;
			vOrgz = vz;

			origOcx = origx;
			origOcy = origy;
			origOcz = origz;

			vOcx = vOrgx;
			vOcy = vOrgy;
			vOcz = vOrgz;
		}

		if (transRequired) {
			invTrans = invAddTrans;

			vOrgx = cInf.r3Velocity.x;
			vOrgy = cInf.r3Velocity.y;
			vOrgz = cInf.r3Velocity.z;

			float[][] invTransmat = invTrans.mat;

			float s00Ws = invTransmat[0][0];
			float s10Ws = invTransmat[1][0];
			float s11Ws = invTransmat[1][1];
			float s21Ws = invTransmat[2][1];
			float s20Ws = invTransmat[2][0];
			float s01Ws = invTransmat[0][1];
			float s22Ws = invTransmat[2][2];
			float s12Ws = invTransmat[1][2];
			float s02Ws = invTransmat[0][2];
			float bxWs = invTransmat[3][0];
			float byWs = invTransmat[3][1];
			float bzWs = invTransmat[3][2];

			origx = orgx * s00Ws + orgy * s10Ws + orgz * s20Ws + bxWs;
			origy = orgx * s01Ws + orgy * s11Ws + orgz * s21Ws + byWs;
			origz = orgx * s02Ws + orgy * s12Ws + orgz * s22Ws + bzWs;

			SimpleVector vt = SimpleVector.create(vOrgx, vOrgy, vOrgz);
			vt.matMul(invAddRot);
			vOrgx = vt.x;
			vOrgy = vt.y;
			vOrgz = vt.z;
		}

		int touchedLeafs = 0;

		Object[] leafArray = (Object[]) null;

		if (useOcTree) {
			float velLength = (float) Math.sqrt(vOcx * vOcx + vOcy * vOcy
					+ vOcz * vOcz);

			leafArray = this.ocTree.getColliderLeafs(
					origOcx,
					origOcy,
					origOcz,
					(cInf.getMaxRadius() + velLength)
							* this.ocTree.getRadiusMultiplier());

			touchedLeafs = ((Integer) leafArray[0]).intValue();

			if (touchedLeafs == 0) {
				return;
			}

		}

		int start = 0;
		int end = this.objMesh.anzTri;

		OcTreeNode[] leafList = (OcTreeNode[]) null;
		int[] polyList = (int[]) null;
		int nodeCnt = 0;

		if (useOcTree) {
			leafList = (OcTreeNode[]) leafArray[1];
		}

		basePoint.x = (origx * cInf.invERadiusx);
		basePoint.y = (origy * cInf.invERadiusy);
		basePoint.z = (origz * cInf.invERadiusz);

		velocity.x = (vOrgx * cInf.invERadiusx);
		velocity.y = (vOrgy * cInf.invERadiusy);
		velocity.z = (vOrgz * cInf.invERadiusz);

		float normalizedVelocityX = velocity.x;
		float normalizedVelocityY = velocity.y;
		float normalizedVelocityZ = velocity.z;

		float len = velocity.length();
		if (len != 0.0F) {
			normalizedVelocityX /= len;
			normalizedVelocityY /= len;
			normalizedVelocityZ /= len;
		} else {
			normalizedVelocityX = 0.0F;
			normalizedVelocityY = 0.0F;
			normalizedVelocityZ = 0.0F;
		}

		int[][] objMeshpoint = this.objMesh.points;
		int[] objMeshcoords = this.objMesh.coords;
		float[] objMeshxOrg = this.objMesh.xOrg;
		float[] objMeshyOrg = this.objMesh.yOrg;
		float[] objMeshzOrg = this.objMesh.zOrg;

		int checks = 0;
		int skipped = 0;

		float collideOffset = Config.collideOffset;
		if ((this.optimizeColDet) && (this.largestPolygonSize != -1.0F)) {
			float newOffset = this.largestPolygonSize + 1.0F;
			if (newOffset < collideOffset) {
				collideOffset = newOffset;
			}
		}
		collideOffset *= collideOffset;
		do {
			if (useOcTree) {
				polyList = leafList[nodeCnt].getPolygons();
				start = 0;
				end = leafList[nodeCnt].getPolyCount();
				nodeCnt++;
			}

			for (int k = start; k < end; k++) {
				int p = k;
				if (useOcTree) {
					p = polyList[k];
				}

				int[] objMeshpointsp = objMeshpoint[p];

				int pi0 = objMeshcoords[objMeshpointsp[0]];
				int pi1 = objMeshcoords[objMeshpointsp[1]];
				int pi2 = objMeshcoords[objMeshpointsp[2]];
				float p2z;
				float p0x;
				float p0y;
				float p0z;
				float p1x;
				float p1y;
				float p1z;
				float p2x;
				float p2y;

				if (transRequired) {
					float px = objMeshxOrg[pi0];
					float py = objMeshyOrg[pi0];
					float pz = objMeshzOrg[pi0];

					float tpx = px * r00 + py * r10 + pz * r20 + rbx;
					float tpy = px * r01 + py * r11 + pz * r21 + rby;
					float tpz = px * r02 + py * r12 + pz * r22 + rbz;

					p0x = tpx * cInf.invERadiusx;
					p0y = tpy * cInf.invERadiusy;
					p0z = tpz * cInf.invERadiusz;

					px = objMeshxOrg[pi1];
					py = objMeshyOrg[pi1];
					pz = objMeshzOrg[pi1];

					tpx = px * r00 + py * r10 + pz * r20 + rbx;
					tpy = px * r01 + py * r11 + pz * r21 + rby;
					tpz = px * r02 + py * r12 + pz * r22 + rbz;

					p1x = tpx * cInf.invERadiusx;
					p1y = tpy * cInf.invERadiusy;
					p1z = tpz * cInf.invERadiusz;

					px = objMeshxOrg[pi2];
					py = objMeshyOrg[pi2];
					pz = objMeshzOrg[pi2];

					tpx = px * r00 + py * r10 + pz * r20 + rbx;
					tpy = px * r01 + py * r11 + pz * r21 + rby;
					tpz = px * r02 + py * r12 + pz * r22 + rbz;

					p2x = tpx * cInf.invERadiusx;
					p2y = tpy * cInf.invERadiusy;
					p2z = tpz * cInf.invERadiusz;
				} else {
					p0x = objMeshxOrg[pi0] * cInf.invERadiusx;
					p0y = objMeshyOrg[pi0] * cInf.invERadiusy;
					p0z = objMeshzOrg[pi0] * cInf.invERadiusz;

					p1x = objMeshxOrg[pi1] * cInf.invERadiusx;
					p1y = objMeshyOrg[pi1] * cInf.invERadiusy;
					p1z = objMeshzOrg[pi1] * cInf.invERadiusz;

					p2x = objMeshxOrg[pi2] * cInf.invERadiusx;
					p2y = objMeshyOrg[pi2] * cInf.invERadiusy;
					p2z = objMeshzOrg[pi2] * cInf.invERadiusz;
				}

				if ((this.optimizeColDet)
						&& (getSquaredDistance(basePoint, p0x, p0y, p0z) > collideOffset)
						&& (getSquaredDistance(basePoint, p1x, p1y, p1z) > collideOffset)
						&& (getSquaredDistance(basePoint, p2x, p2y, p2z) > collideOffset)) {
					skipped++;
				} else {
					checks++;

					this.trianglePlane.setTo(p0x, p0y, p0z, p1x, p1y, p1z, p2x,
							p2y, p2z);

					boolean possible = true;

					if (this.trianglePlane.isFrontFacingTo(normalizedVelocityX,
							normalizedVelocityY, normalizedVelocityZ)) {
						float t0 = 0.0F;
						float t1 = 0.0F;
						boolean embeddedInPlane = false;

						float signedDistToTrianglePlane = this.trianglePlane
								.distanceTo(basePoint);

						SimpleVector nt = this.trianglePlane.normal;
						float normalDotVelocity = nt.x * velocity.x + nt.y
								* velocity.y + nt.z * velocity.z;

						if (normalDotVelocity == 0.0F) {
							if (Math.abs(signedDistToTrianglePlane) >= 1.0F) {
								possible = false;
							} else {
								embeddedInPlane = true;
								t0 = 0.0F;
								t1 = 1.0F;
							}
						} else {
							float tmp = -1.0F / normalDotVelocity;
							float tmp2 = signedDistToTrianglePlane * tmp;
							t0 = tmp2 + tmp;
							t1 = tmp2 - tmp;

							if (t0 > t1) {
								float temp = t0;
								t0 = t1;
								t1 = temp;
							}

							if ((t0 > 1.0F) || (t1 < 0.0F)) {
								possible = false;
							}

							if (t0 < -1.0F)
								t0 = 0.0F;
							else if (t0 > 1.0F) {
								t0 = 1.0F;
							}
							if (t1 < 0.0F)
								t1 = 0.0F;
							else if (t1 > 1.0F) {
								t1 = 1.0F;
							}
						}

						if (possible) {
							collisionPoint.set(0.0F, 0.0F, 0.0F);
							boolean foundCollision = false;
							float t = 1.0F;

							if (!embeddedInPlane) {
								this.t0Vel.set(velocity);
								this.t0Vel.scalarMul(t0);
								this.planeIntersectionPoint.set(basePoint);
								this.planeIntersectionPoint
										.sub(this.trianglePlane.normal);
								this.planeIntersectionPoint.add(this.t0Vel);

								this.fromBaseToIntersection
										.set(this.planeIntersectionPoint);
								this.fromBaseToIntersection.sub(basePoint);
								if (checkPointInTriangle(
										this.fromBaseToIntersection, basePoint,
										p0x, p0y, p0z, p1x, p1y, p1z, p2x, p2y,
										p2z)) {
									foundCollision = true;
									t = t0;
									collisionPoint
											.set(this.planeIntersectionPoint);
								}
							}

							if (!foundCollision) {
								SimpleVector base = basePoint;
								float velocitySquaredLength = velocity.x
										* velocity.x + velocity.y * velocity.y
										+ velocity.z * velocity.z;
								float a = 0.0F;
								float b = 0.0F;
								float c = 0.0F;

								distyx = base.x - p0x;
								distyy = base.y - p0y;
								distyz = base.z - p0z;

								a = velocitySquaredLength;
								b = 2.0F * (velocity.x * distyx + velocity.y
										* distyy + velocity.z * distyz);

								distyx = p0x - base.x;
								distyy = p0y - base.y;
								distyz = p0z - base.z;

								c = distyx * distyx + distyy * distyy + distyz
										* distyz - 1.0F;
								if (getLowestRoot(a, b, c, t, this.newT)) {
									t = this.newT[0];
									foundCollision = true;
									collisionPoint.set(p0x, p0y, p0z);
								}

								distyx = base.x - p1x;
								distyy = base.y - p1y;
								distyz = base.z - p1z;

								a = velocitySquaredLength;
								b = 2.0F * (velocity.x * distyx + velocity.y
										* distyy + velocity.z * distyz);

								distyx = p1x - base.x;
								distyy = p1y - base.y;
								distyz = p1z - base.z;

								c = distyx * distyx + distyy * distyy + distyz
										* distyz - 1.0F;
								if (getLowestRoot(a, b, c, t, this.newT)) {
									t = this.newT[0];
									foundCollision = true;
									collisionPoint.set(p1x, p1y, p1z);
								}

								distyx = base.x - p2x;
								distyy = base.y - p2y;
								distyz = base.z - p2z;

								a = velocitySquaredLength;
								b = 2.0F * (velocity.x * distyx + velocity.y
										* distyy + velocity.z * distyz);

								distyx = p2x - base.x;
								distyy = p2y - base.y;
								distyz = p2z - base.z;

								c = distyx * distyx + distyy * distyy + distyz
										* distyz - 1.0F;
								if (getLowestRoot(a, b, c, t, this.newT)) {
									t = this.newT[0];
									foundCollision = true;
									collisionPoint.set(p2x, p2y, p2z);
								}

								distyx = p1x - p0x;
								distyy = p1y - p0y;
								distyz = p1z - p0z;

								float baseToVertexx = p0x - base.x;
								float baseToVertexy = p0y - base.y;
								float baseToVertexz = p0z - base.z;

								float edgeSquaredLength = distyx * distyx
										+ distyy * distyy + distyz * distyz;
								float edgeDotVelocity = distyx * velocity.x
										+ distyy * velocity.y + distyz
										* velocity.z;
								float edgeDotBaseToVertex = distyx
										* baseToVertexx + distyy
										* baseToVertexy + distyz
										* baseToVertexz;

								a = edgeSquaredLength * -velocitySquaredLength
										+ edgeDotVelocity * edgeDotVelocity;
								b = edgeSquaredLength
										* (2.0F * (velocity.x * baseToVertexx
												+ velocity.y * baseToVertexy + velocity.z
												* baseToVertexz)) - 2.0F
										* edgeDotVelocity * edgeDotBaseToVertex;
								c = edgeSquaredLength
										* (1.0F - (baseToVertexx
												* baseToVertexx + baseToVertexy
												* baseToVertexy + baseToVertexz
												* baseToVertexz))
										+ edgeDotBaseToVertex
										* edgeDotBaseToVertex;

								if (getLowestRoot(a, b, c, t, this.newT)) {
									float f = (edgeDotVelocity * this.newT[0] - edgeDotBaseToVertex)
											/ edgeSquaredLength;
									if ((f >= 0.0F) && (f <= 1.0F)) {
										t = this.newT[0];
										foundCollision = true;
										collisionPoint.set(distyx, distyy,
												distyz);
										collisionPoint.scalarMul(f);
										collisionPoint.add(p0x, p0y, p0z);
									}

								}

								distyx = p2x - p1x;
								distyy = p2y - p1y;
								distyz = p2z - p1z;

								baseToVertexx = p1x - base.x;
								baseToVertexy = p1y - base.y;
								baseToVertexz = p1z - base.z;

								edgeSquaredLength = distyx * distyx + distyy
										* distyy + distyz * distyz;
								edgeDotVelocity = distyx * velocity.x + distyy
										* velocity.y + distyz * velocity.z;
								edgeDotBaseToVertex = distyx * baseToVertexx
										+ distyy * baseToVertexy + distyz
										* baseToVertexz;

								a = edgeSquaredLength * -velocitySquaredLength
										+ edgeDotVelocity * edgeDotVelocity;
								b = edgeSquaredLength
										* (2.0F * (velocity.x * baseToVertexx
												+ velocity.y * baseToVertexy + velocity.z
												* baseToVertexz)) - 2.0F
										* edgeDotVelocity * edgeDotBaseToVertex;
								c = edgeSquaredLength
										* (1.0F - (baseToVertexx
												* baseToVertexx + baseToVertexy
												* baseToVertexy + baseToVertexz
												* baseToVertexz))
										+ edgeDotBaseToVertex
										* edgeDotBaseToVertex;

								if (getLowestRoot(a, b, c, t, this.newT)) {
									float f = (edgeDotVelocity * this.newT[0] - edgeDotBaseToVertex)
											/ edgeSquaredLength;
									if ((f >= 0.0F) && (f <= 1.0F)) {
										t = this.newT[0];
										foundCollision = true;
										collisionPoint.set(distyx, distyy,
												distyz);
										collisionPoint.scalarMul(f);
										collisionPoint.add(p1x, p1y, p1z);
									}

								}

								distyx = p0x - p2x;
								distyy = p0y - p2y;
								distyz = p0z - p2z;

								baseToVertexx = p2x - base.x;
								baseToVertexy = p2y - base.y;
								baseToVertexz = p2z - base.z;

								edgeSquaredLength = distyx * distyx + distyy
										* distyy + distyz * distyz;
								edgeDotVelocity = distyx * velocity.x + distyy
										* velocity.y + distyz * velocity.z;
								edgeDotBaseToVertex = distyx * baseToVertexx
										+ distyy * baseToVertexy + distyz
										* baseToVertexz;

								a = edgeSquaredLength * -velocitySquaredLength
										+ edgeDotVelocity * edgeDotVelocity;
								b = edgeSquaredLength
										* (2.0F * (velocity.x * baseToVertexx
												+ velocity.y * baseToVertexy + velocity.z
												* baseToVertexz)) - 2.0F
										* edgeDotVelocity * edgeDotBaseToVertex;
								c = edgeSquaredLength
										* (1.0F - (baseToVertexx
												* baseToVertexx + baseToVertexy
												* baseToVertexy + baseToVertexz
												* baseToVertexz))
										+ edgeDotBaseToVertex
										* edgeDotBaseToVertex;

								if (getLowestRoot(a, b, c, t, this.newT)) {
									float f = (edgeDotVelocity * this.newT[0] - edgeDotBaseToVertex)
											/ edgeSquaredLength;
									if ((f >= 0.0F) && (f <= 1.0F)) {
										t = this.newT[0];
										foundCollision = true;
										collisionPoint.set(distyx, distyy,
												distyz);
										collisionPoint.scalarMul(f);
										collisionPoint.add(p2x, p2y, p2z);
									}
								}
							}

							if (foundCollision) {
								addPolygonID(p);
								float distToCollision = t;
								if ((!cInf.foundCollision)
										|| (distToCollision <= cInf.nearestDistance)) {
									cInf.nearestDistance = distToCollision;
									cInf.setIntersectionPoint(collisionPoint);
									cInf.foundCollision = true;
									cInf.collision = true;
									cInf.eSpaceBasePoint = basePoint;
									cInf.eSpaceVelocity = velocity;
									cInf.collisionObject = this;
									cInf.isPartOfCollision = true;
								}
							}
						}
					}
				}
			}
		} while ((useOcTree) && (nodeCnt < touchedLeafs));

		if (Logger.isDebugEnabled())
			Logger.log("Polygons checked: " + checks + "/ skipped: " + skipped,
					3);
	}

	private float getSquaredDistance(SimpleVector basePoint, float p0x,
			float p0y, float p0z) {
		float x = -basePoint.x + p0x;
		float y = -basePoint.y + p0y;
		float z = -basePoint.z + p0z;

		return x * x + y * y + z * z;
	}

	private final boolean checkPointInTriangle(SimpleVector d,
			SimpleVector point, float ps0x, float ps0y, float ps0z, float ps1x,
			float ps1y, float ps1z, float ps2x, float ps2y, float ps2z) {
		float v1x = ps0x;
		float v1y = ps0y;
		float v1z = ps0z;

		float edge1X = ps1x - v1x;
		float edge1Y = ps1y - v1y;
		float edge1Z = ps1z - v1z;

		float edge2X = ps2x - v1x;
		float edge2Y = ps2y - v1y;
		float edge2Z = ps2z - v1z;

		float pvec0 = 0.0F;
		float pvec1 = 0.0F;
		float pvec2 = 0.0F;

		pvec0 = d.y * edge2Z - d.z * edge2Y;
		pvec1 = d.z * edge2X - d.x * edge2Z;
		pvec2 = d.x * edge2Y - d.y * edge2X;

		float det = edge1X * pvec0 + edge1Y * pvec1 + edge1Z * pvec2;

		if (det >= 1.0E-017F) {
			float invDet = 1.0F / det;

			edge2X = point.x - v1x;
			edge2Y = point.y - v1y;
			edge2Z = point.z - v1z;

			float u = (edge2X * pvec0 + edge2Y * pvec1 + edge2Z * pvec2)
					* invDet;

			if ((u >= 0.0D) && (u <= 1.0F)) {
				pvec0 = edge2Y * edge1Z - edge2Z * edge1Y;
				pvec1 = edge2Z * edge1X - edge2X * edge1Z;
				pvec2 = edge2X * edge1Y - edge2Y * edge1X;

				float v = (d.x * pvec0 + d.y * pvec1 + d.z * pvec2) * invDet;

				if ((v >= 0.0D) && (u + v <= 1.0D)) {
					return true;
				}
			}
		}
		return false;
	}

	private final boolean getLowestRoot(float a, float b, float c, float maxR,
			float[] root) {
		float determinant = b * b - 4.0F * a * c;
		if (determinant < 0.0F) {
			return false;
		}

		float sqrtd = (float) Math.sqrt(determinant);
		float r1 = (-b - sqrtd) / (2.0F * a);
		float r2 = (-b + sqrtd) / (2.0F * a);

		if (r1 > r2) {
			float temp = r2;
			r2 = r1;
			r1 = temp;
		}

		if ((r1 > 0.0F) && (r1 < maxR)) {
			root[0] = r1;
			return true;
		}

		if ((r2 > 0.0F) && (r2 < maxR)) {
			root[0] = r2;
			return true;
		}

		return false;
	}

	final SimpleVector reverseTransform(Matrix tmpMatrix, SimpleVector orig,
			boolean withTranslation) {
		Matrix trans = getWorldTransformation(tmpMatrix);
		float s00Ws = trans.mat[0][0];
		float s10Ws = trans.mat[1][0];
		float s11Ws = trans.mat[1][1];
		float s21Ws = trans.mat[2][1];
		float s20Ws = trans.mat[2][0];
		float s01Ws = trans.mat[0][1];
		float s22Ws = trans.mat[2][2];
		float s12Ws = trans.mat[1][2];
		float s02Ws = trans.mat[0][2];
		float bxWs = trans.mat[3][0];
		float byWs = trans.mat[3][1];
		float bzWs = trans.mat[3][2];

		float bx = orig.x * s00Ws + orig.y * s10Ws + orig.z * s20Ws;
		float by = orig.x * s01Ws + orig.y * s11Ws + orig.z * s21Ws;
		float bz = orig.x * s02Ws + orig.y * s12Ws + orig.z * s22Ws;

		if (withTranslation) {
			bx += bxWs;
			by += byWs;
			bz += bzWs;
		}

		orig.set(bx, by, bz);
		return orig;
	}

	public int addTriangle(SimpleVector vert1, SimpleVector vert2,
			SimpleVector vert3) {
		return addTriangle(vert1.x, vert1.y, vert1.z, 0.0F, 0.0F, vert2.x,
				vert2.y, vert2.z, 0.0F, 0.0F, vert3.x, vert3.y, vert3.z, 0.0F,
				0.0F, -1, 0, false);
	}

	public final int addTriangle(float x, float y, float z, float x2, float y2,
			float z2, float x3, float y3, float z3) {
		return addTriangle(x, y, z, 0.0F, 0.0F, x2, y2, z2, 0.0F, 0.0F, x3, y3,
				z3, 0.0F, 0.0F, -1, 0, false);
	}

	public int addTriangle(SimpleVector vert1, float u, float v,
			SimpleVector vert2, float u2, float v2, SimpleVector vert3,
			float u3, float v3) {
		return addTriangle(vert1.x, vert1.y, vert1.z, u, v, vert2.x, vert2.y,
				vert2.z, u2, v2, vert3.x, vert3.y, vert3.z, u3, v3, -1, 0,
				false);
	}

	public final int addTriangle(float x, float y, float z, float u, float v,
			float x2, float y2, float z2, float u2, float v2, float x3,
			float y3, float z3, float u3, float v3) {
		return addTriangle(x, y, z, u, v, x2, y2, z2, u2, v2, x3, y3, z3, u3,
				v3, -1, 0, false);
	}

	public int addTriangle(SimpleVector vert1, float u, float v,
			SimpleVector vert2, float u2, float v2, SimpleVector vert3,
			float u3, float v3, int textureID) {
		return addTriangle(vert1.x, vert1.y, vert1.z, u, v, vert2.x, vert2.y,
				vert2.z, u2, v2, vert3.x, vert3.y, vert3.z, u3, v3, textureID,
				0, false);
	}

	final int addTriangle(float x, float y, float z, float u, float v,
			float x2, float y2, float z2, float u2, float v2, float x3,
			float y3, float z3, float u3, float v3, int texturID) {
		return addTriangle(x, y, z, u, v, x2, y2, z2, u2, v2, x3, y3, z3, u3,
				v3, texturID, 0, false);
	}

	public int addTriangle(SimpleVector vert1, float u, float v,
			SimpleVector vert2, float u2, float v2, SimpleVector vert3,
			float u3, float v3, int textureID, int sec) {
		return addTriangle(vert1.x, vert1.y, vert1.z, u, v, vert2.x, vert2.y,
				vert2.z, u2, v2, vert3.x, vert3.y, vert3.z, u3, v3, textureID,
				sec, true);
	}

	public int addTriangle(SimpleVector vert1, SimpleVector vert2,
			SimpleVector vert3, TextureInfo tInf) {
		return addTriangle(vert1.x, vert1.y, vert1.z, 0.0F, 0.0F, vert2.x,
				vert2.y, vert2.z, 0.0F, 0.0F, vert3.x, vert3.y, vert3.z, 0.0F,
				0.0F, 0, 0, false, null, tInf);
	}

	final int addTriangle(float x, float y, float z, float u, float v,
			float x2, float y2, float z2, float u2, float v2, float x3,
			float y3, float z3, float u3, float v3, int texturID, int sec,
			boolean useSec) {
		return addTriangle(x, y, z, u, v, x2, y2, z2, u2, v2, x3, y3, z3, u3,
				v3, texturID, sec, useSec, null);
	}

	final int addTriangle(float x, float y, float z, float u, float v,
			float x2, float y2, float z2, float u2, float v2, float x3,
			float y3, float z3, float u3, float v3, int texturID, int sec,
			boolean useSec, int[] lastPoints) {
		return addTriangle(x, y, z, u, v, x2, y2, z2, u2, v2, x3, y3, z3, u3,
				v3, texturID, sec, useSec, lastPoints, null);
	}

	final int addTriangle(float x, float y, float z, float u, float v,
			float x2, float y2, float z2, float u2, float v2, float x3,
			float y3, float z3, float u3, float v3, int texturID, int sec,
			boolean useSec, int[] lastPoints, TextureInfo tInf) {
		try {
			if (tInf != null) {
				if ((this.multiTex == null) && (tInf.stageCnt > 1)) {
					this.multiTex = new int[Config.maxTextureLayers - 1][this.texture.length];
					this.multiMode = new int[Config.maxTextureLayers - 1][this.texture.length];
					for (int i = 0; i < this.texture.length; i++) {
						for (int ii = 0; ii < Config.maxTextureLayers - 1; ii++) {
							this.multiTex[ii][i] = -1;
						}
					}
					this.objVectors.createMultiCoords();
					this.usesMultiTexturing = true;
				}

				u = tInf.u0[0];
				v = tInf.v0[0];
				u2 = tInf.u1[0];
				v2 = tInf.v1[0];
				u3 = tInf.u2[0];
				v3 = tInf.v2[0];
				texturID = tInf.textures[0];

				if (this.maxStagesUsed < tInf.stageCnt) {
					this.maxStagesUsed = tInf.stageCnt;
				}
			}

			boolean multi = (this.usesMultiTexturing) && (tInf != null);

			if (texturID == -1) {
				texturID = 0;
			}

			float un = u;
			float vn = v;
			float u2n = u2;
			float v2n = v2;
			float u3n = u3;
			float v3n = v3;

			if (this.objMesh.anzVectors + 3 < this.objMesh.maxVectors) {
				int pos = this.objMesh.anzVectors;
				int posTri = this.objMesh.anzTri;
				int pos3 = 0;

				int posm3 = 0;
				int posmm3 = 0;

				pos3 = -1;
				if (!this.neverOptimize) {
					pos3 = this.objVectors.checkCoords(x, y, z);
				} else if (lastPoints != null) {
					pos3 = lastPoints[0];
				}

				if (pos3 == -1) {
					pos3 = this.objVectors.addVertex(x, y, z);
				}

				posmm3 = pos3;

				this.objMesh.coords[pos] = pos3;

				if (multi) {
					for (int i = 0; i < tInf.stageCnt - 1; i++) {
						this.objVectors.uMul[i][pos] = tInf.u0[(i + 1)];
						this.objVectors.vMul[i][pos] = tInf.v0[(i + 1)];
					}
				}

				this.objVectors.nuOrg[pos] = un;
				this.objVectors.nvOrg[pos] = vn;

				this.objMesh.anzVectors += 1;

				this.objMesh.points[posTri][0] = pos;

				pos = this.objMesh.anzVectors;

				pos3 = -1;
				if (!this.neverOptimize) {
					pos3 = this.objVectors.checkCoords(x2, y2, z2);
				} else if (lastPoints != null) {
					pos3 = lastPoints[1];
				}

				if (pos3 == -1) {
					pos3 = this.objVectors.addVertex(x2, y2, z2);
				}

				posm3 = pos3;

				this.objMesh.coords[pos] = pos3;

				if (multi) {
					for (int i = 0; i < tInf.stageCnt - 1; i++) {
						this.objVectors.uMul[i][pos] = tInf.u1[(i + 1)];
						this.objVectors.vMul[i][pos] = tInf.v1[(i + 1)];
					}
				}

				this.objVectors.nuOrg[pos] = u2n;
				this.objVectors.nvOrg[pos] = v2n;

				this.objMesh.anzVectors += 1;
				this.objMesh.points[posTri][1] = pos;
				pos = this.objMesh.anzVectors;

				pos3 = -1;
				if (!this.neverOptimize) {
					pos3 = this.objVectors.checkCoords(x3, y3, z3);
				} else if (lastPoints != null) {
					pos3 = lastPoints[2];
				}

				if (pos3 == -1) {
					pos3 = this.objVectors.addVertex(x3, y3, z3);
				}

				this.objMesh.coords[pos] = pos3;

				if (multi) {
					for (int i = 0; i < tInf.stageCnt - 1; i++) {
						this.objVectors.uMul[i][pos] = tInf.u2[(i + 1)];
						this.objVectors.vMul[i][pos] = tInf.v2[(i + 1)];
					}
				}

				this.objVectors.nuOrg[pos] = u3n;
				this.objVectors.nvOrg[pos] = v3n;

				this.objMesh.anzVectors += 1;

				if (lastPoints != null) {
					lastPoints[0] = posmm3;
					lastPoints[1] = posm3;
					lastPoints[2] = pos3;
				}

				if ((pos3 != posmm3) && (pos3 != posm3) && (posm3 != posmm3)) {
					if (pos3 < this.lowestPos) {
						this.lowestPos = pos3;
					}
					if (posm3 < this.lowestPos) {
						this.lowestPos = posm3;
					}
					if (posmm3 < this.lowestPos) {
						this.lowestPos = posmm3;
					}

					if (pos3 > this.highestPos) {
						this.highestPos = pos3;
					}
					if (posm3 > this.highestPos) {
						this.highestPos = posm3;
					}
					if (posmm3 > this.highestPos) {
						this.highestPos = posmm3;
					}

					this.objMesh.points[posTri][2] = pos;

					this.texture[posTri] = texturID;

					if (multi) {
						for (int i = 0; i < tInf.stageCnt - 1; i++) {
							this.multiTex[i][posTri] = tInf.textures[(i + 1)];
							this.multiMode[i][posTri] = tInf.mode[(i + 1)];
						}
					}

					this.objMesh.anzTri += 1;
				}
			} else {
				Logger.log("Polygon index out of range - object is too large!",
						0);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			Logger.log("Polygon index out of range - object is too large!", 0);
		}

		return this.objMesh.anzTri - 1;
	}

	final int addMD2Triangle(int index1, float u, float v, int index2,
			float u2, float v2, int index3, float u3, float v3) {
		int texturID = 0;

		float un = u;
		float vn = v;
		float u2n = u2;
		float v2n = v2;
		float u3n = u3;
		float v3n = v3;

		if (this.objMesh.anzVectors + 3 < this.objMesh.maxVectors) {
			int pos = this.objMesh.anzVectors;
			int posTri = this.objMesh.anzTri;
			int pos3 = 0;

			int posm3 = 0;
			int posmm3 = 0;

			pos3 = index1;
			posmm3 = pos3;

			this.objMesh.coords[pos] = pos3;
			this.objVectors.nuOrg[pos] = un;
			this.objVectors.nvOrg[pos] = vn;

			this.objMesh.anzVectors += 1;
			this.objMesh.points[posTri][0] = pos;
			pos = this.objMesh.anzVectors;

			pos3 = index2;
			posm3 = pos;

			this.objMesh.coords[pos] = pos3;

			this.objVectors.nuOrg[pos] = u2n;
			this.objVectors.nvOrg[pos] = v2n;

			this.objMesh.anzVectors += 1;
			this.objMesh.points[posTri][1] = pos;
			pos = this.objMesh.anzVectors;
			pos3 = index3;

			this.objMesh.coords[pos] = pos3;

			this.objVectors.nuOrg[pos] = u3n;
			this.objVectors.nvOrg[pos] = v3n;

			this.objMesh.anzVectors += 1;

			if (pos3 < this.lowestPos) {
				this.lowestPos = pos3;
			}
			if (posm3 < this.lowestPos) {
				this.lowestPos = posm3;
			}
			if (posmm3 < this.lowestPos) {
				this.lowestPos = posmm3;
			}

			if (pos3 > this.highestPos) {
				this.highestPos = pos3;
			}
			if (posm3 > this.highestPos) {
				this.highestPos = posm3;
			}
			if (posmm3 > this.highestPos) {
				this.highestPos = posmm3;
			}

			this.objMesh.points[posTri][2] = pos;

			this.texture[posTri] = texturID;

			this.objMesh.anzTri += 1;
		} else {
			Logger.log("Polygon index out of range - object is too large!", 0);
		}

		return this.objMesh.anzTri - 1;
	}

	private final Matrix recurseObjectsBillboarded(Matrix mat) {
		Matrix mc = getTmpMatrix(3);
		mc.setTo(mat);

		Matrix m2 = getTmpMatrix(4);
		mat = recurseObjects(mat);

		Matrix m3 = getTmpMatrix(5);
		m3.setTo(mat);

		float scale = recurseScaling(getScale());

		m2.setIdentity();
		float m30 = -mat.mat[3][0];
		float m31 = -mat.mat[3][1];
		float m32 = -mat.mat[3][2];

		m2.mat[3][0] = m30;
		m2.mat[3][1] = m31;
		m2.mat[3][2] = m32;

		mat.matMul(m2);

		m3.invert3x3(m2);

		m2.mat[3][0] = 0.0F;
		m2.mat[3][1] = 0.0F;
		m2.mat[3][2] = 0.0F;
		m2.mat[3][3] = 1.0F;
		mat.matMul(m2);

		mc.mat[3][0] = 0.0F;
		mc.mat[3][1] = 0.0F;
		mc.mat[3][2] = 0.0F;
		mc.mat[3][3] = 1.0F;
		mat.matMul(mc);

		m2.setIdentity();

		m2.mat[3][0] = (-m30);
		m2.mat[3][1] = (-m31);
		m2.mat[3][2] = (-m32);
		mat.matMul(m2);

		mat.scalarMul(1.0F / (scale * getScale()));
		return mat;
	}

	private final Matrix recurseObjects(Matrix mat) {
		Matrix mat4 = getTmpMatrix(2);

		for (int i = 0; i < this.parentCnt; i++) {
			T3DObject po = this.parent[i];
			mat4.setIdentity();

			float o30 = po.originMatrix.mat[3][0];
			float o31 = po.originMatrix.mat[3][1];
			float o32 = po.originMatrix.mat[3][2];

			mat4.mat[3][0] = (-po.xRotationCenter - o30);
			mat4.mat[3][1] = (-po.yRotationCenter - o31);
			mat4.mat[3][2] = (-po.zRotationCenter - o32);

			mat.matMul(mat4);
			mat.matMul(po.rotationMatrix);

			mat4.mat[3][0] = (po.xRotationCenter
					+ po.translationMatrix.mat[3][0] + o30);
			mat4.mat[3][1] = (po.yRotationCenter
					+ po.translationMatrix.mat[3][1] + o31);
			mat4.mat[3][2] = (po.zRotationCenter
					+ po.translationMatrix.mat[3][2] + o32);

			mat.matMul(mat4);

			if (po.parentCnt != 0) {
				mat = po.recurseObjects(mat);
			}
		}

		return mat;
	}

	private final float recurseScaling(float scale) {
		for (int i = 0; i < this.parentCnt; i++) {
			T3DObject po = this.parent[i];
			scale *= po.getScale();

			if (po.parentCnt != 0) {
				scale = po.recurseScaling(scale);
			}
		}

		return scale;
	}

	final boolean transformVertices(FrameBuffer buffer) {
		float x1 = 0.0F;
		float z1 = 0.0F;
		float y1 = 0.0F;

		int obbXpClipped = 0;
		int obbXnClipped = 0;
		int obbYnClipped = 0;
		int obbYpClipped = 0;
		int obbZClipped = 0;
		int obbZFarClipped = 0;

		float divx = this.myRage3DScene.camera.divx;
		float divy = this.myRage3DScene.camera.divy;
		float viewplane = Config.nearPlane;
		float p1zh = 0.0F;

		Matrix mat5 = this.mat5;
		float[][] mat5mat = mat5.mat;

		this.mat2 = this.myRage3DScene.camera.backMatrix;

		float[] objMeshzOrg = this.objMesh.zOrg;
		float[] objMeshxOrg = this.objMesh.xOrg;
		float[] objMeshyOrg = this.objMesh.yOrg;

		if ((this.lazyTransforms) && (!this.isBillBoard)) {
			getWorldTransformation(mat5);
		} else {
			Matrix matBill = null;

			mat5.setIdentity();

			float[] mat5mat3 = mat5mat[3];
			float[] translationMatrixmat = this.translationMatrix.mat[3];
			float[] originMatrixmat = this.originMatrix.mat[3];

			mat5mat3[0] = (-this.xRotationCenter);
			mat5mat3[1] = (-this.yRotationCenter);
			mat5mat3[2] = (-this.zRotationCenter);

			if (!this.isBillBoard) {
				mat5.matMul(this.rotationMatrix);
			} else {
				if (matBill == null) {
					matBill = new Matrix();
					matBill = matBill;
				}
				this.mat2.invert3x3(matBill);
				matBill.scalarMul(this.scaleFactor);
				mat5.matMul(matBill);
			}

			mat5.translate(this.xRotationCenter + translationMatrixmat[0]
					+ originMatrixmat[0], this.yRotationCenter
					+ translationMatrixmat[1] + originMatrixmat[1],
					this.zRotationCenter + translationMatrixmat[2]
							+ originMatrixmat[2]);

			if (this.parentCnt != 0) {
				if (this.isBillBoard)
					mat5 = recurseObjectsBillboarded(mat5);
				else {
					mat5 = recurseObjects(mat5);
				}
			}
		}

		this.transBuffer.setTo(mat5);
		mat5.translate(-this.myRage3DScene.camera.backBx,
				-this.myRage3DScene.camera.backBy,
				-this.myRage3DScene.camera.backBz);
		mat5.matMul(this.mat2);

		float s00 = mat5mat[0][0];
		float s01 = mat5mat[0][1];
		float s02 = mat5mat[0][2];
		float s10 = mat5mat[1][0];
		float s11 = mat5mat[1][1];
		float s12 = mat5mat[1][2];
		float s21 = mat5mat[2][1];
		float s22 = mat5mat[2][2];
		float s20 = mat5mat[2][0];

		float bx = mat5mat[3][0];
		float by = mat5mat[3][1];
		float bz = mat5mat[3][2];

		int visibleLeafs = 0;

		boolean useOctree = (this.ocTree != null)
				&& (this.ocTree.getRenderingUse());

		if (useOctree) {
			visibleLeafs = this.ocTree.getVisibleLeafs(mat5, divx, divy);

			if (visibleLeafs == 0) {
				return true;
			}
		}

		boolean clipped = false;

		if (this.hasBoundingBox) {
			int pstart = this.objMesh.obbStart;
			int pend = this.objMesh.obbEnd;

			if (pstart <= pend + 1 - pstart) {
				pend = pstart - 1;
				pstart = 0;
			}

			for (int p = pstart; p <= pend; p++) {
				z1 = objMeshzOrg[p];
				x1 = objMeshxOrg[p];
				y1 = objMeshyOrg[p];

				float p1x = x1 * s00 + y1 * s10 + z1 * s20 + bx;
				float p1y = x1 * s01 + y1 * s11 + z1 * s21 + by;
				float p1z = x1 * s02 + y1 * s12 + z1 * s22 + bz;

				boolean culled = false;

				if (p1z < viewplane) {
					obbZClipped++;
					culled = true;
				} else if (p1z > Config.farPlane) {
					obbZFarClipped++;
					culled = true;
				}

				p1zh = p1z * divx;
				if (p1x < -p1zh) {
					obbXnClipped++;
					culled = true;
				} else if (p1x > p1zh) {
					obbXpClipped++;
					culled = true;
				}

				p1zh = p1z * divy;
				if (p1y < -p1zh) {
					obbYnClipped++;
					culled = true;
				} else if (p1y > p1zh) {
					obbYpClipped++;
					culled = true;
				}

				if (!culled) {
					break;
				}
			}
			int obbDif = pend + 1 - pstart;

			if ((obbYpClipped == obbDif) || (obbXpClipped == obbDif)
					|| (obbYnClipped == obbDif) || (obbXnClipped == obbDif)
					|| (obbZClipped == obbDif) || (obbZFarClipped == obbDif)) {
				clipped = true;
			}
		}

		if (!clipped) {
			this.object3DRendered = true;

			OcTreeNode[] leaflist = (OcTreeNode[]) null;
			if (useOctree) {
				if (this.sectors == null) {
					this.sectors = new HashSet(1);
				}
				this.sectors.clear();
				leaflist = this.ocTree.getLeafList();
				for (int p = 0; p < visibleLeafs; p++) {
					this.sectors.add(IntegerC.valueOf(leaflist[p].getID()));
				}
			}

			int endy = this.compiled.size();

			for (int i = 0; i < endy; i++) {
				CompiledInstance ci = null;
				if (this.shareWith != null)
					ci = (CompiledInstance) this.shareWith.compiled.get(i);
				else {
					ci = (CompiledInstance) this.compiled.get(i);
				}
				boolean add = true;
				if ((useOctree) && (ci.getTreeID() != -1)) {
					add = this.sectors
							.contains(IntegerC.valueOf(ci.getTreeID()));
				}
				if (add) {
					if ((this.isTrans) || (!Config.stateOrientedSorting)) {
						float z = this.centerX * s02 + this.centerY * s12
								+ this.centerZ * s22 + bz;
						this.myRage3DScene.visList.addToList(this, z, i, ci);
					} else {
						float sorty = 10000 + (this.texture.length > 1 ? this.texture[ci.polyIndex]
								: this.texture[0]);
						if (this.shader != null) {
							sorty += 1000 * this.shader.id;
						}
						if (sorty > 990000.0F) {
							sorty -= 1000000.0F;
						}
						this.myRage3DScene.visList
								.addToList(this, sorty, i, ci);
					}
				}
			}
			return false;
		}
		return clipped;
	}

	final void addCompiled(CompiledInstance ci) {
		this.compiled.add(ci);
	}

	final void render() {
		Lights tempLight;
		if (!this.isLit)
			tempLight = DUMMY_LIGHTS;
		else {
			tempLight = this.myRage3DScene.lights;
		}
		int anzLights = tempLight.lightCnt;
		VisList visList = this.myRage3DScene.visList;

		if (this.nearestLights == null) {
			this.nearestLights = new float[8][7];
		}

		this.nearestLights[0][0] = -9999.0F;
		this.nearestLights[1][0] = -9999.0F;
		this.nearestLights[2][0] = -9999.0F;
		this.nearestLights[3][0] = -9999.0F;
		this.nearestLights[4][0] = -9999.0F;
		this.nearestLights[5][0] = -9999.0F;
		this.nearestLights[6][0] = -9999.0F;
		this.nearestLights[7][0] = -9999.0F;

		if ((this.isLit) && (anzLights > 0)) {
			SimpleVector v = null;
			ArrayList ls = lightsList;
			ls.clear();

			if (litData.length < anzLights) {
				litData = new float[anzLights][2];
			}

			int overrides = 0;

			for (int i = 0; i < anzLights; i++) {
				if (tempLight.isVisible[i]) {
					if (v == null) {
						fillTransformedCenter(tempTC);
						v = tempTC;
					}

					float dist = tempLight.distanceOverride[i];
					if (dist == -1.0F) {
						float x1 = v.x - tempLight.xOrg[i];
						float y1 = v.y - tempLight.yOrg[i];
						float z1 = v.z - tempLight.zOrg[i];
						dist = (float) Math.sqrt(x1 * x1 + y1 * y1 + z1 * z1);
					} else {
						overrides++;
					}
					if ((dist <= tempLight.discardDistance[i])
							|| ((tempLight.discardDistance[i] < 0.0F) && ((Config.lightDiscardDistance < 0.0F) || (Config.lightDiscardDistance > dist)))) {
						boolean added = false;
						if ((anzLights > 8) || (overrides > 0)) {
							for (int p = 0; p < ls.size(); p++) {
								float[] vals = (float[]) ls.get(p);
								if (dist < vals[0]) {
									litData[i][0] = dist;
									litData[i][1] = i;
									ls.add(p, litData[i]);
									added = true;
									break;
								}
							}
						}
						if (!added) {
							litData[i][0] = dist;
							litData[i][1] = i;
							ls.add(litData[i]);
						}
					}
				}
			}

			int end = this.maxLights;
			if (end > ls.size()) {
				end = ls.size();
			}

			for (int i = 0; i < end; i++) {
				int id = (int) ((float[]) ls.get(i))[1];
				float att = tempLight.getAttenuation(id);
				if ((att != -1.0F) && (att < 0.0F) && (Config.fadeoutLight)) {
					att = Config.linearDiv;
				}

				float[] nearestLightsi = this.nearestLights[i];

				nearestLightsi[0] = att;

				nearestLightsi[1] = tempLight.xTr[id];
				nearestLightsi[2] = (-tempLight.yTr[id]);
				nearestLightsi[3] = (-tempLight.zTr[id]);

				nearestLightsi[4] = (tempLight.rOrg[id] / 255.0F);
				nearestLightsi[5] = (tempLight.gOrg[id] / 255.0F);
				nearestLightsi[6] = (tempLight.bOrg[id] / 255.0F);
			}
		}

		if ((this.objMesh.attrList != null) && (this.dynamic)) {
			List attrs = this.objMesh.attrList;
			for (int i = 0; i < attrs.size(); i++) {
				VertexAttributes vas = (VertexAttributes) attrs.get(i);
				if (vas.updated) {
					vas.updated = false;
					this.modified = true;
					break;
				}
			}
		}

		if ((this.dynamic) && (this.modified)) {
			visList.addToFill(this);
			if (this.shareWith != null) {
				this.shareWith.modified = true;
			}
		}

		if ((this.dynamic) && (this.shareWith != null)
				&& (this.shareWith.modified))
			visList.addToFill(this.shareWith);
	}

	boolean getLazyTransformationState() {
		/* 6235 */
		return this.lazyTransforms;

	}

	public Matrix getInverseWorldTransformation() {
		Matrix mat5 = getWorldTransformation();
		Matrix mat6;
		if ((!this.lazyTransforms) || (this.invCache == null)) {
			mat6 = mat5.invert();
			if (this.lazyTransforms)
				if (this.invCacheDump != null) {
					this.invCache = this.invCacheDump;
					this.invCacheDump = null;
					this.invCache.setTo(mat6);
				} else {
					this.invCache = mat6.cloneMatrix();
				}
		} else {
			mat6 = this.invCache.cloneMatrix();
		}
		return mat6;
	}

	public Matrix getInverseWorldTransformation(Matrix mat) {
		if (mat == null) {
			mat = new Matrix();
		}

		Matrix mat6 = getWorldTransformation(mat);

		if ((!this.lazyTransforms) || (this.invCache == null)) {
			mat6 = mat6.invert(mat);
			if (this.lazyTransforms) {
				if (this.invCacheDump != null) {
					this.invCache = this.invCacheDump;
					this.invCacheDump = null;
					this.invCache.setTo(mat6);
				} else {
					this.invCache = mat6.cloneMatrix();
				}
			}
			if (mat != mat6)
				mat.setTo(mat6);
		} else {
			mat.setTo(this.invCache);
		}
		return mat;
	}

	private final void appendToObject(T3DObject obj) {
		T3DObject tmpObj = obj;
		int vOffset = tmpObj.objMesh.anzVectors;
		int mOffset = tmpObj.objMesh.anzCoords;
		int triOffset = tmpObj.objMesh.anzTri;
		int end = 0;

		if ((this.objVectors != null) && (this.objVectors.uMul != null)) {
			tmpObj.usesMultiTexturing = true;
			tmpObj.maxStagesUsed = Math.max(tmpObj.maxStagesUsed,
					this.maxStagesUsed);
			tmpObj.objVectors.createMultiCoords();
			end = Config.maxTextureLayers - 1;
			for (int i = 0; i < end; i++) {
				float[] tmpObjobjVectorsuMuli = tmpObj.objVectors.uMul[i];
				float[] tmpObjobjVectorsvMuli = tmpObj.objVectors.vMul[i];
				float[] objVectorsuMuli = this.objVectors.uMul[i];
				float[] objVectorsvMuli = this.objVectors.vMul[i];
				for (int p = 0; p < this.objMesh.anzVectors; p++) {
					int iv = p + vOffset;
					tmpObjobjVectorsuMuli[iv] = objVectorsuMuli[p];
					tmpObjobjVectorsvMuli[iv] = objVectorsvMuli[p];
				}
			}
		}

		if (this.multiTex != null) {
			end = Config.maxTextureLayers - 1;
			for (int i = 0; i < end; i++) {
				if (tmpObj.multiTex == null) {
					tmpObj.multiTex = new int[Config.maxTextureLayers - 1][tmpObj.texture.length];
					tmpObj.multiMode = new int[Config.maxTextureLayers - 1][tmpObj.texture.length];
				}

				System.arraycopy(this.multiTex[i], 0, tmpObj.multiTex[i],
						triOffset, this.objMesh.anzTri);
				System.arraycopy(this.multiMode[i], 0, tmpObj.multiMode[i],
						triOffset, this.objMesh.anzTri);
			}
		}

		if (this.objVectors != null) {
			float[] tmpObjobjVectorsnuOrg = tmpObj.objVectors.nuOrg;
			float[] tmpObjobjVectorsnvOrg = tmpObj.objVectors.nvOrg;
			int[] tmpObjobjMeshcoords = tmpObj.objMesh.coords;

			float[] objVectorsnuOrg = this.objVectors.nuOrg;
			float[] objVectorsnvOrg = this.objVectors.nvOrg;
			int[] objMeshcoords = this.objMesh.coords;

			end = this.objMesh.anzVectors;

			for (int i = 0; i < end; i++) {
				int iv = i + vOffset;
				tmpObjobjVectorsnuOrg[iv] = objVectorsnuOrg[i];
				tmpObjobjVectorsnvOrg[iv] = objVectorsnvOrg[i];
				objMeshcoords[i] += mOffset;
			}
		}

		if (this.objMesh != null) {
			end = this.objMesh.anzTri;

			int[][] tmpObjobjMeshpoints = tmpObj.objMesh.points;
			int[][] objMeshpoints = this.objMesh.points;

			for (int i = 0; i < end; i++) {
				int it = i + triOffset;
				int[] tmpObjobjMeshpointsit = tmpObjobjMeshpoints[it];
				int[] objMeshpointsi = objMeshpoints[i];
				objMeshpointsi[0] += vOffset;
				objMeshpointsi[1] += vOffset;
				objMeshpointsi[2] += vOffset;
			}

			end = this.objMesh.anzCoords;
			if (this.objMesh.obbStart != 0) {
				end = this.objMesh.obbStart;
			}

			float[] tmpObjobjMeshxOrg = tmpObj.objMesh.xOrg;
			float[] tmpObjobjMeshyOrg = tmpObj.objMesh.yOrg;
			float[] tmpObjobjMeshzOrg = tmpObj.objMesh.zOrg;
			float[] tmpObjobjMeshnxOrg = tmpObj.objMesh.nxOrg;
			float[] tmpObjobjMeshnyOrg = tmpObj.objMesh.nyOrg;
			float[] tmpObjobjMeshnzOrg = tmpObj.objMesh.nzOrg;

			float[] objMeshxOrg = this.objMesh.xOrg;
			float[] objMeshyOrg = this.objMesh.yOrg;
			float[] objMeshzOrg = this.objMesh.zOrg;
			float[] objMeshnxOrg = this.objMesh.nxOrg;
			float[] objMeshnyOrg = this.objMesh.nyOrg;
			float[] objMeshnzOrg = this.objMesh.nzOrg;

			for (int i = 0; i < end; i++) {
				int im = i + mOffset;
				tmpObjobjMeshxOrg[im] = objMeshxOrg[i];
				tmpObjobjMeshyOrg[im] = objMeshyOrg[i];
				tmpObjobjMeshzOrg[im] = objMeshzOrg[i];
				tmpObjobjMeshnxOrg[im] = objMeshnxOrg[i];
				tmpObjobjMeshnyOrg[im] = objMeshnyOrg[i];
				tmpObjobjMeshnzOrg[im] = objMeshnzOrg[i];
			}

			int srcTriCnt = this.objMesh.anzTri;

			if (this.texture != null) {
				System.arraycopy(this.texture, 0, tmpObj.texture, triOffset,
						srcTriCnt);
			}

			tmpObj.objMesh.anzTri += this.objMesh.anzTri;
			tmpObj.objMesh.anzCoords += end;
			tmpObj.objMesh.anzVectors += this.objMesh.anzVectors;
		}
	}

	final void getProjectedPoint(float x, float y, float z,
			SimpleVector result, float[] result2, Matrix m) {
		Matrix trans = getWorldTransformation(m);

		float s00Ws = trans.mat[0][0];
		float s10Ws = trans.mat[1][0];
		float s11Ws = trans.mat[1][1];
		float s21Ws = trans.mat[2][1];
		float s20Ws = trans.mat[2][0];
		float s01Ws = trans.mat[0][1];
		float s22Ws = trans.mat[2][2];
		float s12Ws = trans.mat[1][2];
		float s02Ws = trans.mat[0][2];
		float bxWs = trans.mat[3][0];
		float byWs = trans.mat[3][1];
		float bzWs = trans.mat[3][2];

		float x1 = x * s00Ws + y * s10Ws + z * s20Ws + bxWs;
		float y1 = x * s01Ws + y * s11Ws + z * s21Ws + byWs;
		float z1 = x * s02Ws + y * s12Ws + z * s22Ws + bzWs;

		if (result != null) {
			result.x = x1;
			result.y = y1;
			result.z = z1;
		}

		if (result2 != null) {
			result2[0] = x1;
			result2[1] = y1;
			result2[2] = z1;
		}
	}

	void notifyCollisionListeners(int type, int algo, T3DObject[] targets,
			SimpleVector contact) {
		notifyCollisionListeners(null, type, algo, targets, contact);
	}

	void notifyCollisionListeners(T3DObject src, int type, int algo,
			T3DObject[] targets, SimpleVector contact) {
		if ((this.collisionListener == null) || (this.disableListeners)) {
			return;
		}

		CollisionEvent ce = new CollisionEvent(this, src, type, algo, targets,
				contact);

		int end = this.collisionListener.size();
		for (int i = 0; i < end; i++) {
			CollisionListener colly = (CollisionListener) this.collisionListener
					.get(i);
			if ((colly.requiresPolygonIDs()) && (ce.getPolygonIDs() == null)) {
				ce.setPolygonIDs(this.polygonIDs, this.pIDCount);
			}
			colly.collision(ce);
		}
	}

	void reallyStrip() {
		this.toStrip = false;
		this.hasBeenStripped = true;

		if ((!this.dynamic) && (this.staticUV)) {
			this.objVectors.strip();
			this.objMesh.strongStrip(this.myRage3DScene, this);
		}

		if ((this.compiled != null) && (Config.aggressiveStripping)) {
			this.oneTextureSet = true;
			int[] st = new int[(this.multiTex != null ? this.multiTex.length
					: 0) + 1];
			for (int i = 0; i < st.length; i++) {
				st[i] = -111;
			}

			for (CompiledInstance ci : this.compiled) {
				int index = ci.polyIndex;
				int tid = this.texture[index];

				if ((tid != st[0]) && (st[0] != -111)) {
					this.oneTextureSet = false;
					break;
				}
				st[0] = tid;

				if (this.multiTex != null) {
					for (int i = 0; i < this.multiTex.length; i++) {
						if ((st[(i + 1)] != this.multiTex[i][index])
								&& (st[(i + 1)] != -111)) {
							this.oneTextureSet = false;
							break;
						}
						st[(i + 1)] = this.multiTex[i][index];
					}
				}
			}

			if (!this.oneTextureSet) {
				Logger.log("Object '" + getName()
						+ "' uses multiple texture sets!");
			} else {
				Logger.log("Object '" + getName()
						+ "' uses one texture setKey!");
				this.texture = new int[1];
				this.texture[0] = st[0];

				if (this.multiTex != null) {
					this.multiTex = new int[this.multiTex.length][1];
					for (int i = 0; i < this.multiTex.length; i++)
						this.multiTex[i][0] = st[(i + 1)];
				}
			}
		}
	}

	void resetPolygonIDCount() {
		this.pIDCount = 0;
		this.lastAddedID = -1;
	}

	private void addPolygonID(int id) {
		if ((this.collisionListener == null) || (this.disableListeners)) {
			return;
		}

		if (this.polygonIDs == null) {
			this.polygonIDs = new int[Config.polygonIDLimit];
			this.pIDCount = 0;
		}
		if (this.pIDCount < this.polygonIDs.length) {
			if (id == this.lastAddedID) {
				return;
			}

			for (int i = 0; i < this.pIDCount; i++) {
				if (this.polygonIDs[i] == id) {
					return;
				}
			}
			this.polygonIDs[this.pIDCount] = id;
			this.lastAddedID = id;
			this.pIDCount += 1;
		}
	}

	private Matrix getTmpMatrix(int index) {
		if ((T3DScene.defaultThread != null)
				&& (Thread.currentThread() != T3DScene.defaultThread)) {
			if (matrixThreadCache == null) {
				Logger.log("Creating new matrix cache!");
				matrixThreadCache = new HashMap();
			}
			Long threadId = Long.valueOf(Thread.currentThread().getId());
			Matrix[] mArray = (Matrix[]) matrixThreadCache.get(threadId);
			if (mArray == null) {
				if (matrixThreadCache.size() > 2) {
					Logger.log("Clearing matrix cache!");
					matrixThreadCache.clear();
				}

				mArray = new Matrix[6];
				mArray[0] = new Matrix();
				mArray[1] = new Matrix();
				mArray[2] = new Matrix();
				mArray[3] = new Matrix();
				mArray[4] = new Matrix();
				mArray[5] = new Matrix();
				matrixThreadCache.put(threadId, mArray);
			}

			return mArray[index];
		}

		if (matrixArray == null) {
			matrixArray = new Matrix[6];
			matrixArray[0] = new Matrix();
			matrixArray[1] = new Matrix();
			matrixArray[2] = new Matrix();
			matrixArray[3] = new Matrix();
			matrixArray[4] = new Matrix();
			matrixArray[5] = new Matrix();
		}

		return matrixArray[index];
	}

	private void compileInternal() {
		if (this.compiled == null)
			this.compiled = new ArrayList(1);
	}
}
