package toto.graphics._3D;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import toto.graphics.color.RGBColor;
import android.annotation.SuppressLint;

@SuppressLint({ "DefaultLocale" })
public final class ModelLoader {
	private static final int DEFAULT_BUFFER = 8192;
	private static final int MAX_CACHE_SIZE = 20000;
	private static String lastFilename = "";

	private static String lastFileData = "";

	private static boolean optimize = true;

	private static byte[] backBuf = new byte[8192];

	public static void setVertexOptimization(boolean enabled) {
		/* 55 */
		optimize = enabled;

	}

	public static String loadTextFile(InputStream stream) {
		return loadTextFile(null, null, stream);

	}

	private static String loadTextFile(URL docBase, String filename,
			InputStream is) {
		if ((filename != null) && (lastFilename != null)
				&& (lastFilename.equals(filename))) {
			Logger.log("Reading file " + filename + " from cache", 2);
			return lastFileData;

		}
		/* 76 */
		clearCache();
		/* 77 */
		String[] ret = new String[1];
		/* 78 */
		loadBinaryFile(docBase, filename, is, ret);
		/* 79 */
		if ((ret != null) && (ret[0] != null)) {
			/* 80 */
			if ((ret[0].length() <= 20000) && (filename != null)) {
				/* 81 */
				lastFilename = filename;
				/* 82 */
				lastFileData = ret[0];

			}
			/* 84 */
			return ret[0];

		}

		/* 87 */
		Logger.log("Couldn't load text-file!", 0);
		/* 88 */
		return "";

	}

	public static T3DObject loadSerializedObject(InputStream is) {
		/* 104 */
		return new DeSerializer().deserialize(is)[0];

	}

	public static T3DObject[] loadSerializedObjectArray(InputStream is) {
		/* 119 */
		return new DeSerializer().deserialize(is);

	}

	public static T3DObject loadMD2(InputStream stream, float newScale) {
		/* 138 */
		return loadMD2(null, null, stream, newScale);

	}

	public static T3DObject[] load3DS(InputStream stream, float scale) {
		/* 165 */
		return load3DS(null, null, stream, scale);

	}

	public static String[] readTextureNames3DS(InputStream stream) {
		/* 176 */
		return readTextureNames3DS(null, null, stream);

	}

	public static T3DObject loadASC(InputStream stream, float scale,
			boolean swap) {
		/* 196 */
		return loadASC(null, null, stream, scale, swap);

	}

	public static T3DObject[] loadOBJ(InputStream objStream,
			InputStream mtlStream, float scale) {
		return loadOBJ(null, null, null, objStream, mtlStream, scale);
	}

	private static T3DObject[] loadOBJ(URL docBase, String objName,
			String mtlName, InputStream objStream, InputStream mtlStream,
			float scale) {
		String mtl = null;
		if ((mtlName != null) || (mtlStream != null)) {
			mtl = loadTextFile(docBase, mtlName, mtlStream);
			mtl = mtl.replace('\t', ' ');

		} else {
			mtl = "";
		}
		HashMap mats = new HashMap();
		StringTokenizer st = new StringTokenizer(mtl, "\n");
		String matName = null;
		RGBColor col = null;
		String texture = null;
		String texture2 = null;
		float[] cols = new float[3];
		boolean next = true;
		String line = null;
		Float trans = null;
		TextureCache texMan = TextureCache.getInstance();
		int mode = 0;
		while (st.hasMoreTokens()) {
			if (next) {
				line = st.nextToken().trim();
			}
			switch (mode) {
			case 0:
				if (line.startsWith("newmtl ")) {
					matName = line.substring(7).trim();
					mode = 1;
					next = true;
					col = null;
					texture = null;
					trans = null;
					Logger.log("Processing new material " + matName + "!", 2);
				}
				break;
			case 1:
				String lLine = line.toLowerCase();
				if (lLine.startsWith("kd ")) {
					String sub = lLine.substring(3).trim();
					StringTokenizer st2 = new StringTokenizer(sub, " ");
					int ind = 0;
					while ((st2.hasMoreTokens()) && (ind < 3)) {
						String c = st2.nextToken();
						try {
							cols[ind] = Float.valueOf(c).floatValue();
						} catch (Exception e) {
							cols[ind] = 1.0F;
							Logger.log("Error in MTL-file near: " + line, 0);
						}
						ind++;
					}
					col = new RGBColor((int) (cols[0] * 255.0F),
							(int) (cols[1] * 255.0F), (int) (cols[2] * 255.0F));
				} else if (((lLine.startsWith("map_kd")) || (lLine
						.startsWith("map_ka"))) && (line.length() > 7)) {
					if (lLine.startsWith("map_kd")) {
						texture = removeBogusData(line.substring(7).trim());
						if (!texMan.containsTexture(texture)) {

							texMan.addTexture(texture);

							Logger.log("Texture named " + texture
									+ " added to TextureCache!", 2);

						}

					} else {

						texture2 = removeBogusData(line.substring(7).trim());

						if (!texMan.containsTexture(texture2)) {

							texMan.addTexture(texture2);

							Logger.log("Texture named " + texture2
									+ " added to TextureCache!", 2);

						}

					}

				} else if (lLine.startsWith("d ")) {

					float tt = -1.0F;

					try {

						tt = Float.parseFloat(line.substring(2).trim());

					} catch (Exception localException1) {

					}

					if ((tt != -1.0F) && (tt != 1.0F)) {

						trans = Float.valueOf(tt);

					}

				} else if (lLine.startsWith("newmtl")) {

					mode = 0;

					next = false;

				}

				break;

			}

			if ((!next) || (!st.hasMoreTokens())) {

				mats.put(matName, new Object[] { col, texture, trans });

			}

		}

		/* 331 */
		String obj = loadTextFile(docBase, objName, objStream);
		/* 332 */
		obj = obj.replace('\t', ' ');

		/* 334 */
		if ((obj.indexOf("o ") == -1) && (obj.indexOf("g ") == -1)
				&& (obj.indexOf("g\n") == -1) && (obj.indexOf("o\n") == -1)) {
			/* 335 */
			obj = "o jPCT_generated\n" + obj;

		}

		/* 338 */
		int vertCnt = countOcc(obj, "v ");
		/* 339 */
		int uvCnt = countOcc(obj, "vt ");
		/* 340 */
		int polyCnt = countOcc(obj, "f ");
		/* 341 */
		int normalCnt = 0;

		/* 343 */
		float[][] vertices = new float[vertCnt][3];
		/* 344 */
		float[][] normals = Config.useNormalsFromOBJ ? new float[countOcc(obj,
				"vn ")][3] : null;
		/* 345 */
		float[][] uvs = new float[uvCnt][2];
		/* 346 */
		int[][][] polys = new int[polyCnt][4][3];
		/* 347 */
		String[] matNames = new String[polyCnt];
		/* 348 */
		polyCnt = 0;
		/* 349 */
		vertCnt = 0;
		/* 350 */
		uvCnt = 0;

		/* 352 */
		int[] posCache = (int[]) null;
		/* 353 */
		int[] lastPoints = (int[]) null;

		/* 355 */
		if ((!optimize) || (normals != null)) {
			/* 356 */
			posCache = new int[vertices.length];
			/* 357 */
			lastPoints = new int[3];
			/* 358 */
			for (int i = 0; i < posCache.length; i++) {
				/* 359 */
				posCache[i] = -1;

			}

		}

		/* 363 */
		st = new StringTokenizer(obj, "\n");
		/* 364 */
		String name = null;
		/* 365 */
		mtlName = null;
		/* 366 */
		int fourSided = 0;
		/* 367 */
		boolean largePolygons = false;
		/* 368 */
		boolean unsupported = false;
		/* 369 */
		ArrayList objs = new ArrayList();

		/* 371 */
		int polyStore = 0;

		/* 373 */
		while (st.hasMoreTokens()) {
			/* 374 */
			line = st.nextToken().trim();
			/* 375 */
			next = false;
			/* 376 */
			if (line.startsWith("v ")) {
				/* 378 */
				String sub = line.substring(2).trim();
				/* 379 */
				StringTokenizer st2 = new StringTokenizer(sub, " ");
				/* 380 */
				int ind = 0;
				/* 381 */
				while ((st2.hasMoreTokens()) && (ind < 3)) {
					/* 382 */
					String c = st2.nextToken();

					try {
						/* 384 */
						vertices[vertCnt][ind] = (Float.valueOf(c).floatValue() * scale);

					} catch (Exception e) {
						/* 386 */
						vertices[vertCnt][ind] = 0.0F;

					}
					/* 388 */
					ind++;

				}
				/* 390 */
				vertCnt++;

			}
			/* 392 */
			else if ((line.startsWith("vn ")) && (Config.useNormalsFromOBJ)) {
				/* 394 */
				String sub = line.substring(2).trim();
				/* 395 */
				StringTokenizer st2 = new StringTokenizer(sub, " ");
				/* 396 */
				int ind = 0;
				/* 397 */
				while ((st2.hasMoreTokens()) && (ind < 3)) {
					/* 398 */
					String c = st2.nextToken();

					try {
						/* 400 */
						normals[normalCnt][ind] = Float.valueOf(c).floatValue();

					} catch (Exception e) {
						/* 402 */
						normals[normalCnt][ind] = 0.0F;

					}
					/* 404 */
					ind++;

				}
				/* 406 */
				normalCnt++;

			}
			/* 408 */
			else if (line.startsWith("vt ")) {
				/* 409 */
				String sub = line.substring(3).trim();
				/* 410 */
				StringTokenizer st2 = new StringTokenizer(sub, " ");
				/* 411 */
				int ind = 0;
				/* 412 */
				uvs[uvCnt][1] = 0.0F;
				/* 413 */
				while ((st2.hasMoreTokens()) && (ind < 2)) {
					/* 414 */
					String c = st2.nextToken();

					try {
						/* 416 */
						uvs[uvCnt][ind] = Float.valueOf(c).floatValue();

					} catch (Exception e) {
						/* 418 */
						uvs[uvCnt][ind] = 0.0F;
						/* 419 */
						Logger.log("Error in OBJ-file near: " + line, 0);

					}
					/* 421 */
					ind++;

				}
				/* 423 */
				uvCnt++;

			}
			/* 425 */
			else if ((line.startsWith("usemtl")) && (line.length() > 6)) {
				/* 426 */
				mtlName = line.substring(7).trim();

			}
			/* 428 */
			else if (line.startsWith("f ")) {
				/* 429 */
				String sub = line.substring(2).trim();
				/* 430 */
				StringTokenizer st2 = new StringTokenizer(sub, " ");
				/* 431 */
				int ind = 0;
				/* 432 */
				while ((st2.hasMoreTokens()) && (ind < 4)) {
					/* 433 */
					if (ind == 3) {
						/* 434 */
						fourSided++;

					}
					/* 436 */
					String c = st2.nextToken();
					/* 437 */
					int pos = c.indexOf("/");
					/* 438 */
					int pos2 = -1;
					/* 439 */
					if (pos == -1)
						/* 440 */pos = c.length();

					else {
						/* 442 */
						pos2 = c.indexOf("/", pos + 1);

					}
					/* 444 */
					if (pos2 == -1) {
						/* 445 */
						pos2 = c.length();

					}
					/* 447 */
					String v = c.substring(0, pos);
					/* 448 */
					String vt = null;
					/* 449 */
					String vn = null;
					/* 450 */
					if (pos + 1 < pos2) {

						vt = c.substring(pos + 1, pos2);

					}

					if ((pos2 != c.length()) && (pos2 != -1)) {

						vn = c.substring(pos2 + 1);

					}

					try {

						polys[polyCnt][ind][0] = Integer.valueOf(v).intValue();

						if (vt == null) {

							vt = "1";

						}

						polys[polyCnt][ind][1] = Integer.valueOf(vt).intValue();

						if (vn != null)
							polys[polyCnt][ind][2] = Integer.valueOf(vn)
									.intValue();

					} catch (Exception e) {

						polys[polyCnt][ind][0] = 1;

						polys[polyCnt][ind][1] = 1;

						polys[polyCnt][ind][2] = 1;

						Logger.log("Error in OBJ-file near: " + line, 0);

					}

					ind++;

					if ((ind == 4) && (st2.hasMoreTokens())) {

						largePolygons = true;

					}

				}

				for (int i = ind; i < 4; i++) {

					polys[polyCnt][ind][0] = -9999;

					polys[polyCnt][ind][1] = -9999;

					polys[polyCnt][ind][2] = -9999;

				}

				matNames[polyCnt] = mtlName;

				polyCnt++;

			} else {

				boolean oo = line.startsWith("o");

				if ((oo) || (line.startsWith("g"))) {

					String oldName = name;

					String tname = line.substring(1).trim();

					if ((tname.length() == 0)
							&& ((name == null) || (name.length() == 0))) {

						name = "noname";

					}

					else if (tname.length() > 0) {

						name = tname;

					}

					if (polyCnt - polyStore > 0) {

						objs.add(createOBJObject(mats, polys, vertices, uvs,
								oldName != null ? oldName : name, matNames,
								polyCnt, polyStore, uvCnt, fourSided, posCache,
								lastPoints, normals));

					}

					if (oo) {

						for (int i = polyStore; i < polyCnt; i++) {

							matNames[i] = null;

						}

					}

					/* 509 */
					Logger.log("Processing object from OBJ-file: " + name, 2);

					/* 511 */
					polyStore = polyCnt;
					/* 512 */
					fourSided = 0;

				}
				/* 514 */
				else if ((!unsupported)
						&& ((line.startsWith("p ")) || (line.startsWith("l "))
								|| (line.startsWith("curv")) || (line
									.startsWith("surf")))) {
					/* 515 */
					unsupported = true;

				}

			}

			/* 523 */
			if (!st.hasMoreTokens()) {
				/* 525 */
				if (polyCnt - polyStore > 0) {
					/* 526 */
					objs.add(createOBJObject(mats, polys, vertices, uvs, name,
							matNames, polyCnt, polyStore, uvCnt, fourSided,
							posCache, lastPoints, normals));

				}

			}

		}

		/* 531 */
		if (unsupported) {
			/* 532 */
			Logger.log(
					"This OBJ-file contains unsupported geometry data. This data has been skipped!",
					1);

		}
		/* 534 */
		if (largePolygons) {
			/* 535 */
			Logger.log(
					"This OBJ-file contains n-polygons with n>4! These polygons wont be displayed correctly!",
					1);

		}

		/* 538 */
		T3DObject[] objA = new T3DObject[objs.size()];
		/* 539 */
		for (int i = 0; i < objs.size(); i++) {
			/* 540 */
			objA[i] = ((T3DObject) objs.get(i));

		}
		/* 542 */
		if (mtl != null) {
			/* 543 */
			clearCache();

		}
		/* 545 */
		return objA;

	}

	private static String removeBogusData(String texName) {
		/* 549 */
		int pos = texName.indexOf("-s");
		/* 550 */
		int pos2 = texName.indexOf("-o");
		/* 551 */
		if ((pos == -1) && (pos2 == -1)) {
			/* 552 */
			return texName;

		}
		/* 554 */
		Logger.log(
				"This .mtl-file contains bogus(?) data in the map_K? line...skipping this data!",
				3);
		/* 555 */
		if (pos2 > pos) {
			/* 556 */
			pos = pos2;

		}
		/* 558 */
		texName = texName.substring(pos + 3);
		/* 559 */
		int c = 0;
		/* 560 */
		pos = -1;

		do {
			/* 562 */
			pos = texName.indexOf(' ', pos + 1);
			/* 563 */
			c++;
			/* 564 */
		} while ((c < 3) && (pos != -1));
		/* 565 */
		if (pos != -1) {
			/* 566 */
			texName = texName.substring(pos).trim();

		}
		/* 568 */
		return texName;

	}

	private static T3DObject createOBJObject(HashMap<String, Object[]> mats,
			int[][][] polys, float[][] vertices, float[][] uvs, String name,
			String[] mtlNames, int polyCnt, int polyStore, int uvCnt, int add,
			int[] posCache, int[] lastPoints, float[][] normals) {
		T3DObject obj = new T3DObject(polyCnt - polyStore + add);
		if (name == null) {
			name = "noname";
		}
		boolean noUV = false;
		if (uvCnt == 0) {
			noUV = true;
			uvs = new float[1][2];
		}

		boolean isTrans = false;
		TextureCache tm = TextureCache.getInstance();
		/* 587 */
		int tid = -1;

		boolean optimize = true;
		/* 589 */
		// optimize = (optimize) && (!Config.useNormalsFromOBJ) && (normals ==
		// null);

		/* 591 */
		if (!optimize) {
			/* 592 */
			obj.disableVertexSharing();

		}
		/* 594 */
		String lastMtl = "jkkjkljdldld----";
		/* 595 */
		for (int i = polyStore; i < polyCnt; i++) {
			/* 597 */
			String mtlName = mtlNames[i];

			if ((mtlName != null) && (!mtlName.equals(lastMtl))) {

				tid = -1;

				lastMtl = mtlName;

				Object[] mtlData = (Object[]) mats.get(mtlName);

				RGBColor col = null;

				if (mtlData == null) {

					tid = tm.getTextureID(mtlName);

				} else {

					if (mtlData[1] != null) {

						tid = tm.getTextureID((String) mtlData[1]);

					}

					if (tid == -1) {

						col = (RGBColor) mtlData[0];

						if (col != null) {

							String colName = "__obj-Color:" + col.getRed()
									+ "/" + col.getGreen() + "/"
									+ col.getBlue();

							if (tm.containsTexture(colName)) {

								tid = tm.getTextureID(colName);

							} else {

								Texture tex = Texture
										.createSingleColoredTexture(col);

								tm.addTexture(colName, tex);

								tid = tm.getTextureID(colName);

							}

						}

					}

					if (mtlData[2] != null) {

						isTrans = true;

						obj.setTransparency((int) (10.0F * ((Float) mtlData[2])
								.floatValue()));

					}

					else if (isTrans) {

						obj.setTransparency(-1);

					}

				}

			}

			int iv1 = polys[i][0][0] - 1;

			int iv2 = polys[i][1][0] - 1;

			int iv3 = polys[i][2][0] - 1;

			int iv4 = polys[i][3][0] - 1;

			int in1 = polys[i][0][2] - 1;

			int in2 = polys[i][1][2] - 1;

			int in3 = polys[i][2][2] - 1;

			int in4 = polys[i][3][2] - 1;

			int uv1 = 0;

			int uv2 = 0;

			int uv3 = 0;

			int uv4 = 0;

			if (!noUV) {

				uv1 = Math.max(0, polys[i][0][1] - 1);

				uv2 = Math.max(0, polys[i][1][1] - 1);

				uv3 = Math.max(0, polys[i][2][1] - 1);

				uv4 = Math.max(0, polys[i][3][1] - 1);

			}

			if ((iv3 >= 0) && (iv2 >= 0)) {

				if (optimize) {

					obj.addTriangle(vertices[iv1][0], vertices[iv1][1],
							vertices[iv1][2], uvs[uv1][0], 1.0F - uvs[uv1][1],
							vertices[iv2][0], vertices[iv2][1],
							vertices[iv2][2], uvs[uv2][0], 1.0F - uvs[uv2][1],
							vertices[iv3][0], vertices[iv3][1],
							vertices[iv3][2], uvs[uv3][0], 1.0F - uvs[uv3][1],
							tid);

					if (iv4 >= 0)

						obj.addTriangle(vertices[iv1][0], vertices[iv1][1],
								vertices[iv1][2], uvs[uv1][0],
								1.0F - uvs[uv1][1], vertices[iv3][0],
								vertices[iv3][1], vertices[iv3][2],
								uvs[uv3][0], 1.0F - uvs[uv3][1],
								vertices[iv4][0], vertices[iv4][1],
								vertices[iv4][2], uvs[uv4][0],
								1.0F - uvs[uv4][1], tid);

				} else {

					lastPoints[0] = posCache[iv1];

					lastPoints[1] = posCache[iv2];

					lastPoints[2] = posCache[iv3];

					Mesh m = obj.getMesh();

					if ((normals != null) && (normals.length > 0)) {

						if ((lastPoints[0] != -1)
								&& ((m.nxOrg[lastPoints[0]] != normals[in1][0])
										|| (m.nyOrg[lastPoints[0]] != normals[in1][1]) || (m.nzOrg[lastPoints[0]] != normals[in1][2]))) {

							lastPoints[0] = -1;

						}

						if ((lastPoints[1] != -1)
								&& ((m.nxOrg[lastPoints[1]] != normals[in2][0])
										|| (m.nyOrg[lastPoints[1]] != normals[in2][1]) || (m.nzOrg[lastPoints[1]] != normals[in2][2]))) {

							lastPoints[1] = -1;

						}

						if ((lastPoints[2] != -1)
								&& ((m.nxOrg[lastPoints[2]] != normals[in3][0])
										|| (m.nyOrg[lastPoints[2]] != normals[in3][1]) || (m.nzOrg[lastPoints[2]] != normals[in3][2]))) {

							lastPoints[2] = -1;

						}

					}

					obj.addTriangle(vertices[iv1][0], vertices[iv1][1],
							vertices[iv1][2], uvs[uv1][0], 1.0F - uvs[uv1][1],
							vertices[iv2][0], vertices[iv2][1],
							vertices[iv2][2], uvs[uv2][0], 1.0F - uvs[uv2][1],
							vertices[iv3][0], vertices[iv3][1],
							vertices[iv3][2], uvs[uv3][0], 1.0F - uvs[uv3][1],
							tid, 0, false, lastPoints);

					if ((normals != null) && (normals.length > 0)) {

						m.normalsCalculated = true;

						m.nxOrg[lastPoints[0]] = normals[in1][0];

						m.nyOrg[lastPoints[0]] = normals[in1][1];

						m.nzOrg[lastPoints[0]] = normals[in1][2];

						m.nxOrg[lastPoints[1]] = normals[in2][0];

						m.nyOrg[lastPoints[1]] = normals[in2][1];

						m.nzOrg[lastPoints[1]] = normals[in2][2];

						m.nxOrg[lastPoints[2]] = normals[in3][0];

						m.nyOrg[lastPoints[2]] = normals[in3][1];

						m.nzOrg[lastPoints[2]] = normals[in3][2];

					}

					if (posCache[iv1] == -1) {

						posCache[iv1] = lastPoints[0];

					}

					if (posCache[iv2] == -1) {

						posCache[iv2] = lastPoints[1];

					}

					if (posCache[iv3] == -1) {

						posCache[iv3] = lastPoints[2];

					}

					if (iv4 >= 0) {

						lastPoints[0] = posCache[iv1];

						lastPoints[1] = posCache[iv3];

						lastPoints[2] = posCache[iv4];

						if ((normals != null) && (normals.length > 0)) {

							if ((lastPoints[0] != -1)
									&& ((m.nxOrg[lastPoints[0]] != normals[in1][0])
											|| (m.nyOrg[lastPoints[0]] != normals[in1][1]) || (m.nzOrg[lastPoints[0]] != normals[in1][2]))) {

								lastPoints[0] = -1;

							}

							if ((lastPoints[1] != -1)
									&& ((m.nxOrg[lastPoints[1]] != normals[in3][0])
											|| (m.nyOrg[lastPoints[1]] != normals[in3][1]) || (m.nzOrg[lastPoints[1]] != normals[in3][2]))) {

								lastPoints[1] = -1;

							}

							if ((lastPoints[2] != -1)
									&& ((m.nxOrg[lastPoints[2]] != normals[in4][0])
											|| (m.nyOrg[lastPoints[2]] != normals[in4][1]) || (m.nzOrg[lastPoints[2]] != normals[in4][2]))) {

								lastPoints[2] = -1;

							}

						}

						obj.addTriangle(vertices[iv1][0], vertices[iv1][1],
								vertices[iv1][2], uvs[uv1][0],
								1.0F - uvs[uv1][1], vertices[iv3][0],
								vertices[iv3][1], vertices[iv3][2],
								uvs[uv3][0], 1.0F - uvs[uv3][1],
								vertices[iv4][0], vertices[iv4][1],
								vertices[iv4][2], uvs[uv4][0],
								1.0F - uvs[uv4][1], tid, 0, false, lastPoints);

						if ((normals != null) && (normals.length > 0)) {

							m.normalsCalculated = true;

							m.nxOrg[lastPoints[0]] = normals[in1][0];

							m.nyOrg[lastPoints[0]] = normals[in1][1];

							m.nzOrg[lastPoints[0]] = normals[in1][2];

							m.nxOrg[lastPoints[1]] = normals[in3][0];

							m.nyOrg[lastPoints[1]] = normals[in3][1];

							m.nzOrg[lastPoints[1]] = normals[in3][2];

							m.nxOrg[lastPoints[2]] = normals[in4][0];

							m.nyOrg[lastPoints[2]] = normals[in4][1];

							m.nzOrg[lastPoints[2]] = normals[in4][2];

						}

						if (posCache[iv1] == -1) {

							posCache[iv1] = lastPoints[0];

						}

						if (posCache[iv3] == -1) {

							posCache[iv3] = lastPoints[1];

						}

						if (posCache[iv4] == -1) {

							posCache[iv4] = lastPoints[2];

						}

					}

				}

			}

		}
		/* 767 */
		obj.setName(name + "Rage3D" + obj.getID());
		/* 768 */
		obj.getMesh().compress();

		/* 770 */
		Logger.log(
				"Object '" + obj.getName() + "' created using "
						+ obj.getMesh().anzTri + " polygons and "
						+ obj.getMesh().anzCoords + " vertices.", 2);
		/* 771 */
		return obj;

	}

	private static int countOcc(String data, String oc) {
		/* 775 */
		int cnt = 0;
		/* 776 */
		int pos = 0;
		/* 777 */
		int len = oc.length();

		do {
			/* 779 */
			pos = data.indexOf(oc, pos + len);
			/* 780 */
			if (pos != -1)
				/* 781 */cnt++;

		}
		/* 783 */while (pos != -1);
		/* 784 */
		return cnt;

	}

	private static T3DObject loadASC(URL docBase, String filename,
			InputStream resource, float scale, boolean swap) {
		/* 788 */
		float tcu1 = 0.0F;
		/* 789 */
		float tcv1 = 0.0F;
		/* 790 */
		float tcu2 = 0.0F;
		/* 791 */
		float tcv2 = 0.0F;
		/* 792 */
		float tcu3 = 0.0F;
		/* 793 */
		float tcv3 = 0.0F;

		/* 795 */
		T3DObject obj = new T3DObject(-1);
		/* 796 */
		int texnum = 0;

		/* 798 */
		int finalAnz = 0;

		/* 800 */
		boolean mapped = false;

		/* 802 */
		float[] x = new float[Config.loadMaxVerticesASC];
		/* 803 */
		float[] y = new float[Config.loadMaxVerticesASC];
		/* 804 */
		float[] z = new float[Config.loadMaxVerticesASC];

		/* 806 */
		int[] p1 = new int[Config.loadMaxTrianglesASC];
		/* 807 */
		int[] p2 = new int[Config.loadMaxTrianglesASC];
		/* 808 */
		int[] p3 = new int[Config.loadMaxTrianglesASC];

		/* 810 */
		float[] tu = new float[1];
		/* 811 */
		float[] tv = new float[1];

		/* 813 */
		int anzVector = 0;
		/* 814 */
		int lastVector = 0;
		/* 815 */
		int anzTri = 0;
		/* 816 */
		int aktPos4 = 0;
		/* 817 */
		int aktPos5 = 0;
		/* 818 */
		int part = 1;

		/* 820 */
		String sub5 = "";
		/* 821 */
		String sub4 = "";
		/* 822 */
		String sub3 = "";

		/* 824 */
		String file = loadTextFile(docBase, filename, resource);
		/* 825 */
		if (!file.endsWith("\n")) {
			/* 826 */
			file = file + "\n";

		}

		/* 829 */
		if (!file.equals("error")) {
			/* 830 */
			Logger.log("Parsing Objectfile!", 2);

			/* 832 */
			if (file.indexOf("U:") != -1) {
				/* 833 */
				mapped = true;
				/* 834 */
				tu = new float[Config.loadMaxTrianglesASC];
				/* 835 */
				tv = new float[Config.loadMaxTrianglesASC];
				/* 836 */
				Logger.log(
						"Objectfile contains additional Texture coordinates!",
						2);

			}

			/* 839 */
			String oneLine = "";

			/* 841 */
			int aktStore = 0;
			/* 842 */
			int aktLast = 0;

			/* 844 */
			int aktPos = file.indexOf("Vertex list");

			/* 846 */
			aktPos = file.indexOf("\n", aktPos + 10);

			/* 848 */
			while (aktPos != -1) {
				/* 849 */
				lastVector = anzVector;

				/* 851 */
				while (aktPos != -1) {
					/* 853 */
					aktPos++;

					/* 855 */
					int aktN = file.indexOf("\n", aktPos);
					/* 856 */
					aktStore = aktN + 1;
					/* 857 */
					aktLast = aktPos;

					/* 859 */
					oneLine = file.substring(aktPos, aktStore);

					/* 861 */
					if (oneLine.indexOf("X:") != -1) {
						/* 863 */
						int aktTemp = oneLine.indexOf("X:", 8);

						/* 865 */
						aktTemp += 2;
						/* 866 */
						aktPos = oneLine.indexOf("Y:", aktTemp);
						/* 867 */
						String sub1 = oneLine.substring(aktTemp, aktPos).trim();
						/* 868 */
						int aktPos2 = oneLine.indexOf("Z:", aktPos + 2);
						/* 869 */
						String sub2 = oneLine.substring(aktPos + 2, aktPos2)
								.trim();
						/* 870 */
						int aktPos3 = oneLine.indexOf("U:", aktPos2 + 2);

						/* 872 */
						mapped = false;

						/* 874 */
						if (aktPos3 != -1) {
							/* 875 */
							sub3 = oneLine.substring(aktPos2 + 2, aktPos3)
									.trim();
							/* 876 */
							aktPos4 = oneLine.indexOf("V:", aktPos3 + 2);
							/* 877 */
							sub4 = oneLine.substring(aktPos3 + 2, aktPos4)
									.trim();
							/* 878 */
							aktPos5 = oneLine.indexOf("\n", aktPos4 + 2);
							/* 879 */
							sub5 = oneLine.substring(aktPos4 + 2, aktPos5)
									.trim();
							/* 880 */
							aktPos3 = aktPos5;
							/* 881 */
							mapped = true;

						} else {
							/* 883 */
							aktPos3 = oneLine.indexOf("\n", aktPos2 + 2);
							/* 884 */
							sub3 = oneLine.substring(aktPos2 + 2, aktPos3)
									.trim();

						}

						/* 887 */
						if (swap) {
							/* 889 */
							Float xF = Float.valueOf(sub1);
							/* 890 */
							Float zF = Float.valueOf(sub2);
							/* 891 */
							Float yF = Float.valueOf(sub3);

							/* 893 */
							x[anzVector] = (xF.floatValue() * scale);
							/* 894 */
							y[anzVector] = (-yF.floatValue() * scale);
							/* 895 */
							z[anzVector] = (zF.floatValue() * scale);

						} else {
							/* 899 */
							Float xF = Float.valueOf(sub1);
							/* 900 */
							Float yF = Float.valueOf(sub2);
							/* 901 */
							Float zF = Float.valueOf(sub3);

							/* 903 */
							x[anzVector] = (xF.floatValue() * scale);
							/* 904 */
							y[anzVector] = (-yF.floatValue() * scale);
							/* 905 */
							z[anzVector] = (-zF.floatValue() * scale);

						}

						/* 909 */
						if (mapped) {
							/* 910 */
							tu[anzVector] = Float.valueOf(sub4).floatValue();
							/* 911 */
							tv[anzVector] = Float.valueOf(sub5).floatValue();

						}

						/* 914 */
						anzVector++;

						/* 916 */
						aktPos = aktN;

					} else {
						/* 919 */
						aktPos = aktN;
						/* 920 */
						if (oneLine.indexOf("Face list") != -1) {
							/* 921 */
							aktPos = -1;

						}

					}

				}

				/* 926 */
				aktPos = aktStore - 1;

				/* 928 */
				while (aktPos != -1) {
					/* 930 */
					aktPos++;

					/* 932 */
					int aktN = file.indexOf("\n", aktPos);
					/* 933 */
					aktStore = aktN + 1;
					/* 934 */
					aktLast = aktPos;

					/* 936 */
					oneLine = file.substring(aktPos, aktStore);

					/* 938 */
					if (oneLine.indexOf("A:") != -1) {
						/* 940 */
						int aktTemp = oneLine.indexOf("A:", 5);
						/* 941 */
						aktTemp += 2;
						/* 942 */
						aktPos = oneLine.indexOf("B:", aktTemp);
						/* 943 */
						String sub1 = oneLine.substring(aktTemp, aktPos).trim();
						/* 944 */
						int aktPos2 = oneLine.indexOf("C:", aktPos + 2);
						/* 945 */
						String sub2 = oneLine.substring(aktPos + 2, aktPos2)
								.trim();

						/* 947 */
						int aktPos3 = 0;

						/* 949 */
						aktPos3 = oneLine.indexOf("AB:", aktPos2 + 2);
						/* 950 */
						sub3 = oneLine.substring(aktPos2 + 2, aktPos3).trim();

						/* 952 */
						Integer p1F = Integer.valueOf(sub1);
						/* 953 */
						Integer p2F = Integer.valueOf(sub2);
						/* 954 */
						Integer p3F = Integer.valueOf(sub3);

						/* 956 */
						p1[anzTri] = (p1F.intValue() + lastVector);
						/* 957 */
						p2[anzTri] = (p2F.intValue() + lastVector);
						/* 958 */
						p3[anzTri] = (p3F.intValue() + lastVector);

						/* 960 */
						anzTri++;
						/* 961 */
						aktPos = aktN;

					} else {
						/* 964 */
						aktPos = aktN;
						/* 965 */
						if (oneLine.indexOf("Vertex list") != -1) {
							/* 966 */
							aktPos = -1;

						}
						/* 968 */
						if (aktStore >= file.length()) {
							/* 969 */
							aktPos = -1;

						}

					}

				}

				/* 975 */
				Logger.log("Part: " + part + " / Faces: " + anzTri
						+ " / Vertices: " + anzVector, 2);

				/* 977 */
				part++;
				/* 978 */
				aktPos = file.indexOf("Vertex list", aktLast);

			}

			/* 981 */
			obj.objMesh = new Mesh((anzTri * 2 + 1) * 3 + 8);
			/* 982 */
			obj.objVectors = new Vectors((anzTri + 1) * 3 + 8, obj.objMesh);
			/* 983 */
			obj.texture = new int[anzTri + 1];
			/* 984 */
			if (!optimize) {
				/* 985 */
				obj.disableVertexSharing();

			}

			/* 988 */
			for (int i = 0; i < anzTri; i++) {
				/* 989 */
				float texX = 1.0F;
				/* 990 */
				float texY = 1.0F;
				/* 991 */
				float texXh = texX;
				/* 992 */
				float texYh = texY;

				/* 994 */
				if (!mapped) {
					/* 995 */
					if ((i & 0x1) == 1) {
						/* 996 */
						tcu1 = 0.0F;
						/* 997 */
						tcv1 = 0.0F;
						/* 998 */
						tcu2 = texX;
						/* 999 */
						tcv2 = 0.0F;
						/* 1000 */
						tcu3 = 0.0F;
						/* 1001 */
						tcv3 = texY;

					} else {
						/* 1003 */
						tcu1 = texX;
						/* 1004 */
						tcv1 = 0.0F;
						/* 1005 */
						tcu2 = texX;
						/* 1006 */
						tcv2 = texY;
						/* 1007 */
						tcu3 = 0.0F;
						/* 1008 */
						tcv3 = texY;

					}

				} else {
					/* 1011 */
					tcu1 = texXh * tu[p1[i]];
					/* 1012 */
					tcv1 = texYh - texYh * tv[p1[i]];
					/* 1013 */
					tcu2 = texXh * tu[p2[i]];
					/* 1014 */
					tcv2 = texYh - texYh * tv[p2[i]];
					/* 1015 */
					tcu3 = texXh * tu[p3[i]];
					/* 1016 */
					tcv3 = texYh - texYh * tv[p3[i]];

					/* 1018 */
					if (tcu1 < 0.0F) {
						/* 1019 */
						tcu1 = 0.0F;

					}
					/* 1021 */
					if (tcu1 > texX) {
						/* 1022 */
						tcu1 = texX;

					}
					/* 1024 */
					if (tcv1 < 0.0F) {
						/* 1025 */
						tcv1 = 0.0F;

					}
					/* 1027 */
					if (tcv1 > texY) {
						/* 1028 */
						tcv1 = texY;

					}
					/* 1030 */
					if (tcu2 < 0.0F) {
						/* 1031 */
						tcu2 = 0.0F;

					}
					/* 1033 */
					if (tcu2 > texX) {
						/* 1034 */
						tcu2 = texX;

					}
					/* 1036 */
					if (tcv2 < 0.0F) {
						/* 1037 */
						tcv2 = 0.0F;

					}
					/* 1039 */
					if (tcv2 > texY) {
						/* 1040 */
						tcv2 = texY;

					}
					/* 1042 */
					if (tcu3 < 0.0F) {
						/* 1043 */
						tcu3 = 0.0F;

					}
					/* 1045 */
					if (tcu3 > texX) {
						/* 1046 */
						tcu3 = texX;

					}
					/* 1048 */
					if (tcv3 < 0.0F) {
						/* 1049 */
						tcv3 = 0.0F;

					}
					/* 1051 */
					if (tcv3 > texY) {
						/* 1052 */
						tcv3 = texY;

					}

				}

				/* 1056 */
				finalAnz = obj.addTriangle(x[p1[i]], y[p1[i]], z[p1[i]], tcu1,
						tcv1, x[p2[i]], y[p2[i]], z[p2[i]], tcu2, tcv2,
						x[p3[i]], y[p3[i]], z[p3[i]], tcu3, tcv3, texnum, 0,
						/* 1057 */false);

			}

			/* 1060 */
			Logger.log("Loaded Rage3DObject: Faces: " + anzTri
					+ " / Vertices: " + anzVector, 2);
			/* 1061 */
			Logger.log("Optimized Rage3DObject: Faces: " + (finalAnz + 1)
					+ " / Vertices: " + obj.objMesh.anzCoords, 2);

			/* 1063 */
			return obj;

		}
		/* 1065 */
		return null;

	}

	public static void clearCache() {
		/* 1075 */
		lastFileData = null;
		/* 1076 */
		lastFilename = null;

	}

	private static int getInt(byte[] b, int offset) {
		/* 1080 */
		if (offset + 3 < b.length) {
			/* 1081 */
			int a = unsignedByteToInt(b[offset]);
			/* 1082 */
			int d = unsignedByteToInt(b[(offset + 1)]);
			/* 1083 */
			int e = unsignedByteToInt(b[(offset + 2)]);
			/* 1084 */
			int f = unsignedByteToInt(b[(offset + 3)]);
			/* 1085 */
			return a + (d << 8) + (e << 16) + (f << 24);

		}
		/* 1087 */
		return -1;

	}

	private static int getShortInt(byte[] b, int offset) {
		/* 1091 */
		if (offset + 1 < b.length) {
			/* 1092 */
			int a = unsignedByteToInt(b[offset]);
			/* 1093 */
			int d = unsignedByteToInt(b[(offset + 1)]);
			/* 1094 */
			return a + (d << 8);

		}
		/* 1096 */
		return -1;

	}

	private static int getUnsignedByte(byte[] b, int offset) {
		/* 1100 */
		if (offset < b.length) {
			/* 1101 */
			return unsignedByteToInt(b[offset]);

		}
		/* 1103 */
		return -1;

	}

	private static int unsignedByteToInt(byte b) {
		/* 1107 */
		return b & 0xFF;

	}

	private static String getSequenceName(String a) {
		/* 1111 */
		char c = ' ';
		/* 1112 */
		StringBuffer res = new StringBuffer(16);
		/* 1113 */
		a = a.toLowerCase();
		/* 1114 */
		for (int i = 0; i < a.length(); i++) {
			/* 1115 */
			c = a.charAt(i);
			/* 1116 */
			if ((c >= 'a') && (c <= 'z')) {
				/* 1117 */
				res.append(c);

			}

		}
		/* 1120 */
		return res.toString();

	}

	private static byte[] loadBinaryFile(URL docBase, String filename,
			InputStream is) {
		/* 1124 */
		return loadBinaryFile(docBase, filename, is, null);

	}

	private static synchronized byte[] loadBinaryFile(URL docBase,
			String filename, InputStream is, String[] ret) {
		/* 1128 */
		byte[] buf = (byte[]) null;
		/* 1129 */
		int bufSize = 8192;
		/* 1130 */
		if (backBuf == null) {
			/* 1131 */
			buf = new byte[bufSize];
			/* 1132 */
			backBuf = buf;

		} else {
			/* 1134 */
			buf = backBuf;

		}
		/* 1136 */
		int anzAkt = 0;
		/* 1137 */
		int anz = 0;
		/* 1138 */
		int aktsize = buf.length;
		/* 1139 */
		int incs = 0;

		/* 1141 */
		if (filename == null) {
			/* 1142 */
			filename = "from InputStream";

		}

		try {

			Logger.log("Loading file " + filename, 2);

			if (is == null) {

				if (docBase == null) {

					is = new FileInputStream(new File(filename));

				} else {

					URL url = new URL(docBase, filename);

					is = url.openStream();

				}

			}

			if (ret == null) {

				do {

					anzAkt = is.read(buf, anz, aktsize - anz);

					if (anzAkt > 0) {

						anz += anzAkt;

					}

					if ((anzAkt != -1) && (anz >= aktsize)) {

						incs++;

						if (incs == 10) {

							bufSize *= 10;

						}

						aktsize += bufSize;

						byte[] npuffer = new byte[aktsize];

						System.arraycopy(buf, 0, npuffer, 0, aktsize - bufSize);

						buf = npuffer;

						backBuf = buf;

						Logger.log("Expanding buffers..." + aktsize + " bytes",
								2);

					}

				} while (anzAkt > 0);

				Logger.log("File " + filename + " loaded..." + anz + " bytes",
						2);

			} else {

				StringBuilder sb = new StringBuilder();

				do {

					anzAkt = is.read(buf, 0, aktsize);

					if (anzAkt > 0)
						sb.append(new String(buf, 0, anzAkt));

				} while (anzAkt > 0);

				ret[0] = sb.toString();

				Logger.log("Text file " + filename + " loaded..." + sb.length()
						+ " bytes", 2);

			}

		} catch (Throwable e) {
			Logger.log("Couldn't read file " + filename, 0);
			byte[] arrayOfByte1 = new byte[0];
			return arrayOfByte1;

		} finally {

			if (is != null)
				try {

					is.close();

				} catch (Exception localException1) {

				}

		}
		if ((backBuf != null) && (backBuf.length > 65536)) {
			backBuf = null;

		}

		if (ret == null) {
			byte[] backBuf = new byte[anz];
			System.arraycopy(buf, 0, backBuf, 0, anz);
			return backBuf;

		}
		return null;

	}

	public static T3DObject loadJAW(InputStream inputstream, float f,
			boolean flag) {
		return loadJAW(null, null, inputstream, f, flag);
	}

	private static int getCharCount(String s, char c) {
		int i = 0;
		for (int j = 0; j < s.length(); j++)
			if (s.charAt(j) == c)
				i++;

		return i;
	}

	private static T3DObject loadJAW(URL url, String s,
			InputStream inputstream, float f, boolean flag) {
		float f1 = 0.0F;
		float f3 = 0.0F;
		float f5 = 0.0F;
		float f7 = 0.0F;
		float f9 = 0.0F;
		float f11 = 0.0F;
		int i = 0;
		T3DObject object3d = new T3DObject(-1);
		TextureCache texturemanager = TextureCache.getInstance();
		int j = 0;
		int k = 0;
		String s1 = "";
		String s3 = "";
		String s6 = loadTextFile(url, s, inputstream);
		if (!s6.endsWith("\n"))
			s6 = s6 + "\n";
		int l = getCharCount(s6, ':') * 3;
		int i1 = getCharCount(s6, 't');
		float af[] = new float[l];
		float af1[] = new float[l];
		float af2[] = new float[l];
		int ai[] = new int[i1];
		int ai1[] = new int[i1];
		int ai2[] = new int[i1];
		String as[] = new String[i1];
		boolean flag1 = false;
		if (!s6.equals("error")) {
			Logger.log("Parsing Objectfile!", 2);
			for (int j1 = s6.indexOf(":"); j1 != -1;) {
				while (j1 != -1) {
					int k1 = j1 + 2;
					j1 = s6.indexOf(" ", k1);
					String s7 = s6.substring(k1, j1).trim();
					int k2 = s6.indexOf(" ", j1 + 1);
					String s9 = s6.substring(j1, k2).trim();
					int j3 = s6.indexOf("\n", k2 + 1);
					String s4 = s6.substring(k2, j3).trim();
					j1 = s6.indexOf(":", j3 + 1);
					Float float1 = Float.valueOf(s7);
					Float float2 = Float.valueOf(s9);
					Float float3 = Float.valueOf(s4);
					af[j] = float1.floatValue() * f;
					af1[j] = -float2.floatValue() * f;
					af2[j] = -float3.floatValue() * f;
					j++;
				}
				for (j1 = s6.indexOf("tri"); j1 != -1;) {
					int l1 = j1 + 4;
					j1 = s6.indexOf(",", l1);
					String s8 = s6.substring(l1, j1).trim();
					int l2 = s6.indexOf(",", j1 + 1);
					String s10 = s6.substring(j1 + 1, l2).trim();
					int k3 = 0;
					String s2;
					String s5;
					if (!flag) {
						k3 = s6.indexOf("\n", l2 + 1);
						s5 = s6.substring(l2 + 1, k3).trim();
						s2 = null;
					} else {
						k3 = s6.indexOf(",", l2 + 1);
						s5 = s6.substring(l2 + 1, k3).trim();
						int l3 = s6.indexOf("\n", k3 + 1);
						s2 = s6.substring(k3 + 1, l3).trim();
						k3 = l3;
					}
					j1 = s6.indexOf("tri", k3 + 1);
					Integer integer = Integer.valueOf(s8);
					Integer integer1 = Integer.valueOf(s10);
					Integer integer2 = Integer.valueOf(s5);
					ai[k] = integer.intValue();
					ai1[k] = integer1.intValue();
					ai2[k] = integer2.intValue();
					as[k] = s2;
					k++;
				}

				object3d.objMesh = new Mesh((k * 2 + 1) * 3 + 8);
				object3d.objVectors = new Vectors((k + 1) * 3 + 8,
						object3d.objMesh);
				object3d.texture = new int[k + 1];
				// object3d.basemap = new int[k + 1];
				int i2 = 0;
				while (i2 < k) {
					int j2 = 0;
					if (as[i2] != null)
						j2 = texturemanager.getTextureID(as[i2]);
					int i3 = j2;
					float f13 = 1.0F;
					float f14 = 1.0F;
					float f2;
					float f4;
					float f6;
					float f8;
					float f10;
					float f12;
					if ((i2 & 1) == 1) {
						f2 = f13;
						f4 = f14;
						f6 = f13;
						f8 = 0.0F;
						f10 = 0.0F;
						f12 = 0.0F;
					} else {
						f2 = 0.0F;
						f4 = f14;
						f6 = f13;
						f8 = f14;
						f10 = 0.0F;
						f12 = 0.0F;
					}
					i = object3d.addTriangle(af[ai[i2]], af1[ai[i2]],
							af2[ai[i2]], f2, f4, af[ai1[i2]], af1[ai1[i2]],
							af2[ai1[i2]], f6, f8, af[ai2[i2]], af1[ai2[i2]],
							af2[ai2[i2]], f10, f12, i3, 0, false);
					i2++;
				}
			}

			Logger.log(
					"Loaded Rage3DObject: Faces: " + k + " / Vertices: " + j, 2);
			Logger.log("Optimized Rage3DObject: Faces: " + (i + 1)
					+ " / Vertices: " + object3d.objMesh.anzCoords, 2);
			return object3d;
		} else {
			return null;
		}
	}

	private static T3DObject loadMD2(URL docBase, String filename,
			InputStream is, float newScale) {
		Vector[] adjList = (Vector[]) null;
		boolean error = false;

		byte[] buf = loadBinaryFile(docBase, filename, is);
		if (buf == null) {
			error = true;
		}

		if (!error) {
			int magicNum = getInt(buf, 0);
			if (magicNum != 844121161) {
				Logger.log("Not a valid MD2-file!", 0);
			}

			int version = getInt(buf, 4);
			int skinWidth = getInt(buf, 8);
			int skinHeight = getInt(buf, 12);
			int frameSize = getInt(buf, 16);
			int numSkins = getInt(buf, 20);
			int numVertices = getInt(buf, 24);
			int numTexCoords = getInt(buf, 28);
			int numTriangles = getInt(buf, 32);
			int numGlCommands = getInt(buf, 36);
			int numFrames = getInt(buf, 40);
			int offsetTexCoords = getInt(buf, 48);
			int offsetTriangles = getInt(buf, 52);
			int offsetFrames = getInt(buf, 56);

			Logger.log("Magic number: " + magicNum, 2);
			Logger.log("Version: " + version, 2);
			Logger.log("Skin width: " + skinWidth, 2);
			Logger.log("Skin height: " + skinHeight, 2);
			Logger.log("Frame size: " + frameSize, 2);
			Logger.log("Number of skins: " + numSkins, 2);
			Logger.log("Number of Vertices: " + numVertices, 2);
			Logger.log("Number of Texture coordinates: " + numTexCoords, 2);
			Logger.log("Number of triangles: " + numTriangles, 2);
			Logger.log("Number of GL-commands: " + numGlCommands, 2);
			Logger.log("Number of Frames: " + numFrames, 2);

			int[][] texCoords = new int[numTexCoords][2];
			int[][] triVertex = new int[numTriangles][3];
			int[][] triTexture = new int[numTriangles][3];

			Logger.log("Reading Texture coordinates...", 2);
			int oF = offsetTexCoords;
			for (int i = 0; i < numTexCoords; i++) {
				int u = getShortInt(buf, oF + i * 4);
				int v = getShortInt(buf, oF + i * 4 + 2);
				texCoords[i][0] = u;
				texCoords[i][1] = v;
			}
			Logger.log("Done!", 2);

			Logger.log("Reading polygonal data...", 2);
			oF = offsetTriangles;
			for (int i = 0; i < numTriangles; i++) {
				int iMul = oF + i * 12;
				int p1 = getShortInt(buf, iMul);
				int p2 = getShortInt(buf, iMul + 2);
				int p3 = getShortInt(buf, iMul + 4);
				int t1 = getShortInt(buf, iMul + 6);
				int t2 = getShortInt(buf, iMul + 8);
				int t3 = getShortInt(buf, iMul + 10);
				triVertex[i][0] = p1;
				triVertex[i][1] = p2;
				triVertex[i][2] = p3;
				triTexture[i][0] = t1;
				triTexture[i][1] = t2;
				triTexture[i][2] = t3;
			}
			Logger.log("Done!", 2);

			float[][] scales = new float[numFrames][3];
			float[][] trans = new float[numFrames][3];
			String[] names = new String[numFrames];
			int[][][] vertices = new int[numFrames][numVertices][3];

			Logger.log("Reading keyframes...", 2);
			for (int i = 0; i < numFrames; i++) {
				oF = i * frameSize + offsetFrames;
				float scaleX = Float.intBitsToFloat(getInt(buf, oF));
				float scaleY = Float.intBitsToFloat(getInt(buf, oF + 4));
				float scaleZ = Float.intBitsToFloat(getInt(buf, oF + 8));
				float transX = Float.intBitsToFloat(getInt(buf, oF + 12));
				float transY = Float.intBitsToFloat(getInt(buf, oF + 16));
				float transZ = Float.intBitsToFloat(getInt(buf, oF + 20));

				String name = new String(buf, oF + 24, 16);
				scales[i][0] = scaleX;
				scales[i][1] = scaleY;
				scales[i][2] = scaleZ;
				trans[i][0] = transX;
				trans[i][1] = transY;
				trans[i][2] = transZ;
				names[i] = name;

				oF += 40;
				for (int p = 0; p < numVertices; p++) {
					int iMul = oF + p * 4;
					int v1 = getUnsignedByte(buf, iMul);
					int v2 = getUnsignedByte(buf, iMul + 1);
					int v3 = getUnsignedByte(buf, iMul + 2);
					vertices[i][p][0] = v1;
					vertices[i][p][1] = v2;
					vertices[i][p][2] = v3;
				}
			}
			Logger.log("Done!", 2);
			Logger.log("Coverting MD2-format into jPCT-format...", 2);

			T3DObject obj = new T3DObject(numTriangles + 1);
			T3DObject tmp = new T3DObject(numTriangles + 1);
			Animation anim = new Animation(numFrames);
			String lastSeq = "dummy";

			for (int p = 0; p < numFrames; p++) {
				tmp.clearObject();
				int frame = p;

				for (int e = 0; e < numVertices; e++) {
					float v1x = vertices[frame][e][0] * scales[frame][0]
							+ trans[frame][0];
					float v1y = vertices[frame][e][1] * scales[frame][1]
							+ trans[frame][1];
					float v1z = vertices[frame][e][2] * scales[frame][2]
							+ trans[frame][2];

					v1x *= newScale;
					v1y *= newScale;
					v1z *= newScale;

					tmp.objVectors.addVertex(v1x, -v1z, v1y);
					if (p == 0) {
						obj.objVectors.addVertex(v1x, -v1z, v1y);
					}
				}

				for (int i = 0; i < numTriangles; i++) {
					int v1p = triVertex[i][0];
					int v2p = triVertex[i][2];
					int v3p = triVertex[i][1];

					int t1p = triTexture[i][0];
					int t2p = triTexture[i][2];
					int t3p = triTexture[i][1];

					float t1u = texCoords[t1p][0] / skinWidth;
					float t1v = texCoords[t1p][1] / skinHeight;

					float t2u = texCoords[t2p][0] / skinWidth;
					float t2v = texCoords[t2p][1] / skinHeight;

					float t3u = texCoords[t3p][0] / skinWidth;
					float t3v = texCoords[t3p][1] / skinHeight;

					if (p == 0) {
						obj.addMD2Triangle(v1p, t1u, t1v, v2p, t2u, t2v, v3p,
								t3u, t3v);
					}
					tmp.addMD2Triangle(v1p, t1u, t1v, v2p, t2u, t2v, v3p, t3u,
							t3v);
				}

				tmp.calcBoundingBox();
				adjList = tmp.objMesh.calcNormalsMD2(adjList);

				String tmpS = getSequenceName(names[p]);
				if (!tmpS.equals(lastSeq)) {
					Logger.log("Processing: " + tmpS + "...", 2);
					lastSeq = tmpS;
					anim.createSubSequence(tmpS);
				}

				anim.addKeyFrame(tmp.getMesh().cloneMesh(true));
			}

			obj.calcBoundingBox();
			obj.setAnimationSequence(anim);

			Logger.log("Done!", 2);

			return obj;
		}

		return null;
	}

	private static String[] readTextureNames3DS(URL docBase, String filename,
			InputStream resource) {
		byte[] buf = loadBinaryFile(docBase, filename, resource);
		int[] data = new int[2];
		int[] tmpData = new int[2];
		int offset = 0;
		int id = -1;
		int size = -1;
		ArrayList names = new ArrayList();
		StringBuilder nBuf = null;

		if (buf != null) {
			getChunkHeader(buf, offset, data);
			offset += 6;
			id = data[0];
			size = data[1];
			if (id != 19789) {
				Logger.log("Not a valid 3DS file!", 0);
			} else {
				boolean unknownChunk = true;
				while ((id >= 0) && (offset < buf.length)) {
					unknownChunk = true;
					getChunkHeader(buf, offset, data);
					offset += 6;
					id = data[0];
					size = data[1];
					if ((id >= 0) && (offset < buf.length)) {
						if (id == 45072) {
							int c = 0;
							do {
								c = getUnsignedByte(buf, offset);
								offset++;
							} while (c > 0);
							offset += 6;
							unknownChunk = false;
						}

						if (id == 45088) {
							offset += 20;
							offset += 4;
							offset += 4;
							offset += 4;
							unknownChunk = false;
						}

						if (id == 16384) {
							int c = 0;
							do {
								c = getUnsignedByte(buf, offset);
								offset++;
							} while (c > 0);
							unknownChunk = false;
						}

						if (id == 16656) {
							int anz = getShortInt(buf, offset);

							offset += 2;
							for (int i = 0; i < anz; i++) {
								offset += 4;
								offset += 4;
								offset += 4;
							}
							unknownChunk = false;
						}

						if (id == 16672) {
							unknownChunk = true;
						}

						if (id == 45055) {
							int offy = offset;
							while ((offy < offset + size - 6)
									&& (offy < buf.length)) {
								getChunkHeader(buf, offy, tmpData);
								offy += 6;
								int tmpId = tmpData[0];
								int tmpSize = tmpData[1];

								if (tmpId == 41472) {
									int offy2 = offy;
									while ((offy2 < offy + size - 6)
											&& (offy2 < buf.length)) {
										getChunkHeader(buf, offy2, tmpData);
										offy2 += 6;
										int tmpId2 = tmpData[0];
										int tmpSize2 = tmpData[1];

										if (tmpId2 == 41728) {
											nBuf = new StringBuilder(40);
											int c = 0;
											int offy3 = offy2;
											do {
												c = getUnsignedByte(buf, offy3);
												offy3++;
												if (c > 0) {
													nBuf.append((char) (byte) c);
												}
												if (offy3 >= buf.length)
													c = -1;
											} while (c > 0);
											String n = nBuf.toString();
											if (!names.contains(n)) {
												names.add(n);
											}
										}

										offy2 += tmpSize2 - 6;
									}
								}
								offy += tmpSize - 6;
							}

							unknownChunk = true;
						}

						if ((id == 15677) || (id == 45056) || (id == 45058)) {
							unknownChunk = false;
						}

						if (unknownChunk) {
							if (offset + size - 6 >= buf.length)
								offset = buf.length;
							else {
								offset += size - 6;
							}
						}
					}
				}
			}
		}

		String[] res = new String[names.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = ((String) names.get(i));
		}
		return res;
	}

	private static T3DObject[] load3DS(URL docBase, String filename,
			InputStream resource, float scale) {
		byte[] buf = loadBinaryFile(docBase, filename, resource);
		int[] data = new int[2];
		int[] tmpData = new int[2];
		int offset = 0;
		int id = -1;
		int size = -1;
		ArrayList objs = new ArrayList();
		StringBuilder nBuf = null;

		float[][] vertices = (float[][]) null;
		int[][] coords = (int[][]) null;
		String[] coordMat = (String[]) null;
		float[][] matTextureData = new float[10][4];
		String[] matName = new String[10];
		String[] textureName = new String[10];
		RGBColor[] matColor = new RGBColor[10];
		int[] matTrans = new int[10];

		for (int m = 0; m < matTrans.length; m++) {
			matTextureData[m][0] = 1.0F;
			matTextureData[m][1] = 1.0F;
			matTextureData[m][2] = 0.0F;
			matTextureData[m][3] = 0.0F;
			matTrans[m] = 1000;
		}

		int vertexCnt = 0;
		int texCnt = 0;
		int crdCnt = 0;
		int matCnt = 0;
		String name = "";
		String oldName = "";
		String lastKeyframeName = "";
		Map name2pivot = new HashMap();

		if (buf != null) {
			getChunkHeader(buf, offset, data);
			offset += 6;
			id = data[0];
			size = data[1];
			if (id != 19789) {
				Logger.log("Not a valid 3DS file!", 0);
			} else {
				boolean unknownChunk = true;

				while ((id >= 0) && (offset < buf.length) && (offset >= 0)) {
					unknownChunk = true;
					getChunkHeader(buf, offset, data);
					offset += 6;

					id = data[0];
					size = data[1];

					if ((id >= 0) && (offset < buf.length)) {
						if (id == 16640) {
							if (crdCnt != 0) {
								objs.add(create3DSObject(oldName, vertices,
										vertexCnt, coords, crdCnt, matName,
										textureName, matTextureData, coordMat,
										matColor, matTrans, matCnt));
							}
							Logger.log("Processing object from 3DS-file: "
									+ name, 2);
							vertexCnt = 0;
							texCnt = 0;
							crdCnt = 0;
							unknownChunk = false;
						}

						if (id == 45072) {
							int c = 0;
							StringBuilder sBuf = new StringBuilder(40);
							do {
								c = getUnsignedByte(buf, offset);
								offset++;
								if (c > 0) {
									sBuf.append((char) (byte) c);
								}
								if (offset >= buf.length)
									c = -1;
							} while (c > 0);
							lastKeyframeName = sBuf.toString();
							Logger.log("Name in hierarchy found: "
									+ lastKeyframeName, 2);
							offset += 6;
							unknownChunk = false;
						}

						if (id == 45088) {
							offset += 20;

							float x = Float.intBitsToFloat(getInt(buf, offset));
							offset += 4;

							float y = Float.intBitsToFloat(getInt(buf, offset));
							offset += 4;

							float z = Float.intBitsToFloat(getInt(buf, offset));
							offset += 4;

							name2pivot.put(lastKeyframeName,
									SimpleVector.create(x, y, z));
							unknownChunk = false;
						}

						if (id == 16384) {
							int c = 0;
							nBuf = new StringBuilder(40);
							do {
								c = getUnsignedByte(buf, offset);
								offset++;
								if (c > 0) {
									nBuf.append((char) (byte) c);
								}
								if (offset >= buf.length)
									c = -1;
							} while (c > 0);
							oldName = name;
							name = nBuf.toString();
							unknownChunk = false;
						}

						if (id == 16656) {
							int anz = getShortInt(buf, offset);

							if ((vertices == null) || (vertices.length < anz)) {
								vertices = new float[anz][5];
							}

							offset += 2;
							for (int i = 0; i < anz; i++) {
								float x = Float.intBitsToFloat(getInt(buf,
										offset)) * scale;
								offset += 4;
								float y = Float.intBitsToFloat(getInt(buf,
										offset)) * scale;
								offset += 4;
								float z = Float.intBitsToFloat(getInt(buf,
										offset)) * scale;
								offset += 4;
								vertices[vertexCnt][0] = x;
								vertices[vertexCnt][1] = y;
								vertices[vertexCnt][2] = z;
								vertexCnt++;
							}
							unknownChunk = false;
						}

						if (id == 16672) {
							int offy = offset;

							int anz = getShortInt(buf, offy);

							if ((coords == null) || (coords.length < anz)) {
								coords = new int[anz][3];
								coordMat = new String[anz];
							}

							offy += 2;

							for (int i = 0; i < anz; i++) {
								int v1 = getShortInt(buf, offy);
								offy += 2;
								int v2 = getShortInt(buf, offy);
								offy += 2;
								int v3 = getShortInt(buf, offy);
								offy += 2;
								offy += 2;
								coords[crdCnt][0] = v1;
								coords[crdCnt][1] = v2;
								coords[crdCnt][2] = v3;
								crdCnt++;
							}

							while ((offy < offset + size - 6)
									&& (offy < buf.length)) {
								getChunkHeader(buf, offy, tmpData);
								offy += 6;
								int tmpId = tmpData[0];
								int tmpSize = tmpData[1];

								if (tmpId == 16688) {
									int c = 0;
									int offy3 = offy;
									nBuf = new StringBuilder(40);
									do {
										c = getUnsignedByte(buf, offy3);
										offy3++;
										if (c > 0) {
											nBuf.append((char) (byte) c);
										}
										if (offy3 >= buf.length)
											c = -1;
									} while (c > 0);

									int tmpCnt = getShortInt(buf, offy3);
									offy3 += 2;
									String n = nBuf.toString();
									for (c = 0; c < tmpCnt; c++) {
										int fc = getShortInt(buf, offy3);
										offy3 += 2;
										coordMat[fc] = n;
									}
								}

								offy += tmpSize - 6;
							}
							unknownChunk = true;
						}

						if (id == 16704) {
							int anz = getShortInt(buf, offset);
							offset += 2;

							for (int i = 0; i < anz; i++) {
								float u = Float.intBitsToFloat(getInt(buf,
										offset));
								offset += 4;
								float v = Float.intBitsToFloat(getInt(buf,
										offset));
								offset += 4;
								vertices[texCnt][3] = u;
								vertices[texCnt][4] = v;
								texCnt++;
							}
							unknownChunk = false;
						}

						if (id == 45055) {
							int offy = offset;

							while ((offy < offset + size - 6)
									&& (offy < buf.length)) {
								getChunkHeader(buf, offy, tmpData);
								offy += 6;
								int tmpId = tmpData[0];
								int tmpSize = tmpData[1];

								if (tmpId == 40960) {
									nBuf = new StringBuilder(40);
									int c = 0;
									int offy3 = offy;
									do {
										c = getUnsignedByte(buf, offy3);
										offy3++;
										if (c > 0) {
											nBuf.append((char) (byte) c);
										}
										if (offy3 >= buf.length)
											c = -1;
									} while (c > 0);
									matName[matCnt] = nBuf.toString();
									Logger.log("Processing new material "
											+ matName[matCnt] + "!", 2);
								}

								if (tmpId == 40992) {
									int offy2 = offy + 6;
									if ((tmpSize != 24)
											|| (Config.oldStyle3DSLoader)) {
										int r = getUnsignedByte(buf, offy2);
										offy2++;
										int g = getUnsignedByte(buf, offy2);
										offy2++;
										int b = getUnsignedByte(buf, offy2);
										offy2++;
										matColor[matCnt] = new RGBColor(r, g, b);
									} else {
										int r = (int) (Float
												.intBitsToFloat(getInt(buf,
														offy2)) * 255.0F);
										offy2 += 4;
										int g = (int) (Float
												.intBitsToFloat(getInt(buf,
														offy2)) * 255.0F);
										offy2 += 4;
										int b = (int) (Float
												.intBitsToFloat(getInt(buf,
														offy2)) * 255.0F);
										offy2 += 4;
										boolean bug = false;
										if (r < 0) {
											r = 0;
											bug = true;
										}
										if (g < 0) {
											g = 0;
											bug = true;
										}
										if (b < 0) {
											b = 0;
											bug = true;
										}
										if (r > 255) {
											r = 255;
											bug = true;
										}
										if (g > 255) {
											g = 255;
											bug = true;
										}
										if (b > 255) {
											b = 255;
											bug = true;
										}

										if (bug) {
											Logger.log(
													"Error reading material's diffuse color...try Config.oldStyle3DSLoader=true!",
													1);
										}

										matColor[matCnt] = new RGBColor(r, g, b);
									}
								}

								if (tmpId == 41040) {
									int offy2 = offy + 6;
									int t = 100 - getShortInt(buf, offy2);
									if (t < 0) {
										t = 0;
									}
									if (t > 100) {
										t = 100;
									}

									if (t == 100)
										t = -1;
									else {
										t /= 10;
									}
									matTrans[matCnt] = t;
								}

								if (tmpId == 41472) {
									int offy2 = offy;
									while ((offy2 < offy + size - 6)
											&& (offy2 < buf.length)) {
										getChunkHeader(buf, offy2, tmpData);
										offy2 += 6;
										int tmpId2 = tmpData[0];
										int tmpSize2 = tmpData[1];

										if (tmpId2 == 41728) {
											nBuf = new StringBuilder(40);
											int c = 0;
											int offy3 = offy2;
											do {
												c = getUnsignedByte(buf, offy3);
												offy3++;
												if (c > 0) {
													nBuf.append((char) (byte) c);
												}
												if (offy3 >= buf.length)
													c = -1;
											} while (c > 0);
											String n = nBuf.toString();
											if (!TextureCache.getInstance()
													.containsTexture(n)) {
												TextureCache.getInstance()
														.addTexture(n);
												Logger.log(
														"Texture named "
																+ n
																+ " added to TextureCache!",
														2);
											}
											textureName[matCnt] = n;
										}

										if (tmpId2 == 41812) {
											matTextureData[matCnt][0] = Float
													.intBitsToFloat(getInt(buf,
															offy2));
										}
										if (tmpId2 == 41814) {
											matTextureData[matCnt][1] = Float
													.intBitsToFloat(getInt(buf,
															offy2));
										}
										if (tmpId2 == 41816) {
											matTextureData[matCnt][2] = Float
													.intBitsToFloat(getInt(buf,
															offy2));
										}
										if (tmpId2 == 41818) {
											matTextureData[matCnt][3] = Float
													.intBitsToFloat(getInt(buf,
															offy2));
										}
										offy2 += tmpSize2 - 6;
									}
								}
								offy += tmpSize - 6;
							}

							matCnt++;

							if (matCnt >= matName.length) {
								int nl = matName.length + 10;
								String[] matNameT = new String[nl];
								String[] textureNameT = new String[nl];
								RGBColor[] matColorT = new RGBColor[nl];
								int[] matTransT = new int[nl];
								float[][] matTextureDataT = new float[nl][4];

								System.arraycopy(matName, 0, matNameT, 0,
										matCnt);
								System.arraycopy(textureName, 0, textureNameT,
										0, matCnt);
								System.arraycopy(matColor, 0, matColorT, 0,
										matCnt);
								System.arraycopy(matTrans, 0, matTransT, 0,
										matCnt);

								for (int i = 0; i < matTextureData.length; i++) {
									System.arraycopy(matTextureData[i], 0,
											matTextureDataT[i], 0, 4);
								}

								matName = matNameT;
								textureName = textureNameT;
								matColor = matColorT;
								matTrans = matTransT;
								matTextureData = matTextureDataT;

								for (int m = matCnt; m < matTextureData.length; m++) {
									matTextureData[m][0] = 1.0F;
									matTextureData[m][1] = 1.0F;
									matTextureData[m][2] = 0.0F;
									matTextureData[m][3] = 0.0F;
									matTrans[m] = 1000;
								}

							}

							unknownChunk = true;
						}

						if ((id == 15677) || (id == 45056) || (id == 45058)) {
							unknownChunk = false;
						}

						if (unknownChunk) {
							if (offset + size - 6 >= buf.length)
								offset = buf.length;
							else {
								offset += size - 6;
							}
						}
					}
				}
			}
		}

		if (vertexCnt != 0) {
			if (crdCnt != 0) {
				objs.add(create3DSObject(name, vertices, vertexCnt, coords,
						crdCnt, matName, textureName, matTextureData, coordMat,
						matColor, matTrans, matCnt));
			}
			vertexCnt = 0;
			texCnt = 0;
			crdCnt = 0;
		}

		if (Config.useRotationPivotFrom3DS) {
			for (int i = 0; i < objs.size(); i++) {
				T3DObject obj = (T3DObject) objs.get(i);
				String on = obj.getName();
				String ons = on;
				int p = on.indexOf("Rage3D");
				if (p != -1) {
					on = on.substring(0, p);
					SimpleVector pivot = (SimpleVector) name2pivot.get(on);
					if (pivot != null) {
						obj.setRotationPivot(pivot);
						obj.skipPivot = true;
						Logger.log("Setting rotation pivot of object " + ons
								+ " to " + pivot, 2);
					}
				}
			}
		}

		T3DObject[] res = new T3DObject[objs.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = ((T3DObject) objs.get(i));
		}
		return res;
	}

	private static T3DObject create3DSObject(String n, float[][] vertices,
			int vsize, int[][] coords, int psize, String[] matName,
			String[] textureName, float[][] matTextureData, String[] coordMat,
			RGBColor[] cols, int[] trans, int matCnt) {
		int size = psize;

		int[] posCache = (int[]) null;
		int[] lastPoints = (int[]) null;

		if (!optimize) {
			posCache = new int[vertices.length];
			lastPoints = new int[3];
			for (int i = 0; i < posCache.length; i++) {
				posCache[i] = -1;
			}
		}

		T3DObject obj = new T3DObject(size);
		if (!optimize) {
			obj.disableVertexSharing();
		}

		TextureCache texMan = TextureCache.getInstance();

		int defTexId = texMan.getTextureID("--dummy--");

		int trs = 100;
		int lastM = -1;
		String lastName = "**hurzigurzi**";

		for (int i = 0; i < psize; i++) {
			int c0 = coords[i][0];
			int c1 = coords[i][1];
			int c2 = coords[i][2];

			float x1 = vertices[c0][0];
			float y1 = vertices[c0][1];
			float z1 = vertices[c0][2];

			float x2 = vertices[c1][0];
			float y2 = vertices[c1][1];
			float z2 = vertices[c1][2];

			float x3 = vertices[c2][0];
			float y3 = vertices[c2][1];
			float z3 = vertices[c2][2];

			int texId = defTexId;
			float uMul = 1.0F;
			float vMul = 1.0F;
			float uOffset = 0.0F;
			float vOffset = 0.0F;

			if (coordMat[i] != null) {
				if (coordMat[i].equals(lastName)) {
					texId = texMan.getTextureID(textureName[lastM]);
					uMul = matTextureData[lastM][0];
					vMul = matTextureData[lastM][1];
					uOffset = matTextureData[lastM][2];
					vOffset = matTextureData[lastM][3];
				} else {
					for (int m = 0; m < matCnt; m++) {
						if ((matName[m] != null)
								&& (matName[m].equals(coordMat[i]))) {
							if ((trans[m] != 1000) && (trs != -1)) {
								trs = trans[m];
							} else {
								trs = -1;
							}

							if (textureName[m] != null) {
								lastName = matName[m];
								lastM = m;
								texId = texMan.getTextureID(textureName[m]);
								uMul = matTextureData[m][0];
								vMul = matTextureData[m][1];
								uOffset = matTextureData[m][2];
								vOffset = matTextureData[m][3];
								break;
							}
							String colName = "__3ds-Color:" + cols[m].getRed()
									+ "/" + cols[m].getGreen() + "/"
									+ cols[m].getBlue();
							if (texMan.containsTexture(colName)) {
								texId = texMan.getTextureID(colName);
								break;
							}
							Texture tex = Texture
									.createSingleColoredTexture(cols[m]);
							texMan.addTexture(colName, tex);
							texId = texMan.getTextureID(colName);

							textureName[m] = colName;
							lastM = m;
							lastName = matName[m];

							break;
						}
					}
				}
			}

			float u1 = vertices[c0][3] * uMul + uOffset;
			float v1 = vertices[c0][4] * vMul + vOffset;
			float u2 = vertices[c1][3] * uMul + uOffset;
			float v2 = vertices[c1][4] * vMul + vOffset;
			float u3 = vertices[c2][3] * uMul + uOffset;
			float v3 = vertices[c2][4] * vMul + vOffset;

			if (optimize) {
				obj.addTriangle(x1, y1, z1, u1, 1.0F - v1, x2, y2, z2, u2,
						1.0F - v2, x3, y3, z3, u3, 1.0F - v3, texId);
			} else {
				lastPoints[0] = posCache[c0];
				lastPoints[1] = posCache[c1];
				lastPoints[2] = posCache[c2];
				obj.addTriangle(x1, y1, z1, u1, 1.0F - v1, x2, y2, z2, u2,
						1.0F - v2, x3, y3, z3, u3, 1.0F - v3, texId, 0, false,
						lastPoints);
				if (posCache[c0] == -1) {
					posCache[c0] = lastPoints[0];
				}
				if (posCache[c1] == -1) {
					posCache[c1] = lastPoints[1];
				}
				if (posCache[c2] == -1) {
					posCache[c2] = lastPoints[2];
				}
			}
		}

		obj.setName(n + "Rage3D" + obj.getID());
		obj.getMesh().compress();

		if ((trs != 1000) && (trs != -1)) {
			obj.setTransparency(trs);
		}

		Logger.log("Object '" + obj.name + "' created using "
				+ obj.getMesh().anzTri + " polygons and "
				+ obj.getMesh().anzCoords + " vertices.", 2);
		return obj;
	}

	private static void getChunkHeader(byte[] b, int offset, int[] data) {
		data[0] = getShortInt(b, offset);
		data[1] = getInt(b, offset + 2);
	}
}
