package toto.graphics._3D;

import java.io.Serializable;

public final class Animation implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int LINEAR = 0;
	public static final int KEYFRAMESONLY = 4;
	public static final int USE_WRAPPING = 0;
	public static final int USE_CLAMPING = 1;
	int aktFrames;
	Mesh[] keyFrames;
	int endFrame;
	int maxFrames;
	int mode;
	int[] startFrames;
	int[] endFrames;
	String[] seqNames;
	int anzAnim;
	int wrapMode;
	private transient int lastFrame = -1;

	boolean cacheIndices = true;

	boolean firstCall = true;

	boolean realFirstCall = true;

	public Animation(int keyframeCount) {
		this.maxFrames = keyframeCount;
		this.keyFrames = new Mesh[keyframeCount];
		this.endFrame = 0;
		this.aktFrames = 0;
		this.mode = 0;
		this.anzAnim = 0;
		this.wrapMode = 0;
		this.startFrames = new int[Config.maxAnimationSubSequences];
		this.endFrames = new int[Config.maxAnimationSubSequences];
		this.seqNames = new String[Config.maxAnimationSubSequences];
		for (int i = 0; i < Config.maxAnimationSubSequences; i++) {
			this.startFrames[i] = -1;
			this.endFrames[i] = -1;
			this.seqNames[i] = null;
		}
	}

	public void setCaching(boolean usesCache) {
		this.cacheIndices = usesCache;
	}

	public void strip() {
		for (int i = 0; i < this.aktFrames; i++)
			this.keyFrames[i].strip();
	}

	public void remove(int seq) {
		int[] poss = getSequenceBorders(seq);
		for (int i = poss[0]; i < poss[1]; i++)
			this.keyFrames[i] = null;
	}

	public String getName(int seq) {
		return this.seqNames[seq];
	}

	public int[] getSequenceBorders(int seq) {
		return new int[] { this.startFrames[seq], this.endFrames[seq] };
	}

	public int getSequence(String name) {
		int end = this.seqNames.length;
		for (int i = 0; i < end; i++) {
			if (name.equalsIgnoreCase(this.seqNames[i])) {
				return i;
			}
		}
		return -1;
	}

	public Mesh[] getKeyFrames() {
		Mesh[] ret = new Mesh[this.aktFrames];
		System.arraycopy(this.keyFrames, 0, ret, 0, this.aktFrames);
		return ret;
	}

	public int createSubSequence(String name) {
		if (this.anzAnim + 1 < Config.maxAnimationSubSequences) {
			this.anzAnim += 1;
			this.startFrames[this.anzAnim] = this.aktFrames;
			this.seqNames[this.anzAnim] = name;
			this.startFrames[0] = 0;
			this.endFrames[0] = 0;
			this.seqNames[0] = "complete";
		} else {
			Logger.log(
					"Too many sub-sequences defined. Modify configuration to allow a higher number.",
					0);
		}
		return this.anzAnim;
	}

	public int getSequenceCount() {
		return this.anzAnim;
	}

	public void setClampingMode(int mode) {
		if ((mode != 0) && (mode != 1))
			Logger.log("Clamping-mode not supported!", 0);
		else
			this.wrapMode = mode;
	}

	public void addKeyFrame(Mesh keyFrame) {
		if (this.anzAnim == 0) {
			Logger.log(
					"Can't add a keyframe without a sub-sequence being created!",
					0);
		} else if (keyFrame.obbStart != 0) {
			if (this.aktFrames < this.maxFrames) {
				this.keyFrames[this.aktFrames] = keyFrame;
				this.aktFrames += 1;
				this.endFrames[this.anzAnim] = this.aktFrames;
				this.endFrame = this.aktFrames;
			} else {
				Logger.log("Too many keyframes defined!", 0);
			}
		} else
			Logger.log("Bounding box missing in this mesh!", 0);

		this.endFrames[0] = this.endFrame;
	}

	public void setInterpolationMethod(int method) {
		this.mode = method;
	}

	void rotateMesh(Matrix rot, float xRot, float yRot, float zRot, float scale) {
		for (int i = 0; i < this.aktFrames; i++)
			if (this.keyFrames[i] != null)
				this.keyFrames[i].rotateMesh(rot, xRot, yRot, zRot, scale);
	}

	void translateMesh(Matrix translationMatrix, Matrix originMatrix) {
		for (int i = 0; i < this.aktFrames; i++)
			if (this.keyFrames[i] != null)
				this.keyFrames[i]
						.translateMesh(translationMatrix, originMatrix);
	}

	void doAnimation(T3DObject obj, int seq, float index) {
		this.realFirstCall = false;

		if ((this.firstCall) && (obj.isVisible)) {
			if (index == 0.0F) {
				index = 1.0E-004F;
			} else if (index == 1.0F) {
				index = 0.99999F;
			}

			this.firstCall = false;
		}

		if (seq <= this.anzAnim) {
			if (index > 1.0F) {
				index = 1.0F;
			} else if (index < 0.0F) {
				index = 0.0F;
			}

			int endFrame = this.endFrames[seq];
			int startFrame = this.startFrames[seq];

			float frame = index * (endFrame - startFrame - this.wrapMode)
					+ startFrame;

			if (this.wrapMode == 1) {
				if (frame >= endFrame) {
					frame = endFrame - 1;
				} else if (frame < startFrame) {
					frame = startFrame;
				}

			}

			int iFrame = (int) frame;
			float inter = frame - iFrame;

			switch (this.mode) {
			case 0:
				interpolateLinear(obj, iFrame, inter, startFrame, endFrame);
				break;
			case 4:
				interpolateNone(obj, iFrame, startFrame, endFrame);
				break;
			case 1:
			case 2:
			case 3:
			default:
				Logger.log("Unsupported interpolation mode used!", 0);
				break;
			}
		} else {
			Logger.log("Sub-sequence number " + seq + " doesn't exist!", 0);
		}
	}

	void interpolateLinear(T3DObject obj, int frame, float inter,
			int startFrame, int endFrame) {
		float first = 1.0F - inter;
		float second = inter;

		int sF = frame;
		int eF = frame + 1;
		if (this.wrapMode == 1) {
			if (eF >= endFrame) {
				eF = endFrame - 1;
			} else if (eF < startFrame) {
				eF = startFrame;
			}

			if (sF >= endFrame) {
				sF = endFrame - 1;
			} else if (sF < startFrame) {
				sF = startFrame;
			}
		} else {
			if (eF >= endFrame) {
				eF = startFrame;
			} else if (eF < startFrame) {
				eF = endFrame - 1;
			}

			if (sF >= endFrame) {
				sF = startFrame;
			} else if (sF < startFrame) {
				sF = endFrame - 1;
			}

		}

		Mesh objobjMesh = obj.objMesh;
		int end = objobjMesh.anzCoords;
		Mesh[] keyFrames = this.keyFrames;

		float[] objobjMeshxOrg = objobjMesh.xOrg;
		float[] objobjMeshyOrg = objobjMesh.yOrg;
		float[] objobjMeshzOrg = objobjMesh.zOrg;

		float[] keyFramesxOrg = keyFrames[sF].xOrg;
		float[] keyFramesyOrg = keyFrames[sF].yOrg;
		float[] keyFrameszOrg = keyFrames[sF].zOrg;

		float[] objobjMeshnxOrg = objobjMesh.nxOrg;
		float[] objobjMeshnyOrg = objobjMesh.nyOrg;
		float[] objobjMeshnzOrg = objobjMesh.nzOrg;

		float[] keyFramesnxOrg = keyFrames[sF].nxOrg;
		float[] keyFramesnyOrg = keyFrames[sF].nyOrg;
		float[] keyFramesnzOrg = keyFrames[sF].nzOrg;

		float[] keyFrameseFxOrg = keyFrames[eF].xOrg;
		float[] keyFrameseFyOrg = keyFrames[eF].yOrg;
		float[] keyFrameseFzOrg = keyFrames[eF].zOrg;

		float[] keyFrameseFnxOrg = keyFrames[eF].nxOrg;
		float[] keyFrameseFnyOrg = keyFrames[eF].nyOrg;
		float[] keyFrameseFnzOrg = keyFrames[eF].nzOrg;

		for (int i = 0; i < end; i++) {
			objobjMeshxOrg[i] = (keyFramesxOrg[i] * first + keyFrameseFxOrg[i]
					* second);
			objobjMeshyOrg[i] = (keyFramesyOrg[i] * first + keyFrameseFyOrg[i]
					* second);
			objobjMeshzOrg[i] = (keyFrameszOrg[i] * first + keyFrameseFzOrg[i]
					* second);

			objobjMeshnxOrg[i] = (keyFramesnxOrg[i] * first + keyFrameseFnxOrg[i]
					* second);
			objobjMeshnyOrg[i] = (keyFramesnyOrg[i] * first + keyFrameseFnyOrg[i]
					* second);
			objobjMeshnzOrg[i] = (keyFramesnzOrg[i] * first + keyFrameseFnzOrg[i]
					* second);
		}
		obj.modified = true;
	}

	void interpolateNone(T3DObject obj, int frame, int startFrame,
			int endFrame) {
		int sF = frame;
		if (this.wrapMode == 1) {
			if (sF >= endFrame) {
				sF = endFrame - 1;
			} else if (sF < startFrame) {
				sF = startFrame;
			}

		} else if (sF >= endFrame) {
			sF = startFrame;
		} else if (sF < startFrame) {
			sF = endFrame - 1;
		}

		int end = obj.objMesh.anzCoords;

		float[] objobjMeshxOrg = obj.objMesh.xOrg;
		float[] objobjMeshyOrg = obj.objMesh.yOrg;
		float[] objobjMeshzOrg = obj.objMesh.zOrg;

		float[] keyFramesxOrg = this.keyFrames[sF].xOrg;
		float[] keyFramesyOrg = this.keyFrames[sF].yOrg;
		float[] keyFrameszOrg = this.keyFrames[sF].zOrg;

		float[] objobjMeshnxOrg = obj.objMesh.nxOrg;
		float[] objobjMeshnyOrg = obj.objMesh.nyOrg;
		float[] objobjMeshnzOrg = obj.objMesh.nzOrg;

		float[] keyFramesnxOrg = this.keyFrames[sF].nxOrg;
		float[] keyFramesnyOrg = this.keyFrames[sF].nyOrg;
		float[] keyFramesnzOrg = this.keyFrames[sF].nzOrg;

		for (int i = 0; i < end; i++) {
			objobjMeshxOrg[i] = keyFramesxOrg[i];
			objobjMeshyOrg[i] = keyFramesyOrg[i];
			objobjMeshzOrg[i] = keyFrameszOrg[i];

			objobjMeshnxOrg[i] = keyFramesnxOrg[i];
			objobjMeshnyOrg[i] = keyFramesnyOrg[i];
			objobjMeshnzOrg[i] = keyFramesnzOrg[i];
		}

		if (sF != this.lastFrame) {
			obj.modified = true;
			this.lastFrame = sF;
		}
	}

	void validate(T3DObject obj) {
		Mesh objMesh = obj.getMesh();
		if (objMesh == null) {
			return;
		}
		for (int i = 0; i < this.aktFrames; i++) {
			Mesh mesh = this.keyFrames[i];
			if ((mesh != null) && (mesh == objMesh)) {
				Logger.log(
						"The animation shares a mesh with the animated object...fixing this...!",
						1);
				this.keyFrames[i] = mesh.cloneMesh(true);
			}
		}
	}
}
