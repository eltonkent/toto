package toto.graphics._3D;

public abstract interface IPostProcessor {
	public abstract void init(FrameBuffer paramFrameBuffer);

	public abstract void process();

	public abstract void dispose();

	public abstract boolean isInitialized();
}
