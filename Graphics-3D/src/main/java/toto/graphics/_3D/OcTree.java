package toto.graphics._3D;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/*     */
/*     */
/*     */

final class OcTree implements Serializable {
	private static final long serialVersionUID = 1L;
	static final int MODE_NORMAL = 0;
	static final int MODE_OPTIMIZED = 1;
	static final boolean COLLISION_USE = true;
	static final boolean COLLISION_DONT_USE = false;
	static final boolean RENDERING_USE = true;
	static final boolean RENDERING_DONT_USE = false;
	private int curLeafs = 0;

	private OcTreeNode[] leafList = null;

	private boolean[] visibleLeafs = null;
	private float radiusMul;
	private Mesh objMesh = null;

	private OcTreeNode[] threadsBuffer = null;

	private ArrayList<OcTreeNode> allLeafs = null;

	private HashSet<Integer> used = new HashSet();

	private int[] leafCount = new int[1];

	private Object[] objArray = new Object[2];

	int leafs = 0;

	int nodes = 0;

	int[] tris = null;

	int maxPoly = 0;

	int maxDepth = -1;

	int totalPolys = 0;

	boolean useForCollision = true;

	boolean useForRendering = true;
	int mode;
	OcTreeNode root = null;

	OcTree(Mesh mesh, int maxPoly, int mode) {
		initOcTree(mesh, maxPoly, -1, mode);
	}

	OcTree(Mesh mesh, int maxPoly, int maxDepth, int mode) {
		initOcTree(mesh, maxPoly, maxDepth, mode);
	}

	OcTree(T3DObject obj, int maxPoly, int mode) {
		initOcTree(obj.getMesh(), maxPoly, -1, mode);
	}

	OcTree(T3DObject obj, int maxPoly, int maxDepth, int mode) {
		initOcTree(obj.getMesh(), maxPoly, maxDepth, mode);
	}

	OcTree() {
	}

	void postConstruct() {
		this.leafList = new OcTreeNode[this.leafs];
		this.visibleLeafs = new boolean[this.leafs];
		this.radiusMul = 1.5F;
	}

	private void initOcTree(Mesh mesh, int maxPoly, int maxDepth, int mode) {
		this.leafs = 0;
		this.nodes = 0;
		this.maxDepth = maxDepth;
		this.maxPoly = maxPoly;
		this.tris = new int[maxPoly + 1];
		this.objMesh = mesh;
		buildTree();
		this.used = null;
		this.tris = null;
		this.mode = mode;
		this.useForCollision = false;
		postConstruct();
	}

	ArrayList<OcTreeNode> getFilledLeafs() {
		if (this.allLeafs == null) {
			this.allLeafs = new ArrayList();
			fillLeafs(this.root);
		}
		return this.allLeafs;
	}

	private void fillLeafs(OcTreeNode n) {
		if (n.getPolyCount() > 0) {
			this.allLeafs.add(n);
		}
		for (int i = 0; i < n.getChildCount(); i++)
			fillLeafs(n.getChildren()[i]);
	}

	void setCollisionUse(boolean useIt)
	/*     */{
		/* 244 */
		this.useForCollision = useIt;
		/*     */
	}

	void setRenderingUse(boolean useIt)
	/*     */{
		/* 258 */
		this.useForRendering = useIt;
		/*     */
	}

	boolean getCollisionUse()
	/*     */{
		/* 269 */
		return this.useForCollision;
		/*     */
	}

	boolean getRenderingUse()
	/*     */{
		/* 280 */
		return this.useForRendering;
		/*     */
	}

	void setRadiusMultiplier(float mul) {
		if (mul > 0.0F)
			this.radiusMul = mul;
	}

	float getRadiusMultiplier()
	/*     */{
		/* 307 */
		return this.radiusMul;
		/*     */
	}

	boolean isOfOrderZero() {
		return this.root.isLeaf();
	}

	List<OcTreeNode> getAffectedLeafs(float bx, float by, float bz, float radius) {
		Object[] res = getColliderLeafs(bx, by, bz, radius);
		List ret = new ArrayList();
		if (res != null) {
			Integer cnt = (Integer) res[0];
			for (int i = 0; i < cnt.intValue(); i++) {
				ret.add(((OcTreeNode[]) res[1])[i]);
			}
		}
		return ret;
	}

	int getVisibleLeafs(Matrix transform, float divX, float divY) {
		this.leafCount[0] = 0;
		this.curLeafs = 0;
		getVisibleLeafs(this.root, transform, divX, divY, this.leafCount);
		this.curLeafs = this.leafCount[0];
		return this.curLeafs;
	}

	Object[] getColliderLeafs(float bx, float by, float bz, float radius) {
		this.leafCount[0] = 0;
		return getColliderLeafs(this.root, bx, by, bz, radius, this.leafCount,
				null);
	}

	int getLeafCount() {
		/* 360 */
		return this.curLeafs;
		/*     */
	}

	int getTotalLeafs() {
		/* 364 */
		return this.leafs;
		/*     */
	}

	int getTotalPolyCount() {
		/* 368 */
		return this.totalPolys;
		/*     */
	}

	boolean isCompletelyVisible(int leafNum) {
		return this.visibleLeafs[leafNum];
	}

	OcTreeNode[] getLeafList() {
		/* 376 */
		return this.leafList;
		/*     */
	}

	private Object[] getColliderLeafs(OcTreeNode node, float bx, float by,
			float bz, float radius, int[] leafCnt, OcTreeNode[] colLeafList) {
		boolean intersects = false;
		if ((node.getChildCount() != 0) || (node.getPolyCount() != 0)) {
			intersects = node.sphereIntersectsNode(bx, by, bz, radius);
		}

		if (intersects) {
			Thread defaultThread = T3DScene.defaultThread;

			if (defaultThread == null) {
				T3DScene.setDefaultThread(Thread.currentThread());
				defaultThread = T3DScene.defaultThread;
			}

			if (colLeafList == null) {
				Thread trd = Thread.currentThread();
				if ((defaultThread == null)
						|| (trd != defaultThread)
						|| ((defaultThread == trd) && (this.threadsBuffer == null))) {
					colLeafList = new OcTreeNode[this.leafs];
					if ((defaultThread != null) && (defaultThread == trd)) {
						this.threadsBuffer = colLeafList;
					}
				} else if (this.threadsBuffer != null) {
					colLeafList = this.threadsBuffer;
				}

			}

			if ((node.getPolyCount() != 0) && (node.getChildCount() == 0)) {
				colLeafList[leafCnt[0]] = node;
				leafCnt[0] += 1;
			} else if (node.getChildCount() != 0) {
				int end = node.getChildCount();
				OcTreeNode[] children = node.getChildren();
				for (int i = 0; i < end; i++) {
					getColliderLeafs(children[i], bx, by, bz, radius, leafCnt,
							colLeafList);
				}
			}

		}

		this.objArray[0] = IntegerC.valueOf(leafCnt[0]);
		this.objArray[1] = colLeafList;

		return this.objArray;
	}

	private void markAllLeafsAsVisible(OcTreeNode node, int[] leafCnt) {
		if ((node.getPolyCount() != 0) && (node.getChildCount() == 0)) {
			this.leafList[leafCnt[0]] = node;
			this.visibleLeafs[leafCnt[0]] = true;
			leafCnt[0] += 1;
		} else {
			OcTreeNode[] children = node.getChildren();
			int end = node.getChildCount();
			for (int i = 0; i < end; i++)
				markAllLeafsAsVisible(children[i], leafCnt);
		}
	}

	private void getVisibleLeafs(OcTreeNode node, Matrix transform, float divX,
			float divY, int[] leafCnt) {
		boolean visible = false;
		boolean visComplete = false;

		int childCount = node.getChildCount();

		if ((childCount != 0) || (node.getPolyCount() != 0)) {
			int vis = node.isVisible(transform, divX, divY);
			if (vis == 999) {
				visComplete = true;
				if (childCount != 0) {
					markAllLeafsAsVisible(node, leafCnt);
					vis = 0;
				} else {
					vis = 1;
				}
			}
			visible = vis == 1;
		}

		if ((visible) && (node.getPolyCount() != 0) && (childCount == 0)) {
			this.leafList[leafCnt[0]] = node;
			this.visibleLeafs[leafCnt[0]] = visComplete;
			leafCnt[0] += 1;
		} else if ((visible) && (childCount != 0)) {
			OcTreeNode[] tmpChilds = node.getChildren();
			int end = childCount;
			for (int i = 0; i < end; i++)
				getVisibleLeafs(tmpChilds[i], transform, divX, divY, leafCnt);
		}
	}

	private boolean createChildren(OcTreeNode of, int level) {
		this.nodes += 1;
		int triCnt = 0;
		level++;

		if (of != null) {
			for (int i = 0; i < this.objMesh.anzTri; i++) {
				int p0 = this.objMesh.coords[this.objMesh.points[i][0]];
				if (!this.used.contains(Integer
						.valueOf(this.objMesh.points[i][0]))) {
					int p1 = this.objMesh.coords[this.objMesh.points[i][1]];
					int p2 = this.objMesh.coords[this.objMesh.points[i][2]];

					float x1 = this.objMesh.xOrg[p0];
					float y1 = this.objMesh.yOrg[p0];
					float z1 = this.objMesh.zOrg[p0];

					float x2 = this.objMesh.xOrg[p1];
					float y2 = this.objMesh.yOrg[p1];
					float z2 = this.objMesh.zOrg[p1];

					float x3 = this.objMesh.xOrg[p2];
					float y3 = this.objMesh.yOrg[p2];
					float z3 = this.objMesh.zOrg[p2];

					if (this.tris.length < triCnt + 1) {
						int[] tmpTris = new int[this.tris.length * 2];
						System.arraycopy(this.tris, 0, tmpTris, 0,
								this.tris.length);
						this.tris = tmpTris;
					}

					if (of.completeFit(x1, y1, z1, x2, y2, z2, x3, y3, z3)) {
						this.tris[triCnt] = i;
						triCnt++;
					} else if (of
							.partialFit(x1, y1, z1, x2, y2, z2, x3, y3, z3)) {
						this.tris[triCnt] = i;
						triCnt++;
					}

					if ((triCnt > this.maxPoly) && (level != this.maxDepth + 1)) {
						break;
					}
				}
			}

			if ((triCnt <= this.maxPoly) || (level == this.maxDepth + 1)) {
				if (triCnt != 0) {
					for (int ii = 0; ii < triCnt; ii++) {
						int i = this.tris[ii];
						int p0 = this.objMesh.coords[this.objMesh.points[i][0]];
						int p1 = this.objMesh.coords[this.objMesh.points[i][1]];
						int p2 = this.objMesh.coords[this.objMesh.points[i][2]];

						float x1 = this.objMesh.xOrg[p0];
						float y1 = this.objMesh.yOrg[p0];
						float z1 = this.objMesh.zOrg[p0];

						float x2 = this.objMesh.xOrg[p1];
						float y2 = this.objMesh.yOrg[p1];
						float z2 = this.objMesh.zOrg[p1];

						float x3 = this.objMesh.xOrg[p2];
						float y3 = this.objMesh.yOrg[p2];
						float z3 = this.objMesh.zOrg[p2];

						if (of.partialFit(x1, y1, z1, x2, y2, z2, x3, y3, z3)) {
							of.extendDimensions(x1, y1, z1, x2, y2, z2, x3, y3,
									z3);
						}
						of.addTriangle(triCnt, i, p0, p1, p2);

						this.used.add(Integer
								.valueOf(this.objMesh.points[i][0]));
						this.used.add(Integer
								.valueOf(this.objMesh.points[i][1]));
						this.used.add(Integer
								.valueOf(this.objMesh.points[i][2]));
					}

					if (this.mode == 1) {
						of.packPoints();
					}

					this.totalPolys += of.getPolyCount();
					this.leafs += 1;
				}
			} else {
				float xLow = of.xLow;
				float yLow = of.yLow;
				float zLow = of.zLow;

				float xHigh = of.xHigh;
				float yHigh = of.yHigh;
				float zHigh = of.zHigh;

				float xHalf = (xHigh - xLow) / 2.0F + xLow;
				float yHalf = (yHigh - yLow) / 2.0F + yLow;
				float zHalf = (zHigh - zLow) / 2.0F + zLow;

				OcTreeNode upperLeftFront = new OcTreeNode();
				OcTreeNode upperLeftBack = new OcTreeNode();

				OcTreeNode upperRightFront = new OcTreeNode();
				OcTreeNode upperRightBack = new OcTreeNode();

				OcTreeNode lowerLeftFront = new OcTreeNode();
				OcTreeNode lowerLeftBack = new OcTreeNode();

				OcTreeNode lowerRightFront = new OcTreeNode();
				OcTreeNode lowerRightBack = new OcTreeNode();

				upperLeftFront.setDimensions(xLow, yHalf, zLow, xHalf, yHigh,
						zHalf);
				upperLeftBack.setDimensions(xLow, yHalf, zHalf, xHalf, yHigh,
						zHigh);

				upperRightFront.setDimensions(xHalf, yHalf, zLow, xHigh, yHigh,
						zHalf);
				upperRightBack.setDimensions(xHalf, yHalf, zHalf, xHigh, yHigh,
						zHigh);

				lowerLeftFront.setDimensions(xLow, yLow, zLow, xHalf, yHalf,
						zHalf);
				lowerLeftBack.setDimensions(xLow, yLow, zHalf, xHalf, yHalf,
						zHigh);

				lowerRightFront.setDimensions(xHalf, yLow, zLow, xHigh, yHalf,
						zHalf);
				lowerRightBack.setDimensions(xHalf, yLow, zHalf, xHigh, yHalf,
						zHigh);

				of.addChild(upperLeftFront);
				boolean hasC = createChildren(upperLeftFront, level);
				if ((!hasC) && (upperLeftFront.getChildCount() == 0)) {
					of.removeChild(upperLeftFront);
					this.nodes -= 1;
				}

				of.addChild(upperRightFront);
				hasC = createChildren(upperRightFront, level);
				if ((!hasC) && (upperRightFront.getChildCount() == 0)) {
					of.removeChild(upperRightFront);
					this.nodes -= 1;
				}

				of.addChild(upperLeftBack);
				hasC = createChildren(upperLeftBack, level);
				if ((!hasC) && (upperLeftBack.getChildCount() == 0)) {
					of.removeChild(upperLeftBack);
					this.nodes -= 1;
				}

				of.addChild(upperRightBack);
				hasC = createChildren(upperRightBack, level);
				if ((!hasC) && (upperRightBack.getChildCount() == 0)) {
					of.removeChild(upperRightBack);
					this.nodes -= 1;
				}

				of.addChild(lowerLeftFront);
				hasC = createChildren(lowerLeftFront, level);
				if ((!hasC) && (lowerLeftFront.getChildCount() == 0)) {
					of.removeChild(lowerLeftFront);
					this.nodes -= 1;
				}

				of.addChild(lowerRightFront);
				hasC = createChildren(lowerRightFront, level);
				if ((!hasC) && (lowerRightFront.getChildCount() == 0)) {
					of.removeChild(lowerRightFront);
					this.nodes -= 1;
				}

				of.addChild(lowerLeftBack);
				hasC = createChildren(lowerLeftBack, level);
				if ((!hasC) && (lowerLeftBack.getChildCount() == 0)) {
					of.removeChild(lowerLeftBack);
					this.nodes -= 1;
				}

				of.addChild(lowerRightBack);
				hasC = createChildren(lowerRightBack, level);
				if ((!hasC) && (lowerRightBack.getChildCount() == 0)) {
					of.removeChild(lowerRightBack);
					this.nodes -= 1;
				}
			}

			return of.getPolyCount() != 0;
		}
		return false;
	}

	private void buildTree() {
		OcTreeNode.resetNodeID();
		this.root = new OcTreeNode();

		Logger.log(
				"Building octree for " + this.objMesh.anzTri + " triangles!", 2);

		float[] mins = this.objMesh.calcBoundingBox();

		this.root.setDimensions(mins[0], mins[2], mins[4], mins[1], mins[3],
				mins[5]);
		createChildren(this.root, 0);

		Logger.log("Octree constructed with " + this.nodes + " nodes / "
				+ this.leafs + " leafs.", 2);
	}
}
