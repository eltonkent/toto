package toto.graphics._3D;

public class CollisionEvent {
	private static final long serialVersionUID = 1L;
	public static final int TYPE_TARGET = 0;
	public static final int TYPE_SOURCE = 1;
	public static final int ALGORITHM_RAY = 0;
	public static final int ALGORITHM_SPHERE = 1;
	public static final int ALGORITHM_ELLIPSOID = 2;
	private T3DObject obj = null;

	private T3DObject source = null;

	private int type = 0;

	private int algorithm = 0;

	private int[] ids = null;

	private T3DObject[] targets = null;

	private SimpleVector contact = null;

	private static final String[] TYPES = { "target", "source" };

	private static final String[] ALGOS = { "ray-polygon", "sphere-polygon",
			"ellipsoid-polygon" };

	CollisionEvent(T3DObject obj, T3DObject source, int type,
			int algorithm, T3DObject[] targets, SimpleVector contact) {
		this.obj = obj;
		this.type = type;
		this.algorithm = algorithm;
		this.source = source;
		this.targets = targets;
		this.contact = contact;
	}

	public T3DObject getObject()
	/*     */{
		/* 76 */
		return this.obj;
		/*     */
	}

	public T3DObject[] getTargets()
	/*     */{
		/* 87 */
		return this.targets;
		/*     */
	}

	public T3DObject getSource() {
		if (this.type == 1) {
			return this.obj;
		}
		return this.source;
	}

	public int getType()
	/*     */{
		/* 115 */
		return this.type;
		/*     */
	}

	public int getAlgorithm()
	/*     */{
		/* 128 */
		return this.algorithm;
		/*     */
	}

	public int[] getPolygonIDs() {
		if (this.type == 1) {
			return null;
		}
		return this.ids;
	}

	public SimpleVector getFirstContact() {
		if (this.type == 1) {
			return null;
		}
		return this.contact;
	}

	public String toString() {
		return "Object: " + this.obj.getName() + "/" + TYPES[this.type] + "/"
				+ ALGOS[this.algorithm];
	}

	void setPolygonIDs(int[] numbers, int cnt) {
		if (numbers != null) {
			this.ids = new int[cnt];
			System.arraycopy(numbers, 0, this.ids, 0, cnt);
		}
	}
}
