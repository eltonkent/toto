package toto.graphics._3D;

class PolygonManager {
	private float[] u0 = new float[3];
	private float[] v0 = new float[3];

	T3DObject myObj = null;

	PolygonManager(T3DObject obj) {
		this.myObj = obj;
	}

	int getPolygonTexture(int polyID) {
		if (outOfBounds(polyID)) {
			return -1;
		}
		return this.myObj.texture[polyID];
	}

	int[] getPolygonTextures(int polyID) {
		if (outOfBounds(polyID)) {
			return null;
		}
		int add = 0;
		if (this.myObj.multiTex != null) {
			add = this.myObj.multiTex.length;
		}

		int[] res = new int[1 + add];
		res[0] = this.myObj.texture[polyID];
		if (this.myObj.multiTex != null) {
			for (int i = 0; i < this.myObj.multiTex.length; i++) {
				res[(1 + i)] = this.myObj.multiTex[i][polyID];
			}
		}

		return res;
	}

	void setPolygonTexture(int polyID, int textureID) {
		if (this.myObj.hasBeenStripped) {
			return;
		}

		if (outOfBounds(polyID)) {
			return;
		}
		this.myObj.texture[polyID] = textureID;
	}

	void addTexture(int polyID, int tid, int mode) {
		if (this.myObj.hasBeenStripped) {
			return;
		}

		if (this.myObj.multiTex == null) {
			if (Logger.isDebugEnabled()) {
				Logger.log("Creating texture arrays of size "
						+ (Config.maxTextureLayers - 1), 3);
			}
			this.myObj.multiTex = new int[Config.maxTextureLayers - 1][this.myObj.texture.length];
			this.myObj.multiMode = new int[Config.maxTextureLayers - 1][this.myObj.texture.length];
			for (int i = 0; i < this.myObj.texture.length; i++) {
				for (int ii = 0; ii < Config.maxTextureLayers - 1; ii++) {
					this.myObj.multiTex[ii][i] = -1;
				}
			}
			this.myObj.objVectors.createMultiCoords();
		}

		boolean added = false;
		for (int i = 0; i < this.myObj.multiTex.length; i++) {
			if (this.myObj.multiTex[i][polyID] == -1) {
				if (Logger.isDebugEnabled()) {
					Logger.log("Found empty stage at " + i, 3);
				}
				this.myObj.multiTex[i][polyID] = tid;
				this.myObj.multiMode[i][polyID] = mode;
				this.myObj.maxStagesUsed = Math.max(this.myObj.maxStagesUsed,
						i + 2);
				added = true;
				break;
			}
			if (Logger.isDebugEnabled()) {
				Logger.log("Stage " + i + " not empty: "
						+ this.myObj.multiTex[i][polyID], 3);
			}

		}

		if ((!added)
				&& (this.myObj.multiTex.length < Config.maxTextureLayers - 1)) {
			Logger.log("Expanding texture arrays ("
					+ this.myObj.multiTex.length + "->"
					+ (this.myObj.multiTex.length + 1) + ")...");
			int[][] mt = new int[this.myObj.multiTex.length + 1][this.myObj.texture.length];
			int[][] mm = new int[this.myObj.multiMode.length + 1][this.myObj.texture.length];
			float[][] mu = new float[this.myObj.objVectors.uMul.length + 1][this.myObj.objVectors.maxVectors];
			float[][] mv = new float[this.myObj.objVectors.vMul.length + 1][this.myObj.objVectors.maxVectors];
			for (int i = 0; i < this.myObj.multiTex.length; i++) {
				System.arraycopy(this.myObj.multiTex[i], 0, mt[i], 0,
						mt[i].length);
				System.arraycopy(this.myObj.multiMode[i], 0, mm[i], 0,
						mm[i].length);
			}
			for (int i = 0; i < this.myObj.objVectors.uMul.length; i++) {
				System.arraycopy(this.myObj.objVectors.uMul[i], 0, mu[i], 0,
						this.myObj.objVectors.maxVectors);
				System.arraycopy(this.myObj.objVectors.vMul[i], 0, mv[i], 0,
						this.myObj.objVectors.maxVectors);
			}

			int end = this.myObj.texture.length;
			for (int p = 0; p < end; p++) {
				mt[this.myObj.multiTex.length][p] = -1;
			}
			end = this.myObj.objVectors.maxVectors;
			for (int p = 0; p < end; p++) {
				mu[this.myObj.objVectors.uMul.length][p] = this.myObj.objVectors.nuOrg[p];
				mv[this.myObj.objVectors.vMul.length][p] = this.myObj.objVectors.nvOrg[p];
			}
			this.myObj.multiTex = mt;
			this.myObj.multiMode = mm;
			this.myObj.objVectors.uMul = mu;
			this.myObj.objVectors.vMul = mv;
			this.myObj.multiTex[(this.myObj.multiTex.length - 1)][polyID] = tid;
			this.myObj.multiMode[(this.myObj.multiMode.length - 1)][polyID] = mode;
			this.myObj.maxStagesUsed = Math.max(this.myObj.maxStagesUsed,
					this.myObj.multiTex.length + 1);
			added = true;
		}

		if (added) {
			this.myObj.usesMultiTexturing = true;

			if (this.myObj.maxStagesUsed > Config.maxTextureLayers)
				this.myObj.maxStagesUsed = Config.maxTextureLayers;
		} else {
			Logger.log("No further texture stage available ("
					+ this.myObj.maxStagesUsed + "/"
					+ this.myObj.multiTex.length + "/"
					+ Config.maxTextureLayers + ")!", 1);
		}
	}

	synchronized void setPolygonTexture(int polyID, TextureInfo tInf) {
		if (this.myObj.hasBeenStripped) {
			return;
		}

		if (tInf != null) {
			if ((this.myObj.multiTex == null) && (tInf.stageCnt > 1)) {
				this.myObj.multiTex = new int[Config.maxTextureLayers - 1][this.myObj.texture.length];
				this.myObj.multiMode = new int[Config.maxTextureLayers - 1][this.myObj.texture.length];
				for (int i = 0; i < this.myObj.texture.length; i++) {
					for (int ii = 0; ii < Config.maxTextureLayers - 1; ii++) {
						this.myObj.multiTex[ii][i] = -1;
					}
				}
				this.myObj.objVectors.createMultiCoords();
				this.myObj.usesMultiTexturing = true;
			}

			Vectors objVectors = this.myObj.objVectors;

			this.u0[0] = tInf.u0[0];
			this.u0[1] = tInf.u1[0];
			this.u0[2] = tInf.u2[0];

			this.v0[0] = tInf.v0[0];
			this.v0[1] = tInf.v1[0];
			this.v0[2] = tInf.v2[0];

			int texturID = tInf.textures[0];

			if (this.myObj.maxStagesUsed < tInf.stageCnt) {
				this.myObj.maxStagesUsed = tInf.stageCnt;
			}

			boolean multi = (this.myObj.usesMultiTexturing) && (tInf != null);

			this.myObj.texture[polyID] = texturID;

			if (multi) {
				for (int i = 0; i < tInf.stageCnt - 1; i++) {
					this.myObj.multiTex[i][polyID] = tInf.textures[(i + 1)];
					this.myObj.multiMode[i][polyID] = tInf.mode[(i + 1)];
				}

				int end = Math.min(this.myObj.multiTex.length,
						Config.maxTextureLayers - 1);
				for (int t = tInf.stageCnt - 1; t < end; t++) {
					this.myObj.multiTex[t][polyID] = -1;
				}
			}

			float[] u = (float[]) null;
			float[] v = (float[]) null;

			for (int i = 0; i < 3; i++) {
				int p = this.myObj.objMesh.points[polyID][i];

				objVectors.nuOrg[p] = this.u0[i];
				objVectors.nvOrg[p] = this.v0[i];

				switch (i) {
				case 0:
					u = tInf.u0;
					v = tInf.v0;
					break;
				case 1:
					u = tInf.u1;
					v = tInf.v1;
					break;
				case 2:
					u = tInf.u2;
					v = tInf.v2;
				}

				for (int t = 0; t < tInf.stageCnt - 1; t++) {
					objVectors.uMul[t][p] = u[(t + 1)];
					objVectors.vMul[t][p] = v[(t + 1)];
				}
			}
		}
	}

	void setVertexAlpha(int polyID, int vertexNumber, float alpha) {
		if ((this.myObj.hasBeenStripped) || (outOfBounds(polyID))
				|| (vertexNumber < 0) || (vertexNumber > 2)) {
			return;
		}
		int p = this.myObj.objMesh.points[polyID][vertexNumber];
		Vectors objVectors = this.myObj.objVectors;
		objVectors.createAlpha();

		if (alpha < 0.0F) {
			alpha = 0.0F;
		} else if (alpha > 1.0F) {
			alpha = 1.0F;
		}

		objVectors.alpha[p] = alpha;
	}

	SimpleVector getTransformedVertex(int polyID, int vertexNumber) {
		if ((outOfBounds(polyID)) || (vertexNumber < 0) || (vertexNumber > 2)) {
			return null;
		}
		Matrix trans = this.myObj.getWorldTransformation();
		SimpleVector s = SimpleVector.create();

		Mesh objMesh = this.myObj.objMesh;

		int p = objMesh.coords[objMesh.points[polyID][vertexNumber]];

		s.x = objMesh.xOrg[p];
		s.y = objMesh.yOrg[p];
		s.z = objMesh.zOrg[p];

		s.matMul(trans);
		return s;
	}

	SimpleVector getTextureUV(int polyID, int vertexNumber) {
		return getTextureUV(polyID, vertexNumber, SimpleVector.create());
	}

	SimpleVector getTextureUV(int polyID, int vertexNumber, SimpleVector toFill) {
		if ((this.myObj.hasBeenStripped) || (outOfBounds(polyID))
				|| (vertexNumber < 0) || (vertexNumber > 2)) {
			return null;
		}

		int p = this.myObj.objMesh.points[polyID][vertexNumber];

		Vectors objVectors = this.myObj.objVectors;
		SimpleVector uv = toFill;
		uv.x = objVectors.nuOrg[p];
		uv.y = objVectors.nvOrg[p];
		uv.z = 0.0F;
		return uv;
	}

	SimpleVector getTransformedNormal(int polyID) {
		if (outOfBounds(polyID)) {
			return null;
		}

		Matrix trans = this.myObj.getWorldTransformation();

		SimpleVector s = SimpleVector.create();
		Mesh objMesh = this.myObj.objMesh;

		int p0 = objMesh.coords[objMesh.points[polyID][0]];
		int p1 = objMesh.coords[objMesh.points[polyID][1]];
		int p2 = objMesh.coords[objMesh.points[polyID][2]];

		float x1 = objMesh.xOrg[p2];
		float y1 = objMesh.yOrg[p2];
		float z1 = objMesh.zOrg[p2];

		float x2 = objMesh.xOrg[p1];
		float y2 = objMesh.yOrg[p1];
		float z2 = objMesh.zOrg[p1];

		float x0 = objMesh.xOrg[p0];
		float y0 = objMesh.yOrg[p0];
		float z0 = objMesh.zOrg[p0];

		float vx = x0 - x1;
		float vy = y0 - y1;
		float vz = z0 - z1;

		float wx = x2 - x1;
		float wy = y2 - y1;
		float wz = z2 - z1;

		s.x = (vy * wz - vz * wy);
		s.y = (vz * wx - vx * wz);
		s.z = (vx * wy - vy * wx);

		trans.setRow(3, 0.0F, 0.0F, 0.0F, 1.0F);
		s.matMul(trans);

		return s.normalize(s);
	}

	int getMaxPolygonID()
	/*     */{
		/* 511 */
		return this.myObj.objMesh.anzTri;
		/*     */
	}

	private boolean outOfBounds(int polyID) {
		if ((polyID < 0) || (polyID >= this.myObj.objMesh.anzTri)) {
			Logger.log("No such polygon!", 0);
			return true;
		}
		return false;
	}
}
