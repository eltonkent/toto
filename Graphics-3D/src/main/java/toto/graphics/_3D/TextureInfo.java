package toto.graphics._3D;

public class TextureInfo {
	private static final int MODE_BASE = 0;
	public static final int MODE_MODULATE = 1;
	public static final int MODE_ADD = 2;
	public static final int MODE_REPLACE = 3;
	public static final int MODE_BLEND = 4;
	public static final int MAX_PHYSICAL_TEXTURE_STAGES = 4;
	int[] textures = new int[Config.maxTextureLayers];
	int[] mode = new int[Config.maxTextureLayers];
	float[] u0 = new float[Config.maxTextureLayers];
	float[] v0 = new float[Config.maxTextureLayers];
	float[] u1 = new float[Config.maxTextureLayers];
	float[] v1 = new float[Config.maxTextureLayers];
	float[] u2 = new float[Config.maxTextureLayers];
	float[] v2 = new float[Config.maxTextureLayers];
	int stageCnt = 0;

	public TextureInfo(int texID, float u0, float v0, float u1, float v1,
			float u2, float v2) {
		add(texID, u0, v0, u1, v1, u2, v2, 0);
	}

	public TextureInfo(int texID) {
		add(texID, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0);
	}

	public void add(int texID, int mode) {
		add(texID, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, mode);
	}

	public void set(int texID, int stageCnt, int mode) {
		set(texID, stageCnt, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, mode);
	}

	public void add(int texID, float u0, float v0, float u1, float v1,
			float u2, float v2, int mode) {
		set(texID, this.stageCnt, u0, v0, u1, v1, u2, v2, mode);
		this.stageCnt += 1;
	}

	public void set(int texID, int stageCnt, float u0, float v0, float u1,
			float v1, float u2, float v2, int mode) {
		if ((mode == 0) && (stageCnt != 0)) {
			Logger.log("Wrong mode for texture blending!", 0);
			return;
		}
		if (stageCnt >= Config.maxTextureLayers) {
			Logger.log(
					"Maximum number of texture layer configured by Config.maxTextureLayers is "
							+ Config.maxTextureLayers + "!", 1);
			return;
		}
		this.textures[stageCnt] = texID;
		this.u0[stageCnt] = u0;
		this.v0[stageCnt] = v0;
		this.u1[stageCnt] = u1;
		this.v1[stageCnt] = v1;
		this.u2[stageCnt] = u2;
		this.v2[stageCnt] = v2;
		this.mode[stageCnt] = mode;
	}
}
