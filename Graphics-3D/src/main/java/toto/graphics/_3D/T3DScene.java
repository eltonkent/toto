package toto.graphics._3D;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import toto.graphics.color.RGBColor;

/**
 * A Scene is what holds {@link toto.graphics._3D.T3DObject}s together
 */
public class T3DScene implements Serializable {
	private static final long serialVersionUID = 3L;
	public static final int FOGGING_DISABLED = 0;
	public static final int FOGGING_ENABLED = 1;
	static final int RGB_SCALE_DEFAULT = 1;
	static final int RGB_SCALE_2X = 2;
	static final int RGB_SCALE_4X = 4;
	private Object3DCompiler compiler = new Object3DCompiler();

	static Thread defaultThread = null;
	T3DObjectList objectList;
	VisList visList;
	Camera camera;
	Lights lights;
	long drawCnt = 0L;

	boolean useFogging = false;

	int fogModeChanged = 0;

	float fogStart = 1.0F;

	float fogDistance = 150.0F;

	float fogColorR = 0.0F;

	float fogColorG = 0.0F;

	float fogColorB = 0.0F;

	int ambientRed = 100;

	int ambientGreen = 100;

	int ambientBlue = 100;

	transient GLSLShader globalShader = null;

	private transient VisListManager vlManager = new VisListManager();

	private transient T3DObjectList targets = null;

	private boolean disposed = false;

	private float nearPlane = -1.0F;

	private float farPlane = -1.0F;

	private float nearPlaneOld = Config.nearPlane;

	private float farPlaneOld = Config.farPlane;

	private Matrix tmpMat = new Matrix();
	private Matrix tmpMatCol = new Matrix();

	private Plane slidePlaneTmp = new Plane();

	private List<Polyline> lines = null;

	private boolean calledRender = false;

	public T3DScene() {
		this.objectList = new T3DObjectList();
		this.visList = new VisList(Config.maxPolysVisible);

		addObject(T3DObject.createDummyObj());
		addObject(T3DObject.createDummyObj());

		this.camera = new Camera();
		this.lights = new Lights(Config.maxLights);
	}

	public void dispose() {
		this.disposed = true;
		if (this.vlManager != null)
			this.vlManager.dispose();
	}

	void setRGBScale(int scale) {
		getLights().setRGBScale(scale);
	}

	Lights getLights() {
		/* 152 */
		return this.lights;
		/*      */
	}

	public int getSize() {
		return this.objectList.size() - 2;
	}

	public Camera getCamera()
	/*      */{
		/* 170 */
		return this.camera;
		/*      */
	}

	public void newCamera() {
		this.camera = new Camera();
	}

	public void setCameraTo(Camera cam)
	/*      */{
		/* 188 */
		this.camera = cam;
		/*      */
	}

	public void setClippingPlanes(float near, float far) {
		this.nearPlane = Math.max(near, 0.1F);
		this.farPlane = Math.max(far, 1.0F);
	}

	void setPlanes(boolean back) {
		if (this.nearPlane != -1.0F)
			if (!back) {
				this.nearPlaneOld = Config.nearPlane;
				this.farPlaneOld = Config.farPlane;
				Config.nearPlane = this.nearPlane;
				Config.farPlane = this.farPlane;
			} else {
				Config.nearPlane = this.nearPlaneOld;
				Config.farPlane = this.farPlaneOld;
			}
	}

	public void removeObject(int id) {
		boolean found = false;
		id += 2;
		for (int i = 0; i < this.objectList.size(); i++) {
			if (this.objectList.elementAt(i).number == id) {
				found = true;
				this.objectList.elementAt(i).myRage3DScene = null;
				this.objectList.removeElementAt(i);
				break;
			}
		}
		if (!found) {
			Logger.log("Can't remove object #" + (id - 2) + "!", 0);
		} else if (this.visList != null)
			this.visList.deepClear();
	}

	public void removeObject(T3DObject obj) {
		if (!this.objectList.removeElement(obj)) {
			if ((obj.getUserObject() == null)
					|| (!(obj.getUserObject() instanceof Overlay)))
				Logger.log("Can't remove object #" + obj.getID()
						+ ", because it doesn't belong to this Rage3DScene!", 0);
		} else {
			obj.myRage3DScene = null;
			if (this.visList != null)
				this.visList.deepClear();
		}
	}

	public T3DObject getObject(int id) {
		id += 2;
		for (int i = 0; i < this.objectList.size(); i++) {
			if (this.objectList.elementAt(i).number == id) {
				return this.objectList.elementAt(i);
			}
		}
		Logger.log("Can't retrieve object #" + (id - 2) + "!", 0);
		return null;
	}

	public T3DObject getObjectByName(String name) {
		return getInternalObjectByName(name);
	}

	public int addObject(T3DObject obj) {
		if (obj == null) {
			Logger.log("Can't add 'null' to a Rage3DScene!", 0);
			return -100;
		}

		if ((obj.myRage3DScene == this) && (this.objectList.contains(obj))) {
			Logger.log("Object '" + obj.getName()
					+ "' already belongs to this Rage3DScene!", 1);
			return obj.getID();
		}

		this.objectList.addElement(obj);
		obj.myRage3DScene = this;
		return obj.getID();
	}

	public void addObjects(T3DObject[] objs) {
		for (int i = 0; i < objs.length; i++)
			addObject(objs[i]);
	}

	int addLight(SimpleVector lightPos, float r, float g, float b) {
		return this.lights
				.addLight(lightPos.x, lightPos.y, lightPos.z, r, g, b);
	}

	int addLight(SimpleVector lightPos, RGBColor col) {
		return this.lights.addLight(lightPos.x, lightPos.y, lightPos.z,
				col.getRed(), col.getGreen(), col.getBlue());
	}

	void setLightPosition(int number, SimpleVector lightPos) {
		this.lights.setPosition(number, lightPos.x, lightPos.y, lightPos.z);
	}

	void setLightVisibility(int number, boolean mode) {
		this.lights.setVisibility(number, mode);
	}

	void remove(int number) {
		this.lights.remove(number);
	}

	void setLightDiscardDistance(int number, float distance) {
		this.lights.setDiscardDistance(number, distance);
	}

	void setLightAttenuation(int number, float at) {
		this.lights.setAttenuation(number, at);
	}

	void setLightIntensity(int number, float r, float g, float b) {
		this.lights.setLightIntensity(number, r, g, b);
	}

	float getLightAttenuation(int number) {
		return this.lights.getAttenuation(number);
	}

	float getLightDiscardDistance(int number) {
		return this.lights.discardDistance[number];
	}

	void setLightDistanceOverride(int number, float distance) {
		this.lights.setDistanceOverride(number, distance);
	}

	float getLightDistanceOverride(int number) {
		return this.lights.getDistanceOverride(number);
	}

	SimpleVector getLightPosition(int number, SimpleVector s) {
		return this.lights.getPosition(number, s);
	}

	SimpleVector getLightIntensity(int number) {
		return this.lights.getIntensity(number);
	}

	public void setAmbientLight(int r, int g, int b) {
		this.ambientRed = r;
		this.ambientGreen = g;
		this.ambientBlue = b;
	}

	public int[] getAmbientLight() {
		return new int[] { this.ambientRed, this.ambientGreen, this.ambientBlue };
	}

	void fillAmbientLight(float[] ambient) {
		ambient[0] = this.ambientRed;
		ambient[1] = this.ambientGreen;
		ambient[2] = this.ambientBlue;
	}

	public void setFogging(int fog) {
		this.useFogging = (fog == 1);
		if (this.useFogging)
			this.fogModeChanged = 1;
		else
			this.fogModeChanged = 2;
	}

	public int getFogging() {
		if (this.useFogging) {
			return 1;
		}
		return 0;
	}

	public void setFogParameters(float distance, float red, float green,
			float blue) {
		setFogParameters(-999.0F, distance, red, green, blue);
	}

	public void setFogParameters(float start, float distance, float red,
			float green, float blue) {
		if (this.useFogging) {
			this.fogModeChanged = 1;
		}

		if (start != -999.0F) {
			this.fogStart = start;
		}
		this.fogDistance = Math.max(distance, 1.0F);
		this.fogColorR = red;
		this.fogColorG = green;
		this.fogColorB = blue;
	}

	public int checkCollision(SimpleVector org, SimpleVector dr, float step) {
		float[] drA = dr.toArray();
		float len = (float) Math.sqrt(drA[0] * drA[0] + drA[1] * drA[1]
				+ drA[2] * drA[2]);
		drA[0] /= len;
		drA[1] /= len;
		drA[2] /= len;
		return checkSomeCollision(org.toArray(), drA, step, null);
	}

	public SimpleVector checkCollisionSpherical(SimpleVector org,
			SimpleVector translation, float radius) {
		return checkSomeCollisionSpherical(org.toArray(),
				translation.toArray(), radius, null);
	}

	public SimpleVector checkCollisionEllipsoid(SimpleVector org,
			SimpleVector translation, SimpleVector ellipsoid, int recursionDepth) {
		if (recursionDepth < 1) {
			recursionDepth = 1;
		}
		return checkSomeCollisionEllipsoid(org, translation, ellipsoid, null,
				recursionDepth);
	}

	public boolean checkCameraCollision(int mode, float moveSpeed) {
		return checkCameraCollision(null, mode, moveSpeed, 3.0F, true);
	}

	public boolean checkCameraCollision(int mode, float moveSpeed,
			boolean slideMode) {
		return checkCameraCollision(null, mode, moveSpeed, 3.0F, slideMode);
	}

	public boolean checkCameraCollision(int mode, float moveSpeed,
			float distance, boolean slideMode) {
		return checkCameraCollision(null, mode, moveSpeed, distance, slideMode);
	}

	public boolean checkCameraCollision(SimpleVector direction,
			float moveSpeed, float distance, boolean slideMode) {
		return checkCameraCollision(direction, -1, moveSpeed, distance,
				slideMode);
	}

	public Object[] calcMinDistanceAndObject3D(SimpleVector orig,
			SimpleVector dir, float ignoreIfLarger) {
		T3DObject current = null;
		T3DObject minObj = null;

		float min = 3.4028235E+38F;

		int end = this.objectList.size();
		T3DObject[] objArray = this.objectList.getInternalArray();

		for (int i = 2; i < end; i++) {
			current = objArray[i];

			if ((current.isPotentialCollider)
					&& (current.isVisible)
					&& ((!current.hasBoundingBox) || (current
							.rayIntersectsAABB(orig, dir, true) < ignoreIfLarger))) {
				float tmp = current.calcMinDistance(orig, dir, ignoreIfLarger,
						false);
				if ((tmp < min) && (tmp != 1.0E+012F)) {
					min = tmp;
					minObj = current;
				}
			}
		}
		if ((min != 3.4028235E+38F) && (min != 1.0E+012F)) {
			if (minObj.collisionListener != null) {
				SimpleVector pos = new SimpleVector(dir);
				pos.scalarMul(min);
				pos.add(new SimpleVector(orig));
				minObj.notifyCollisionListeners(0, 0,
						new T3DObject[] { minObj }, pos);
			}
			minObj.wasCollider = true;
			return new Object[] { Float.valueOf(min), minObj };
		}
		return new Object[] { Float.valueOf(1.0E+012F) };
	}

	public float calcMinDistance(SimpleVector orig, SimpleVector dir,
			float ignoreIfLarger) {
		T3DObject current = null;
		T3DObject minObj = null;

		float min = 3.4028235E+38F;
		int end = this.objectList.size();

		T3DObject[] objArray = this.objectList.getInternalArray();

		for (int i = 2; i < end; i++) {
			current = objArray[i];

			if ((current.isVisible)
					&& (current.isPotentialCollider)
					&& ((!current.hasBoundingBox) || (current
							.rayIntersectsAABB(orig, dir, true) < ignoreIfLarger))) {
				float tmp = current.calcMinDistance(orig, dir, ignoreIfLarger,
						false);
				if ((tmp < min) && (tmp != 1.0E+012F)) {
					min = tmp;
					minObj = current;
				}
			}
		}
		if ((min != 3.4028235E+38F) && (min != 1.0E+012F)) {
			if (minObj.collisionListener != null) {
				SimpleVector pos = new SimpleVector(dir);
				pos.scalarMul(min);
				pos.add(new SimpleVector(orig));
				minObj.notifyCollisionListeners(0, 0,
						new T3DObject[] { minObj }, pos);
			}
			minObj.wasCollider = true;
			return min;
		}
		return 1.0E+012F;
	}

	public boolean checkCameraCollisionSpherical(int mode, float radius,
			float moveSpeed, boolean slideMode) {
		return checkCameraCollisionSpherical(null, mode, radius, moveSpeed,
				slideMode);
	}

	public boolean checkCameraCollisionSpherical(SimpleVector direction,
			float radius, float moveSpeed, boolean slideMode) {
		return checkCameraCollisionSpherical(direction, -1, radius, moveSpeed,
				slideMode);
	}

	public boolean checkCameraCollisionEllipsoid(int mode,
			SimpleVector ellipsoid, float moveSpeed, int recursionDepth) {
		if (recursionDepth < 1) {
			recursionDepth = 1;
		}
		return checkCameraCollisionEllipsoid(null, mode, ellipsoid, moveSpeed,
				recursionDepth);
	}

	public boolean checkCameraCollisionEllipsoid(SimpleVector direction,
			SimpleVector ellipsoid, float moveSpeed, int recursionDepth) {
		if (recursionDepth < 1) {
			recursionDepth = 1;
		}
		return checkCameraCollisionEllipsoid(direction, -1, ellipsoid,
				moveSpeed, recursionDepth);
	}

	public void buildAllObjects() {
		int end = this.objectList.size();
		for (int i = 2; i < end; i++)
			this.objectList.elementAt(i).build();
	}

	public synchronized void compileAllObjects() {
		int end = this.objectList.size();

		for (int i = 2; i < end; i++) {
			T3DObject current = this.objectList.elementAt(i);
			current.object3DRendered = false;

			if (!current.hasBeenBuild) {
				Logger.log("Object " + current.name
						+ " hasn't been build yet. Forcing build()!", 1);
				current.build();
			}

			if ((current.compiled != null) && (!current.isCompiled())
					&& (current.getMesh().anzTri > 0)) {
				if (current.shareWith != null) {
					T3DObject share = current.shareWith;
					if ((!share.isCompiled()) && (share.getMesh().anzTri > 0)) {
						if (share.compiled == null) {
							share.compile(current.dynamic, current.staticUV);
						}
						Logger.log("Compiling source object...", 2);
						compile(share);
					}
				}
				compile(current);
			}
		}
	}

	public synchronized void renderScene(FrameBuffer buffer) {
		try {
			if (defaultThread == null) {
				defaultThread = Thread.currentThread();
			}

			this.calledRender = true;
			buffer.glRend.disableBlitting();
			setPlanes(false);
			FrameBuffer lBuffer = buffer;

			if (this.vlManager == null) {
				this.vlManager = new VisListManager();
			}
			this.visList = this.vlManager.getVisList(lBuffer, this.visList);
			this.visList.clearList();

			int width = lBuffer.width;
			int height = lBuffer.height;

			if (buffer.renderTarget != null) {
				width = buffer.renderTarget.width;
				height = buffer.renderTarget.height;
				if (buffer.virtualHeight > 0) {
					width = buffer.virtualWidth;
					height = buffer.virtualHeight;
				}
			}

			this.camera.calcFOV(width, height);
			buffer.glRend.setFrustumAndFog(this, buffer);
			this.lights.transformLights(this.camera);

			int end = this.objectList.size();
			T3DObject[] objArray = this.objectList.getInternalArray();

			for (int i = 2; i < end; i++) {
				T3DObject current = objArray[i];

				if (current == null) {
					Logger.log("Null object in queue...?", 0);
				} else {
					current.object3DRendered = false;

					if ((!current.hasBeenBuild)
							|| ((current.hasBeenBuild) && (current.compiled == null))) {
						Logger.log("Object " + current.name
								+ " hasn't been build yet. Forcing build()!", 2);
						current.build();
					}

					if ((current.compiled != null) && (!current.isCompiled())
							&& (current.getMesh().anzTri > 0)) {
						if (current.shareWith != null) {
							T3DObject share = current.shareWith;
							if ((!share.isCompiled())
									&& (share.getMesh().anzTri > 0)) {
								if (share.compiled == null) {
									share.compile(current.dynamic,
											current.staticUV);
								}
								Logger.log("Compiling source object...", 2);
								compile(share);
							}
						}
						compile(current);
					}

					Object usrObj = current.getUserObject();
					if ((usrObj != null) && ((usrObj instanceof Overlay))) {
						((Overlay) usrObj).update(buffer);
					}

					if (current.isVisible) {
						boolean clipped = current.transformVertices(buffer);
						if ((!clipped) && (current.objMesh.anzTri > 0)) {
							current.render();
						}
					}
				}
			}
			this.visList.fillInstances();

			this.visList.sort(0, this.visList.anzpoly);
			setPlanes(true);
		} catch (NullPointerException npe) {
			Logger.log(
					"There's a problem with the object list not being consistent during rendering. This is often caused by concurrent modification of Rage3D objects on a thread different from the rendering thread!",
					1);
			Logger.log(npe, 0);
		}
	}

	public void draw(FrameBuffer buffer) {
		draw(buffer, false, 0, 0, false);
	}

	public void drawWireframe(FrameBuffer buffer, RGBColor color, int size,
			boolean pointMode) {
		draw(buffer, true, color.getRGB(), size, pointMode);
	}

	public long getFrameCounter()
	/*      */{
		/* 1124 */
		return this.drawCnt;
		/*      */
	}

	public Enumeration<T3DObject> getObjects() {
		Enumeration objs = this.objectList.elements();
		if (this.objectList.size() > 1) {
			objs.nextElement();
			objs.nextElement();
		}
		return objs;
	}

	public void setObjectsVisibility(boolean visible) {
		for (Enumeration e = this.objectList.elements(); e.hasMoreElements();)
			((T3DObject) e.nextElement()).setVisibility(visible);
	}

	public void removeAll() {
		removeAllLights();
		removeAllObjects();
	}

	public void removeAllObjects() {
		T3DObject clippedPolys = this.objectList.elementAt(0);
		T3DObject portals = this.objectList.elementAt(1);
		for (int i = 0; i < this.objectList.size(); i++) {
			this.objectList.elementAt(i).myRage3DScene = null;
		}
		this.objectList = new T3DObjectList();
		addObject(clippedPolys);
		addObject(portals);
		if (this.visList != null)
			this.visList.deepClear();
	}

	public void removeAllLights() {
		this.lights = new Lights(Config.maxLights);
	}

	public void addPolyline(Polyline line) {
		if (this.lines == null) {
			this.lines = new ArrayList();
		}
		this.lines.add(line);
	}

	public void removePolyline(Polyline line) {
		if (this.lines == null) {
			return;
		}
		this.lines.remove(line);
	}

	public static synchronized void setDefaultThread(Thread def)
	/*      */{
		/* 1229 */
		defaultThread = def;
		/*      */
	}

	public static synchronized Thread getDefaultThread()
	/*      */{
		/* 1238 */
		return defaultThread;
		/*      */
	}

	public void setGlobalShader(GLSLShader globalShader)
	/*      */{
		/* 1249 */
		this.globalShader = globalShader;
		/*      */
	}

	public GLSLShader getGlobalShader()
	/*      */{
		/* 1258 */
		return this.globalShader;
		/*      */
	}

	T3DObject getInternalObjectByName(String name) {
		for (int i = 0; i < this.objectList.size(); i++) {
			if (this.objectList.elementAt(i).name.equals(name)) {
				return this.objectList.elementAt(i);
			}
		}
		return null;
	}

	final SimpleVector checkObjCollisionSpherical(T3DObject obj,
			SimpleVector dirVec, float radius) {
		if (obj.mayCollide) {
			float[] orig = new float[3];
			float[] dir = new float[3];

			obj.getProjectedPoint(obj.centerX, obj.centerY, obj.centerZ, null,
					orig, this.tmpMat);

			dir[0] = dirVec.x;
			dir[1] = dirVec.y;
			dir[2] = dirVec.z;

			return checkSomeCollisionSpherical(orig, dir, radius, obj);
		}
		return SimpleVector.create(dirVec.x, dirVec.y, dirVec.z);
	}

	final SimpleVector checkObjCollisionEllipsoid(T3DObject obj,
			SimpleVector dirVec, SimpleVector ellipsoid, int maxDepth) {
		if (obj.mayCollide) {
			SimpleVector orig = SimpleVector.create();

			obj.getProjectedPoint(obj.centerX, obj.centerY, obj.centerZ, orig,
					null, this.tmpMat);

			return checkSomeCollisionEllipsoid(orig, dirVec, ellipsoid, obj,
					maxDepth);
		}
		return SimpleVector.create(dirVec);
	}

	final int checkObjCollision(T3DObject obj, SimpleVector dirVec,
			float step) {
		if (obj.mayCollide) {
			float[] orig = new float[3];
			float[] dir = new float[3];

			obj.getProjectedPoint(obj.centerX, obj.centerY, obj.centerZ, null,
					orig, this.tmpMat);

			float x = dirVec.x;
			float y = dirVec.y;
			float z = dirVec.z;

			float dn = 1.0F / (float) Math.sqrt(x * x + y * y + z * z);
			dir[0] = (x * dn);
			dir[1] = (y * dn);
			dir[2] = (z * dn);

			return checkSomeCollision(orig, dir, step, obj);
		}
		return -100;
	}

	private final void draw(FrameBuffer buffer, boolean wireframe,
			int frameColor, int size, boolean pointMode) {
		if (!this.calledRender) {
			buffer.glRend.setFrustumAndFog(this, buffer);
		}
		this.calledRender = false;

		setPlanes(false);
		GLRenderer gl = buffer.glRend;

		boolean hasToResetViewport = false;

		this.drawCnt += 1L;

		int width = buffer.getWidth();
		int height = buffer.getHeight();
		int end = this.visList.anzpoly;

		gl.startPainting();

		if (this.visList.anzpoly != -1) {
			if ((Config.viewportOffsetX != 0.0F)
					|| (Config.viewportOffsetY != 0.0F)) {
				gl.setBufferViewport((int) (Config.viewportOffsetX * width),
						(int) (-Config.viewportOffsetY * height), width, height);
				hasToResetViewport = true;
			}

			if (wireframe)
				gl.drawWireframe(this.visList, 0, end, frameColor, buffer,
						this, size, pointMode);
			else {
				gl.drawVertexArray(this.visList, 0, end, buffer, this);
			}

			gl.endState();
			if (hasToResetViewport) {
				buffer.glRend.setBufferViewport(0, 0, width, height);
			}
		}

		if (this.lines != null) {
			gl.enableLineMode(this);
			for (int i = 0; i < this.lines.size(); i++) {
				Polyline line = (Polyline) this.lines.get(i);
				if (line.isVisible()) {
					gl.drawLine(line);
				}
			}
			gl.disableLineMode();
		}

		gl.endPainting();
		setPlanes(true);
	}

	private final boolean checkCameraCollision(SimpleVector direction,
			int mode, float moveSpeed, float distance, boolean slideMode) {
		if (mode == 7) {
			mode = 1;
		}

		float maxXStep = 3.4028235E+38F;
		float maxYStep = 3.4028235E+38F;
		float maxZStep = 3.4028235E+38F;

		float dx = 0.0F;
		float dy = 0.0F;
		float dz = 0.0F;
		float mul = 1.0F;
		float sBackX = 0.0F;
		float sBackY = 0.0F;
		float sBackZ = 0.0F;

		float[] orig = new float[3];
		float[] dirX = new float[3];
		float[] dirY = new float[3];
		float[] dirZ = new float[3];

		float bx = this.camera.backBx;
		float by = this.camera.backBy;
		float bz = this.camera.backBz;

		int pos = 0;

		orig[0] = bx;
		orig[1] = by;
		orig[2] = bz;

		if (direction == null) {
			if ((mode & 0x1) != 1) {
				mul = -1.0F;
			}

			pos = 2 - ((mode + 1) / 2 - 1);

			sBackX = this.camera.backMatrix.mat[0][pos];
			sBackY = this.camera.backMatrix.mat[1][pos];
			sBackZ = this.camera.backMatrix.mat[2][pos];

			dx = sBackX * mul;
			dy = sBackY * mul;
			dz = sBackZ * mul;
		} else {
			mul = 1.0F;
			sBackX = direction.x;
			sBackY = direction.y;
			sBackZ = direction.z;

			dx = sBackX;
			dy = sBackY;
			dz = sBackZ;
		}

		float[] dA = { dx, dy, dz };

		dirX[0] = dx;
		dirX[1] = 0.0F;
		dirX[2] = 0.0F;

		dirY[0] = 0.0F;
		dirY[1] = dy;
		dirY[2] = 0.0F;

		dirZ[0] = 0.0F;
		dirZ[1] = 0.0F;
		dirZ[2] = dz;

		if (dirX[0] < 0.0F)
			dirX[0] = -1.0F;
		else {
			dirX[0] = 1.0F;
		}
		if (dirY[1] < 0.0F)
			dirY[1] = -1.0F;
		else {
			dirY[1] = 1.0F;
		}
		if (dirZ[2] < 0.0F)
			dirZ[2] = -1.0F;
		else {
			dirZ[2] = 1.0F;
		}

		boolean colXR = true;
		boolean colYR = true;
		boolean colZR = true;
		boolean anyCol = false;

		float lastT = 1.0E+012F;
		T3DObject colliderX = null;
		T3DObject colliderY = null;
		T3DObject colliderZ = null;

		for (int i = 2; i < this.objectList.size(); i++) {
			T3DObject current = this.objectList.elementAt(i);
			current.wasCollider = false;
			current.resetPolygonIDCount();

			if ((current.isPotentialCollider)
					&& (current.isVisible)
					&& ((!current.hasBoundingBox) || (current
							.rayIntersectsAABB(orig, dA, false) < Config.collideOffset))) {
				lastT = current.collide(orig, dirX, 3.0F,
						Config.collideSectorOffset);
				if (lastT < maxXStep) {
					maxXStep = lastT;
					colliderX = current;
				}
				lastT = current.collide(orig, dirY, 3.0F,
						Config.collideSectorOffset);
				if (lastT < maxYStep) {
					maxYStep = lastT;
					colliderY = current;
				}
				lastT = current.collide(orig, dirZ, 3.0F,
						Config.collideSectorOffset);
				if (lastT < maxZStep) {
					maxZStep = lastT;
					colliderZ = current;
				}
			}

		}

		float distHalf = distance * 0.9F;

		if ((maxXStep < distance) && (maxXStep > distHalf)) {
			maxXStep = distance;
		}
		if ((maxYStep < distance) && (maxYStep > distHalf)) {
			maxYStep = distance;
		}
		if ((maxZStep < distance) && (maxZStep > distHalf)) {
			maxZStep = distance;
		}

		float speedX = sBackX * moveSpeed * mul;
		float absSpeed = Math.abs(speedX);
		if (maxXStep - absSpeed <= distance) {
			anyCol = true;
			if (speedX >= 0.0F)
				speedX = maxXStep - distance;
			else {
				speedX = -(maxXStep - distance);
			}
			if (colliderX != null)
				colliderX.wasCollider = true;
		} else {
			colXR = false;
		}

		float speedY = sBackY * moveSpeed * mul;
		absSpeed = Math.abs(speedY);
		if (maxYStep - absSpeed <= distance) {
			anyCol = true;
			if (speedY >= 0.0F)
				speedY = maxYStep - distance;
			else {
				speedY = -(maxYStep - distance);
			}
			if (colliderY != null)
				colliderY.wasCollider = true;
		} else {
			colYR = false;
		}

		float speedZ = sBackZ * moveSpeed * mul;
		absSpeed = Math.abs(speedZ);
		if (maxZStep - absSpeed <= distance) {
			anyCol = true;
			if (speedZ >= 0.0F)
				speedZ = maxZStep - distance;
			else {
				speedZ = -(maxZStep - distance);
			}
			if (colliderZ != null)
				colliderZ.wasCollider = true;
		} else {
			colZR = false;
		}

		if ((colliderX != null) && (colliderX.wasCollider)) {
			colliderX.notifyCollisionListeners(0, 0,
					new T3DObject[] { colliderX }, null);
		}

		if ((colliderY != null) && (colliderY.wasCollider)
				&& (!((Object) colliderY).equals(colliderX))) {
			colliderY.notifyCollisionListeners(0, 0,
					new T3DObject[] { colliderY }, null);
		}

		if ((colliderZ != null) && (colliderZ.wasCollider)
				&& (!((Object) colliderZ).equals(colliderX))
				&& (!((Object) colliderZ).equals(colliderY))) {
			colliderZ.notifyCollisionListeners(0, 0,
					new T3DObject[] { colliderZ }, null);
		}

		if ((slideMode) || (!anyCol)) {
			this.camera.backBx += speedX;
			this.camera.backBy += speedY;
			this.camera.backBz += speedZ;
		}

		return colXR | colYR | colZR;
	}

	private final int checkSomeCollision(float[] orig, float[] dir, float step,
			T3DObject obj) {
		T3DObject current = null;
		T3DObject lastCur = null;

		float maxStep = 3.4028235E+38F;

		float lastT = 1.0E+012F;

		for (int i = 2; i < this.objectList.size(); i++) {
			current = this.objectList.elementAt(i);
			current.wasCollider = false;
			current.resetPolygonIDCount();

			if ((current.isPotentialCollider)
					&& ((obj == null) || (current != obj))
					&& (current.isVisible)
					&& ((!current.hasBoundingBox) || (current
							.rayIntersectsAABB(orig, dir, true) < Config.collideOffset))) {
				lastT = current.collide(orig, dir, step,
						Config.collideSectorOffset);
				if (lastT < maxStep) {
					maxStep = lastT;
					if (maxStep < step) {
						lastCur = current;
					}
				}
			}
		}

		if ((maxStep < step) && (lastCur != null)) {
			lastCur.wasCollider = true;
			for (int k = 2; k < this.objectList.size(); k++) {
				T3DObject cur = this.objectList.elementAt(k);
				if (cur != lastCur) {
					cur.wasCollider = false;
					cur.resetPolygonIDCount();
				}
			}

			if (((obj != null) && (obj.collisionListener != null) && (!obj.disableListeners))
					|| ((lastCur.collisionListener != null) && (!lastCur.disableListeners))) {
				T3DObject[] targets = { lastCur };
				SimpleVector pos = SimpleVector.create(dir[0], dir[1], dir[2]);
				pos.scalarMul(maxStep);
				pos.x += orig[0];
				pos.y += orig[1];
				pos.z += orig[2];

				if (obj != null) {
					obj.notifyCollisionListeners(1, 0, targets, pos);
				}
				lastCur.notifyCollisionListeners(obj, 0, 0, targets, pos);
			}
			return lastCur.getID();
		}
		return -100;
	}

	private final SimpleVector checkSomeCollisionSpherical(float[] org,
			float[] dir, float radius, T3DObject obj) {
		T3DObject current = null;
		float[] orig = new float[3];

		org[0] += dir[0];
		org[1] += dir[1];
		org[2] += dir[2];

		boolean[] col = new boolean[1];

		for (int i = 2; i < this.objectList.size(); i++) {
			current = this.objectList.elementAt(i);
			current.wasCollider = false;
			current.resetPolygonIDCount();

			if ((current.isPotentialCollider)
					&& ((obj == null) || (current != obj))
					&& (current.isVisible)) {
				if ((!current.hasBoundingBox)
						|| (current.sphereIntersectsAABB(orig, radius))) {
					boolean storeCol = col[0];
					col[0] = false;
					orig = current.collideSpherical(orig, radius,
							Config.collideSectorOffset, col, false);
					if (col[0]) {
						current.wasCollider = true;
					}
					col[0] |= storeCol;
				}
			}

		}

		if (col[0]) {
			SimpleVector dirS = SimpleVector.create(orig[0], orig[1], orig[2]);
			dirS.x -= org[0];
			dirS.y -= org[1];
			dirS.z -= org[2];
			notifyAll(obj, null, 1, orig);
			return dirS;
		}
		return SimpleVector.create(dir[0], dir[1], dir[2]);
	}

	private final boolean checkCameraCollisionSpherical(SimpleVector direction,
			int mode, float radius, float moveSpeed, boolean slideMode) {
		float[] orig = { this.camera.backBx, this.camera.backBy,
				this.camera.backBz };

		if (direction == null) {
			if (mode != 7) {
				float mul = -1.0F;
				if ((mode & 0x1) == 1) {
					mul = 1.0F;
				}
				mul *= moveSpeed;

				int pos = 2 - ((mode + 1) / 2 - 1);

				orig[0] += this.camera.backMatrix.mat[0][pos] * mul;
				orig[1] += this.camera.backMatrix.mat[1][pos] * mul;
				orig[2] += this.camera.backMatrix.mat[2][pos] * mul;
			}
		} else {
			orig[0] += direction.x * moveSpeed;
			orig[1] += direction.y * moveSpeed;
			orig[2] += direction.z * moveSpeed;
		}

		boolean[] col = new boolean[1];

		for (int i = 2; i < this.objectList.size(); i++) {
			T3DObject current = this.objectList.elementAt(i);
			current.wasCollider = false;
			current.resetPolygonIDCount();

			if ((current.isPotentialCollider)
					&& (current.isVisible)
					&& ((!current.hasBoundingBox) || (current
							.sphereIntersectsAABB(orig, radius)))) {
				boolean storeCol = col[0];
				col[0] = false;
				orig = current.collideSpherical(orig, radius,
						Config.collideSectorOffset, col, true);
				if (col[0]) {
					current.wasCollider = true;
				}
				col[0] |= storeCol;
			}

		}

		boolean collision = col[0];

		if ((slideMode) || (!collision)) {
			this.camera.backBx = orig[0];
			this.camera.backBy = orig[1];
			this.camera.backBz = orig[2];
		}

		if (collision) {
			notifyAll(null, null, 1, new SimpleVector(orig));
		}

		return collision;
	}

	private final SimpleVector checkSomeCollisionEllipsoid(SimpleVector org,
			SimpleVector direction, SimpleVector ellipsoid, T3DObject obj,
			int maxDepth) {
		CollisionInfo cInf = new CollisionInfo();

		cInf.eRadius = ellipsoid;
		cInf.r3Pos = SimpleVector.create(org);
		cInf.r3Velocity = SimpleVector.create(direction);
		cInf.calculateInverseAndDest();

		if ((obj != null)
				&& (obj.getEllipsoidMode() == EllipsoidMode.ELLIPSOID_TRANSFORMED)) {
			cInf.addTransMat = obj.getWorldTransformation();
			Matrix m = cInf.addTransMat.cloneMatrix();
			m.mat[3][0] = 0.0F;
			m.mat[3][1] = 0.0F;
			m.mat[3][2] = 0.0F;
			cInf.addRotMat = m;
		}

		doWorldCollisionEllipsoid(cInf, 0, obj, maxDepth);

		SimpleVector orig = cInf.r3Pos;

		if (cInf.collision) {
			orig.x -= org.x;
			orig.y -= org.y;
			orig.z -= org.z;
		} else {
			orig.set(direction);
		}
		return orig;
	}

	private final boolean checkCameraCollisionEllipsoid(SimpleVector direction,
			int mode, SimpleVector ellipsoid, float moveSpeed, int maxDepth) {
		float dirX = 0.0F;
		float dirY = 0.0F;
		float dirZ = 0.0F;

		if (direction == null) {
			if (mode != 7) {
				float mul = -1.0F;
				if ((mode & 0x1) == 1) {
					mul = 1.0F;
				}
				mul *= moveSpeed;

				int pos = 2 - ((mode + 1) / 2 - 1);

				dirX = this.camera.backMatrix.mat[0][pos] * mul;
				dirY = this.camera.backMatrix.mat[1][pos] * mul;
				dirZ = this.camera.backMatrix.mat[2][pos] * mul;
			}
		} else {
			dirX = direction.x * moveSpeed;
			dirY = direction.y * moveSpeed;
			dirZ = direction.z * moveSpeed;
		}

		CollisionInfo cInf = new CollisionInfo();

		cInf.eRadius = ellipsoid;
		cInf.r3Pos = new SimpleVector(this.camera.backBx, this.camera.backBy,
				this.camera.backBz);
		cInf.r3Velocity = new SimpleVector(dirX, dirY, dirZ);
		cInf.calculateInverseAndDest();

		if (this.camera.getEllipsoidMode() == 1) {
			Matrix mat = new Matrix();
			mat.mat[3][0] = this.camera.backBx;
			mat.mat[3][1] = this.camera.backBy;
			mat.mat[3][2] = this.camera.backBz;

			Matrix mat2 = this.camera.backMatrix.cloneMatrix();
			mat2.matMul(mat);

			cInf.addTransMat = mat2;
			cInf.addRotMat = this.camera.backMatrix.cloneMatrix();
		}

		doWorldCollisionEllipsoid(cInf, 0, null, maxDepth);

		this.camera.backBx = cInf.r3Pos.x;
		this.camera.backBy = cInf.r3Pos.y;
		this.camera.backBz = cInf.r3Pos.z;

		return cInf.collision;
	}

	private final void doWorldCollisionEllipsoid(CollisionInfo cInf, int depth,
			T3DObject obj, int maxDepth) {
		float veryCloseDistance = Config.collideEllipsoidThreshold;
		float veryCloseDistanceSquared = veryCloseDistance * veryCloseDistance;
		boolean collision = false;

		if (depth >= maxDepth) {
			return;
		}

		cInf.foundCollision = false;
		cInf.intersectionPoint = SimpleVector.create();
		cInf.nearestDistance = 1.0E+011F;
		if (depth == 0) {
			cInf.collision = false;
		}

		SimpleVector tr = cInf.r3Velocity;
		SimpleVector tr2 = null;

		float ltr = tr.x * tr.x + tr.y * tr.y + tr.z * tr.z;

		float min = cInf.eRadius.x;
		if (min > cInf.eRadius.y) {
			min = cInf.eRadius.y;
		}
		if (min > cInf.eRadius.z) {
			min = cInf.eRadius.z;
		}

		min *= 2.0F;
		if (ltr > min * min) {
			tr = SimpleVector.create(tr);
			tr.x = Math.abs(tr.x);
			tr.y = Math.abs(tr.y);
			tr.z = Math.abs(tr.z);
			tr.add(cInf.eRadius);
			tr2 = SimpleVector.create(cInf.r3Velocity);
			tr2.scalarMul(0.5F);
			tr2.add(cInf.r3Pos);
		}

		for (int i = 2; i < this.objectList.size(); i++) {
			T3DObject current = this.objectList.elementAt(i);
			boolean lazy = current.getLazyTransformationState();
			if (!lazy) {
				current.enableLazyTransformations();
			}

			if (depth == 0) {
				current.wasCollider = false;
				current.resetPolygonIDCount();
			}

			if ((current.isPotentialCollider)
					&& ((obj == null) || (current != obj))
					&& (current.isVisible)
					&& ((!current.hasBoundingBox)
							|| ((tr2 != null) && (current
									.ellipsoidIntersectsAABB(tr2, tr))) || ((tr2 == null) && ((current
							.ellipsoidIntersectsAABB(cInf.r3Pos, cInf.eRadius)) || (current
							.ellipsoidIntersectsAABB(cInf.r3Destx,
									cInf.r3Desty, cInf.r3Destz, cInf.eRadius)))))) {
				cInf.isPartOfCollision = false;

				current.collideEllipsoid(cInf, Config.collideSectorOffset);
				current.wasCollider |= cInf.isPartOfCollision;
				collision |= cInf.isPartOfCollision;
			}

			if (!lazy) {
				current.disableLazyTransformations();
			}

		}

		if (!collision) {
			cInf.r3Pos.add(cInf.r3Velocity);
			return;
		}

		SimpleVector eSpaceDestPoint = SimpleVector
				.create(cInf.eSpaceBasePoint);
		eSpaceDestPoint.add(cInf.eSpaceVelocity);

		SimpleVector newBasePoint = SimpleVector.create(cInf.r3Pos);

		float newBasePointEx = cInf.eSpaceBasePoint.x;
		float newBasePointEy = cInf.eSpaceBasePoint.y;
		float newBasePointEz = cInf.eSpaceBasePoint.z;

		if ((cInf.nearestDistance >= veryCloseDistance)
				|| (cInf.nearestDistance <= 0.0F)) {
			SimpleVector velocity = cInf.r3Velocity;
			float mul = cInf.nearestDistance - veryCloseDistance;

			float vx = velocity.x * mul;
			float vy = velocity.y * mul;
			float vz = velocity.z * mul;

			newBasePoint.x += vx;
			newBasePoint.y += vy;
			newBasePoint.z += vz;

			velocity = cInf.eSpaceVelocity;
			vx = velocity.x * mul;
			vy = velocity.y * mul;
			vz = velocity.z * mul;

			newBasePointEx += vx;
			newBasePointEy += vy;
			newBasePointEz += vz;

			float len = (float) Math.sqrt(velocity.x * velocity.x + velocity.y
					* velocity.y + velocity.z * velocity.z);

			vx = velocity.x / len;
			vy = velocity.y / len;
			vz = velocity.z / len;

			cInf.intersectionPoint = SimpleVector.create(
					cInf.intersectionPoint.x - veryCloseDistance * vx,
					cInf.intersectionPoint.y - veryCloseDistance * vy,
					cInf.intersectionPoint.z - veryCloseDistance * vz);
		}

		SimpleVector slidePlaneOrigin = cInf.intersectionPoint;
		SimpleVector slidePlaneNormal = SimpleVector.create(newBasePointEx
				- slidePlaneOrigin.x, newBasePointEy - slidePlaneOrigin.y,
				newBasePointEz - slidePlaneOrigin.z);
		slidePlaneNormal = slidePlaneNormal.normalize(slidePlaneNormal);
		this.slidePlaneTmp.setTo(slidePlaneOrigin, slidePlaneNormal);

		slidePlaneNormal.scalarMul(this.slidePlaneTmp
				.distanceTo(eSpaceDestPoint));

		eSpaceDestPoint.sub(slidePlaneNormal);
		SimpleVector newVelocityVector = eSpaceDestPoint
				.calcSub(cInf.intersectionPoint);

		float length = newVelocityVector.x * newVelocityVector.x
				+ newVelocityVector.y * newVelocityVector.y
				+ newVelocityVector.z * newVelocityVector.z;

		newVelocityVector.x *= cInf.eRadius.x;
		newVelocityVector.y *= cInf.eRadius.y;
		newVelocityVector.z *= cInf.eRadius.z;

		if (cInf.addTransMat == null)
			newVelocityVector = cInf.collisionObject.reverseTransform(
					this.tmpMatCol, newVelocityVector, false);
		else {
			newVelocityVector.matMul(cInf.addRotMat);
		}

		cInf.r3Pos = newBasePoint;
		cInf.r3Velocity = newVelocityVector;

		SimpleVector storePos = null;
		if (depth == 0) {
			storePos = SimpleVector.create(newBasePoint);
		}

		cInf.recalcDest();

		if (length >= veryCloseDistanceSquared) {
			doWorldCollisionEllipsoid(cInf, depth + 1, obj, maxDepth);
		}
		if (depth == 0)
			notifyAll(obj, cInf, 2, storePos);
	}

	private void compile(T3DObject obj) {
		this.compiler.compile(obj);
		if (obj.toStrip)
			obj.reallyStrip();
	}

	private void notifyAll(T3DObject obj, CollisionInfo cInf, int algorithm,
			float[] contact) {
		if (hasToNotify(cInf))
			notifyAll(obj, cInf, algorithm,
					SimpleVector.create(contact[0], contact[1], contact[2]));
	}

	private boolean hasToNotify(CollisionInfo cInf) {
		return ((cInf == null) || (cInf.collision))
				&& (T3DObject.globalListenerCount > 0);
	}

	private void notifyAll(T3DObject obj, CollisionInfo cInf, int algorithm,
			SimpleVector contact) {
		if (hasToNotify(cInf)) {
			if (this.targets == null)
				this.targets = new T3DObjectList(10);
			else {
				this.targets.clear();
			}
			for (int i = 2; i < this.objectList.size(); i++) {
				T3DObject current = this.objectList.elementAt(i);
				if (current.wasCollider) {
					this.targets.addElement(current);
				}
			}

			T3DObject[] arr = this.targets.toArray();

			if (obj != null) {
				obj.notifyCollisionListeners(1, algorithm, arr, contact);
			}

			for (int i = 0; i < this.targets.size(); i++) {
				T3DObject cur = this.targets.elementAt(i);
				cur.notifyCollisionListeners(obj, 0, algorithm, arr, contact);
			}
		}
	}

	protected void finalize() {
		if (!this.disposed)
			dispose();
	}
}
