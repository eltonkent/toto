package toto.graphics._3D;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import android.content.Context;

public class Virtualizer {
	private static int cnt = 0;
	private int myCnt = 0;
	private Map<Integer, String> virtualized = new HashMap();
	private boolean cleaned = false;
	private Context ctx = null;
	private long maxSize = -1L;
	private long currentSize;
	private byte[] buffy = new byte[4];
	private byte[] buffer = new byte[2048];

	public Virtualizer(int sizeInMb) {
		this();
		this.maxSize = (sizeInMb * 1024 * 1024);
	}

	public Virtualizer() {
		this.myCnt = (cnt++);
	}

	public void setContext(Context ctx) {
		this.ctx = ctx;
		try {
			cleanUp(ctx);
		} catch (Exception localException) {
		}
	}

	public void cleanUp() {
		if (this.ctx != null)
			try {
				cleanUpInstanceOnly(this.ctx);
			} catch (Exception e) {
				Logger.log("Failed to clean up virtualizer: " + e.getMessage());
			}
	}

	void freeHandles(Texture tex) {
		Integer key = Integer.valueOf(System.identityHashCode(tex));
		if (this.virtualized.containsKey(key)) {
			tex.texels = null;
			tex.zippedTexels = null;
		}
	}

	boolean isFull() {
		return (this.maxSize != -1L) && (this.maxSize <= this.currentSize);
	}

	boolean restore(Texture tex) {
		if (this.ctx == null) {
			Logger.log("No context setKey!", 0);
			return false;
		}

		InputStream is = null;
		try {
			Integer key = Integer.valueOf(System.identityHashCode(tex));
			String name = (String) this.virtualized.get(key);

			if (name == null)
				return false;
			boolean zipped = name.endsWith("_z_.dat");

			is = new BufferedInputStream(this.ctx.openFileInput(name), 2048);
			int cnt = 0;

			int size = tex.height * tex.width;
			if (!zipped) {
				size *= 4;
			}

			ByteArrayOutputStream bos = new ByteArrayOutputStream(size);
			do {
				cnt = is.read(this.buffer);
				if (cnt != -1)
					bos.write(this.buffer, 0, cnt);
			} while (cnt > -1);

			byte[] texels = bos.toByteArray();

			if (zipped)
				tex.zippedTexels = texels;
			else {
				tex.texels = ZipHelper.byteArrayToInt(texels);
			}

			Logger.log("Retrieved texture data from disk!");
		} catch (Exception e) {
			Logger.log("Unable to restore texture: " + e.getMessage(), 1);
			return false;
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException localIOException2) {
				}
		}
		if (is != null) {
			try {
				is.close();
			} catch (IOException localIOException3) {
			}
		}
		return true;
	}

	boolean store(Texture tex) {
		if (this.ctx == null) {
			Logger.log("No context setKey!", 0);
			return false;
		}

		if ((tex.texels == null) && (tex.zippedTexels == null)) {
			return false;
		}
		OutputStream os = null;
		try {
			cleanUp(this.ctx);

			boolean zipped = true;
			byte[] texels = tex.zippedTexels;

			if (texels == null) {
				texels = ZipHelper.intToByteArray(tex.texels);
				zipped = false;
			}

			if ((this.maxSize != -1L)
					&& (this.currentSize + texels.length > this.maxSize)) {
				Logger.log(
						"Maximum disk space für virtual textures exceeded!", 1);
				return false;
			}
			this.currentSize += texels.length;

			Integer key = Integer.valueOf(System.identityHashCode(tex));
			String name = "_vir_" + this.myCnt + "_" + key
					+ (zipped ? "_z_" : "_n_") + ".dat";
			os = new BufferedOutputStream(this.ctx.openFileOutput(name, 0),
					2048);
			os.write(texels);
			this.virtualized.put(key, name);
			tex.texels = null;
			tex.zippedTexels = null;
			Logger.log("Stored texture data on disk!");
		} catch (Exception e) {
			Logger.log("Unable to virtualize texture: " + e.getMessage(), 1);
			return false;
		} finally {
			if (os != null)
				try {
					os.close();
				} catch (IOException localIOException2) {
				}
		}
		if (os != null) {
			try {
				os.close();
			} catch (IOException localIOException3) {
			}
		}
		return true;
	}

	<T extends Buffer> Buffer restore(CompiledInstance ci, Class<T> clazz,
			String subName) {
		if (this.ctx == null) {
			Logger.log("No context setKey!", 0);
			return null;
		}

		String add = "_bb_";
		int bufferType = 0;
		if (IntBuffer.class.equals(clazz)) {
			add = "_ib_";
			bufferType = 1;
		}
		if (FloatBuffer.class.equals(clazz)) {
			add = "_fb_";
			bufferType = 2;
		}
		if (ShortBuffer.class.equals(clazz)) {
			add = "_sb_";
			bufferType = 3;
		}

		String key = System.identityHashCode(ci) + "_" + subName;
		String name = "_vir_" + this.myCnt + "_" + key + add + ".dat";

		String nv = (String) this.virtualized.get(Integer.valueOf(System
				.identityHashCode(ci)));
		if ((nv != null) && (nv.indexOf(name) != -1)) {
			InputStream is = null;
			Buffer buffer = null;
			try {
				Logger.log("Trying to restore buffer from file " + name, 3);
				is = new BufferedInputStream(new GZIPInputStream(
						this.ctx.openFileInput(name)), 2048);
				ByteBuffer bb = readIntoBuffer(is);

				switch (bufferType) {
				case 0:
					buffer = bb;
					break;
				case 1:
					buffer = bb.asIntBuffer();
					break;
				case 2:
					buffer = bb.asFloatBuffer();
					break;
				case 3:
					buffer = bb.asShortBuffer();
				}

				Logger.log("Buffer of type " + bufferType
						+ " restored from disk!");
			} catch (Exception e) {
				Logger.log("Unable to restore buffer of type " + bufferType
						+ ": " + e.getMessage(), 1);

				if (is != null)
					try {
						is.close();
					} catch (IOException localIOException) {
					}
			} finally {
				if (is != null)
					try {
						is.close();
					} catch (IOException localIOException1) {
					}
			}
			return (Buffer) clazz.cast(buffer);
		}

		Logger.log("Buffer data with name " + name + " not found!", 3);
		return null;
	}

	private ByteBuffer readIntoBuffer(InputStream is) throws Exception {
		int len = ((is.read() & 0xFF) << 24) + ((is.read() & 0xFF) << 16)
				+ ((is.read() & 0xFF) << 8) + (is.read() & 0xFF);
		ByteBuffer buffer = ByteBuffer.allocateDirect(len).order(
				ByteOrder.nativeOrder());
		byte[] content = new byte[len];
		int read = is.read(content);
		if (read != len) {
			throw new RuntimeException(
					"Read file length doesn't match buffer length: " + read
							+ "!=" + len);
		}
		buffer.put(content);
		content = (byte[]) null;
		buffer.rewind();
		return buffer;
	}

	ByteBuffer readFromCache(String name) {
		if (this.ctx == null) {
			Logger.log("No context setKey!", 0);
			return null;
		}

		InputStream is = null;
		try {
			File cacheDir = this.ctx.getCacheDir();
			File file = new File(cacheDir, name);
			if (file.isFile()) {
				is = new BufferedInputStream(new FileInputStream(file), 2048);
				ByteBuffer bb = readIntoBuffer(is);
				Logger.log("Loaded " + name + " from cache!");
				return bb;
			}
		} catch (Exception e) {
			Logger.log("Unable to load " + name + " from cache: " + e, 0);
			return null;
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException localIOException2) {
				}
		}
		if (is != null) {
			try {
				is.close();
			} catch (IOException localIOException3) {
			}
		}
		Logger.log(name + " not found in cache!");
		return null;
	}

	boolean storeInCache(ByteBuffer buffer, String name) {
		if (this.ctx == null) {
			Logger.log("No context setKey!", 0);
			return false;
		}

		if (buffer == null) {
			return false;
		}

		buffer.rewind();
		OutputStream os = null;
		try {
			File cacheDir = this.ctx.getCacheDir();
			File[] files = cacheDir.listFiles();

			List<File> fileList = new ArrayList(Arrays.asList(files));
			Collections.sort(fileList, new Comparator<File>()
			/*     */{
				/*     */
				public int compare(File lhs, File rhs) {
					return (int) -Math.signum((float) (lhs.lastModified() - rhs
							.lastModified()));
				}
				/*     */
			});
			long length = 0L;
			for (File file : fileList) {
				if (!file.isDirectory()) {
					length += file.length();
				}
			}

			long maxSize = 5242880L;

			if (length > maxSize) {
				Logger.log("Purging cache directory!");
				for (File file : fileList) {
					if (length < maxSize) {
						break;
					}
					if (!file.isDirectory()) {
						length -= file.length();
						file.delete();
					}
				}
			}

			os = new BufferedOutputStream(new FileOutputStream(new File(
					cacheDir, name)), 2048);
			ByteBuffer bb = buffer;
			byte[] data = new byte[bb.limit()];
			bb.rewind();
			bb.get(data);
			int len = data.length;
			writeInt(len, os, true);
			os.write(data);
			buffer.rewind();
		} catch (Exception e) {
			Logger.log("Unable to cache buffer: " + e.getMessage(), 1);
			if (Logger.isDebugEnabled()) {
				Logger.log(e);
			}
			return false;
		} finally {
			if (os != null)
				try {
					os.close();
				} catch (IOException localIOException1) {
				}
		}
		Logger.log("Stored " + name + " in cache!");
		return true;
	}

	boolean store(CompiledInstance ci, Buffer buffer, String subName) {
		if (this.ctx == null) {
			Logger.log("No context setKey!", 0);
			return false;
		}

		if (buffer == null) {
			return false;
		}

		OutputStream os = null;

		String add = "_bb_";
		int bufferType = 0;
		if ((buffer instanceof IntBuffer)) {
			add = "_ib_";
			bufferType = 1;
		}
		if ((buffer instanceof FloatBuffer)) {
			add = "_fb_";
			bufferType = 2;
		}
		if ((buffer instanceof ShortBuffer)) {
			add = "_sb_";
			bufferType = 3;
		}

		String key = System.identityHashCode(ci) + "_" + subName;
		String name = "_vir_" + this.myCnt + "_" + key + add + ".dat";
		try {
			cleanUp(this.ctx);
			os = new BufferedOutputStream(new GZIPOutputStream(
					this.ctx.openFileOutput(name, 0)), 2048);

			int len = 0;
			switch (bufferType) {
			case 0:
				ByteBuffer bb = (ByteBuffer) buffer;
				byte[] data = new byte[bb.limit()];
				bb.rewind();
				bb.get(data);
				len = data.length;
				writeInt(len, os, true);
				os.write(data);
				data = (byte[]) null;
				break;
			case 1:
				IntBuffer ib = (IntBuffer) buffer;
				int[] datai = new int[ib.limit()];
				ib.rewind();
				ib.get(datai);
				len = datai.length << 2;
				writeInt(len, os, true);
				for (int i = 0; i < datai.length; i++) {
					writeInt(datai[i], os, false);
				}
				datai = (int[]) null;
				break;
			case 2:
				FloatBuffer fb = (FloatBuffer) buffer;
				float[] dataf = new float[fb.limit()];
				fb.rewind();
				fb.get(dataf);
				len = dataf.length << 2;
				writeInt(len, os, true);
				for (int i = 0; i < dataf.length; i++) {
					writeInt(Float.floatToIntBits(dataf[i]), os, false);
				}
				dataf = (float[]) null;
				break;
			case 3:
				ShortBuffer sb = (ShortBuffer) buffer;
				short[] datas = new short[sb.limit()];
				sb.rewind();
				sb.get(datas);
				len = datas.length << 1;
				writeInt(len, os, true);
				for (int i = 0; i < datas.length; i++) {
					writeShort(datas[i], os);
				}
				datas = (short[]) null;
			}

			int hkey = System.identityHashCode(ci);

			String nv = (String) this.virtualized.get(Integer.valueOf(hkey));
			if (nv == null) {
				nv = "";
			}

			this.virtualized.put(Integer.valueOf(hkey), nv + "/" + name);
			Logger.log("Stored buffer of type " + bufferType + " on disk ("
					+ len + " bytes / " + name + ")!");
		} catch (Exception e) {
			Logger.log("Unable to virtualize buffer of type " + bufferType
					+ ": " + e.getMessage(), 1);
			if (Logger.isDebugEnabled()) {
				Logger.log(e);
			}
			return false;
		} finally {
			if (os != null)
				try {
					os.close();
				} catch (IOException localIOException1) {
				}
		}
		return true;
	}

	private void writeInt(int ii, OutputStream os, boolean alwaysBig)
			throws Exception {
		if ((ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN) || (alwaysBig)) {
			this.buffy[0] = ((byte) (ii >> 24));
			this.buffy[1] = ((byte) (ii >> 16 & 0xFF));
			this.buffy[2] = ((byte) (ii >> 8 & 0xFF));
			this.buffy[3] = ((byte) (ii & 0xFF));
		} else {
			this.buffy[3] = ((byte) (ii >> 24));
			this.buffy[2] = ((byte) (ii >> 16 & 0xFF));
			this.buffy[1] = ((byte) (ii >> 8 & 0xFF));
			this.buffy[0] = ((byte) (ii & 0xFF));
		}
		os.write(this.buffy);
	}

	private void writeShort(short ii, OutputStream os) throws Exception {
		if (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN) {
			this.buffy[0] = ((byte) (ii >> 8));
			this.buffy[1] = ((byte) (ii & 0xFF));
		} else {
			this.buffy[1] = ((byte) (ii >> 8));
			this.buffy[0] = ((byte) (ii & 0xFF));
		}
		os.write(this.buffy, 0, 2);
	}

	public void finalize() {
		if (this.ctx != null)
			try {
				cleanUpInstanceOnly(this.ctx);
			} catch (Exception localException) {
			}
	}

	private void cleanUp(Context ctx) throws Exception {
		if (this.cleaned) {
			return;
		}
		this.cleaned = true;
		String[] files = ctx.fileList();
		boolean ok = true;
		for (String file : files) {
			if ((file.startsWith("_vir_")) && (file.endsWith(".dat"))) {
				ok &= ctx.deleteFile(file);
			}
		}
		Logger.log("Cleaned up cache (" + files.length + " files): " + ok);
	}

	private void cleanUpInstanceOnly(Context ctx) throws Exception {
		Logger.log("Finalizing Virtualizer...cleaning up instance cache!");
		String[] files = ctx.fileList();
		boolean ok = true;
		for (String file : files) {
			if ((file.startsWith("_vir_" + this.myCnt))
					&& (file.endsWith(".dat"))) {
				ok &= ctx.deleteFile(file);
			}
		}
		Logger.log("Cleaned up instance cache (" + files.length + " files): "
				+ ok);
	}

	OutputStream getOutputCacheStream(String fileName) {
		File dir = this.ctx.getCacheDir();
		try {
			return new FileOutputStream(new File(dir, fileName));
		} catch (FileNotFoundException e) {
			Logger.log("Cache not available!");
		}
		return null;
	}

	InputStream getInputCacheStream(String fileName) {
		File dir = this.ctx.getCacheDir();
		try {
			return new FileInputStream(new File(dir, fileName));
		} catch (FileNotFoundException e) {
			Logger.log("File '" + fileName + "' not in cache!");
		}
		return null;
	}

	boolean isVirtual(Texture tex) {
		Integer key = Integer.valueOf(System.identityHashCode(tex));
		String name = (String) this.virtualized.get(key);
		return name != null;
	}

	boolean isCached(String fileName) {
		File dir = this.ctx.getCacheDir();
		return new File(dir, fileName).isFile();
	}
}
