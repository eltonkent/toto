package toto.graphics._3D;

import java.lang.reflect.Field;
import java.util.ArrayList;

/*     */

public final class Config {
	static final int BLACK_MASK = 15790320;
	static final float ADD_Z_SORT = 1000000.0F;
	private static final String INT_VERSION = "1.27";
	public static float defaultCameraFOV = 1.25F;

	public static boolean autoMaintainAspectRatio = true;

	public static int maxTextures = 64;

	public static int maxPolysVisible = 512;

	public static int maxLights = 8;

	public static int maxAnimationSubSequences = 20;

	public static float collideOffset = 40.0F;

	public static float collideEdgeMul = 1.0F;

	public static float collideSectorOffset = 3.0F;

	public static float collideEllipsoidThreshold = 0.1F;

	public static int flashSortThreshold = 150;

	public static boolean stateOrientedSorting = true;

	public static float nearPlane = 1.0F;

	public static float farPlane = 1000.0F;

	public static float viewportOffsetX = 0.0F;

	public static float viewportOffsetY = 0.0F;

	public static boolean viewportOffsetAffectsRenderTarget = false;

	public static int maxParentObjects = 1;

	public static float specTerm = 10.0F;

	public static float specPow = 6.0F;

	public static int maxTextureLayers = 2;

	public static float glTransparencyOffset = 0.1F;

	public static float glTransparencyMul = 0.06F;

	public static boolean internalMipmapCreation = false;

	public static boolean glIgnoreNearPlane = true;

	public static boolean glTriangleStrips = false;

	public static boolean glDither = true;

	public static int glDebugLevel = 0;

	public static boolean glTrilinear = false;

	public static boolean glForceEnvMapToSecondStage = false;

	public static boolean glUseIgnorantBlits = false;

	public static boolean glRevertADDtoMODULATE = false;

	public static int glBatchSize = 8000;

	public static boolean glForceHighPrecision = false;

	public static boolean renderTargetsAsSubImages = true;

	public static boolean useVBO = true;

	public static int polygonIDLimit = 50;

	public static int vertexBufferSize = 240;

	public static boolean aggressiveStripping = true;

	public static boolean disableNativeBufferCopies = false;

	public static boolean unloadImmediately = false;

	public static boolean useRotationPivotFrom3DS = false;

	public static boolean useNormalsFromOBJ = false;

	public static boolean oldStyle3DSLoader = false;

	public static boolean cacheCompressedTextures = false;

	public static boolean skipDefaultShaders = false;

	public static boolean fixCollapsingVertices = false;

	public static int byteBufferLimit = 65536;

	public static int aaMode = 0;

	static int loadMaxVerticesASC = 66000;
	static int loadMaxTrianglesASC = 66000;

	static boolean useFBO = true;

	static boolean glIgnoreAlphaBlendingFBO = false;

	static int glStageCount = 1;

	static boolean fadeoutLight = false;
	static float linearDiv = 50.0F;
	static float lightDiscardDistance = -1.0F;
	static final float COLLIDE_CAMERA_RANGE = 3.0F;

	public static String getVersion() {
		return "1.27";
	}

	public static void setParameterValue(String name, Object value) {
		Field field = null;
		try {
			field = Config.class.getField(name);
			field.set(Config.class, value);
		} catch (IllegalArgumentException ia) {
			Logger.log("Wrong parameter value type: " + value.getClass()
					+ " found, " + field.getType() + " expected!", 0);
		} catch (Exception e) {
			Logger.log("Unknown parameter: " + name, 0);
		}
	}

	public static Object getParameterValue(String name) {
		try {
			Field field = Config.class.getField(name);
			return field.get(Config.class);
		} catch (Exception e) {
		}
		return null;
	}

	public static String[] getParameterNames() {
		Field[] fields = Config.class.getFields();
		ArrayList names = new ArrayList();
		int cnt = 0;
		for (int i = 0; i < fields.length; i++) {
			String n = fields[i].getName();
			if (!n.equals("VERSION")) {
				names.add(n);
				cnt++;
			}
		}
		return (String[]) names.toArray(new String[names.size()]);
	}
}
