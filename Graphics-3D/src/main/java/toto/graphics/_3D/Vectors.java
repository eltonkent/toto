package toto.graphics._3D;

import java.io.Serializable;

final class Vectors implements Serializable {
	private static final long serialVersionUID = 1L;
	float[] nuOrg;
	float[] nvOrg;
	float[][] uMul;
	float[][] vMul;
	float[] alpha;
	int maxVectors;
	private Mesh baseMesh;

	Vectors(int max, Mesh mesh) {

		this.baseMesh = mesh;
		this.maxVectors = max;
		this.nuOrg = new float[this.maxVectors];
		this.nvOrg = new float[this.maxVectors];
	}

	/*     */void createMultiCoords()
	/*     */{
		if (this.uMul == null) {
			this.uMul = new float[Config.maxTextureLayers - 1][this.maxVectors];
			this.vMul = new float[Config.maxTextureLayers - 1][this.maxVectors];
		}
	}

	/*     */void createAlpha() {
		if (this.alpha == null) {
			this.alpha = new float[this.maxVectors];
			for (int i = 0; i < this.maxVectors; i++)
				this.alpha[i] = 1.0F;
		}
	}

	/*     */void killMultiCoords()
	/*     */{
		this.uMul = null;
		this.vMul = null;
	}

	/*     */int checkCoords(float x, float y, float z)
	/*     */{
		int i = this.baseMesh.anzCoords - 1;
		float[] xOrg = this.baseMesh.xOrg;
		float[] yOrg = this.baseMesh.yOrg;
		float[] zOrg = this.baseMesh.zOrg;
		while (i >= 0) {
			if ((xOrg[i] == x) && (yOrg[i] == y) && (zOrg[i] == z)) {
				return i;
			}
			i--;
		}
		return -1;
	}

	int addVertex(float x, float y, float z) {
		int pos3 = this.baseMesh.anzCoords;
		this.baseMesh.xOrg[pos3] = x;
		this.baseMesh.yOrg[pos3] = y;
		this.baseMesh.zOrg[pos3] = z;
		this.baseMesh.anzCoords += 1;
		return pos3;
	}

	void setMesh(Mesh Mesh) {
		/* 93 */
		this.baseMesh = Mesh;
		/*     */
	}

	void strip() {
		this.nuOrg = null;
		this.nvOrg = null;
		this.uMul = null;
		this.vMul = null;
		this.alpha = null;
	}
}
