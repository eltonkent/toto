package toto.graphics._3D;

import java.io.Serializable;

class Plane implements Serializable {
	private static final long serialVersionUID = 1L;
	private float equation3 = 0.0F;
	SimpleVector normal = new SimpleVector();

	public Plane() {
	}

	public Plane(SimpleVector point, SimpleVector normal) {
		setTo(point, normal);
	}

	public void setTo(SimpleVector point, SimpleVector normal) {
		this.normal.set(normal);
		this.equation3 = (-(normal.x * point.x + normal.y * point.y + normal.z
				* point.z));
	}

	public Plane(SimpleVector p0, SimpleVector p1, SimpleVector p2) {
		setTo(p0, p1, p2);
	}

	public void setTo(SimpleVector p0, SimpleVector p1, SimpleVector p2) {
		setTo(p0.x, p0.y, p0.z, p1.x, p1.y, p1.z, p2.x, p2.y, p2.z);
	}

	public void setTo(float p0x, float p0y, float p0z, float p1x, float p1y,
			float p1z, float p2x, float p2y, float p2z) {
		float x0 = p1x - p0x;
		float y0 = p1y - p0y;
		float z0 = p1z - p0z;

		float x1 = p2x - p0x;
		float y1 = p2y - p0y;
		float z1 = p2z - p0z;

		float x = y0 * z1 - z0 * y1;
		float y = z0 * x1 - x0 * z1;
		float z = x0 * y1 - y0 * x1;

		float val = x * x + y * y + z * z;
		float n = (float) Math.sqrt(val);
		if (n != 0.0F) {
			float nt = 1.0F / n;
			this.normal.set(x * nt, y * nt, z * nt);
		} else {
			this.normal.set(0.0F, 0.0F, 0.0F);
		}
		this.equation3 = (-(this.normal.x * p0x + this.normal.y * p0y + this.normal.z
				* p0z));
	}

	public boolean isFrontFacingTo(SimpleVector point) {
		return this.normal.x * point.x + this.normal.y * point.y
				+ this.normal.z * point.z <= 0.0F;
	}

	public boolean isFrontFacingTo(float pointX, float pointY, float pointZ) {
		return this.normal.x * pointX + this.normal.y * pointY + this.normal.z
				* pointZ <= 0.0F;
	}

	public float distanceTo(SimpleVector point) {
		return this.normal.x * point.x + this.normal.y * point.y
				+ this.normal.z * point.z + this.equation3;
	}
}
