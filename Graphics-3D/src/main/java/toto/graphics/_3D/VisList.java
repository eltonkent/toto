package toto.graphics._3D;

import java.io.Serializable;
import java.util.ArrayList;

final class VisList implements Serializable {
	private static final long serialVersionUID = 1L;
	int anzpoly;
	int size;
	T3DObject[] vorg;
	int[] vnum;
	int[] vertexIndex;
	float[] zValue;
	int[] stageCnt;
	long lastCycle = -1L;
	private int pivotindex;
	private int msgCnt = 0;
	private int maxStages = 0;
	private ArrayList<T3DObject> toFill = new ArrayList();
	private int[] bucket = new int[30];

	VisList(int maxpoly) {
		this.vorg = new T3DObject[maxpoly + 1];
		this.vnum = new int[maxpoly + 1];
		this.vertexIndex = new int[maxpoly + 1];
		this.zValue = new float[maxpoly + 1];
		this.stageCnt = new int[maxpoly + 1];
		this.size = maxpoly;
		this.anzpoly = -1;
	}

	synchronized void addToFill(T3DObject obj) {
		if (!this.toFill.contains(obj))
			/* 58 */this.toFill.add(obj);
	}

	public int getMaxSize()
	/*     */{
		/* 69 */
		return this.size;
		/*     */
	}

	void clearList() {
		this.anzpoly = -1;
		this.maxStages = 0;
	}

	void deepClear() {
		clearList();
		for (int i = 0; i < this.size; i++)
			this.vorg[i] = null;
	}

	void addToList(T3DObject obj, float mitz, int pt, CompiledInstance ci) {
		if (this.anzpoly < this.size) {
			int polyNum = ci.getPolyIndex();
			int stgCnt = ci.getStageCount() - 1;
			this.anzpoly += 1;
			if (stgCnt > this.maxStages) {
				this.maxStages = stgCnt;
			}
			this.stageCnt[this.anzpoly] = stgCnt;
			this.vorg[this.anzpoly] = obj;
			this.vnum[this.anzpoly] = polyNum;

			mitz += obj.sortOffset;
			if (obj.isTrans)
				this.zValue[this.anzpoly] = (1000000.0F - mitz);
			else {
				this.zValue[this.anzpoly] = mitz;
			}
			this.vertexIndex[this.anzpoly] = pt;
		} else {
			if (this.msgCnt == 0) {
				Logger.log(
						"You've exceeded the configured instance limit for the visibility list. Consider adjusting Config.maxPolysVisible!",
						1);
			}
			this.msgCnt += 1;
		}
	}

	synchronized void fillInstances() {
		int end = this.toFill.size();
		for (int i = 0; i < end; i++) {
			T3DObject obj = (T3DObject) this.toFill.get(i);
			if (obj.modified) {
				int size = obj.compiled.size();
				for (int ii = 0; ii < size; ii++) {
					((CompiledInstance) obj.compiled.get(ii)).fill();
				}
				obj.modified = false;
			}
		}
		this.toFill.clear();
	}

	void sort(int i, int j) {
		if (j - i >= Config.flashSortThreshold) {
			j++;
			fsort(i, j);
			insertionSort(i, j);
		} else {
			qsort(i, j);
		}
	}

	private void qsort(int i, int j) {
		findpivot(i, j);
		if (this.pivotindex != -1) {
			int k = partition(i, j, this.zValue[this.pivotindex]);
			qsort(i, k - 1);
			qsort(k, j);
		}
	}

	private int partition(int l, int r, float pivot) {
		while (l <= r) {
			if (this.zValue[l] < pivot) {
				l++;
			} else if (this.zValue[r] >= pivot) {
				r--;
			} else {
				float fake1 = this.zValue[r];
				this.zValue[r] = this.zValue[l];
				this.zValue[l] = fake1;
				T3DObject fakeo = this.vorg[r];
				this.vorg[r] = this.vorg[l];
				this.vorg[l] = fakeo;
				int faken = this.vnum[r];
				this.vnum[r] = this.vnum[l];
				this.vnum[l] = faken;
				faken = this.vertexIndex[r];
				this.vertexIndex[r] = this.vertexIndex[l];
				this.vertexIndex[l] = faken;
				faken = this.stageCnt[r];
				this.stageCnt[r] = this.stageCnt[l];
				this.stageCnt[l] = faken;
				l++;
				r--;
			}
		}
		return l;
	}

	private void findpivot(int i2, int j2)
	/*     */{
		this.pivotindex = -1;
		int k = i2;
		float zv = this.zValue[i2];
		while ((this.pivotindex == -1) && (k <= j2))
			/* 200 */if (this.zValue[k] > zv) {
				this.pivotindex = k;
			} else {
				if (this.zValue[k] < zv) {
					this.pivotindex = i2;
				}
				k++;
			}
	}

	private void fsort(int i, int j) {
		int k = i;
		float anmin = zValue[0];
		int nmax = 0;
		int n = j;
		int m = n / 20;
		if (m < 30)
			m = 30;
		if (m > bucket.length)
			bucket = new int[m];
		int l[] = bucket;
		int he = l.length;
		for (int h = 0; h < he; h++)
			l[h] = 0;

		for (i = 1; i < n; i++) {
			if (zValue[i] < anmin)
				anmin = zValue[i];
			if (zValue[i] > zValue[nmax])
				nmax = i;
		}

		if (anmin == zValue[nmax])
			return;
		float c1 = ((float) m - 1.0F) / (zValue[nmax] - anmin);
		for (i = 0; i < n; i++) {
			k = (int) (c1 * (zValue[i] - anmin));
			l[k]++;
		}

		for (k = 1; k < m; k++)
			l[k] += l[k - 1];

		flip(nmax, 0);
		int nmove = 0;
		j = 0;
		k = m - 1;
		while (nmove < n - 1) {
			while (j > l[k] - 1) {
				j++;
				k = (int) (c1 * (zValue[j] - anmin));
			}
			float zf = zValue[j];
			int vnumf = vnum[j];
			int scf = stageCnt[j];
			int vif = vertexIndex[j];
			T3DObject vorgf = vorg[j];
			while (j != l[k]) {
				k = (int) (c1 * (zf - anmin));
				int index = l[k] - 1;
				float zh = zValue[index];
				zValue[index] = zf;
				zf = zh;
				int vnumh = vnum[index];
				vnum[index] = vnumf;
				vnumf = vnumh;
				int sch = stageCnt[index];
				stageCnt[index] = scf;
				scf = sch;
				int vih = vertexIndex[index];
				vertexIndex[index] = vif;
				vif = vih;
				T3DObject vorgh = vorg[index];
				vorg[index] = vorgf;
				vorgf = vorgh;
				l[k]--;
				nmove++;
			}
		}
	}

	private void insertionSort(int s, int e) {
		for (int i = e - 3; i >= s; i--)
			if (this.zValue[(i + 1)] < this.zValue[i]) {
				float zValueHold = this.zValue[i];
				T3DObject vorgHold = this.vorg[i];
				int vnumHold = this.vnum[i];
				int stageCntHold = this.stageCnt[i];
				int vertexIndexHold = this.vertexIndex[i];

				int j = i;

				while (this.zValue[(j + 1)] < zValueHold) {
					int jo = j + 1;
					this.zValue[j] = this.zValue[jo];
					this.vorg[j] = this.vorg[jo];
					this.vnum[j] = this.vnum[jo];
					this.stageCnt[j] = this.stageCnt[jo];
					this.vertexIndex[j] = this.vertexIndex[jo];
					j++;
				}

				this.zValue[j] = zValueHold;
				this.vorg[j] = vorgHold;
				this.vnum[j] = vnumHold;
				this.stageCnt[j] = stageCntHold;
				this.vertexIndex[j] = vertexIndexHold;
			}
	}

	private void flip(int l, int r) {
		float fake1 = this.zValue[r];
		this.zValue[r] = this.zValue[l];
		this.zValue[l] = fake1;

		T3DObject fakeo = this.vorg[r];
		this.vorg[r] = this.vorg[l];
		this.vorg[l] = fakeo;

		int faken = this.vnum[r];
		this.vnum[r] = this.vnum[l];
		this.vnum[l] = faken;

		faken = this.stageCnt[r];
		this.stageCnt[r] = this.stageCnt[l];
		this.stageCnt[l] = faken;

		faken = this.vertexIndex[r];
		this.vertexIndex[r] = this.vertexIndex[l];
		this.vertexIndex[l] = faken;
	}
}
