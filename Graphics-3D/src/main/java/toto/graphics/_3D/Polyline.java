package toto.graphics._3D;

import java.io.Serializable;
import java.nio.FloatBuffer;

import toto.graphics.color.RGBColor;

/*     */

public class Polyline implements Serializable {
	private static final long serialVersionUID = 1L;
	private FloatBufferWrapper vertices = null;
	private int length = 0;
	private int maxLength = 0;
	private RGBColor color = RGBColor.WHITE;
	private float width = 1.0F;
	private TransparencyMode transparencyMode = TransparencyMode.DEFAULT;
	private float[] buffer = new float[3];
	private float percentage = 1.0F;
	private boolean visible = true;
	private boolean pointMode = false;

	public Polyline(SimpleVector[] points, RGBColor color) {
		this.vertices = new FloatBufferWrapper(points.length * 3);
		int end = points.length;
		for (int i = 0; i < end; i++) {
			this.buffer[0] = points[i].x;
			this.buffer[1] = points[i].y;
			this.buffer[2] = points[i].z;
			this.vertices.put(this.buffer);
		}
		this.vertices.rewind();
		this.length = points.length;
		this.maxLength = this.length;
		setColor(color);
	}

	public void update(SimpleVector[] newPoints) {
		if (newPoints.length > this.maxLength) {
			Logger.log("New data's size exceeds the size of the Polyline!", 0);
			return;
		}
		int end = newPoints.length;
		this.vertices.clear();
		for (int i = 0; i < end; i++) {
			this.buffer[0] = newPoints[i].x;
			this.buffer[1] = newPoints[i].y;
			this.buffer[2] = newPoints[i].z;
			this.vertices.put(this.buffer);
		}
		this.vertices.rewind();
		this.length = end;
	}

	public int getLength() {
		if (this.percentage == 1.0F) {
			return this.length;
		}
		return (int) (this.length * this.percentage);
	}

	public void setColor(RGBColor color)
	/*     */{
		/* 94 */
		this.color = color;
		/*     */
	}

	public RGBColor getColor()
	/*     */{
		/* 103 */
		return this.color;
		/*     */
	}

	FloatBuffer getData() {
		/* 107 */
		return this.vertices.floats;
		/*     */
	}

	public void setWidth(float width)
	/*     */{
		/* 117 */
		this.width = width;
		/*     */
	}

	public float getWidth()
	/*     */{
		/* 126 */
		return this.width;
		/*     */
	}

	public void setTransparencyMode(TransparencyMode transparencyMode)
	/*     */{
		/* 137 */
		this.transparencyMode = transparencyMode;
		/*     */
	}

	public TransparencyMode getTransparencyMode()
	/*     */{
		/* 146 */
		return this.transparencyMode;
		/*     */
	}

	public void setPercentage(float percentage) {
		if (percentage < 0.0F)
			percentage = 0.0F;
		else if (percentage > 1.0F) {
			percentage = 1.0F;
		}
		this.percentage = percentage;
	}

	public float getPercentage()
	/*     */{
		/* 172 */
		return this.percentage;
		/*     */
	}

	public void setVisible(boolean visible)
	/*     */{
		/* 182 */
		this.visible = visible;
		/*     */
	}

	public boolean isVisible()
	/*     */{
		/* 191 */
		return this.visible;
		/*     */
	}

	public void setPointMode(boolean pointMode)
	/*     */{
		/* 201 */
		this.pointMode = pointMode;
		/*     */
	}

	public boolean isPointMode()
	/*     */{
		/* 210 */
		return this.pointMode;
		/*     */
	}
}
