package toto.graphics._3D;

public class Light {
	public static final boolean LIGHT_VISIBLE = true;
	public static final boolean LIGHT_INVISIBLE = false;
	private int light = -1;

	private boolean on = false;

	private T3DScene rage3DScene = null;

	private SimpleVector rotCache = new SimpleVector();

	public Light(T3DScene rage3DScene) {
		this.rage3DScene = rage3DScene;
		this.light = rage3DScene.addLight(new SimpleVector(), 255.0F, 255.0F,
				255.0F);
		this.on = true;
	}

	public void enable() {
		this.on = true;
		this.rage3DScene.setLightVisibility(this.light, true);
	}

	public void disable() {
		this.on = false;
		this.rage3DScene.setLightVisibility(this.light, false);
	}

	public boolean isEnabled()
	/*     */{
		/* 63 */
		return this.on;
		/*     */
	}

	public void dispose() {
		this.rage3DScene.remove(this.light);
		this.light = -999;
	}

	public void setIntensity(SimpleVector inty) {
		setIntensity(inty.x, inty.y, inty.z);
	}

	public void setIntensity(float red, float green, float blue) {
		this.rage3DScene.setLightIntensity(this.light, red, green, blue);
	}

	public SimpleVector getIntensity() {
		return this.rage3DScene.getLightIntensity(this.light);
	}

	public void setAttenuation(float att) {
		this.rage3DScene.setLightAttenuation(this.light, att);
	}

	public float getAttenuation() {
		return this.rage3DScene.getLightAttenuation(this.light);
	}

	public void setDiscardDistance(float dist) {
		if (dist < 0.0F) {
			dist = -1.0F;
		}
		this.rage3DScene.setLightDiscardDistance(this.light, dist);
	}

	public float getDiscardDistance() {
		return this.rage3DScene.getLightDiscardDistance(this.light);
	}

	public void setDistanceOverride(float distance) {
		if (distance < 0.0F) {
			distance = -1.0F;
		}
		this.rage3DScene.setLightDistanceOverride(this.light, distance);
	}

	public float getDistanceOverride() {
		return this.rage3DScene.getLightDistanceOverride(this.light);
	}

	public void setPosition(SimpleVector pos) {
		this.rage3DScene.setLightPosition(this.light, pos);
	}

	public SimpleVector getPosition() {
		return this.rage3DScene
				.getLightPosition(this.light, new SimpleVector());
	}

	public SimpleVector getPosition(SimpleVector store) {
		return this.rage3DScene.getLightPosition(this.light, store);
	}

	public void rotate(SimpleVector degrees, SimpleVector pivot) {
		SimpleVector pos = this.rage3DScene.getLightPosition(this.light,
				this.rotCache);
		pos.sub(pivot);
		pos.rotateX(degrees.x);
		pos.rotateY(degrees.y);
		pos.rotateZ(degrees.z);
		pos.add(pivot);
		setPosition(pos);
	}
}
