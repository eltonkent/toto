package toto.graphics._3D;

public final class Interact2D {
	private static final float VIEWPLANE_Z_VALUE = 1.0F;
	private static Matrix workMat = new Matrix();

	public static SimpleVector reproject2D3D(Camera camera, FrameBuffer buffer,
			int x, int y) {
		return reproject2D3D(camera, buffer, x, y, 1.0F);
	}

	public static SimpleVector reproject2D3D(Camera camera, FrameBuffer buffer,
			int x, int y, SimpleVector toFill) {
		return reproject2D3D(camera, buffer, x, y, 1.0F, toFill);
	}

	public static SimpleVector reproject2D3DWS(Camera camera,
			FrameBuffer buffer, int x, int y) {
		SimpleVector ray = reproject2D3D(camera, buffer, x, y, 1.0F);
		workMat.setIdentity();
		ray.matMul(camera.backMatrix.invert3x3(workMat));
		return ray;
	}

	public static SimpleVector reproject2D3DWS(Camera camera,
			FrameBuffer buffer, int x, int y, SimpleVector toFill) {
		SimpleVector ray = reproject2D3D(camera, buffer, x, y, 1.0F, toFill);
		workMat.setIdentity();
		ray.matMul(camera.backMatrix.invert3x3(workMat));
		return ray;
	}

	public static SimpleVector reproject2D3DWS(Camera camera,
			FrameBuffer buffer, int x, int y, float z) {
		SimpleVector ray = reproject2D3D(camera, buffer, x, y, z);
		workMat.setIdentity();
		ray.matMul(camera.backMatrix.invert3x3(workMat));
		return ray;
	}

	public static SimpleVector reproject2D3DWS(Camera camera,
			FrameBuffer buffer, int x, int y, float z, SimpleVector toFill) {
		SimpleVector ray = reproject2D3D(camera, buffer, x, y, z, toFill);
		workMat.setIdentity();
		ray.matMul(camera.backMatrix.invert3x3(workMat));
		return ray;
	}

	public static SimpleVector reproject2D3D(Camera camera, FrameBuffer buffer,
			int x, int y, float z) {
		return reproject2D3D(camera, buffer, x, y, z, SimpleVector.create());
	}

	public static SimpleVector reproject2D3D(Camera camera, FrameBuffer buffer,
			int x, int y, float z, SimpleVector toFill) {
		camera.calcFOV(buffer.width, buffer.height);
		float cmx = buffer.middleX + buffer.middleX * 2.0F
				* Config.viewportOffsetX;
		float cmy = buffer.middleY + buffer.middleY * 2.0F
				* Config.viewportOffsetY;

		float xf = (x - cmx) * z / camera.scaleX;
		float yf = (y - cmy) * z / camera.scaleY;
		toFill.set(xf, yf, z);
		return toFill;
	}

	public static SimpleVector project3D2D(Camera camera, FrameBuffer buffer,
			SimpleVector vertex) {
		return project3D2DInternal(camera, buffer, vertex, null, null);
	}

	public static synchronized SimpleVector project3D2D(Camera camera,
			FrameBuffer buffer, SimpleVector vertex, SimpleVector toFill) {
		return project3D2DInternal(camera, buffer, vertex, null, toFill);
	}

	public static SimpleVector projectCenter3D2D(FrameBuffer buffer,
			T3DObject obj) {
		return projectCenter3D2D(null, buffer, obj);
	}

	public static SimpleVector projectCenter3D2D(Camera camera,
			FrameBuffer buffer, T3DObject obj) {
		if ((camera == null) && (obj.myRage3DScene == null)) {
			Logger.log("Object doesn't belong to a world!", 0);
			return new SimpleVector();
		}

		if (camera == null) {
			camera = obj.myRage3DScene.camera;
		}
		return project3D2DInternal(camera, buffer, obj.getCenter(),
				obj.getWorldTransformation(), null);
	}

	static SimpleVector reproject2D3DBlit(float scaleX, float scaleY,
			FrameBuffer buffer, int x, int y, float z, SimpleVector fillMe) {
		if (fillMe == null) {
			fillMe = new SimpleVector();
		}
		float xf = (x - buffer.middleX) * z / scaleX;
		float yf = (y - buffer.middleY) * z / scaleY;
		fillMe.x = xf;
		fillMe.y = yf;
		fillMe.z = z;
		return fillMe;
	}

	private static SimpleVector project3D2DInternal(Camera camera,
			FrameBuffer buffer, SimpleVector vertex, Matrix mat,
			SimpleVector toFill) {
		Matrix mat2 = camera.backMatrix;
		Matrix mat3 = null;

		if (toFill != null) {
			mat3 = workMat;
			mat3.setIdentity();
		} else {
			mat3 = new Matrix();
			toFill = SimpleVector.create();
		}

		mat3.mat[3][0] = (-camera.backBx);
		mat3.mat[3][1] = (-camera.backBy);
		mat3.mat[3][2] = (-camera.backBz);

		if (mat != null)
			mat.matMul(mat3);
		else {
			mat = mat3;
		}

		mat.matMul(mat2);

		float s22 = mat.mat[2][2];
		float s12 = mat.mat[1][2];
		float s02 = mat.mat[0][2];

		float bz = mat.mat[3][2];

		float x1 = vertex.x;
		float y1 = vertex.y;
		float z1 = vertex.z;

		float p1z = x1 * s02 + y1 * s12 + z1 * s22 + bz;

		if (p1z > 0.0F) {
			float s00 = mat.mat[0][0];
			float s10 = mat.mat[1][0];
			float s11 = mat.mat[1][1];
			float s21 = mat.mat[2][1];
			float s20 = mat.mat[2][0];
			float s01 = mat.mat[0][1];

			float bx = mat.mat[3][0];
			float by = mat.mat[3][1];
			float p1x = x1 * s00 + y1 * s10 + z1 * s20 + bx;
			float p1y = x1 * s01 + y1 * s11 + z1 * s21 + by;
			float cmx = buffer.middleX + buffer.middleX * 2.0F
					* Config.viewportOffsetX;
			float cmy = buffer.middleY + buffer.middleY * 2.0F
					* Config.viewportOffsetY;

			camera.calcFOV(buffer.width, buffer.height);

			float sz = 1.0F / p1z;
			float sx = camera.scaleX * (p1x * sz) + cmx;
			float sy = camera.scaleY * (p1y * sz) + cmy;

			toFill.set(sx, sy, sz);
			return toFill;
		}
		toFill.set(0.0F, 0.0F, 0.0F);
		return null;
	}
}
