package toto.graphics._3D;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Manager for storing and retrieving {@link toto.graphics._3D.Texture}s. Each
 * texture stored is given a unique name.
 */
public final class TextureCache {
	public static final int TEXTURE_NOTFOUND = -1;
	static final String DUMMY_NAME = "--dummy--";
	Texture[] textures;
	private Texture dummy;
	private static TextureCache myInstance = null;
	private int textureCount;
	private HashMap<String, Integer> textureList;
	private int lastID = -1;

	private String lastName = null;

	private Virtualizer textureVirtualizer = null;

	private TextureCache() {
		flush();
	}

	public static synchronized TextureCache getInstance() {
		if (myInstance == null) {
			myInstance = new TextureCache();
		}
		return myInstance;
	}

	public List<?> getState() {
		ArrayList t = new ArrayList();
		for (int i = 0; i < this.textureCount; i++) {
			Texture tex = this.textures[i];
			String name = getNameByID(i);
			Object[] tmp = new Object[2];
			tmp[0] = name;
			tmp[1] = tex;
			t.add(tmp);
		}
		return t;
	}

	public void setState(List<?> dump) {
		flush();
		try {
			for (int i = 0; i < dump.size(); i++) {
				Object[] tmp = (Object[]) dump.get(i);
				String name = (String) tmp[0];
				Texture tex = (Texture) tmp[1];
				if (!name.equals("--dummy--"))
					addTexture(name, tex);
			}
		} catch (Exception e) {
			Logger.log("Not a valid dump!", 0);
		}
	}

	public void addTexture(String name) {
		addTexture(name, this.dummy);
	}

	public synchronized void addTexture(String name, Texture tex) {
		if (this.textureCount >= this.textures.length) {
			Texture[] newy = new Texture[this.textures.length * 2];
			System.arraycopy(this.textures, 0, newy, 0, this.textures.length);
			this.textures = newy;
		}
		if (!this.textureList.containsKey(name)) {
			this.textureList.put(name, IntegerC.valueOf(this.textureCount));
			this.textures[this.textureCount] = tex;
			this.textureCount += 1;
		} else {
			Logger.log("A texture with the name '" + name
					+ "' has been declared twice!", 0);
		}
	}

	Texture[] getTextures() {
		/* 146 */
		return this.textures;
		/*     */
	}

	public void replaceTexture(String name, Texture tex) {
		int pos = getTextureID(name);
		if (pos != -1)
			this.textures[pos] = tex;
		else
			Logger.log("Texture '" + tex + "' not found!", 0);
	}

	public long getMemoryUsage() {
		long mem = 0L;
		for (int i = 0; i < this.textureCount; i++) {
			Texture tex = this.textures[i];
			if (tex.texels != null) {
				mem += tex.texels.length * 4;
			}
			if (tex.zippedTexels != null) {
				mem += tex.zippedTexels.length;
			}
		}
		return mem;
	}

	public void unloadTexture(FrameBuffer from, Texture texture) {
		if (from.glRend != null)
			from.glRend.addForUnload(texture);
	}

	public synchronized void removeTexture(String name) {
		Texture texture = getTexture(name);
		for (int i = 0; i < this.textureCount; i++)
			if (texture == this.textures[i]) {
				this.textureList.remove(name);
				this.textures[i] = this.dummy;
				if (i == this.textureCount - 1)
					this.textureCount -= 1;
			}
	}

	public void removeAndUnload(String name, FrameBuffer from) {
		unloadTexture(from, getTexture(name));
		removeTexture(name);
	}

	public void flush() {
		this.textureList = new HashMap();
		this.textures = new Texture[Config.maxTextures];
		this.dummy = new Texture();
		this.textureCount = 0;
		addTexture("--dummy--", this.dummy);
	}

	public void compress() {
		for (int i = 0; i < this.textures.length; i++)
			if (this.textures[i] != null)
				this.textures[i].compress();
	}

	public void preWarm(FrameBuffer buffer) {
		boolean doneSomething = false;

		if (buffer.glRend != null) {
			GLRenderer renderer = buffer.glRend;
			for (int i = 0; i < this.textureCount; i++) {
				Texture t = this.textures[i];
				if ((t != null)
						&& ((t.texels != null) || (t.zippedTexels != null) || ((getVirtualizer() != null) && (getVirtualizer()
								.isVirtual(t))))) {
					renderer.upload(t);
					doneSomething = true;
				}
			}
		}

		if (doneSomething)
			Logger.log("Pre-warming done!");
	}

	public void setDummyTexture(Texture texture) {
		if (texture != null) {
			this.dummy = texture;
			replaceTexture("--dummy--", texture);
		} else {
			Logger.log("Texture can't be null!", 0);
		}
	}

	public Texture getDummyTexture()
	/*     */{
		/* 336 */
		return this.dummy;
		/*     */
	}

	public synchronized boolean containsTexture(String name) {
		return this.textureList.containsKey(name);
	}

	public int getTextureCount() {
		return this.textureList.size();
	}

	public synchronized int getTextureID(String name) {
		if (name.equals(this.lastName)) {
			return this.lastID;
		}
		Object ti = this.textureList.get(name);
		if (ti != null) {
			this.lastID = ((Integer) ti).intValue();
			this.lastName = name;
			return this.lastID;
		}
		return -1;
	}

	public Texture getTextureByID(int id) {
		return getTexture(getNameByID(id));
	}

	public Texture getTexture(String name) {
		if (name != null) {
			int i = getTextureID(name);
			if (i != -1) {
				return this.textures[i];
			}
		}

		Logger.log("Requested texture not found!", 0);
		return null;
	}

	public synchronized HashSet<String> getNames() {
		return new HashSet(this.textureList.keySet());
	}

	synchronized void flushOpenGLIDs(int renderer) {
		for (String st : this.textureList.keySet()) {
			Texture t = getTexture(st);
			t.clearIDs(renderer);
		}
	}

	public synchronized String getNameByID(int id) {
		for (String tname : this.textureList.keySet()) {
			if (((Integer) this.textureList.get(tname)).intValue() == id) {
				return tname;
			}
		}
		return null;
	}

	public void setVirtualizer(Virtualizer textureVirtualizer)
	/*     */{
		/* 453 */
		this.textureVirtualizer = textureVirtualizer;
		/*     */
	}

	public Virtualizer getVirtualizer()
	/*     */{
		/* 462 */
		return this.textureVirtualizer;
		/*     */
	}

	public void virtualize(Texture tex) {
		if (this.textureVirtualizer == null) {
			Logger.log("No Virtualizer setKey!", 0);
			return;
		}

		if (tex.myEffect == null)
			this.textureVirtualizer.store(tex);
	}
}
