package toto.graphics._3D;

class GenericContainer implements Comparable<GenericContainer> {
	private int[] content = null;
	private int pos = 0;
	private int hash = 0;

	public GenericContainer() {
		this.content = new int[4];
	}

	public GenericContainer(int cnt) {
		this.content = new int[cnt];
	}

	public void add(float f) {
		add(Float.floatToRawIntBits(f));
	}

	public void clear() {
		this.pos = 0;
		this.hash = 0;
	}

	public void add(int val) {
		if (this.pos == this.content.length) {
			int[] newy = new int[this.content.length * 2];
			for (int i = 0; i < this.pos; i++) {
				newy[i] = this.content[i];
			}
			this.content = newy;
		}
		this.content[this.pos] = val;
		this.pos += 1;
		if ((this.pos & 0x1) == 1)
			this.hash += val;
		else
			this.hash -= val;
	}

	public boolean equals(Object o) {
		if ((o instanceof GenericContainer)) {
			GenericContainer io = (GenericContainer) o;
			if ((this.pos != io.pos) || (this.hash != io.hash)) {
				return false;
			}
			for (int i = 0; i < this.pos; i++) {
				if (this.content[i] != io.content[i]) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public int hashCode() {
		return this.hash;
	}

	public String toString() {
		return String.valueOf(hashCode());
	}

	public int compareTo(GenericContainer io) {
		if (this.pos != io.pos) {
			return this.pos - io.pos;
		}
		for (int i = 0; i < this.pos; i++) {
			int dif = this.content[i] - io.content[i];
			if (dif != 0) {
				return dif;
			}
		}
		return 0;
	}
}
