package toto.graphics._3D;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.EGLConfigChooser;

/*     */
/*     */
/*     */
/*     */
/*     */

public class NVDepthConfigChooser implements EGLConfigChooser {
	private GLSurfaceView view = null;
	private boolean withAlpha = false;
	private int depth = 24;

	public NVDepthConfigChooser(GLSurfaceView view) {
		this.view = view;
	}

	public NVDepthConfigChooser(GLSurfaceView view, boolean withAlpha) {
		this.view = view;
		this.withAlpha = withAlpha;
	}

	public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
		int greenSize = this.withAlpha ? 5 : 6;
		int alphaSize = this.withAlpha ? 1 : 0;
		int redSize = 5;
		int blueSize = 5;
		try {
			this.view.setEGLContextClientVersion(2);
		} catch (IllegalStateException localIllegalStateException) {
		} catch (Throwable t) {
			Logger.log("Couldn't initialize OpenGL ES 2.0", 0);
			return null;
		}

		int[] val = new int[1];

		int EGL_DEPTH_ENCODING_NV = 12514;
		int EGL_DEPTH_ENCODING_NONLINEAR_NV = 12515;

		int[] configSpec = { 12324, redSize, 12323, greenSize, 12322, blueSize,
				12321, alphaSize, 12325, 16, 12352, 4, 12514, 12515, 12344 };

		int numConfigs = 0;

		if (egl.eglChooseConfig(display, configSpec, null, 0, val)) {
			numConfigs = val[0];
		}

		if (numConfigs <= 0) {
			Logger.log("No nonlinear depth buffer config found...using default mode!");
			configSpec = new int[] { 12324, redSize, 12323, greenSize, 12322,
					blueSize, 12321, alphaSize, 12325, this.depth, 12352, 4,
					12344 };

			if (!egl.eglChooseConfig(display, configSpec, null, 0, val)) {
				Logger.log("No depth buffer config for 24bit found...using 16bit !");
				configSpec = new int[] { 12324, redSize, 12323, greenSize,
						12322, blueSize, 12321, alphaSize, 12325, 16, 12352, 4,
						12344 };

				if (!egl.eglChooseConfig(display, configSpec, null, 0, val)) {
					error();
				}
			}
			numConfigs = val[0];

			if (numConfigs <= 0)
				error();
		} else {
			Logger.log("Nonlinear depth buffer enabled!");
		}

		EGLConfig[] configs = new EGLConfig[numConfigs];
		if (!egl.eglChooseConfig(display, configSpec, configs, numConfigs, val)) {
			error();
		}

		int index = -1;
		for (int i = 0; i < configs.length; i++) {
			if (findConfigAttrib(egl, display, configs[i], 12324, 0) == 5) {
				index = i;
				break;
			}
		}
		if (index == -1) {
			Logger.log("Unable to find a matching config...using default!");
			index = 0;
		}
		EGLConfig config = configs.length > 0 ? configs[index] : null;
		if (config == null) {
			error();
		}
		return config;
	}

	private void error() {
		Logger.log("Failed to choose config!", 0);
	}

	private int findConfigAttrib(EGL10 egl, EGLDisplay display,
			EGLConfig config, int attribute, int defaultValue) {
		int[] val = new int[1];
		if (egl.eglGetConfigAttrib(display, config, attribute, val)) {
			return val[0];
		}
		return defaultValue;
	}
}
