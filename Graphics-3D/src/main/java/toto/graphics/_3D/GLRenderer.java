/*      */
package toto.graphics._3D;

/*      */
/*      */

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import toto.graphics.color.RGBColor;
import android.annotation.SuppressLint;

@SuppressLint({ "DefaultLocale" })
/*      */final class GLRenderer
/*      */{
	/* 23 */float[] lineCol = new float[4];
	/*      */
	/* 25 */boolean disposed = false;
	/*      */int myID;
	/* 29 */private static float COLOR_INV = 0.003921569F;
	/*      */private static final int VERTEX_ARRAY_SIZE = 600;
	/*      */private static final boolean AGGREGATE_BLITS = true;
	/* 35 */private static int[] stageMap = { 33984, 33985, 33986, 33987 };
	/* 37 */private static int[] modeMap = { 8448, 8448, 260, 7681, 3042 };
	/* 39 */private static int rendererID = 0;
	/*      */private float lastFOV;
	/* 43 */private boolean lastFOVMode = Config.autoMaintainAspectRatio;
	/*      */
	/* 45 */private boolean init = false;
	/*      */
	/* 47 */private int stateChanges = 0;
	/*      */
	/* 49 */private T3DScene myRage3DScene = null;
	/*      */private IntBuffer pixelBuffer;
	/* 53 */private int pixelBufferSize = 0;
	/*      */private Texture blitBuffer;
	/* 57 */private int blitBufferWidth = 0;
	/*      */
	private int blitBufferHeight = 0;
	private int currentRGBScaling = 1;
	private TextureCache texMan = null;
	private boolean vertexArraysInitialized = false;
	private IPaintListener listener = null;
	private boolean listenerActive = true;
	private boolean[] stageInitialized = new boolean[4];
	int[] lastTextures = new int[4];
	private int maxStages = 0;
	private int[] lastMultiTextures = new int[4];
	private int[] lastMultiModes = new int[4];
	private int[] lastMode = new int[4];
	private int minDriverAndConfig = 0;
	private float lastFarPlane = -999.0F;
	private IntBuffer colors = null;
	private int[] colorArray = null;
	private IntBuffer vertices = null;
	private int[] vertexArray = null;
	private IntBuffer textures = null;
	private int[] textureArray = null;
	private ShortBuffer indices = null;
	private short[] indexArray = null;

	private Texture renderTarget = null;
	private int yTargetStart = 0;
	private Matrix textureScale = new Matrix();
	private boolean[] enabledStages = new boolean[4];
	private boolean singleTexturing = true;
	private int currentFogColor = -1;
	private boolean currentFoggingState = false;
	private float currentFogDistance = -1.0F;
	private IntBuffer[] smallBuffer = new IntBuffer[3];
	private SimpleVector blitCoords1 = new SimpleVector();
	private SimpleVector blitCoords2 = new SimpleVector();
	private boolean depthBuffer = false;
	private boolean blitMode = false;
	private boolean blitTrans = false;
	private boolean blitAdditive = false;
	private boolean scissorEnabled = false;
	private boolean scissorClearAll = true;
	private int blitScaling = 0;
	private HashSet<Texture> toUnload = new HashSet();
	protected HashMap<T3DObject, float[]> matrixCache = new HashMap();
	private boolean lineMode = false;
	GL10 gl10 = null;
	GL11 gl11 = null;
	boolean gl20 = false;

	private boolean blending = false;
	private float lastNearPlane;
	private int curPos;
	private int colPos;
	private int vertPos;
	private int texPos;
	private int indexPos;
	private float[] ambient = new float[4];
	private float[] cols = new float[4];

	protected float[] dumpy = new float[16];

	private List<Integer> vbos = new ArrayList();

	private boolean textureMatrixSet = false;

	private int lastActivatedStage = -1;

	private boolean hasToReEnable = false;

	private boolean mipmapsByGpu = false;

	private Set<Texture> uploadedTextures = new HashSet();

	private GL20Handler gl20Handler = null;

	private FloatBuffer fogColors = ByteBuffer.allocateDirect(16)
			.order(ByteOrder.nativeOrder()).asFloatBuffer();

	private int lastWorldHash = 0;

	private Matrix tmpMat = new Matrix();
	private Matrix moMat = new Matrix();
	private float[] cameraMatrix = new float[16];
	private GLSLShader lineColorShader = null;

	private String extensions = null;

	GLRenderer() {

		float[] mf = { 0.5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.5F, 0.0F, 0.0F, 0.0F,
				0.0F, 0.5F, 0.0F, 0.5F, 0.5F, 0.5F, 1.0F };

		this.textureScale.setDump(mf);
		/*      */
		/* 195 */
		resetStates();
		/*      */
		/* 197 */
		this.myID = rendererID;
		/* 198 */
		rendererID += 1;
		/*      */
		/* 200 */
		this.lastFOV = -999.0F;
		/* 201 */
		this.lastFarPlane = -999.0F;
		/* 202 */
		this.lastNearPlane = -999.0F;
		/* 203 */
		this.init = false;
		/* 204 */
		this.stateChanges = 0;
		/* 205 */
		this.pixelBuffer = null;
		/* 206 */
		this.pixelBufferSize = 0;
		/* 207 */
		this.blitBuffer = null;
		/* 208 */
		this.blitBufferWidth = 0;
		/* 209 */
		this.blitBufferHeight = 0;
		/* 210 */
		this.texMan = TextureCache.getInstance();
		/* 211 */
		if (Logger.isDebugEnabled())
			/* 212 */
			Logger.log("GLRenderer created with id " + this.myID + " on "
					+ Thread.currentThread(), 3);
		/*      */
	}

	/*      */
	/*      */
	private void resetStates()
	/*      */{
		/* 217 */
		for (int i = 0; i < 4; i++) {
			/* 218 */
			this.stageInitialized[i] = false;
			/* 219 */
			this.enabledStages[i] = false;
			/* 220 */
			this.lastTextures[i] = 0;
			/* 221 */
			this.lastMultiTextures[i] = 0;
			/* 222 */
			this.lastMultiModes[i] = 0;
			/* 223 */
			this.lastMode[i] = -1;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	public final void setPaintListener(IPaintListener listener) {
		/* 228 */
		this.listener = listener;
		/*      */
	}

	/*      */
	/*      */
	public final boolean isInitialized() {
		/* 232 */
		return this.init;
		/*      */
	}

	/*      */
	/*      */
	public final void registerVBO(int id) {
		/* 236 */
		synchronized (this) {
			/* 237 */
			this.vbos.add(IntegerC.valueOf(id));
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	public final void unregisterVBO(int id) {
		/* 242 */
		synchronized (this) {
			/* 243 */
			this.vbos.remove(IntegerC.valueOf(id));
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private void unsetBlendingMode() {
		/* 248 */
		this.gl10.glDisable(3042);
		/*      */
	}

	/*      */
	/*      */
	private void setDepthBuffer() {
		if (this.renderTarget == null)
			/* 253 */this.gl10.glDepthMask(false);
	}

	private void setBlendingMode(TransparencyMode mode)
	/*      */{
		switch (mode) {
		case DEFAULT:
			this.gl10.glEnable(3042);
			this.gl10.glBlendFunc(770, 771);
			break;
		case ADD:
			this.gl10.glEnable(3042);
			this.gl10.glBlendFunc(770, 1);
		}
	}

	private void doPostProcessing(FrameBuffer fb, IPostProcessor post)
	/*      */{
		/* 273 */
		if (!post.isInitialized()) {
			/* 274 */
			post.init(fb);
			/*      */
		}
		/*      */
		/* 277 */
		int scaleTmp = this.currentRGBScaling;
		/* 278 */
		disableAllHigherStages();
		/*      */
		/* 281 */
		enableStage(0);
		/*      */
		/* 283 */
		setRGBScaling(1);
		/*      */
		/* 285 */
		if (this.renderTarget != null) {
			/* 286 */
			resetViewport(fb);
			/*      */
		}
		/*      */
		/* 289 */
		this.lastTextures[0] = -1;
		/* 290 */
		switchTextureMode(0, modeMap[0]);
		/* 291 */
		post.process();
		/*      */
		/* 293 */
		if (this.renderTarget != null) {
			/* 294 */
			setViewport(fb);
			/*      */
		}
		/* 296 */
		setRGBScaling(scaleTmp);
		/*      */
	}

	/*      */
	/*      */
	private void resetTextureStates() {
		/* 300 */
		for (int i = 0; i < 4; i++)
			/* 301 */
			this.lastTextures[i] = -1;
		/*      */
	}

	/*      */
	/*      */
	private boolean renderToTarget()
	/*      */{
		/* 306 */
		if (this.renderTarget == null) {
			/* 307 */
			return false;
			/*      */
		}
		/*      */
		/* 310 */
		if ((this.gl20) && (Config.useFBO)) {
			/* 311 */
			return true;
			/*      */
		}
		/*      */
		/* 314 */
		disableUnusedStages();
		/* 315 */
		switchTextureMode(0, modeMap[0]);
		/*      */
		/* 317 */
		int scaleTmp = this.currentRGBScaling;
		/* 318 */
		setRGBScaling(1);
		/*      */
		/* 320 */
		if (this.renderTarget.getOpenGLID(this.myID) == 0) {
			/* 321 */
			this.renderTarget.setMarker(this.myID, Texture.MARKER_NOTHING);
			/* 322 */
			convertTexture(this.renderTarget);
			/* 323 */
			this.lastTextures[0] = -1;
			/*      */
		}
		/*      */
		/* 326 */
		int tid = this.renderTarget.getOpenGLID(this.myID);
		/* 327 */
		bindTexture(0, tid);
		/*      */
		/* 329 */
		if (Logger.isDebugEnabled()) {
			/* 330 */
			Logger.log("Copy data from framebuffer into render target...", 3);
			/*      */
		}
		/*      */
		/* 333 */
		if (Config.renderTargetsAsSubImages)
			/* 334 */
			this.gl10
					.glCopyTexSubImage2D(3553, 0, 0, 0, 0, this.yTargetStart,
							this.renderTarget.getWidth(),
							this.renderTarget.getHeight());
		/*      */
		else {
			/* 336 */
			this.gl10.glCopyTexImage2D(3553, 0, 6407, 0, this.yTargetStart,
					this.renderTarget.getWidth(),
					this.renderTarget.getHeight(), 0);
			/*      */
		}
		/*      */
		/* 339 */
		setRGBScaling(scaleTmp);
		/*      */
		/* 341 */
		if (Logger.isDebugEnabled()) {
			/* 342 */
			Logger.log("...success!", 3);
			/*      */
		}
		/*      */
		/* 345 */
		return true;
		/*      */
	}

	/*      */
	/*      */
	private void disableAllHigherStages() {
		/* 349 */
		for (int i = 1; i < this.maxStages; i++)
			/* 350 */
			disableStage(i);
		/*      */
	}

	/*      */
	/*      */
	public final void init(GL10 glContext, int x, int y)
	/*      */{
		/* 355 */
		init(glContext, x, y, glContext == null);
		/*      */
	}

	/*      */
	/*      */
	private final void init(GL10 glContext, int x, int y, boolean openGLES20) {
		/* 359 */
		if (openGLES20)
		/*      */{
			/*      */
			try
			/*      */{
				/* 364 */
				Class clazz = Class.forName("rage.graphics._3D.GL20");
				/* 365 */
				glContext = (GL10) clazz.newInstance();
				/*      */
			} catch (Exception e) {
				/* 367 */
				Logger.log(e);
				/*      */
			}
			/*      */
		}
		/*      */
		/* 371 */
		if ((glContext instanceof GL20Handler)) {
			/* 372 */
			this.gl20 = true;
			/* 373 */
			this.gl20Handler = ((GL20Handler) glContext);
			/*      */
		}
		/*      */
		/* 376 */
		if (Config.glDebugLevel != 0) {
			/* 377 */
			glContext = (GL10) GLDebug.create(glContext);
			/*      */
		}
		/*      */
		/* 380 */
		this.gl10 = glContext;
		/* 381 */
		if ((this.gl10 instanceof GL11)) {
			/* 382 */
			this.gl11 = ((GL11) glContext);
			/*      */
		}
		/*      */
		/* 385 */
		this.gl10.glFinish();
		/* 386 */
		this.gl10.glFlush();
		/*      */
		/* 388 */
		this.gl10.glViewport(0, 0, x, y);
		/* 389 */
		this.gl10.glMatrixMode(5889);
		/* 390 */
		this.gl10.glLoadIdentity();
		/* 391 */
		this.gl10.glMatrixMode(5888);
		/* 392 */
		this.gl10.glLoadIdentity();
		/*      */
		/* 394 */
		this.gl10.glShadeModel(7425);
		/* 395 */
		this.gl10.glClearDepthf(1.0F);
		/* 396 */
		enableDepthBuffer();
		/* 397 */
		this.gl10.glDepthFunc(515);
		/*      */
		/* 399 */
		if (Config.glDither)
			/* 400 */this.gl10.glEnable(3024);
		/*      */
		else {
			/* 402 */
			this.gl10.glDisable(3024);
			/*      */
		}
		/*      */
		/* 410 */
		initTextureStage(0);
		/*      */
		/* 412 */
		this.lastFOV = -999.0F;
		/* 413 */
		this.lastFarPlane = -999.0F;
		/* 414 */
		this.lastNearPlane = -999.0F;
		/*      */
		/* 416 */
		if (!openGLES20)
			/* 417 */this.minDriverAndConfig = Math.min(getTextureStages(),
					Config.maxTextureLayers);
		/*      */
		else {
			/* 419 */
			this.minDriverAndConfig = Math.min(4, Config.maxTextureLayers);
			/*      */
		}
		/*      */
		/* 422 */
		Config.glStageCount = this.minDriverAndConfig;
		/*      */
		/* 424 */
		int raw = getTextureStagesRaw();
		/*      */
		/* 426 */
		Logger.log("OpenGL vendor:     " + this.gl10.glGetString(7936), 2);
		/* 427 */
		Logger.log("OpenGL renderer:   " + this.gl10.glGetString(7937), 2);
		/* 428 */
		Logger.log("OpenGL version:    " + this.gl10.glGetString(7938), 2);
		/* 429 */
		Logger.log("OpenGL renderer initialized (using "
				+ this.minDriverAndConfig + "/" + raw + " texture stages)", 2);
		/*      */
		/* 431 */
		this.gl10.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
		/* 432 */
		this.gl10.glClear(16640);
		/*      */
		/* 434 */
		this.mipmapsByGpu = ((this.gl10.glGetString(7939).indexOf(
				"generate_mipmap") != -1) || (this.gl10.glGetString(7938)
				.indexOf("1.1") != -1));
		/*      */
		/* 436 */
		this.init = true;
		/*      */
	}

	/*      */
	/*      */
	public void dispose() {
		/* 440 */
		if (!this.disposed) {

			this.disposed = true;

			this.init = false;

			this.lastFOV = -999.0F;

			this.lastFarPlane = -999.0F;

			this.lastNearPlane = -999.0F;

			this.pixelBuffer = null;

			this.blitBuffer = null;

			this.listener = null;

			this.myRage3DScene = null;

			Texture[] tex = TextureCache.getInstance().textures;

			for (int i = 0; i < tex.length; i++)

				try {

					removeTexture(tex[i]);

				} catch (Exception localException) {

				}

			unloadKnownTextures();

			this.texMan.flushOpenGLIDs(this.myID);

			if ((this.gl11 != null) && (Config.useVBO)) {

				Logger.log("Disposing VBOs!", 2);

				List<Integer> copy = new ArrayList(this.vbos);

				synchronized (this) {

					for (Integer vboId : copy) {

						deleteBuffer(vboId.intValue());

					}

				}

				copy = null;

				this.vbos.clear();

			}

			Logger.log("Renderer disposed!", 2);

			if (Logger.isDebugEnabled())

				Logger.log("GLRenderer disposed with id " + this.myID + " on "
						+ Thread.currentThread(), 3);

		}
	}

	public void deleteBuffer(int bufferId) {

		if (bufferId != 0) {

			int[] buffer = new int[1];

			buffer[0] = bufferId;

			this.gl11.glDeleteBuffers(1, buffer, 0);

			unregisterVBO(bufferId);

		}

	}

	/*      */
	/*      */
	public void resetShader() {
		/* 492 */
		if ((this.gl20) && (!this.lineMode))
			/* 493 */this.gl20Handler.reset();
		/*      */
	}

	/*      */
	/*      */
	public void setShader(GLSLShader shader)
	/*      */{
		/* 498 */
		if ((this.gl20) && (!this.lineMode))
			/* 499 */this.gl20Handler.setShader(shader);
		/*      */
	}

	/*      */
	/*      */
	public void clearShader()
	/*      */{
		/* 504 */
		if ((this.gl20) && (!this.lineMode))
			/* 505 */this.gl20Handler.clearShader();
		/*      */
	}

	/*      */
	/*      */
	public GLSLShader initShader()
	/*      */{
		/* 510 */
		if (this.gl20) {
			/* 511 */
			return this.gl20Handler.updateShaderData();
			/*      */
		}
		/* 513 */
		return null;
		/*      */
	}

	/*      */
	/*      */
	public void closeShader() {
		/* 517 */
		if (this.gl20)
			/* 518 */this.gl20Handler.resetShaderData();
		/*      */
	}

	/*      */
	/*      */
	public void setTangents(Buffer tangents)
	/*      */{
		/* 523 */
		if (this.gl20)
			/* 524 */this.gl20Handler.setTangents(tangents);
		/*      */
	}

	/*      */
	/*      */
	public void setTangents(int tangentsId)
	/*      */{
		/* 529 */
		if (this.gl20)
			/* 530 */this.gl20Handler.setTangents(tangentsId);
		/*      */
	}

	/*      */
	/*      */
	public void bindVertexAttributes(VertexAttributes vas, Buffer attributes)
	/*      */{
		/* 535 */
		if (this.gl20)
			/* 536 */this.gl20Handler.bindVertexAttributes(vas.name, vas.type,
					attributes);
		/*      */
	}

	/*      */
	/*      */
	public void bindVertexAttributes(VertexAttributes vas, int attributesId)
	/*      */{
		/* 541 */
		if (this.gl20)
			/* 542 */this.gl20Handler.bindVertexAttributes(vas.name, vas.type,
					attributesId);
		/*      */
	}

	/*      */
	/*      */
	public void unbindVertexAttributes(VertexAttributes vas, Buffer attributes)
	/*      */{
		/* 547 */
		if (this.gl20)
			/* 548 */this.gl20Handler.unbindVertexAttributes(vas.name,
					vas.type, attributes);
		/*      */
	}

	/*      */
	/*      */
	public void unbindVertexAttributes(VertexAttributes vas, int attributesId)
	/*      */{
		/* 553 */
		if (this.gl20)
			/* 554 */this.gl20Handler.unbindVertexAttributes(vas.name,
					vas.type, attributesId);
		/*      */
	}

	/*      */
	/*      */
	public void clearTangents()
	/*      */{
		/* 559 */
		if (this.gl20)
			/* 560 */this.gl20Handler.clearTangents();
		/*      */
	}

	/*      */
	/*      */
	public void clearTangents(int tangentsId)
	/*      */{
		/* 565 */
		if (this.gl20)
			/* 566 */this.gl20Handler.clearTangents(tangentsId);
		/*      */
	}

	/*      */
	/*      */void clearStageFlag()
	/*      */{
		/* 571 */
		this.lastActivatedStage = -1;
		/*      */
	}

	/*      */
	/*      */
	private final void initTextureStage(int stage, int mode) {
		/* 575 */
		switchTextureMode(stage, mode);
		/* 576 */
		this.stageInitialized[stage] = true;
		/*      */
	}

	/*      */
	/*      */
	private final void disableUnusedStages() {
		/* 580 */
		if (!this.singleTexturing) {
			/* 581 */
			for (int i = 1; i < this.maxStages; i++) {
				/* 582 */
				if (this.enabledStages[i]) {
					/* 583 */
					activateStage(i);
					/* 584 */
					this.gl10.glDisable(3553);
					/* 585 */
					this.enabledStages[i] = false;
					/*      */
				}
				/*      */
			}
			/* 588 */
			enableStage(0);
			/* 589 */
			this.singleTexturing = true;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private final void enableStage(int stage) {
		/* 594 */
		activateStage(stage);
		/* 595 */
		if (!this.enabledStages[stage]) {
			/* 596 */
			this.gl10.glEnable(3553);
			/* 597 */
			this.enabledStages[stage] = true;
			/* 598 */
			if (stage > 0)
				/* 599 */this.singleTexturing = false;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private void activateStage(int stage)
	/*      */{
		/* 605 */
		if (this.lastActivatedStage != stage) {
			/* 606 */
			this.lastActivatedStage = stage;
			/* 607 */
			this.gl10.glActiveTexture(stageMap[stage]);
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private final void disableStage(int stage) {
		/* 612 */
		if ((this.enabledStages[stage]) && (!this.singleTexturing)) {
			/* 613 */
			activateStage(stage);
			/* 614 */
			this.gl10.glDisable(3553);
			/* 615 */
			this.enabledStages[stage] = false;
			/* 616 */
			this.singleTexturing = true;
			/* 617 */
			for (int i = 1; i < this.maxStages; i++)
				/* 618 */
				if (this.enabledStages[i]) {
					/* 619 */
					this.singleTexturing = false;
					/* 620 */
					break;
					/*      */
				}
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private void bindAndProject(int stage, Texture texture)
	/*      */{
		/* 627 */
		int texID = texture.getOpenGLID(this.myID);
		/*      */
		/* 629 */
		if (texID != this.lastTextures[stage]) {
			/* 630 */
			bindTexture(stage, texID);
			/*      */
		}
		/* 632 */
		else if (stage != 0)
			/* 633 */enableStage(stage);
		/*      */
	}

	/*      */
	/*      */
	private final void enableCompiledPipeline()
	/*      */{
		/* 639 */
		CompiledInstance.lastVertexBuffer = null;
		/* 640 */
		this.gl10.glEnable(2977);
		/* 641 */
		this.gl10.glEnable(2896);
		/* 642 */
		this.gl10.glEnable(2884);
		/*      */
	}

	/*      */
	/*      */
	private final void disableCompiledPipeline() {
		/* 646 */
		CompiledInstance.lastVertexBuffer = null;
		/* 647 */
		this.gl10.glDisable(2884);
		/* 648 */
		this.gl10.glDisable(2896);
		/* 649 */
		this.gl10.glDisable(2977);
		/*      */
	}

	/*      */
	/*      */
	protected final void bindTexture(int stage, int texID) {
		/* 653 */
		enableStage(stage);
		/* 654 */
		this.gl10.glBindTexture(3553, texID);
		/* 655 */
		this.stateChanges += 1;
		/* 656 */
		this.lastTextures[stage] = texID;
		/*      */
	}

	/*      */
	/*      */
	private final void switchTextureMode(int stage, int mode) {
		/* 660 */
		if (this.lastMode[stage] != mode) {
			/* 661 */
			enableStage(stage);
			/* 662 */
			this.gl10.glTexEnvx(8960, 8704, mode);
			/* 663 */
			this.lastMode[stage] = mode;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	final void removeTexture(Texture t) {
		/* 668 */
		if ((t != null) && (t.getOpenGLID(this.myID) != 0)) {
			/* 669 */
			IntBuffer buf = getSmallBuffer(0);
			/* 670 */
			int tid = t.getOpenGLID(this.myID);
			/* 671 */
			buf.put(tid);
			/* 672 */
			buf.flip();
			/*      */
			try {
				/* 674 */
				this.gl10.glDeleteTextures(1, buf);
				/*      */
			} catch (Throwable t2) {
				/* 676 */
				Logger.log(
						"Failed to unload texture due to: " + t2.getMessage(),
						1);
				/*      */
			}
			/* 678 */
			if (Logger.isDebugEnabled()) {
				/* 679 */
				Logger.log("Unloaded texture: " + tid, 3);
				/*      */
			}
			/*      */
		}
		/* 682 */
		this.uploadedTextures.remove(t);
		/*      */
	}

	/*      */
	/*      */
	protected void addForUnload(Texture texture) {
		/* 686 */
		synchronized (this) {
			/* 687 */
			this.toUnload.add(texture);
			/* 688 */
			if (Config.unloadImmediately)
				/* 689 */unloadTextures();
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private final IntBuffer getSmallBuffer(int num)
	/*      */{
		/* 695 */
		IntBuffer smallBuffer = this.smallBuffer[num];
		/* 696 */
		if (smallBuffer == null) {
			/* 697 */
			smallBuffer = ByteBuffer.allocateDirect(4)
					.order(ByteOrder.nativeOrder()).asIntBuffer();
			/* 698 */
			this.smallBuffer[num] = smallBuffer;
			/*      */
		} else {
			/* 700 */
			smallBuffer.clear();
			/*      */
		}
		/* 702 */
		return smallBuffer;
		/*      */
	}

	/*      */
	/*      */
	private final void disableFogging() {
		/* 706 */
		this.gl10.glDisable(2912);
		/* 707 */
		this.currentFoggingState = false;
		/* 708 */
		this.currentFogColor = -1;
		/* 709 */
		this.currentFogDistance = -1.0F;
		/*      */
	}

	/*      */
	/*      */
	private final void setRGBScaling(int scaling) {
		/* 713 */
		if (scaling != this.currentRGBScaling) {
			/* 714 */
			enableStage(0);
			/* 715 */
			this.gl10.glTexEnvx(8960, 8704, 34160);
			/* 716 */
			this.gl10.glTexEnvx(8960, 34161, 8448);
			/* 717 */
			this.gl10.glTexEnvx(8960, 34163, scaling);
			/* 718 */
			this.currentRGBScaling = scaling;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private final void enableFogging(float start, float distance, float r,
			float g, float b)
	/*      */{
		/* 724 */
		if (r < 0.0F) {
			/* 725 */
			r = 0.0F;
			/*      */
		}
		/* 727 */
		else if (r > 255.0F) {
			/* 728 */
			r = 255.0F;
			/*      */
		}
		/*      */
		/* 731 */
		if (g < 0.0F) {
			/* 732 */
			g = 0.0F;
			/*      */
		}
		/* 734 */
		else if (g > 255.0F) {
			/* 735 */
			g = 255.0F;
			/*      */
		}
		/*      */
		/* 738 */
		if (b < 0.0F) {
			/* 739 */
			b = 0.0F;
			/*      */
		}
		/* 741 */
		else if (b > 255.0F) {
			/* 742 */
			b = 255.0F;
			/*      */
		}
		/*      */
		/* 746 */
		int col = (int) r << 16 | (int) g << 8 | (int) b;
		/*      */
		/* 748 */
		if ((distance != this.currentFogDistance)
				|| (col != this.currentFogColor))
		/*      */{
			/* 750 */
			if (this.currentFoggingState) {
				/* 751 */
				disableFogging();
				/*      */
			}
			/* 753 */
			this.currentFoggingState = true;
			/*      */
			/* 755 */
			this.fogColors.rewind();
			/* 756 */
			this.fogColors.put(r / 255.0F);
			/* 757 */
			this.fogColors.put(g / 255.0F);
			/* 758 */
			this.fogColors.put(b / 255.0F);
			/* 759 */
			this.fogColors.put(1.0F);
			/* 760 */
			this.fogColors.flip();
			/*      */
			/* 762 */
			this.gl10.glEnable(2912);
			/* 763 */
			this.gl10.glFogf(2915, start);
			/* 764 */
			this.gl10.glFogf(2916, distance);
			/* 765 */
			this.gl10.glFogx(2917, 9729);
			/* 766 */
			this.gl10.glFogfv(2918, this.fogColors);
			/* 767 */
			this.gl10.glFogf(2914, 1.0F);
			/*      */
			/* 769 */
			this.currentFogColor = col;
			/* 770 */
			this.currentFogDistance = distance;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	final void convertTexture(Texture tex) {
		/* 775 */
		boolean virtual = false;
		/* 776 */
		if ((tex.texels == null) && (tex.zippedTexels == null)) {
			/* 777 */
			Virtualizer tv = this.texMan.getVirtualizer();
			/* 778 */
			if (tv != null) {
				/* 779 */
				boolean ok = tv.restore(tex);
				/* 780 */
				if (ok) {
					/* 781 */
					virtual = true;
					/*      */
				}
				/*      */
			}
			/*      */
		}
		/*      */
		/* 786 */
		if ((tex.nPot) && (!hasExtension("npot"))) {
			/* 787 */
			Logger.log("This device doesn't support nPot-textures!", 1);
			/*      */
		}
		/*      */
		/* 790 */
		int[] pixels = tex.texels;
		/* 791 */
		if ((pixels == null) && (tex.zippedTexels != null)) {
			/* 792 */
			pixels = ZipHelper.unzip(tex.zippedTexels);
			/*      */
		}
		/*      */
		/* 795 */
		int h = tex.getHeight();
		/* 796 */
		int w = tex.getWidth();
		/*      */
		/* 798 */
		if ((h != w) && ((this.gl10 instanceof GL20Handler)))
		/*      */{
			/* 800 */
			Logger.log(
					"Texture's size is "
							+ w
							+ "/"
							+ h
							+ ", but textures should be square for OpenGL ES2.0! This may result in a black texture!",
					1);
			/*      */
		}
		/*      */
		/* 803 */
		int texLen = h * w;
		/*      */
		/* 805 */
		ByteBuffer temp = null;
		/* 806 */
		int len = texLen << 2;
		/*      */
		/* 808 */
		if ((tex.etc1) || (texLen == 1))
		/*      */{
			/* 811 */
			tex.convertTo4444 = false;
			/*      */
		}
		/*      */
		/* 814 */
		if (tex.convertTo4444) {
			/* 815 */
			len >>= 1;
			/*      */
		}
		/*      */
		/* 818 */
		if (Logger.isDebugEnabled()) {
			/* 819 */
			Logger.log("Allocating native memory for " + w + "*" + h
					+ " texture(" + tex.bilinear + "/" + tex.etc1 + "/"
					+ (tex.zippedTexels != null) + "/" + tex.isUnicolor + "/" +
					/* 820 */"): " + len + " bytes!", 3);
			/*      */
		}
		/*      */
		/* 823 */
		temp = ByteBuffer.allocateDirect(len).order(ByteOrder.LITTLE_ENDIAN);
		/*      */
		/* 825 */
		int mode = 5121;
		/* 826 */
		int[] res = (int[]) null;
		/* 827 */
		if (tex.convertTo4444) {
			/* 828 */
			int[] alpha = pixels;
			/* 829 */
			if (pixels != null) {
				/* 830 */
				res = new int[texLen >> 1];
				/* 831 */
				int cbm = 15790320;
				/*      */
				/* 833 */
				mode = 32819;
				/* 834 */
				if (tex.alpha) {
					/* 835 */
					for (int i = 0; i < texLen; i++)
					/*      */{
						/* 837 */
						int texel = pixels[i];
						/* 838 */
						int rgba = ((texel & 0xFF00) >> 12 << 4
								| (texel & 0xFF) >> 4 | (texel & 0xFF0000) >> 20 << 8) << 4;
						/* 839 */
						rgba |= (alpha[i] & 0xFF000000) >>> 28;
						/* 840 */
						if ((i & 0x1) == 0)
							/* 841 */res[(i >> 1)] = rgba;
						/*      */
						else
							/* 843 */res[(i >> 1)] |= rgba << 16;
						/*      */
					}
					/*      */
				}
				/*      */
				else {
					/* 847 */
					mode = 32820;
					/* 848 */
					for (int i = 0; i < texLen; i++)
					/*      */{
						/* 850 */
						int texel = pixels[i];
						/* 851 */
						int rgba = ((texel & 0xFF00) >> 11 << 5
								| (texel & 0xFF) >> 3 | (texel & 0xFF0000) >> 19 << 10) << 1;
						/* 852 */
						if ((texel & 0xFFFFFF & cbm) != 0) {
							/* 853 */
							rgba |= 1;
							/*      */
						}
						/* 855 */
						if ((i & 0x1) == 0)
							/* 856 */res[(i >> 1)] = rgba;
						/*      */
						else {
							/* 858 */
							res[(i >> 1)] |= rgba << 16;
							/*      */
						}
						/*      */
					}
					/*      */
				}
				/* 862 */
				temp.rewind();
				/* 863 */
				temp.asIntBuffer().put(res);
				/*      */
			}
			/*      */
		} else {
			/* 866 */
			int[] alpha = pixels;
			/* 867 */
			if (pixels != null) {
				/* 868 */
				res = new int[texLen];
				/* 869 */
				int cbm = 15790320;
				/* 870 */
				for (int i = 0; i < texLen; i++) {
					/* 871 */
					int texel = pixels[i];
					/* 872 */
					int rgba = texel & 0xFF00 | (texel & 0xFF) << 16
							| (texel & 0xFF0000) >> 16;
					/*      */
					/* 874 */
					if (!tex.alpha) {
						/* 875 */
						if ((texel & 0xFFFFFF & cbm) != 0)
							/* 876 */rgba |= -16777216;
						/*      */
					}
					/*      */
					else {
						/* 879 */
						rgba |= alpha[i] & 0xFF000000;
						/*      */
					}
					/* 881 */
					res[i] = rgba;
					/*      */
				}
				/* 883 */
				temp.rewind();
				/* 884 */
				temp.asIntBuffer().put(res);
				/*      */
			}
			/*      */
		}
		/*      */
		/* 888 */
		if (pixels == null) {
			/* 889 */
			temp.rewind();
			/* 890 */
			temp.asIntBuffer().put(
					new int[tex.convertTo4444 ? texLen >> 1 : texLen]);
			/*      */
		}
		/*      */
		/* 893 */
		ByteBuffer texBuf = temp;
		/*      */
		/* 895 */
		IntBuffer buf = getSmallBuffer(1);
		/* 896 */
		this.gl10.glGenTextures(1, buf);
		/*      */
		/* 898 */
		int texID = buf.get(0);
		/*      */
		/* 900 */
		if (Logger.isDebugEnabled()) {
			/* 901 */
			Logger.log("New texture's id is: " + texID, 3);
			/*      */
		}
		/*      */
		/* 904 */
		if (texID == 0) {
			/* 905 */
			Logger.log("Failed to upload texture!", 0);
			/*      */
		}
		/*      */
		/* 908 */
		int sid = this.lastTextures[0];
		/* 909 */
		bindTexture(0, texID);
		/*      */
		/* 911 */
		int myMode = 9729;
		/*      */
		/* 913 */
		if (!tex.nPot) {
			/* 914 */
			if (tex.mipmap) {
				/* 915 */
				myMode = 9985;
				/*      */
			}
			/* 917 */
			if ((Config.glTrilinear) && (!tex.etc1) && (tex.mipmap)) {
				/* 918 */
				myMode = 9987;
				/*      */
			}
			/*      */
		}
		/*      */
		/* 922 */
		this.gl10.glTexParameterx(3553, 10241, myMode);
		/*      */
		/* 924 */
		if (tex.bilinear)
			/* 925 */this.gl10.glTexParameterx(3553, 10240, 9729);
		/*      */
		else {
			/* 927 */
			this.gl10.glTexParameterx(3553, 10240, 9728);
			/*      */
		}
		/*      */
		/* 930 */
		int clampMode = 10497;
		/* 931 */
		if ((!tex.repeat) || (tex.nPot)) {
			/* 932 */
			clampMode = 33071;
			/*      */
		}
		/* 934 */
		this.gl10.glTexParameterx(3553, 10242, clampMode);
		/* 935 */
		this.gl10.glTexParameterx(3553, 10243, clampMode);
		/*      */
		/* 937 */
		if ((myMode == 9729) || (this.gl11 == null) || (tex.isShadowMap)
				|| (tex.nPot)) {
			/* 938 */
			res = (int[]) null;
			/* 939 */
			this.gl10.glTexImage2D(3553, 0, 6408, w, h, 0, 6408, mode, texBuf);
			/*      */
		} else {
			/* 941 */
			long s = System.currentTimeMillis();
			/* 942 */
			if ((this.mipmapsByGpu) && (!Config.internalMipmapCreation)
					&& (!tex.etc1)) {
				/* 943 */
				res = (int[]) null;
				/* 944 */
				this.gl10.glTexParameterf(3553, 33169, 1.0F);
				/* 945 */
				uploadInternal(w, h, mode, 0, texBuf, tex);
				/* 946 */
				if (Logger.isDebugEnabled()) {
					/* 947 */
					Logger.log(
							"Mipmaps generated by the GPU in "
									+ (System.currentTimeMillis() - s) + "ms",
							3);
					/*      */
				}
				/*      */
			}
			/*      */
			else
			/*      */{
				/* 952 */
				uploadInternal(w, h, mode, 0, texBuf, tex);
				/* 953 */
				buildMipmap(this.gl10, tex, texID, mode, res);
				/* 954 */
				if (Logger.isDebugEnabled()) {
					/* 955 */
					Logger.log(
							"Mipmaps generated by the CPU in "
									+ (System.currentTimeMillis() - s) + "ms",
							3);
					/*      */
				}
				/*      */
			}
			/*      */
		}
		/*      */
		/* 960 */
		tex.setOpenGLID(this.myID, texID);
		/* 961 */
		if (sid != 0) {
			/* 962 */
			bindTexture(0, sid);
			/*      */
		}
		/*      */
		/* 965 */
		if (Logger.isDebugEnabled()) {
			/* 966 */
			Logger.log(
					"New texture uploaded: " + tex + " in thread "
							+ Thread.currentThread(), 3);
			/*      */
		}
		/*      */
		/* 969 */
		this.uploadedTextures.add(tex);
		/*      */
		/* 971 */
		if (virtual)
			/* 972 */this.texMan.getVirtualizer().freeHandles(tex);
		/*      */
	}

	/*      */
	/*      */
	private void uploadInternal(int w, int h, int mode, int level,
			ByteBuffer texture, Texture tex)
	/*      */{
		/* 977 */
		boolean doEtc = (tex.etc1) && (this.gl20);
		/* 978 */
		if (doEtc) {
			/* 979 */
			doEtc = this.gl20Handler.uploadTexture(w, h, mode, level,
					tex.convertTo4444, texture);
			/*      */
		}
		/*      */
		/* 982 */
		if (!doEtc)
			/* 983 */this.gl10.glTexImage2D(3553, level, 6408, w, h, 0, 6408,
					mode, texture);
		/*      */
	}

	/*      */
	/*      */
	private void buildMipmap(GL10 gl, Texture tex, int textureID, int mode,
			int[] data)
	/*      */{
		/* 988 */
		int level = 1;
		/* 989 */
		int width = tex.getWidth() >> 1;
		/* 990 */
		int height = tex.getHeight() >> 1;
		/*      */
		/* 992 */
		while ((height >= 1) || (width >= 1))
		/*      */{
			/* 994 */
			if (tex.convertTo4444) {
				/* 995 */
				if (tex.alpha)
					/* 996 */data = rescale16(data, width, height, false);
				/*      */
				else
					/* 998 */data = rescale16(data, width, height, true);
				/*      */
			}
			/*      */
			else {
				/* 1001 */
				data = rescale32(data, width, height);
				/*      */
			}
			/*      */
			/* 1004 */
			int len = data.length << 2;
			/* 1005 */
			ByteBuffer temp = ByteBuffer.allocateDirect(len);
			/* 1006 */
			temp.order(ByteOrder.LITTLE_ENDIAN);
			/* 1007 */
			temp.rewind();
			/* 1008 */
			temp.asIntBuffer().put(data);
			/*      */
			/* 1012 */
			uploadInternal(width, height, mode, level, temp, tex);
			/* 1013 */
			level++;
			/*      */
			/* 1015 */
			height >>= 1;
			/* 1016 */
			width >>= 1;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private static int[] rescale16(int[] data, int newWidth, int newHeight,
			boolean alphaOne) {
		/* 1021 */
		int[] res = new int[Math.max(1, newWidth * newHeight >> 1)];
		/* 1022 */
		if (data == null) {
			/* 1023 */
			return res;
			/*      */
		}
		/* 1025 */
		int oldWidth = newWidth;
		/* 1026 */
		for (int y = 0; y < newHeight; y++) {
			/* 1027 */
			int offset = y * (newWidth >> 1);
			/* 1028 */
			int oldOffset = (y << 1) * oldWidth;
			/*      */
			/* 1030 */
			for (int x = 0; x < newWidth; x++) {
				/* 1031 */
				int index = offset + (x >> 1);
				/* 1032 */
				int oldIndex = oldOffset + x;
				/*      */
				/* 1034 */
				if (!alphaOne) {
					/* 1035 */
					int c0 = data[oldIndex] & 0xFFFF;
					/* 1036 */
					int c1 = (data[oldIndex] & 0xFFFF0000) >> 16;
					/* 1037 */
					int c2 = data[(oldIndex + oldWidth)] & 0xFFFF;
					/* 1038 */
					int c3 = (data[(oldIndex + oldWidth)] & 0xFFFF0000) >> 16;
					/*      */
					/* 1040 */
					int nr = ((c0 >>> 12 & 0xF) + (c1 >>> 12 & 0xF)
							+ (c2 >>> 12 & 0xF) + (c3 >>> 12 & 0xF) >> 2 & 0xF) << 12;
					/* 1041 */
					int ng = ((c0 >> 8 & 0xF) + (c1 >> 8 & 0xF)
							+ (c2 >> 8 & 0xF) + (c3 >> 8 & 0xF) >> 2 & 0xF) << 8;
					/* 1042 */
					int nb = ((c0 >> 4 & 0xF) + (c1 >> 4 & 0xF)
							+ (c2 >> 4 & 0xF) + (c3 >> 4 & 0xF) >> 2 & 0xF) << 4;
					/* 1043 */
					int na = (c0 & 0xF) + (c1 & 0xF) + (c2 & 0xF) + (c3 & 0xF) >> 2 & 0xF;
					/*      */
					/* 1045 */
					if ((x & 0x1) == 0)
						/* 1046 */res[index] = (na | nr | ng | nb);
					/*      */
					else
						/* 1048 */res[index] |= (na | nr | ng | nb) << 16;
					/*      */
				}
				/*      */
				else {
					/* 1051 */
					int c0 = data[oldIndex] & 0xFFFF;
					/* 1052 */
					int c1 = (data[oldIndex] & 0xFFFF0000) >> 16;
					/* 1053 */
					int c2 = data[(oldIndex + oldWidth)] & 0xFFFF;
					/* 1054 */
					int c3 = (data[(oldIndex + oldWidth)] & 0xFFFF0000) >> 16;
					/*      */
					/* 1056 */
					int nr = ((c0 >>> 11 & 0x1F) + (c1 >>> 11 & 0x1F)
							+ (c2 >>> 11 & 0x1F) + (c3 >>> 11 & 0x1F) >> 2 & 0x1F) << 11;
					/* 1057 */
					int ng = ((c0 >> 6 & 0x1F) + (c1 >> 6 & 0x1F)
							+ (c2 >> 6 & 0x1F) + (c3 >> 6 & 0x1F) >> 2 & 0x1F) << 6;
					/* 1058 */
					int nb = ((c0 >> 1 & 0x1F) + (c1 >> 1 & 0x1F)
							+ (c2 >> 1 & 0x1F) + (c3 >> 1 & 0x1F) >> 2 & 0x1F) << 1;
					/* 1059 */
					int na = (c0 & 0x1) + (c1 & 0x1) + (c2 & 0x1) + (c3 & 0x1) >> 2 & 0x1;
					/*      */
					/* 1061 */
					if ((x & 0x1) == 0)
						/* 1062 */res[index] = (na | nr | ng | nb);
					/*      */
					else {
						/* 1064 */
						res[index] |= (na | nr | ng | nb) << 16;
						/*      */
					}
					/*      */
				}
				/*      */
			}
			/*      */
		}
		/* 1069 */
		return res;
		/*      */
	}

	/*      */
	/*      */
	private static int[] rescale32(int[] data, int newWidth, int newHeight) {
		/* 1073 */
		int[] res = new int[newWidth * newHeight];
		/* 1074 */
		if (data == null) {
			/* 1075 */
			return res;
			/*      */
		}
		/* 1077 */
		int oldWidth = newWidth << 1;
		/* 1078 */
		for (int y = 0; y < newHeight; y++) {
			/* 1079 */
			int offset = y * newWidth;
			/* 1080 */
			int oldOffset = (y << 1) * oldWidth;
			/* 1081 */
			for (int x = 0; x < newWidth; x++) {
				/* 1082 */
				int index = offset + x;
				/* 1083 */
				int oldIndex = oldOffset + (x << 1);
				/*      */
				/* 1085 */
				int c0 = data[oldIndex];
				/* 1086 */
				int c1 = data[(oldIndex + 1)];
				/* 1087 */
				int c2 = data[(oldIndex + oldWidth)];
				/* 1088 */
				int c3 = data[(oldIndex + 1 + oldWidth)];
				/*      */
				/* 1090 */
				int na = ((c0 >>> 24 & 0xFF) + (c1 >>> 24 & 0xFF)
						+ (c2 >>> 24 & 0xFF) + (c3 >>> 24 & 0xFF) >> 2 & 0xFF) << 24;
				/* 1091 */
				int nr = ((c0 >> 16 & 0xFF) + (c1 >> 16 & 0xFF)
						+ (c2 >> 16 & 0xFF) + (c3 >> 16 & 0xFF) >> 2 & 0xFF) << 16;
				/* 1092 */
				int ng = ((c0 >> 8 & 0xFF) + (c1 >> 8 & 0xFF)
						+ (c2 >> 8 & 0xFF) + (c3 >> 8 & 0xFF) >> 2 & 0xFF) << 8;
				/* 1093 */
				int nb = (c0 & 0xFF) + (c1 & 0xFF) + (c2 & 0xFF) + (c3 & 0xFF) >> 2 & 0xFF;
				/*      */
				/* 1095 */
				res[index] = (na | nr | ng | nb);
				/*      */
			}
			/*      */
		}
		/* 1098 */
		return res;
		/*      */
	}

	/*      */
	/*      */
	private final void enableDepthBuffer() {
		/* 1102 */
		if (!this.depthBuffer) {
			/* 1103 */
			this.gl10.glEnable(2929);
			/*      */
			/* 1105 */
			this.depthBuffer = true;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private final void disableDepthBuffer() {
		/* 1110 */
		if (this.depthBuffer) {
			/* 1111 */
			this.gl10.glDisable(2929);
			/*      */
			/* 1113 */
			this.depthBuffer = false;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private void enableColorArray()
	/*      */{
		/* 1120 */
		this.gl10.glColorPointer(4, 5132, 16, this.colors);
		/* 1121 */
		this.gl10.glEnableClientState(32886);
		/*      */
	}

	/*      */
	/*      */
	private final void renableVertexArrays() {
		/* 1125 */
		if ((this.hasToReEnable) && (this.vertices != null)) {
			/* 1126 */
			this.gl10.glVertexPointer(3, 5132, 12, this.vertices);
			/*      */
			/* 1128 */
			this.gl10.glEnableClientState(32884);
			/* 1129 */
			this.gl10.glDisableClientState(32885);
			/*      */
			/* 1131 */
			clearStageFlag();
			/* 1132 */
			this.gl10.glClientActiveTexture(stageMap[0]);
			/* 1133 */
			this.gl10.glEnableClientState(32888);
			/* 1134 */
			this.gl10.glTexCoordPointer(2, 5132, 8, this.textures);
			/* 1135 */
			this.hasToReEnable = false;
			/*      */
		}
		/*      */
	}

	private void enableBlitting(FrameBuffer fb, boolean trans, boolean additive) {
		if (this.myRage3DScene == null) {
			this.myRage3DScene = new T3DScene();
			int width = fb.getWidth();
			int height = fb.getHeight();
			this.myRage3DScene.getCamera().calcFOV(width, height);
			setFrustum(this.myRage3DScene, fb);
		}
		if ((trans) && ((!this.blitMode) || (!this.blitTrans))) {
			if (this.blitMode) {
				executeBufferedBlits();
			}
			TransparencyMode mode = TransparencyMode.DEFAULT;
			if (additive) {
				mode = TransparencyMode.ADD;
			}
			this.blitAdditive = additive;
			setBlendingMode(mode);
			this.blitTrans = true;
		}
		if (!this.blitMode) {
			this.blitScaling = this.currentRGBScaling;
			if (this.myRage3DScene != null) {
				setRGBScaling(1);
			}
			disableDepthBuffer();
			this.blitMode = true;
		} else {
			if ((this.blitTrans) && (!trans)) {
				executeBufferedBlits();
				this.gl10.glDisable(3042);
				this.blitTrans = false;
			}
			if ((this.blitTrans) && (trans) && (additive != this.blitAdditive)) {
				executeBufferedBlits();
				this.blitAdditive = additive;
				TransparencyMode mode = TransparencyMode.DEFAULT;
				if (additive) {
					mode = TransparencyMode.ADD;
				}
				setBlendingMode(mode);
				/*      */
			}
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	public void disableBlitting() {
		/* 1198 */
		if (this.blitMode) {
			/* 1199 */
			executeBufferedBlits();
			/* 1200 */
			if ((this.myRage3DScene != null)
					&& (this.blitScaling != this.currentRGBScaling)) {
				/* 1201 */
				setRGBScaling(this.blitScaling);
				/*      */
			}
			/*      */
			/* 1204 */
			if (this.blitTrans) {
				/* 1205 */
				this.gl10.glDisable(3042);
				/* 1206 */
				this.blitTrans = false;
				/*      */
			}
			/* 1208 */
			enableDepthBuffer();
			/* 1209 */
			this.blitMode = false;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	private void setLightsAndFog(T3DScene myRage3DScene) {
		/* 1214 */
		setRGBScaling(myRage3DScene.lights.rgbScale);
		/*      */
		/* 1216 */
		int hash = myRage3DScene.hashCode();
		/* 1217 */
		boolean forceEnable = false;
		/* 1218 */
		if (hash != this.lastWorldHash) {
			/* 1219 */
			forceEnable = myRage3DScene.useFogging;
			/* 1220 */
			this.lastWorldHash = hash;
			/*      */
		}
		if ((myRage3DScene.fogModeChanged == 1) || (forceEnable)) {
			enableFogging(myRage3DScene.fogStart, myRage3DScene.fogDistance,
					myRage3DScene.fogColorR, myRage3DScene.fogColorG,
					myRage3DScene.fogColorB);
			myRage3DScene.fogModeChanged = 0;
		} else if (myRage3DScene.fogModeChanged == 2) {
			disableFogging();
			myRage3DScene.fogModeChanged = 0;
		}
	}

	private void prepareForBlitting(int[] src, int sourceWidth, int sourceHeight)
	/*      */{
		int sTexID = 0;
		int w = sourceWidth;
		int h = sourceHeight;
		if (this.blitBuffer != null) {
			sTexID = this.blitBuffer.getOpenGLID(this.myID);
			if ((this.blitBufferWidth == w) && (this.blitBufferHeight == h)) {
				if (!Config.glUseIgnorantBlits) {
					this.blitBuffer.refill(src, w, h);
					this.blitBuffer.setMarker(this.myID,
							Texture.MARKER_DELETE_AND_UPLOAD);
					this.blitBuffer.setOpenGLID(this.myID, sTexID);
				}
			} else {
				removeTexture(this.blitBuffer);
				this.blitBuffer = new Texture(src, w, h, true);
				this.blitBuffer.setMarker(this.myID,
						Texture.MARKER_DELETE_AND_UPLOAD);
				this.blitBuffer.setOpenGLID(this.myID, sTexID);
			}
		} else {
			this.blitBuffer = new Texture(src, w, h, true);
		}
		this.blitBufferWidth = w;
		this.blitBufferHeight = h;
	}

	private final void initializeVertexArrays() {
		if (this.init) {
			this.colors = ByteBuffer.allocateDirect(9600)
					.order(ByteOrder.nativeOrder()).asIntBuffer();
			this.vertices = ByteBuffer.allocateDirect(7200)
					.order(ByteOrder.nativeOrder()).asIntBuffer();
			this.gl10.glColorPointer(4, 5132, 16, this.colors);
			this.gl10.glVertexPointer(3, 5132, 12, this.vertices);
			this.gl10.glEnableClientState(32886);
			this.gl10.glEnableClientState(32884);
			this.gl10.glEnableClientState(32888);
			this.textures = ByteBuffer.allocateDirect(4800)
					.order(ByteOrder.nativeOrder()).asIntBuffer();
			this.gl10.glTexCoordPointer(2, 5132, 8, this.textures);
			this.indices = ByteBuffer.allocateDirect(2400)
					.order(ByteOrder.nativeOrder()).asShortBuffer();
			this.vertexArraysInitialized = true;
			this.colorArray = new int[2400];
			this.vertexArray = new int[1800];
			this.textureArray = new int[1200];
			this.indexArray = new short[600];
		}
	}

	private void executeBufferedBlits()
	/*      */{
		if ((this.colors == null) || (this.indexPos == 0)) {
			return;
		}
		this.colors.rewind();
		this.colors.put(this.colorArray, 0, this.colPos);
		this.colors.rewind();
		this.vertices.rewind();
		this.vertices.put(this.vertexArray, 0, this.vertPos);
		this.vertices.rewind();
		this.textures.rewind();
		this.textures.put(this.textureArray, 0, this.texPos);
		this.textures.rewind();
		this.indices.rewind();
		this.indices.put(this.indexArray, 0, this.indexPos);
		this.indices.rewind();
		clearShader();
		closeShader();
		if (this.currentFoggingState) {
			this.gl10.glDisable(2912);
		}
		initShader();
		renableVertexArrays();
		enableColorArray();
		this.gl10.glDrawElements(4, this.indexPos, 5123, this.indices);
		clearShader();
		closeShader();
		if (this.currentFoggingState) {
			this.gl10.glEnable(2912);
		}
		this.curPos = 0;
		this.colPos = 0;
		this.texPos = 0;
		this.vertPos = 0;
		this.indexPos = 0;
	}

	private void blit(FrameBuffer fb, Texture t, float spx, float spy, int sw,
			int sh, int posx, int posy, boolean addData, int addX, int addY,
			int transValue, int red, int green, int blue)
	/*      */{
		if (this.init)
		/*      */{
			if (this.myRage3DScene == null) {
				return;
			}
			Camera camera = this.myRage3DScene.getCamera();
			float curScaleX = camera.scaleX;
			float curScaleY = camera.scaleY;
			if (!this.vertexArraysInitialized) {
				initializeVertexArrays();
			}
			disableUnusedStages();
			switchTextureMode(0, modeMap[0]);
			float z = -1.00002F;
			if ((!Config.glIgnoreNearPlane) && (Config.nearPlane < 1.0F)) {
				z = -Config.nearPlane - 0.0002F;
			}
			float h = t.getHeight();
			float w = t.getWidth();
			int texID = t.getOpenGLID(this.myID);
			if ((texID == 0)
					|| (t.getMarker(this.myID) == Texture.MARKER_DELETE_AND_UPLOAD)) {
				t.setMarker(this.myID, Texture.MARKER_NOTHING);
				if (texID != 0) {
					IntBuffer buf = getSmallBuffer(2);
					buf.put(texID);
					buf.flip();
					this.gl10.glDeleteTextures(1, buf);
				}
				convertTexture(t);
				texID = t.getOpenGLID(this.myID);
			}
			if (texID != this.lastTextures[0]) {
				executeBufferedBlits();
				bindTexture(0, texID);
			}
			float trans = 1.0F;
			if (addData) {
				trans = Config.glTransparencyOffset + transValue
						* Config.glTransparencyMul;
				if (trans > 1.0F)
					/* 1395 */trans = 1.0F;
			} else {
				addX = sw;
				addY = sh;
				red = 255;
				green = 255;
				blue = 255;
			}
			float dw = 1.0F / w;
			float dh = 1.0F / h;
			float u1 = dw * spx;
			float v1 = dh * spy;
			float u2 = dw * (sw + spx);
			float v2 = dh * (sh + spy);
			if ((this.myRage3DScene != null) && (fb != null))
			/*      */{
				Interact2D.reproject2D3DBlit(curScaleX, curScaleY, fb, posx,
						posy, 1.0F, this.blitCoords1);
				Interact2D.reproject2D3DBlit(curScaleX, curScaleY, fb, posx
						+ addX, posy + addY, 1.0F, this.blitCoords2);
				if (this.curPos >= 594) {
					executeBufferedBlits();
				}
				int[] colorArray = this.colorArray;
				int[] vertexArray = this.vertexArray;
				int[] textureArray = this.textureArray;
				short[] indexArray = this.indexArray;
				int ri = red << 8;
				int gi = green << 8;
				int bi = blue << 8;

				int ti = (int) (trans * 65536.0F);

				colorArray[(this.colPos++)] = ri;

				colorArray[(this.colPos++)] = gi;

				colorArray[(this.colPos++)] = bi;

				colorArray[(this.colPos++)] = ti;

				colorArray[(this.colPos++)] = ri;

				colorArray[(this.colPos++)] = gi;

				colorArray[(this.colPos++)] = bi;

				colorArray[(this.colPos++)] = ti;

				colorArray[(this.colPos++)] = ri;

				colorArray[(this.colPos++)] = gi;

				colorArray[(this.colPos++)] = bi;

				colorArray[(this.colPos++)] = ti;

				colorArray[(this.colPos++)] = ri;

				colorArray[(this.colPos++)] = gi;

				colorArray[(this.colPos++)] = bi;

				colorArray[(this.colPos++)] = ti;

				int b1x = (int) (this.blitCoords1.x * 65536.0F);

				int b2x = (int) (this.blitCoords2.x * 65536.0F);

				int b1y = (int) (-this.blitCoords1.y * 65536.0F);

				int b2y = (int) (-this.blitCoords2.y * 65536.0F);

				int bz = (int) (z * 65536.0F);

				vertexArray[(this.vertPos++)] = b1x;

				vertexArray[(this.vertPos++)] = b2y;

				vertexArray[(this.vertPos++)] = bz;

				vertexArray[(this.vertPos++)] = b2x;

				vertexArray[(this.vertPos++)] = b2y;

				vertexArray[(this.vertPos++)] = bz;

				vertexArray[(this.vertPos++)] = b1x;

				vertexArray[(this.vertPos++)] = b1y;

				vertexArray[(this.vertPos++)] = bz;

				vertexArray[(this.vertPos++)] = b2x;

				vertexArray[(this.vertPos++)] = b1y;

				vertexArray[(this.vertPos++)] = bz;

				int ui1 = (int) (u1 * 65536.0F);

				int ui2 = (int) (u2 * 65536.0F);

				int vi1 = (int) (v1 * 65536.0F);

				int vi2 = (int) (v2 * 65536.0F);

				textureArray[(this.texPos++)] = ui1;

				textureArray[(this.texPos++)] = vi2;

				textureArray[(this.texPos++)] = ui2;

				textureArray[(this.texPos++)] = vi2;

				textureArray[(this.texPos++)] = ui1;

				textureArray[(this.texPos++)] = vi1;

				textureArray[(this.texPos++)] = ui2;

				textureArray[(this.texPos++)] = vi1;

				short ip = (short) ((this.vertPos - 12) / 3);

				indexArray[(this.indexPos++)] = ip;

				indexArray[(this.indexPos++)] = ((short) (ip + 1));

				indexArray[(this.indexPos++)] = ((short) (ip + 2));

				indexArray[(this.indexPos++)] = ((short) (ip + 2));

				indexArray[(this.indexPos++)] = ((short) (ip + 3));

				indexArray[(this.indexPos++)] = ((short) (ip + 1));

				this.curPos += 6;

			}

		}
	}

	public void unloadKnownTextures()
	/*      */{
		this.toUnload.addAll(this.uploadedTextures);
		unloadTextures();
		this.uploadedTextures.clear();
		Logger.log("All texture data unloaded from gpu!");
	}

	/*      */void upload(Texture t) {
		synchronized (this) {
			if ((t.getOpenGLID(this.myID) == 0) && (t != this.renderTarget)) {
				t.setMarker(this.myID, Texture.MARKER_NOTHING);
				convertTexture(t);
			}
		}
	}

	private void unloadTextures() {
		if (this.toUnload.size() > 0)
			/* 1635 */synchronized (this) {
				for (Texture t : this.toUnload) {
					if (t.getOpenGLID(this.myID) != 0) {
						if (this.gl20Handler != null) {
							this.gl20Handler.unloadRenderTarget(t);
						}
						removeTexture(t);
						t.clearIDs(this.myID);
					}
				}
				this.toUnload.clear();
			}
	}

	private final int getTextureStages()
	/*      */{
		IntBuffer sm = ByteBuffer.allocateDirect(64)
				.order(ByteOrder.nativeOrder()).asIntBuffer();
		this.gl10.glGetIntegerv(34018, sm);
		int max = sm.get(0);
		if (max > 4) {
			max = 4;
		}
		return max;
	}

	private final int getTextureStagesRaw() {
		if (this.gl20) {
			/* 1662 */
			return this.gl20Handler.getTextureStagesRaw();
			/*      */
		}
		/* 1664 */
		IntBuffer sm = ByteBuffer.allocateDirect(64)
				.order(ByteOrder.nativeOrder()).asIntBuffer();
		/* 1665 */
		this.gl10.glGetIntegerv(34018, sm);
		/* 1666 */
		int max = sm.get(0);
		/* 1667 */
		return max;
		/*      */
	}

	/*      */
	/*      */
	private final void initTextureStage(int stage) {
		/* 1671 */
		initTextureStage(stage, modeMap[0]);
		/*      */
	}

	/*      */
	/*      */void resetViewport(FrameBuffer buffer) {
		/* 1675 */
		int xViewStart = (int) (Config.viewportOffsetX * buffer.getWidth());
		/* 1676 */
		int yViewStart = (int) (-Config.viewportOffsetY * buffer.getHeight());
		/* 1677 */
		int xViewEnd = buffer.getWidth();
		/* 1678 */
		int yViewEnd = buffer.getHeight();
		/* 1679 */
		this.gl10.glViewport(xViewStart, yViewStart, xViewEnd, yViewEnd);
		/*      */
	}

	/*      */
	/*      */
	private void setViewport(FrameBuffer buffer) {
		/* 1683 */
		this.yTargetStart = (buffer.getHeight() - this.renderTarget.getHeight());
		/* 1684 */
		this.gl10.glViewport(0, this.yTargetStart,
				this.renderTarget.getWidth(), this.renderTarget.getHeight());
		/*      */
	}

	/*      */
	/*      */
	private void enableScissor(FrameBuffer buffer, Texture renderTarget,
			int xMin, int yMin, int xMax, int yMax)
	/*      */{
		/* 1689 */
		int xcMin = 0;
		/* 1690 */
		int ycMin = buffer.getHeight() - renderTarget.getHeight();
		/* 1691 */
		int xcMax = renderTarget.getWidth();
		/* 1692 */
		int ycMax = renderTarget.getHeight();
		/*      */
		/* 1694 */
		if (xMin != -1) {
			/* 1695 */
			this.scissorEnabled = true;
			/* 1696 */
			xcMin += xMin;
			/*      */
		}
		/*      */
		/* 1699 */
		if (yMin != -1) {
			/* 1700 */
			this.scissorEnabled = true;
			/* 1701 */
			ycMin += yMin;
			/*      */
		}
		/*      */
		/* 1704 */
		if (xMax != -1) {
			/* 1705 */
			this.scissorEnabled = true;
			/* 1706 */
			xcMax -= xMin + xMax;
			/*      */
		}
		/*      */
		/* 1709 */
		if (yMax != -1) {
			/* 1710 */
			this.scissorEnabled = true;
			/* 1711 */
			ycMax -= yMin + yMax;
			/*      */
		}
		/*      */
		/* 1714 */
		if (this.scissorEnabled) {
			/* 1715 */
			this.gl10.glEnable(3089);
			/*      */
			/* 1717 */
			if (xcMin < 0) {
				/* 1718 */
				xcMin = 0;
				/*      */
			}
			/* 1720 */
			if (xcMax < 0) {
				/* 1721 */
				xcMax = 0;
				/*      */
			}
			/* 1723 */
			if (ycMin < 0) {
				/* 1724 */
				ycMin = 0;
				/*      */
			}
			/* 1726 */
			if (ycMax < 0) {
				/* 1727 */
				ycMax = 0;
				/*      */
			}
			/*      */
			/* 1730 */
			this.gl10.glScissor(xcMin, ycMin, xcMax, ycMax);
			/*      */
		}
		/*      */
	}

	private void disableScissor() {
		if (this.scissorEnabled) {
			this.gl10.glDisable(3089);
			this.scissorEnabled = false;
		}
	}

	public void endState() {
		disableBlitting();
	}

	public void blitTexture(Texture src, FrameBuffer frameBuffer, int srcX,
			int srcY, int destX, int destY, int width, int height,
			boolean transparent) {
		enableBlitting(frameBuffer, transparent, false);
		blit(frameBuffer, src, srcX, srcY, width, height, destX, destY, false,
				0, 0, 0, 0, 0, 0);
	}

	public void blitTexture(Texture src, FrameBuffer frameBuffer, int srcX,
			int srcY, int destX, int destY, int width, int height,
			boolean additive, boolean addColor, int addX, int addY,
			int transValue, int red, int green, int blue)
	/*      */{
		enableBlitting(frameBuffer, transValue > -1, additive);
		blit(frameBuffer, src, srcX, srcY, width, height, destX, destY, true,
				addX, addY, transValue, red, green, blue);
	}

	public void blitIntArray(int[] src, FrameBuffer frameBuffer, int srcX,
			int srcY, int destX, int destY, int width, int height,
			boolean transparent, int sourceWidth, int sourceHeight)
	/*      */{
		/* 1758 */
		enableBlitting(frameBuffer, transparent, false);
		/* 1759 */
		prepareForBlitting(src, sourceWidth, sourceHeight);
		/* 1760 */
		blit(frameBuffer, this.blitBuffer, srcX, srcY, width, height, destX,
				destY, false, 0, 0, 0, 0, 0, 0);
		/*      */
	}

	/*      */
	/*      */
	public void setFrustumAndFog(T3DScene myRage3DScene, FrameBuffer buffer) {
		/* 1764 */
		T3DScene oldRage3DScene = this.myRage3DScene;
		/* 1765 */
		this.myRage3DScene = myRage3DScene;
		/*      */
		/* 1767 */
		setFrustum(myRage3DScene, buffer);
		/*      */
		/* 1769 */
		if (myRage3DScene != oldRage3DScene)
		/*      */{
			/* 1771 */
			if (myRage3DScene.useFogging)
				/* 1772 */myRage3DScene.fogModeChanged = 1;
			/*      */
			else {
				/* 1774 */
				myRage3DScene.fogModeChanged = 2;
				/*      */
			}
			/*      */
		}
		/* 1777 */
		oldRage3DScene = null;
		/*      */
		/* 1779 */
		setLightsAndFog(myRage3DScene);
		/* 1780 */
		unloadTextures();
		/*      */
	}

	/*      */
	/*      */
	public void swapBuffers() {
		/* 1784 */
		disableBlitting();
		/* 1785 */
		renderToTarget();
		/* 1786 */
		this.hasToReEnable = true;
		/*      */
	}

	/*      */
	/*      */
	public void revalidate(int width, int height) {
		/* 1790 */
		this.gl10.glViewport(0, 0, width, height);
		/*      */
	}

	/*      */
	/*      */
	public void clear(RGBColor col) {
		/* 1794 */
		disableBlitting();
		/* 1795 */
		if ((this.scissorEnabled) && (this.scissorClearAll)) {
			/* 1796 */
			this.gl10.glDisable(3089);
			/*      */
		}
		/*      */
		/* 1799 */
		int cmode = 16640;
		/*      */
		/* 1801 */
		if (Config.aaMode == 2) {
			/* 1802 */
			cmode |= 32768;
			/*      */
		}
		/*      */
		/* 1805 */
		if (col != null)
			/* 1806 */
			this.gl10.glClearColor(col.getNormalizedRed(),
					col.getNormalizedGreen(), col.getNormalizedBlue(),
					col.getNormalizedAlpha());
		/*      */
		else {
			/* 1808 */
			this.gl10.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
			/*      */
		}
		/* 1810 */
		this.gl10.glClear(cmode);
		/* 1811 */
		if ((this.scissorEnabled) && (this.scissorClearAll))
			/* 1812 */this.gl10.glEnable(3089);
		/*      */
	}

	/*      */
	/*      */
	public void clearZBufferOnly()
	/*      */{
		/* 1817 */
		disableBlitting();
		/* 1818 */
		if ((this.scissorEnabled) && (this.scissorClearAll)) {
			/* 1819 */
			this.gl10.glDisable(3089);
			/*      */
		}
		/* 1821 */
		this.gl10.glClear(256);
		/* 1822 */
		if ((this.scissorEnabled) && (this.scissorClearAll))
			/* 1823 */this.gl10.glEnable(3089);
		/*      */
	}

	/*      */
	/*      */
	public void clearColorBufferOnly(RGBColor col)
	/*      */{
		/* 1828 */
		disableBlitting();
		/* 1829 */
		if ((this.scissorEnabled) && (this.scissorClearAll)) {
			/* 1830 */
			this.gl10.glDisable(3089);
			/*      */
		}
		/* 1832 */
		int cmode = 16384;
		/* 1833 */
		if (Config.aaMode == 2) {
			/* 1834 */
			cmode |= 32768;
			/*      */
		}
		/*      */
		/* 1837 */
		if (col != null)
			/* 1838 */
			this.gl10.glClearColor(col.getNormalizedRed(),
					col.getNormalizedGreen(), col.getNormalizedBlue(),
					col.getNormalizedAlpha());
		/*      */
		else {
			/* 1840 */
			this.gl10.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
			/*      */
		}
		/*      */
		/* 1843 */
		this.gl10.glClear(cmode);
		/* 1844 */
		if ((this.scissorEnabled) && (this.scissorClearAll))
			/* 1845 */this.gl10.glEnable(3089);
		/*      */
	}

	/*      */
	/*      */
	public void grabScreen(FrameBuffer fb, int[] pixelout)
	/*      */{
		/* 1850 */
		disableBlitting();
		/* 1851 */
		int w = fb.getWidth();
		/* 1852 */
		int h = fb.getHeight();
		/* 1853 */
		int sizeI = w * h;
		/* 1854 */
		int size = sizeI << 2;
		/*      */
		/* 1856 */
		IntBuffer pixels = null;
		/* 1857 */
		if ((this.pixelBuffer != null) && (size == this.pixelBufferSize)) {
			/* 1858 */
			pixels = this.pixelBuffer;
			/*      */
		}
		/*      */
		/* 1861 */
		if (pixels == null) {
			/* 1862 */
			pixels = ByteBuffer.allocateDirect(size)
					.order(ByteOrder.nativeOrder()).asIntBuffer();
			/* 1863 */
			this.pixelBuffer = pixels;
			/* 1864 */
			this.pixelBufferSize = size;
			/*      */
		}
		/*      */
		/* 1868 */
		this.gl10.glReadPixels(0, 0, w, h, 6408, 5121, pixels);
		/*      */
		/* 1870 */
		for (int i = 0; i < sizeI; i++) {
			/* 1871 */
			pixelout[i] = pixels.get(i);
			/*      */
		}
		/*      */
		/* 1874 */
		for (int y = 0; y < h >> 1; y++) {
			/* 1875 */
			int y1 = y * w;
			/* 1876 */
			int y2 = (h - 1 - y) * w;
			/* 1877 */
			for (int x = 0; x < w; x++) {
				/* 1878 */
				int p1 = x + y1;
				/* 1879 */
				int p2 = y2 + x;
				/* 1880 */
				int tmpPix = pixelout[p1];
				/* 1881 */
				pixelout[p1] = pixelout[p2];
				/* 1882 */
				pixelout[p2] = tmpPix;
				/*      */
			}
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	public void setBufferViewport(int xViewStart, int yViewStart, int xViewEnd,
			int yViewEnd) {
		/* 1888 */
		disableBlitting();
		/* 1889 */
		if ((this.renderTarget == null)
				|| (Config.viewportOffsetAffectsRenderTarget))
			/* 1890 */this.gl10.glViewport(xViewStart, yViewStart, xViewEnd,
					yViewEnd);
		/*      */
	}

	/*      */
	/*      */
	public void startPainting()
	/*      */{
		/* 1895 */
		disableBlitting();
		/* 1896 */
		if ((this.listener != null) && (this.listenerActive))
			/* 1897 */this.listener.startPainting();
		/*      */
	}

	/*      */
	/*      */
	public void endPainting()
	/*      */{
		/* 1902 */
		disableBlitting();
		/* 1903 */
		resetTextureStates();
		/* 1904 */
		if ((this.listener != null) && (this.listenerActive))
			/* 1905 */this.listener.finishedPainting();
		/*      */
	}

	/*      */
	/*      */
	public void postProcess(FrameBuffer fb, IPostProcessor post)
	/*      */{
		/* 1910 */
		disableBlitting();
		/* 1911 */
		doPostProcessing(fb, post);
		/*      */
	}

	/*      */
	/*      */
	public void disposeProcessor(IPostProcessor pp) {
		/* 1915 */
		disableBlitting();
		/* 1916 */
		pp.dispose();
		/*      */
	}

	/*      */
	/*      */
	public void setRenderTarget(Texture target, FrameBuffer buffer, int xMin,
			int yMin, int xMax, int yMax, boolean scissorClearAll) {
		/* 1920 */
		disableBlitting();
		/* 1921 */
		this.renderTarget = target;
		/*      */
		/* 1923 */
		if (this.renderTarget != null)
			/* 1924 */enableScissor(buffer, this.renderTarget, xMin, yMin,
					xMax, yMax);
		/*      */
		else {
			/* 1926 */
			disableScissor();
			/*      */
		}
		/*      */
		/* 1929 */
		if ((!this.gl20) || (!Config.useFBO)) {
			/* 1930 */
			if (this.renderTarget == null) {
				/* 1931 */
				resetViewport(buffer);
				/* 1932 */
				this.gl10.glColorMask(true, true, true, true);
				/*      */
			} else {
				/* 1934 */
				setViewport(buffer);
				/*      */
			}
			/*      */
		}
		/* 1937 */
		else
			this.gl20Handler.setRenderTarget(this.renderTarget, this, buffer);
		/*      */
	}

	/*      */
	/*      */
	private void setFrustum(T3DScene rage3DScene, FrameBuffer buffer)
	/*      */{
		/* 1942 */
		Camera cam = rage3DScene.getCamera();
		/* 1943 */
		float fov = cam.getFOV();
		/* 1944 */
		float fovy = 0.0F;
		/* 1945 */
		if ((fov != this.lastFOV)
				|| (Config.farPlane != this.lastFarPlane)
				|| ((!Config.glIgnoreNearPlane) && (Config.nearPlane != this.lastNearPlane))
				|| (Config.autoMaintainAspectRatio != this.lastFOVMode)) {
			/* 1946 */
			this.gl10.glMatrixMode(5889);
			/* 1947 */
			this.gl10.glLoadIdentity();
			/*      */
			/* 1949 */
			float h = buffer.getHeight();
			/* 1950 */
			float w = buffer.getWidth();
			/*      */
			/* 1952 */
			if (buffer.renderTarget != null)
			/*      */{
				/* 1958 */
				if ((buffer.virtualHeight > 0) && (buffer.virtualWidth > 0)) {
					/* 1959 */
					h = buffer.virtualHeight;
					/* 1960 */
					w = buffer.virtualWidth;
					/*      */
				} else {
					/* 1962 */
					h = buffer.renderTarget.height;
					/* 1963 */
					w = buffer.renderTarget.width;
					/*      */
				}
				/*      */
			}
			/*      */
			/* 1967 */
			if (Config.autoMaintainAspectRatio)
				/* 1968 */fovy = fov * (h / w);
			/*      */
			else {
				/* 1970 */
				fovy = fov;
				/*      */
			}
			/*      */
			/* 1973 */
			if (cam.getYFOV() != -1.0F) {
				/* 1974 */
				fovy = cam.getYFOV();
				/*      */
			}
			/*      */
			/* 1977 */
			float far = Config.farPlane;
			/* 1978 */
			float near = 1.0F;
			/* 1979 */
			if (!Config.glIgnoreNearPlane) {
				/* 1980 */
				near = Config.nearPlane;
				/*      */
			}
			/*      */
			/* 1984 */
			float top = fovy * 0.5F;
			/* 1985 */
			float bottom = -fovy * 0.5F;
			/* 1986 */
			float left = -fov * 0.5F;
			/* 1987 */
			float right = fov * 0.5F;
			/*      */
			/* 1989 */
			this.gl10.glFrustumf(left, right, bottom, top, near, far);
			/*      */
			/* 1991 */
			this.lastFOV = (fov + 100.0F * fovy);
			/* 1992 */
			this.lastFarPlane = Config.farPlane;
			/* 1993 */
			this.lastNearPlane = Config.nearPlane;
			/* 1994 */
			this.lastFOVMode = Config.autoMaintainAspectRatio;
			/*      */
		}
		/*      */
	}

	/*      */
	/*      */
	public void enableLineMode(T3DScene rage3DScene) {
		/* 1999 */
		disableBlitting();
		/*      */
		/* 2001 */
		clearShader();
		/* 2002 */
		closeShader();
		/*      */
		/* 2004 */
		disableCompiledPipeline();
		/*      */
		/* 2006 */
		Camera cam = rage3DScene.getCamera();
		/*      */
		/* 2008 */
		this.tmpMat.setTo(cam.getBack());
		/* 2009 */
		this.tmpMat.transformToGL();
		/*      */
		/* 2011 */
		this.moMat.setIdentity();
		/* 2012 */
		this.moMat.translate(-cam.backBx, -cam.backBy, -cam.backBz);
		/* 2013 */
		this.moMat.matMul(this.tmpMat);
		/*      */
		/* 2015 */
		setLineShader();
		/* 2016 */
		renableVertexArrays();
		/* 2017 */
		this.cameraMatrix = this.moMat.fillDump(this.cameraMatrix);
		/*      */
		/* 2019 */
		this.gl10.glMatrixMode(5888);
		/* 2020 */
		this.gl10.glPushMatrix();
		/* 2021 */
		this.gl10.glLoadIdentity();
		/* 2022 */
		this.gl10.glLoadMatrixf(this.cameraMatrix, 0);
		/*      */
		/* 2024 */
		if (!this.gl20) {
			/* 2025 */
			this.gl10.glDisable(3553);
			/*      */
		}
		/*      */
		/* 2028 */
		this.gl10.glEnableClientState(32884);
		/* 2029 */
		this.gl10.glDisableClientState(32888);
		/* 2030 */
		this.gl10.glDisableClientState(32885);
		/*      */
		/* 2032 */
		disableUnusedStages();
		/*      */
	}

	/*      */
	/*      */
	public void disableLineMode() {
		/* 2036 */
		this.gl10.glLineWidth(1.0F);
		/*      */
		/* 2038 */
		clearShader();
		/* 2039 */
		closeShader();
		/*      */
		/* 2041 */
		this.gl10.glMatrixMode(5888);
		/* 2042 */
		this.gl10.glPopMatrix();
		/*      */
		/* 2044 */
		if (!this.gl20) {
			/* 2045 */
			this.gl10.glEnable(3553);
			/*      */
		}
		/*      */
		/* 2048 */
		initShader();
		/*      */
		/* 2054 */
		this.hasToReEnable = true;
		/* 2055 */
		renableVertexArrays();
		/* 2056 */
		this.hasToReEnable = true;
		/*      */
	}

	/*      */
	/*      */
	public void drawLine(Polyline line) {
		/* 2060 */
		line.getData().rewind();
		/*      */
		/* 2062 */
		RGBColor col = line.getColor();
		/* 2063 */
		setLineColor(col);
		/*      */
		/* 2065 */
		if (col.alpha < 255) {
			/* 2066 */
			setBlendingMode(line.getTransparencyMode());
			/*      */
		}
		/*      */
		/* 2069 */
		initShader();
		/*      */
		/* 2071 */
		if (line.isPointMode())
			/* 2072 */this.gl10.glPointSize(line.getWidth());
		/*      */
		else {
			/* 2074 */
			this.gl10.glLineWidth(line.getWidth());
			/*      */
		}
		/*      */
		/* 2077 */
		this.gl10.glVertexPointer(3, 5126, 12, line.getData());
		/*      */
		/* 2079 */
		if (line.isPointMode())
			/* 2080 */this.gl10.glDrawArrays(0, 0, line.getLength());
		/*      */
		else {
			/* 2082 */
			this.gl10.glDrawArrays(3, 0, line.getLength());
			/*      */
		}
		/*      */
		/* 2085 */
		if (col.alpha < 255)
			/* 2086 */unsetBlendingMode();
		/*      */
	}

	/*      */
	/*      */
	private void setLineColor(RGBColor color)
	/*      */{
		/* 2091 */
		if (!this.gl20) {
			/* 2092 */
			this.gl10.glColor4f(color.getNormalizedRed(),
					color.getNormalizedGreen(), color.getNormalizedBlue(),
					color.getNormalizedAlpha());
			/* 2093 */
			return;
			/*      */
		}
		/*      */
		/* 2096 */
		this.lineCol[0] = color.getNormalizedRed();
		/* 2097 */
		this.lineCol[1] = color.getNormalizedGreen();
		/* 2098 */
		this.lineCol[2] = color.getNormalizedBlue();
		/* 2099 */
		this.lineCol[3] = color.getNormalizedAlpha();
		/* 2100 */
		this.lineColorShader.setUniform("color", this.lineCol);
		/*      */
	}

	/*      */
	/*      */
	private void setLineShader() {
		/* 2104 */
		if (!this.gl20) {
			/* 2105 */
			return;
			/*      */
		}
		/* 2107 */
		if (this.lineColorShader == null) {
			/* 2108 */
			String vert = "uniform mat4 modelViewProjectionMatrix; uniform vec4 color; attribute vec4 position; varying vec4 col; void main() {\tcol=color; gl_Position = modelViewProjectionMatrix * position; }";
			/* 2109 */
			String frag = "precision lowp float; varying vec4 col; void main() { gl_FragColor=col; }";
			/*      */
			/* 2111 */
			this.lineColorShader = new GLSLShader(vert, frag);
			/*      */
		}
		/* 2113 */
		setShader(this.lineColorShader);
		/*      */
	}

	/*      */
	/*      */
	public void drawVertexArray(VisList visList, int start, int end,
			FrameBuffer buffer, T3DScene rage3DScene) {
		/* 2117 */
		CompiledInstance.lastObj = null;
		/* 2118 */
		this.lastActivatedStage = -1;
		/* 2119 */
		this.hasToReEnable = true;
		/*      */
		/* 2121 */
		if (this.init) {

			resetShader();

			disableBlitting();

			enableCompiledPipeline();

			int[] lastMultiTextures = this.lastMultiTextures;

			int[] lastMultiModes = this.lastMultiModes;

			for (int i = 0; i < this.minDriverAndConfig; i++) {

				lastMultiTextures[i] = -9999;

				lastMultiModes[i] = -9999;

			}

			int lastNumber = -9999;

			int lastTNumber = -9999;

			int number = lastNumber;

			int tNumber = lastTNumber;

			T3DObject obj = null;

			rage3DScene.fillAmbientLight(this.ambient);

			for (int i = 0; i < 3; i++) {

				this.ambient[i] *= COLOR_INV;

				if (this.ambient[i] < 0.0F) {

					this.ambient[i] = 0.0F;

				}

			}

			this.ambient[3] = 1.0F;

			this.blending = false;

			for (int ind = start; ind <= end; ind++) {

				boolean hasToReset = false;

				obj = visList.vorg[ind];

				lastNumber = number;

				number = visList.vnum[ind];

				if (obj.oneTextureSet) {

					tNumber = 0;

					lastTNumber = 0;

				} else {

					tNumber = number;

					lastTNumber = lastNumber;

				}

				int endStage = obj.maxStagesUsed;

				if (endStage > this.minDriverAndConfig) {

					endStage = this.minDriverAndConfig;

				}

				boolean isTransparent = obj.isTrans;

				if (isTransparent) {

					if (!this.blending) {

						setBlendingMode(obj.transMode);

						setDepthBuffer();

					}

					hasToReset = true;

					if (ind < end) {

						T3DObject nextObj = visList.vorg[(ind + 1)];

						if ((nextObj.isCompiled())
								&& (nextObj.isTrans == isTransparent)
								&& (nextObj.transMode == obj.transMode)) {

							hasToReset = false;

							this.blending = true;

						}

					}

				}

				setTextures(obj, tNumber, number, buffer, rage3DScene);

				RGBColor col = obj.getAdditionalColor();

				this.cols[0] = col.getNormalizedRed();

				this.cols[1] = col.getNormalizedGreen();

				this.cols[2] = col.getNormalizedBlue();

				float tr = 1.0F;

				if (isTransparent) {

					tr = Config.glTransparencyOffset + obj.transValue
							* Config.glTransparencyMul;

					if (tr > 1.0F) {

						tr = 1.0F;

					}

				}

				this.cols[3] = tr;

				CompiledInstance ci = (CompiledInstance) obj.compiled
						.get(visList.vertexIndex[ind]);

				ci.render(this.myID, this, this.ambient, this.cols,
						this.renderTarget != null, rage3DScene.camera,
						obj.nearestLights, 0);

				if (hasToReset) {

					this.blending = false;

					unsetBlendingMode();

					this.gl10.glDepthMask(true);

					hasToReset = false;

				}

			}

			setTextureMatrix(null);

			this.matrixCache.clear();

			disableCompiledPipeline();

			CompiledInstance.lastObj = null;
			/*      */
		}
		/* 2227 */
		this.lastActivatedStage = -1;
		/*      */
	}

	/*      */
	/*      */
	public void drawWireframe(VisList visList, int start, int end, int color,
			FrameBuffer buffer, T3DScene rage3DScene, int size,
			boolean pointMode) {
		/* 2231 */
		if (this.init) {

			disableFogging();

			disableBlitting();

			clearShader();

			closeShader();

			setLineShader();

			renableVertexArrays();

			this.gl10.glLineWidth(size);

			this.gl10.glPointSize(size);

			if (!this.gl20) {

				this.gl10.glDisable(3553);

			}

			this.gl10.glDisable(2929);

			disableUnusedStages();

			if (!this.gl20) {

				this.gl10.glColor4f(color >> 16 & 0xFF, color >> 8 & 0xFF,
						color & 0xFF, color >> 24 & 0xFF);

			} else {

				this.lineCol[0] = (color >> 16 & 0xFF);

				this.lineCol[1] = (color >> 8 & 0xFF);

				this.lineCol[2] = (color & 0xFF);

				this.lineCol[3] = (color >> 24 & 0xFF);

				this.lineColorShader.setUniform("color", this.lineCol);

			}

			this.ambient[0] = 1.0F;

			this.ambient[1] = 1.0F;

			this.ambient[2] = 1.0F;

			this.ambient[3] = 1.0F;

			this.cols[0] = 1.0F;

			this.cols[1] = 1.0F;

			this.cols[2] = 1.0F;

			this.cols[3] = 1.0F;

			CompiledInstance.lastVertexBuffer = null;

			this.lineMode = true;

			for (int ind = start; ind <= end; ind++) {

				T3DObject obj = visList.vorg[ind];

				CompiledInstance ci = (CompiledInstance) obj.compiled
						.get(visList.vertexIndex[ind]);

				initShader();

				ci.render(this.myID, this, this.ambient, this.cols,
						this.renderTarget != null, rage3DScene.camera,
						obj.nearestLights, pointMode ? 2 : 1);

			}

			this.lineMode = false;

			clearShader();

			closeShader();

			if (!this.gl20) {

				this.gl10.glEnable(3553);

			}

			this.gl10.glEnable(2929);

			initShader();

			this.hasToReEnable = true;

			renableVertexArrays();

			this.hasToReEnable = true;

			disableCompiledPipeline();

			this.matrixCache.clear();

			this.gl10.glLineWidth(1.0F);

			this.gl10.glPointSize(1.0F);

			CompiledInstance.lastObj = null;

			CompiledInstance.lastVertexBuffer = null;

		}
	}

	private void setTextureMatrix(Matrix m) {
		if (this.textureMatrixSet) {
			activateStage(0);
			this.gl10.glMatrixMode(5890);
			this.gl10.glPopMatrix();
			this.textureMatrixSet = false;
		}
		if (m == null) {
			return;
		}
		activateStage(0);
		this.gl10.glMatrixMode(5890);
		this.gl10.glPushMatrix();
		m.fillDump(this.dumpy);
		this.gl10.glLoadMatrixf(this.dumpy, 0);
		this.textureMatrixSet = true;
	}

	private void setTextures(T3DObject obj, int number, int modeNumber,
			FrameBuffer buffer, T3DScene rage3DScene) {
		int texInd = 0;
		if (obj.oneTextureSet) {
			number = 0;
		}
		Texture texture = this.texMan.textures[obj.texture[number]];
		if ((texture.getOpenGLID(this.myID) == 0)
				|| (texture.getMarker(this.myID) == Texture.MARKER_DELETE_AND_UPLOAD)) {
			texture.setMarker(this.myID, Texture.MARKER_NOTHING);
			if (texture != this.renderTarget)
			/*      */{
				if (texture.getOpenGLID(this.myID) != 0) {
					removeTexture(texture);
				}
				convertTexture(texture);
			}
			this.lastTextures[0] = -1;
		}
		setTextureMatrix(obj.textureMatrix);
		bindAndProject(0, texture);
		if (obj.usesMultiTexturing) {
			if (this.maxStages < obj.maxStagesUsed) {
				this.maxStages = obj.maxStagesUsed;
				if (this.maxStages > this.minDriverAndConfig) {
					this.maxStages = this.minDriverAndConfig;
				}
			}
			int len = obj.multiTex.length;
			for (int i = 1; i < this.maxStages; i++)
			/*      */{
				texInd = i - 1;
				int tt = texInd >= len ? -1 : obj.multiTex[texInd][number];
				if (tt != -1) {
					Texture tex = this.texMan.textures[tt];
					if (tex.enabled) {
						int mode = modeMap[obj.multiMode[texInd][modeNumber]];
						if ((Config.glRevertADDtoMODULATE) && (mode == 260)) {
							mode = 8448;
						}
						if (!this.stageInitialized[i])
							/* 2381 */initTextureStage(i, mode);
						else {
							switchTextureMode(i, mode);
						}
						if ((tex.getOpenGLID(this.myID) == 0)
								|| (tex.getMarker(this.myID) == Texture.MARKER_DELETE_AND_UPLOAD)) {
							tex.setMarker(this.myID, Texture.MARKER_NOTHING);
							if (tex != this.renderTarget)
							/*      */{
								if (tex.getOpenGLID(this.myID) != 0) {
									removeTexture(tex);
								}
								convertTexture(tex);
							}
							this.lastTextures[i] = -1;
						}
						bindAndProject(i, tex);
					} else {
						disableStage(i);
					}
				} else {
					disableStage(i);
				}
			}
		} else {
			disableUnusedStages();
		}
	}

	private boolean hasExtension(String ext)
	/*      */{
		if (this.extensions == null) {
			this.extensions = this.gl10.glGetString(7939).toLowerCase();
		}
		return this.extensions.contains(ext);
	}

	public void sync() {
		this.gl10.glFlush();
		this.gl10.glFinish();
	}

	public void flush() {
		this.gl10.glFlush();
	}
}
