package toto.util.zip.streams;

import java.io.IOException;
import java.io.InputStream;

import toto.util.zip.streams.TarConstants;
import toto.util.zip.streams.TarEntry;

/**
 * Stream for reading <a
 * href="http://en.wikipedia.org/wiki/Tar_(computing)">.tar</a> files
 * 
 * @see TarEntry
 * @see toto.util.zip.Tar
 */
public class TarInputStream extends java.io.FilterInputStream {

	private static final int SKIP_BUFFER_SIZE = 2048;
	private TarEntry currentEntry;
	private long currentFileSize;
	private long bytesRead;
	private boolean defaultSkip = false;

	public TarInputStream(final InputStream in) {
		super(in);
		currentFileSize = 0;
		bytesRead = 0;
	}

	@Override
	public boolean markSupported() {
		return false;
	}

	/**
	 * Not supported
	 *
	 */
	@Override
	public synchronized void mark(final int readlimit) {
	}

	/**
	 * Not supported
	 *
	 */
	@Override
	public synchronized void reset() throws IOException {
		throw new IOException("mark/reset not supported");
	}

	/**
	 * Read a byte
	 *
	 * @see java.io.FilterInputStream#read()
	 */
	@Override
	public int read() throws IOException {
		final byte[] buf = new byte[1];

		final int res = this.read(buf, 0, 1);

		if (res != -1) {
			return buf[0];
		}

		return res;
	}

	/**
	 * Checks if the bytes being read exceed the entry size and adjusts the byte
	 * array length. Updates the byte counters
	 *
	 *
	 * @see java.io.FilterInputStream#read(byte[], int, int)
	 */
	@Override
	public int read(final byte[] b, final int off, int len) throws IOException {
		if (currentEntry != null) {
			if (currentFileSize == currentEntry.getSize()) {
				return -1;
			} else if ((currentEntry.getSize() - currentFileSize) < len) {
				len = (int) (currentEntry.getSize() - currentFileSize);
			}
		}

		final int br = super.read(b, off, len);

		if (br != -1) {
			if (currentEntry != null) {
				currentFileSize += br;
			}

			bytesRead += br;
		}

		return br;
	}

	/**
	 * Returns the next entry in the tar file
	 *
	 * @return TarEntry
	 * @throws java.io.IOException
	 */
	public TarEntry getNextEntry() throws IOException {
		closeCurrentEntry();

		final byte[] header = new byte[TarConstants.HEADER_BLOCK];
		final byte[] theader = new byte[TarConstants.HEADER_BLOCK];
		int tr = 0;

		// Read full header
		while (tr < TarConstants.HEADER_BLOCK) {
			final int res = read(theader, 0, TarConstants.HEADER_BLOCK - tr);

			if (res < 0) {
				break;
			}

			System.arraycopy(theader, 0, header, tr, res);
			tr += res;
		}

		// Check if record is null
		boolean eof = true;
		for (final byte b : header) {
			if (b != 0) {
				eof = false;
				break;
			}
		}

		if (!eof) {
			bytesRead += header.length;
			currentEntry = new TarEntry(header);
		}

		return currentEntry;
	}

	/**
	 * Closes the current tar entry
	 *
	 * @throws java.io.IOException
	 */
	protected void closeCurrentEntry() throws IOException {
		if (currentEntry != null) {
			if (currentEntry.getSize() > currentFileSize) {
				// Not fully read, skip rest of the bytes
				long bs = 0;
				while (bs < currentEntry.getSize() - currentFileSize) {
					final long res = skip(currentEntry.getSize()
							- currentFileSize - bs);

					if (res == 0
							&& currentEntry.getSize() - currentFileSize > 0) {
						throw new IOException("Possible tar file corruption");
					}

					bs += res;
				}
			}

			currentEntry = null;
			currentFileSize = 0L;
			skipPad();
		}
	}

	/**
	 * Skips the pad at the end of each tar entry file content
	 *
	 * @throws java.io.IOException
	 */
	protected void skipPad() throws IOException {
		if (bytesRead > 0) {
			final int extra = (int) (bytesRead % TarConstants.DATA_BLOCK);

			if (extra > 0) {
				long bs = 0;
				while (bs < TarConstants.DATA_BLOCK - extra) {
					final long res = skip(TarConstants.DATA_BLOCK - extra - bs);
					bs += res;
				}
			}
		}
	}

	/**
	 * Skips 'n' bytes on the InputStream<br>
	 * Overrides default implementation of skip
	 * 
	 */
	@Override
	public long skip(final long n) throws IOException {
		if (defaultSkip) {
			// use skip method of parent stream
			// may not work if skip not implemented by parent
			return super.skip(n);
		}

		if (n <= 0) {
			return 0;
		}

		long left = n;
		final byte[] sBuff = new byte[SKIP_BUFFER_SIZE];

		while (left > 0) {
			final int res = read(sBuff, 0,
					(int) (left < SKIP_BUFFER_SIZE ? left : SKIP_BUFFER_SIZE));
			if (res < 0) {
				break;
			}
			left -= res;
		}

		return n - left;
	}

	public boolean isDefaultSkip() {
		return defaultSkip;
	}

	public void setDefaultSkip(final boolean defaultSkip) {
		this.defaultSkip = defaultSkip;
	}
}
