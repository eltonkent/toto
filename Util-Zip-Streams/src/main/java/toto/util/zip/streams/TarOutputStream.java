package toto.util.zip.streams;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import toto.util.zip.streams.TarConstants;
import toto.util.zip.streams.TarEntry;

/**
 * Stream for writing <a
 * href="http://en.wikipedia.org/wiki/Tar_(computing)">.tar</a> archive files.
 * 
 * @see toto.util.zip.Tar
 */
public class TarOutputStream extends FilterOutputStream {
	private long bytesWritten;
	private long currentFileSize;
	private TarEntry currentEntry;

	public TarOutputStream(final OutputStream out) {
		super(out);
		bytesWritten = 0;
		currentFileSize = 0;
	}

	/**
	 * Appends the EOF record and closes the stream
	 * 
	 * @see java.io.FilterOutputStream#close()
	 */
	@Override
	public void close() throws IOException {
		closeCurrentEntry();
		write(new byte[TarConstants.EOF_BLOCK]);
		super.close();
	}

	/**
	 * Writes a byte to the stream and updates byte counters
	 * 
	 * @see java.io.FilterOutputStream#write(int)
	 */
	@Override
	public void write(final int b) throws IOException {
		super.write(b);
		bytesWritten += 1;

		if (currentEntry != null) {
			currentFileSize += 1;
		}
	}

	/**
	 * Checks if the bytes being written exceed the current entry size.
	 * 
	 * @see java.io.FilterOutputStream#write(byte[], int, int)
	 */
	@Override
	public void write(final byte[] b, final int off, final int len)
			throws IOException {
		if (currentEntry != null && !currentEntry.isDirectory()) {
			if (currentEntry.getSize() < currentFileSize + len) {
				throw new IOException("The current entry["
						+ currentEntry.getName() + "] size["
						+ currentEntry.getSize()
						+ "] is smaller than the bytes["
						+ (currentFileSize + len) + "] being written.");
			}
		}

		super.write(b, off, len);
	}

	/**
	 * Writes the next tar entry header on the stream
	 * 
	 * @param entry
	 * @throws java.io.IOException
	 */
	public void putNextEntry(final TarEntry entry) throws IOException {
		closeCurrentEntry();

		final byte[] header = new byte[TarConstants.HEADER_BLOCK];
		entry.writeEntryHeader(header);

		write(header);

		currentEntry = entry;
	}

	/**
	 * Closes the current tar entry
	 *
	 * @throws java.io.IOException
	 */
	protected void closeCurrentEntry() throws IOException {
		if (currentEntry != null) {
			if (currentEntry.getSize() > currentFileSize) {
				throw new IOException("The current entry["
						+ currentEntry.getName() + "] of size["
						+ currentEntry.getSize()
						+ "] has not been fully written.");
			}

			currentEntry = null;
			currentFileSize = 0;

			pad();
		}
	}

	/**
	 * Pads the last content block
	 *
	 * @throws java.io.IOException
	 */
	protected void pad() throws IOException {
		if (bytesWritten > 0) {
			final int extra = (int) (bytesWritten % TarConstants.DATA_BLOCK);

			if (extra > 0) {
				write(new byte[TarConstants.DATA_BLOCK - extra]);
			}
		}
	}
}
