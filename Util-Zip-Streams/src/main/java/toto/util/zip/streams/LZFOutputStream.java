/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.util.zip.streams;

import java.io.IOException;
import java.io.OutputStream;

import toto.util.zip.LZF;

/**
 * An output stream to write an LZF stream. The data is automatically
 * compressed.
 */
public class LZFOutputStream extends OutputStream {

	/**
	 * The file header of a LZF file.
	 */
	static final int MAGIC = ('H' << 24) | ('2' << 16) | ('I' << 8) | 'S';

	private final OutputStream out;
	private final LZF compress = new LZF();
	private final byte[] buffer;
	private int pos;
	private byte[] outBuffer;

	public LZFOutputStream(final OutputStream out) throws IOException {
		this.out = out;
		final int len = 128 * 1024;
		buffer = new byte[len];
		ensureOutput(len);
		writeInt(MAGIC);
	}

	private void ensureOutput(final int len) {
		// TODO calculate the maximum overhead (worst case) for the output
		// buffer
		final int outputLen = (len < 100 ? len + 100 : len) * 2;
		if (outBuffer == null || outBuffer.length < outputLen) {
			outBuffer = new byte[outputLen];
		}
	}

	@Override
	public void write(final int b) throws IOException {
		if (pos >= buffer.length) {
			flush();
		}
		buffer[pos++] = (byte) b;
	}

	private void compressAndWrite(final byte[] buff, final int len)
			throws IOException {
		if (len > 0) {
			ensureOutput(len);
			final int compressed = compress.compress(buff, len, outBuffer, 0);
			if (compressed > len) {
				writeInt(-len);
				out.write(buff, 0, len);
			} else {
				writeInt(compressed);
				writeInt(len);
				out.write(outBuffer, 0, compressed);
			}
		}
	}

	private void writeInt(final int x) throws IOException {
		out.write((byte) (x >> 24));
		out.write((byte) (x >> 16));
		out.write((byte) (x >> 8));
		out.write((byte) x);
	}

	@Override
	public void write(final byte[] buff, int off, int len) throws IOException {
		while (len > 0) {
			final int copy = Math.min(buffer.length - pos, len);
			System.arraycopy(buff, off, buffer, pos, copy);
			pos += copy;
			if (pos >= buffer.length) {
				flush();
			}
			off += copy;
			len -= copy;
		}
	}

	@Override
	public void flush() throws IOException {
		compressAndWrite(buffer, pos);
		pos = 0;
	}

	@Override
	public void close() throws IOException {
		flush();
		out.close();
	}

}
