package toto.util.zip.streams;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * Preamble header for {@link SnappyOutputStream}.
 * 
 * <p>
 * The magic header is the following 8 bytes data:
 * 
 * <pre>
 * -126, 'S', 'N', 'A', 'P', 'P', 'Y', 0
 * </pre>
 * 
 * </p>
 * 
 * @author leo
 * 
 */
public class SnappyCodec {
	public static final byte[] MAGIC_HEADER = new byte[] { -126, 'S', 'N', 'A',
			'P', 'P', 'Y', 0 };
	public static final int MAGIC_LEN = 8;

	public static final int DEFAULT_VERSION = 1;
	public static final int MINIMUM_COMPATIBLE_VERSION = 1;

	public final byte[] magic;
	public final int version;
	public final int compatibleVersion;

	private SnappyCodec(final byte[] magic, final int version,
                        final int compatibleVersion) {
		this.magic = magic;
		this.version = version;
		this.compatibleVersion = compatibleVersion;
	}

	@Override
	public String toString() {
		return String.format("version:%d, compatible version:%d", version,
				compatibleVersion);
	}

	public static int headerSize() {
		return MAGIC_LEN + 4 * 2;
	}

	public void writeHeader(final OutputStream out) throws IOException {
		final ByteArrayOutputStream header = new ByteArrayOutputStream();
		final DataOutputStream d = new DataOutputStream(header);
		d.write(magic, 0, MAGIC_LEN);
		d.writeInt(version);
		d.writeInt(compatibleVersion);
		d.close();
		out.write(header.toByteArray(), 0, header.size());
	}

	public boolean isValidMagicHeader() {
		return Arrays.equals(MAGIC_HEADER, magic);
	}

	public static SnappyCodec readHeader(final InputStream in)
			throws IOException {
		final DataInputStream d = new DataInputStream(in);
		final byte[] magic = new byte[MAGIC_LEN];
		d.readFully(magic, 0, MAGIC_LEN);
		final int version = d.readInt();
		final int compatibleVersion = d.readInt();
		return new SnappyCodec(magic, version, compatibleVersion);
	}

	public static SnappyCodec currentHeader() {
		return new SnappyCodec(MAGIC_HEADER, DEFAULT_VERSION,
				MINIMUM_COMPATIBLE_VERSION);
	}

}