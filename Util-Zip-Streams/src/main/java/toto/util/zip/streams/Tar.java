package toto.util.zip.streams;

import com.mi.toto.Conditions;
import toto.io.file.FileUtils;

import java.io.*;

/**
 * Utility for working with TAR archive files.
 * 
 * @see TarOutputStream
 * @see TarInputStream
 */
public class Tar {

	/**
	 * @param filesToAdd
	 * @param destination
	 * @throws IOException
	 */
	public static void tar(File[] filesToAdd, File destination)
			throws IOException {
		if (!destination.exists()) {
			destination.createNewFile();
		}
		FileOutputStream fos = new FileOutputStream(destination);
		TarOutputStream out = new TarOutputStream(new BufferedOutputStream(fos));
		for (File f : filesToAdd) {
			out.putNextEntry(new TarEntry(f, f.getName()));
			out.write(FileUtils.readFileToByteArray(f));
			out.flush();
		}
		out.close();
	}

	/**
	 * UnTar a given tar file.
	 * 
	 * @param tarFile
	 * @param destinationDir
	 *            to put the extracted contents
	 */
	public static void unTar(File tarFile, File destinationDir)
			throws IOException {
		Conditions.checkArgument(destinationDir.isDirectory(),
				"Destination directory location is invalid");
		TarInputStream tis = new TarInputStream(new BufferedInputStream(
				new FileInputStream(tarFile)));
		TarEntry entry;
		while ((entry = tis.getNextEntry()) != null) {
			int count;
			byte data[] = new byte[2048];
			FileOutputStream fos = new FileOutputStream(
					destinationDir.toString() + entry.getName());
			BufferedOutputStream dest = new BufferedOutputStream(fos);

			while ((count = tis.read(data)) != -1) {
				dest.write(data, 0, count);
			}

			dest.flush();
			dest.close();
		}

		tis.close();
	}

	/**
	 * Determines the tar file size of the given folder/file path
	 * 
	 * @param path
	 * @return
	 */
	public static long calculateTarSize(File path) {
		return tarSize(path) + TarConstants.EOF_BLOCK;
	}

	private static long tarSize(File dir) {
		long size = 0;

		if (dir.isFile()) {
			return entrySize(dir.length());
		} else {
			File[] subFiles = dir.listFiles();
			if (subFiles != null && subFiles.length > 0) {
				for (File file : subFiles) {
					if (file.isFile()) {
						size += entrySize(file.length());
					} else {
						size += tarSize(file);
					}
				}
			} else {
				// Empty folder header
				return TarConstants.HEADER_BLOCK;
			}
		}

		return size;
	}

	private static long entrySize(long fileSize) {
		long size = 0;
		size += TarConstants.HEADER_BLOCK; // Header
		size += fileSize; // File size

		long extra = size % TarConstants.DATA_BLOCK;

		if (extra > 0) {
			size += (TarConstants.DATA_BLOCK - extra); // pad
		}
		return size;
	}

}
