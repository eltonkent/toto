/**
 * Custom embedded database implementations and Utilities for extracting and inserting  with Android's SQLite database.
 * <p>
 * Contains a document database (JSONDb), Key-Value database (KVDb) and Object oriented database (ODb).
 * </p>
 *
 */
package toto.db;