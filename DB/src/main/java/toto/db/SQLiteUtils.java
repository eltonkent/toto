/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.db;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mi.toto.ToTo;
import toto.beans.Introspector;
import toto.io.IOUtils;
import toto.lang.reflect.MethodUtils;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

public final class SQLiteUtils {

	/**
	 * Compares two cursors to see if they contain the same data.
	 * 
	 * @return Returns true of the cursors contain the same data and are not
	 *         null, false otherwise
	 */
	public static boolean compareCursors(final Cursor c1, final Cursor c2) {
		if (c1 == null || c2 == null)
			return false;

		final int numColumns = c1.getColumnCount();
		if (numColumns != c2.getColumnCount())
			return false;

		if (c1.getCount() != c2.getCount())
			return false;

		c1.moveToPosition(-1);
		c2.moveToPosition(-1);
		while (c1.moveToNext() && c2.moveToNext()) {
			for (int i = 0; i < numColumns; i++) {
				if (!TextUtils.equals(c1.getString(i), c2.getString(i)))
					return false;
			}
		}

		return true;
	}

	/**
	 * This method cleans up the cache's created by the toBean methods.
	 * <p>
	 * Should be called in critical memory conditions or a application shutdown.
	 * </p>
	 */
	public static void doCacheCleanUp() {
		Introspector.flushCaches();
		MethodUtils.flushCaches();
	}

	public static List<String> getColumns(final SQLiteDatabase db,
			final String tableName) {
		List<String> ar = null;
		Cursor c = null;
		try {
			c = db.rawQuery("select * from " + tableName + " limit 1", null);
			if (c != null) {
				ar = new ArrayList<String>(Arrays.asList(c.getColumnNames()));
			}
		} catch (final Exception e) {
			ToTo.logException(e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return ar;
	}

	public static MatrixCursor matrixCursorFromCursor(final Cursor cursor) {
		final MatrixCursor newCursor = new MatrixCursor(cursor.getColumnNames());
		final int numColumns = cursor.getColumnCount();
		final String data[] = new String[numColumns];
		cursor.moveToPosition(-1);
		while (cursor.moveToNext()) {
			for (int i = 0; i < numColumns; i++) {
				data[i] = cursor.getString(i);
			}
			newCursor.addRow(data);
		}
		return newCursor;
	}

	public static String printCursor(final Cursor cursor) {
		final StringBuilder retval = new StringBuilder();

		retval.append("|");
		final int numcolumns = cursor.getColumnCount();
		for (int column = 0; column < numcolumns; column++) {
			final String columnName = cursor.getColumnName(column);
			retval.append(String.format("%-20s |",
					columnName.substring(0, Math.min(20, columnName.length()))));
		}
		retval.append("\n|");
		for (int column = 0; column < numcolumns; column++) {
			for (int i = 0; i < 21; i++) {
				retval.append("-");
			}
			retval.append("+");
		}
		retval.append("\n|");

		while (cursor.moveToNext()) {
			for (int column = 0; column < numcolumns; column++) {
				String columnValue = cursor.getString(column);
				if (columnValue != null) {
					columnValue = columnValue.substring(0,
							Math.min(20, columnValue.length()));
				}
				retval.append(String.format("%-20s |", columnValue));
			}
			retval.append("\n");
		}

		return retval.toString();
	}

	/**
	 * Convert a <code>ResultSet</code> row into an <code>Object[]</code>. This
	 * implementation copies column values into the array in the same order
	 * they're returned from the <code>ResultSet</code>. Array elements will be
	 * setKey to <code>null</code> if the column was SQL NULL.
	 * 
	 * @param rs
	 *            ResultSet that supplies the array data
	 * @throws SQLException
	 *             if a database access error occurs
	 * @return the newly created array
	 */
	public Object[] toArray(final ResultSet rs) throws SQLException {
		final ResultSetMetaData meta = rs.getMetaData();
		final int cols = meta.getColumnCount();
		final Object[] result = new Object[cols];

		for (int i = 0; i < cols; i++) {
			result[i] = rs.getObject(i + 1);
		}

		return result;
	}

	public static void copyDataBaseFromAssets(final Context context,
			final String dbName) throws IOException {
		// Path to the just created empty db
		final File dbDir = new File("data/data/" + context.getPackageName()
				+ "/databases/");
		dbDir.mkdirs();
		final String outFileName = dbDir.getAbsolutePath() + dbName;
		IOUtils.copy(context.getAssets().open(dbName), new File(outFileName));

	}

	/**
	 * Create an in memory database
	 * 
	 * @return
	 */
	public static SQLiteDatabase createMemoryBackedDatabase() {
		return SQLiteDatabase.create(null);
	}
}
