package toto.db;

import java.io.File;

import com.mi.toto.Conditions;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.sqlite.SQLiteDatabase;

/**
 * Use this DB context if the SQLite DB is stored on an external storage
 * location.
 * 
 * @author Mobifluence Interactive
 * 
 */
public class ExternalStorageDBContext extends ContextWrapper {

	private final File storagePath;

	/**
	 * 
	 * @param base
	 *            Application context.
	 * @param storagePath
	 *            directory where we save the database.
	 * @param dbName
	 *            Database file name.
	 */
	public ExternalStorageDBContext(final Context base, final File storagePath) {
		super(base);
		this.storagePath = storagePath;
		Conditions.checkArgument(storagePath.isDirectory(),
				"DB storage path is not a directory");
	}

	@Override
	public File getDatabasePath(final String name) {
		return new File(storagePath.toString() + name);
	}

	@Override
	public SQLiteDatabase openOrCreateDatabase(final String name,
			final int mode, final SQLiteDatabase.CursorFactory factory) {
		final SQLiteDatabase result = SQLiteDatabase.openOrCreateDatabase(
				getDatabasePath(name), null);
		return result;
	}
}
