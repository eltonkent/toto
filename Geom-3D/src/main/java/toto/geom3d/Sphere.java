package toto.geom3d;

import java.io.Serializable;

/**
 * Encapsulates a 3D sphere with a center and a radius
 */
public class Sphere implements Serializable {
	private static final long serialVersionUID = -6487336868908521596L;
	/**
	 * the radius of the sphere *
	 */
	public float radius;
	/**
	 * the center of the sphere *
	 */
	public final Vector3D center;

	/**
	 * Constructs a sphere with the given center and radius
	 * 
	 * @param center
	 *            The center
	 * @param radius
	 *            The radius
	 */
	public Sphere(Vector3D center, float radius) {
		this.center = new Vector3D(center);
		this.radius = radius;
	}

	/**
	 * @param sphere
	 *            the other sphere
	 * @return whether this and the other sphere overlap
	 */
	public boolean overlaps(Sphere sphere) {
		return center.dst2(sphere.center) < (radius + sphere.radius)
				* (radius + sphere.radius);
	}
}