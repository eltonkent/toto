package toto.geom3d;

import toto.geom3d.Plane.PlaneSide;

/**
 * A truncated rectangular pyramid. Used to define the viewable region and its
 * projection onto the screen.
 */
public class Frustum {
	protected static final Vector3D[] clipSpacePlanePoints = {
			new Vector3D(-1, -1, -1), new Vector3D(1, -1, -1),
			new Vector3D(1, 1, -1),
			new Vector3D(-1, 1, -1), // near clip
			new Vector3D(-1, -1, 1), new Vector3D(1, -1, 1),
			new Vector3D(1, 1, 1), new Vector3D(-1, 1, 1) }; // far clip
	protected static final float[] clipSpacePlanePointsArray = new float[8 * 3];

	static {
		int j = 0;
		for (Vector3D v : clipSpacePlanePoints) {
			clipSpacePlanePointsArray[j++] = v.x;
			clipSpacePlanePointsArray[j++] = v.y;
			clipSpacePlanePointsArray[j++] = v.z;
		}
	}

	/**
	 * the six clipping planes, near, far, left, right, top, bottm *
	 */
	public final Plane[] planes = new Plane[6];

	/**
	 * eight points making up the near and far clipping "rectangles". order is
	 * counter clockwise, starting at bottom left *
	 */
	public final Vector3D[] planePoints = { new Vector3D(), new Vector3D(),
			new Vector3D(), new Vector3D(), new Vector3D(), new Vector3D(),
			new Vector3D(), new Vector3D() };
	protected final float[] planePointsArray = new float[8 * 3];

	public Frustum() {
		for (int i = 0; i < 6; i++) {
			planes[i] = new Plane(new Vector3D(), 0);
		}
	}

	/**
	 * Updates the clipping plane's based on the given inverse combined
	 * projection and view matrix
	 * 
	 * @param inverseProjectionView
	 *            the combined projection and view matrices.
	 */
	public void update(Matrix4 inverseProjectionView) {
		System.arraycopy(clipSpacePlanePointsArray, 0, planePointsArray, 0,
				clipSpacePlanePointsArray.length);
		Matrix4.prjNative(inverseProjectionView.val, planePointsArray, 0, 8, 3);
		for (int i = 0, j = 0; i < 8; i++) {
			Vector3D v = planePoints[i];
			v.x = planePointsArray[j++];
			v.y = planePointsArray[j++];
			v.z = planePointsArray[j++];
		}

		planes[0].set(planePoints[1], planePoints[0], planePoints[2]);
		planes[1].set(planePoints[4], planePoints[5], planePoints[7]);
		planes[2].set(planePoints[0], planePoints[4], planePoints[3]);
		planes[3].set(planePoints[5], planePoints[1], planePoints[6]);
		planes[4].set(planePoints[2], planePoints[3], planePoints[6]);
		planes[5].set(planePoints[4], planePoints[0], planePoints[1]);
	}

	/**
	 * Returns whether the point is in the frustum.
	 * 
	 * @param point
	 *            The point
	 * @return Whether the point is in the frustum.
	 */
	public boolean pointInFrustum(Vector3D point) {
		for (int i = 0; i < planes.length; i++) {
			Plane.PlaneSide result = planes[i].testPoint(point);
			if (result == Plane.PlaneSide.Back)
				return false;
		}
		return true;
	}

	/**
	 * Returns whether the point is in the frustum.
	 * 
	 * @param x
	 *            The X coordinate of the point
	 * @param y
	 *            The Y coordinate of the point
	 * @param z
	 *            The Z coordinate of the point
	 * @return Whether the point is in the frustum.
	 */
	public boolean pointInFrustum(float x, float y, float z) {
		for (int i = 0; i < planes.length; i++) {
			PlaneSide result = planes[i].testPoint(x, y, z);
			if (result == PlaneSide.Back)
				return false;
		}
		return true;
	}

	/**
	 * Returns whether the given sphere is in the frustum.
	 * 
	 * @param center
	 *            The center of the sphere
	 * @param radius
	 *            The radius of the sphere
	 * @return Whether the sphere is in the frustum
	 */
	public boolean sphereInFrustum(Vector3D center, float radius) {
		for (int i = 0; i < 6; i++)
			if ((planes[i].normal.x * center.x + planes[i].normal.y * center.y + planes[i].normal.z
					* center.z) < (-radius - planes[i].d))
				return false;
		return true;
	}

	/**
	 * Returns whether the given sphere is in the frustum.
	 * 
	 * @param x
	 *            The X coordinate of the center of the sphere
	 * @param y
	 *            The Y coordinate of the center of the sphere
	 * @param z
	 *            The Z coordinate of the center of the sphere
	 * @param radius
	 *            The radius of the sphere
	 * @return Whether the sphere is in the frustum
	 */
	public boolean sphereInFrustum(float x, float y, float z, float radius) {
		for (int i = 0; i < 6; i++)
			if ((planes[i].normal.x * x + planes[i].normal.y * y + planes[i].normal.z
					* z) < (-radius - planes[i].d))
				return false;
		return true;
	}

	/**
	 * Returns whether the given sphere is in the frustum not checking whether
	 * it is behind the near and far clipping plane.
	 * 
	 * @param center
	 *            The center of the sphere
	 * @param radius
	 *            The radius of the sphere
	 * @return Whether the sphere is in the frustum
	 */
	public boolean sphereInFrustumWithoutNearFar(Vector3D center, float radius) {
		for (int i = 2; i < 6; i++)
			if ((planes[i].normal.x * center.x + planes[i].normal.y * center.y + planes[i].normal.z
					* center.z) < (-radius - planes[i].d))
				return false;
		return true;
	}

	/**
	 * Returns whether the given sphere is in the frustum not checking whether
	 * it is behind the near and far clipping plane.
	 * 
	 * @param x
	 *            The X coordinate of the center of the sphere
	 * @param y
	 *            The Y coordinate of the center of the sphere
	 * @param z
	 *            The Z coordinate of the center of the sphere
	 * @param radius
	 *            The radius of the sphere
	 * @return Whether the sphere is in the frustum
	 */
	public boolean sphereInFrustumWithoutNearFar(float x, float y, float z,
			float radius) {
		for (int i = 2; i < 6; i++)
			if ((planes[i].normal.x * x + planes[i].normal.y * y + planes[i].normal.z
					* z) < (-radius - planes[i].d))
				return false;
		return true;
	}

	/**
	 * Returns whether the given {@link BoundingBox} is in the frustum.
	 * 
	 * @param bounds
	 *            The bounding box
	 * @return Whether the bounding box is in the frustum
	 */
	public boolean boundsInFrustum(BoundingBox bounds) {
		Vector3D[] corners = bounds.getCorners();
		int len = corners.length;

		for (int i = 0, len2 = planes.length; i < len2; i++) {
			int out = 0;

			for (int j = 0; j < len; j++)
				if (planes[i].testPoint(corners[j]) == PlaneSide.Back)
					out++;

			if (out == 8)
				return false;
		}

		return true;
	}

}