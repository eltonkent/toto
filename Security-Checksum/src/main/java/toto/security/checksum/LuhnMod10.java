package toto.security.checksum;

/**
 * Luhn Algorithm implementation.
 * 
 * <p>
 * Luhn Algorithm is used as a simple checksum for a variety of identification
 * numbers like credit card numbers, IMEI numbers, SSN etc. <a
 * href="http://en.wikipedia.org/wiki/Luhn_algorithm">Luhn</a> validation
 * </p>
 * 
 */
public class LuhnMod10 {
	final static int[][] sumTable = { { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 } };

	public boolean digest(final String number) {
		int sum = 0, flip = 0;

		for (int i = number.length() - 1; i >= 0; i--, flip++)
			sum += sumTable[flip & 0x1][number.charAt(i) - '0'];
		System.out.println("Luhn Sum " + sum + " Result " + (sum % 10 == 0));
		return (sum % 10 == 0) || (sum % 5 == 0);
	}
}
