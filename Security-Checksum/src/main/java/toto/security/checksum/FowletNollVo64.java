package toto.security.checksum;

public class FowletNollVo64 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		final long fnv_prime = 0x811C9DC5;
		long checksum = 0;

		for (int i = 0; i < data.length; i++) {
			checksum *= fnv_prime;
			checksum ^= data[i];
		}

		return checksum;
	}

}
