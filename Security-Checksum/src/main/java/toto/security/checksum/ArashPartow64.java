package toto.security.checksum;

public class ArashPartow64 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		long checksum = 0xAAAAAAAA;
		for (int i = 0; i < data.length; i++) {
			if ((i & 1) == 0) {
				checksum ^= ((checksum << 7) ^ data[i] * (checksum >> 3));
			} else {
				checksum ^= (~((checksum << 11) + data[i] ^ (checksum >> 5)));
			}
		}
		return checksum;
	}

}
