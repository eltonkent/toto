package toto.security.checksum;

public class SDBM64 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		long checksum = 0;
		for (int i = 0; i < data.length; i++) {
			checksum = data[i] + (checksum << 6) + (checksum << 16) - checksum;
		}
		return checksum;
	}

}
