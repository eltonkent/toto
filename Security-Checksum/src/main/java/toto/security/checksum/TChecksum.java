package toto.security.checksum;

public abstract class TChecksum {

	public abstract long digest(byte[] data);

	public String toHexString(final byte[] data) {
		return Long.toHexString(digest(data));
	}
}
