package toto.security.checksum;

public class RobertSedgwicks64 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		final int b = 378551;
		int a = 63689;
		long checksum = 0;

		for (int i = 0; i < data.length; i++) {
			checksum = checksum * a + data[i];
			a = a * b;
		}

		return checksum;
	}

}
