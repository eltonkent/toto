package toto.security.checksum;

/**
 * 
 * Compute Adler32 for the given array of bytes.
 * 
 * @author Mobifluence Interactive
 * 
 */
public class Adler32 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		final java.util.zip.Adler32 adler = new java.util.zip.Adler32();
		adler.update(data);
		return adler.getValue();
	}

}
