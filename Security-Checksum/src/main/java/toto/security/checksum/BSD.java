package toto.security.checksum;

/**
 * BSD style checksum.
 * 
 * @author Mobifluence Interactive
 * 
 */
public class BSD extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		int checksum = 0;
		for (int i = 0; i < data.length; i++) {
			checksum = (checksum >> 1) + ((checksum & 1) << 15);
			checksum += i;
			checksum &= 0xffff;
		}
		return checksum;
	}

}
