package toto.security.checksum;

public class PeterWeinberger32 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		final long BitsInUnsignedInt = 4 * 8;
		final long ThreeQuarters = (BitsInUnsignedInt * 3) / 4;
		final long OneEighth = BitsInUnsignedInt / 8;
		final long HighBits = (long) (0xFFFFFFFF) << (BitsInUnsignedInt - OneEighth);
		long checksum = 0;
		long test = 0;

		for (int i = 0; i < data.length; i++) {
			checksum = (checksum << OneEighth) + data[i];
			if ((test = checksum & HighBits) != 0) {
				checksum = ((checksum ^ (test >> ThreeQuarters)) & (~HighBits));
			}
		}

		return checksum;
	}

}
