package toto.security.checksum;

public class Fletcher32 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		int sum1 = 0xffff, sum2 = 0xffff;
		int n, rem;

		for (int i = 0; i < data.length; i++) {
			// We can defer the modulo operation:
			// s1 maximally grows from 65521 to 65521 + 255 * 3800
			// s2 maximally grows by 3800 * median(s1) = 2090079800 < 2^31
			rem = data.length - i;
			n = (rem < 3800) ? rem : 3800;
			while (--n >= 0) {
				sum1 += data[i++];
				sum2 += sum1;
			}
			sum1 = (sum1 & 0xffff) + (sum1 >> 16);
			sum2 = (sum2 & 0xffff) + (sum2 >> 16);
		}

		sum1 = (sum1 & 0xffff) + (sum1 >> 16);
		sum2 = (sum2 & 0xffff) + (sum2 >> 16);

		return (sum2 << 16 | sum1) & 0xffffffffL;
	}

	public long digest(final int[] data) {
		int sum1 = 0xffff, sum2 = 0xffff;
		int n, rem;

		for (int i = 0; i < data.length; i++) {
			// We can defer the modulo operation:
			// s1 maximally grows from 65521 to 65521 + 255 * 3800
			// s2 maximally grows by 3800 * median(s1) = 2090079800 < 2^31
			rem = data.length - i;
			n = (rem < 3800) ? rem : 3800;
			while (--n >= 0) {
				sum1 += data[i++];
				sum2 += sum1;
			}
			sum1 = (sum1 & 0xffff) + (sum1 >> 16);
			sum2 = (sum2 & 0xffff) + (sum2 >> 16);
		}

		sum1 = (sum1 & 0xffff) + (sum1 >> 16);
		sum2 = (sum2 & 0xffff) + (sum2 >> 16);

		return (sum2 << 16 | sum1) & 0xffffffffL;
	}

}
