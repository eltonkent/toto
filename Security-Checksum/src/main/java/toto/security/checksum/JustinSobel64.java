package toto.security.checksum;

public class JustinSobel64 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		long checksum = 1315423911;
		for (int i = 0; i < data.length; i++) {
			checksum ^= ((checksum << 5) + data[i] + (checksum >> 2));
		}
		return checksum;
	}

}
