package toto.security.checksum;

public class ELF32 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		long checksum = 0;
		long x = 0;

		for (int i = 0; i < data.length; i++) {
			checksum = (checksum << 4) + data[i];
			if ((x = checksum & 0xF0000000L) != 0) {
				checksum ^= (x >> 24);
			}
			checksum &= ~x;
		}

		return checksum;
	}

}
