package toto.security.checksum;

public class DonaldKnuth64 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		long checksum = data.length;

		for (int i = 0; i < data.length; i++) {
			checksum = ((checksum << 5) ^ (checksum >> 27)) ^ data[i];
		}

		return checksum;
	}

}
