package toto.security.checksum;

public class DanielBernstein64 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		long checksum = 5381;

		for (int i = 0; i < data.length; i++) {
			checksum = ((checksum << 5) + checksum) + data[i];
		}

		return checksum;
	}

}
