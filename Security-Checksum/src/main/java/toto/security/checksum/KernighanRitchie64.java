package toto.security.checksum;

public class KernighanRitchie64 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		final long seed = 131; // 31 131 1313 13131 131313 etc..
		long checksum = 0;

		for (int i = 0; i < data.length; i++) {
			checksum = (checksum * seed) + data[i];
		}

		return checksum;
	}

}
