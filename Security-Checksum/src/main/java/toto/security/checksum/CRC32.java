package toto.security.checksum;

/**
 * CRC32 implementation
 * 
 * @author Mobifluence Interactive
 * 
 */
public class CRC32 extends TChecksum {

	@Override
	public long digest(final byte[] data) {
		final java.util.zip.CRC32 crc = new java.util.zip.CRC32();
		crc.update(data);
		return crc.getValue();
	}

}
