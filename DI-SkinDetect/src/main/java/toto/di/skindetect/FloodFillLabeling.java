package toto.di.skindetect;

import toto.bitmap.ToToBitmap;

abstract class FloodFillLabeling extends RegionLabeling {

	FloodFillLabeling(final ToToBitmap imgProcessor) {
		super(imgProcessor);
	}

	void applyLabeling() {
		resetLabel();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (isForeground(x, y)) {
					// start a new region
					final int label = getNextLabel();
					floodFill(x, y, label);
				}
			}
		}
	}

	abstract void floodFill(int x, int y, int label);

}