/**
 * Skin detection utilities to detect regions of skin in a bitmap and/or nudity.
 * @see toto.di.skindetect.SkinRegion
 * @author ekent4
 *
 */
package toto.di.skindetect;