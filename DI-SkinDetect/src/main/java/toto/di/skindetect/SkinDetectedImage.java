package toto.di.skindetect;

import toto.bitmap.ToToBitmap;

class SkinDetectedImage {

	private ToToBitmap image;

	private int skinPixelCount;

	ToToBitmap getImage() {
		return image;
	}

	void setImage(final ToToBitmap image) {
		this.image = image;
	}

	int getSkinPixelCount() {
		return skinPixelCount;
	}

	void setSkinPixelCount(final int skinPixelCount) {
		this.skinPixelCount = skinPixelCount;
	}
}
