package toto.di.skindetect;

import java.util.Collections;
import java.util.List;

import toto.bitmap.ToToBitmap;

class ColoredImageNudityDetector implements ImageNudityDetector {

	private static final int PIXEL_COUNT_THRESHOLD = 10;

	private Detector skinDetector;

	private SkinRegionDetector skinRegionDetector;

	public boolean isImageNude(final ToToBitmap inputImage) {

		skinDetector = new BinaryImageSkinDetector();
		final SkinDetectedImage skinDetectedImage = skinDetector
				.detectSkin(inputImage);

		skinRegionDetector = new BinaryImageSkinRegionDetector();
		final List<SkinRegion> skinRegions = skinRegionDetector
				.detectSkinRegions(skinDetectedImage.getImage());

		filterOutSmallRegions(skinRegions);

		Collections.sort(skinRegions, new SkinRegionComparator());

		final int skinRegionsCount = skinRegions.size();
		final int totalPixels = inputImage.getWidth() * inputImage.getHeight();
		final int skinPixelCount = skinDetectedImage.getSkinPixelCount();

		if (skinRegionsCount < 3) {
			return false;
		}

		final double skinPixelPercentage = (skinPixelCount / (totalPixels * 1.0)) * 100;
		if (skinPixelPercentage < 15) {
			return false;
		}

		final SkinRegion largestSkinRegion = skinRegions.get(0);
		final SkinRegion secondLargestSkinRegion = skinRegions.get(1);
		final SkinRegion thirdLargestSkinRegion = skinRegions.get(2);

		final double skinPixelPercentLargest = (largestSkinRegion
				.getNumberOfPixels() / (skinPixelCount * 1.0)) * 100;
		final double skinPixelPercent2ndLargest = (secondLargestSkinRegion
				.getNumberOfPixels() / (skinPixelCount * 1.0)) * 100;
		final double skinPixelPercent3rdLargest = (thirdLargestSkinRegion
				.getNumberOfPixels() / (skinPixelCount * 1.0)) * 100;

		if (skinPixelPercentLargest < 35 && skinPixelPercent2ndLargest < 30
				&& skinPixelPercent3rdLargest < 30) {
			return false;
		}

		if (skinPixelPercentLargest < 45) {
			return false;
		}

		if (skinPixelCount < (0.3 * totalPixels)) {
			return false;
		}

		if (skinRegionsCount > 60) {
			return false;
		}

		return true;
	}

	private void filterOutSmallRegions(final List<SkinRegion> skinRegions) {
		SkinRegion[] regionsArray = new SkinRegion[skinRegions.size()];

		regionsArray = skinRegions.toArray(regionsArray);

		for (final SkinRegion skinRegion : regionsArray) {
			if (skinRegion.getNumberOfPixels() < PIXEL_COUNT_THRESHOLD) {
				skinRegions.remove(skinRegion);
			}
		}
	}

}