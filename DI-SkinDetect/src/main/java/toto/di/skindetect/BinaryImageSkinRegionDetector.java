package toto.di.skindetect;

import java.util.List;

import toto.bitmap.ToToBitmap;

class BinaryImageSkinRegionDetector implements SkinRegionDetector {

	private RegionLabeling labeling;

	public List<SkinRegion> detectSkinRegions(final ToToBitmap image) {

		labeling = new BreadthFirstLabeling(image);

		return labeling.getRegions();
	}
}