package toto.di.skindetect;

import java.util.List;

import toto.bitmap.ToToBitmap;

interface SkinRegionDetector {

	List<SkinRegion> detectSkinRegions(ToToBitmap image);
}