package toto.di.skindetect;

import toto.bitmap.ToToBitmap;

interface Detector {

	SkinDetectedImage detectSkin(ToToBitmap inputImage);
}
