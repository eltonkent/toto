package toto.di.skindetect;

interface SkinColorClassifier {

	boolean isSkinPixel(int pixel);
}