package toto.di.skindetect;

import java.util.LinkedList;

import toto.bitmap.ToToBitmap;
import android.graphics.Point;

class BreadthFirstLabeling extends FloodFillLabeling {

	private static final int LIMIT = Integer.MAX_VALUE;

	BreadthFirstLabeling(final ToToBitmap imgProcessor) {
		super(imgProcessor);
	}

	void floodFill(final int x, final int y, final int label) {
		int maxDepth = 0;
		final LinkedList<Point> queue = new LinkedList<Point>();
		int counter = 0;
		queue.addFirst(new Point(x, y));
		while (!queue.isEmpty() && counter < LIMIT) {

			final int k = queue.size(); // for logging only - remove for
										// efficiency!
			if (k > maxDepth)
				maxDepth = k;

			final Point point = queue.removeLast();

			final int u = point.x;
			final int v = point.y;

			if ((u >= 0) && (u < width) && (v >= 0) && (v < height)
					&& isForeground(u, v)) {
				counter++;
				setLabel(u, v, label);
				queue.addFirst(new Point(u + 1, v));
				queue.addFirst(new Point(u, v + 1));
				queue.addFirst(new Point(u, v - 1));
				queue.addFirst(new Point(u - 1, v));
			}
		}
	}

}
