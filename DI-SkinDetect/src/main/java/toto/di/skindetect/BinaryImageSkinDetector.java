package toto.di.skindetect;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import toto.bitmap.ToToBitmap;

class BinaryImageSkinDetector implements Detector {

	private static final int FOREGROUND_PIXEL = 1;

	private static final int BACKGROUND_PIXEL = 0;

	private final List<SkinColorClassifier> skinClassifiers = new ArrayList<SkinColorClassifier>();

	public BinaryImageSkinDetector() {
		init();
	}

	private void init() {
		skinClassifiers.add(new RGBClassifier());
		skinClassifiers.add(new NormalizedRGBClassifier());
		skinClassifiers.add(new HSVClassifier());
	}

	public SkinDetectedImage detectSkin(final ToToBitmap image) {
		// Conditions.checkArgument(image != null,"Input image cannot be null");
		final int skinPixelCount = countSkinPixels(image);
		final SkinDetectedImage skinDetectedImage = new SkinDetectedImage();
		skinDetectedImage.setImage(image);
		skinDetectedImage.setSkinPixelCount(skinPixelCount);

		return skinDetectedImage;
	}

	private int countSkinPixels(final ToToBitmap image) {

		int skinPixelCount = 0;

		for (int y = 0; y < image.getHeight(); y++) {
			for (int x = 0; x < image.getWidth(); x++) {
				final int pixel = image.getPixel(x, y);

				final boolean isSkinPixel = classifySkinPixel(pixel);
				if (isSkinPixel) {
					image.setPixel(x, y, FOREGROUND_PIXEL);
					skinPixelCount++;
				} else {
					image.setPixel(x, y, BACKGROUND_PIXEL);
				}
			}
		}
		return skinPixelCount;
	}

	private boolean classifySkinPixel(final int pixel) {
		boolean isSkinPixel = false;

		final Iterator<SkinColorClassifier> classifierIterator = skinClassifiers
				.iterator();

		while (classifierIterator.hasNext() && !isSkinPixel) {
			final SkinColorClassifier skinClassifier = classifierIterator
					.next();
			isSkinPixel = skinClassifier.isSkinPixel(pixel);
		}

		return isSkinPixel;
	}

}