package toto.di.skindetect;

import java.util.Comparator;

class SkinRegionComparator implements Comparator<SkinRegion> {

	public int compare(final SkinRegion skinRegion1,
			final SkinRegion skinRegion2) {

		if (skinRegion1 == null || skinRegion2 == null) {
			throw new IllegalArgumentException("SkinRegion cannot be null");
		}

		final int skinRegion1Pixels = skinRegion1.getNumberOfPixels();
		final int skinRegion2Pixels = skinRegion2.getNumberOfPixels();

		if (skinRegion1Pixels > skinRegion2Pixels)
			return -1;
		else if (skinRegion1Pixels < skinRegion2Pixels)
			return 1;
		else
			return 0;
	}
}