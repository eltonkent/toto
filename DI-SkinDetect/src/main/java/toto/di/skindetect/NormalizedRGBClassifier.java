package toto.di.skindetect;

import toto.graphics.color.RGBUtils;

class NormalizedRGBClassifier implements SkinColorClassifier {

	private static final int RED = 0;

	private static final int GREEN = 1;

	private static final int BLUE = 2;

	public boolean isSkinPixel(final int pixel) {

		final float[] normalizedRGBValues = new float[3];
		RGBUtils.getNormalizedRGB(pixel, normalizedRGBValues);

		final float normalRed = normalizedRGBValues[RED];
		final float normalGreen = normalizedRGBValues[GREEN];
		final float normalBlue = normalizedRGBValues[BLUE];

		return (normalRed / normalGreen > 1.185)
				&& ((normalRed * normalBlue)
						/ (Math.pow(normalRed + normalGreen + normalBlue, 2)) > 0.107)
				&& ((normalRed * normalGreen)
						/ (Math.pow(normalRed + normalGreen + normalBlue, 2)) > 0.112);
	}
}