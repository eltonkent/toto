package toto.di.skindetect;

import toto.bitmap.ToToBitmap;

interface ImageNudityDetector {

	boolean isImageNude(ToToBitmap inputImage);
}
