package toto.di.skindetect;

import java.util.List;

import toto.bitmap.ToToBitmap;

/**
 * Skin detection utility.
 * <p>
 * 
 * </p>
 * 
 * @author ekent4
 * 
 */
public final class SkinDetector {

	/**
	 * Check if the given bitmap has excessive skin
	 * 
	 * @param bitmap
	 * @return
	 */
	public static boolean hasExesssiveSkin(final ToToBitmap bitmap) {
		final ImageNudityDetector nudityDetector = new ColoredImageNudityDetector();
		return nudityDetector.isImageNude(bitmap);
	}

	/**
	 * Get the list of screen regions
	 * 
	 * @param bitmap
	 * @see SkinRegion
	 * @return
	 */
	public static List<SkinRegion> getSkinRegions(final ToToBitmap bitmap) {
		final BinaryImageSkinRegionDetector skinRegionDetector = new BinaryImageSkinRegionDetector();
		return skinRegionDetector.detectSkinRegions(bitmap);

	}

	/**
	 * Get the number of skin pixels
	 * 
	 * @param bitmap
	 * @return
	 */
	public static int getSkinPixelCount(final ToToBitmap bitmap) {
		final BinaryImageSkinDetector skinDetector = new BinaryImageSkinDetector();
		final SkinDetectedImage skinDetectedImage = skinDetector
				.detectSkin(bitmap);
		return skinDetectedImage.getSkinPixelCount();
	}
}
