package toto.di.skindetect;

import static toto.graphics.color.RGBUtils.getRGB;

class RGBClassifier implements SkinColorClassifier {

	private static final int RED = 0;

	private static final int GREEN = 1;

	private static final int BLUE = 2;

	private int cachePixel;
	private boolean cacheResponse;
	final int[] rgbValues = new int[3];

	public boolean isSkinPixel(final int pixel) {
		if (pixel != cachePixel) {
			getRGB(pixel, rgbValues);
			cacheResponse = ((rgbValues[RED] > 95)
					&& (rgbValues[GREEN] > 40)
					&& (rgbValues[BLUE] > 20)
					&& ((Math.max(rgbValues[RED],
							Math.max(rgbValues[GREEN], rgbValues[BLUE])) - Math
							.min(rgbValues[RED],
									Math.min(rgbValues[GREEN], rgbValues[BLUE]))) > 15)
					&& (Math.abs(rgbValues[RED] - rgbValues[GREEN]) > 15)
					&& (rgbValues[RED] > rgbValues[GREEN]) && (rgbValues[RED] > rgbValues[BLUE]));
			cachePixel = pixel;
		}

		return cacheResponse;
	}
}
