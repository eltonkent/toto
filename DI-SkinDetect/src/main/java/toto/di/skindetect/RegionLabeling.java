package toto.di.skindetect;

import java.util.ArrayList;
import java.util.List;

import toto.bitmap.ToToBitmap;
import toto.graphics.color.convert.HSBConverter;

/**
 * This class does the complete region labeling for a given image. It is
 * abstract, because the implementation of some parts depend upon the particular
 * region labeling algorithm being used.
 */
abstract class RegionLabeling {

	static boolean doDisplay = false;

	static final int BACKGROUND = 0;
	static final int FOREGROUND = 1;
	static final int START_LABEL = 2;
	int[] labels = null;
	int width;
	int height;
	boolean verbosity;
	private int currentLabel;
	private int maxLabel;

	List<SkinRegion> regions = null;

	RegionLabeling(final ToToBitmap ip) {
		width = ip.getWidth();
		height = ip.getHeight();

		makeLabelArray(ip);

		applyLabeling();
		collectRegions();
	}

	private void makeLabelArray(final ToToBitmap ip) {
		// setKey all pixels to FOREGROUND or BACKGROUND (thresholding)
		labels = new int[width * height];
		for (int v = 0; v < height; v++) {
			for (int u = 0; u < width; u++) {
				final int p = ip.getPixel(u, v);
				if (p > 0) {
					labels[v * width + u] = FOREGROUND;
				} else {
					labels[v * width + u] = BACKGROUND;
				}
			}
		}
	}

	/**
	 * Must be implemented by the sub-classes:
	 */
	abstract void applyLabeling();

	/**
	 * Creates a container of BinaryRegion objects, then collects the regions'
	 * coordinates by scanning the "labels" array.
	 */
	private void collectRegions() {
		// create an array of BinaryRegion objects
		final SkinRegion[] regionArray = new SkinRegion[maxLabel + 1];
		for (int i = START_LABEL; i <= maxLabel; i++) {
			regionArray[i] = new SkinRegion(i);
		}
		// scan the labels array and collect the coordinates for each region
		for (int v = 0; v < height; v++) {
			for (int u = 0; u < width; u++) {
				final int lb = labels[v * width + u];
				if (lb >= START_LABEL && lb <= maxLabel
						&& regionArray[lb] != null) {
					regionArray[lb].addPixel(u, v);
				}
			}
		}

		// collect all nonempty regions and create the final list of regions
		final List<SkinRegion> regionList = new ArrayList<SkinRegion>();
		for (final SkinRegion r : regionArray) {
			if (r != null && r.getNumberOfPixels() > 0) {
				r.updateRegionStatistics(); // calculate the statistics for this
											// region
				regionList.add(r);
			}
		}
		regions = regionList;
	}

	boolean isForeground(final int u, final int v) {
		return (labels[v * width + u] == FOREGROUND);
	}

	void resetLabel() {
		currentLabel = -1;
		maxLabel = -1;
	}

	int getNextLabel() {
		if (currentLabel < 1) {
			currentLabel = START_LABEL;
		} else {
			currentLabel = currentLabel + 1;
		}
		maxLabel = currentLabel;
		return currentLabel;
	}

	void setLabel(final int u, final int v, final int label) {
		if (u >= 0 && u < width && v >= 0 && v < height) {
			labels[v * width + u] = label;
		}
	}

	static void setDisplay(final boolean display) {
		doDisplay = display;
	}

	void setVerbosity(final boolean verbosity) {
		this.verbosity = verbosity;
	}

	List<SkinRegion> getRegions() {
		return regions;
	}

	int getMaxLabel() {
		return maxLabel;
	}

	int getLabel(final int u, final int v) {
		if (u >= 0 && u < width && v >= 0 && v < height) {
			return labels[v * width + u];
		} else {
			return BACKGROUND;
		}
	}

	boolean isLabel(final int u, final int v) {
		return (labels[v * width + u] >= START_LABEL);
	}

	// public ImageProcessor makeLabelImage(final boolean color) {
	// if (color) {
	// return makeRandomColorImage();
	// } else {
	// return makeGrayImage();
	// }
	// }
	//
	// public FloatProcessor makeGrayImage() {
	// final FloatProcessor ip = new FloatProcessor(width, height, labels);
	// ip.resetMinAndMax();
	// return ip;
	// }
	//
	// public ColorProcessor makeRandomColorImage() {
	// final int[] colorLUT = new int[maxLabel + 1];
	//
	// for (int i = START_LABEL; i <= maxLabel; i++) {
	// colorLUT[i] = makeRandomColor();
	// }
	//
	// final ColorProcessor cp = new ColorProcessor(width, height);
	// final int[] colPix = (int[]) cp.getPixels();
	//
	// for (int i = 0; i < labels.length; i++) {
	// if (labels[i] >= 0 && labels[i] < colorLUT.length) {
	// colPix[i] = colorLUT[labels[i]];
	// } else {
	// throw new Error("illegal label = " + labels[i]);
	// }
	// }
	// return cp;
	// }
	//
	// public void printSummary() {
	// if (regions != null && regions.size() > 0) {
	// IJ.log("Summary: number of regions = " + regions.size());
	// for (final SkinRegion r : regions) {
	// IJ.log(r.toString());
	// }
	// } else {
	// IJ.log("Summary: no regions found.");
	// }
	// }
	//
	// protected void showLabelArray() {
	// final ImageProcessor ip = new FloatProcessor(width, height, labels);
	// ip.resetMinAndMax();
	// final ImagePlus im = new ImagePlus("Label Array", ip);
	// im.show();
	// }

	int makeRandomColor() {
		final double saturation = 0.2;
		final double brightness = 0.2;
		final float h = (float) Math.random();
		final float s = (float) (saturation * Math.random() + 1 - saturation);
		final float b = (float) (brightness * Math.random() + 1 - brightness);
		return HSBConverter.toRGB(h, s, b);
	}

	void snooze(final int time) {
		if (time > 0) {
			try {
				Thread.sleep(time);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}