package toto.di.skindetect;

import toto.graphics.color.RGBUtils;
import toto.graphics.color.convert.RGBConverter;

class HSVClassifier implements SkinColorClassifier {
	private static final int HUE = 0;

	private static final int SATURATION = 1;

	private int cachePixel;
	private boolean cacheResponse;
	final int[] rgb = new int[3];

	public boolean isSkinPixel(final int pixel) {
		if (pixel != cachePixel) {
			RGBUtils.getRGB(pixel, rgb);
			final float[] hsvValues = new float[3];
			RGBConverter.toHSB(rgb[0], rgb[1], rgb[2], hsvValues);
			cacheResponse = (hsvValues[HUE] >= 0 && hsvValues[HUE] <= 50
					&& hsvValues[SATURATION] >= 0.23 && hsvValues[SATURATION] <= 0.68);
			cachePixel = pixel;
		}
		return cacheResponse;
	}
}
