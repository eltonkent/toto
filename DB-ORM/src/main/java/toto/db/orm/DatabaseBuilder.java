/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.db.orm;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import toto.db.ToToDBException;
import toto.text.WordUtils;

/**
 * Defines DB schema definition statements from provided Java classes. <br/>
 * Use this class to specify structure of your DB. Call method addClass() for
 * each table and provide corresponding Java class. <br/>
 * Normally this class instantiated only once at the very beginning of the
 * application life cycle. Once instantiated it is used by underlying
 * SQLDatabaseHelper and provides SQL statements for create or upgrade of DB
 * schema. <br/>
 * <br/>
 * <b>Building a database from java classes</b>
 * 
 * <pre>
 * <code>
 *  DatabaseBuilder builder = new DatabaseBuilder("Test.db");
 *  builder.addClass(Table1.class);
 *  builder.addClass(Table2.class);
 *  Database.setBuilder(builder);
 *  try{
 *  DatabaseClient client= DatabaseClient.open(this, "dbName.db",
 *                                         "dbVersion");
 *  // purge Table1 table
 *  _db.delete(Table1.class, null, null);
 * 
 *  }catch(DBException e){
 *  }
 *  </code>
 * 
 * </pre>
 */
public class DatabaseBuilder {

	String _dbName;
	@SuppressWarnings("unchecked")
	Map<String, Class> classes = new HashMap<String, Class>();

	/**
	 * Create a new DatabaseBuilder for a database.
	 */
	public DatabaseBuilder(final String dbName) {
		this._dbName = dbName;
	}

	/**
	 * Add or update a table for an AREntity that is stored in the current
	 * database.
	 * 
	 * @param <T>
	 *            Any ActiveRecordBase type.
	 * @param c
	 *            The class to reference when updating or adding a table.
	 */
	public <T extends DatabaseClient> void addClass(final Class<T> c) {
		classes.put(c.getSimpleName(), c);
	}

	@SuppressWarnings("unchecked")
	private Class getClassBySqlName(final String table) {
		final String jName = WordUtils.toJavaClassName(table);
		return classes.get(jName);
	}

	public String getDatabaseName() {
		return _dbName;
	}

	/**
	 * Returns SQL create statement for specified table
	 * 
	 * @param table
	 *            name in SQL notation
	 * @throws ToToDBException
	 */
	@SuppressWarnings("unchecked")
	public <T extends DatabaseClient> String getSQLCreate(final String table)
			throws ToToDBException {
		StringBuilder sb = null;
		final Class<T> c = getClassBySqlName(table);
		T e = null;
		try {
			e = c.newInstance();
		} catch (final IllegalAccessException e1) {
			throw new ToToDBException(e1.getLocalizedMessage());
		} catch (final InstantiationException e1) {
			throw new ToToDBException(e1.getLocalizedMessage());
		}
		if (null != c) {
			sb = new StringBuilder("CREATE TABLE ").append(table).append(
					" (_id integer primary key");
			for (final Field column : e.getColumnFieldsWithoutID()) {
				final String jname = column.getName();
				final String qname = WordUtils.toSQLName(jname);
				final Class<?> jtype = column.getType();
				final String qtype = Database.getSQLiteTypeString(jtype);
				sb.append(", ").append(qname).append(" ").append(qtype);
			}
			sb.append(")");

		}
		return sb.toString();
	}

	/**
	 * Returns SQL drop table statement for specified table
	 * 
	 * @param table
	 *            name in SQL notation
	 */
	public String getSQLDrop(final String table) {
		return "DROP TABLE IF EXISTS " + table;
	}

	/**
	 * Returns list of DB tables according to classes added to a schema map
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String[] getTables() {
		final String[] ret = new String[classes.size()];
		Class[] arr = new Class[classes.size()];
		arr = classes.values().toArray(arr);
		for (int i = 0; i < arr.length; i++) {
			final Class c = arr[i];
			ret[i] = WordUtils.toSQLName(c.getSimpleName());
		}
		return ret;
	}
}
