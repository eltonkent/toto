/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.db.orm;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import toto.db.ToToDBException;
import toto.text.WordUtils;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

/**
 * Base class for tables entities
 * 
 * @see Database
 * 
 */
public class DatabaseClient {

	static EntitiesMap s_EntitiesMap = new EntitiesMap();

	/**
	 * Creates new ActiveRecord instance. Returned instances is not initially
	 * opened. Calling application must explicitly open it by calling open()
	 * method
	 * 
	 * @param db
	 * @return
	 */
	public static DatabaseClient createInstance(final Database db) {
		return new DatabaseClient(db);
	}

	/**
	 * Creates and opens new ActiveRecord object instance and underlying
	 * database. Returned ActiveRecord object is fully ready for use.
	 * 
	 * @param ctx
	 * @param dbName
	 * @return
	 * @throws ToToDBException
	 */
	static public DatabaseClient open(final Context ctx, final String dbName,
			final int dbVersion) throws ToToDBException {
		final Database db = Database.createInstance(ctx, dbName, dbVersion);
		db.open();
		return DatabaseClient.createInstance(db);
	}

	protected long _id = 0;

	Database m_Database;

	boolean m_NeedsInsert = true;

	protected DatabaseClient() {
	}

	/**
	 * 
	 * @param db
	 */
	protected DatabaseClient(final Database db) {
		m_Database = db;
	}

	/**
	 * Closes ActiveRecord object and associated underlying database
	 */
	public void close() {
		m_Database.close();
	}

	/**
	 * Copies values of fields from src to current object. Scans src object for
	 * the fields with the same names as in current object and copies it's
	 * valies. All fields are copied except special fields: 'id', 'created',
	 * 'modified', and also fields prefixed as 'm_*' and 's_' If src has fields
	 * not defiend in current object such fields are ignored
	 * 
	 * @param src
	 */
	public void copyFrom(final Object src) {
		for (final Field dstField : this.getColumnFieldsWithoutID()) {
			try {
				final Field srcField = src.getClass().getField(
						dstField.getName());
				dstField.set(this, srcField.get(src));

			} catch (final SecurityException e) {
			} catch (final NoSuchFieldException e) {
			} catch (final IllegalArgumentException e) {
			} catch (final IllegalAccessException e) {
			}
		}

	}

	/**
	 * Remove this entity from the database.
	 * 
	 * @return Whether or the entity was successfully deleted.
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public boolean delete() throws ToToDBException {
		if (m_Database == null)
			throw new ToToDBException("Set database first");
		final boolean toRet = m_Database.delete(getTableName(), "_id = ?",
				new String[] { String.valueOf(_id) }) != 0;
		_id = 0;
		m_NeedsInsert = true;
		return toRet;
	}

	/**
	 * Delete selected entities from the database.
	 * 
	 * @param <T>
	 *            Any AREntity class.
	 * @param type
	 *            The class of the entities to delete.
	 * @param whereClause
	 *            The condition to match (Don't include "where").
	 * @param whereArgs
	 *            The arguments to replace "?" with.
	 * @return The number of rows affected.
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public <T extends DatabaseClient> int delete(final Class<T> type,
			final String whereClause, final String[] whereArgs)
			throws ToToDBException {
		if (m_Database == null)
			throw new ToToDBException("Set database first");
		T entity;
		try {
			entity = type.newInstance();
		} catch (final IllegalAccessException e) {
			throw new ToToDBException(e.getLocalizedMessage());
		} catch (final InstantiationException e) {
			throw new ToToDBException(e.getLocalizedMessage());
		}
		return m_Database.delete(entity.getTableName(), whereClause, whereArgs);
	}

	/**
	 * Delete all instances of an entity from the database where a column has a
	 * specified value.
	 * 
	 * @param <T>
	 *            Any AREntity class.
	 * @param type
	 *            The class of the entities to delete.
	 * @param column
	 *            The column to match.
	 * @param value
	 *            The value required for deletion.
	 * @return The number of rows affected.
	 * @throws ToToDBException
	 */
	public <T extends DatabaseClient> int deleteByColumn(final Class<T> type,
			final String column, final String value) throws ToToDBException {
		return delete(type, String.format("%s = ?", column),
				new String[] { value });
	}

	/**
	 * Return all instances of an entity that match the given criteria.
	 * 
	 * @param <T>
	 *            Any ActiveRecordBase class.
	 * @param type
	 *            The class of the entities to return.
	 * @param distinct
	 * @param whereClause
	 *            The condition to match (Don't include "where").
	 * @param whereArgs
	 *            The arguments to replace "?" with.
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @param limit
	 * @return A generic list of all matching entities.
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public <T extends DatabaseClient> List<T> find(final Class<T> type,
			final boolean distinct, final String whereClause,
			final String[] whereArgs, final String groupBy,
			final String having, final String orderBy, final String limit)
			throws ToToDBException {
		if (m_Database == null)
			throw new ToToDBException("Set database first");
		T entity;
		try {
			entity = type.newInstance();
		} catch (final IllegalAccessException e) {
			throw new ToToDBException(e.getLocalizedMessage());
		} catch (final InstantiationException e) {
			throw new ToToDBException(e.getLocalizedMessage());
		}
		final Cursor c = m_Database.query(distinct, entity.getTableName(),
				null, whereClause, whereArgs, groupBy, having, orderBy, limit);
		return populateList(c, s_EntitiesMap, type);
	}

	/**
	 * Return all instances of an entity that match the given criteria. Use
	 * whereClause to specify condition, using reqular SQL syntax for WHERE
	 * clause.
	 * <p>
	 * For example selecting all JOHNs born in 2001 from USERS table may look
	 * like:
	 * 
	 * <pre>
	 * users.find(Users.class, &quot;NAME='?' and YEAR=?&quot;, new String[] { &quot;John&quot;, &quot;2001&quot; });
	 * </pre>
	 * 
	 * @param <T>
	 *            Any ActiveRecordBase class.
	 * @param type
	 *            The class of the entities to return.
	 * @param whereClause
	 *            The condition to match (Don't include "where").
	 * @param whereArgs
	 *            The arguments to replace "?" with.
	 * @return A generic list of all matching entities.
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public <T extends DatabaseClient> List<T> find(final Class<T> type,
			final String whereClause, final String[] whereArgs)
			throws ToToDBException {
		if (m_Database == null)
			throw new ToToDBException("Set database first");
		T entity = null;
		try {
			entity = type.newInstance();
		} catch (final IllegalAccessException e1) {
			throw new ToToDBException(e1.getLocalizedMessage());
		} catch (final InstantiationException e1) {
			throw new ToToDBException(e1.getLocalizedMessage());
		}
		final Cursor c = m_Database.query(entity.getTableName(), null,
				whereClause, whereArgs);
		return populateList(c, s_EntitiesMap, type);
	}

	private <T extends DatabaseClient> List<T> populateList(final Cursor c,
			final EntitiesMap s_EntitiesMap, final Class<T> type)
			throws ToToDBException {
		final List<T> toRet = new ArrayList<T>();
		T entity = null;
		try {
			while (c.moveToNext()) {
				entity = s_EntitiesMap.get(type,
						c.getLong(c.getColumnIndex("_id")));
				if (entity == null) {
					entity = type.newInstance();
					entity.m_NeedsInsert = false;
					entity.inflate(c);
					entity.m_Database = m_Database;

				}
				toRet.add(entity);
			}
		} catch (final IllegalAccessException e) {
			throw new ToToDBException(e.getLocalizedMessage());
		} catch (final InstantiationException e) {
			throw new ToToDBException(e.getLocalizedMessage());
		} finally {
			c.close();
		}
		return toRet;
	}

	/**
	 * Return all instances of an entity from the database.
	 * 
	 * @param <T>
	 *            Any ActiveRecordBase class.
	 * @param type
	 *            The class of the entities to return.
	 * @return A generic list of all matching entities.
	 * @throws ToToDBException
	 */
	public <T extends DatabaseClient> List<T> findAll(final Class<T> type)
			throws ToToDBException {
		return find(type, null, null);
	}

	/**
	 * Return all instances of an entity from the database where a column has a
	 * specified value.
	 * 
	 * @param <T>
	 *            Any ActiveRecordBase class.
	 * @param type
	 *            The class of the entities to return.
	 * @param column
	 *            The tables's column to match. Note - it must be name from DB
	 *            schema, not Java field name
	 * @param value
	 *            The desired value.
	 * @return A generic list of all matching entities.
	 * @throws ToToDBException
	 */
	public <T extends DatabaseClient> List<T> findByColumn(final Class<T> type,
			final String column, final String value) throws ToToDBException {
		return find(type, String.format("%s = ?", column),
				new String[] { value });
	}

	/**
	 * Return the instance of an entity with a matching id.
	 * 
	 * @param <T>
	 *            Any ActiveRecordBase class.
	 * @param type
	 *            The class of the entity to return.
	 * @param id
	 *            The desired ID.
	 * @return The matching entity if reocrd found in DB, null otherwise
	 * @throws ToToDBException
	 */
	public <T extends DatabaseClient> T findByID(final Class<T> type,
			final long id) throws ToToDBException {
		if (m_Database == null)
			throw new ToToDBException("Set database first");
		T entity = s_EntitiesMap.get(type, id);
		if (entity != null)
			return entity;

		try {
			entity = type.newInstance();
		} catch (final IllegalAccessException e) {
			throw new ToToDBException(e.getLocalizedMessage());
		} catch (final InstantiationException e) {
			throw new ToToDBException(e.getLocalizedMessage());
		}

		final Cursor c = m_Database.query(entity.getTableName(), null,
				"_id = ?", new String[] { String.valueOf(id) });
		try {
			if (!c.moveToNext()) {
				return null;
			} else {
				entity.inflate(c);
				entity.m_NeedsInsert = false;
				entity.m_Database = m_Database;
			}
		} finally {
			c.close();
		}
		return entity;
	}

	/**
	 * Get this class's fields.
	 * 
	 * @return An array of fields for this class.
	 */
	protected List<Field> getColumnFields() {
		Field[] fields = getClass().getDeclaredFields();
		final List<Field> columns = new ArrayList<Field>();
		for (final Field field : fields) {
			if (!field.getName().startsWith("m_")
					&& !field.getName().startsWith("s_")) {
				columns.add(field);
			}
		}
		if (!getClass().equals(DatabaseClient.class)) {
			fields = DatabaseClient.class.getDeclaredFields();
			for (final Field field : fields) {
				if (!field.getName().startsWith("m_")
						&& !field.getName().startsWith("s_")) {
					columns.add(field);
				}
			}
		}
		return columns;
	}

	/**
	 * Get this class's fields without id.
	 * 
	 * @return An array of fields for this class.
	 */
	protected List<Field> getColumnFieldsWithoutID() {
		final Field[] fields = getClass().getDeclaredFields();
		final List<Field> columns = new ArrayList<Field>();
		for (final Field field : fields) {
			if (!field.getName().startsWith("m_")
					&& !field.getName().startsWith("s_"))
				columns.add(field);
		}
		return columns;
	}

	/**
	 * Get this class's columns.
	 * 
	 * @return An array of the columns in this class's table.
	 */
	protected String[] getColumns() {
		final List<String> columns = new ArrayList<String>();
		for (final Field field : getColumnFields()) {
			columns.add(field.getName());
		}
		return columns.toArray(new String[0]);
	}

	/**
	 * Get this class's columns without the id column.
	 * 
	 * @return An array of the columns in this class's table.
	 */
	protected String[] getColumnsWithoutID() {
		final List<String> columns = new ArrayList<String>();
		for (final Field field : getColumnFieldsWithoutID()) {
			columns.add(field.getName());
		}
		return columns.toArray(new String[0]);
	}

	/**
	 * Returns underlying database object for direct manipulations
	 * 
	 * @return
	 */
	public Database getDatabase() {
		return m_Database;
	}

	/**
	 * This entities row id.
	 * 
	 * @return The SQLite row id.
	 */
	public long getID() {
		return _id;
	}

	/**
	 * Get the table name for this class.
	 * 
	 * @return The table name for this class.
	 */
	protected String getTableName() {
		return WordUtils.toSQLName(getClass().getSimpleName());
	}

	/**
	 * Inflate this entity using the current row from the given cursor.
	 * 
	 * @param cursor
	 *            The cursor to get object data from.
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unchecked")
	void inflate(final Cursor cursor) throws ToToDBException {
		final HashMap<Field, Long> entities = new HashMap<Field, Long>();
		for (final Field field : getColumnFields()) {
			try {
				final String typeString = field.getType().getName();
				final String colName = WordUtils.toSQLName(field.getName());
				if (typeString.equals("long")) {
					field.setLong(this,
							cursor.getLong(cursor.getColumnIndex(colName)));
				} else if (typeString.equals("java.lang.String")) {
					final String val = cursor.getString(cursor
							.getColumnIndex(colName));
					field.set(this, val.equals("null") ? null : val);
				} else if (typeString.equals("double")) {
					field.setDouble(this,
							cursor.getDouble(cursor.getColumnIndex(colName)));
				} else if (typeString.equals("boolean")) {
					field.setBoolean(this,
							cursor.getString(cursor.getColumnIndex(colName))
									.equals("true"));
				} else if (typeString.equals("[B")) {
					field.set(this,
							cursor.getBlob(cursor.getColumnIndex(colName)));
				} else if (typeString.equals("int")) {
					field.setInt(this,
							cursor.getInt(cursor.getColumnIndex(colName)));
				} else if (typeString.equals("float")) {
					field.setFloat(this,
							cursor.getFloat(cursor.getColumnIndex(colName)));
				} else if (typeString.equals("short")) {
					field.setShort(this,
							cursor.getShort(cursor.getColumnIndex(colName)));
				} else if (typeString.equals("java.sql.Timestamp")) {
					final long l = cursor.getLong(cursor
							.getColumnIndex(colName));
					field.set(this, new Timestamp(l));
				} else if (field.getType().getSuperclass() == DatabaseClient.class) {
					final long id = cursor.getLong(cursor
							.getColumnIndex(colName));
					if (id > 0)
						entities.put(field, id);
					else
						field.set(this, null);
				} else
					throw new ToToDBException(
							"Class cannot be read from Sqlite3 database.");
			} catch (final IllegalArgumentException e) {
				throw new ToToDBException(e.getLocalizedMessage());
			} catch (final IllegalAccessException e) {
				throw new ToToDBException(e.getLocalizedMessage());
			}

		}

		s_EntitiesMap.set(this);
		for (final Field f : entities.keySet()) {
			try {
				f.set(this, this.findByID(
						(Class<? extends DatabaseClient>) f.getType(),
						entities.get(f)));
			} catch (final SQLiteException e) {
				throw new ToToDBException(e.getLocalizedMessage());
			} catch (final IllegalArgumentException e) {
				throw new ToToDBException(e.getLocalizedMessage());
			} catch (final IllegalAccessException e) {
				throw new ToToDBException(e.getLocalizedMessage());
			}
		}
	}

	/**
	 * Insert this entity into the database.
	 * 
	 * @return the row ID of the newly inserted row, or -1 if an error occurred
	 * @throws ToToDBException
	 */
	public long insert() throws ToToDBException {
		final List<Field> columns = _id > 0 ? getColumnFields()
				: getColumnFieldsWithoutID();
		final ContentValues values = new ContentValues(columns.size());
		for (final Field column : columns) {
			try {
				if (column.getType().getSuperclass() == DatabaseClient.class)
					values.put(
							WordUtils.toSQLName(column.getName()),
							column.get(this) != null ? String
									.valueOf(((DatabaseClient) column.get(this))._id)
									: "0");
				else
					values.put(WordUtils.toSQLName(column.getName()),
							String.valueOf(column.get(this)));
			} catch (final IllegalAccessException e) {
				throw new ToToDBException(e.getLocalizedMessage());
			}
		}
		_id = m_Database.insert(getTableName(), values);
		if (-1 != _id)
			m_NeedsInsert = false;

		return _id;
	}

	/**
	 * Returns true is underlying database object is open
	 * 
	 * @return
	 */
	public boolean isOpen() {
		return m_Database.isOpen();
	}

	/**
	 * Creates new entity instance connected with opened database
	 * 
	 * @param <T>
	 * @param type
	 *            The type of the required entity
	 * @return New entity instance
	 */
	public <T extends DatabaseClient> T newEntity(final Class<T> type)
			throws ToToDBException {
		T entity = null;
		try {
			entity = type.newInstance();
			entity.setDatabase(m_Database);
		} catch (final IllegalAccessException e) {
			throw new ToToDBException("Can't instantiate " + type.getClass());
		} catch (final InstantiationException e) {
			throw new ToToDBException("Can't instantiate " + type.getClass());
		}
		return entity;
	}

	/**
	 * Opens ActiveRecord object and associated underlying database
	 * 
	 * @throws ToToDBException
	 */
	public void open() throws ToToDBException {
		m_Database.open();
	}

	/**
	 * Saves this entity to the database, inserts or updates as needed.
	 * 
	 * @return number of rows affected on success, -1 on failure
	 * @throws ToToDBException
	 */
	public long save() throws ToToDBException {
		long r = -1;

		if (m_Database == null)
			throw new ToToDBException("Set database first");

		if (null == findByID(this.getClass(), _id))
			r = insert();
		else
			r = update();
		s_EntitiesMap.set(this);

		return r;
	}

	/**
	 * Call this once at application launch, sets the database to use for
	 * AREntities.
	 * 
	 * @param database
	 *            The database to use.
	 */
	public void setDatabase(final Database database) {
		m_Database = database;
	}

	/**
	 * Update this entity in the database.
	 * 
	 * @return The number of rows affected
	 * @throws NoSuchFieldException
	 */
	public int update() throws ToToDBException {
		final List<Field> columns = getColumnFieldsWithoutID();
		final ContentValues values = new ContentValues(columns.size());
		for (final Field column : columns) {
			try {
				if (column.getType().getSuperclass() == DatabaseClient.class)
					values.put(
							WordUtils.toSQLName(column.getName()),
							column.get(this) != null ? String
									.valueOf(((DatabaseClient) column.get(this))._id)
									: "0");
				else
					values.put(WordUtils.toSQLName(column.getName()),
							String.valueOf(column.get(this)));
			} catch (final IllegalArgumentException e) {
				throw new ToToDBException("No column " + column.getName());
			} catch (final IllegalAccessException e) {
				throw new ToToDBException("No column " + column.getName());
			}
		}
		final int r = m_Database.update(getTableName(), values, "_id = ?",
				new String[] { String.valueOf(_id) });
		return r;
	}
}
