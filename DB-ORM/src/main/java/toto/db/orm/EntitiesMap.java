/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.db.orm;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * 
 */
class EntitiesMap {
	WeakHashMap<DatabaseClient, String> _map = new WeakHashMap<DatabaseClient, String>();
	private final Map<String, WeakReference<DatabaseClient>> map = new HashMap<String, WeakReference<DatabaseClient>>();

	@SuppressWarnings("unchecked")
	<T extends DatabaseClient> T get(final Class<T> c, final long id) {
		final String key = makeKey(c, id);
		final WeakReference<DatabaseClient> i = map.get(key);
		if (i == null)
			return null;
		return (T) i.get();
	}

	@SuppressWarnings("unchecked")
	private String makeKey(final Class entityType, final long id) {
		final StringBuilder sb = new StringBuilder();
		sb.append(entityType.getName()).append(id);
		return sb.toString();
	}

	void set(final DatabaseClient e) {
		final String key = makeKey(e.getClass(), e.getID());
		map.put(key, new WeakReference<DatabaseClient>(e));
	}
}
