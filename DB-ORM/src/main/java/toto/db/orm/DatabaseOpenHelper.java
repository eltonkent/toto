/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.db.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import toto.db.ToToDBException;

/**
 * Internal framework class. Utilize DatabaseBuilder to produce DDL statements
 * directly out of Java classes.
 * 
 */
class DatabaseOpenHelper extends SQLiteOpenHelper {

	DatabaseBuilder _builder;
	int _version;

	/**
	 * Constructor
	 * 
	 * @param ctx
	 * @param dbPath
	 * @param dbVersion
	 * @param builder
	 */
	public DatabaseOpenHelper(final Context ctx, final String dbPath,
			final int dbVersion, final DatabaseBuilder builder) {
		super(ctx, dbPath, null, dbVersion);
		_builder = builder;
		_version = dbVersion;
	}

	@Override
	public void onCreate(final SQLiteDatabase db) {
		for (final String table : _builder.getTables()) {
			String sqlStr = null;
			try {
				sqlStr = _builder.getSQLCreate(table);
			} catch (final ToToDBException e) {
				Log.e(this.getClass().getName(), e.getMessage(), e);
			}
			if (sqlStr != null)
				db.execSQL(sqlStr);
		}
		db.setVersion(_version);
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, final int oldVersion,
			final int newVersion) {
		for (final String table : _builder.getTables()) {
			final String sqlStr = _builder.getSQLDrop(table);
			db.execSQL(sqlStr);
		}
		onCreate(db);
	}
}
