/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.drawable;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable.Orientation;

/**
 * Draw a simple linear gradient.
 */
public class LinearGradientDrawable extends Drawable {

	private final Paint mFillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Rect mPadding;
	private float mCornerRadius = 0;
	private boolean mRectIsDirty = true;
	private int mAlpha = 0xFF; // modified by the caller
	private boolean mDither = true;
	private ColorFilter mColorFilter;
	private Orientation mOrientation = Orientation.LEFT_RIGHT;
	private final int[] mColors;
	private final float[] mPositions;
	private final RectF mRect = new RectF();

	/**
	 * Instantiates a new linear gradient drawable.
	 * 
	 * @param orientation
	 *            the orientation
	 * @param colors
	 *            the colors
	 * @param positions
	 *            the positions
	 */
	public LinearGradientDrawable(final Orientation orientation,
			final int[] colors, final float[] positions) {
		mOrientation = orientation;
		mColors = colors;
		mPositions = positions;
	}

	@Override
	public void draw(final Canvas canvas) {

		if (!ensureValidRect())
			return;

		mFillPaint.setAlpha(mAlpha);
		mFillPaint.setDither(mDither);
		mFillPaint.setColorFilter(mColorFilter);

		if (mCornerRadius > 0) {
			float rad = mCornerRadius;
			final float r = Math.min(mRect.width(), mRect.height()) * 0.5f;
			if (rad > r) {
				rad = r;
			}
			canvas.drawRoundRect(mRect, rad, rad, mFillPaint);
		} else {
			canvas.drawRect(mRect, mFillPaint);
		}

	}

	/**
	 * Ensure valid rect.
	 * 
	 * @return true, if successful
	 */
	private boolean ensureValidRect() {
		if (mRectIsDirty) {
			mRectIsDirty = false;

			final Rect bounds = getBounds();
			final float inset = 0;

			mRect.set(bounds.left + inset, bounds.top + inset, bounds.right
					- inset, bounds.bottom - inset);

			final int[] colors = mColors;

			if (colors != null) {
				final RectF r = mRect;
				float x0, x1, y0, y1;

				final float level = 1.0f;
				switch (mOrientation) {
				case TOP_BOTTOM:
					x0 = r.left;
					y0 = r.top;
					x1 = x0;
					y1 = level * r.bottom;
					break;
				case TR_BL:
					x0 = r.right;
					y0 = r.top;
					x1 = level * r.left;
					y1 = level * r.bottom;
					break;
				case RIGHT_LEFT:
					x0 = r.right;
					y0 = r.top;
					x1 = level * r.left;
					y1 = y0;
					break;
				case BR_TL:
					x0 = r.right;
					y0 = r.bottom;
					x1 = level * r.left;
					y1 = level * r.top;
					break;
				case BOTTOM_TOP:
					x0 = r.left;
					y0 = r.bottom;
					x1 = x0;
					y1 = level * r.top;
					break;
				case BL_TR:
					x0 = r.left;
					y0 = r.bottom;
					x1 = level * r.right;
					y1 = level * r.top;
					break;
				case LEFT_RIGHT:
					x0 = r.left;
					y0 = r.top;
					x1 = level * r.right;
					y1 = y0;
					break;
				default:/* TL_BR */
					x0 = r.left;
					y0 = r.top;
					x1 = level * r.right;
					y1 = level * r.bottom;
					break;
				}

				mFillPaint.setShader(new LinearGradient(x0, y0, x1, y1, colors,
						mPositions, Shader.TileMode.CLAMP));
			}
		}
		return !mRect.isEmpty();
	}

	@Override
	protected void onBoundsChange(final Rect r) {
		super.onBoundsChange(r);
		mRectIsDirty = true;
	}

	@Override
	public int getOpacity() {
		return PixelFormat.TRANSLUCENT;
	}

	@Override
	public void setAlpha(final int alpha) {
		if (alpha != mAlpha) {
			mAlpha = alpha;
			invalidateSelf();
		}
	}

	@Override
	public void setDither(final boolean dither) {
		if (dither != mDither) {
			mDither = dither;
			invalidateSelf();
		}
	}

	@Override
	public void setColorFilter(final ColorFilter cf) {
		if (cf != mColorFilter) {
			mColorFilter = cf;
			invalidateSelf();
		}
	}

	@Override
	public boolean getPadding(final Rect padding) {
		if (mPadding != null) {
			padding.set(mPadding);
			return true;
		} else {
			return super.getPadding(padding);
		}
	}

	/**
	 * Sets the corner radius.
	 * 
	 * @param radius
	 *            the new corner radius
	 */
	public void setCornerRadius(final float radius) {
		mCornerRadius = radius;
		invalidateSelf();
	}

}
