package toto.ui.drawable;

import android.content.Context;
import android.graphics.BitmapShader;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;

/**
 * Drawable that lets you specify a tile that can be repeated across the surface
 * its assigned to.
 * 
 * @author ekent4
 * 
 */
public class TiledDrawable extends ShapeDrawable {
	public TiledDrawable(final Context context, final int resid,
			final Shape shape) {
		setShape(shape);
		final BitmapDrawable tile = (BitmapDrawable) context.getResources()
				.getDrawable(resid);
		setShaderFactory(new ShaderFactory() {
			@Override
			public Shader resize(final int width, final int height) {
				return new BitmapShader(tile.getBitmap(),
						Shader.TileMode.REPEAT, Shader.TileMode.REPEAT) {
				};
			}
		});
	}
}