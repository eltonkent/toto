package toto.ui.drawable;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

/**
 * Created by ekent4 on 2/13/14.
 */
public class TypefaceDrawable extends Drawable {

	@Override
	public void draw(Canvas canvas) {

	}

	@Override
	public void setAlpha(int alpha) {

	}

	@Override
	public void setColorFilter(ColorFilter cf) {

	}

	@Override
	public int getOpacity() {
		return 0;
	}
}
