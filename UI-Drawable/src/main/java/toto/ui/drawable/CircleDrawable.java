/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.drawable;

import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public class CircleDrawable extends Drawable {

	public CircleDrawable(final int radius, final boolean soft) {
		mRadius = radius;
		mOvalRect = new RectF(0.0F, 0.0F, radius, radius);
		mPaint = new Paint(1);
		mPaint.setColor(0xff000000);
		if (soft)
			mPaint.setMaskFilter(new BlurMaskFilter(2.0F,
					android.graphics.BlurMaskFilter.Blur.NORMAL));
	}

	@Override
	public void draw(final Canvas canvas) {
		final RectF rect = new RectF(getBounds());
		canvas.drawColor(mBackgroundColor);
		canvas.drawCircle(rect.centerX(), rect.centerY(), mRadius / 2, mPaint);
	}

	public void setBackgroundColor(final int color) {
		mBackgroundColor = color;
	}

	@Override
	public int getOpacity() {
		return -1;
	}

	@Override
	public void setAlpha(final int i) {
	}

	@Override
	public void setColorFilter(final ColorFilter colorfilter) {
	}

	Paint mPaint;
	int mRadius;
	int mBackgroundColor;
	RectF mOvalRect;
}
