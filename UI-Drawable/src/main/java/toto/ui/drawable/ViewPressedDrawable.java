/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.drawable;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

/**
 * Drawable that can be used to setKey for a Button(or any view) to change UI
 * look when its pressed. is pressed. {@link toto.ui.UIUtils.setPressedState}
 * 
 * @author elton.kent
 * 
 */
public class ViewPressedDrawable extends LayerDrawable {

	// The color filter to apply when the button is pressed
	protected ColorFilter _pressedFilter = new LightingColorFilter(
			Color.LTGRAY, 1);
	// Alpha value when the button is disabled
	protected int _disabledAlpha = 100;
	// Alpha value when the button is enabled
	protected int _fullAlpha = 255;

	public ViewPressedDrawable(final Drawable d) {
		super(new Drawable[] { d });
	}

	@Override
	protected boolean onStateChange(final int[] states) {
		boolean enabled = false;
		boolean pressed = false;

		for (final int state : states) {
			if (state == android.R.attr.state_enabled)
				enabled = true;
			else if (state == android.R.attr.state_pressed)
				pressed = true;
		}

		mutate();
		if (enabled && pressed) {
			setColorFilter(_pressedFilter);
		} else if (!enabled) {
			setColorFilter(null);
			setAlpha(_disabledAlpha);
		} else {
			setColorFilter(null);
			setAlpha(_fullAlpha);
		}

		invalidateSelf();

		return super.onStateChange(states);
	}

	@Override
	public boolean isStateful() {
		return true;
	}
}
