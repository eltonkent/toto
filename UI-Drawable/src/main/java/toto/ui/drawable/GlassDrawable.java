package toto.ui.drawable;

import toto.bitmap.ToToJavaBitmap;
import toto.bitmap.filters.BlurFilters;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;

/**
 * BETA
 * 
 * @author Mobifluence Interactive
 * @since 1.1
 */
public class GlassDrawable extends Drawable {

    Bitmap bitmap;
    Context context;

    public GlassDrawable(final Context context) {
        this.context = context;
    }

    protected void onBoundsChange(final Rect bounds) {
        Log.d("ToTo", "Drawable bounds changed" + bounds);
        createGlassBitmap(bounds);
    }

    private void createGlassBitmap(final Rect bounds) {
        bitmap = Bitmap.createBitmap(300, 300, Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        final Paint paint = new Paint();
        // paint.setAlpha(100);
        paint.setColor(Color.GRAY);
        canvas.drawPaint(paint);

        // bitmap = BlurFilters.stackBlurRS(context, bitmap, 7);

        // bitmap = BitmapFactory.decodeResource(context.getResources(),
        // R.drawable.girlskin);

        final ToToJavaBitmap bm = new ToToJavaBitmap(bitmap);
        BlurFilters.stackBlur(bm, 50);
        bitmap = bm.getBitmap();
        invalidateSelf();
    }

    @Override
    public void draw(final Canvas canvas) {

        if (bitmap != null) {
            final Paint paint = new Paint();
            paint.setAlpha(150);
            canvas.drawBitmap(bitmap, 0, 0, paint);
        }
        // final RectF rect = new RectF(getBounds());
        // final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // paint.setColor(0xffffff);
        // paint.setAlpha(100);
        // paint.setMaskFilter(new BlurMaskFilter(canvas.getWidth() / 2,
        // BlurMaskFilter.Blur.NORMAL));
        // canvas.drawPaint(paint);
    }

    @Override
    public void setColorFilter(final ColorFilter cf) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }

    @Override
    public void setAlpha(final int alpha) {
        // TODO Auto-generated method stub

    }
}