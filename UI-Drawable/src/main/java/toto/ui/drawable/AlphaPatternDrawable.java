/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.drawable;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/**
 * This drawable that draws a simple white and gray chessboard pattern. It's
 * pattern you will often see as a background behind a partly transparent image
 * in many applications.
 * 
 */
public class AlphaPatternDrawable extends Drawable {

	/**
	 * Bitmap in which the pattern will be cahched.
	 */
	private Bitmap mBitmap;

	private final Paint mPaint = new Paint();
	private final Paint mPaintGray = new Paint();
	private final Paint mPaintWhite = new Paint();

	private int mRectangleSize = 10;
	private int numRectanglesHorizontal;

	private int numRectanglesVertical;

	public AlphaPatternDrawable(final int rectangleSize) {
		mRectangleSize = rectangleSize;
		mPaintWhite.setColor(0xffffffff);
		mPaintGray.setColor(0xffcbcbcb);
	}

	@Override
	public void draw(final Canvas canvas) {
		canvas.drawBitmap(mBitmap, null, getBounds(), mPaint);
	}

	/**
	 * This will generate a bitmap with the pattern as big as the rectangle we
	 * were allow to draw on. We do this to chache the bitmap so we don't need
	 * to recreate it each time draw() is called since it takes a few
	 * milliseconds.
	 */
	private void generatePatternBitmap() {

		if (getBounds().width() <= 0 || getBounds().height() <= 0) {
			return;
		}

		mBitmap = Bitmap.createBitmap(getBounds().width(),
				getBounds().height(), Config.ARGB_8888);
		final Canvas canvas = new Canvas(mBitmap);

		final Rect r = new Rect();
		boolean verticalStartWhite = true;
		for (int i = 0; i <= numRectanglesVertical; i++) {

			boolean isWhite = verticalStartWhite;
			for (int j = 0; j <= numRectanglesHorizontal; j++) {

				r.top = i * mRectangleSize;
				r.left = j * mRectangleSize;
				r.bottom = r.top + mRectangleSize;
				r.right = r.left + mRectangleSize;

				canvas.drawRect(r, isWhite ? mPaintWhite : mPaintGray);

				isWhite = !isWhite;
			}

			verticalStartWhite = !verticalStartWhite;

		}

	}

	@Override
	public int getOpacity() {
		return 0;
	}

	@Override
	protected void onBoundsChange(final Rect bounds) {
		super.onBoundsChange(bounds);

		final int height = bounds.height();
		final int width = bounds.width();

		numRectanglesHorizontal = (int) Math.ceil((width / mRectangleSize));
		numRectanglesVertical = (int) Math.ceil(height / mRectangleSize);

		generatePatternBitmap();

	}

	@Override
	public void setAlpha(final int alpha) {
		throw new UnsupportedOperationException(
				"Alpha is not supported by this drawwable.");
	}

	@Override
	public void setColorFilter(final ColorFilter cf) {
		throw new UnsupportedOperationException(
				"ColorFilter is not supported by this drawwable.");
	}

}
