package toto.text.io;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import toto.io.IOUtils;
import toto.text.TextParser;

public class StreamParser implements TextParser {

	long lines = 1l;
	long columns = 1l;
	long offset = 0;

	Reader reader;
	char[] buf = new char[256];
	int start = 0;
	int end = 0;
	StringBuilder cache = new StringBuilder(1000);

	public StreamParser(InputStream in) throws IOException {
		if (!in.markSupported())
			in = new BufferedInputStream(in);
		this.reader = new InputStreamReader(in, IOUtils.determineEncoding(in));
	}

	public StreamParser(final Reader reader) {
		if (reader == null) {
			throw new NullPointerException();
		}
		this.reader = reader;
	}

	public int next() throws IOException {
		if (start == end) {
			final int size = reader.read(buf, start,
					Math.min(buf.length - start, buf.length / 2));
			if (size != -1) {
				end = (end + size) % buf.length;
			} else {
				return -1;
			}
		}
		final char c = buf[start];
		if (c == '\r'
				|| (c == '\n' && buf[(start + buf.length - 1) % (buf.length)] != '\r')) {
			lines++;
			columns = 0;
		} else {
			columns++;
		}
		offset++;
		start = (start + 1) % buf.length;
		return c;
	}

	public void back() {
		offset--;
		columns--;
		start = (start + buf.length - 1) % buf.length;
	}

	public long getLineNumber() {
		return lines;
	}

	public long getColumnNumber() {
		return columns;
	}

	public long getOffset() {
		return offset;
	}

	public StringBuilder getCachedBuilder() {
		cache.setLength(0);
		return cache;
	}

	public String toString() {
		final StringBuffer sb = new StringBuffer();
		final int maxlength = (columns - 1 < buf.length) ? (int) columns - 1
				: buf.length - 1;
		for (int i = maxlength; i >= 0; i--) {
			sb.append(buf[(start - 2 + buf.length - i) % (buf.length - 1)]);
		}
		return sb.toString();
	}
}
