package toto.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import toto.security.checksum.LuhnMod10;

/**
 * Created by ekent4
 * 
 * @see toto.net.NetValidationUtils
 */
public class ValidationUtils {

	/**
	 * Validates an SSN with a range of different delimiters.
	 * 
	 * @param ssn
	 * @return true if the <code>ssn</code> is valid.
	 */
	public static boolean isValidSNN(final String ssn) {
		return match(
				ssn,
				"(?<!(\\w|\\d|-))([0-6]\\d\\d|7[01256]\\d|73[0123]|77[0123])([- ]?)(\\d{2})\\3(\\d{4})(?!(\\w|\\d|-))");
	}

	/**
	 * Does a simple representation check of the credit card number using RegEx.
	 * 
	 * @param creditCard
	 * @param is15Digit
	 *            true if its a 15 digit card like american express or diner
	 *            club
	 * @return
	 */
	public static boolean isValidCreditCard(final String creditCard,
			final boolean is15Digit) {
		String pattern;
		if (is15Digit) {
			pattern = "(3[4|7]\\d{2}|2014|2149|2131|1800)([ -]?)\\d{6}\\2\\d{5}";
		} else {
			pattern = "((6011)|(4\\d{3})|(5[1-5]\\d{2}))[ -]?(\\d{4}[ -]?){3}";
		}
		return match(creditCard, pattern);
	}

	/**
	 * Does a Mod10 check on the credit card number.
	 * 
	 * @see ChecksumUtils#getLuhnMod10(String)
	 */
	public static boolean isValidCreditCard(final String cardNumber) {
		return new LuhnMod10().digest(cardNumber);
	}

	/**
	 * Validate US zip code
	 * 
	 * @param zip
	 * @return
	 */
	public static boolean isValidUSZip(final String zip) {
		return match(zip, "^\\d{5}(-\\d{4})?$");
	}

	/**
	 * Check if the given text has a smiley ( :), ;o) ,:(
	 * 
	 * @param text
	 * @return
	 */
	public static boolean hasSmiley(final String text) {
		return match(text, "[:]{1}[-~+o]?[)&gt;]+");
	}

	/**
	 * Check if the given text is alpha numeric
	 * 
	 * @param text
	 * @return
	 * @see toto.text.StringUtils#containsAlphabets(String)
	 * @see toto.text.StringUtils#containsNumbers(String)
	 */
	public static boolean isAlphaNumeric(final String text) {
		return match(text, "^[a-zA-Z0-9]+$");
	}

	private static final boolean match(final String text, final String pattern) {
		final Pattern patt = Pattern.compile(pattern);
		final Matcher matcher = patt.matcher(text);
		return matcher.matches();
	}

	public static boolean isHexNumber(final String number) {
		return match(number, "^([0-9a-fA-F]){8}$");
	}

	/**
	 * Check if the given text is HTML 4
	 * 
	 * @param text
	 * @return
	 */
	public static boolean isHTML4(final String text) {
		final String pattern = "(<\\/?)(?i:(?<element>a(bbr|cronym|ddress|pplet|rea)?|b(ase(font)?|do|ig|lockquote|ody|r|utton)?|c(aption|enter|ite|(o(de|l(group)?)))|d(d|el|fn|i(r|v)|l|t)|em|f(ieldset|o(nt|rm)|rame(setKey)?)|h([1-6]|ead|r|tml)|i(frame|mg|n(put|s)|sindex)?|kbd|l(abel|egend|i(nk)?)|m(ap|e(nu|ta))|no(frames|script)|o(bject|l|pt(group|ion))|p(aram|re)?|q|s(amp|cript|elect|mall|pan|t(r(ike|ong)|yle)|u(b|p))|t(able|body|d|extarea|foot|h|itle|r|t)|u(l)?|var))(\\s(?<attr>.+?))*>\n";
		return match(text, pattern);
	}
}
