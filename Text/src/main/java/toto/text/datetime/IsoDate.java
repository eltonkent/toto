package toto.text.datetime;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class IsoDate {

	public static final int DATE = 1;
	public static final int TIME = 2;
	public static final int DATE_TIME = 3;

	static void dd(final StringBuffer buf, final int i) {
		buf.append((char) (('0') + i / 10));
		buf.append((char) (('0') + i % 10));
	}

	public static String dateToString(final Date date, final int type) {
		final Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("GMT"));
		c.setTime(date);
		final StringBuffer buf = new StringBuffer();
		if ((type & DATE) != 0) {
			final int year = c.get(Calendar.YEAR);
			dd(buf, year / 100);
			dd(buf, year % 100);
			buf.append('-');
			dd(buf, c.get(Calendar.MONTH) - Calendar.JANUARY + 1);
			buf.append('-');
			dd(buf, c.get(Calendar.DAY_OF_MONTH));
			if (type == DATE_TIME)
				buf.append("T");
		}

		if ((type & TIME) != 0) {
			dd(buf, c.get(Calendar.HOUR_OF_DAY));
			buf.append(':');
			dd(buf, c.get(Calendar.MINUTE));
			buf.append(':');
			dd(buf, c.get(Calendar.SECOND));
			buf.append('.');
			final int ms = c.get(Calendar.MILLISECOND);
			buf.append((char) (('0') + (ms / 100)));
			dd(buf, ms % 100);
			buf.append('Z');
		}
		return buf.toString();
	}

	public static Date stringToDate(String text, final int type) {
		final Calendar c = Calendar.getInstance();
		if ((type & DATE) != 0) {
			c.set(Calendar.YEAR, Integer.parseInt(text.substring(0, 4)));
			c.set(Calendar.MONTH, Integer.parseInt(text.substring(5, 7)) - 1
					+ Calendar.JANUARY);
			c.set(Calendar.DAY_OF_MONTH,
					Integer.parseInt(text.substring(8, 10)));

			if (type != DATE_TIME || text.length() < 11) {
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				return c.getTime();
			}
			text = text.substring(11);
		} else
			c.setTime(new Date(0));
		c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(text.substring(0, 2)));
		// -11
		c.set(Calendar.MINUTE, Integer.parseInt(text.substring(3, 5)));
		c.set(Calendar.SECOND, Integer.parseInt(text.substring(6, 8)));

		int pos = 8;
		if (pos < text.length() && text.charAt(pos) == '.') {
			int ms = 0;
			int f = 100;
			while (true) {
				final char d = text.charAt(++pos);
				if (d < '0' || d > '9')
					break;
				ms += (d - '0') * f;
				f /= 10;
			}
			c.set(Calendar.MILLISECOND, ms);
		} else
			c.set(Calendar.MILLISECOND, 0);
		if (pos < text.length()) {
			if (text.charAt(pos) == '+' || text.charAt(pos) == '-')
				c.setTimeZone(TimeZone.getTimeZone("GMT" + text.substring(pos)));
			else if (text.charAt(pos) == 'Z')
				c.setTimeZone(TimeZone.getTimeZone("GMT"));
			else
				throw new RuntimeException("illegal time format!");
		}
		return c.getTime();
	}

}
