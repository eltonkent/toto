package toto.text;

import java.io.IOException;


/**
 * Interface to create multiple parsing streams
 * 
 * @author Mobifluence Interactive
 * @see toto.io.streams.StreamParser
 * @see CharSequenceParser
 */
public interface TextParser {
	public int next() throws IOException;

	public void back();

	public long getLineNumber();

	public long getColumnNumber();

	public long getOffset();

	public StringBuilder getCachedBuilder();
}
