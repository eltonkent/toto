/**
 * Text/Date/Time manipulation, extraction and validation utilities.
 * @author Elton Kent
 *
 */
package toto.text;