package toto.text;

public class CharSequenceParser implements TextParser {
	int lines = 1;
	int columns = 1;
	int offset = 0;

	CharSequence cs;
	StringBuilder cache;

	public CharSequenceParser(final CharSequence cs, final int size) {
		if (cs == null) {
			throw new NullPointerException();
		}
		this.cs = cs;
		this.cache = new StringBuilder(size);
	}

	public int next() {
		if (offset < cs.length()) {
			final char c = cs.charAt(offset++);
			if (c == '\r'
					|| (c == '\n' && offset > 1 && cs.charAt(offset - 2) != '\r')) {
				lines++;
				columns = 0;
			} else {
				columns++;
			}
			return c;
		}
		return -1;
	}

	public void back() {
		offset--;
		columns--;
	}

	public long getLineNumber() {
		return lines;
	}

	public long getColumnNumber() {
		return columns;
	}

	public long getOffset() {
		return offset;
	}

	public StringBuilder getCachedBuilder() {
		cache.setLength(0);
		return cache;
	}

	public String toString() {
		return cs.subSequence(offset - columns + 1, offset).toString();
	}
}
