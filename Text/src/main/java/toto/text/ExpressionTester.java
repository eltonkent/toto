package toto.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Expression Tester allows you to construct and test against a string against a
 * series of difficult regular expressions.
 * <p>
 * <div>
 * <h3>Using Expression Tester</h3>
 * <h4>Constructing an Expression Tester</h4>
 * The following constructs an ExpressionTester for validating a URL.
 * 
 * <pre>
 * ExpressionTester testExp=new ExpressionTester.Builder()
 * 			<span>//startOfLine() always starts the building</span>
 * 			.startOfLine();
 * 			.then("http");
 * 			.maybe("s");
 * 			.then("://")
 * 			.maybe("www.")
 * 			<span>//we dont need spaces in a URL</span>
 * 			.anythingButNot(" ")
 * 			.endOfLine()
 * 			.build();
 * </pre>
 * 
 * <h4>Validating a built expression</h4>
 * 
 * <pre>
 * String url="http://mobifluence.com";
 * <span>//returns true</span>
 * boolean valid=testExp.testExact(url);
 * </pre>
 * 
 * </div>
 * </p>
 * 
 * @author Mobifluence Interactive
 * 
 */
public class ExpressionTester {

	private final Pattern pattern;

	/**
	 * Helps contruct input guidelines for Expression Tester
	 * 
	 * @author Mobifluence Interactive
	 * 
	 */
	public static class Builder {

		private String prefixes = "", source = "", suffixes = "";
		private Pattern pattern;
		private int modifiers = Pattern.MULTILINE;

		private String sanitize(final String pValue) {
			final Matcher matcher = Pattern.compile("").matcher("")
					.usePattern(Pattern.compile("[^\\w]"));
			int lastEnd = 0;
			String result = "";
			matcher.reset(pValue);
			boolean matcherCalled = false;
			while (matcher.find()) {
				matcherCalled = true;
				if (matcher.start() != lastEnd) {
					result += pValue.substring(lastEnd, matcher.start());
				}
				result += "\\"
						+ pValue.substring(matcher.start(), matcher.end());
				lastEnd = matcher.end();
			}
			if (!matcherCalled) {
				return pValue;
			}
			return result;
		}

		public Builder add(final String pValue) {
			this.source += pValue;
			return this;
		}

		public ExpressionTester build() {
			pattern = Pattern.compile(this.prefixes + this.source
					+ this.suffixes, this.modifiers);
			return new ExpressionTester(this);
		}

		public Builder startOfLine(final boolean pEnable) {
			this.prefixes = pEnable ? "^" : "";
			return this;
		}

		public Builder startOfLine() {
			return startOfLine(true);
		}

		public Builder endOfLine(final boolean pEnable) {
			this.suffixes = pEnable ? "$" : "";
			return this;
		}

		public Builder endOfLine() {
			return endOfLine(true);
		}

		/**
		 * Specify the next qualifier.
		 * 
		 * @param pValue
		 * @return
		 */
		public Builder then(final String pValue) {
			this.add("(" + sanitize(pValue) + ")");
			return this;
		}

		public Builder find(final String value) {
			this.then(value);
			return this;
		}

		public Builder maybe(final String pValue) {
			this.add("(" + sanitize(pValue) + ")?");
			return this;
		}

		public Builder anything() {
			this.add("(.*)");
			return this;
		}

		public Builder anythingButNot(final String pValue) {
			this.add("([^" + sanitize(pValue) + "]*)");
			return this;
		}

		/**
		 * Any amount of text till the next qualifier is specified.
		 * 
		 * @return
		 */
		public Builder something() {
			this.add("(.+)");
			return this;
		}

		/**
		 * Any value but <code>pValue</code>
		 * 
		 * @param pValue
		 * @return
		 */
		public Builder somethingButNot(final String pValue) {
			this.add("([^" + sanitize(pValue) + "]+)");
			return this;
		}

		public Builder lineBreak() {
			this.add("(\\n|(\\r\\n))");
			return this;
		}

		/**
		 * Indicates a line break
		 * 
		 * @return
		 */
		public Builder br() {
			this.lineBreak();
			return this;
		}

		public Builder tab() {
			this.add("\\t");
			return this;
		}

		public Builder word() {
			this.add("\\w+");
			return this;
		}

		public Builder anyOf(final String pValue) {
			this.add("[" + sanitize(pValue) + "]");
			return this;
		}

		public Builder any(final String value) {
			this.anyOf(value);
			return this;
		}

		/**
		 * Define a range to search for.
		 * <p>
		 * To define a range from 0 t0 100, simple call
		 * <code>range(0,100)</code>
		 * </p>
		 * 
		 * @param pArgs
		 * @return
		 */
		public Builder range(final String... pArgs) {
			String value = "[";
			for (int _to = 1; _to < pArgs.length; _to += 2) {
				final String from = sanitize(pArgs[_to - 1]);
				final String to = sanitize(pArgs[_to]);

				value += from + "-" + to;
			}
			value += "]";

			this.add(value);
			return this;
		}

		public Builder addModifier(final char pModifier) {
			switch (pModifier) {
			case 'd':
				modifiers |= Pattern.UNIX_LINES;
				break;
			case 'i':
				modifiers |= Pattern.CASE_INSENSITIVE;
				break;
			case 'x':
				modifiers |= Pattern.COMMENTS;
				break;
			case 'm':
				modifiers |= Pattern.MULTILINE;
				break;
			case 's':
				modifiers |= Pattern.DOTALL;
				break;
			case 'u':
				modifiers |= Pattern.UNICODE_CASE;
				break;
			case 'U':
				modifiers |= 0x100;// Pattern.UNICODE_CHARACTER_CLASS;
				break;
			default:
				break;
			}

			return this;
		}

		public Builder removeModifier(final char pModifier) {
			switch (pModifier) {
			case 'd':
				modifiers ^= Pattern.UNIX_LINES;
				break;
			case 'i':
				modifiers ^= Pattern.CASE_INSENSITIVE;
				break;
			case 'x':
				modifiers ^= Pattern.COMMENTS;
				break;
			case 'm':
				modifiers ^= Pattern.MULTILINE;
				break;
			case 's':
				modifiers ^= Pattern.DOTALL;
				break;
			case 'u':
				modifiers ^= Pattern.UNICODE_CASE;
				break;
			case 'U':
				modifiers ^= 0x100;// Pattern.UNICODE_CHARACTER_CLASS;
				break;
			default:
				break;
			}

			return this;
		}

		public Builder withAnyCase(final boolean pEnable) {
			if (pEnable) {
				this.addModifier('i');
			} else {
				this.removeModifier('i');
			}
			return this;
		}

		public Builder withAnyCase() {
			return withAnyCase(true);
		}

		public Builder searchOneLine(final boolean pEnable) {
			if (pEnable) {
				this.removeModifier('m');
			} else {
				this.addModifier('m');
			}
			return this;
		}

		public Builder multiple(final String pValue) {
			String value = this.sanitize(pValue);
			switch (value.charAt(0)) {
			case '*':
			case '+':
				break;
			default:
				value += '+';
			}
			this.add(value);
			return this;
		}

		public Builder or(final String pValue) {
			if (this.prefixes.indexOf("(") == -1) {
				this.prefixes += "(";
			}
			if (this.suffixes.indexOf(")") == -1) {
				this.suffixes = ")" + this.suffixes;
			}

			this.add(")|(");
			if (pValue != null) {
				this.then(pValue);
			}
			return this;
		}
	}

	public boolean testExact(final String pToTest) {
		boolean ret = false;
		if (pToTest != null) {
			ret = pattern.matcher(pToTest).matches();
		}
		return ret;
	}

	public boolean test(final String pToTest) {
		boolean ret = false;
		if (pToTest != null) {
			ret = pattern.matcher(pToTest).find();
		}
		return ret;
	}

	private ExpressionTester(final Builder pBuilder) {
		pattern = pBuilder.pattern;
	}

	public String getText(final String toTest) {
		final Matcher m = pattern.matcher(toTest);
		final StringBuilder result = new StringBuilder();
		while (m.find()) {
			result.append(m.group());
		}
		return result.toString();
	}

	@Override
	public String toString() {
		return pattern.pattern();
	}
}
