package toto.text;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ekent4
 */
public final class ExtractionUtils {

	/**
	 * Extract Http/ftp links from a given text.
	 * <p>
	 * Looks for the following patterns<br/>
	 * <ul>
	 * <li>http[s]://www.google.com</li>
	 * <li>http[s]://www.google.com:80</li>
	 * <li>www.google.com</li>
	 * <li>http[s]://www.google.com/test/test.html</li>
	 * <li>www.google.com/test/test.html</li>
	 * <li>http[s]://google.com</li>
	 * <li>ftp[s]://google.com</li>
	 * </ul>
	 * </p>
	 * 
	 * @param text
	 * @return
	 */
	public static ArrayList<String> extractHttpLinks(final String text) {
		final ArrayList<String> links = new ArrayList<String>();
		// final String regex =
		// "\\(?\\b(http://|www[.]|https://)[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
		final Pattern p = Pattern
				.compile("\\b(((ht|f)tp(s?)\\:\\/\\/|~\\/|\\/)|www.)"
						+ "(\\w+:\\w+@)?(([-\\w]+\\.)+(com|org|net|gov"
						+ "|mil|biz|info|mobi|name|aero|jobs|museum|co"
						+ "|travel|[a-z]{2}))(:[\\d]{1,5})?"
						+ "(((\\/([-\\w~!$+|.,=]|%[a-f\\d]{2})+)+|\\/)+|\\?|#)?"
						+ "((\\?([-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?"
						+ "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)"
						+ "(&(?:[-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?"
						+ "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)*)*"
						+ "(#([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)?\\b");
		// final Pattern p = Pattern.compile(regex);
		final Matcher m = p.matcher(text);
		while (m.find()) {
			String urlStr = m.group();
			urlStr.toCharArray();
			if (urlStr.startsWith("(") && urlStr.endsWith(")")) {
				final char[] stringArray = urlStr.toCharArray();
				final char[] newArray = new char[stringArray.length - 2];
				System.arraycopy(stringArray, 1, newArray, 0,
						stringArray.length - 2);
				urlStr = new String(newArray);
				System.out.println("Finally Url =" + newArray.toString());

			}
			links.add(urlStr);
		}
		return links;
	}

	/**
	 * Extract all java script from the specified HTML text.
	 * 
	 * @param html
	 * @return
	 */
	public static List<String> extractJavaScript(final String html) {
		final List<String> links = new ArrayList<String>();
		final Pattern pattern = Pattern
				.compile("<script[^>]*>[\\w|\\t|\\r|\\W]*</script>");
		final Matcher match = pattern.matcher(html);
		while (match.find()) {
			links.add(match.group(1));
		}
		return links;
	}

	/**
	 * Extract all text that is between quotes (" ")
	 * 
	 * @param text
	 * @see #getTextBetween(char, String)
	 * @return
	 */
	public static List<String> getTextBetweenQuotes(final String text) {
		return getTextBetween('"', text);
	}

	/**
	 * Extract all text that is between the given character delimiter
	 * 
	 * 
	 * @param delimit
	 * @param text
	 * @return
	 */
	public static List<String> getTextBetween(final char delimit,
			final String text) {
		final List<String> links = new ArrayList<String>();
		final StringBuilder builder = new StringBuilder(delimit);
		builder.append("[^\"\\r\\n]*");
		builder.append(delimit);
		final Pattern pattern = Pattern.compile(builder.toString());
		final Matcher match = pattern.matcher(text);
		while (match.find()) {
			links.add(match.group(1));
		}
		return links;
	}

}
