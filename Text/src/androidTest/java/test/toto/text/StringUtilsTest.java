package test.toto.text;

import test.toto.ToToTestCase;
import toto.text.StringUtils;

public class StringUtilsTest extends ToToTestCase{
	
	public void testLCS(){
		String lcs=StringUtils.getLCS("abcdefghijklmnopqrstuvwxyz", "testinglmnopqstring");
		assertEquals(lcs, "lmnopq");
	}

	public void testReplaceAllSB(){
		StringBuilder builder=new StringBuilder("dada dede dada dede dodo dada");
		StringUtils.replaceAll(builder,"da","ta");
		assertEquals(builder.toString(), "tata dede tata dede dodo tata");
	}
}
