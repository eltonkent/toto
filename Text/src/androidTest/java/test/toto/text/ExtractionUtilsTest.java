package test.toto.text;

import java.util.List;

import test.toto.ToToTestCase;
import toto.text.ExtractionUtils;

public class ExtractionUtilsTest extends ToToTestCase {

	public void testLinkExtraction(){
		String test="this is a test String www.google.com and http://www.mobifluence.com also www.mobifluence.in";
		List<String> links=ExtractionUtils.extractHttpLinks(test);
		assertEquals(3, links.size());
	}
}
