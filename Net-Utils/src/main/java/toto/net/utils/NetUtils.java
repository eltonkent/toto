/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import toto.lang.reflect.ClassUtils;
import toto.lang.reflect.MethodUtils;
import toto.security.HexUtils;
import toto.text.CharUtils;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;

public class NetUtils {

	/**
	 * Create and initialise sn SSLContext.
	 * 
	 * @param protocol
	 *            the protocol used to instatiate the context
	 * @param keyManagers
	 *            the array of key managers, may be {@code null} but array
	 *            entries must not be {@code null}
	 * @param trustManagers
	 *            the array of trust managers, may be {@code null} but array
	 *            entries must not be {@code null}
	 * @return the initialised context.
	 * @throws IOException
	 *             this is used to wrap any {@link GeneralSecurityException}
	 *             that occurs
	 */
	public static SSLContext createSSLContext(final String protocol,
			final KeyManager[] keyManagers, final TrustManager[] trustManagers)
			throws IOException {
		SSLContext ctx;
		try {
			ctx = SSLContext.getInstance(protocol);
			ctx.init(keyManagers, trustManagers, /* SecureRandom */null);
		} catch (final GeneralSecurityException e) {
			final IOException ioe = new IOException(
					"Could not initialize SSL context");
			ioe.initCause(e);
			throw ioe;
		}
		return ctx;
	}

	/**
	 * Enable Http Response cache. Works only on Ice-cream sandwich.
	 * 
	 * @param cacheDir
	 *            Directory to be used as a response cache
	 * @param cacheSize
	 *            Size of the cache
	 */
	public static void enableHttpResponseCache(final String cacheDir,
			final long cacheSize) {
		try {
			final File httpCacheDir = new File(cacheDir, "http");

			MethodUtils.invokeExactStaticMethod(
					ClassUtils.getClass("android.net.http.HttpResponseCache"),
					"install", null, httpCacheDir, cacheSize);
		} catch (final ClassNotFoundException e) {
		} catch (final NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Encodes the string in the string buffer
	 * <p>
	 * Encodes a string so that it is suitable for transmission over HTTP
	 * </p>
	 * 
	 * @param string
	 * @return the encoded string
	 */
	public final static String encodeString(final StringBuilder string) {
		final StringBuilder encodedUrl = new StringBuilder(); // Encoded URL

		final int len = string.length();
		// Encode each URL character
		final String UNRESERVED = "-_.!~*'()\"";
		for (int i = 0; i < len; i++) {
			final char c = string.charAt(i); // Get next character
			if (((c >= '0') && (c <= '9')) || ((c >= 'a') && (c <= 'z'))
					|| ((c >= 'A') && (c <= 'Z'))) {
				// Alphanumeric characters require no encoding, append as is
				encodedUrl.append(c);
			} else {
				final int imark = UNRESERVED.indexOf(c);
				if (imark >= 0) {
					// Unreserved punctuation marks and symbols require
					// no encoding, append as is
					encodedUrl.append(c);
				} else {
					// Encode all other characters to Hex, using the format
					// "%XX",
					// where XX are the hex digits
					encodedUrl.append('%'); // Add % character
					// Encode the character's high-order nibble to Hex
					encodedUrl.append(CharUtils.toHexChar((c & 0xF0) >> 4));
					// Encode the character's low-order nibble to Hex
					encodedUrl.append(CharUtils.toHexChar(c & 0x0F));
				}
			}
		}
		return encodedUrl.toString(); // Return encoded URL
	}

	/**
	 * Get the data network's IP address
	 * 
	 * @return
	 */
	public static String getCarrierAllocatedIPAddress() {
		try {
			for (final Enumeration<NetworkInterface> en = NetworkInterface
					.getNetworkInterfaces(); en.hasMoreElements();) {
				final NetworkInterface intf = en.nextElement();
				System.out.println("Interface Name" + intf.getDisplayName());
				for (final Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					final InetAddress inetAddress = enumIpAddr.nextElement();

					if (!inetAddress.isLoopbackAddress()) {
						return inetAddress.getHostAddress().toString();
					}
				}
			}
		} catch (final SocketException ex) {
			Log.e("CDI", ex.toString());
		}

		return null;
	}

	/**
	 * Check if the given resource exists on a web server without actually
	 * downloading it.
	 * 
	 * @param url
	 * @return
	 */
	public static boolean exists(final String url) {
		try {
			HttpURLConnection.setFollowRedirects(false);
			final HttpURLConnection localHttpURLConnection = (HttpURLConnection) new URL(
					url).openConnection();
			localHttpURLConnection.setRequestMethod("HEAD");
			localHttpURLConnection
					.setRequestProperty(
							"User-Agent",
							"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
			return (localHttpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (final Exception e) {
		}
		return false;
	}

	/**
	 * Finds this device's global IP address
	 * 
	 * @param url
	 *            the url to find the global IP
	 * @return The global IP address, or null if a problem occurred
	 */
	public static Inet4Address getGlobalAddress(final String url) {
		try {
			final URLConnection uc = new URL(url).openConnection();
			final BufferedReader br = new BufferedReader(new InputStreamReader(
					uc.getInputStream()));
			return (Inet4Address) InetAddress.getByName(br.readLine());
		} catch (final MalformedURLException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Finds a local, non-loopback, IPv4 address
	 * 
	 * @return The first non-loopback IPv4 address found, or <code>null</code>
	 *         if no such addresses found
	 * @throws SocketException
	 *             If there was a problem querying the network interfaces
	 */
	public static InetAddress getLocalAddress() throws SocketException {
		final Enumeration<NetworkInterface> ifaces = NetworkInterface
				.getNetworkInterfaces();
		while (ifaces.hasMoreElements()) {
			final NetworkInterface iface = ifaces.nextElement();
			final Enumeration<InetAddress> addresses = iface.getInetAddresses();

			while (addresses.hasMoreElements()) {
				final InetAddress addr = addresses.nextElement();
				if ((addr instanceof Inet4Address) && !addr.isLoopbackAddress()) {
					return addr;
				}
			}
		}
		return null;
	}

	/**
	 * Get I/P address of wifi adapter. The routine first checks if the Wifi
	 * adapter is connected before obtaining the address.
	 * 
	 * @param paramContext
	 * @return I/P address of Wifi Adapter. Null if Wifi is not connected
	 */
	public static String getWifiIpAddress(final Context paramContext) {
		final WifiInfo localWifiInfo = ((WifiManager) paramContext
				.getSystemService("wifi")).getConnectionInfo();
		int ipaddress = -1;
		if ((localWifiInfo != null) && (wifiIsConnected(paramContext))) {
			ipaddress = localWifiInfo.getIpAddress();
			return Formatter.formatIpAddress(ipaddress);
		}
		return null;
	}

	public static boolean wifiIsConnected(final Context paramContext) {
		for (final NetworkInfo localNetworkInfo : ((ConnectivityManager) paramContext
				.getSystemService("connectivity")).getAllNetworkInfo())
			if ((localNetworkInfo.getTypeName().equalsIgnoreCase("WIFI"))
					&& (localNetworkInfo.isConnected())) {
				return true;
			}
		return false;
	}

	/**
	 * Makes the platform trust all SSL connections.
	 * <p>
	 * Warning: This should be used for development purposes only
	 * </p>
	 */
	public static void trustAllSecureConnections() {
		try {
			HttpsURLConnection
					.setDefaultHostnameVerifier(new HostnameVerifier() {
						@Override
						public boolean verify(final String hostname,
								final SSLSession session) {
							return true;
						}
					});
			final SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[] { new X509TrustManager() {
				@Override
				public void checkClientTrusted(final X509Certificate[] chain,
						final String authType) throws CertificateException {
				}

				@Override
				public void checkServerTrusted(final X509Certificate[] chain,
						final String authType) throws CertificateException {
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}
			} }, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(context
					.getSocketFactory());
		} catch (final Exception e) { // should never happen
			e.printStackTrace();
		}
	}

	/**
	 * @return True if the url is correctly URL encoded
	 */
	public static boolean isValidURLEncoding(final String url) {
		final int count = url.length();
		if (count == 0) {
			return false;
		}

		int index = url.indexOf('%');
		while (index >= 0 && index < count) {
			if (index < count - 2) {
				try {
					HexUtils.parseHex(url.charAt(++index));
					HexUtils.parseHex(url.charAt(++index));
				} catch (final IllegalArgumentException e) {
					return false;
				}
			} else {
				return false;
			}
			index = url.indexOf('%', index + 1);
		}
		return true;
	}

	/**
	 * check if the app has network connectivity
	 * 
	 * @param ctxt
	 * @return
	 */
	public static boolean isConnected(final Context ctxt) {
		final NetworkInfo nfo = ((ConnectivityManager) ctxt
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo();
		if (nfo != null) {
			return nfo.isConnected();
		}
		return false;
	}

	/**
	 * Check if background data is enabled
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isBackgroundDataEnabled(final Context context) {
		final ConnectivityManager localConnectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (localConnectivityManager != null)
			return localConnectivityManager.getBackgroundDataSetting();
		return true;
	}

	/**
	 * Converts the given IP address to Classless Inter-Domain Routing number
	 * 
	 * @throws IllegalArgumentException
	 *             if the ip address is invalid.
	 * @param ip
	 * @return
	 * 
	 */
	public static int IpToCidr(final String ip) {
		if (!NetValidationUtils.isValidIPv4(ip)) {
			throw new IllegalArgumentException("Invalid IP address");
		}
		double sum = -2;
		final String[] part = ip.split("\\.");
		for (final String p : part) {
			sum += 256D - Double.parseDouble(p);
		}
		return 32 - (int) (Math.log(sum) / Math.log(2d));
	}

	/**
	 * Check if the given host is reachable in the specified timeout.
	 * 
	 * @param host
	 */
	public static void doPing(final String host, final int timeout) {
		try {
			// TODO: Use ProcessBuilder ?
			Runtime.getRuntime().exec(
					String.format("/system/bin/ping -q -n -w 1 -c 1 %s", host));
		} catch (final Exception e) {
			try {
				if (InetAddress.getByName(host).isReachable(timeout)) {
				}
			} catch (final Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Get the average response time of the given host.
	 * <p>
	 * Uses the ping utility to check reachability and response time.
	 * </p>
	 * 
	 * @param host
	 * @param timeout
	 *            good timeout values are between 1000 and 5000
	 * @return
	 */
	public static int getAverageResponseTime(final String host,
			final int timeout) {
		// TODO: Reduce allocation
		BufferedReader reader = null;
		final int rate = 800;
		Matcher matcher;
		try {
			final Process proc = Runtime.getRuntime().exec(
					"/system/bin/ping -A -q -n -w 3 -W 2 -c 3 " + host);
			reader = new BufferedReader(new InputStreamReader(
					proc.getInputStream()), 512);
			String line;
			final Pattern mPattern = null;
			while ((line = reader.readLine()) != null) {
				matcher = mPattern.matcher(line);
				if (matcher.matches()) {
					reader.close();
					return (int) Float.parseFloat(matcher.group(1));
				}
			}
			reader.close();
		} catch (final Exception e) {
			try {
				final long start = System.nanoTime();
				if (InetAddress.getByName(host).isReachable(timeout)) {
					return (int) ((System.nanoTime() - start) / 1000);
				}
			} catch (final Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (final IOException e) {
			}
		}
		return rate;
	}

	/**
	 * Send a stop signal to the given port.
	 * <p>
	 * Stops any incoming stream on the given port.
	 * </p>
	 * 
	 * @param port
	 * @throws IOException
	 * @throws UnknownHostException
	 */
	public static void sendStopSignal(final int port)
			throws UnknownHostException, IOException {
		final Socket s = new Socket(InetAddress.getByName("127.0.0.1"), port);
		final OutputStream out = s.getOutputStream();
		System.err.println("sending server stop request");
		out.write(("\r\n").getBytes());
		out.flush();
		s.close();
	}

}
