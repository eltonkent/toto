/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Performs validations of email,IP addresses,URL's
 * 
 * @author elton.kent
 * @see toto.text.ValidationUtils
 */
public class NetValidationUtils {

	public static boolean isValidEmailAddress(final String address) {
		final String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		final Pattern pattern = Pattern.compile(emailPattern);
		final Matcher matcher = pattern.matcher(address);
		return matcher.matches();
	}

	/**
	 * Validate the given IPv4 address.
	 * 
	 * @param address
	 *            the IP address as a String.
	 * 
	 * @return true if a valid IPv4 address, false otherwise
	 */
	public static boolean isValidIPv4(final String address) {
		final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

		final Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
		final Matcher matcher = pattern.matcher(address);
		return matcher.matches();
	}

	public static boolean isValidIPv4WithNetmask(final String address) {
		final int index = address.indexOf("/");
		final String mask = address.substring(index + 1);

		return (index > 0) && isValidIPv4(address.substring(0, index))
				&& (isValidIPv4(mask) || isMaskValue(mask, 32));
	}

	/**
	 * Validate the given IPv6 address.
	 * 
	 * @param address
	 *            the IP address as a String.
	 * 
	 * @return true if a valid IPv4 address, false otherwise
	 */
	public static boolean isValidIPv6(final String address) {
		if (address.length() == 0) {
			return false;
		}

		int octet;
		int octets = 0;

		final String temp = address + ":";
		boolean doubleColonFound = false;
		int pos;
		int start = 0;
		while (start < temp.length()
				&& (pos = temp.indexOf(':', start)) >= start) {
			if (octets == 8) {
				return false;
			}

			if (start != pos) {
				final String value = temp.substring(start, pos);

				if (pos == (temp.length() - 1) && value.indexOf('.') > 0) {
					if (!isValidIPv4(value)) {
						return false;
					}

					octets++; // add an extra one as address covers 2 words.
				} else {
					try {
						octet = Integer
								.parseInt(temp.substring(start, pos), 16);
					} catch (final NumberFormatException ex) {
						return false;
					}
					if (octet < 0 || octet > 0xffff) {
						return false;
					}
				}
			} else {
				if (pos != 1 && pos != temp.length() - 1 && doubleColonFound) {
					return false;
				}
				doubleColonFound = true;
			}
			start = pos + 1;
			octets++;
		}

		return octets == 8 || doubleColonFound;
	}

	public static boolean isValidIPv6WithNetmask(final String address) {
		final int index = address.indexOf("/");
		final String mask = address.substring(index + 1);

		return (index > 0)
				&& (isValidIPv6(address.substring(0, index)) && (isValidIPv6(mask) || isMaskValue(
						mask, 128)));
	}

	/**
	 * Uses regular expressions to validate a URL
	 * 
	 * @param url
	 * @return
	 */
	public static boolean isValidURL(final String url) {
		final String regex = "\\b(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]";
		final Pattern patt = Pattern.compile(regex);
		final Matcher matcher = patt.matcher(url);
		return matcher.matches();
	}

	private static boolean isMaskValue(final String component, final int size) {
		try {
			final int value = Integer.parseInt(component);

			return value >= 0 && value <= size;
		} catch (final NumberFormatException e) {
			return false;
		}
	}

}
