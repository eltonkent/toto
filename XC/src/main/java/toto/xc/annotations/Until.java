package toto.xc.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation that indicates the version number until a member or a type
 * should be present. Basically, if Json is created with a version number that
 * exceeds the value stored in the {@code Until} annotation then the field will
 * be ignored from the JSON output. This annotation is useful to manage
 * versioning of your JSON classes for a web-service.
 * 
 * <p>
 * This annotation has no effect unless you build {@link toto.xc.json.Json} with
 * a {@link toto.xc.json.JsonBuilder} and invoke
 * {@link toto.xc.json.JsonBuilder#setVersion(double)} method.
 * 
 * <p>
 * Here is an example of how this annotation is meant to be used:
 * </p>
 * 
 * <pre>
 * public class User {
 *   private String firstName;
 *   private String lastName;
 *   &#64Until(1.1) private String emailAddress;
 *   &#64Until(1.1) private String password;
 * }
 * </pre>
 * 
 * <p>
 * If you created Json with {@code new Json()}, the {@code toJson()} and
 * {@code fromJson()} methods will use all the fields for serialization and
 * deserialization. However, if you created Json with
 * {@code Json gson = new JsonBuilder().setVersion(1.2).create()} then the
 * {@code toJson()} and {@code fromJson()} methods of Json will exclude the
 * {@code emailAddress} and {@code password} fields from the example above,
 * because the version number passed to the JsonBuilder, {@code 1.2}, exceeds
 * the version number setKey on the {@code Until} annotation, {@code 1.1}, for
 * those fields.
 * 
 * @author Inderjeet Singh
 * @author Joel Leitch
 * @since 1.3
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.TYPE })
public @interface Until {

	/**
	 * the value indicating a version number until this member or type should be
	 * ignored.
	 */
	double value();
}
