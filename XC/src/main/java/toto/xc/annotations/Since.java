package toto.xc.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation that indicates the version number since a member or a type has
 * been present. This annotation is useful to manage versioning of your Json
 * classes for a web-service.
 * 
 * <p>
 * This annotation has no effect unless you build {@link toto.xc.json.Json} with
 * a {@link toto.xc.json.JsonBuilder} and invoke
 * {@link toto.xc.json.JsonBuilder#setVersion(double)} method.
 * 
 * <p>
 * Here is an example of how this annotation is meant to be used:
 * </p>
 * 
 * <pre>
 * public class User {
 *   private String firstName;
 *   private String lastName;
 *   &#64Since(1.0) private String emailAddress;
 *   &#64Since(1.0) private String password;
 *   &#64Since(1.1) private Address address;
 * }
 * </pre>
 * 
 * <p>
 * If you created Json with {@code new Json()}, the {@code toJson()} and
 * {@code fromJson()} methods will use all the fields for serialization and
 * deserialization. However, if you created Json with
 * {@code Json gson = new JsonBuilder().setVersion(1.0).create()} then the
 * {@code toJson()} and {@code fromJson()} methods of Json will exclude the
 * {@code address} field since it's version number is setKey to {@code 1.1}.
 * </p>
 * 
 * @author Inderjeet Singh
 * @author Joel Leitch
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.TYPE })
public @interface Since {
	/**
	 * the value indicating a version number since this member or type has been
	 * present.
	 */
	double value();
}
