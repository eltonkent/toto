package toto.xc.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation that indicates this member should be serialized to JSON/XML with
 * the provided name value as its field name.
 * 
 * <p>
 * This annotation will override any {@link toto.xc.json.FieldNamingPolicy},
 * including the default field naming policy, that may have been setKey on the
 * {@link toto.xc.json.Json} instance. A different naming policy can setKey
 * using the {@code JsonBuilder} class. See
 * {@link toto.xc.json.JsonBuilder#setFieldNamingPolicy(toto.xc.json.FieldNamingPolicy)}
 * for more information.
 * </p>
 * 
 * <p>
 * Here is an example of how this annotation is meant to be used:
 * </p>
 * 
 * <pre>
 * public class SomeClassWithFields {
 *   &#64SerializedName("name") private final String someField;
 *   private final String someOtherField;
 * 
 *   public SomeClassWithFields(String a, String b) {
 *     this.someField = a;
 *     this.someOtherField = b;
 *   }
 * }
 * </pre>
 * 
 * <p>
 * The following shows the output that is generated when serializing an instance
 * of the above example class:
 * </p>
 * 
 * <pre>
 * SomeClassWithFields objectToSerialize = new SomeClassWithFields("a", "b");
 * Json gson = new Json();
 * String jsonRepresentation = gson.toJson(objectToSerialize);
 * System.out.println(jsonRepresentation);
 * 
 * ===== OUTPUT =====
 * {"name":"a","someOtherField":"b"}
 * </pre>
 * 
 * <p>
 * NOTE: The value you specify in this annotation must be a valid JSON field
 * name.
 * </p>
 * 
 * @see toto.xc.json.FieldNamingPolicy
 * 
 * @author Inderjeet Singh
 * @author Joel Leitch
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SerializedName {

	/**
	 * @return the desired name of the field when it is serialized
	 */
	String value();
}
