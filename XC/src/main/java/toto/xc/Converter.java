/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.xc;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Primitive mapper for XML and JSON serialization classes
 * 
 * @author elton.stephen.kent
 * 
 */
public final class Converter {

	private static Map<Class<?>, Integer> clzTypeKeyMap = new HashMap<Class<?>, Integer>();

	private static final int TYPE_BOOLEAN = 8;
	private static final int TYPE_CHAR = 5;
	private static final int TYPE_DATE = 9;
	private static final int TYPE_DOUBLE = 7;
	private static final int TYPE_FLOAT = 6;
	private static final int TYPE_INT = 3;
	private static final int TYPE_LONG = 4;
	private static final int TYPE_SHORT = 2;
	private static final int TYPE_STRING = 1;

	static {
		clzTypeKeyMap.put(String.class, TYPE_STRING);
		clzTypeKeyMap.put(short.class, TYPE_SHORT);
		clzTypeKeyMap.put(int.class, TYPE_INT);
		clzTypeKeyMap.put(long.class, TYPE_LONG);
		clzTypeKeyMap.put(char.class, TYPE_CHAR);
		clzTypeKeyMap.put(float.class, TYPE_FLOAT);
		clzTypeKeyMap.put(double.class, TYPE_DOUBLE);
		clzTypeKeyMap.put(boolean.class, TYPE_BOOLEAN);
		clzTypeKeyMap.put(Date.class, TYPE_DATE);
	}

	/**
	 * Converts a {@link String} value to specified type, if possible.
	 * 
	 * @param raw
	 *            Raw, string value, to be converted
	 * @param clz
	 *            Target type to be converted to
	 * @return Converted value, if converstion was possible, null otherwise
	 * @throws NumberFormatException
	 *             If the value was not in correct format, while converting to
	 *             numeric type
	 * @throws RuntimeException
	 *             If the value was not in correct format, while converting to
	 *             Date or Boolean type
	 */
	public static Object convertTo(final String raw, final Class<?> clz) {
		Object value = null;
		if (clzTypeKeyMap.containsKey(clz)) {
			final int code = clzTypeKeyMap.get(clz);
			switch (code) {
			case TYPE_STRING:
				value = raw;
				break;
			case TYPE_SHORT:
				value = Short.parseShort(raw);
				break;
			case TYPE_INT:
				value = Integer.parseInt(raw);
				break;
			case TYPE_LONG:
				value = Long.parseLong(raw);
				break;
			case TYPE_CHAR:
				if ((raw != null) && (raw.length() > 0)) {
					value = raw.charAt(0);
				} else {
					value = '\0';
				}
				break;
			case TYPE_FLOAT:
				value = Float.parseFloat(raw);
				break;
			case TYPE_DOUBLE:
				value = Double.parseDouble(raw);
				break;
			case TYPE_BOOLEAN:
				value = Boolean.parseBoolean(raw);
				break;
			case TYPE_DATE:
				value = Date.parse(raw);
				break;
			default:
				break;
			}
		}
		return value;
	}

	public static boolean isBoolean(final Class<?> clz) {
		final Integer type = clzTypeKeyMap.get(clz);
		if (type != null && type == TYPE_BOOLEAN)
			return true;
		else
			return false;
	}

	public static boolean isCollectionType(final Class<?> type) {
		return Collection.class.isAssignableFrom(type)
				|| Map.class.isAssignableFrom(type);
	}

	public static boolean isPseudoPrimitive(final Class<?> clz) {
		return clzTypeKeyMap.containsKey(clz);
	}

	private Converter() {
	}
}
