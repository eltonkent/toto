/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.websocket;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import toto.text.StringUtils;

/**
 * Base class for supporting different protocol and framing types on a websocket
 * 
 */
public class WireProtocol {
	protected static final Random random = new Random();
	protected static final Pattern VERIFY_STATUSLINE_PATTERN = Pattern
			.compile("^HTTP\\/[^ ]+ 101 ");

	protected WireProtocol() {
	}

	/**
	 * Generate a sequence of random chars
	 *
	 * @param charCount  Number of misc ascii chars
	 * @param numCount   Number of numeric chars
	 * @param spaceCount Number of spaces
	 * @param armor      true to armor the result (add a printable ascii char to front
	 *                   and back)
	 * @return the result
	 */
	public static String generateKey() {
		final int divisor = random.nextInt(8) + 1;
		final int number = (random.nextInt(1000000) + 1000) * divisor; // Make
		// sure
		// it
		// evenly
		// divides

		final String numberStr = String.valueOf(number);

		final StringBuilder ret = new StringBuilder(32);
		ret.append(numberStr);
		for (int i = 0; i < divisor; i++) {
			// Insert one space per divisor
			final int index = random.nextInt(ret.length());
			ret.insert(index, ' ');
		}

		for (int i = 0; i < numberStr.length() / 2; i++) {
			// Strew some random characters around
			final int index = random.nextInt(ret.length());
			ret.insert(index, (char) (random.nextInt(122 - 65) + 65));
		}

		// Finally, add a regular ascii printable at front and back
		ret.insert(0, (char) (random.nextInt(122 - 65) + 65));
		ret.append((char) (random.nextInt(122 - 65) + 65));

		return ret.toString();
	}

	public Message readMessage(final WebSocket socket,
			final DataInputStream input) throws Exception {
		throw new UnsupportedOperationException();
	}

	public boolean sendMessage(final WebSocket socket,
			final DataOutputStream output, final Message message)
			throws Exception {
		throw new UnsupportedOperationException();
	}

	public void initiateClose(final WebSocket socket) {
		socket.abort();
	}

	public void performHandshake(final WebSocket socket, final URI uri,
			final DataInputStream in, final DataOutputStream out)
			throws Exception {
		final String key1 = generateKey();
		final String key2 = generateKey();
		final byte[] quad = new byte[8];
		random.nextBytes(quad);

		final Map<String, String> headerMap = socket.getRequestHeaders();
		headerMap.put("Connection", "Upgrade");
		headerMap.put("Upgrade", "AndroidWebSocket");
		headerMap.put("Sec-AndroidWebSocket-Key1", key1);
		headerMap.put("Sec-AndroidWebSocket-Key2", key2);
		final String[] requestedProtocols = socket.getRequestedProtocols();
		if (requestedProtocols != null && requestedProtocols.length > 0) {
			final StringBuilder joinedProtocol = new StringBuilder();
			for (final String protocol : requestedProtocols) {
				if (joinedProtocol.length() > 0)
					joinedProtocol.append(' ');
				joinedProtocol.append(protocol);
			}
			headerMap.put("Sec-AndroidWebSocket-Protocol",
					joinedProtocol.toString());
		}

		// Build the request
		String path = uri.getRawPath();
		if (path.length() == 0)
			path = "/"; // Deal with malformed root
		if (uri.getRawQuery() != null) {
			path += '?' + uri.getRawQuery();
		}

		final StringBuilder request = new StringBuilder(1500);
		request.append("GET ").append(path).append(" HTTP/1.1\r\n");
		for (final Map.Entry<String, String> entry : headerMap.entrySet()) {
			request.append(entry.getKey());
			request.append(": ");
			request.append(entry.getValue());
			request.append("\r\n");
		}
		request.append("\r\n");

		// System.out.println("Sending request \n'" + request + "'");
		out.write(StringUtils.getUTF8Bytes(request));
		// out.flush(); // Give proxys a better chance of dealing with what
		// follows
		out.write(quad);
		out.flush();

		// Read the HTTP status line
		final String statusLine = readLine(in);
		if (!VERIFY_STATUSLINE_PATTERN.matcher(statusLine).find())
			throw new IOException("Bad status line from server: " + statusLine);

		// Read each header line until we get an empty
		final Map<String, String> responseHeaders = new HashMap<String, String>();
		for (;;) {
			final String headerLine = readLine(in);
			if (headerLine.length() == 0)
				break; // End of headers
			final int colonPos = headerLine.indexOf(": ");
			if (colonPos < 0) {
				throw new IOException("Illegal HTTP header in response");
			}

			final String name = headerLine.substring(0, colonPos), value = headerLine
					.substring(colonPos + 2);
			responseHeaders.put(name.toLowerCase(), value);
		}

		socket.setResponseHeaders(responseHeaders);

		// Now read the handshake from the input and verify
		final byte[] serverHandshake = new byte[16];
		in.readFully(serverHandshake);
		if (socket.isVerifyHandshake())
			validateHandshake(key1, key2, quad, serverHandshake);

		// And finally ready to go
		socket.setReadyState(WebSocket.OPEN);
	}

	/**
	 * Validate the handshake per the spec. Really you can't make this stuff up.
	 * Implementation of the most obscure security measures to guard access to a
	 * public park follows.
	 *
	 * @param key1
	 * @param key2
	 * @param randomBody
	 * @param serverHandshake
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	protected void validateHandshake(final String key1, final String key2,
			final byte[] clientQuad, final byte[] serverHandshake)
			throws IOException, NoSuchAlgorithmException {
		final StringBuilder key1Numbers = new StringBuilder(key1.length());
		final StringBuilder key2Numbers = new StringBuilder(key2.length());
		int key1Spaces = 0, key2Spaces = 0;

		// Scan key1 for numbers and spaces
		for (int i = 0; i < key1.length(); i++) {
			final char c = key1.charAt(i);
			if (c == ' ')
				key1Spaces++;
			else if (c >= '0' && c <= '9')
				key1Numbers.append(c);
		}

		// Scan key2 for numbers and spaces
		for (int i = 0; i < key2.length(); i++) {
			final char c = key2.charAt(i);
			if (c == ' ')
				key2Spaces++;
			else if (c >= '0' && c <= '9')
				key2Numbers.append(c);
		}

		final int key1Int = Integer.parseInt(key1Numbers.toString())
				/ key1Spaces;
		final int key2Int = Integer.parseInt(key2Numbers.toString())
				/ key2Spaces;

		final ByteArrayOutputStream bufferOut = new ByteArrayOutputStream(16);
		final DataOutputStream bufferData = new DataOutputStream(bufferOut);
		bufferData.writeInt(key1Int);
		bufferData.writeInt(key2Int);
		bufferData.write(clientQuad);

		final byte[] buffer = bufferOut.toByteArray();
		final MessageDigest digest = MessageDigest.getInstance("MD5");
		final byte[] clientHandshake = digest.digest(buffer);

		if (!Arrays.equals(clientHandshake, serverHandshake)) {
			throw new IOException(
					"Client and server handshake don't match:\nraw="
							+ Arrays.toString(buffer) + "\nclient="
							+ Arrays.toString(clientHandshake) + "\nserver="
							+ Arrays.toString(serverHandshake));
		} else {
			// System.out.println("Handshakes match");
		}
	}

	/**
	 * Reads a line of bytes terminated by CR-LF and returns it without the line
	 * ending decoded as UTF-8. It will actually accept just a naked LF and
	 * ignore the preceeding CR if present.
	 * <p>
	 * This method presumes that the stream supports mark/reset semantics.
	 * 
	 * @param in
	 * @return String
	 * @throws IOException
	 */
	protected String readLine(final InputStream in) throws IOException {
		final StringBuilder ret = new StringBuilder(256);

		boolean verifyLf = false;
		final CharsetDecoder decoder = StringUtils.UTF8.newDecoder();
		final byte[] source = new byte[256];
		final char[] dest = new char[256];
		final ByteBuffer sourceBuffer = ByteBuffer.wrap(source);
		final CharBuffer destBuffer = CharBuffer.wrap(dest);
		for (;;) {
			in.mark(source.length);
			final int r = in.read(source);
			if (r < 0) {
				throw new IOException("Short line reading response.  Got: '"
						+ new String(source, "ISO-8859-1") + "'");
			}
			sourceBuffer.clear();
			sourceBuffer.limit(r);

			// Scan for a line ending
			int lineEndIndex;
			for (lineEndIndex = 0; lineEndIndex < r; lineEndIndex++) {
				final byte b = source[lineEndIndex];
				if (b == '\n') {
					// Naked newline terminated (no CR)
					break;
				} else if (b == '\r') {
					// Treat this as an end of line but setKey a flag saying we
					// should read the next
					// byte and verify LF
					verifyLf = true;
					break;
				}
			}

			if (lineEndIndex == r) {
				// We rolled off the buffer without finding a line ending
				// Consume all of it
				sourceBuffer.flip();
				destBuffer.clear();
				if (decoder.decode(sourceBuffer, destBuffer, false).isError()) {
					throw new IOException("Error decoding");
				}
				ret.append(dest, 0, destBuffer.position());
				continue;
			} else {
				// The line ending was in the buffer
				sourceBuffer.position(lineEndIndex);
				sourceBuffer.flip();
				destBuffer.clear();
				if (decoder.decode(sourceBuffer, destBuffer, true).isError()) {
					throw new IOException("Error decoding");
				}
				ret.append(dest, 0, destBuffer.position());

				// Reset the input stream and then fast forward
				in.reset();
				in.skip(lineEndIndex + 1);
				break;
			}
		}

		// Check for dangling LF
		if (verifyLf) {
			final int lf = in.read();
			if (lf != '\n') {
				throw new IOException("Malformed line.  Unmatched CR.");
			}
		}

		// System.out.println("READLINE: " + ret);
		return ret.toString();
	}

}
