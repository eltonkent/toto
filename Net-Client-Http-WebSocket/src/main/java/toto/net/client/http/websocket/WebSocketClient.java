/*******************************************************************************
 * Mobifluence Interactive
 * mobifluence.com (c) 2013
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Mobifluence Interactive.
 * <p/>
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.websocket;

import android.os.Handler;

/**
 * Android specific AndroidWebSocket client that interfaces with the Looper to
 * provide threading control.
 */
public class WebSocketClient extends WebSocket {
	private final Handler handler;

	/**
	 * Instantiate the class so that events are directed back at the Looper
	 * attached to this thread. In typical parlance, this means that you must
	 * instantiate the instance on the main thread. A task will be posted to the
	 * current thread to start the instance.
	 *
	 * @param url
	 * @param requestedProtocols
	 */
	public WebSocketClient(final String url,
						   final String... requestedProtocols) {
		this(new Handler(), url, requestedProtocols);
		handler.post(new Runnable() {
			@Override
			public void run() {
				start();
			}
		});
	}

	/**
	 * Instantiate the instance targeting an explicit handler. This variant is
	 * typically not used. This constructor does not automatically post a
	 * message to the handler to start the instance. The caller must arrange to
	 * start it via another means.
	 *
	 * @param url
	 * @param requestedProtocols
	 */
	public WebSocketClient(final Handler handler, final String url,
						   final String... requestedProtocols) {
		super(url, requestedProtocols);
		this.handler = new Handler();
	}

	@Override
	protected void dispatchEvent(final Event event, final EventListener l) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				l.handleEvent(event);
			}
		});
	}
}
