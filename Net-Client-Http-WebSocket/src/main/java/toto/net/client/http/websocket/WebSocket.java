/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.websocket;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.net.SocketFactory;

/**
 * Provide an implementation of the HTML5 AndroidWebSocket class:
 * http://dev.w3.org/html5/websockets/
 * <p>
 * Where feasible, we use similar conventions/names here as in the HTML5 spec
 * for familiarity. This class manages a worker thread which actually performs
 * all of the connection management. For simplicity, the several events are
 * collapsed down to a single event handler registration and distinguished with
 * an event code.
 * <p>
 * This class implements thewebsocketprotocol-03
 * http://tools.ietf.org/html/draft-ietf-hybi-thewebsocketprotocol-03
 * 
 * 
 */
public class WebSocket {
	// -- readyState constants
	public static final int CONNECTING = 0;
	public static final int OPEN = 1;
	public static final int CLOSING = 2;
	public static final int CLOSED = 3;
	/**
	 * Combines the open and close events and just exposes one readyState change
	 * event
	 */
	public static final int EVENT_READYSTATE = 0;
	/**
	 * A message has been received. The message field will be setKey.
	 */
	public static final int EVENT_MESSAGE = 1;
	/**
	 * An error occurred. If this was due to an exception, the event error field
	 * will be setKey. In general, errors cause the io thread to commence
	 * shutdown.
	 */
	public static final int EVENT_ERROR = 2;
	private static final Pattern INVALID_HEADER_NAME_PATTERN = Pattern
			.compile("[\\r\\n\\:]");
	private static final Pattern INVALID_HEADER_VALUE_PATTERN = Pattern
			.compile("[\\r\\n]");
	private final String url;
	private final String[] requestedProtocols;
	private final MessageQueue transmissionQueue = new MessageQueue();
	// -- public properties (read-only)
	private int readyState;
	private Map<String, String> requestHeaders = new HashMap<String, String>();
	private Map<String, String> responseHeaders;
	private boolean verifyHandshake = true;

	// -- public properties (read-write)
	private NetConfig netConfig = new NetConfig();
	private WireProtocol wireProtocol = WireProtocolDraft76.INSTANCE;
	// -- package private (to protocol implementations)
	private byte[] closeCookie;
	// -- internal implementation
	private boolean started;
	private Thread readerThread, writerThread;
	private List<EventListener> listeners;
	private boolean dispatchingEvent;
	// -- IO Management. Everything from here on runs under either the reader or
	// writer thread
	private URI uri;
	private String hostName;
	private int port;
	private SocketFactory socketFactory;
	private Socket socket;
	private DataInputStream in;
	private DataOutputStream out;

	// -- public constructors
	public WebSocket(final String url, final String... requestedProtocols) {
		this.url = url;
		this.requestedProtocols = requestedProtocols.clone();
	}

	public synchronized int getReadyState() {
		return readyState;
	}

	// -- public api

	protected synchronized void setReadyState(final int readyState) {
		boolean notifyListeners;
		synchronized (this) {
			if (readyState != this.readyState) {
				this.readyState = readyState;
				notifyListeners = listeners != null;
				this.notifyAll();
			} else {
				notifyListeners = false;
			}
		}

		if (notifyListeners) {
			final Event event = new Event();
			event.source = this;
			event.type = EVENT_READYSTATE;
			event.readyState = readyState;
			signalEvent(event);
		}
	}

	public synchronized String getProtocol() {
		if (responseHeaders == null)
			return null;
		return responseHeaders.get("sec-websocket-protocol");
	}

	public synchronized String[] getResponseHeaderNames() {
		if (responseHeaders == null)
			return null;
		return responseHeaders.keySet().toArray(
				new String[responseHeaders.size()]);
	}

	public synchronized String getResponseHeader(final String name) {
		if (responseHeaders == null)
			return null;
		return responseHeaders.get(name.toLowerCase());
	}

	public String getUrl() {
		return url;
	}

	public NetConfig getNetConfig() {
		return netConfig;
	}

	public void setNetConfig(final NetConfig netConfig) {
		if (started)
			throw new IllegalStateException();
		this.netConfig = netConfig;
	}

	public WireProtocol getWireProtocol() {
		return wireProtocol;
	}

	public void setWireProtocol(final WireProtocol wireProtocol) {
		if (started)
			throw new IllegalStateException();
		this.wireProtocol = wireProtocol;
	}

	public boolean isVerifyHandshake() {
		return verifyHandshake;
	}

	/**
	 * Use to disable handshake verification. Handshake is still generated per
	 * spec but not verified
	 *
	 * @param verifyHandshake
	 */
	public void setVerifyHandshake(final boolean verifyHandshake) {
		if (started)
			throw new IllegalStateException();
		this.verifyHandshake = verifyHandshake;
	}

	public void addRequestHeader(final String name, final String value) {
		if (started)
			throw new IllegalStateException();
		if (INVALID_HEADER_NAME_PATTERN.matcher(name).find()
				|| INVALID_HEADER_VALUE_PATTERN.matcher(value).find())
			throw new IllegalArgumentException();
		if (requestHeaders == null)
			requestHeaders = new HashMap<String, String>();
		requestHeaders.put(name, value);
	}

	/**
	 * Queues a message for sending (puts the message at the tail of the queue)
	 */
	public void send(final Message message) {
		transmissionQueue.addTail(message);
	}

	/**
	 * Queues a message for immediate transmission (puts it at the head of the
	 * queue).
	 *
	 * @param message
	 */
	public void sendImmediate(final Message message) {
		transmissionQueue.addHead(message);
	}

	public void send(final CharSequence message) {
		send(new Message(message));
	}

	/**
	 * @return the number of messages on the transmission queue
	 */
	public int getOutgoingDepth() {
		return transmissionQueue.getDepth();
	}

	/**
	 * @return the approximate number of bytes queued for transmission
	 *         (excluding framing)
	 */
	public long getOutgoingAmount() {
		return transmissionQueue.getBytes();
	}

	public void close() {
		synchronized (this) {
			if (readyState != OPEN) {
				abort();
				return;
			}
		}
		wireProtocol.initiateClose(this);
	}

	/**
	 * Immediately abort the connection.
	 */
	public void abort() {
		if (socket != null) {
			try {
				socket.close();
				// This will stop anything blocked on read or write
			} catch (final IOException e) {
				// Not much else to do
				e.printStackTrace();
			}
			socket = null;
		}

		Thread localReaderThread = null, localWriterThread = null;
		synchronized (this) {
			if (readerThread != null && readerThread.isAlive()) {
				readerThread.interrupt();
				localReaderThread = readerThread;
			}
			if (writerThread != null && writerThread.isAlive()) {
				writerThread.interrupt();
				localWriterThread = writerThread;
			}
			readerThread = null;
			writerThread = null;
		}

		if (localReaderThread != null) {
			try {
				localReaderThread.join();
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

		if (localWriterThread != null) {
			try {
				localWriterThread.join();
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

		setReadyState(CLOSED);
	}

	public void waitForReadyState(final int targetReadyState)
			throws InterruptedException {
		synchronized (this) {
			while (readyState != targetReadyState) {
				this.wait();
			}
		}
	}

	public synchronized void addListener(final EventListener l) {
		if (dispatchingEvent && listeners != null) {
			// Make a defensive copy
			listeners = new ArrayList<EventListener>(listeners);
		}

		if (listeners == null)
			listeners = new ArrayList<EventListener>(2);
		listeners.add(l);
	}

	public synchronized void removeListener(final EventListener l) {
		if (listeners != null) {
			if (dispatchingEvent)
				listeners = new ArrayList<EventListener>(listeners);
			listeners.remove(l);
		}
	}

	public synchronized void removeAllListeners() {
		listeners = null;
	}

	/**
	 * The HTML5 implementation presumably starts after the current tick. This
	 * base class has no threading ideas so it has an explicit start() method.
	 * If already started does nothing. Otherwise, starts the processing thread.
	 * Environment specific subclasses will typically integrate with the native
	 * message loop to handle this detail for you.
	 */
	public void start() {
		if (started)
			return;
		started = true;
		readerThread = new Thread("AndroidWebSocket read " + url) {
			@Override
			public void run() {
				runReader();
			}
		};
		readerThread.start();
	}

	private void startWriter() {
		writerThread = new Thread("AndroidWebSocket write " + url) {
			@Override
			public void run() {
				runWriter();
			}
		};
		writerThread.start();
	}

	protected Map<String, String> getRequestHeaders() {
		return requestHeaders;
	}

	protected String[] getRequestedProtocols() {
		return requestedProtocols;
	}

	protected synchronized void setResponseHeaders(
			final Map<String, String> responseHeaders) {
		this.responseHeaders = responseHeaders;
	}

	protected MessageQueue getTransmissionQueue() {
		return transmissionQueue;
	}

	public synchronized byte[] getCloseCookie() {
		return closeCookie;
	}

	public synchronized void setCloseCookie(final byte[] closeCookie) {
		this.closeCookie = closeCookie;
	}

	protected final void signalEvent(final Event event) {
		List<EventListener> listenersCopy;
		synchronized (this) {
			dispatchingEvent = true;
			listenersCopy = listeners;
		}

		try {
			if (listenersCopy != null) {
				for (int i = 0; i < listenersCopy.size(); i++) {
					dispatchEvent(event, listenersCopy.get(i));
				}
			}
		} finally {
			synchronized (this) {
				dispatchingEvent = false;
			}
		}
	}

	protected void signalError(final Throwable t) {
		// t.printStackTrace();
		final Event event = new Event();
		event.source = this;
		event.type = EVENT_ERROR;
		event.readyState = readyState;
		event.error = t;
		signalEvent(event);
	}

	protected void signalMessage(final Message msg) {
		final Event event = new Event();
		event.source = this;
		event.readyState = readyState;
		event.type = EVENT_MESSAGE;
		event.message = msg;
		signalEvent(event);
	}

	/**
	 * Dispatch an event to the given listener. Subclasses can do custom thread
	 * marshalling by overriding this method
	 *
	 * @param event
	 * @param l
	 */
	protected void dispatchEvent(final Event event, final EventListener l) {
		l.handleEvent(event);
	}

	private void setupConnection() throws Throwable {
		uri = new URI(url);

		// Detect protocol, host, port
		String hostHeader;
		final String scheme = uri.getScheme();
		port = uri.getPort();
		hostName = uri.getHost();
		if ("ws".equalsIgnoreCase(scheme) || "http".equals(scheme)) {
			// Default to http
			if (port < 0)
				port = 80;
			if (port != 80)
				hostHeader = hostName + ':' + port;
			else
				hostHeader = hostName;
			socketFactory = netConfig.getPlainSocketFactory();
		} else if ("wss".equalsIgnoreCase(scheme)
				|| "https".equalsIgnoreCase(scheme)) {
			// Secure
			if (port < 0)
				port = 443;
			if (port != 443)
				hostHeader = hostName + ':' + port;
			else
				hostHeader = hostName;
			socketFactory = netConfig.getSecureSocketFactory();
		} else {
			throw new IllegalArgumentException("Unsupported websocket protocol");
		}

		// Add the host header
		requestHeaders.put("Host", hostHeader);

		// Connect the socket
		socket = socketFactory.createSocket(hostName, port);
		try {
			// Buffer the streams to a typical network packet size
			in = new DataInputStream(new BufferedInputStream(
					socket.getInputStream(), 1500));
			out = new DataOutputStream(new BufferedOutputStream(
					socket.getOutputStream(), 1500));
		} catch (final Throwable t) {
			socket.close();
			throw t;
		}
	}

	private void pumpSocketInput() throws Exception {
		for (;;) {
			final Message message = wireProtocol.readMessage(this, in);
			if (message == null)
				break;
			signalMessage(message);
		}
	}

	private void runReader() {
		setReadyState(CONNECTING);

		// Before starting the main loop, we need to resolve the URI
		try {
			setupConnection();
			wireProtocol.performHandshake(this, uri, in, out);
			startWriter();
			pumpSocketInput();
			abort();
		} catch (final Throwable t) {
			exceptionalShutdown(t);
			return;
		}
	}

	private void runWriter() {
		// System.out.println("Writer starting");
		for (;;) {
			Message next;
			try {
				next = transmissionQueue.waitNext();
			} catch (final InterruptedException e) {
				// Shutdown
				break;
			}

			try {
				final boolean shouldContinue = wireProtocol.sendMessage(this,
						out, next);
				transmissionQueue.remove(next);
				if (!shouldContinue)
					break;
			} catch (final Throwable t) {
				// Replace the message
				exceptionalShutdown(t);
				break;
			}
		}
		// System.out.println("Writer exiting");
	}

	/**
	 * Called on exception. Fires events and shuts everything down.
	 */
	private void exceptionalShutdown(final Throwable t) {
		signalError(t);
		abort();
	}

	public static interface EventListener {
		public void handleEvent(Event event);
	}

	public static class Event {
		/**
		 * An EVENT_* constant
		 */
		protected int type;

		/**
		 * The source AndroidWebSocket
		 */
		protected WebSocket source;

		/**
		 * Snapshot of the readyState at the time of the event
		 */
		protected int readyState = -1;

		/**
		 * On MESSAGE event types this is the message
		 */
		protected Message message;

		/**
		 * On error event types, this is the error
		 */
		protected Throwable error;

		public int getType() {
			return type;
		}

		public WebSocket getSource() {
			return source;
		}

		public int getReadyState() {
			return readyState;
		}

		public Message getMessage() {
			return message;
		}

		public Throwable getError() {
			return error;
		}

		@Override
		public String toString() {
			final StringWriter ret = new StringWriter();
			String typeName;
			if (type == EVENT_READYSTATE)
				typeName = "ReadyState";
			else if (type == EVENT_MESSAGE)
				typeName = "Message";
			else if (type == EVENT_ERROR)
				typeName = "Error";
			else
				typeName = String.valueOf(type);

			ret.write("<Event ");
			ret.append(typeName);
			ret.append(", readyState=");
			ret.append(String.valueOf(readyState));
			ret.append(">\n");
			if (type == EVENT_ERROR && error != null) {
				final PrintWriter pout = new PrintWriter(ret);
				error.printStackTrace(pout);
				pout.flush();
			} else if (type == EVENT_MESSAGE) {
				ret.append(message.toString());
			}
			ret.append("</Event>");
			return ret.toString();
		}
	}
}
