/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.websocket;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

/**
 * Class holding various network configurations
 * 
 * @author stella
 * 
 */
public class NetConfig {
	private SocketFactory plainSocketFactory;
	private SocketFactory secureSocketFactory;

	/**
	 * @return a value previously specified or system default
	 */
	public SocketFactory getPlainSocketFactory() {
		if (plainSocketFactory == null)
			return SocketFactory.getDefault();
		return plainSocketFactory;
	}

	public void setPlainSocketFactory(final SocketFactory plainSocketFactory) {
		this.plainSocketFactory = plainSocketFactory;
	}

	/**
	 * @return a value previously specified or system default
	 */
	public SocketFactory getSecureSocketFactory() {
		if (secureSocketFactory == null)
			return SSLSocketFactory.getDefault();
		return secureSocketFactory;
	}

	public void setSecureSocketFactory(final SocketFactory secureSocketFactory) {
		this.secureSocketFactory = secureSocketFactory;
	}
}
