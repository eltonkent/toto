/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.app;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

import com.mi.toto.ToTo;
import android.app.ActivityManager;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

/**
 * 
 * @author Mobifluence Interactive
 * 
 */
public final class ApplicationUtils {

	/**
	 * Get the Dex
	 * 
	 * @param context
	 * @return
	 */
	public File getDexFileDirectory(final Context context) {
		return context.getDir("dex", 0);
	}

	/**
	 * Utility method to test if a package is installed or not.
	 * 
	 * @param context
	 * @param packagName
	 * @return
	 */
	public static boolean isPackageInstalled(final Context context,
			final String packagName) {
		boolean rv = true;

		try {
			final PackageManager pm = context.getPackageManager();
			pm.getPackageInfo(packagName, 0);
		} catch (final NameNotFoundException e) {
			rv = false;
			ToTo.logException(e);
		}

		return rv;
	}

	public static int getAPINumber() {
		if (isAPILevelLower4())
			return 3;
		int version = 3;
		try {
			final Class buildClass = Build.VERSION.class;
			final Field sdkint = buildClass.getField("SDK_INT");
			version = sdkint.getInt(null);
		} catch (final Exception ignore) {
			ToTo.logException(ignore);
		}
		return version;
	}

	/**
	 * Returns true when platform version is lower or equal to 1.5 Since prior
	 * to 1.5 there was no Build.VERSION.SDK_INT available.
	 * 
	 * @return
	 */
	public static boolean isAPILevelLower4() {
		return "1.5".compareTo(Build.VERSION.RELEASE) >= 0;
	}

	/**
	 * Check if the application with the given package name is running.
	 * <p>
	 * Eg: To check if the browser is running.<br/>
	 * <code>
	 * isApplicationRunning(getApplicationContext(),"com.android.browser");
	 * </code>
	 * </p>
	 * 
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static boolean isApplicationRunning(final Context context,
			final String packageName) {
		final ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		final List<ActivityManager.RunningAppProcessInfo> procInfos = manager
				.getRunningAppProcesses();
		for (int i = 0; i < procInfos.size(); i++) {
			if (procInfos.get(i).processName.equals(packageName)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Enable or disable an android component using the given class name
	 * 
	 * @param context
	 * @param componentClass
	 * @param enable
	 */
	public static void setComponentEnabled(final Context context,
			final Class<?> componentClass, final boolean enable) {
		if (enable)
			;
		for (int i = 1;; i = 2) {
			final ComponentName localComponentName = new ComponentName(context,
					componentClass);
			context.getPackageManager().setComponentEnabledSetting(
					localComponentName, i, 1);
			return;
		}
	}

	/**
	 * Get the current wallpaper as a bitmap based on the width and height
	 * currently setKey.
	 * 
	 * @param paramContext
	 * @return
	 */
	public static Bitmap getWallPaperBitmap(final Context paramContext) {
		final WallpaperManager localWallpaperManager = WallpaperManager
				.getInstance(paramContext);
		final Drawable localDrawable = localWallpaperManager.getDrawable();
		if (localDrawable == null)
			return null;

		if ((localDrawable instanceof BitmapDrawable))
			return ((BitmapDrawable) localDrawable).getBitmap();
		final int i = localDrawable.getIntrinsicWidth();
		final int j = localDrawable.getIntrinsicHeight();
		final Bitmap localBitmap = Bitmap.createBitmap(i, j,
				Bitmap.Config.ARGB_8888);
		final Canvas localCanvas = new Canvas(localBitmap);
		localDrawable.setBounds(0, 0, i, j);
		localDrawable.draw(localCanvas);
		return localBitmap;
	}

	public static void screenCaptureToFile(final File file) {

	}

}
