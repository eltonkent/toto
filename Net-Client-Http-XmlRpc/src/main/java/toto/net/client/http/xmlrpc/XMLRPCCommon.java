/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.xmlrpc;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

class XMLRPCCommon {

	protected IXMLRPCSerializer iXMLRPCSerializer;
	protected XmlSerializer serializer;

	XMLRPCCommon() {
		serializer = Xml.newSerializer();
		iXMLRPCSerializer = new XMLRPCSerializer();
	}

	protected void serializeParams(final Object[] params)
			throws IllegalArgumentException, IllegalStateException, IOException {
		if (params != null && params.length != 0) {
			// setKey method params
			serializer.startTag(null, Tag.PARAMS);
			for (int i = 0; i < params.length; i++) {
				serializer.startTag(null, Tag.PARAM).startTag(null,
						IXMLRPCSerializer.TAG_VALUE);
				iXMLRPCSerializer.serialize(serializer, params[i]);
				serializer.endTag(null, IXMLRPCSerializer.TAG_VALUE).endTag(
						null, Tag.PARAM);
			}
			serializer.endTag(null, Tag.PARAMS);
		}
	}

	/**
	 * Sets custom IXMLRPCSerializer serializer (in case when server doesn't
	 * support standard XMLRPC protocol)
	 * 
	 * @param serializer
	 *            custom serializer
	 */
	public void setSerializer(final IXMLRPCSerializer serializer) {
		iXMLRPCSerializer = serializer;
	}

}
