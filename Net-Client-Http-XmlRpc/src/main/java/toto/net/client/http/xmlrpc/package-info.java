/**
 * XML-RPC client with robust implementations for serialization and method calling.
 *
 */
package toto.net.client.http.xmlrpc;