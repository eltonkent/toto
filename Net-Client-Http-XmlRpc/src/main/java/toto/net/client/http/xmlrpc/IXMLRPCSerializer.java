/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.xmlrpc;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

public interface IXMLRPCSerializer {
	String DATETIME_FORMAT = "yyyyMMdd'T'HH:mm:ss";
	String TAG_DATA = "data";
	String TAG_MEMBER = "member";
	String TAG_NAME = "name";

	String TAG_VALUE = "value";
	String TYPE_ARRAY = "array";
	String TYPE_BASE64 = "base64";
	String TYPE_BOOLEAN = "boolean";
	String TYPE_DATE_TIME_ISO8601 = "dateTime.iso8601";
	String TYPE_DOUBLE = "double";
	String TYPE_I4 = "i4";
	String TYPE_I8 = "i8";
	String TYPE_INT = "int";
	// This added by mattias.ellback as part of issue #19
	String TYPE_NULL = "nil";
	String TYPE_STRING = "string";

	String TYPE_STRUCT = "struct";

	Object deserialize(XmlPullParser parser) throws XmlPullParserException,
			IOException;

	void serialize(XmlSerializer serializer, Object object) throws IOException;
}
