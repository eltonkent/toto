/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.xmlrpc;

/**
 * Allows to pass any XMLRPCSerializable object as input parameter. When
 * implementing getSerializable() you should return one of XMLRPC primitive
 * types (or another XMLRPCSerializable: be careful not going into recursion by
 * passing this object reference!)
 */
public interface XMLRPCSerializable {

	/**
	 * Gets XMLRPC serialization object
	 * 
	 * @return object to serialize This object is most likely one of XMLRPC
	 *         primitive types, however you can return also another
	 *         XMLRPCSerializable
	 */
	Object getSerializable();
}
