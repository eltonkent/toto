/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.xmlrpc;

import java.util.ArrayList;

/**
 * XML-RPC method call
 * 
 * @author Elton Kent
 */
public class MethodCall {

	private static final int TOPIC = 1;
	String methodName;
	ArrayList<Object> params = new ArrayList<Object>();

	public String getMethodName() {
		return methodName;
	}

	void setMethodName(final String methodName) {
		this.methodName = methodName;
	}

	public ArrayList<Object> getParams() {
		return params;
	}

	void setParams(final ArrayList<Object> params) {
		this.params = params;
	}

	public String getTopic() {
		return (String) params.get(TOPIC);
	}
}
