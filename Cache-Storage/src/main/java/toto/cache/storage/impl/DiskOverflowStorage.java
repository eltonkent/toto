// NEED TO REVIST THIS DESIGN
// package toto.cache.storage.impl;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;
//
//
//import toto.cache.SerializedStorageConverter;
//import toto.cache.Storage;
//import toto.cache.storage.impl.HeapStorage.CacheEntry;
//import toto.io.file.FileUtils;
//
///**
// * Stores entries on the heap until the specified capacity and then starts
// * storing the oldest entries onto the disk.
// * <p>
// * <div> <b>Note:</b>Like the {@link HeapStorage}, all entries saved using
// * {@link DiskOverflowStorage} are lost once the cache is closed or the
// * application is terminated. </div>
// * </p>
// *
// * @author Mobifluence Interactive
// *
// * @param <K>
// *            type for the key
// * @param <V>
// *            type for the value
// */
//public final class DiskOverflowStorage<K, V> extends DiskStorage<K, V> {
//
//	private int onHeapCount = 25;
//	private int offloadCount = 5;
//
//	private final ConcurrentHashMap<K, CacheEntry<V>> cache;
//
//	public DiskOverflowStorage(
//			final SerializedStorageConverter<K> keyConverter,
//			final SerializedStorageConverter<V> valueConverter,
//			final File cacheDirectory) {
//		super(keyConverter, valueConverter, cacheDirectory);
//		try {
//			FileUtils.emptyDirectory(cacheDirectory);
//		} catch (final IOException e) {
//			e.printStackTrace();
//		}
//		cache = new ConcurrentHashMap<K, CacheEntry<V>>(onHeapCount);
//	}
//
//	@Override
//	protected boolean remove(final K key) {
//		final CacheEntry<V> v = cache.remove(key);
//		boolean removed;
//		if (v != null) {
//			removed = true;
//		} else {
//			final byte[] keyBytes = keyConverter.toBytes(key);
//			removed = super.delete(keyBytes);
//		}
//		if (removed && getCacheListener() != null) {
//			getCacheListener().onEntryRemoved(key);
//		}
//		return removed;
//	}
//
//	@Override
//	protected boolean put(final K key, final V value) {
//		final boolean added = true;
//		cache.put(key, new CacheEntry<V>(value, System.currentTimeMillis()));
//		if (cache.size() > onHeapCount) {
//			cacheOldestToDisk();
//		}
//		if (added && getCacheListener() != null) {
//			getCacheListener().onEntryAdded(key, value);
//		}
//		return added;
//	}
//
//	private void cacheOldestToDisk() {
//		for (int i = 0; i < offloadCount; i++) {
//			final K key = getOldestOnDisk();
//			if (key != null) {
//				final CacheEntry<V> value = cache.remove(key);
//				super.save(keyConverter.toBytes(key),
//						valueConverter.toBytes(value.value));
//			}
//		}
//	}
//
//	private K getOldestOnDisk() {
//		long oldestTime = Long.MAX_VALUE;
//		K oldestKey = null;
//		final Set<K> set = cache.keySet();
//		CacheEntry<V> cacheEntry;
//		for (final K entry : set) {
//			cacheEntry = cache.get(entry);
//			if (cacheEntry.timeAdded < oldestTime) {
//				oldestTime = cacheEntry.timeAdded;
//				oldestKey = entry;
//			}
//		}
//		return oldestKey;
//	}
//
//	protected V get(final K key) {
//		final CacheEntry<V> entry = cache.get(key);
//		if (entry == null) {
//			return findEntryOnDisk(key);
//		} else {
//			entry.timesAccessed = entry.timesAccessed++;
//			entry.lastAccessed = System.currentTimeMillis();
//			return entry.value;
//		}
//	}
//
//	private V findEntryOnDisk(final K key) {
//		final byte[] keyBytes = keyConverter.toBytes(key);
//		final byte[] value = fetch(keyBytes);
//		if (value != null) {
//			return valueConverter.toType(value);
//		}
//		return null;
//	}
//
//	/**
//	 * Get the max number of entries that can be kept on the heap.
//	 * <p>
//	 * After the heap max is reached, the oldest entry is overflowed to the disk
//	 * </p>
//	 *
//	 * @return
//	 */
//	public final int getOnHeapCount() {
//		return onHeapCount;
//	}
//
//	/**
//	 * Set Max number of items on the heap. default is 25
//	 *
//	 * @param onHeapCount
//	 */
//	public final void setOnHeapCount(final int onHeapCount) {
//		this.onHeapCount = onHeapCount;
//	}
//
//	@Override
//	public void close() {
//		cache.clear();
//		super.clear();
//
//	}
//
//	@Override
//	protected int size() {
//		return cache.size() + super.size();
//	}
//
//	/**
//	 * Get the number of entries that have overflowed to the disk
//	 *
//	 * @return
//	 */
//	public int getOverflowCount() {
//		return super.size();
//	}
//
//	/**
//	 * Get the number of entries that should be off-loaded to the disk as a
//	 * batch
//	 * <p>
//	 * When the value for {@link #setOnHeapCount(int)} is reached, a batch of
//	 * entries of this size are off-loaded to the disk.
//	 * </p>
//	 *
//	 * @return
//	 */
//	public final int getOffloadCount() {
//		return offloadCount;
//	}
//
//	/**
//	 * Get the number of entries that should be off-loaded to the disk as a
//	 * batch.
//	 * <p>
//	 * The default is 5
//	 * </p>
//	 *
//	 * @param offloadCount
//	 */
//	public final void setOffloadCount(final int offloadCount) {
//		this.offloadCount = offloadCount;
//	}
//
//	@Override
//	protected Storage<K, V>.EntryMetaInfo getEntryMetaInfo(final K key) {
//		if (cache.containsKey(key)) {
//			final CacheEntry<V> v = cache.get(key);
//			final EntryMetaInfo info = new EntryMetaInfo();
//			info.setTimesAccessed(v.timesAccessed);
//			info.setTimeAdded(v.timeAdded);
//			info.setLastAccessed(v.lastAccessed);
//			return info;
//		} else {
//			return super.getEntryMetaInfo(key);
//		}
//	}
//
//	@Override
//	protected Map<K, Storage<K, V>.EntryMetaInfo> getAllEntriesMetaInfo() {
//		final Map<K, Storage<K, V>.EntryMetaInfo> metaInfo = new HashMap<K, Storage<K, V>.EntryMetaInfo>();
//		final Set<Entry<K, CacheEntry<V>>> set = cache.entrySet();
//		EntryMetaInfo info;
//		CacheEntry<V> v;
//		for (final Entry<K, CacheEntry<V>> entry : set) {
//			info = new EntryMetaInfo();
//			v = entry.getValue();
//			info.setTimesAccessed(v.timesAccessed);
//			info.setTimeAdded(v.timeAdded);
//			info.setLastAccessed(v.lastAccessed);
//			metaInfo.put(entry.getKey(), info);
//		}
//		// put entries on the disk
//		metaInfo.putAll(super.getAllEntriesMetaInfo());
//		return metaInfo;
//	}
//
//}
