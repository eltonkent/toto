package toto.cache.storage.impl;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.mi.toto.ToTo;

import toto.cache.SerializedStorageConverter;
import toto.cache.Storage;
import toto.io.IOUtils;
import toto.io.file.FileUtils;

/**
 * {@link DiskStorage} that uses a journal file which is maintained on heap and
 * stored on disk to store information regarding entries in the cache. This
 * helps in fast eviction cycles.
 * 
 * <p>
 * Disadvantage : The Journal file could get corrupted if the application
 * terminates abnormally or the {@link toto.cache.TCache#close()} call is not made when the
 * cache is not needed anymore.
 * </p>
 * 
 * @author Mobifluence Interactive
 * 
 * @param <K>
 * @param <V>
 */
public final class JournaledDiskStorage<K, V> extends DiskStorage<K, V> {

	private final String journalFile = "TCache.journal";

	private final Map<String, EntryMetaInfo> entries = new HashMap<String, EntryMetaInfo>();

	public JournaledDiskStorage(final SerializedStorageConverter<K> keyConverter,
			final SerializedStorageConverter<V> valueConverter,
			final File cacheDirectory) {
		super(keyConverter, valueConverter, cacheDirectory);
		setupJournalFile();
	}

	@Override
	protected boolean save(final byte[] key, final byte[] value) {
		final String filename = getHashForKey(key);
		DataOutputStream dos = null;
		try {
			final FileOutputStream fos = new FileOutputStream(new File(
					cacheDirectory, filename));
			dos = new DataOutputStream(fos);
			writeMergedKeyValue(key, value, dos);
			dos.flush();
			dos.close();
			final EntryMetaInfo entry = new EntryMetaInfo();
			entry.setLastAccessed(System.currentTimeMillis());
			entry.setTimeAdded(System.currentTimeMillis());
			entry.setTimesAccessed(0);
			entry.setValueSizeInBytes(value.length);
			entries.put(filename, entry);
			return true;
		} catch (final FileNotFoundException e) {
			ToTo.logException(e);
		} catch (final IOException e) {
			ToTo.logException(e);
		} finally {
			IOUtils.closeQuietly(dos);
		}
		return false;
	}

	@Override
	protected boolean delete(final byte[] key) {
		final String filename = getHashForKey(key);
		final File cacheFile = new File(cacheDirectory, filename);
		final boolean deleted = cacheFile.delete();
		if (deleted) {
			entries.remove(filename);
			return true;
		}
		return false;
	}

	@Override
	protected byte[] fetch(final byte[] key) {
		final String filename = getHashForKey(key);
		final File cacheFile = new File(cacheDirectory, filename);
		byte[] value = null;
		if (!cacheFile.exists()) {
			return value;
		}
		RandomAccessFile file = null;
		try {
			file = new RandomAccessFile(cacheFile, "r");
			value = readValueFromMerge(file);
			final EntryMetaInfo meta = entries.get(filename);
			meta.setLastAccessed(System.currentTimeMillis());
			meta.setTimesAccessed(meta.getTimesAccessed() + 1);
		} catch (final FileNotFoundException e) {
			ToTo.logException(e);
		} catch (final IOException e) {
			ToTo.logException(e);
		} finally {
			IOUtils.closeQuietly(file);
		}
		return value;
	}

	@Override
	protected void clear() {
		entries.clear();
		try {
			FileUtils.emptyDirectory(cacheDirectory);
		} catch (final IOException e) {
			ToTo.logException(e);
		}
	}

	@Override
	protected void close() {
		super.close();
		saveJournalFile();
	}

	@Override
	protected int size() {
		return entries.size();
	}

	private void loadJournalFile(final File file) {
		DataInputStream dis = null;
		try {
			dis = new DataInputStream(new FileInputStream(file));
			final int numberOfEntries = dis.readInt();
			File cacheEntry;
			for (int i = 0; i < numberOfEntries; i++) {
				final String hash = dis.readUTF();
				final EntryMetaInfo meta = readMetaInfo(dis);
				cacheEntry = new File(cacheDirectory, hash);
				if (cacheEntry.exists() && meta != null && hash != null) {
					entries.put(hash, meta);
				}
			}
		} catch (final IOException e) {
			ToTo.logException(e);
			throw new IllegalStateException("Could not load journal file");
		} finally {
			IOUtils.closeQuietly(dis);
		}
	}

	private void saveJournalFile() {
		final File file = new File(cacheDirectory, journalFile);
		if (file.exists()) {
			file.delete();
		}
		DataOutputStream dos = null;
		try {
			final FileOutputStream fos = new FileOutputStream(file);
			dos = new DataOutputStream(fos);
			dos.writeInt(entries.size());
			final Set<Entry<String, EntryMetaInfo>> set = entries.entrySet();
			for (final Entry<String, EntryMetaInfo> entry : set) {
				dos.writeUTF(entry.getKey());
				writeMetaInfo(entry.getValue(), dos);
			}
			dos.flush();
			entries.clear();
		} catch (final IOException e) {
			ToTo.logException(e);
		} finally {
			IOUtils.closeQuietly(dos);
		}
	}

	private void setupJournalFile() {
		final File file = new File(cacheDirectory, journalFile);
		if (file.exists()) {
			/* journal file exists, read and load */
			loadJournalFile(file);
		}
	}

	@Override
	protected Storage<K, V>.EntryMetaInfo getEntryMetaInfo(final K key) {
		final byte[] keyData = keyConverter.from(key);
		if (keyData != null) {
			final String hash = getHashForKey(keyData);
			return entries.get(hash);
		}
		return null;
	}

	@Override
	protected Map<K, Storage<K, V>.EntryMetaInfo> getAllEntriesMetaInfo() {
		final Map<K, Storage<K, V>.EntryMetaInfo> metaInfo = new HashMap<K, Storage<K, V>.EntryMetaInfo>();
		final Set<String> hashes = entries.keySet();
		RandomAccessFile raf = null;
		File file;
		K k;
		for (final String str : hashes) {
			file = new File(cacheDirectory, str);
			if (file.exists()) {
				try {
					raf = new RandomAccessFile(file, "r");
					/* fast forward the value entry */
					final int valSize = raf.readInt();
					raf.seek(valSize + 4);
					final byte[] key = readKeyFromMerge(raf);
					k = keyConverter.to(key);
					if (k != null) {
						metaInfo.put(k, entries.get(str));
					}
				} catch (final FileNotFoundException e) {
					ToTo.logException(e);
				} catch (final IOException e) {
					ToTo.logException(e);
				} finally {
					IOUtils.closeQuietly(raf);
				}
			} else {
				entries.remove(str);
			}
		}
		return metaInfo;
	}
}
