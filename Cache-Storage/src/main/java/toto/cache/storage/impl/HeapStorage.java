package toto.cache.storage.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import toto.cache.Storage;


/**
 * Heap based storage provider.
 * <p>
 * Holds strong references to the cached values.
 * </p>
 * 
 * @author Mobifluence Interactive
 */
public class HeapStorage<K, V> extends Storage<K, V> {

	private ConcurrentHashMap<K, CacheEntry<V>> cache;

	static final class CacheEntry<V> {
		CacheEntry(final V value, final long timeAdded) {
			this.value = value;
			this.timeAdded = timeAdded;
			lastAccessed = timeAdded;
		}

		V value;
		long timeAdded;
		int timesAccessed;
		long lastAccessed;
	}

	/**
	 * Create a RAM based storage provider with the max
	 * <code>numberOfEntries</code>
	 * 
	 * @param numberOfEntries
	 */
	public HeapStorage(final int numberOfEntries) {
		cache = new ConcurrentHashMap<K, CacheEntry<V>>(numberOfEntries);
	}

	/**
	 * Creates a memory storage provider with max entries of 50.
	 */
	public HeapStorage() {
		this(50);
	}

	@Override
	protected boolean put(final K key, final V value) {
		final CacheEntry<V> v = cache.put(key,
				new CacheEntry<V>(value, System.currentTimeMillis()));
		return v == null;
	}

	@Override
	protected boolean remove(final K key) {
		final CacheEntry<V> v = cache.remove(key);
		if (v != null) {
			return true;
		}
		return false;
	}

	@Override
	protected V get(final K key) {
		final CacheEntry<V> v = cache.get(key);
		if (v != null) {
			v.timesAccessed++;
			v.lastAccessed = System.currentTimeMillis();
			return v.value;
		}
		return null;
	}

	@Override
	protected void clear() {
		cache.clear();
	}

	@Override
	protected void destroy() {
		clear();
		cache = null;
	}

	/**
	 * Calling close does nothing on a {@link HeapStorage}
	 */
	@Override
	protected void close() {
	}

	@Override
	protected int size() {
		return cache.size();
	}

	@Override
	protected toto.cache.Storage<K, V>.EntryMetaInfo getEntryMetaInfo(
			final K key) {
		final CacheEntry<V> v = cache.get(key);
		if (v != null) {
			final Storage.EntryMetaInfo info = new EntryMetaInfo();
			info.setTimeAdded(v.timeAdded);
			info.setTimesAccessed(v.timesAccessed);
			info.setLastAccessed(v.lastAccessed);
			return info;
		}
		return null;
	}

	@Override
	protected Map<K, toto.cache.Storage<K, V>.EntryMetaInfo> getAllEntriesMetaInfo() {
		final Map<K, Storage<K, V>.EntryMetaInfo> metaInfo = new HashMap<K, Storage<K, V>.EntryMetaInfo>();
		final Set<Entry<K, CacheEntry<V>>> set = cache.entrySet();
		EntryMetaInfo info;
		CacheEntry<V> v;
		for (final Entry<K, CacheEntry<V>> entry : set) {
			info = new EntryMetaInfo();
			v = entry.getValue();
			info.setTimesAccessed(v.timesAccessed);
			info.setTimeAdded(v.timeAdded);
			info.setLastAccessed(v.lastAccessed);
			metaInfo.put(entry.getKey(), info);
		}
		return metaInfo;
	}
}
