package toto.cache.storage.impl;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;

import com.mi.toto.ToTo;

import toto.cache.SerializedStorage;
import toto.cache.SerializedStorageConverter;
import toto.cache.Storage;
import toto.io.file.FileUtils;
import toto.security.checksum.Adler32;

/**
 * Storage provider that stores cache entries as files in the provided
 * directory.
 * <p>
 * 
 * <div> As with any file storage mechanism, the number of characters on the
 * file name is limited. Therefore, its best to map cache entries with small key
 * values.<br/>
 * Recommended locations for flat file storage provider.<br/>
 * 
 * <pre>
 * context.getCacheDir()
 * </pre>
 * 
 * or
 * 
 * <pre>
 * context.getExternalCacheDir()
 * </pre>
 * 
 * </div>
 * </p>
 * 
 * @author Mobifluence Interactive
 * 
 */
public class DiskStorage<K, V> extends SerializedStorage<K, V> {

	protected File cacheDirectory;

	public DiskStorage(final SerializedStorageConverter<K> keyConverter,
			final SerializedStorageConverter<V> valueConverter,
			final File cacheDirectory) {
		super(keyConverter, valueConverter);
		this.cacheDirectory = cacheDirectory;
		if (!cacheDirectory.exists()) {
			cacheDirectory.mkdirs();
		}
		if (!cacheDirectory.canWrite()) {
			throw new IllegalAccessError("CacheDirectory is not writable: "
					+ cacheDirectory.toString());
		}
		if (!cacheDirectory.isDirectory()) {
			throw new IllegalArgumentException(
					"CacheDirectory should be a directory");
		}
	}

	@Override
	protected void close() {
	}

	protected String getHashForKey(final byte[] key) {
		return new Adler32().toHexString(key);
	}

	@Override
	protected boolean save(final byte[] key, final byte[] value) {
		final String keyHex = getHashForKey(key);
		final String cacheDir = cacheDirectory.toString() + File.separator
				+ keyHex;
		final File file = new File(cacheDir);
		if (file.exists()) {
			file.delete();
		}
		return saveEntry(file, key, value);
	}

	private boolean saveEntry(final File file, final byte[] key,
			final byte[] value) {
		try {
			/* save key data along with file */
			final DataOutputStream dos = new DataOutputStream(
					new FileOutputStream(file));
			/* write access count */
			dos.writeInt(0);
			/* last accessed */
			dos.writeLong(System.currentTimeMillis());
			/* write time added */
			dos.writeLong(System.currentTimeMillis());
			/* write value data size */
			writeMergedKeyValue(key, value, dos);
			dos.flush();
			dos.close();
			return true;
		} catch (final IOException e) {
			ToTo.logException(e);
			return false;
		}
	}

	/**
	 * The value is written first and later the key
	 * 
	 * @param key
	 * @param value
	 * @param os
	 * @throws IOException
	 * @see {@link #readValueFromMerge(DataInput)}
	 * @see {@link #readKeyFromMerge(DataInput)}
	 * 
	 */
	protected void writeMergedKeyValue(final byte[] key, final byte[] value,
			final DataOutput os) throws IOException {
		os.writeInt(value.length);
		os.write(value);
		os.writeInt(key.length);
		os.write(key);
	}

	/**
	 * 
	 * @param input
	 * @return
	 * @throws IOException
	 * @see #writeMergedKeyValue(byte[], byte[], DataOutput)
	 * @see #readKeyFromMerge(DataInput)
	 */
	protected byte[] readValueFromMerge(final DataInput input)
			throws IOException {
		/* get the length of the value */
		final int valLen = input.readInt();
		final byte[] value = new byte[valLen];
		input.readFully(value);
		return value;
	}

	/**
	 * Read pointer should be at location to read the value
	 * 
	 * @param input
	 * @return
	 * @throws IOException
	 * @see #readValueFromMerge(byte[], byte[], DataOutput)
	 */
	protected byte[] readKeyFromMerge(final DataInput input) throws IOException {
		/* get the length of the value */
		final int keyLen = input.readInt();
		final byte[] key = new byte[keyLen];
		input.readFully(key);
		return key;
	}

	private byte[] readValueEntry(final File file) {
		try {
			final RandomAccessFile rfa = new RandomAccessFile(file, "rw");
			/* int,long,long */
			int accessCount = rfa.readInt();
			accessCount++;
			rfa.seek(0);
			rfa.writeInt(accessCount);
			rfa.writeLong(System.currentTimeMillis());
			rfa.seek(20);
			final byte[] valData = readValueFromMerge(rfa);
			rfa.close();
			return valData;
		} catch (final IOException e) {
			ToTo.logException(e);
		}
		return null;

	}

	@Override
	protected boolean delete(final byte[] key) {
		final File file = getFileForKey(key);
		if (file.exists()) {
			file.delete();
			return true;
		}
		return false;
	}

	@Override
	protected byte[] fetch(final byte[] key) {
		final File file = getFileForKey(key);
		if (file.exists()) {
			return readValueEntry(file);
		}
		return null;
	}

	private File getFileForKey(final byte[] key) {
		final String keyHex = new Adler32().toHexString(key);
		final String cacheDir = cacheDirectory.toString() + File.separator
				+ keyHex;
		return new File(cacheDir);
	}

	@Override
	protected void clear() {
		try {
			FileUtils.emptyDirectory(cacheDirectory);
		} catch (final IOException e) {
			ToTo.logException(e);
		}
	}

	@Override
	protected int size() {
		return cacheDirectory.list().length;
	}

	@Override
	protected void destroy() {
		try {
			FileUtils.emptyDirectory(cacheDirectory);
		} catch (final IOException e) {
			ToTo.logException(e);
		}
	}

	@Override
	protected Storage<K, V>.EntryMetaInfo getEntryMetaInfo(final K key) {
		final byte[] keyData = keyConverter.from(key);
		if (keyData != null) {
			final File file = getFileForKey(keyData);
			EntryMetaInfo meta = null;
			if (file.exists()) {
				try {
					final RandomAccessFile rfa = new RandomAccessFile(file, "r");
					meta = readMetaInfo(rfa);
					rfa.close();
					return meta;
				} catch (final FileNotFoundException e) {
					ToTo.logException(e);
				} catch (final IOException e) {
					ToTo.logException(e);
				}
			}
		}
		return null;
	}

	@Override
	protected Map<K, Storage<K, V>.EntryMetaInfo> getAllEntriesMetaInfo() {
		final Map<K, Storage<K, V>.EntryMetaInfo> metaInfo = new HashMap<K, Storage<K, V>.EntryMetaInfo>();
		final File[] file = cacheDirectory.listFiles();
		EntryMetaInfo meta = null;
		K key = null;
		try {
			for (int i = 0; i < file.length; i++) {
				if (file[i].exists()) {
					final RandomAccessFile rfa = new RandomAccessFile(file[i],
							"r");
					meta = readMetaInfo(rfa);
					final int valSize = meta.getValueSizeInBytes();
					rfa.seek(24 + valSize);/* skip reading value */
					final int keySize = rfa.readInt();
					final byte[] keyData = new byte[keySize];
					rfa.read(keyData);
					rfa.close();
					key = keyConverter.to(keyData);
					if (key != null) {
						metaInfo.put(key, meta);
					}
				}
			}
		} catch (final IOException e) {
			ToTo.logException(e);
		}
		return metaInfo;
	}
}
