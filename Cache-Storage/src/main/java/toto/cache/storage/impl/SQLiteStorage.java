package toto.cache.storage.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;

import com.mi.toto.ToTo;

import toto.cache.SerializedStorageConverter;
import toto.cache.Storage;
import toto.io.file.FileUtils;
import toto.lang.IntegerUtils;
import toto.security.checksum.Adler32;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;

public final class SQLiteStorage<K, V> extends DiskStorage<K, V> {

	private SQLiteDatabase db;

	/**
	 * Cache entries storage mechanism
	 * 
	 * @author Mobifluence Interactive
	 * 
	 */
	public enum StorageMode {
		/**
		 * Store cache entries as BLOB's. BLOB based storage modes are fast for
		 * values that are just a few KB.
		 */
		BLOB,
		/**
		 * Store cache entries in the cache directory.
		 */
		FLAT_FILE;
	}

	private final StorageMode storageMode;

	/**
	 * Create a SQlite based provider instance
	 * <p>
	 * The default storage mode is {@link StorageMode#BLOB}.
	 * </p>
	 * 
	 * @param context
	 * @param cacheDirectory
	 * @throws TCacheException
	 *             if create a SQlite based provider fails.
	 * @see toto.cache.TCache#setStorageProvider(Storage)
	 */
	public SQLiteStorage(final SerializedStorageConverter<K> keyConverter,
			final SerializedStorageConverter<V> valueConverter,
			final File cacheDirectory) {
		this(keyConverter, valueConverter, cacheDirectory, StorageMode.BLOB);
	}

	/**
	 * Create a SQlite based provider instance
	 * 
	 * @param context
	 * @param cacheDirectory
	 * @param storageMode
	 *            For saving entries. The default {@link StorageMode} is
	 *            {@link StorageMode#BLOB}. Once a cache is created using a
	 *            storage mode, it should be accessed using the same storage
	 *            mode.
	 */
	public SQLiteStorage(final SerializedStorageConverter<K> keyConverter,
			final SerializedStorageConverter<V> valueConverter,
			final File cacheDirectory, final StorageMode storageMode) {
		super(keyConverter, valueConverter, cacheDirectory);
		this.storageMode = storageMode;
		setupDB();
	}

	private final String selectHashQuery = "select " + COL_HASH + " from "
			+ TABLE_NAME + " where " + COL_HASH + " =?";

	private byte[] extractKey(final byte[] blob) {
		final int valueLen = IntegerUtils.fromBytes(blob, 0);
		/**/
		final int keyLocation = valueLen + 4;
		final int keyLen = IntegerUtils.fromBytes(blob, keyLocation);
		final byte[] key = new byte[keyLen];
		System.arraycopy(blob, keyLocation + 4, key, 0, keyLen);
		return key;
	}

	private boolean saveEntry(final byte[] data, final File file) {
		try {
			final FileOutputStream fos = new FileOutputStream(file);
			fos.write(data);
			fos.flush();
			fos.close();
			return true;
		} catch (final IOException e) {
			ToTo.logException(e);
		}
		return false;
	}

	@Override
	protected boolean save(final byte[] key, final byte[] value) {
		boolean saved = false;
		try {
			final String hash = new Adler32().toHexString(key);
			final Cursor c = db
					.rawQuery(selectHashQuery, new String[] { hash });
			final int existingCount = c.getCount();
			c.close();
			/* populate the values to be saved */
			final ByteArrayOutputStream bos = new ByteArrayOutputStream();
			writeMergedKeyValue(key, value, new DataOutputStream(bos));
			final byte[] mergedData = bos.toByteArray();
			final ContentValues values = new ContentValues();
			values.put(COL_HASH, hash);
			values.put(COL_VALUE_SIZE, mergedData.length);
			values.put(COL_CREATED_ON, System.currentTimeMillis());
			values.put(COL_ACCESS_COUNT, 0);
			values.put(COL_LAST_ACCESS, System.currentTimeMillis());
			if (storageMode == StorageMode.BLOB) {
				values.put(COL_VALUE, mergedData);
			}
			/* check if the file exists */
			if (existingCount > 0) {
				// perform update
				final int rows = db.update(TABLE_NAME, values, COL_HASH + "=?",
						new String[] { hash });
				if (rows == 0) {
					return false;
				}
			} else {
				// perform insert
				final long id = db.insert(TABLE_NAME, null, values);
				saved = (id == -1) ? false : true;
			}
			if (storageMode == StorageMode.FLAT_FILE) {
				/* file system storage mode */
				final File file = new File(cacheDirectory + File.separator
						+ hash);
				if (existingCount > 0) {
					file.delete();
				}
				saved = saveEntry(mergedData, file);
				if (!saved) {
					db.delete(TABLE_NAME, COL_HASH + "=?",
							new String[] { hash });
				}
			}
		} catch (final SQLiteDiskIOException sqlite) {
			saved = false;
		} catch (final IOException e) {
			ToTo.logException(e);
			saved = false;
		}
		return saved;
	}

	@Override
	protected boolean delete(final byte[] key) {
		final String hash = getHashForKey(key);
		return removeByHash(hash);
	}

	private boolean removeByHash(final String hash) {
		final int count = db.delete(TABLE_NAME, COL_HASH + "=?",
				new String[] { hash });
		if (count == 0) {
			return false;
		}
		if (count > 0 && storageMode == StorageMode.FLAT_FILE) {
			final File file = new File(cacheDirectory + File.separator + hash);
			file.delete();
		}
		return true;
	}

	private final String selectQueryBlob = "select " + COL_HASH + ','
			+ COL_VALUE + ',' + COL_ACCESS_COUNT + " from " + TABLE_NAME
			+ " where " + COL_HASH + " =?";

	private final String selectQueryFlat = "select " + COL_HASH + ','
			+ COL_ACCESS_COUNT + " from " + TABLE_NAME + " where " + COL_HASH
			+ " =?";

	@Override
	protected byte[] fetch(final byte[] key) {
		try {
			final String hash = getHashForKey(key);
			String query = null;
			byte[] value = null;
			if (storageMode == StorageMode.BLOB) {
				query = selectQueryBlob;
			} else {
				query = selectQueryFlat;
			}
			final Cursor cursor = db.rawQuery(query, new String[] { hash });
			if (cursor != null && cursor.getCount() > 0) {
				if (!cursor.moveToFirst()) {
					/* the cursor is empty */
					cursor.close();
					return null;
				}
				if (storageMode == StorageMode.BLOB) {
					final byte[] valData = cursor.getBlob(cursor
							.getColumnIndex(COL_VALUE));
					if (valData != null) {
						value = readValueFromMerge(new DataInputStream(
								new ByteArrayInputStream(valData)));
					}
				} else {
					final File file = new File(cacheDirectory + File.separator
							+ hash);
					if (!file.exists()) {
						/* the file is removed */
						cursor.close();
						return null;
					}
					/* proceed with extracting the data from the file. */
					final RandomAccessFile rfa = new RandomAccessFile(file, "r");
					value = readValueFromMerge(rfa);
					rfa.close();
				}
				final int accessCount = cursor.getInt(cursor
						.getColumnIndex(COL_ACCESS_COUNT));
				cursor.close();
				/* update access count */
				final ContentValues values = new ContentValues();
				values.put(COL_ACCESS_COUNT, accessCount + 1);
				values.put(COL_LAST_ACCESS, System.currentTimeMillis());
				db.update(TABLE_NAME, values, COL_HASH + " = ?",
						new String[] { hash });
			}
			return value;
		} catch (final SQLiteDiskIOException e) {
			ToTo.logException(e);
		} catch (final IOException e) {
			ToTo.logException(e);
		}
		return null;
	}

	@Override
	protected void clear() {
		db.execSQL("delete from " + TABLE_NAME);
		db.close();
		db = null;
		super.clear();
	}

	@Override
	protected void close() {
		db.close();
	}

	@Override
	protected void destroy() {
		close();
		super.destroy();
	}

	private void setupDB() {
		final File dbFile = new File(cacheDirectory + File.separator + DB_NAME);
		db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
		if (db != null) {
			try {
				if (db.getVersion() != CURRENT_VERSION) {
					// table exists
					db.close();
					/* remove older versions of the cache */
					FileUtils.emptyDirectory(cacheDirectory);
					db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
					createTables();
				}
				return;
			} catch (final Exception e) {
				ToTo.logException(e);
			}
		}
		throw new ExceptionInInitializerError(
				"Could not create SQlite based storage. Creating DB file failed");
	}

	private void createTables() throws SQLException {
		String CREATE = null;
		if (storageMode == StorageMode.BLOB) {
			CREATE = "create table if not exists " + TABLE_NAME
					+ "(_id integer primary key autoincrement,  " + COL_HASH
					+ " text not null, " + COL_VALUE + " BLOB not null, "
					+ COL_VALUE_SIZE + " integer, " + COL_ACCESS_COUNT
					+ " integer, " + COL_LAST_ACCESS + " integer, "
					+ COL_CREATED_ON + " integer, " + COL_VALUE_HASH
					+ " integer)";
		} else {
			CREATE = "create table if not exists " + TABLE_NAME
					+ "(_id integer primary key autoincrement,  " + COL_HASH
					+ " text not null, " + COL_VALUE_SIZE + " integer, "
					+ COL_ACCESS_COUNT + " integer, " + COL_LAST_ACCESS
					+ " integer, " + COL_CREATED_ON + " integer, "
					+ COL_VALUE_HASH + " integer)";
		}
		db.execSQL(CREATE);
		db.setVersion(CURRENT_VERSION);

	}

	static final String COL_VALUE = "value";
	static final String COL_ACCESS_COUNT = "access_times";
	static final String COL_LAST_ACCESS = "last_accessed";
	static final String COL_CREATED_ON = "created_on";
	static final String COL_VALUE_HASH = "value_hash";
	static final String COL_VALUE_SIZE = "value_size";
	static final String COL_HASH = "hash";
	static final String TABLE_NAME = "TCache";
	static final String DB_NAME = "TCache.db";
	static final int CURRENT_VERSION = 0x3;

	@Override
	protected int size() {
		return (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
	}

	@Override
	protected toto.cache.Storage<K, V>.EntryMetaInfo getEntryMetaInfo(
			final K key) {
		final byte[] keyData = keyConverter.from(key);
		if (keyData != null) {
			final Cursor entries = db.query(TABLE_NAME, new String[] {
					COL_ACCESS_COUNT, COL_CREATED_ON, COL_LAST_ACCESS,
					COL_VALUE_SIZE }, COL_HASH + " = ?",
					new String[] { getHashForKey(keyData) }, null, null, null);
			if (!entries.moveToFirst()) {
				return null;
			}
			return getMeta(entries);
		}
		return null;
	}

	private EntryMetaInfo getMeta(final Cursor entries) {
		final EntryMetaInfo meta = new EntryMetaInfo();
		meta.setTimesAccessed(entries.getInt(entries
				.getColumnIndex(COL_ACCESS_COUNT)));
		meta.setTimeAdded(entries.getLong(entries
				.getColumnIndex(COL_CREATED_ON)));
		meta.setLastAccessed(entries.getLong(entries
				.getColumnIndex(COL_LAST_ACCESS)));
		meta.setValueSizeInBytes(entries.getInt(entries
				.getColumnIndex(COL_VALUE_SIZE)));
		return meta;
	}

	@Override
	protected Map<K, toto.cache.Storage<K, V>.EntryMetaInfo> getAllEntriesMetaInfo() {
		final Map<K, Storage<K, V>.EntryMetaInfo> metaInfo = new HashMap<K, Storage<K, V>.EntryMetaInfo>();
		final Cursor entries = db.rawQuery("select * from " + TABLE_NAME, null);
		if (!entries.moveToFirst()) {
			return null;
		}
		String hash;
		byte[] blob;
		byte[] keyData;
		File file;
		while (entries.moveToNext()) {
			hash = entries.getString(entries.getColumnIndex(COL_HASH));
			if (storageMode == StorageMode.BLOB) {
				blob = entries.getBlob(entries.getColumnIndex(COL_VALUE));
			} else {
				file = new File(cacheDirectory + File.separator + hash);
				if (!file.exists()) {
					continue;
				}
				blob = FileUtils.readFileToByteArraySilent(file);
			}
			if (blob == null) {
				continue;
			}
			keyData = extractKey(blob);
			metaInfo.put(keyConverter.to(keyData), getMeta(entries));
		}
		entries.close();
		return metaInfo;
	}
}
