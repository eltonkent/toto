package test.toto.cache;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import test.toto.R;
import test.toto.ToToTestCase;
import com.mi.toto.ToTo;
import toto.cache.TCache;
import toto.cache.storage.impl.DiskStorage;
import toto.cache.impl.converters.BitmapConverter;
import toto.cache.storage.converter.impl.EncryptionCallback;
import toto.cache.storage.converter.impl.StringConverter;
import toto.io.file.FileUtils;
import toto.util.random.RandomString;
import toto.util.zip.Zip;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Log;

public class FileStorageProviderTest extends ToToTestCase {
	File cacheLocation;

	public FileStorageProviderTest() {
		cacheLocation = new File(getTestStorageLocation() + File.separator
				+ "testcache/");
		if (!cacheLocation.exists()) {
			cacheLocation.mkdir();
		}
		try {
			FileUtils.emptyDirectory(cacheLocation);
		} catch (IOException e) {
			ToTo.logException(e);
		}
	}
	
	public void testOldestEntryEvictionFile(){
		DiskStorage<String, Bitmap> sp1 = new DiskStorage<String, Bitmap>(
				new StringConverter(null), new BitmapConverter(
						CompressFormat.JPEG), cacheLocation);
		TCache<String, Bitmap> cache = new TCache<String, Bitmap>(sp1);
		cache.setEvictionStrategy(cache.new OlderThanEviction(1000*5) /*5 milliseconds*/);
		Bitmap bitmap = BitmapFactory.decodeResource(getContext()
				.getResources(), R.drawable.girlskin);
		for(int i=0;i<5;i++){
			cache.cache(RandomString.random(15), bitmap);
			Log.d("ToTo","Cache Size "+cache.getSize());
		}
		int finalSize=cache.getSize();
		Log.d("ToTo","Final Cache Size "+finalSize);
		Log.d("ToTo","================================================================= ");
		cache.destroy();
	}
	
	
	public void testMaxEntrySize(){
		BitmapConverter bc=new BitmapConverter(
				CompressFormat.JPEG);
		bc.setMaxConvertedSize(50);/*store if the value is only within 50 bytes*/
		DiskStorage<String, Bitmap> sp1 = new DiskStorage<String, Bitmap>(
				new StringConverter(null), bc , cacheLocation);
		TCache<String, Bitmap> cache = new TCache<String, Bitmap>(sp1);
		Bitmap bitmap = BitmapFactory.decodeResource(getContext()
				.getResources(), R.drawable.girlskin);
		boolean saved=cache.cache("test", bitmap);
		assertFalse(saved);
		bc.setMaxConvertedSize(112145);
		saved=cache.cache("test", bitmap);
		assertTrue(saved);
		cache.destroy();
	}
	
	public void testWithCompression(){
		BitmapConverter bc=new BitmapConverter(
				CompressFormat.JPEG);
		bc.setCompressionAlgorithm(new Zip());
		DiskStorage<String, Bitmap> sp1 = new DiskStorage<String, Bitmap>(
				new StringConverter(null), bc , cacheLocation);
		TCache<String, Bitmap> cache = new TCache<String, Bitmap>(sp1);
		Bitmap bitmap = BitmapFactory.decodeResource(getContext()
				.getResources(), R.drawable.girlskin);
		boolean saved=cache.cache("test", bitmap);
		assertTrue(saved);
		Bitmap ret=cache.fetch("test");
		assertNotNull(ret);
		cache.destroy();
	}
	
	public void testCacheLocation() {
		DiskStorage<String, String> sp = new DiskStorage<String, String>(
				new StringConverter(null), new StringConverter(null),
				getContext().getCacheDir());
		TCache<String, String> cache = new TCache<String, String>(sp);
		boolean ret = cache.cache("test", "value");
		assertTrue(ret);
		ret = cache.contains("test");
		assertTrue(ret);
		String val = cache.fetch("test");
		assertEquals(val, "value");
		cache.remove("test");
		val = cache.fetch("test");
		assertNull(val);
		assertTrue(cache.getMisses() > 0);
		cache.destroy();
	}
	
	
	public void testCacheProviderDiskWithEncryption()
			throws NoSuchAlgorithmException {
		BitmapConverter bc = new BitmapConverter(CompressFormat.JPEG);
		try {
			final Cipher cipher = Cipher.getInstance("AES");
			Log.d("ToTo", "Adding encryption callback");
			final byte[] plainkey = "This is a String that is used to encrypt the data saved in the cache. Hope it does the job"
					.getBytes();
			KeyGenerator kgen = KeyGenerator.getInstance("AES");
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			sr.setSeed(plainkey);
			kgen.init(128, sr); // 192 and 256 bits may not be available
			SecretKey skey = kgen.generateKey();
			final byte[] key = skey.getEncoded();
			final SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
			bc.setEncryptionCallback(new EncryptionCallback() {

				@Override
				public byte[] doEncryption(byte[] plain) {
					try {
						Log.d("ToTo", "before encryption :" + plain.length);
						cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
						byte[] encrypted = cipher.doFinal(plain);
						Log.d("ToTo", "after encryption :" + encrypted.length);
						return encrypted;
					} catch (InvalidKeyException e) {
						e.printStackTrace();
					} catch (IllegalBlockSizeException e) {
						e.printStackTrace();
					} catch (BadPaddingException e) {
						e.printStackTrace();
					}
					return plain;
				}

				@Override
				public byte[] doDecryption(byte[] enc) {
					try {
						cipher.init(Cipher.DECRYPT_MODE, skeySpec);
						byte[] decrypted = cipher.doFinal(enc);
						return decrypted;
					} catch (InvalidKeyException e) {
						e.printStackTrace();
					} catch (IllegalBlockSizeException e) {
						e.printStackTrace();
					} catch (BadPaddingException e) {
						e.printStackTrace();
					}

					return enc;
				}
			});
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (NoSuchPaddingException e1) {
			e1.printStackTrace();
		}
		DiskStorage<String, Bitmap> sp = new DiskStorage<String, Bitmap>(
				new StringConverter(null), bc, getContext().getCacheDir());
		TCache<String, Bitmap> cache = new TCache<String, Bitmap>(sp);
		Bitmap val = cache.fetch("test");
		assertNull(val);
		Bitmap bitmap = BitmapFactory.decodeResource(getContext()
				.getResources(), R.drawable.girlskin);
		boolean ret = cache.cache("test", bitmap);
		assertTrue(ret);
		int size = cache.getSize();
		assert (size == 1);
		val = cache.fetch("test");
		assertNotNull(val);
		Long timeAdded;
		try {
			Method method = TCache.class.getDeclaredMethod("getTimeAdded",
					Object.class);
			method.setAccessible(true);
			timeAdded = (Long) method.invoke(cache, "test");
			Log.d("ToTo", "Time Added: " + timeAdded);

			method = TCache.class.getDeclaredMethod("getTimesAccessed",
					Object.class);
			method.setAccessible(true);
			int timesAccess = (Integer) method.invoke(cache, "test");
			assertTrue(timesAccess > 0);
			Log.d("ToTo", "timesAccess: " + timesAccess);

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		boolean removed = cache.remove("test");
		assertTrue(removed);
		val = cache.fetch("test");
		assertNull(val);
		cache.destroy();
	}
}
