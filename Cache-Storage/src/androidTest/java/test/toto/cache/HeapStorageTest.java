package test.toto.cache;

import java.io.File;
import java.io.IOException;

import test.toto.cache.R;
import test.toto.ToToTestCase;
import toto.cache.TCache;
import toto.cache.storage.impl.DiskStorage;
import toto.cache.storage.impl.HeapStorage;
import toto.cache.storage.converter.impl.CursorConverter;
import toto.cache.storage.converter.impl.StringConverter;
import toto.io.file.FileUtils;
import toto.util.random.RandomPrimitives;
import toto.util.random.RandomString;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class HeapStorageTest extends ToToTestCase {

	File cacheLocation;

	public HeapStorageTest() {
		cacheLocation = new File(getTestStorageLocation() + File.separator
				+ "testcache/");
		if (!cacheLocation.exists()) {
			cacheLocation.mkdir();
		}
		try {
			FileUtils.emptyDirectory(cacheLocation);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// public void testODbProvider() {
	// File dbDir = new File("/sdcard/testodbTCache/");
	// dbDir.mkdir();
	// ODbStorageProvider<String, Bitmap> sp = new ODbStorageProvider<String,
	// Bitmap>(
	// new StringEntryConverter(null), new BitmapEntryConverter(
	// CompressFormat.JPEG), dbDir);
	// TCache<String, Bitmap> cache = new TCache<String, Bitmap>(sp);
	// Bitmap val = cache.fetch("test");
	// assertNull(val);
	// Bitmap bitmap = BitmapFactory.decodeResource(getContext()
	// .getResources(), R.drawable.girlskin);
	// boolean ret = cache.cache("test", bitmap);
	// assertTrue(ret);
	// val = cache.fetch("test");
	// assertNotNull(val);
	// cache.destroy();
	// }

//	
	
	public void testHeapCache() {
		TCache<String, String> cache = new TCache<String, String>();
		String val = cache.fetch("test");
		assertNull(val);
		boolean ret = cache.cache("test", "rage cache");
		assertTrue(ret);
		val = cache.fetch("test");
		assertEquals(val, "rage cache");
		cache.destroy();
	}

	public void testHeapCacheWithBitmap() {
		TCache<String, Bitmap> cache = new TCache<String, Bitmap>(
				new HeapStorage<String, Bitmap>());
		Bitmap val = cache.fetch("test");
		assertNull(val);
		Bitmap bitmap = BitmapFactory.decodeResource(getContext()
				.getResources(), R.drawable.girlskin);
		boolean ret = cache.cache("test", bitmap);
		assertTrue(ret);
		val = cache.fetch("test");
		assertNotNull(val);
		cache.remove("test");
		val = cache.fetch("test");
		assertNull(val);
		cache.destroy();
	}

	

	

	

	
	

	public void testCursorConverter() {
		String tablename = "testrage";
		getContext().deleteDatabase("test_cache.db");
		TestSQLiteHelper helper = new TestSQLiteHelper(getContext());
		SQLiteDatabase db = helper.getWritableDatabase();
		RandomString rstring = new RandomString();
		for (int i = 0; i < 10; i++) {
			ContentValues values = new ContentValues();
			values.put(
					"col_text",
					"Welcome:"
							+ i
							+ " fdgfdgfd sgsfdg fdgfd fdg fdg fdg fdg dfgfd gfdgfd fgdfdgfdg dgfd gfd gfdg fdgfd fdg fdgfd fdgfdg fdgfdgfdgfd");
			db.insert("testrage", null, values);
		}
		long time = System.currentTimeMillis();
		Cursor query = db.query(tablename,
				new String[] { "col_id", "col_text" }, "col_id="
						+ RandomPrimitives.randomInt(1, 9), null, null, null,
				null);
		query.moveToFirst();
		String text = query.getString(1);
		// query.close();
		time = System.currentTimeMillis() - time;
		logI("Query :" + text + " Time taken(ms):" + time);

		CursorConverter cc = new CursorConverter(
				new CursorConverter.ColumnType[] {
						CursorConverter.ColumnType.INTEGER,
						CursorConverter.ColumnType.STRING });
		DiskStorage<String, Cursor> sp = new DiskStorage<String, Cursor>(
				new StringConverter(null), cc, cacheLocation);
		TCache<String, Cursor> cache = new TCache<String, Cursor>(sp);
		cache.cache("test", query);
		time = System.currentTimeMillis();
		Cursor cursor = cache.fetch("test");
		time = System.currentTimeMillis() - time;
		logI("Cursor" + cursor + " time:" + time);
		String text2 = cursor.getString(1);
		logI("text2:" + text2);
		assertEquals(text2, text);

		Cursor longContents = db.rawQuery("select * from " + tablename, null);
		cache.cache("all", longContents);
		cursor = cache.fetch("all");
		assertNotNull(cursor);
		cursor.moveToFirst();
		while (!cursor.isLast()) {
			logI("Column [col_text]:" + cursor.getString(1));
			cursor.moveToNext();
		}
		cache.destroy();
		getContext().deleteDatabase("test_cache.db");
	}

	private class TestSQLiteHelper extends SQLiteOpenHelper {
		public TestSQLiteHelper(Context context) {
			super(context, "test_cache.db", null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("create table testrage (col_id integer primary key autoincrement, col_text text not null);");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS testrage");
			onCreate(db);
		}
	}
}
