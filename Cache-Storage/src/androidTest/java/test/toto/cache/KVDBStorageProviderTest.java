package test.toto.cache;

import java.io.File;
import java.io.IOException;

import test.toto.R;
import test.toto.ToToTestCase;
import toto.cache.TCache;
import toto.cache.impl.converters.BitmapConverter;
import toto.cache.storage.converter.impl.StringConverter;
import toto.db.kvdb.KVDBStorage;
import toto.io.file.FileUtils;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;


public class KVDBStorageProviderTest  extends ToToTestCase{

	
	File cacheLocation;

	public KVDBStorageProviderTest() {
		cacheLocation = new File(getTestStorageLocation() + File.separator
				+ "testcache/");
		if (!cacheLocation.exists()) {
			cacheLocation.mkdir();
		}
		try {
			FileUtils.emptyDirectory(cacheLocation);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void testKVDbProvider() {
		TCache<String, Bitmap> cache;
		try {
			KVDBStorage<String, Bitmap> sp = new KVDBStorage<String, Bitmap>(
					new StringConverter(null), new BitmapConverter(
							CompressFormat.JPEG), cacheLocation);
			cache = new TCache<String, Bitmap>(sp);
			Bitmap val = cache.fetch("test");
			assertNull(val);
			Bitmap bitmap = BitmapFactory.decodeResource(getContext()
					.getResources(), R.drawable.girlskin);
			boolean ret = cache.cache("test", bitmap);
			assertTrue(ret);
			int size=cache.getSize();
			assertEquals(size, 1);
			val = cache.fetch("test");
			assertNotNull(val);
			cache.close();
			sp = new KVDBStorage<String, Bitmap>(
					new StringConverter(null), new BitmapConverter(
							CompressFormat.JPEG), cacheLocation);
			cache = new TCache<String, Bitmap>(sp);
			size=cache.getSize();
			assertEquals(size, 1);
			boolean removed = cache.remove("test");
			assertTrue(removed);
			size=cache.getSize();
			assertEquals(size, 0);
			val = cache.fetch("test");
			assertNull(val);
			cache.destroy();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
