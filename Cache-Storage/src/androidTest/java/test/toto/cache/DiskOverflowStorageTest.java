package test.toto.cache;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import test.toto.ToToTestCase;
import com.mi.toto.ToTo;
import toto.cache.TCache;

import toto.io.file.FileUtils;
import toto.util.random.RandomString;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;

public class DiskOverflowStorageTest extends ToToTestCase {

//	private File cacheLocation;
//
//	public DiskOverflowStorageTest() {
//		cacheLocation = new File(getTestStorageLocation() + File.separator
//				+ "testcache/");
//		if (!cacheLocation.exists()) {
//			cacheLocation.mkdir();
//		}
//		try {
//			FileUtils.emptyDirectory(cacheLocation);
//		} catch (IOException e) {
//			ToTo.logException(e);
//		}
//	}
//
//	public void testInsertReadAndDelete() {
//		DiskOverflowStorage<String, Bitmap> sp1 = new DiskOverflowStorage<String, Bitmap>(
//				new StringConverter(null), new BitmapConverter(
//						CompressFormat.JPEG), cacheLocation);
//		sp1.setOnHeapCount(20);
//		TCache<String, Bitmap> cache = new TCache<String, Bitmap>(sp1);
//		Bitmap bitmap = BitmapFactory.decodeResource(getContext()
//				.getResources(), R.drawable.girlskin);
//		List<String> entries=new ArrayList<String>();
//		for (int i = 0; i < 25; i++) {
//			String name=RandomString.random(15);
//			entries.add(name);
//			cache.cache(name, bitmap);
//		}
//		Log.d("ToTo", "Offloaded count " + sp1.getOverflowCount());
//		assertEquals(sp1.getOverflowCount(), 5);
//		for(int i=0;i<entries.size();i++){
//			Bitmap bm=cache.fetch(entries.get(i));
//			assertNotNull(bm);
//		}
//		for(int i=0;i<entries.size();i++){
//			boolean removed=cache.remove(entries.get(i));
//			assertTrue(removed);
//		}
//		cache.close();
//	}
}
