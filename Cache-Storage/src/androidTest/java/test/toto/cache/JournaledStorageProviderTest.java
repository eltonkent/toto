package test.toto.cache;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import test.toto.R;
import test.toto.ToToTestCase;
import toto.cache.TCache;
import toto.cache.storage.impl.JournaledDiskStorage;
import toto.cache.impl.converters.BitmapConverter;
import toto.cache.storage.converter.impl.StringConverter;
import toto.io.file.FileUtils;
import toto.util.random.RandomString;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Log;

public class JournaledStorageProviderTest extends ToToTestCase {

	File cacheLocation;

	public JournaledStorageProviderTest() {
		cacheLocation = new File(getTestStorageLocation() + File.separator
				+ "testcache/");
		if (!cacheLocation.exists()) {
			cacheLocation.mkdir();
		}
		try {
			FileUtils.emptyDirectory(cacheLocation);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void testCacheLocation() {
		JournaledDiskStorage<String, String> sp = new JournaledDiskStorage<String, String>(
				new StringConverter(null), new StringConverter(null),
				getContext().getCacheDir());
		TCache<String, String> cache = new TCache<String, String>(sp);
		boolean ret = cache.cache("test", "value");
		assertTrue(ret);
		ret = cache.contains("test");
		assertTrue(ret);
		String val = cache.fetch("test");
		assertEquals(val, "value");
		cache.remove("test");
		val = cache.fetch("test");
		assertNull(val);
		assertTrue(cache.getMisses() > 0);
		cache.destroy();
	}

	public void testOldestEntryEvictionFile() {
		JournaledDiskStorage<String, Bitmap> sp1 = new JournaledDiskStorage<String, Bitmap>(
				new StringConverter(null), new BitmapConverter(
						CompressFormat.JPEG), cacheLocation);
		TCache<String, Bitmap> cache = new TCache<String, Bitmap>(sp1);
		cache.setEvictionStrategy(cache.new OlderThanEviction(1000 * 5) /*
																		 * 5
																		 * milliseconds
																		 */);
		Bitmap bitmap = BitmapFactory.decodeResource(getContext()
				.getResources(), R.drawable.girlskin);
		for (int i = 0; i < 5; i++) {
			cache.cache(RandomString.random(15), bitmap);
			Log.d("ToTo", "Cache Size " + cache.getSize());
		}
		int finalSize = cache.getSize();
		Log.d("ToTo", "Final Cache Size " + finalSize);
		Log.d("ToTo",
				"================================================================= ");
		cache.destroy();
	}

	public void testExists() {
		JournaledDiskStorage<String, Bitmap> sp1 = new JournaledDiskStorage<String, Bitmap>(
				new StringConverter(null), new BitmapConverter(
						CompressFormat.JPEG), cacheLocation);
		TCache<String, Bitmap> cache = new TCache<String, Bitmap>(sp1);
		Bitmap bitmap = BitmapFactory.decodeResource(getContext()
				.getResources(), R.drawable.girlskin);
		for (int i = 0; i < 5; i++) {
			cache.cache(RandomString.random(15), bitmap);
			Log.d("ToTo", "Cache Size " + cache.getSize());
		}
		int finalSize = cache.getSize();
		Log.d("ToTo", "Final Cache Size " + finalSize);
		Log.d("ToTo",
				"================================================================= ");
		cache.close();
		File file = new File(cacheLocation, "TCache.journal");
		assertTrue(file.exists());
		File[] files = cacheLocation.listFiles();
		assertEquals(files.length, 6);
		cache.destroy();
		assertFalse(file.exists());
	}

	public void testEntryMeta() {
		JournaledDiskStorage<String, Bitmap> sp1 = new JournaledDiskStorage<String, Bitmap>(
				new StringConverter(null), new BitmapConverter(
						CompressFormat.JPEG), cacheLocation);
		TCache<String, Bitmap> cache = new TCache<String, Bitmap>(sp1);
		Bitmap val = cache.fetch("test");
		assertNull(val);
		Bitmap bitmap = BitmapFactory.decodeResource(getContext()
				.getResources(), R.drawable.girlskin);
		boolean ret = cache.cache("test", bitmap);
		assertTrue(ret);
		int size = cache.getSize();
		assert (size == 1);
		val = cache.fetch("test");
		assertNotNull(val);
		Long timeAdded;
		try {
			Method method = TCache.class.getDeclaredMethod("getTimeAdded",
					Object.class);
			method.setAccessible(true);
			timeAdded = (Long) method.invoke(cache, "test");
			Log.d("ToTo", "Time Added: " + timeAdded);

			method = TCache.class.getDeclaredMethod("getTimesAccessed",
					Object.class);
			method.setAccessible(true);
			int timesAccess = (Integer) method.invoke(cache, "test");
			assertTrue(timesAccess > 0);
			Log.d("ToTo", "timesAccess: " + timesAccess);

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		boolean removed = cache.remove("test");
		assertTrue(removed);
		val = cache.fetch("test");
		assertNull(val);
		cache.destroy();
	}
}
