/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget;

import toto.geom2d.Angle;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.mi.toto.ui.widget.R;

/**
 * Used for creating a rotary knob <br/>
 * knobDrawable - the drawable used as the knob
 * 
 * @author elton.kent
 * 
 */
public class KnobView extends View {
	private float angle = 0f;
	private float theta_old = 0f;
	private Drawable bitmapDraw;
	private RotaryKnobListener listener;

	public interface RotaryKnobListener {
		public void onKnobChanged(int arg);
	}

	public void setKnobListener(final RotaryKnobListener l) {
		listener = l;
	}

	public KnobView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
		final TypedArray styledAttrs = context.obtainStyledAttributes(attrs,
				R.styleable.KnobView);
		final Drawable knobDrawable = styledAttrs
				.getDrawable(R.styleable.KnobView_knobDrawable);
		initialize(knobDrawable);
	}

	public void initialize(final Drawable bitmapDraw) {
		// bitmapDraw = new
		// BitmapDrawable(BitmapFactory.decodeResource(getResources(),
		// R.drawable.jog));
		bitmapDraw.setBounds(0, 0, getWidth(), getHeight());
		// this.setImageResource(R.drawable.jog);
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(final View v, final MotionEvent event) {
				final int action = event.getAction();
				final int actionCode = action & MotionEvent.ACTION_MASK;
				if (actionCode == MotionEvent.ACTION_POINTER_DOWN) {
					final float x = event.getX(0);
					final float y = event.getY(0);
					theta_old = Angle.getAngleOfPoint(x, y, getWidth(),
							getHeight());
				} else if (actionCode == MotionEvent.ACTION_MOVE) {
					invalidate();

					final float x = event.getX(0);
					final float y = event.getY(0);

					final float theta = Angle.getAngleOfPoint(x, y, getWidth(),
							getHeight());
					final float delta_theta = theta - theta_old;

					theta_old = theta;

					final int direction = (delta_theta > 0) ? 1 : -1;
					angle += 5 * direction;

					notifyListener(direction);

				}
				return true;
			}

		});
	}

	/**
	 * Determines the width of this view
	 * 
	 * @param measureSpec
	 *            A measureSpec packed into an int
	 * @return The width of the view, honoring constraints from measureSpec
	 */
	private int measureWidth(final int measureSpec) {
		int result = 0;
		final int specMode = MeasureSpec.getMode(measureSpec);
		final int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.EXACTLY) {
			// We were told how big to be
			result = specSize;
		} else {
			// Measure the text
			result = specSize;
		}

		return result;
	}

	/**
	 * Determines the height of this view
	 * 
	 * @param measureSpec
	 *            A measureSpec packed into an int
	 * @return The height of the view, honoring constraints from measureSpec
	 */
	private int measureHeight(final int measureSpec) {
		int result = 0;
		final int specMode = MeasureSpec.getMode(measureSpec);
		final int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.EXACTLY) {
			// We were told how big to be
			result = specSize;
		} else {
			// Measure the text (beware: ascent is a negative number)
			result = specSize;
		}
		return result;
	}

	/**
	 * @see android.view.View#measure(int, int)
	 */
	@Override
	protected void onMeasure(final int widthMeasureSpec,
			final int heightMeasureSpec) {
		int finalWidth, finalHeight;
		finalWidth = measureWidth(widthMeasureSpec);
		finalHeight = measureHeight(heightMeasureSpec);
		bitmapDraw.setBounds(0, 0, finalWidth, finalHeight);
		setMeasuredDimension(finalWidth, finalHeight);
	}

	private void notifyListener(final int arg) {
		if (null != listener)
			listener.onKnobChanged(arg);
	}

	protected void onDraw(final Canvas c) {
		c.rotate(angle, getWidth() / 2, getHeight() / 2);
		// bitmapDraw.setBounds(0, 0, getWidth(), getHeight());
		bitmapDraw.draw(c);
		super.onDraw(c);
	}
}
