/**
 * A whole lotta Custom UI Widgets.
 * <p>
 * The attribute set for each widget is named after the widget and is declared in the values/attrs.xml file
 * </p>
 */
package toto.ui.widget;