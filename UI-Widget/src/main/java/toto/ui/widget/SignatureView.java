/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.widget;

import toto.ui.utils.ViewUtils;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.mi.toto.ui.widget.R;

/**
 * View that captures signatures
 * <p>
 * <div class="api-usage">
 * <h4>Usage</h4>
 * The default stroke color is black. Therefore one needs to change the stroke
 * color or background color to see the view rendering. <br/>
 * <table cellspacing="1" cellpadding="3">
 * <tr>
 * <th><b>Attribute</b></td>
 * <td width="50"><b>Type</b></td>
 * <td><b>Default</b></td>
 * <td><b>Description</b></td>
 * </tr>
 * <tr>
 * <td><code>sv_strokeColor</code></td>
 * <td>Color</td>
 * <td>Color.Black(0x000000)</td>
 * <td>Color of the signature stroke.</td>
 * </tr>
 * <tr>
 * <td><code>sv_strokeWidth</code></td>
 * <td>float</td>
 * <td>5f</td>
 * <td>Width of signature stroke</td>
 * </tr>
 * </table>
 * <br/>
 * <h4>Demo</h4><br/>
 * <center><OBJECT CLASSID="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
 * WIDTH="428" HEIGHT="716" CODEBASE
 * ="http://active.macromedia.com/flash5/cabs/swflash.cab#version=7,0,0,0">
 * <PARAM NAME=movie VALUE="../../../resources/signatureview.swf"> <PARAM
 * NAME=play VALUE=true> <PARAM NAME=loop VALUE=false> <PARAM NAME=wmode
 * VALUE=transparent> <PARAM NAME=quality VALUE=low> <EMBED
 * SRC="../../../resources/signatureview.swf" WIDTH=428 HEIGHT=716 quality=low
 * loop=false wmode=transparent TYPE="application/x-shockwave-flash"
 * PLUGINSPAGE=
 * "http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"
 * > </EMBED> </OBJECT></center> <SCRIPT
 * src='../../../resources/pagecurl.js'></script> </div>
 * </p>
 * 
 * @author elton.kent
 * 
 */
public class SignatureView extends View {

	private final float STROKE_WIDTH = 5f;

	/** Need to track this so the dirty region can accommodate the stroke. **/
	private float HALF_STROKE_WIDTH;

	private final Paint paint = new Paint();
	private final Path path = new Path();

	/**
	 * Optimizes painting by invalidating the smallest possible area.
	 */
	private float lastTouchX;
	private float lastTouchY;
	private final RectF dirtyRect = new RectF();
	private final int inkColor = Color.parseColor("#042FAF");

	public SignatureView(final Context context) {
		super(context);
		init(inkColor, STROKE_WIDTH);
	}

	public SignatureView(final Context context, final AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SignatureView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
		final TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.SignatureView);
		final int color = a.getColor(R.styleable.SignatureView_sv_stroke_color,
				inkColor);
		final float width = a.getFloat(R.styleable.SignatureView_sv_stroke_width,
				5f);
		init(color, width);

	}

	@Override
	protected void onMeasure(final int widthMeasureSpec,
			final int heightMeasureSpec) {
		final int[] wh = ViewUtils.doOnMeasure(widthMeasureSpec,
				heightMeasureSpec, 200, 300);
		setMeasuredDimension(wh[0], wh[1]);
	}

	private void init(final int color, final float width) {
		setBackgroundColor(Color.WHITE);
		paint.setAntiAlias(true);
		paint.setColor(color);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeWidth(width);
		HALF_STROKE_WIDTH = width / 2;

	}

	/**
	 * Set the stroke color of the signature.
	 * <p>
	 * the default color is #042FAF.
	 * </p>
	 * 
	 * @param color
	 */
	public void setStrokeColor(final int color) {
		clear();
		paint.setColor(color);
	}

	/**
	 * Set the stroke width of the signature view.
	 * <p>
	 * The Default is 5.0
	 * </p>
	 * 
	 * @param width
	 */
	public void setStrokeWidth(final float width) {
		paint.setStrokeWidth(width);
	}

	/**
	 * Erases the signature.
	 */
	public void clear() {
		path.reset();

		// Repaints the entire view.
		invalidate();
	}

	@Override
	protected void onDraw(final Canvas canvas) {
		canvas.drawPath(path, paint);
	}

	@Override
	public boolean onTouchEvent(final MotionEvent event) {
		Log.d("Tesat", "Signature view touched");
		final float eventX = event.getX();
		final float eventY = event.getY();

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			path.moveTo(eventX, eventY);
			lastTouchX = eventX;
			lastTouchY = eventY;
			// There is no end point yet, so don't waste cycles invalidating.
			return true;

		case MotionEvent.ACTION_MOVE:
		case MotionEvent.ACTION_UP:
			// Start tracking the dirty region.
			resetDirtyRect(eventX, eventY);

			// When the hardware tracks events faster than they are delivered,
			// the
			// event will contain a history of those skipped points.
			final int historySize = event.getHistorySize();
			for (int i = 0; i < historySize; i++) {
				final float historicalX = event.getHistoricalX(i);
				final float historicalY = event.getHistoricalY(i);
				expandDirtyRect(historicalX, historicalY);
				path.lineTo(historicalX, historicalY);
			}

			// After replaying history, connect the line to the touch point.
			path.lineTo(eventX, eventY);
			break;

		default:
			return false;
		}

		// Include half the stroke width to avoid clipping.
		invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
				(int) (dirtyRect.top - HALF_STROKE_WIDTH),
				(int) (dirtyRect.right + HALF_STROKE_WIDTH),
				(int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

		lastTouchX = eventX;
		lastTouchY = eventY;

		return true;
	}

	/**
	 * Get the captured signature as a bitmap
	 * 
	 * @param config
	 *            of the bitmap to be returned.
	 * @return Bitmap of the signature.
	 */
	public Bitmap getImage(final Bitmap.Config config) {
		final Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(),
				config);

		final Canvas canvas = new Canvas();
		canvas.setBitmap(bitmap);
		final Drawable bg = getBackground();
		if (bg != null) {
			bg.draw(canvas);
		}
		canvas.drawPath(path, paint);
		return bitmap;
	}

	/**
	 * Called when replaying history to ensure the dirty region includes all
	 * points.
	 */
	private void expandDirtyRect(final float historicalX,
			final float historicalY) {
		if (historicalX < dirtyRect.left) {
			dirtyRect.left = historicalX;
		} else if (historicalX > dirtyRect.right) {
			dirtyRect.right = historicalX;
		}
		if (historicalY < dirtyRect.top) {
			dirtyRect.top = historicalY;
		} else if (historicalY > dirtyRect.bottom) {
			dirtyRect.bottom = historicalY;
		}
	}

	/**
	 * Resets the dirty region when the motion event occurs.
	 */
	private void resetDirtyRect(final float eventX, final float eventY) {

		// The lastTouchX and lastTouchY were setKey when the ACTION_DOWN
		// motion event occurred.
		dirtyRect.left = Math.min(lastTouchX, eventX);
		dirtyRect.right = Math.max(lastTouchX, eventX);
		dirtyRect.top = Math.min(lastTouchY, eventY);
		dirtyRect.bottom = Math.max(lastTouchY, eventY);
	}
}
