package toto.net.client.http.soap;

import java.util.Map;
import java.util.Vector;

/**
 * A simple dynamic object that can be used to build soap calls without
 * implementing KvmSerializable
 * 
 * Essentially, this is what goes inside the body of a soap envelope - it is the
 * direct subelement of the body and all further subelements
 * 
 * Instead of this this class, custom classes can be used if they implement the
 * KvmSerializable interface.
 */

public class SoapObject implements toto.net.client.http.soap.SoapSerializable {
	/** The namespace of this soap object. */
	protected String namespace;
	/** The name of this soap object. */
	protected String name;
	/** The Vector of properties. */
	protected Vector properties = new Vector();
	/** The Vector of attributes. */
	protected Vector attributes = new Vector();

	/**
	 * Creates a new <code>SoapObject</code> instance.
	 *
	 * @param namespace
	 *            the namespace for the soap object
	 * @param name
	 *            the name of the soap object
	 */

	public SoapObject(final String namespace, final String name) {
		this.namespace = namespace;
		this.name = name;
	}

	/**
	 * Adds a attribute (parameter) to the object. This is essentially a sub
	 * element.
	 *
	 * @param propertyInfo
	 *            designated retainer of desired attribute
	 */
	public SoapObject addAttribute(final toto.net.client.http.soap.AttributeInfo attributeInfo) {
		attributes.addElement(attributeInfo);
		return this;
	}

	/**
	 * Adds a attribute (parameter) to the object. This is essentially a sub
	 * element.
	 *
	 * @param name
	 *            The name of the attribute
	 * @param value
	 *            the value of the attribute
	 */
	public SoapObject addAttribute(final String name, final Object value) {
		final toto.net.client.http.soap.AttributeInfo attributeInfo = new toto.net.client.http.soap.AttributeInfo();
		attributeInfo.name = name;
		attributeInfo.type = value == null ? toto.net.client.http.soap.PropertyInfo.OBJECT_CLASS : value
				.getClass();
		attributeInfo.value = value;
		return addAttribute(attributeInfo);
	}

	/**
	 * Adds a property (parameter) to the object. This is essentially a sub
	 * element.
	 *
	 * @param propertyInfo
	 *            designated retainer of desired property
	 */
	public SoapObject addProperty(final toto.net.client.http.soap.PropertyInfo propertyInfo) {
		properties.addElement(propertyInfo);
		return this;
	}

	/**
	 * Adds a property (parameter) to the object. This is essentially a sub
	 * element.
	 *
	 * @param propertyInfo
	 *            designated retainer of desired property
	 * @param value
	 *            the value of the property
	 * @deprecated property info now contains the value
	 */
	public SoapObject addProperty(final toto.net.client.http.soap.PropertyInfo propertyInfo,
			final Object value) {
		propertyInfo.setValue(value);
		addProperty(propertyInfo);
		return this;
	}

	/**
	 * Adds a property (parameter) to the object. This is essentially a sub
	 * element.
	 *
	 * @param name
	 *            The name of the property
	 * @param value
	 *            the value of the property
	 */
	public SoapObject addProperty(final String name, final Object value) {
		final toto.net.client.http.soap.PropertyInfo propertyInfo = new toto.net.client.http.soap.PropertyInfo();
		propertyInfo.name = name;
		propertyInfo.type = value == null ? toto.net.client.http.soap.PropertyInfo.OBJECT_CLASS : value
				.getClass();
		propertyInfo.value = value;
		return addProperty(propertyInfo);
	}

	public boolean equals(final Object obj) {
		if (!(obj instanceof SoapObject))
			return false;

		final SoapObject otherSoapObject = (SoapObject) obj;

		final int numProperties = properties.size();
		if (numProperties != otherSoapObject.properties.size())
			return false;
		final int numAttributes = attributes.size();
		if (numAttributes != otherSoapObject.attributes.size())
			return false;

		try {
			for (int propIndex = 0; propIndex < numProperties; propIndex++) {
				final toto.net.client.http.soap.PropertyInfo thisProp = (toto.net.client.http.soap.PropertyInfo) this.properties
						.elementAt(propIndex);
				final Object thisPropValue = thisProp.getValue();
				final Object otherPropValue = otherSoapObject
						.getProperty(thisProp.getName());
				if (!thisPropValue.equals(otherPropValue)) {
					return false;
				}
			}
			for (int attribIndex = 0; attribIndex < numAttributes; attribIndex++) {
				final toto.net.client.http.soap.AttributeInfo thisAttrib = (toto.net.client.http.soap.AttributeInfo) this.properties
						.elementAt(attribIndex);
				final Object thisAttribValue = thisAttrib.getValue();
				final Object otherAttribValue = otherSoapObject
						.getProperty(thisAttrib.getName());
				if (!thisAttribValue.equals(otherAttribValue)) {
					return false;
				}
			}
		} catch (final Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Returns a specific attribute at a certain index.
	 *
	 * @param index
	 *            the index of the desired attribute
	 * @return the value of the desired attribute
	 *
	 */
	public Object getAttribute(final int index) {
		return ((toto.net.client.http.soap.AttributeInfo) attributes.elementAt(index)).getValue();
	}

	/** Returns a property with the given name. */
	public Object getAttribute(final String name) {
		for (int i = 0; i < attributes.size(); i++) {
			if (name.equals(((toto.net.client.http.soap.AttributeInfo) attributes.elementAt(i)).getName()))
				return getAttribute(i);
		}
		throw new RuntimeException("illegal property: " + name);
	}

	/**
	 * Returns the number of attributes
	 *
	 * @return the number of attributes
	 */
	public int getAttributeCount() {
		return attributes.size();
	}

	/**
	 * Places AttributeInfo of desired attribute into a designated AttributeInfo
	 * object
	 *
	 * @param index
	 *            index of desired attribute
	 * @param propertyInfo
	 *            designated retainer of desired attribute
	 */
	public void getAttributeInfo(final int index,
			final toto.net.client.http.soap.AttributeInfo attributeInfo) {
		final toto.net.client.http.soap.AttributeInfo p = (toto.net.client.http.soap.AttributeInfo) attributes.elementAt(index);
		attributeInfo.name = p.name;
		attributeInfo.namespace = p.namespace;
		attributeInfo.flags = p.flags;
		attributeInfo.type = p.type;
		attributeInfo.elementType = p.elementType;
		attributeInfo.value = p.getValue();
	}

	public String getName() {
		return name;
	}

	public String getNamespace() {
		return namespace;
	}

	/**
	 * Returns a specific property at a certain index.
	 *
	 * @param index
	 *            the index of the desired property
	 * @return the desired property
	 */
	public Object getProperty(final int index) {
		return ((toto.net.client.http.soap.PropertyInfo) properties.elementAt(index)).getValue();
	}

	public Object getProperty(final String name) {
		for (int i = 0; i < properties.size(); i++) {
			if (name.equals(((toto.net.client.http.soap.PropertyInfo) properties.elementAt(i)).getName()))
				return getProperty(i);
		}
		throw new RuntimeException("illegal property: " + name);
	}

	/**
	 * Returns the number of properties
	 *
	 * @return the number of properties
	 */
	public int getPropertyCount() {
		return properties.size();
	}

	/**
	 * Places PropertyInfo of desired property into a designated PropertyInfo
	 * object
	 *
	 * @param index
	 *            index of desired property
	 * @param propertyInfo
	 *            designated retainer of desired property
	 * @deprecated
	 */
	public void getPropertyInfo(final int index, final Map properties,
			final toto.net.client.http.soap.PropertyInfo propertyInfo) {
		getPropertyInfo(index, propertyInfo);
	}

	/**
	 * Places PropertyInfo of desired property into a designated PropertyInfo
	 * object
	 *
	 * @param index
	 *            index of desired property
	 * @param propertyInfo
	 *            designated retainer of desired property
	 */
	public void getPropertyInfo(final int index, final toto.net.client.http.soap.PropertyInfo propertyInfo) {
		final toto.net.client.http.soap.PropertyInfo p = (toto.net.client.http.soap.PropertyInfo) properties.elementAt(index);
		propertyInfo.name = p.name;
		propertyInfo.namespace = p.namespace;
		propertyInfo.flags = p.flags;
		propertyInfo.type = p.type;
		propertyInfo.elementType = p.elementType;
	}

	/**
	 * Creates a new SoapObject based on this, allows usage of SoapObjects as
	 * templates. One application is to set the expected return type of a soap
	 * call if the server does not send explicit type information.
	 *
	 * @return a copy of this.
	 */
	public SoapObject newInstance() {
		final SoapObject o = new SoapObject(namespace, name);
		for (int propIndex = 0; propIndex < properties.size(); propIndex++) {
			final toto.net.client.http.soap.PropertyInfo propertyInfo = (toto.net.client.http.soap.PropertyInfo) properties
					.elementAt(propIndex);
			o.addProperty(propertyInfo);
		}
		for (int attribIndex = 0; attribIndex < attributes.size(); attribIndex++) {
			final toto.net.client.http.soap.AttributeInfo attributeInfo = (toto.net.client.http.soap.AttributeInfo) attributes
					.elementAt(attribIndex);
			o.addAttribute(attributeInfo);
		}
		return o;
	}

	/**
	 * Sets a specified property to a certain value.
	 *
	 * @param index
	 *            the index of the specified property
	 * @param value
	 *            the new value of the property
	 */
	public void setProperty(final int index, final Object value) {
		((toto.net.client.http.soap.PropertyInfo) properties.elementAt(index)).setValue(value);
	}

	public String toString() {
		final StringBuffer buf = new StringBuffer("" + name + "{");
		for (int i = 0; i < getPropertyCount(); i++) {
			buf.append("" + ((toto.net.client.http.soap.PropertyInfo) properties.elementAt(i)).getName()
					+ "=" + getProperty(i) + "; ");
		}
		buf.append("}");
		return buf.toString();
	}

}
