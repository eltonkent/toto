package toto.net.client.http.soap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

/**
 * Exception class encapsulating SOAP Faults
 */

public class SoapFault extends IOException {

	/** The SOAP fault code */
	public String faultcode;
	/** The SOAP fault code */
	public String faultstring;
	/** The SOAP fault code */
	public String faultactor;
	/** A KDom Node holding the details of the fault */
	public Node detail;

	/**
	 * @see Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return faultstring;
	}

	/** Fills the fault details from the given XML stream */
	public void parse(final XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, toto.net.client.http.soap.SoapEnvelope.ENV, "Fault");
		while (parser.nextTag() == XmlPullParser.START_TAG) {
			final String name = parser.getName();
			if (name.equals("detail")) {
				detail = new Node();
				detail.parse(parser);
				continue;
			} else if (name.equals("faultcode"))
				faultcode = parser.nextText();
			else if (name.equals("faultstring"))
				faultstring = parser.nextText();
			else if (name.equals("faultactor"))
				faultactor = parser.nextText();
			else
				throw new RuntimeException("unexpected tag:" + name);
			parser.require(XmlPullParser.END_TAG, null, name);
		}
		parser.require(XmlPullParser.END_TAG, toto.net.client.http.soap.SoapEnvelope.ENV, "Fault");
		parser.nextTag();
	}

	/** Returns a simple string representation of the fault */
	public String toString() {
		return "SoapFault - faultcode: '" + faultcode + "' faultstring: '"
				+ faultstring + "' faultactor: '" + faultactor + "' detail: "
				+ detail;
	}

	/** Writes the fault to the given XML stream */
	public void write(final XmlSerializer xw) throws IOException {
		xw.startTag(toto.net.client.http.soap.SoapEnvelope.ENV, "Fault");
		xw.startTag(null, "faultcode");
		xw.text("" + faultcode);
		xw.endTag(null, "faultcode");
		xw.startTag(null, "faultstring");
		xw.text("" + faultstring);
		xw.endTag(null, "faultstring");
		xw.startTag(null, "detail");
		if (detail != null)
			detail.write(xw);
		xw.endTag(null, "detail");
		xw.endTag(toto.net.client.http.soap.SoapEnvelope.ENV, "Fault");
	}
}
