package toto.net.client.http.soap;

/**
 * This class is used to store information about each property an implementation
 * of KvmSerializable exposes.
 */

public class PropertyInfo {
	public static final Class OBJECT_CLASS = new Object().getClass();
	public static final Class STRING_CLASS = "".getClass();
	public static final Class INTEGER_CLASS = new Integer(0).getClass();
	public static final Class LONG_CLASS = new Long(0).getClass();
	public static final Class BOOLEAN_CLASS = new Boolean(true).getClass();
	public static final Class VECTOR_CLASS = new java.util.Vector().getClass();
	public static final PropertyInfo OBJECT_TYPE = new PropertyInfo();
	public static final int TRANSIENT = 1;
	public static final int MULTI_REF = 2;
	public static final int REF_ONLY = 4;

	/**
	 * Name of the property
	 */
	public String name;

	/**
	 * Namespace of this property
	 */
	public String namespace;

	/**
	 * Type of property, Transient, multi_ref, Ref_only *JHS* Note, not really
	 * used that effectively
	 */
	public int flags;
	/**
	 * Type of the property/elements. Should usually be an instance of Class.
	 */
	public Object type = OBJECT_CLASS;
	/**
	 * if a property is multi-referenced, set this flag to true.
	 */
	public boolean multiRef;
	/**
	 * Element type for array properties, null if not array prop.
	 */
	public PropertyInfo elementType;
	/**
	 * The current value of this property.
	 */
	protected Object value;

	public PropertyInfo() {
	}

	public void clear() {
		type = OBJECT_CLASS;
		flags = 0;
		name = null;
		namespace = null;
	}

	/**
	 * @return Returns the elementType.
	 */
	public PropertyInfo getElementType() {
		return elementType;
	}

	/**
	 * @param elementType
	 *            The elementType to set.
	 */
	public void setElementType(final PropertyInfo elementType) {
		this.elementType = elementType;
	}

	/**
	 * @return Returns the flags.
	 */
	public int getFlags() {
		return flags;
	}

	/**
	 * @param flags
	 *            The flags to set.
	 */
	public void setFlags(final int flags) {
		this.flags = flags;
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return Returns the namespace.
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace
	 *            The namespace to set.
	 */
	public void setNamespace(final String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return Returns the type.
	 */
	public Object getType() {
		return type;
	}

	/**
	 * @param type
	 *            The type to set.
	 */
	public void setType(final Object type) {
		this.type = type;
	}

	/**
	 * @return Returns the value.
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *            The value to set.
	 */
	public void setValue(final Object value) {
		this.value = value;
	}

	/**
	 * @return Returns the multiRef.
	 */
	public boolean isMultiRef() {
		return multiRef;
	}

	/**
	 * @param multiRef The multiRef to set.
	 */
	public void setMultiRef(final boolean multiRef) {
		this.multiRef = multiRef;
	}

	/**
	 * Show the name and value.
	 * 
	 * @see Object#toString()
	 */
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		sb.append(name);
		sb.append(" : ");
		if (value != null) {
			sb.append(value);
		} else {
			sb.append("(not set)");
		}
		return sb.toString();
	}
}