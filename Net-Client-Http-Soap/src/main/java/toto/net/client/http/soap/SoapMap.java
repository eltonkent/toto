package toto.net.client.http.soap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;


/**
 * Serializes instances of hashtable to and from xml. This implementation is
 * based on the xml schema from apache-soap, namely the type 'map' in the
 * namespace 'http://xml.apache.org/xml-soap'. Other soap implementations
 * including apache (obviously) and glue are also interoperable with the schema.
 */
public class SoapMap implements SoapMarshaller {

	/** use then during registration */
	public static final String NAMESPACE = "http://xml.apache.org/xml-soap";
	/** use then during registration */
	public static final String NAME = "Map";
	/** CLDC does not support .class, so this helper is needed. */
	public static final Class HASHTABLE_CLASS = new Hashtable().getClass();
	toto.net.client.http.soap.SoapSerializationEnvelope envelope;

	public Object readInstance(final XmlPullParser parser,
			final String namespace, final String name,
			final toto.net.client.http.soap.PropertyInfo expected) throws IOException,
			XmlPullParserException {
		final Map instance = new HashMap();
		final String elementName = parser.getName();
		while (parser.nextTag() != XmlPullParser.END_TAG) {
			final SoapObject item = new ItemSoapObject(instance);
			parser.require(XmlPullParser.START_TAG, null, "item");
			parser.nextTag();
			final Object key = envelope.read(parser, item, 0, null, null,
					toto.net.client.http.soap.PropertyInfo.OBJECT_TYPE);
			parser.nextTag();
			if (key != null) {
				item.setProperty(0, key);
			}
			final Object value = envelope.read(parser, item, 1, null, null,
					PropertyInfo.OBJECT_TYPE);
			parser.nextTag();
			if (value != null) {
				item.setProperty(1, value);
			}
			parser.require(XmlPullParser.END_TAG, null, "item");
		}
		parser.require(XmlPullParser.END_TAG, null, elementName);
		return instance;
	}

	public void register(final SoapSerializationEnvelope cm) {
		envelope = cm;
		cm.addMapping(SoapMap.NAMESPACE, SoapMap.NAME, HASHTABLE_CLASS, this);
	}

	public void writeInstance(final XmlSerializer writer, final Object instance)
			throws IOException {
		final Map h = (HashMap) instance;
		final SoapObject item = new SoapObject(null, null);
		item.addProperty("key", null);
		item.addProperty("value", null);
		final Set set = h.keySet();
		for (final Object key : set) {
			writer.startTag("", "item");
			item.setProperty(0, key);
			item.setProperty(1, h.get(key));
			envelope.writeObjectBody(writer, item);
			writer.endTag("", "item");

		}
	}

	final class ItemSoapObject extends SoapObject {
		Map h;
		int resolvedIndex = -1;

		ItemSoapObject(final Map h) {
			super(null, null);
			this.h = h;
			addProperty("key", null);
			addProperty("value", null);
		}

		// 0 & 1 only valid
		public void setProperty(final int index, final Object value) {
			if (resolvedIndex == -1) {
				super.setProperty(index, value);
				resolvedIndex = index;
			} else {
				// already have a key or value
				final Object resolved = resolvedIndex == 0 ? getProperty(0)
						: getProperty(1);
				if (index == 0)
					h.put(value, resolved);
				else
					h.put(resolved, value);
			}
		}
	}
}
