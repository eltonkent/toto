package toto.net.client.http.soap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * 
 * This class extends the SoapEnvelope with Soap Serialization functionality.
 */
public class SoapSerializationEnvelope extends SoapEnvelope {
	protected static final int QNAME_TYPE = 1;
	protected static final int QNAME_NAMESPACE = 0;
	protected static final int QNAME_MARSHAL = 3;
	static final SoapMarshaller DEFAULT_MARSHAL = new DM();
	private static final String ANY_TYPE_LABEL = "anyType";
	private static final String ARRAY_MAPPING_NAME = "Array";
	private static final String NULL_LABEL = "null";
	private static final String NIL_LABEL = "nil";
	private static final String HREF_LABEL = "href";
	private static final String ID_LABEL = "id";
	private static final String ROOT_LABEL = "root";
	private static final String TYPE_LABEL = "type";
	private static final String ITEM_LABEL = "item";
	private static final String ARRAY_TYPE_LABEL = "arrayType";
	public Map properties = new HashMap();
	public boolean implicitTypes;
	/**
	 * Set this variable to true for compatibility with what seems to be the
	 * default encoding for .Net-Services. This feature is an extremely ugly
	 * hack. A much better option is to change the configuration of the
	 * .Net-Server to standard Soap Serialization!
	 */

	public boolean dotNet;
	/**
	 * Map from XML qualified names to Java classes
	 */

	protected Hashtable qNameToClass = new Hashtable();
	/**
	 * Map from Java class names to XML name and namespace pairs
	 */

	protected Hashtable classToQName = new Hashtable();
	/**
	 * Set to true to add and ID and ROOT label to the envelope. Change to false
	 * for compatibility with WSDL.
	 */
	protected boolean addAdornments = true;
	Map idMap = new HashMap();
	List multiRef; // = new Vector();

	public SoapSerializationEnvelope(final int version) {
		super(version);
		addMapping(enc, ARRAY_MAPPING_NAME, PropertyInfo.VECTOR_CLASS);
		DEFAULT_MARSHAL.register(this);
	}

	/**
	 * Defines a direct mapping from a namespace and name to a java class (and
	 * vice versa)
	 */
	public void addMapping(final String namespace, final String name,
			final Class clazz) {
		addMapping(namespace, name, clazz, null);
	}

	/**
	 * Defines a direct mapping from a namespace and name to a java class (and
	 * vice versa), using the given marshal mechanism
	 */
	public void addMapping(final String namespace, final String name,
			final Class clazz, final SoapMarshaller marshal) {
		qNameToClass.put(new SoapPrimitive(namespace, name, null),
				marshal == null ? (Object) clazz : marshal);
		classToQName.put(clazz.getName(), new Object[] { namespace, name, null,
				marshal });
	}

	/**
	 * Adds a SoapObject to the class map. During parsing, objects of the given
	 * type (namespace/name) will be mapped to corresponding copies of the given
	 * SoapObject, maintaining the structure of the template.
	 */
	public void addTemplate(final SoapObject so) {
		qNameToClass.put(new SoapPrimitive(so.namespace, so.name, null), so);
	}

	private int getIndex(final String value, final int start, final int dflt) {
		if (value == null)
			return dflt;
		return value.length() - start < 3 ? dflt : Integer.parseInt(value
				.substring(start + 1, value.length() - 1));
	}

	/**
	 * Returns a string array containing the namespace, name, id and Marshal
	 * object for the given java object. This method is used by the SoapWriter
	 * in order to map Java objects to the corresponding SOAP section five XML
	 * code.
	 */
	public Object[] getInfo(Object type, final Object instance) {
		if (type == null) {
			if (instance instanceof SoapObject
					|| instance instanceof SoapPrimitive)
				type = instance;
			else
				type = instance.getClass();
		}
		if (type instanceof SoapObject) {
			final SoapObject so = (SoapObject) type;
			return new Object[] { so.getNamespace(), so.getName(), null, null };
		}
		if (type instanceof SoapPrimitive) {
			final SoapPrimitive sp = (SoapPrimitive) type;
			return new Object[] { sp.getNamespace(), sp.getName(), null,
					DEFAULT_MARSHAL };
		}
		if ((type instanceof Class) && type != PropertyInfo.OBJECT_CLASS) {
			final Object[] tmp = (Object[]) classToQName.get(((Class) type)
					.getName());
			if (tmp != null)
				return tmp;
		}
		return new Object[] { xsd, ANY_TYPE_LABEL, null, null };
	}

	/**
	 * Response from the soap call. Pulls the object from the wrapper object and
	 * returns it.
	 * 
	 * @since 2.0.3
	 * @return response from the soap call.
	 * @throws SoapFault
	 */
	public Object getResponse() throws SoapFault {
		if (bodyIn instanceof SoapFault) {
			throw (SoapFault) bodyIn;
		}
		final SoapSerializable ks = (SoapSerializable) bodyIn;
		return ks.getPropertyCount() == 0 ? null : ks.getProperty(0);
	}

	/**
	 * @deprecated Please use the getResponse going forward
	 * @see #getResponse()
	 */
	public Object getResult() {
		final SoapSerializable ks = (SoapSerializable) bodyIn;
		return ks.getPropertyCount() == 0 ? null : ks.getProperty(0);
	}

	/**
	 * @return the addAdornments
	 */
	public boolean isAddAdornments() {
		return addAdornments;
	}

	/**
	 * @param addAdornments the addAdornments to set
	 */
	public void setAddAdornments(final boolean addAdornments) {
		this.addAdornments = addAdornments;
	}

	public void parseBody(final XmlPullParser parser) throws IOException,
			XmlPullParserException {
		bodyIn = null;
		parser.nextTag();
		if (parser.getEventType() == XmlPullParser.START_TAG
				&& parser.getNamespace().equals(env)
				&& parser.getName().equals("Fault")) {
			final SoapFault fault = new SoapFault();
			fault.parse(parser);
			bodyIn = fault;
		} else {
			while (parser.getEventType() == XmlPullParser.START_TAG) {
				final String rootAttr = parser.getAttributeValue(enc,
						ROOT_LABEL);
				final Object o = read(parser, null, -1, parser.getNamespace(),
						parser.getName(), PropertyInfo.OBJECT_TYPE);
				if ("1".equals(rootAttr) || bodyIn == null)
					bodyIn = o;
				parser.nextTag();
			}
		}
	}

	/**
	 * Builds an object from the XML stream. This method is public for usage in
	 * conjuction with Marshal subclasses. Precondition: On the start tag of the
	 * object or property, so href can be read.
	 */

	public Object read(final XmlPullParser parser, final Object owner,
			final int index, String namespace, String name,
			final PropertyInfo expected) throws IOException,
			XmlPullParserException {
		final String elementName = parser.getName();
		String href = parser.getAttributeValue(null, HREF_LABEL);
		Object obj;
		if (href != null) {
			if (owner == null)
				throw new RuntimeException("href at root level?!?");
			href = href.substring(1);
			obj = idMap.get(href);
			if (obj == null || obj instanceof FwdRef) {
				final FwdRef f = new FwdRef();
				f.next = (FwdRef) obj;
				f.obj = owner;
				f.index = index;
				idMap.put(href, f);
				obj = null;
			}
			parser.nextTag(); // start tag
			parser.require(XmlPullParser.END_TAG, null, elementName);
		} else {
			String nullAttr = parser.getAttributeValue(xsi, NIL_LABEL);
			final String id = parser.getAttributeValue(null, ID_LABEL);
			if (nullAttr == null)
				nullAttr = parser.getAttributeValue(xsi, NULL_LABEL);
			if (nullAttr != null && SoapEnvelope.stringToBoolean(nullAttr)) {
				obj = null;
				parser.nextTag();
				parser.require(XmlPullParser.END_TAG, null, elementName);
			} else {
				final String type = parser.getAttributeValue(xsi, TYPE_LABEL);
				if (type != null) {
					final int cut = type.indexOf(':');
					name = type.substring(cut + 1);
					final String prefix = cut == -1 ? "" : type.substring(0,
							cut);
					namespace = parser.getNamespace(prefix);
				} else if (name == null && namespace == null) {
					if (parser.getAttributeValue(enc, ARRAY_TYPE_LABEL) != null) {
						namespace = enc;
						name = ARRAY_MAPPING_NAME;
					} else {
						final Object[] names = getInfo(expected.type, null);
						namespace = (String) names[0];
						name = (String) names[1];
					}
				}
				// be sure to set this flag if we don't know the types.
				if (type == null) {
					implicitTypes = true;
				}
				obj = readInstance(parser, namespace, name, expected);
				if (obj == null)
					obj = readUnknown(parser, namespace, name);
			}
			// finally, care about the id....
			if (id != null) {
				final Object hlp = idMap.get(id);
				if (hlp instanceof FwdRef) {
					FwdRef f = (FwdRef) hlp;
					do {
						if (f.obj instanceof SoapSerializable)
							((SoapSerializable) f.obj)
									.setProperty(f.index, obj);
						else
							((Vector) f.obj).setElementAt(obj, f.index);
						f = f.next;
					} while (f != null);
				} else if (hlp != null)
					throw new RuntimeException("double ID");
				idMap.put(id, obj);
			}
		}

		parser.require(XmlPullParser.END_TAG, null, elementName);
		return obj;
	}

	/**
	 * Returns a new object read from the given parser. If no mapping is found,
	 * null is returned. This method is used by the SoapParser in order to
	 * convert the XML code to Java objects.
	 */
	public Object readInstance(final XmlPullParser parser,
			final String namespace, final String name,
			final PropertyInfo expected) throws IOException,
			XmlPullParserException {
		Object obj = qNameToClass.get(new SoapPrimitive(namespace, name, null));
		if (obj == null)
			return null;
		if (obj instanceof SoapMarshaller)
			return ((SoapMarshaller) obj).readInstance(parser, namespace, name,
					expected);
		else if (obj instanceof SoapObject) {
			obj = ((SoapObject) obj).newInstance();
		} else if (obj == SoapObject.class) {
			obj = new SoapObject(namespace, name);
		} else {
			try {
				obj = ((Class) obj).newInstance();
			} catch (final Exception e) {
				throw new RuntimeException(e.toString());
			}
		}
		// ok, obj is now the instance, fill it....
		if (obj instanceof SoapObject)
			readSerializable(parser, (SoapObject) obj);
		else if (obj instanceof SoapSerializable)
			readSerializable(parser, (SoapSerializable) obj);
		else if (obj instanceof Vector)
			readVector(parser, (Vector) obj, expected.elementType);
		else
			throw new RuntimeException("no deserializer for " + obj.getClass());
		return obj;
	}

	/**
	 * Read a SoapObject. This extracts any attributes and then reads the object
	 * as a KvmSerializable.
	 */
	protected void readSerializable(final XmlPullParser parser,
			final SoapObject obj) throws IOException, XmlPullParserException {
		for (int counter = 0; counter < parser.getAttributeCount(); counter++) {
			final String attributeName = parser.getAttributeName(counter);
			final String value = parser.getAttributeValue(counter);
			obj.addAttribute(attributeName, value);
		}
		readSerializable(parser, (SoapSerializable) obj);
	}

	/** Read a KvmSerializable. */
	protected void readSerializable(final XmlPullParser parser,
			final SoapSerializable obj) throws IOException,
			XmlPullParserException {
		int testIndex = -1; // inc at beg. of loop for perf. reasons
		final int propertyCount = obj.getPropertyCount();
		final PropertyInfo info = new PropertyInfo();
		while (parser.nextTag() != XmlPullParser.END_TAG) {
			final String name = parser.getName();
			int countdown = propertyCount;
			// I don't really understand what's going on in this "while(true)"
			// clause. The structure surely is wrong "while(true)" with a break
			// is
			// pretty much always because the person who wrote it couldn't
			// figure out what
			// it was really supposed to be doing.
			// So, here's a little CYA since I think the code is only broken for
			// implicitTypes
			if (!implicitTypes || !(obj instanceof SoapObject)) {
				while (true) {
					if (countdown-- == 0) {
						throw new RuntimeException("Unknown Property: " + name);
					}
					if (++testIndex >= propertyCount) {
						testIndex = 0;
					}
					obj.getPropertyInfo(testIndex, properties, info);
					if (info.namespace == null && name.equals(info.name)
							|| info.name == null && testIndex == 0
							|| name.equals(info.name)
							&& parser.getNamespace().equals(info.namespace)) {
						break;
					}
				}
				obj.setProperty(testIndex,
						read(parser, obj, testIndex, null, null, info));
			} else {
				// I can only make this work for SoapObjects - hence the check
				// above
				// I don't understand namespaces well enough to know whether it
				// is correct in the next line...
				((SoapObject) obj).addProperty(
						parser.getName(),
						read(parser, obj, obj.getPropertyCount(),
								((SoapObject) obj).getNamespace(), name,
								PropertyInfo.OBJECT_TYPE));
			}
		}
		parser.require(XmlPullParser.END_TAG, null, null);
	}

	/**
	 * If the type of the object cannot be determined, and thus no Marshal class
	 * can handle the object, this method is called. It will build either a
	 * SoapPrimitive or a SoapObject
	 *
	 * @param parser
	 * @param typeNamespace
	 * @param typeName
	 * @return unknownObject wrapped as a SoapPrimitive or SoapObject
	 * @throws IOException
	 * @throws XmlPullParserException
	 */

	protected Object readUnknown(final XmlPullParser parser,
			final String typeNamespace, final String typeName)
			throws IOException, XmlPullParserException {
		final String name = parser.getName();
		final String namespace = parser.getNamespace();
		parser.next(); // move to text, inner start tag or end tag
		Object result = null;
		String text = null;
		if (parser.getEventType() == XmlPullParser.TEXT) {
			text = parser.getText();
			result = new SoapPrimitive(typeNamespace, typeName, text);
			parser.next();
		} else if (parser.getEventType() == XmlPullParser.END_TAG) {
			result = new SoapObject(typeNamespace, typeName);
		}

		if (parser.getEventType() == XmlPullParser.START_TAG) {
			if (text != null && text.trim().length() != 0) {
				throw new RuntimeException("Malformed input: Mixed content");
			}
			final SoapObject so = new SoapObject(typeNamespace, typeName);
			while (parser.getEventType() != XmlPullParser.END_TAG) {
				so.addProperty(
						parser.getName(),
						read(parser, so, so.getPropertyCount(), null, null,
								PropertyInfo.OBJECT_TYPE));
				parser.nextTag();
			}
			result = so;
		}
		parser.require(XmlPullParser.END_TAG, namespace, name);
		return result;
	}

	protected void readVector(final XmlPullParser parser, final Vector v,
			PropertyInfo elementType) throws IOException,
			XmlPullParserException {
		String namespace = null;
		String name = null;
		int size = v.size();
		boolean dynamic = true;
		final String type = parser.getAttributeValue(enc, ARRAY_TYPE_LABEL);
		if (type != null) {
			final int cut0 = type.indexOf(':');
			final int cut1 = type.indexOf("[", cut0);
			name = type.substring(cut0 + 1, cut1);
			final String prefix = cut0 == -1 ? "" : type.substring(0, cut0);
			namespace = parser.getNamespace(prefix);
			size = getIndex(type, cut1, -1);
			if (size != -1) {
				v.setSize(size);
				dynamic = false;
			}
		}
		if (elementType == null)
			elementType = PropertyInfo.OBJECT_TYPE;
		parser.nextTag();
		int position = getIndex(parser.getAttributeValue(enc, "offset"), 0, 0);
		while (parser.getEventType() != XmlPullParser.END_TAG) {
			// handle position
			position = getIndex(parser.getAttributeValue(enc, "position"), 0,
					position);
			if (dynamic && position >= size) {
				size = position + 1;
				v.setSize(size);
			}
			// implicit handling of position exceeding specified size
			v.setElementAt(
					read(parser, v, position, namespace, name, elementType),
					position);
			position++;
			parser.nextTag();
		}
		parser.require(XmlPullParser.END_TAG, null, null);
	}

	/**
	 * Serializes the request object to the given XmlSerliazer object
	 * 
	 * @param writer
	 *            XmlSerializer object to write the body into.
	 */
	public void writeBody(final XmlSerializer writer) throws IOException {
		multiRef = new Vector();
		multiRef.add(bodyOut);
		final Object[] qName = getInfo(null, bodyOut);
		writer.startTag((dotNet) ? "" : (String) qName[QNAME_NAMESPACE],
				(String) qName[QNAME_TYPE]);
		if (dotNet) {
			writer.attribute(null, "xmlns", (String) qName[QNAME_NAMESPACE]);
		}
		if (addAdornments) {
			writer.attribute(null, ID_LABEL, qName[2] == null ? ("o" + 0)
					: (String) qName[2]);
			writer.attribute(enc, ROOT_LABEL, "1");
		}
		writeElement(writer, bodyOut, null, qName[QNAME_MARSHAL]);
		writer.endTag((dotNet) ? "" : (String) qName[QNAME_NAMESPACE],
				(String) qName[QNAME_TYPE]);

	}

	private void writeElement(final XmlSerializer writer, final Object element,
			final PropertyInfo type, final Object marshal) throws IOException {
		if (marshal != null)
			((SoapMarshaller) marshal).writeInstance(writer, element);
		else if (element instanceof SoapObject)
			writeObjectBody(writer, (SoapObject) element);
		else if (element instanceof SoapSerializable)
			writeObjectBody(writer, (SoapSerializable) element);
		else if (element instanceof Vector)
			writeVectorBody(writer, (Vector) element, type.elementType);
		else
			throw new RuntimeException("Cannot serialize: " + element);
	}

	/**
	 * Writes the body of an SoapObject. This method write the attributes and
	 * then calls "writeObjectBody (writer, (KvmSerializable)obj);"
	 */
	public void writeObjectBody(final XmlSerializer writer, final SoapObject obj)
			throws IOException {
		final SoapObject soapObject = obj;
		for (int counter = 0; counter < soapObject.getAttributeCount(); counter++) {
			final AttributeInfo attributeInfo = new AttributeInfo();
			soapObject.getAttributeInfo(counter, attributeInfo);
			writer.attribute(attributeInfo.getNamespace(), attributeInfo
					.getName(), attributeInfo.getValue().toString());
		}
		writeObjectBody(writer, (SoapSerializable) obj);
	}

	/**
	 * Writes the body of an KvmSerializable object. This method is public for
	 * access from Marshal subclasses.
	 */
	public void writeObjectBody(final XmlSerializer writer,
			final SoapSerializable obj) throws IOException {
		final PropertyInfo info = new PropertyInfo();
		final int cnt = obj.getPropertyCount();
		for (int i = 0; i < cnt; i++) {
			obj.getPropertyInfo(i, properties, info);
			if ((info.flags & PropertyInfo.TRANSIENT) == 0) {
				writer.startTag(info.namespace, info.name);
				writeProperty(writer, obj.getProperty(i), info);
				writer.endTag(info.namespace, info.name);
			}
		}
	}

	protected void writeProperty(final XmlSerializer writer, final Object obj,
			final PropertyInfo type) throws IOException {
		if (obj == null) {
			writer.attribute(xsi, version >= VER12 ? NIL_LABEL : NULL_LABEL,
					"true");
			return;
		}
		final Object[] qName = getInfo(null, obj);
		if (type.multiRef || qName[2] != null) {
			int i = multiRef.indexOf(obj);
			if (i == -1) {
				i = multiRef.size();
				multiRef.add(obj);
			}
			writer.attribute(null, HREF_LABEL, qName[2] == null ? ("#o" + i)
					: "#" + qName[2]);
		} else {
			if (!implicitTypes || obj.getClass() != type.type) {
				final String prefix = writer.getPrefix(
						(String) qName[QNAME_NAMESPACE], true);
				writer.attribute(xsi, TYPE_LABEL, prefix + ":"
						+ qName[QNAME_TYPE]);
			}
			writeElement(writer, obj, type, qName[QNAME_MARSHAL]);
		}
	}

	protected void writeVectorBody(final XmlSerializer writer,
			final Vector vector, PropertyInfo elementType) throws IOException {
		if (elementType == null)
			elementType = PropertyInfo.OBJECT_TYPE;
		final int cnt = vector.size();
		final Object[] arrType = getInfo(elementType.type, null);
		// I think that this needs an implicitTypes check, but don't have a
		// failure case for that
		writer.attribute(enc, ARRAY_TYPE_LABEL,
				writer.getPrefix((String) arrType[0], false) + ":" + arrType[1]
						+ "[" + cnt + "]");
		boolean skipped = false;
		for (int i = 0; i < cnt; i++) {
			if (vector.elementAt(i) == null)
				skipped = true;
			else {
				writer.startTag(null, ITEM_LABEL);
				if (skipped) {
					writer.attribute(enc, "position", "[" + i + "]");
					skipped = false;
				}
				writeProperty(writer, vector.elementAt(i), elementType);
				writer.endTag(null, ITEM_LABEL);
			}
		}
	}

}
