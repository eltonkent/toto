package toto.net.client.http.soap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

import toto.io.codec.Base64;

/**
 * Base64 (de)serializer
 */
class MarshalBase64 implements SoapMarshaller {
	public static Class BYTE_ARRAY_CLASS = new byte[0].getClass();

	public Object readInstance(final XmlPullParser parser,
			final String namespace, final String name,
			final PropertyInfo expected) throws IOException,
			XmlPullParserException {
		return Base64.decode(parser.nextText());
	}

	public void register(final SoapSerializationEnvelope cm) {
		cm.addMapping(cm.xsd, "base64Binary", MarshalBase64.BYTE_ARRAY_CLASS,
				this);
		cm.addMapping(SoapEnvelope.ENC, "base64",
				MarshalBase64.BYTE_ARRAY_CLASS, this);
	}

	public void writeInstance(final XmlSerializer writer, final Object obj)
			throws IOException {
		writer.text(Base64.encode((byte[]) obj));
	}
}
