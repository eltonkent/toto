package toto.net.client.http.soap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;


/**
 * This class is not public, so save a few bytes by using a short class name (DM
 * stands for DefaultMarshal)...
 */
class DM implements SoapMarshaller {

	public Object readInstance(final XmlPullParser parser,
			final String namespace, final String name,
			final PropertyInfo expected) throws IOException,
			XmlPullParserException {
		final String text = parser.nextText();
		switch (name.charAt(0)) {
		case 's':
			return text;
		case 'i':
			return new Integer(Integer.parseInt(text));
		case 'l':
			return new Long(Long.parseLong(text));
		case 'b':
			return new Boolean(SoapEnvelope.stringToBoolean(text));
		default:
			throw new RuntimeException();
		}
	}

	public void register(final SoapSerializationEnvelope cm) {
		cm.addMapping(cm.xsd, "int", PropertyInfo.INTEGER_CLASS, this);
		cm.addMapping(cm.xsd, "long", PropertyInfo.LONG_CLASS, this);
		cm.addMapping(cm.xsd, "string", PropertyInfo.STRING_CLASS, this);
		cm.addMapping(cm.xsd, "boolean", PropertyInfo.BOOLEAN_CLASS, this);
	}

	public void writeInstance(final XmlSerializer writer, final Object instance)
			throws IOException {
		writer.text(instance.toString());
	}
}
