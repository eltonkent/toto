package toto.net.client.http.soap;

import java.util.Map;

/**
 * Provides get and set methods for properties. Can be used to replace
 * reflection (to some extend) for "serialization-aware" classes.
 */

public interface SoapSerializable {

	/**
	 * Returns the property at a specified index (for serialization)
	 * 
	 * @param index
	 *            the specified index
	 * @return the serialized property
	 */
	Object getProperty(int index);

	/**
	 * @return the number of serializable properties
	 */
	int getPropertyCount();

	/**
	 * Fills the given property info record.
	 * 
	 * @param index
	 *            the index to be queried
	 * @param properties
	 *            information about the (de)serializer. Not frequently used.
	 * @param info
	 *            The return parameter, to be filled with information about the
	 *            property with the given index.
	 */
	void getPropertyInfo(int index, Map properties, PropertyInfo info);

	/**
	 * Sets the property with the given index to the given value.
	 * 
	 * @param index
	 *            the index to be set
	 * @param value
	 *            the value of the property
	 */
	void setProperty(int index, Object value);

}
