package toto.net.client.http.soap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;


/**
 * A SOAP envelope, holding head and body objects. The SoapSerializationEnvelope
 * provides support for the SOAP Serialization format specification and simple
 * object serialization.
 * 
 * @see SoapSerializationEnvelope
 */
public class SoapEnvelope {

	/** SOAP Version 1.0 constant */
	public static final int VER10 = 100;
	/** SOAP Version 1.1 constant */
	public static final int VER11 = 110;
	/** SOAP Version 1.2 constant */
	public static final int VER12 = 120;
	public static final String ENV2001 = "http://www.w3.org/2001/12/soap-envelope";
	public static final String ENC2001 = "http://www.w3.org/2001/12/soap-encoding";
	/** Namespace constant: http://schemas.xmlsoap.org/soap/envelope/ */
	public static final String ENV = "http://schemas.xmlsoap.org/soap/envelope/";
	/** Namespace constant: http://schemas.xmlsoap.org/soap/encoding/ */
	public static final String ENC = "http://schemas.xmlsoap.org/soap/encoding/";
	/** Namespace constant: http://www.w3.org/2001/XMLSchema */
	public static final String XSD = "http://www.w3.org/2001/XMLSchema";
	/** Namespace constant: http://www.w3.org/2001/XMLSchema */
	public static final String XSI = "http://www.w3.org/2001/XMLSchema-instance";
	/** Namespace constant: http://www.w3.org/1999/XMLSchema */
	public static final String XSD1999 = "http://www.w3.org/1999/XMLSchema";
	/** Namespace constant: http://www.w3.org/1999/XMLSchema */
	public static final String XSI1999 = "http://www.w3.org/1999/XMLSchema-instance";
	/**
	 * Incoming header elements
	 */
	public Element[] headerIn;
	/**
	 * Outgoing header elements
	 */
	public Element[] headerOut;
	public String encodingStyle;
	/**
	 * The SOAP version, set by the constructor
	 */
	public int version;
	/** Envelope namespace, set by the constructor */
	public String env;
	/** Encoding namespace, set by the constructor */
	public String enc;
	/** Xml Schema instance namespace, set by the constructor */
	public String xsi;
	/** Xml Schema data namespace, set by the constructor */
	public String xsd;
	/**
	 * The body object received with this envelope. Will be an KDom Node for
	 * literal encoding. For SOAP Serialization, please refer to
	 * SoapSerializationEnvelope.
	 */
	protected Object bodyIn;
	/**
	 * The body object to be sent with this envelope. Must be a KDom Node
	 * modelling the remote call including all parameters for literal encoding.
	 * For SOAP Serialization, please refer to SoapSerializationEnvelope
	 */
	protected Object bodyOut;

	/**
	 * Initializes a SOAP Envelope. The version parameter must be set to one of
	 * VER10, VER11 or VER12
	 */
	public SoapEnvelope(final int version) {
		this.version = version;
		if (version == SoapEnvelope.VER10) {
			xsi = SoapEnvelope.XSI1999;
			xsd = SoapEnvelope.XSD1999;
		} else {
			xsi = SoapEnvelope.XSI;
			xsd = SoapEnvelope.XSD;
		}
		if (version < SoapEnvelope.VER12) {
			enc = SoapEnvelope.ENC;
			env = SoapEnvelope.ENV;
		} else {
			enc = SoapEnvelope.ENC2001;
			env = SoapEnvelope.ENV2001;
		}
	}

	/**
	 * Returns true for the string values "1" and "true", ignoring upper/lower
	 * case and whitespace, false otherwise.
	 */
	public static boolean stringToBoolean(String booleanAsString) {
		if (booleanAsString == null)
			return false;
		booleanAsString = booleanAsString.trim().toLowerCase();
		return (booleanAsString.equals("1") || booleanAsString.equals("true"));
	}

	/** Parses the SOAP envelope from the given parser */
	public void parse(final XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, env, "Envelope");
		encodingStyle = parser.getAttributeValue(env, "encodingStyle");
		parser.nextTag();
		if (parser.getEventType() == XmlPullParser.START_TAG
				&& parser.getNamespace().equals(env)
				&& parser.getName().equals("Header")) {
			parseHeader(parser);
			parser.require(XmlPullParser.END_TAG, env, "Header");
			parser.nextTag();
		}
		parser.require(XmlPullParser.START_TAG, env, "Body");
		encodingStyle = parser.getAttributeValue(env, "encodingStyle");
		parseBody(parser);
		parser.require(XmlPullParser.END_TAG, env, "Body");
		parser.nextTag();
		parser.require(XmlPullParser.END_TAG, env, "Envelope");
	}

	public void parseBody(final XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.nextTag();
		// insert fault generation code here
		if (parser.getEventType() == XmlPullParser.START_TAG
				&& parser.getNamespace().equals(env)
				&& parser.getName().equals("Fault")) {
			final SoapFault fault = new SoapFault();
			fault.parse(parser);
			bodyIn = fault;
		} else {
			final Node node = (bodyIn instanceof Node) ? (Node) bodyIn
					: new Node();
			node.parse(parser);
			bodyIn = node;
		}
	}

	public void parseHeader(final XmlPullParser parser) throws IOException,
			XmlPullParserException {
		// consume start header
		parser.nextTag();
		// look at all header entries
		final Node headers = new Node();
		headers.parse(parser);
		int count = 0;
		for (int i = 0; i < headers.getChildCount(); i++) {
			final Element child = headers.getElement(i);
			if (child != null)
				count++;
		}
		headerIn = new Element[count];
		count = 0;
		for (int i = 0; i < headers.getChildCount(); i++) {
			final Element child = headers.getElement(i);
			if (child != null)
				headerIn[count++] = child;
		}
	}

	/**
	 * Assigns the object to the envelope as the outbound message for the soap
	 * call.
	 * 
	 * @param soapObject
	 *            the object to send in the soap call.
	 */
	public void setOutputSoapObject(final Object soapObject) {
		bodyOut = soapObject;
	}

	/**
	 * Writes the complete envelope including header and body elements to the
	 * given XML writer.
	 */
	public void write(final XmlSerializer writer) throws IOException {
		writer.setPrefix("i", xsi);
		writer.setPrefix("d", xsd);
		writer.setPrefix("c", enc);
		writer.setPrefix("v", env);
		writer.startTag(env, "Envelope");
		writer.startTag(env, "Header");
		writeHeader(writer);
		writer.endTag(env, "Header");
		writer.startTag(env, "Body");
		writeBody(writer);
		writer.endTag(env, "Body");
		writer.endTag(env, "Envelope");
	}

	/**
	 * Writes the SOAP body stored in the object variable bodyIn, Overwrite this
	 * method for customized writing of the soap message body.
	 */
	public void writeBody(final XmlSerializer writer) throws IOException {
		if (encodingStyle != null)
			writer.attribute(env, "encodingStyle", encodingStyle);
		((Node) bodyOut).write(writer);
	}

	/**
	 * Writes the header elements contained in headerOut
	 */
	public void writeHeader(final XmlSerializer writer) throws IOException {
		if (headerOut != null) {
			for (int i = 0; i < headerOut.length; i++) {
				headerOut[i].write(writer);
			}
		}
	}

}
