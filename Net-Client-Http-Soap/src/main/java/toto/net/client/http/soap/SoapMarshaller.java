package toto.net.client.http.soap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;


/**
 * Interface for custom (de)serialization.
 */

public interface SoapMarshaller {

	/**
	 * This methods reads an instance from the given parser. For implementation,
	 * please note that the start and and tag must be consumed. This is not
	 * symmetric to writeInstance, but otherwise it would not be possible to
	 * access the attributes of the start tag here.
	 * 
	 * @param parser
	 *            the xml parser
	 * @param namespace
	 *            the namespace.
	 * @return the object read from the xml stream.
	 */
	public Object readInstance(XmlPullParser parser, String namespace,
							   String name, PropertyInfo expected) throws IOException,
			XmlPullParserException;

	/**
	 * Register this Marshal with Envelope
	 * 
	 * @param envelope
	 *            the soap serialization envelope.
	 */
	public void register(SoapSerializationEnvelope envelope);

	/**
	 * Write the instance to the given XmlSerializer. In contrast to
	 * readInstance, it is not neccessary to care about the surrounding start
	 * and end tags. Additional attributes must be writen before anything else
	 * is written.
	 * 
	 * @param writer
	 *            the xml serializer.
	 * @param instance
	 *            the instance to write to the writer.
	 */
	public void writeInstance(XmlSerializer writer, Object instance)
			throws IOException;
}
