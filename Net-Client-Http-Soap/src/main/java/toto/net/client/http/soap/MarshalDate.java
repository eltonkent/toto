package toto.net.client.http.soap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.Date;

import toto.text.datetime.IsoDate;

/**
 * Marshal class for Dates.
 */
class MarshalDate implements SoapMarshaller {
	static Class DATE_CLASS = new Date().getClass();

	public Object readInstance(final XmlPullParser parser,
			final String namespace, final String name,
			final PropertyInfo expected) throws IOException,
			XmlPullParserException {
		return IsoDate.stringToDate(parser.nextText(), IsoDate.DATE_TIME);
	}

	public void register(final SoapSerializationEnvelope cm) {
		cm.addMapping(cm.xsd, "dateTime", MarshalDate.DATE_CLASS, this);
	}

	public void writeInstance(final XmlSerializer writer, final Object obj)
			throws IOException {
		writer.text(IsoDate.dateToString((Date) obj, IsoDate.DATE_TIME));
	}

}
