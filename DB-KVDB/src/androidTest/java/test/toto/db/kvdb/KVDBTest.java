package test.toto.db.kvdb;

import java.io.File;
import java.io.IOException;

import test.toto.ToToTestCase;
import toto.db.kvdb.KVDb;
import toto.io.file.SDCardUtils;

public class KVDBTest extends ToToTestCase {

		public String getTestStorageLocation(){
		return SDCardUtils.getDirectory().toString()+File.separator+"ToToTest";
	}
	String path = getTestStorageLocation()+File.separator+"LevelDB/";

	public KVDBTest() {
		File file = new File(path);
		if (!file.exists()) {
			file.mkdir();
		}
	}
	
	public void testInsertAndRead(){
		KVDb db;
		try {
			db = new KVDb(new File(path));
			byte[] key="This is a Key".getBytes();
			byte[] value="this is a value".getBytes();
			db.put(key, value);
			value=db.get(key);
			assertEquals("this is a value", new String(value));
			db.delete(key);
			value=db.get(key);
			assertNull(value);
			db.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
