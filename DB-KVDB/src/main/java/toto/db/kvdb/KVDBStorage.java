package toto.db.kvdb;

import java.io.File;
import java.io.IOException;

import toto.cache.SerializedStorageConverter;
import toto.cache.storage.impl.DiskStorage;

/**
 * KVDB based Storage provider for {@link toto.cache.TCache}.
 * 
 * @author Mobifluence Interactive
 * 
 */
public class KVDBStorage<K, V> extends DiskStorage<K, V> {

	private final KVDb db;

	public KVDBStorage(final SerializedStorageConverter<K> keyConverter,
			final SerializedStorageConverter<V> valueConverter,
			final File cacheLocation) throws IOException {
		super(keyConverter, valueConverter, cacheLocation);
		db = new KVDb(cacheLocation);
	}

	@Override
	protected boolean save(final byte[] key, final byte[] value) {
		db.put(key, value);
		return true;
	}

	@Override
	protected boolean delete(final byte[] key) {
		db.delete(key);
		return true;
	}

	@Override
	protected byte[] fetch(final byte[] key) {
		return db.get(key);
	}

	@Override
	protected void clear() {
		db.clear();
	}

	@Override
	protected void destroy() {
		db.close();
		clear();
		KVDb.destroy(cacheDirectory);
	}

	@Override
	protected void close() {
		db.close();
	}

	@Override
	protected int size() {
		return db.getSize();
	}

}
