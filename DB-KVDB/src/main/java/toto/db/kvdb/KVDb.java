package toto.db.kvdb;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

import com.mi.toto.ToTo;
import toto.io.SerializationUtils;

/**
 * Key-Value DB instance
 * 
 * @author Mobifluence Interactive
 * 
 */
public class KVDb extends NativeObject {
	/**
	 * Represents an instance of the current KVDb state.
	 */
	public abstract static class Snapshot extends NativeObject {
		Snapshot(final long ptr) {
			super(ptr);
		}
	}

	private final File mPath;
	int numberOfEntries;
	String counterFile = "kvdb.counter";
	private boolean mDestroyOnClose = false;

	/**
	 * Create or open a level DB database.
	 * <p>
	 * The database is opened once an instance is created.
	 * </p>
	 * 
	 * @param path
	 *            database path
	 * @throws IOException
	 * @throws IllegalArgumentException
	 *             if <code>path</code> is not a directory.
	 * @throws NullPointerException
	 *             if <code>path</code> is null
	 * @throws IOException
	 *             if creating the key-value database at given path fails.
	 * @see #open()
	 */
	public KVDb(final File path) throws IOException {
		super();

		if (path == null) {
			throw new NullPointerException();
		}
		if (!path.isDirectory()) {
			throw new IllegalArgumentException("Path is not a directory");
		}
		if (!path.canWrite()) {
			throw new IllegalArgumentException("Path is not writable");
		}
		mPath = path;
		open();
	}

	/**
	 * Get the number of entries in KVDB
	 * 
	 * @return
	 */
	public int getSize() {
		return numberOfEntries;
	}

	@Override
	public void finalize() throws Throwable {
		close();
		super.finalize();
	}

	public void close() {
		super.close();
		writeCounterFile();
	}

	private void writeCounterFile() {
		final File file = new File(mPath, counterFile);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			final DataOutputStream dos = new DataOutputStream(
					new FileOutputStream(file));
			dos.writeInt(numberOfEntries);
			dos.close();
		} catch (final IOException e) {
			ToTo.logException(e);
		}
	}

	/**
	 * Opens up the key value database and retrieves its pointer from the native
	 * space.
	 */
	public void open() throws IOException {
		final File file = new File(mPath, counterFile);
		if (file.exists()) {
			final DataInputStream dis = new DataInputStream(
					new FileInputStream(file));
			numberOfEntries = dis.readInt();
			dis.close();
		}
		mPtr = nativeOpen(mPath.getAbsolutePath());
	}

	@Override
	protected void closeNativeObject(final long ptr) {
		nativeClose(ptr);
		if (mDestroyOnClose) {
			destroy(mPath);
		}
	}

	/**
	 * Save a key-value pair represented as a byte array.
	 * 
	 * @param key
	 * @param value
	 * @throws java.lang.NullPointerException
	 *             if key or value is null.
	 */
	public void put(final byte[] key, final byte[] value) {
		assertOpen("Database is closed");
		if (key == null) {
			throw new NullPointerException("key");
		}
		if (value == null) {
			throw new NullPointerException("value");
		}

		nativePut(mPtr, key, value);
		numberOfEntries++;
	}

	/**
	 * Write a Serializable key-value
	 * 
	 * @param key
	 * @param value
	 */
	public void put(final Serializable key, final Serializable value) {
		put(SerializationUtils.serialize(key),
				SerializationUtils.serialize(value));
	}

	/**
	 * Read a serializable from the key-value database
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public Object get(final Serializable key) throws IOException,
			ClassNotFoundException {
		return SerializationUtils
				.deserialize(SerializationUtils.serialize(key));
	}

	/**
	 * Get a value associated with the given key. If it exists.
	 * 
	 * @param key
	 * @return
	 */
	public byte[] get(final byte[] key) {
		return get(null, key);
	}

	/**
	 * 
	 * @param snapshot
	 * @param key
	 * @return
	 */
	public byte[] get(final Snapshot snapshot, final byte[] key) {
		assertOpen("Database is closed");
		if (key == null) {
			throw new NullPointerException();
		}

		return nativeGet(mPtr, snapshot != null ? snapshot.getPtr() : 0, key);
	}

	/**
	 * Delete the value represented by the given key.
	 * 
	 * @param key
	 * @throws java.lang.NullPointerException
	 *             if the given key is null.
	 */
	public void delete(final byte[] key) {
		assertOpen("Database is closed");
		if (key == null) {
			throw new NullPointerException();
		}

		nativeDelete(mPtr, key);
		numberOfEntries--;
	}

	/**
	 * Apply batch updates to the database.
	 * 
	 * @param batch
	 */
	public void write(final WriteBatch batch) {
		assertOpen("Database is closed");
		if (batch == null) {
			throw new NullPointerException();
		}
		nativeWrite(mPtr, batch.getPtr());
		numberOfEntries += batch.getEntryCount();
	}

	/**
	 * Returns an iterator over the contents of the database.
	 * 
	 * @return
	 */
	public Iterator iterator() {
		return iterator(null);
	}

	public Iterator iterator(final Snapshot snapshot) {
		assertOpen("Database is closed");

		ref();

		if (snapshot != null) {
			snapshot.ref();
		}

		return new Iterator(nativeIterator(mPtr,
				snapshot != null ? snapshot.getPtr() : 0)) {
			@Override
			protected void closeNativeObject(final long ptr) {
				super.closeNativeObject(ptr);
				if (snapshot != null) {
					snapshot.unref();
				}

				toto.db.kvdb.KVDb.this.unref();
			}
		};
	}

	public Snapshot getSnapshot() {
		assertOpen("Database is closed");
		ref();
		return new Snapshot(nativeGetSnapshot(mPtr)) {
			protected void closeNativeObject(final long ptr) {
				nativeReleaseSnapshot(KVDb.this.getPtr(), getPtr());
				KVDb.this.unref();
			}
		};
	}

	public void destroy() {
		mDestroyOnClose = true;
		if (getPtr() == 0) {
			destroy(mPath);
		}
		numberOfEntries = 0;
	}

	public void clear() {
		numberOfEntries = 0;
		if (getPtr() == 0) {
			destroy(mPath);
		}
		try {
			open();
		} catch (final IOException e) {
			ToTo.logException(e);
		}
	}

	/**
	 * Destroy's the database and deletes the database folder
	 * 
	 * @param path
	 */
	public static void destroy(final File path) {
		nativeDestroy(path.getAbsolutePath());
	}

	/**
	 * Repair KVDb
	 * <p>
	 * If KVDB cannot be opened, you may attempt to call this method to
	 * resurrect as much of the contents of the database as possible. Some data
	 * may be lost, so be careful when calling this function on a database that
	 * contains important information.
	 * 
	 * </p>
	 * 
	 * @param path
	 */
	public static void repair(final File path) {
		nativeRepair(path.getAbsolutePath());
	}

	private static native long nativeOpen(String dbpath);

	private static native void nativeClose(long dbPtr);

	private static native void nativePut(long dbPtr, byte[] key, byte[] value);

	private static native byte[] nativeGet(long dbPtr, long snapshotPtr,
			byte[] key);

	private static native void nativeDelete(long dbPtr, byte[] key);

	private static native void nativeWrite(long dbPtr, long batchPtr);

	private static native void nativeDestroy(String dbpath);

	private static native void nativeRepair(String dbpath);

	private static native long nativeIterator(long dbPtr, long snapshotPtr);

	private static native long nativeGetSnapshot(long dbPtr);

	private static native void nativeReleaseSnapshot(long dbPtr,
			long snapshotPtr);

	static {
		ToTo.loadLib("DB_KVDb");

	}
}
