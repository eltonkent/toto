package toto.db.kvdb;

import toto.db.ToToDBException;

public class KVDbException extends ToToDBException {
	private static final long serialVersionUID = 2903013251786326801L;

	public KVDbException() {
	}

	public KVDbException(final String error) {
		super(error);
	}

	public KVDbException(final String error, final Throwable cause) {
		super(error, cause);
	}
}
