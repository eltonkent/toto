package toto.db.kvdb;

public class KVDbCorruptException extends KVDbException {
	private static final long serialVersionUID = -2110293580518875321L;

	public KVDbCorruptException() {
	}

	public KVDbCorruptException(final String error) {
		super(error);
	}
}
