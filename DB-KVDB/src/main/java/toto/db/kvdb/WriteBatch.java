package toto.db.kvdb;

import java.nio.ByteBuffer;

public class WriteBatch extends NativeObject {
	public WriteBatch() {
		super(nativeCreate());
	}

	private int entryCount;

	final int getEntryCount() {
		return entryCount;
	}

	@Override
	protected void closeNativeObject(final long ptr) {
		nativeDestroy(ptr);
	}

	public void n_delete(final ByteBuffer key) {
		assertOpen("WriteBatch is closed");
		if (key == null) {
			throw new NullPointerException("key");
		}

		nativeDelete(mPtr, key);
		entryCount--;
	}

	public void n_put(final ByteBuffer key, final ByteBuffer value) {
		assertOpen("WriteBatch is closed");
		if (key == null) {
			throw new NullPointerException("key");
		}
		if (value == null) {
			throw new NullPointerException("value");
		}

		nativePut(mPtr, key, value);
		entryCount++;
	}

	public void n_clear() {
		assertOpen("WriteBatch is closed");
		nativeClear(mPtr);
		entryCount = 0;
	}

	private static native long nativeCreate();

	private static native void nativeDestroy(long ptr);

	private static native void nativeDelete(long ptr, ByteBuffer key);

	private static native void nativePut(long ptr, ByteBuffer key,
			ByteBuffer val);

	private static native void nativeClear(long ptr);
}
