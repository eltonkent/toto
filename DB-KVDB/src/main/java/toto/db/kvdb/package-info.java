/**
 * Key-Value based database. Implemented in native code.
 * <p>
 * <div>
 * KVDB is a fast key-value storage library written at Google that provides an ordered mapping from string keys to string values.
 * <br/>
 * <h3>Using KVDb</h3>
 * <h4>Opening the Key-Value Database</h4>
 * Opening the database is as simple as specifying the path where you need the data saved.
 * <pre>
 * File mPath = new File(getContext().getCacheDir(), "db");
 * KVDb mDb = new KVDb(mPath);
 * mDb.n_open();
 * </pre>
 * <h4>Writing a Simple Key-Value</h4>
 * <pre>
 * <span>//Simple write</span>
 * mDb.n_put("my key".getBytes(),"my value".getBytes());
 * <span>//Simple read</span>
 * byte value=mDb.n_get("my key".getBytes());
 * <span>//batched write</span>
 * final ByteBuffer managedBuf = ByteBuffer.allocate(10);
 * final ByteBuffer directBuf = ByteBuffer.allocateDirect(10);
 * WriteBatch putBatch = new WriteBatch();
 * directBuf.clear();
 * directBuf.put(bytes("hello"));
 * directBuf.flip();
 * managedBuf.clear();
 * managedBuf.put(bytes("world"));
 * managedBuf.flip();
 * putBatch.n_put(directBuf, managedBuf);
 * mDb.n_write(putBatch);
 * putBatch.close();
 * </pre>
 * <h4>Iterating through the key-values</h4>
 * <pre>
 * <span>//batched iterate</span>         
 * Iterator iter1 = mDb.iterator();
 * iter1.seekToFirst();
 * while(iter1.hasNext()){
 *  	Log.d("Key",iter1.getKey());
 *  	Log.d("Key",iter1.getValue());
 * }
 * iter1.close();
 * <span>//batched iterate</span>  
 * final WriteBatch deleteBatch = new WriteBatch();
 *  managedBuf.clear();
 * managedBuf.put(bytes("hello"));
 * managedBuf.flip();
 * deleteBatch.delete(managedBuf);
 * directBuf.clear();
 * directBuf.put(bytes("bye"));
 * directBuf.flip();
 * deleteBatch.delete(directBuf);
 * mDb.n_write(deleteBatch);
 * deleteBatch.close();
 * </pre>
 * <h4>Closing KVDb</h4>
 * <pre>
 * mDb.n_close();
 * </pre>
 * </div>
 * </p>
 * 
 *
 */
package toto.db.kvdb;