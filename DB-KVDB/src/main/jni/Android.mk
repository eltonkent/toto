LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := DB_KVDb
LOCAL_C_INCLUDES := $(LOCAL_PATH)/leveldb-GIT/include
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := -DLEVELDB_PLATFORM_ANDROID -std=gnu++0x
LOCAL_SRC_FILES := KVDb.cc Iterator.cc WriteBatch.cc leveldbjni.cc
LOCAL_STATIC_LIBRARIES +=  leveldb
LOCAL_LDLIBS +=  -llog -ldl
include $(BUILD_SHARED_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE := leveldb
LOCAL_CFLAGS := -D_REENTRANT -DOS_ANDROID -DLEVELDB_PLATFORM_POSIX -DNDEBUG -DSNAPPY
LOCAL_CPP_EXTENSION := .cc
LOCAL_C_INCLUDES := $(LOCAL_PATH)/leveldb-GIT $(LOCAL_PATH)/leveldb-GIT/include $(LOCAL_PATH)/snappy-1.1.0
LOCAL_SRC_FILES := leveldb-GIT/db/builder.cc leveldb-GIT/db/c.cc leveldb-GIT/db/db_impl.cc leveldb-GIT/db/db_iter.cc leveldb-GIT/db/dbformat.cc leveldb-GIT/db/filename.cc leveldb-GIT/db/log_reader.cc leveldb-GIT/db/log_writer.cc leveldb-GIT/db/memtable.cc leveldb-GIT/db/repair.cc leveldb-GIT/db/table_cache.cc leveldb-GIT/db/version_edit.cc leveldb-GIT/db/version_set.cc leveldb-GIT/db/write_batch.cc leveldb-GIT/table/block.cc leveldb-GIT/table/block_builder.cc leveldb-GIT/table/filter_block.cc leveldb-GIT/table/format.cc leveldb-GIT/table/iterator.cc leveldb-GIT/table/merger.cc leveldb-GIT/table/table.cc leveldb-GIT/table/table_builder.cc leveldb-GIT/table/two_level_iterator.cc leveldb-GIT/util/arena.cc leveldb-GIT/util/bloom.cc leveldb-GIT/util/cache.cc leveldb-GIT/util/coding.cc leveldb-GIT/util/comparator.cc leveldb-GIT/util/crc32c.cc leveldb-GIT/util/env.cc leveldb-GIT/util/env_posix.cc leveldb-GIT/util/filter_policy.cc leveldb-GIT/util/hash.cc leveldb-GIT/util/histogram.cc leveldb-GIT/util/logging.cc leveldb-GIT/util/options.cc leveldb-GIT/util/status.cc leveldb-GIT/port/port_posix.cc
LOCAL_PRELINK_MODULE := false
LOCAL_SHARED_LIBRARIES := Zip_Snappy
include $(BUILD_STATIC_LIBRARY)
