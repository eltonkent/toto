# RAGE Android Make File
# compile for all supported architectures
APP_ABI := all
APP_PLATFORM := android-8
APP_STL := gnustl_static
#gnustl_static
# stlport_static
# gnustl_static
APP_CPPFLAGS += -fexceptions -frtti
APP_OPTIM := release
# the current architectures supported are armeabi, armeabi-v7a, x86, mips,