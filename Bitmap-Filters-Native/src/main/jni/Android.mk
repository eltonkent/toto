LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
#LOCAL_LDLIBS += -ljnigraphics
LOCAL_CPPFLAGS += -ffunction-sections -fdata-sections -fvisibility=hidden
LOCAL_CFLAGS += -ffunction-sections -fdata-sections  

#remove duplicate code.
ifeq ($(TARGET_ARCH),mips)
  LOCAL_LDFLAGS += -Wl,--gc-sections
else
  LOCAL_LDFLAGS += -Wl,--gc-sections
endif


LOCAL_LDLIBS +=  -llog 
LOCAL_MODULE    := Bitmap_Filters
LOCAL_C_INCLUDES := $(LOCAL_PATH)/src
LOCAL_SRC_FILES := 	NativeFilters.cpp \
				   	src/AverageSmoothFilter.cpp \
				   	src/ContrastFilter.cpp \
				   	src/BrightFilter.cpp \
					src/GaussianBlurFilter.cpp \
					src/HDRFilter.cpp \
					src/SepiaFilter.cpp \
					src/GrayscaleFilter.cpp \
					src/InvertFilter.cpp \
					src/SoftGlowFilter.cpp \
					src/LightFilter.cpp \
					src/LomoAddBlackRound.cpp \
					src/NeonFilter.cpp \
					src/OilFilter.cpp \
					src/SketchFilter.cpp \
					src/TvFilter.cpp \
					src/SharpenFilter.cpp \
					src/ScaleBIFilter.cpp \
					src/ReliefFilter.cpp \
					src/PixelateFilter.cpp \
					src/BlockFilter.cpp \
					src/GammaCorrectionFilter.cpp \
					src/MotionBlurFilter.cpp \
					src/TransparencyFilter.cpp \
					src/TemperatureFilter.cpp \
					src/BrightContrastFilter.cpp \
					src/ColorTranslator.cpp \
					src/HueSaturationFilter.cpp \
					src/GothamFilter.cpp \
					src/blur.cpp \
	
include $(BUILD_SHARED_LIBRARY)
