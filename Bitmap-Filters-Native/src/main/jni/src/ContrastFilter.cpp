ContrastFilter::ContrastFilter(int *_pixels, int x, int y, int fw, int fh,
		int _width, int _height, ContrastOptions options) :
		ImageFilter(_pixels, x, y, fw, fh, _width, _height), contrast(
				options.contrast) {

}
ContrastFilter::ContrastFilter(int *_pixels, int x, int y, int fw, int fh,
		int _width, int _height, double contrast) :
		ImageFilter(_pixels, x, y, fw, fh, _width, _height), contrast(
				contrast) {

}

int* ContrastFilter::procImage() {

	int position, r, g, b;
	for (int y = filterY; y < filterHeight; y++) {
		for (int x = filterX; x < filterWidth; x++) {
			position = (y * width) + x;
			Color color(pixels[position]);
			r = 128 + (color.R() - 128) * contrast;
			g = 128 + (color.G() - 128) * contrast;
			b = 128 + (color.B() - 128) * contrast;

			r = min(255, max(0, r));
			g = min(255, max(0, g));
			b = min(255, max(0, b));

			pixels[position] = RGB2Color(r, g, b);

		}
	}
	return pixels;
}

