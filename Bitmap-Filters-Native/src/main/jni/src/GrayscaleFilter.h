
#ifndef GRAYSCALEFILTER_H_
#define GRAYSCALEFILTER_H_

#include "Util.h"
#include "ColorGetter.h"
#include "ImageFilter.h"

typedef struct _grayScaleOptions {
	_grayScaleOptions(int s) {
		this->saturation = s;
	}
	int saturation;
} GrayScaleOptions;

class GrayScaleFilter : public ImageFilter {
public:
	GrayScaleFilter(int *_pixels,int x,int y, int fw,int fh, int _width, int _height, GrayScaleOptions options);
	int* procImage();
private:
	int saturation;
};


#endif /* GRAYSCALEFILTER_H_ */
