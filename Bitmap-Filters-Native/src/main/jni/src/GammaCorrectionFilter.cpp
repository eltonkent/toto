GammaCorrectionFilter::GammaCorrectionFilter(int *pixels, int x, int y, int fw,
		int fh, int width, int height) :
		ImageFilter(pixels, x, y, fw, fh, width, height), gamma(1) {
	initGammaTable();
}

GammaCorrectionFilter::GammaCorrectionFilter(int *pixels, int x, int y, int fw,
		int fh, int width, int height, GammaCorrectionOptions options) :
		ImageFilter(pixels, x, y, fw, fh, width, height), gamma(options.gamma) {
	initGammaTable();
}

GammaCorrectionFilter::GammaCorrectionFilter(int *pixels, int x, int y, int fw,
		int fh, int width, int height, double _gamma) :
		ImageFilter(pixels, x, y, fw, fh, width, height), gamma(_gamma) { // normally gamma is 1/2.2=0.45
	initGammaTable();
}

void GammaCorrectionFilter::initGammaTable() {
	double inverseGamma = 1.0 / gamma;
	for (int i = 0; i < GAMMA_TABLE_SIZE; i++) {
		gammaTable[i] = pow((double) (i / COLOR_UPPER_BOUND),
				inverseGamma) * COLOR_UPPER_BOUND;
	}
}

int* GammaCorrectionFilter::procImage() {
	int position,x,y;
	for ( y = filterY; y < filterHeight; y++) {
		for ( x = filterX; x < filterWidth; x++) {
			position = (y * width) + x;
			Color color(pixels[position]);
			int r = gammaTable[color.R()];
			int g = gammaTable[color.G()];
			int b = gammaTable[color.B()];
			pixels[position] = RGB2Color(r, g, b);
		}
	}
	return pixels;
}

