SepiaFilter::SepiaFilter(int *_pixels, int x, int y, int fw, int fh, int _width,
		int _height, SepiaOptions options) :
		ImageFilter(_pixels, x, y, fw, fh, _width, _height), depth(
				options.depth) {

}

int* SepiaFilter::procImage() {

	int pixel = 0;
	int position,x,y,gry,a,r,g,b;

	for (int y = filterY; y < filterHeight; y++) {
		for (int x = filterX; x < filterWidth; x++) {
			position = (y * width) + x;
			pixel = pixels[position];
			a = (pixel >> 24) & 0xff;
			r = (pixel >> 16) & 0xff;
			g = (pixel >> 8) & 0xff;
			b = pixel & 0xff;

			gry = (r + g + b) / 3;
			r = g = b = gry;
			r = r + (depth * 2);
			g = g + depth;
			if (r > 255) {
				r = 255;
			}
			if (g > 255) {
				g = 255;
			}
			pixels[position] = (a & 0xff) << 24 | (r & 0xff) << 16
					| (g & 0xff) << 8 | (b & 0xff) << 0;

		}
	}
	return pixels;
}

