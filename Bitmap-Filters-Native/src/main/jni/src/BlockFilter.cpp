BlockFilter::BlockFilter(int *pixels, int x, int y, int fw, int fh, int width,
		int height) :
		ImageFilter(pixels, x, y, fw, fh, width, height), threshold(100) {

}

int* BlockFilter::procImage() {
	int position,x,y;
	for ( y = filterY; y < filterHeight; y++) {
		for (x = filterX; x < filterWidth; x++) {
			position = (y * width) + x;
			Color color(pixels[position]);
			int gray = color.grayScale();
			if (gray >= threshold) {
				pixels[position] = RGB2Color(255, 255, 255);
			} else {
				pixels[position] = RGB2Color(0, 0, 0);
			}
		}
	}
	return pixels;
}

