BrightFilter::BrightFilter(int *_pixels, int x, int y, int fw, int fh,
		int _width, int _height, BrightOptions options) :
		ImageFilter(_pixels, x, y, fw, fh, _width, _height), brightness(
				options.brightness) {

}

BrightFilter::BrightFilter(int *_pixels, int x, int y, int fw, int fh,
		int _width, int _height, double brightness) :
		ImageFilter(_pixels, x, y, fw, fh, _width, _height), brightness(
				brightness) {

}

int* BrightFilter::procImage() {
	int pixel = 0;
		int position, r, g, b,argb;
		for (int y = filterY; y < filterHeight; y++) {
			for (int x = filterX; x < filterWidth; x++) {
				position = (y * width) + x;
				Color color(pixels[position]);
						 r = color.R() + brightness;
						 g = color.G() + brightness;
						 b = color.B() + brightness;

						r = min(255, max(0, r));
						g = min(255, max(0, g));
						b = min(255, max(0, b));

						pixels[position] = RGB2Color(r, g, b);
			}
		}
		return pixels;
}

