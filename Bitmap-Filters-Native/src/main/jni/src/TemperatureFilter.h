#ifndef TEMPERATUREFILTER_H_
#define TEMPERATUREFILTER_H_

#include "Util.h"
#include "ColorGetter.h"
#include "ImageFilter.h"

typedef struct _temperatureOptions {
	_temperatureOptions(int s) {
		this->temperature = s;
	}
	int temperature;
} TemperatureOptions;

class TemperatureFilter: public ImageFilter {
public:
	TemperatureFilter(int *_pixels, int x, int y, int fw, int fh, int _width,
			int _height, TemperatureOptions options);
	int* procImage();
private:
	int temperature;

};

#endif /* TEMPERATUREFILTER_H_ */
