ScaleBIFilter::ScaleBIFilter(int *_pixels, int x,int y, int fw,int fh, int _width, int _height,
		ScaleBIOptions options) :
		ImageFilter(_pixels,x,y,fw,fh, _width, _height), newWidth(options.newWidth), newHeight(
				options.newHeight) {

}

int* ScaleBIFilter::procImage() {
//	int* newBitmapPixels = new int[newWidth * newHeight];
//	    // position of the top left pixel of the 4 pixels to use interpolation on
//	    int xTopLeft, yTopLeft;
//	    int x, y, lastTopLefty;
//	    float xRatio = (float) newWidth / (float) width, yratio =
//		    (float) newHeight / (float) height;
//	    // Y color ratio to use on left and right pixels for interpolation
//	    float ycRatio2 = 0, ycRatio1 = 0;
//	    // pixel target in the src
//	    float xt, yt;
//	    // X color ratio to use on left and right pixels for interpolation
//	    float xcRatio2 = 0, xcratio1 = 0;
//	    ARGB rgbTopLeft, rgbTopRight, rgbBottomLeft, rgbBottomRight, rgbTopMiddle,
//		    rgbBottomMiddle, result;
//	    for (x = 0; x < newWidth; ++x)
//		{
//		xTopLeft = (int) (xt = x / xRatio);
//		// when meeting the most right edge, move left a little
//		if (xTopLeft >= width - 1)
//		    xTopLeft--;
//		if (xt <= xTopLeft + 1)
//		    {
//		    // we are between the left and right pixel
//		    xcratio1 = xt - xTopLeft;
//		    // color ratio in favor of the right pixel color
//		    xcRatio2 = 1 - xcratio1;
//		    }
//		for (y = 0, lastTopLefty = -30000; y < newHeight; ++y)
//		    {
//		    yTopLeft = (int) (yt = y / yratio);
//		    // when meeting the most bottom edge, move up a little
//		    if (yTopLeft >= height - 1)
//			--yTopLeft;
//		    if (lastTopLefty == yTopLeft - 1)
//			{
//			// we went down only one rectangle
//			rgbTopLeft = rgbBottomLeft;
//			rgbTopRight = rgbBottomRight;
//			rgbTopMiddle = rgbBottomMiddle;
//			//rgbBottomLeft=startingImageData[xTopLeft][yTopLeft+1];
//			convertIntToArgb(
//				pixels[((yTopLeft + 1) * width) + xTopLeft],
//				&rgbBottomLeft);
//			//rgbBottomRight=startingImageData[xTopLeft+1][yTopLeft+1];
//			convertIntToArgb(
//				pixels[((yTopLeft + 1) * width)
//					+ (xTopLeft + 1)], &rgbBottomRight);
//			rgbBottomMiddle.alpha = rgbBottomLeft.alpha * xcRatio2
//				+ rgbBottomRight.alpha * xcratio1;
//			rgbBottomMiddle.red = rgbBottomLeft.red * xcRatio2
//				+ rgbBottomRight.red * xcratio1;
//			rgbBottomMiddle.green = rgbBottomLeft.green * xcRatio2
//				+ rgbBottomRight.green * xcratio1;
//			rgbBottomMiddle.blue = rgbBottomLeft.blue * xcRatio2
//				+ rgbBottomRight.blue * xcratio1;
//			}
//		    else if (lastTopLefty != yTopLeft)
//			{
//			// we went to a totally different rectangle (happens in every loop start,and might happen more when making the picture smaller)
//			//rgbTopLeft=startingImageData[xTopLeft][yTopLeft];
//			convertIntToArgb(pixels[(yTopLeft * width) + xTopLeft],
//				&rgbTopLeft);
//			//rgbTopRight=startingImageData[xTopLeft+1][yTopLeft];
//			convertIntToArgb(
//				pixels[((yTopLeft + 1) * width) + xTopLeft],
//				&rgbTopRight);
//			rgbTopMiddle.alpha = rgbTopLeft.alpha * xcRatio2
//				+ rgbTopRight.alpha * xcratio1;
//			rgbTopMiddle.red = rgbTopLeft.red * xcRatio2
//				+ rgbTopRight.red * xcratio1;
//			rgbTopMiddle.green = rgbTopLeft.green * xcRatio2
//				+ rgbTopRight.green * xcratio1;
//			rgbTopMiddle.blue = rgbTopLeft.blue * xcRatio2
//				+ rgbTopRight.blue * xcratio1;
//			//rgbBottomLeft=startingImageData[xTopLeft][yTopLeft+1];
//			convertIntToArgb(
//				pixels[((yTopLeft + 1) * width) + xTopLeft],
//				&rgbBottomLeft);
//			//rgbBottomRight=startingImageData[xTopLeft+1][yTopLeft+1];
//			convertIntToArgb(
//				pixels[((yTopLeft + 1) * width)
//					+ (xTopLeft + 1)], &rgbBottomRight);
//			rgbBottomMiddle.alpha = rgbBottomLeft.alpha * xcRatio2
//				+ rgbBottomRight.alpha * xcratio1;
//			rgbBottomMiddle.red = rgbBottomLeft.red * xcRatio2
//				+ rgbBottomRight.red * xcratio1;
//			rgbBottomMiddle.green = rgbBottomLeft.green * xcRatio2
//				+ rgbBottomRight.green * xcratio1;
//			rgbBottomMiddle.blue = rgbBottomLeft.blue * xcRatio2
//				+ rgbBottomRight.blue * xcratio1;
//			}
//		    lastTopLefty = yTopLeft;
//		    if (yt <= yTopLeft + 1)
//			{
//			// color ratio in favor of the bottom pixel color
//			ycRatio1 = yt - yTopLeft;
//			ycRatio2 = 1 - ycRatio1;
//			}
//		    // prepared all pixels to look at, so finally set the new pixel data
//		    result.alpha = rgbTopMiddle.alpha * ycRatio2
//			    + rgbBottomMiddle.alpha * ycRatio1;
//		    result.blue = rgbTopMiddle.blue * ycRatio2
//			    + rgbBottomMiddle.blue * ycRatio1;
//		    result.red = rgbTopMiddle.red * ycRatio2
//			    + rgbBottomMiddle.red * ycRatio1;
//		    result.green = rgbTopMiddle.green * ycRatio2
//			    + rgbBottomMiddle.green * ycRatio1;
//		    newBitmapPixels[(y * newWidth) + x] = convertArgbToInt(result);
//		    }
//		}
//		return newBitmapPixels;


	int* newBitmapPixels = new int[newWidth * newHeight];
//	int x2, y2;
//	int whereToPut = 0;
//	for (int y = 0; y < newHeight; ++y) {
//		for (int x = 0; x < newWidth; ++x) {


//			x2 = x * width / newWidth;
//			if (x2 < 0)
//				x2 = 0;
//			else if (x2 >= width)
//				x2 = width - 1;
//			y2 = y * height / newHeight;
//			if (y2 < 0)
//				y2 = 0;
//			else if (y2 >= height)
//				y2 = height - 1;


//			newBitmapPixels[whereToPut++] = pixels[(y2 * width) + x2];
//		}
//	}
	return newBitmapPixels;
}
//
//int ScaleBIFilter::convertArgbToInt(ARGB argb) {
//	return (argb.alpha) | (argb.red << 24) | (argb.green << 16)
//			| (argb.blue << 8);
//}
//
//void ScaleBIFilter::convertIntToArgb(int pixel, ARGB* argb) {
//	argb->red = ((pixel >> 24) & 0xff);
//	argb->green = ((pixel >> 16) & 0xff);
//	argb->blue = ((pixel >> 8) & 0xff);
//	argb->alpha = (pixel & 0xff);
//}
