ReliefFilter::ReliefFilter(int *pixels, int x,int y, int fw,int fh, int width, int height):
	ImageFilter(pixels,x,y,fw,fh, width, height),
	colorCompensation(100) {

}


int* ReliefFilter::procImage() {
	SharpenFilter *sharpenFilter = new SharpenFilter(pixels,filterX,filterY,filterWidth,filterHeight, width, height);
	pixels = sharpenFilter->highBoostSharpen();

	Color preColor(pixels[0]);



	for (int i = filterY; i < filterWidth; i++) {
		for (int j = filterY; j < filterHeight; j++) {
			Color currentColor(pixels[j * width + i]);
			int r = currentColor.R() - preColor.R() + colorCompensation;
			int g = currentColor.G() - preColor.G() + colorCompensation;
			int b = currentColor.B() - preColor.B() + colorCompensation;
			r = min(255, max(r, 0));
			g = min(255, max(g, 0));
			b = min(255, max(b, 0));

			pixels[j * width + i] = ARGB2Color(currentColor.alpha(), r, g, b);
			preColor = currentColor;
		}
	}

	delete sharpenFilter;
	return pixels;
}


