
#ifndef BRIGHTNESSFILTER_H_
#define BRIGHTNESSFILTER_H_

#include "Util.h"
#include "ColorGetter.h"
#include "ImageFilter.h"

typedef struct _contrastOptions {
	_contrastOptions(double s) {
		this->contrast = s;
	}
	double contrast;
}ContrastOptions;

class ContrastFilter : public ImageFilter {
public:
	ContrastFilter(int *_pixels,int x,int y, int fw,int fh, int _width, int _height, ContrastOptions options);
	ContrastFilter(int *_pixels,int x,int y, int fw,int fh, int _width, int _height, double contrast);
	int* procImage();
private:
	double contrast;
};


#endif /* BRIGHTNESSFILTER_H_ */
