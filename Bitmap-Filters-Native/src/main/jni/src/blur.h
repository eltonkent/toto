#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

// Detects whether a given luminance matrix is blurred or not.
// The input matrix size if width * height. 1 is returned when
// input image is blurred along with blur confidence and extent
// returned through output value blur and extent.
int IsBlurred(const uint8* const luminance, const int width, const int height,
              float* const blur, float* const extent);

#ifdef __cplusplus
}
#endif

