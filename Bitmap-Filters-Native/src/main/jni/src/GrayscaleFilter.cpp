GrayScaleFilter::GrayScaleFilter(int *_pixels,int x,int y, int fw,int fh, int _width, int _height,
		GrayScaleOptions options) :
		ImageFilter(_pixels,x,y,fw,fh, _width, _height), saturation(options.saturation) {

}

int* GrayScaleFilter::procImage() {
	int alpha, red, green, blue;
	int output_red, output_green, output_blue;

	const int RW = 306; // 0.299 * 1024
	const int RG = 601; // 0.587 * 1024
	const int RB = 117; // 0.114 * 1024

	const int a = (1024 - saturation) * RW + saturation * 1024;
	const int b = (1024 - saturation) * RW;
	const int c = (1024 - saturation) * RW;
	const int d = (1024 - saturation) * RG;
	const int e = (1024 - saturation) * RG + saturation * 1024;
	const int f = (1024 - saturation) * RG;
	const int g = (1024 - saturation) * RB;
	const int h = (1024 - saturation) * RB;
	const int i = (1024 - saturation) * RB + saturation * 1024;

	int pixel = 0;
	int position;

	for (int y = filterY; y < filterHeight; y++) {
		for (int x = filterX; x < filterWidth; x++) {
			position = (y * width) + x;
			pixel = pixels[position];
			alpha = (0xFF000000 & pixel);
			red = (0x00FF & (pixel >> 16));
			green = (0x0000FF & (pixel >> 8));
			blue = pixel & (0x000000FF);
			output_red = ((a * red + d * green + g * blue) >> 4) & 0x00FF0000;
			output_green = ((b * red + e * green + h * blue) >> 12)
					& 0x0000FF00;
			output_blue = (c * red + f * green + i * blue) >> 20;
			pixels[position] = alpha | output_red | output_green | output_blue;
		}
	}
	return pixels;
}

