AverageSmoothFilter::AverageSmoothFilter(int *_pixels,int x,int y, int fw,int fh, int _width, int _height, AverageSmoothOptions options) :
	ImageFilter(_pixels,x,y,fw,fh, _width, _height),
	maskSize(options.maskSize) {
	// TODO Auto-generated constructor stub

}

AverageSmoothFilter::AverageSmoothFilter(int* pixels,int x,int y, int fw,int fh, int width, int height):
	ImageFilter(pixels,x,y,fw,fh, width, height) {
	maskSize = 5;
}

int* AverageSmoothFilter::procImage() {
	int sumR = 0;
	int sumG = 0;
	int sumB = 0;
	int div = maskSize * maskSize;
	int halfMaskSize = maskSize / 2;



	for (int row = halfMaskSize; row < filterHeight - halfMaskSize; row++) {
		for (int col = halfMaskSize; col < filterWidth - halfMaskSize; col++) {
			sumR = sumG = sumB = 0;
			for (int m = -halfMaskSize; m <= halfMaskSize; m++) {
				for (int n = -halfMaskSize; n <= halfMaskSize; n++) {
					Color color(pixels[(row + m) * width + col + n]);
					sumR += color.R();
					sumG += color.G();
					sumB += color.B();
				}
			}
			pixels[row * width + col] = RGB2Color(sumR / div, sumG / div, sumB / div);
		}
	}

	return this->pixels;
}

