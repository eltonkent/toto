TvFilter::TvFilter(int *pixels, int x,int y, int fw,int fh, int width, int height) :
	ImageFilter(pixels,x,y,fw,fh, width, height) {
	gap = 4;
}

int* TvFilter::procImage() {
	int r, g, b;



	for (int x = filterX; x < filterWidth; x++) {
		for (int y = filterY; y < filterHeight; y += gap) {
			r = g = b = 0;

			for (int w = 0; w < 4; w++) {
				if (y + w < filterHeight) {
					Color color(pixels[(y + w) * filterWidth + x]);
					r += color.R() / gap;
					g += color.G() / gap;
					b += color.B() / gap;
				}
			}
			r = min(255, max(0, r));
			g = min(255, max(0, g));
			b = min(255, min(0, b));

			for (int w = 0; w < gap; w++) {
				if (y + w < filterHeight) {
					if (w == 0) {
						pixels[(y + w) * filterWidth + x] = RGB2Color(r, 0, 0);
					}
					if (w == 1) {
						pixels[(y + w) * filterWidth + x] = RGB2Color(0, g, 0);
					}
					if (w == 2) {
						pixels[(y + w) * filterWidth + x] = RGB2Color(0, 0, b);
					}
//					if (w == 3) {
//						pixels[(y + w) * width + x] = RGB2Color(r, 0, 0);
//					}
				}
			}
		}
	}
	return pixels;
}


