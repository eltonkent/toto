#ifndef BRIGHTFILTER_H_
#define BRIGHTFILTER_H_

#include "Util.h"
#include "ColorGetter.h"
#include "ImageFilter.h"

typedef struct _brightOptions {
	_brightOptions(double s) {
		this->brightness = s;
	}
	double brightness;
}BrightOptions;

class BrightFilter : public ImageFilter {
public:
	BrightFilter(int *_pixels,int x,int y, int fw,int fh, int _width, int _height, BrightOptions options);
	BrightFilter(int *_pixels,int x,int y, int fw,int fh, int _width, int _height, double brightness);
	int* procImage();
private:
	double brightness;
};


#endif /* BRIGHTFILTER_H_ */
