#ifndef TRANSPARENCYFILTER_H_
#define TRANSPARENCYFILTER_H_

#include "Util.h"
#include "ColorGetter.h"
#include "ImageFilter.h"

typedef struct _scaleBIOptions {
	_scaleBIOptions(int nw, int nh) {
		this->newWidth = nw;
		this->newHeight = nh;
	}
	int newWidth;
	int newHeight;
} ScaleBIOptions;

typedef struct {
	uint8_t alpha, red, green, blue;
} ARGB;


class ScaleBIFilter: public ImageFilter {
public:
	ScaleBIFilter(int *_pixels, int x,int y, int fw,int fh, int _width, int _height,
			ScaleBIOptions options);
	int* procImage();
//	void convertIntToArgb(int pixel, ARGB* argb);
//	int convertArgbToInt(ARGB argb);
private:
	int newWidth;
	int newHeight;
};

#endif /* TRANSPARENCY_h_ */
