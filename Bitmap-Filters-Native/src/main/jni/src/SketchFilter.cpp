SketchFilter::SketchFilter(int *pixels, int x,int y, int fw,int fh, int width, int height):
	ImageFilter(pixels,x,y,fw,fh, width, height) {
	this->threshold = 7;
}

int* SketchFilter::procImage() {
	changeImageToGray(pixels, width, height);

	int *originPixels = new int[width * height];
	memcpy(originPixels, pixels, width * height * sizeof(int));

	int threshold = 7;




	for (int i = filterY+1; i < filterHeight - 1; i++) {
		for (int j = filterX+1; j < filterWidth - 1; j++) {
			Color centerColor(originPixels[i * width + j]);
			int centerGray = centerColor.R();

			Color rightBottomColor(originPixels[(i + 1) * width + j + 1]);
			int rightBottomGray = rightBottomColor.R();
			if (abs(centerGray - rightBottomGray) >= threshold) {
				pixels[i * width + j] = RGB2Color(0, 0, 0); // black
			} else {
				pixels[i * width + j] = RGB2Color(255, 255, 255); // white
			}
		}
	}

	delete[] originPixels;

	return pixels;
}

