
#ifndef INVERTFILTER_H_
#define INVERTFILTER_H_

#include "Util.h"
#include "ColorGetter.h"
#include "ImageFilter.h"


class InvertFilter : public ImageFilter {
public:
	InvertFilter(int *_pixels,int x,int y, int fw,int fh, int _width, int _height);
	int* procImage();
};

#endif /* INVERTFILTER_H_ */
