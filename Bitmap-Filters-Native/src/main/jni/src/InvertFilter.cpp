InvertFilter::InvertFilter(int *_pixels, int x, int y, int fw, int fh,
		int _width, int _height) :
		ImageFilter(_pixels, x, y, fw, fh, _width, _height) {

}

int* InvertFilter::procImage() {

	int pixel = 0;
	int position, a, r, g, b;

	for (int y = filterY; y < filterHeight; y++) {
		for (int x = filterX; x < filterWidth; x++) {
			position = (y * width) + x;
			pixel = pixels[position];

			a = (pixel >> 24) & 0xff;
			r = (pixel >> 16) & 0xff;
			g = (pixel >> 8) & 0xff;
			b = pixel & 0xff;

			r = 255 - r;
			g = 255 - g;
			b = 255 - b;
			pixels[position] = ARGB2Color(a, r, g, b);
		}
	}
	return pixels;
}

