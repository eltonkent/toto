
#ifndef SEPIAFILTER_H_
#define SEPIAFILTER_H_

#include "Util.h"
#include "ColorGetter.h"
#include "ImageFilter.h"

typedef struct _sepiaOptions {
	_sepiaOptions(int s) {
		this->depth = s;
	}
	int depth;
} SepiaOptions;

class SepiaFilter : public ImageFilter {
public:
	SepiaFilter(int *_pixels,int x,int y, int fw,int fh, int _width, int _height, SepiaOptions options);
	int* procImage();
private:
	int depth;
};


#endif /* SEPIAFILTER_H_ */
