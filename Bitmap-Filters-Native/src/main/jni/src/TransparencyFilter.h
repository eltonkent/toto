
#ifndef TRANSPARENCYFILTER_H_
#define TRANSPARENCYFILTER_H_

#include "Util.h"
#include "ColorGetter.h"
#include "ImageFilter.h"


typedef struct _transparencyOptions {
	_transparencyOptions(int level){
		this->level=level;
	}
	int level;
} TransparencyOptions;



class TransparencyFilter : public ImageFilter {
public:
	TransparencyFilter(int *_pixels, int x,int y, int fw,int fh, int _width, int _height, TransparencyOptions options);
	int* procImage();
private:
	int level;
};


#endif /* TRANSPARENCY_H_ */
