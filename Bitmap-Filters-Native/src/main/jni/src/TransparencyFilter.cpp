TransparencyFilter::TransparencyFilter(int *_pixels, int x,int y, int fw,int fh, int _width, int _height,
		TransparencyOptions options) :
		ImageFilter(_pixels,x,y,fw,fh, _width, _height), level(options.level) {

}

int* TransparencyFilter::procImage() {
	int pixel = 0;
	int position;



	for (int y = filterY; y < filterHeight; y++) {
		for (int x =filterX; x < filterWidth; x++) {
			position = (y * width) + x;
			pixel = pixels[position];
			pixels[position] = (pixel & 0x00ffffff) | level;
		}
	}
	return pixels;
}

