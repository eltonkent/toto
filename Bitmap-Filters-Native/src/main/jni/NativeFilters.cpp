#include "BrightFilter.h"
#include "TransparencyFilter.h"
#include "NeonFilter.h"
#include "OilFilter.h"
#include "SepiaFilter.h"
#include "InvertFilter.h"
#include "TvFilter.h"
#include "ContrastFilter.h"
#include "LomoAddBlackRound.h"
#include "SoftGlowFilter.h"
#include "SketchFilter.h"
#include "AverageSmoothFilter.h"
#include "GaussianBlurFilter.h"
#include "HDRFilter.h"
#include "LightFilter.h"
#include "SharpenFilter.h"
#include "ReliefFilter.h"
#include "PixelateFilter.h"
#include "BlockFilter.h"
#include "MotionBlurFilter.h"
#include "GothamFilter.h"
#include "GammaCorrectionFilter.h"
#include "GrayscaleFilter.h"
#include "types.h"
#include "blur.h"


jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_lightFilter(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jint centerX, jint centerY, jint radius) {

	LightFilterOptions options(centerX, centerY, radius);
	jintArray result =
			PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, LightFilter, options);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_gammaCorrection(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jdouble gamma) {

	GammaCorrectionOptions options(gamma);
	jintArray result =
			PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, GammaCorrectionFilter, options);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_lomoAddBlckRound(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jdouble roundRadius) {

	BlackRoundOptions options(roundRadius);
	jintArray result =
			PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, LomoAddBlackRound, options);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_neonFilter(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jint r, jint g, jint b) {

	NeonFilterOptions options(r, g, b);
	jintArray result =
			PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, NeonFilter, options);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_oilFilter(JNIEnv *env,
		jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height) {

	jintArray result =
			PROC_IMAGE_WITHOUT_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, OilFilter);
	return result;
}


jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_invertFilter(JNIEnv *env,
		jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height) {

	jintArray result =
			PROC_IMAGE_WITHOUT_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, InvertFilter);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_tvFilter(JNIEnv *env,
		jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height) {
	jintArray result =
			PROC_IMAGE_WITHOUT_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, TvFilter);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_averageSmooth(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jint maskSize) {

	AverageSmoothOptions options(maskSize);
	jintArray result =
			PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, AverageSmoothFilter, options);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_hdrFilter(JNIEnv *env,
		jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height) {

	jintArray result =
			PROC_IMAGE_WITHOUT_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, HDRFilter);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_discreteGaussianBlur(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jdouble sigma) {

	GaussianBlurOptions options(sigma);
	jintArray result =
			PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, GaussianBlurFilter, options);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_softGlow(JNIEnv *env,
		jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jdouble blurSigma) {

	SoftGlowOptions options(blurSigma);
	jintArray result =
			PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, SoftGlowFilter, options);

	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_sketchFilter(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height) {
	jintArray result =
			PROC_IMAGE_WITHOUT_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, SketchFilter);

	return result;
}

jintArray JNICALL
Java_toto_graphics_bitmap_filters_nativ_NativeFilters_sharpenFilter(
		JNIEnv *env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height) {
	jintArray result =
			PROC_IMAGE_WITHOUT_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, SharpenFilter);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_reliefFilter(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height) {
	jintArray result =
			PROC_IMAGE_WITHOUT_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, ReliefFilter);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_pxelateFilter(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jint pixelSize) {
	PixelateOptions options(pixelSize);
	jintArray result =
			PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, PixelateFilter, options);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_blockFilter(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height) {
	jintArray result =
			PROC_IMAGE_WITHOUT_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, BlockFilter);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_motionBlurFilter(
		JNIEnv* env, jclass, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jint xSpeed, jint ySpeed) {
	MotionBlurFilterOpitons options(xSpeed, ySpeed);
	jintArray result =
			PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, MotionBlurFilter, options);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_gothamFilter(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height) {
	jintArray result =
			PROC_IMAGE_WITHOUT_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, GothamFilter);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_transparencyFilter(
		JNIEnv* env, jclass object, jintArray pixels,jint x,jint y,jint filterWidth,jint filterHeight, jint width, jint height,
		jint level) {

	TransparencyOptions options(level);
 	jintArray result =PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, TransparencyFilter, options);
	return result;
}

jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_grayScaleFilter(
		JNIEnv* env, jclass object, jintArray pixels, jint x,jint y,jint filterWidth,jint filterHeight,jint width, jint height,
		jint saturation) {
	GrayScaleOptions options(saturation);
	jintArray result =PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, GrayScaleFilter, options);
	return result;
}


jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_contrastFilter(
		JNIEnv* env, jclass object, jintArray pixels, jint x,jint y,jint filterWidth,jint filterHeight,jint width, jint height,
		jdouble contrast) {
	ContrastOptions options(contrast);
	jintArray result =PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, ContrastFilter, options);
	return result;
}


jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_brightFilter(
		JNIEnv* env, jclass object, jintArray pixels, jint x,jint y,jint filterWidth,jint filterHeight,jint width, jint height,
		jdouble bright) {
	BrightOptions opts(bright);
	jintArray result =PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, BrightFilter, opts);
	return result;
}


jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_sepiaFilter(
		JNIEnv* env, jclass object, jintArray pixels, jint x,jint y,jint filterWidth,jint filterHeight,jint width, jint height,
		jint depth) {
	SepiaOptions options(depth);
	jintArray result =PROC_IMAGE_WITH_OPTIONS(env, pixels,x,y,filterWidth,filterHeight, width, height, SepiaFilter, options);
	return result;
}

jboolean Java_toto_graphics_bitmap_filters_nativ_NativeFilters_isBlurred(JNIEnv *env,
		jclass clazz, jbyteArray input, jint width, jint height) {
	jboolean inputCopy = JNI_FALSE;
	jbyte* const i = env->GetByteArrayElements(input, &inputCopy);

	float blur = 0;
	float extent = 0;

	int blurred = IsBlurred(reinterpret_cast<uint8*>(i), width, height, &blur,
			&extent);
	env->ReleaseByteArrayElements(input, i, JNI_ABORT);
	return blurred ? JNI_TRUE : JNI_FALSE;
}
