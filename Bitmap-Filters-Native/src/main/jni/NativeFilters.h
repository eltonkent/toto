/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class rage_graphics_bitmap_filters_nativ_nativ_NativeFilters */

#ifndef _Included_toto_graphics_bitmap_filters_nativ_NativeFilters
#define _Included_toto_graphics_bitmap_filters_nativ_NativeFilters
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    lightFilter
 * Signature: ([IIIIII)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_lightFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jint, jint, jint);

JNIEXPORT jintArray Java_toto_graphics_bitmap_filters_nativ_NativeFilters_gammaCorrection(
		JNIEnv*, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jdouble);
/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    lomoAddBlckRound
 * Signature: ([IIID)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_lomoAddBlckRound(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jdouble);

JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_contrastFilter(
		JNIEnv* , jclass , jintArray , jint ,jint ,jint ,jint ,jint , jint ,
		jdouble );

JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_brightFilter(
		JNIEnv* , jclass , jintArray , jint ,jint ,jint ,jint ,jint , jint ,
		jdouble );

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    neonFilter
 * Signature: ([IIIIII)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_neonFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jint, jint, jint);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    oilFilter
 * Signature: ([III)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_oilFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint);

JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_invertFilter(
		JNIEnv *,
		jclass , jintArray,jint ,jint ,jint ,jint , jint , jint );
/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    tvFilter
 * Signature: ([III)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_tvFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    averageSmooth
 * Signature: ([IIII)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_averageSmooth(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jint);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    hdrFilter
 * Signature: ([III)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_hdrFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    discreteGaussianBlur
 * Signature: ([IIID)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_discreteGaussianBlur(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jdouble);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    softGlow
 * Signature: ([IIID)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_softGlow(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jdouble);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    sketchFilter
 * Signature: ([III)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_sketchFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    sharpenFilter
 * Signature: ([III)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_sharpenFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    reliefFilter
 * Signature: ([III)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_reliefFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    pxelateFilter
 * Signature: ([IIII)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_pxelateFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jint);


/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    blockFilter
 * Signature: ([III)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_blockFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    motionBlurFilter
 * Signature: ([IIIII)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_motionBlurFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jint, jint);

/*
 * Class:     rage_graphics_bitmap_filters_nativ_NativeFilters
 * Method:    gothamFilter
 * Signature: ([III)[I
 */
JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_gothamFilter(
		JNIEnv *, jclass, jintArray,jint ,jint,jint ,jint, jint, jint);

JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_transparencyFilter(
		JNIEnv*, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jint);

JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_grayScaleFilter(
		JNIEnv*, jclass, jintArray,jint ,jint,jint ,jint, jint, jint, jint);

JNIEXPORT jintArray JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_sepiaFilter(
		JNIEnv* , jclass , jintArray , jint ,jint ,jint ,jint ,jint , jint ,
		jint);


JNIEXPORT jboolean JNICALL Java_toto_graphics_bitmap_filters_nativ_NativeFilters_isBlurred(
		JNIEnv*, jclass, jbyteArray, jint, jint);


#ifdef __cplusplus
}
#endif
#endif
