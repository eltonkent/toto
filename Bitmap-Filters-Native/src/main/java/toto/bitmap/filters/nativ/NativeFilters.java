package toto.bitmap.filters.nativ;

import com.mi.toto.ToTo;

public class NativeFilters {

    static {
        ToTo.loadLib("Bitmap_Filters","Please include the Bitmap-Filters-Native module in your project");
    }

    public  static native int[] lightFilter(int[] pixels, int x, int y, int filterW,
                                    int filterH, int width, int height, int centerX, int centerY,
                                    int radius);

    public   static native int[] lomoAddBlckRound(int[] pixels, int x, int y,
                                         int filterW, int filterH, int width, int height, double roundRadius);

    public  static native int[] gammaCorrection(int[] pixels, int x, int y,
                                        int filterW, int filterH, int width, int height, double gamma);

    public  static native int[] neonFilter(int[] pixels, int x, int y, int filterW,
                                   int filterH, int width, int height, int r, int g, int b);

    public static native int[] oilFilter(int[] pixels, int x, int y, int filterW,
                                  int filterH, int width, int height);

    public static native int[] invertFilter(int[] pixels, int x, int y, int filterW,
                                     int filterH, int width, int height);

    public static native int[] tvFilter(int[] pixels, int x, int y, int filterW,
                                 int filterH, int width, int height);

    public static native int[] averageSmooth(int[] pixels, int x, int y, int filterW,
                                      int filterH, int width, int height, int maskSize);

    public static native int[] hdrFilter(int[] pixels, int x, int y, int filterW,
                                  int filterH, int width, int height);

    public static native int[] discreteGaussianBlur(int[] pixels, int x, int y,
                                             int filterW, int filterH, int width, int height, double sigma);

    public static native int[] softGlow(int[] pixels, int x, int y, int filterW,
                                 int filterH, int width, int height, double blurSigma);

    public static native int[] sketchFilter(int[] pixels, int x, int y, int filterW,
                                     int filterH, int width, int height);

    public static native int[] sharpenFilter(int[] pixels, int x, int y, int filterW,
                                      int filterH, int width, int height);

    public static native int[] reliefFilter(int[] pixels, int x, int y, int filterW,
                                     int filterH, int width, int height);

    public static native int[] pxelateFilter(int[] pixels, int x, int y, int filterW,
                                      int filterH, int width, int height, int pixelSize);

    public static native int[] blockFilter(int[] pixels, int x, int y, int filterW,
                                    int filterH, int width, int height);

    public static native int[] motionBlurFilter(int[] pixels, int x, int y,
                                         int filterW, int filterH, int width, int height, int xSpeed,
                                         int ySpeed);

    public static native int[] gothamFilter(int[] pixels, int x, int y, int filterW,
                                     int filterH, int width, int height);

    public static native int[] grayScaleFilter(int[] pixels, int x, int y,
                                        int filterW, int filterH, int width, int height, int saturation);

    public static native int[] brightFilter(int[] pixels, int x, int y, int filterW,
                                     int filterH, int width, int height, double brightness);

    public static native int[] contrastFilter(int[] pixels, int x, int y, int filterW,
                                       int filterH, int width, int height, double contrast);

    public static native int[] sepiaFilter(int[] pixels, int x, int y, int filterW,
                                    int filterH, int width, int height, int depth);

    public static native int[] transparencyFilter(int[] pixels, int x, int y,
                                           int filterW, int filterH, int width, int height, int level);

    public static native boolean isBlurred(byte[] input, int width, int height);

    // static native int[] scaleBI(final int[] pixels, final int width,
    // final int height, final int newWidth, final int newHeight);
}
