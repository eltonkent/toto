package toto.di.langdetect;

/**
 */
enum ErrorCode {
	NoTextError, FormatError, FileLoadError, DuplicateLangError, NeedLoadProfileError, CantDetectError, CantOpenTrainData, TrainDataFormatError, InitParamError
}

/**
 * 
 */
public class LangDetectException extends Exception {
	private static final long serialVersionUID = 1L;
	private final ErrorCode code;

	/**
	 * @param code
	 * @param message
	 */
	public LangDetectException(final ErrorCode code, final String message) {
		super(message);
		this.code = code;
	}

	/**
	 * @return the error code
	 */
	public ErrorCode getCode() {
		return code;
	}
}
