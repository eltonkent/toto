package toto.di.langdetect;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This is {@link Messages} class generated by Eclipse automatically.
 */
class Messages {

	static Properties prop = new Properties();
	static {
		try {
			prop.load(Messages.class.getResourceAsStream("messages.properties"));
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	static void setPropertyResource(final InputStream is) throws IOException {
		prop.load(is);
	}

	private Messages() {
	}

	static String getString(final String key) {
		// try {
		return prop.getProperty(key);
		// } catch (final MissingResourceException e) {
		// return '!' + key + '!';
		// }
	}
}
