package toto.di.langdetect;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import toto.xc.json.Json;
import android.content.Context;
import android.content.res.Resources.NotFoundException;

import com.mi.toto.di.langdetect.R;

/**
 * Language Detector Factory Class
 * 
 * This class manages an initialization and constructions of {@link Detector}.
 * 
 * Before using language detection library, load profiles with
 * {@link DetectorFactory#loadProfile(String)} method and setKey initialization
 * parameters.
 * 
 * When the language detection, construct Detector instance via
 * {@link DetectorFactory#create()}. See also {@link Detector}'s sample code.
 * 
 * 
 * @see Detector
 */
public class DetectorFactory {
	public HashMap<String, double[]> wordLangProbMap;
	public ArrayList<String> langlist;
	public Long seed = null;

	private DetectorFactory() {
		wordLangProbMap = new HashMap<String, double[]>();
		langlist = new ArrayList<String>();
	}

	static private DetectorFactory instance_ = new DetectorFactory();

	/**
	 * Load profiles from specified directory. This method must be called once
	 * before language detection.
	 * 
	 * @param profileDirectory
	 *            profile directory path
	 * @throws LangDetectException
	 *             Can't open profiles(error code =
	 *             {@link ErrorCode#FileLoadError}) or profile's format is wrong
	 *             (error code = {@link ErrorCode#FormatError})
	 */
	public static void loadProfile(final String profileDirectory)
			throws LangDetectException {
		loadProfile(new File(profileDirectory));
	}

	/**
	 * Load profiles from specified directory. This method must be called once
	 * before language detection.
	 * 
	 * @param profileDirectory
	 *            profile directory path
	 * @throws LangDetectException
	 *             Can't open profiles(error code =
	 *             {@link ErrorCode#FileLoadError}) or profile's format is wrong
	 *             (error code = {@link ErrorCode#FormatError})
	 */
	public static void loadProfile(final File profileDirectory)
			throws LangDetectException {
		final File[] listFiles = profileDirectory.listFiles();
		if (listFiles == null)
			throw new LangDetectException(ErrorCode.NeedLoadProfileError,
					"Not found profile: " + profileDirectory);

		final int langsize = listFiles.length;
		int index = 0;
		for (final File file : listFiles) {
			if (file.getName().startsWith(".") || !file.isFile())
				continue;
			FileInputStream is = null;
			try {
				is = new FileInputStream(file);
				final LangProfile profile = new Json().fromJson(
						new BufferedReader(new InputStreamReader(is)),
						LangProfile.class);
				addProfile(profile, index, langsize);
				++index;
			} catch (final IOException e) {
				throw new LangDetectException(ErrorCode.FileLoadError,
						"can't open '" + file.getName() + "'");
			} finally {
				try {
					if (is != null)
						is.close();
				} catch (final IOException e) {
				}
			}
		}
	}

	/**
	 * Load profiles from specified directory. This method must be called once
	 * before language detection.
	 * 
	 * @param profileDirectory
	 *            profile directory path
	 * @throws LangDetectException
	 *             Can't open profiles(error code =
	 *             {@link ErrorCode#FileLoadError}) or profile's format is wrong
	 *             (error code = {@link ErrorCode#FormatError})
	 */
	public static void loadProfile(final List<String> json_profiles)
			throws LangDetectException {
		int index = 0;
		final int langsize = json_profiles.size();
		if (langsize < 2)
			throw new LangDetectException(ErrorCode.NeedLoadProfileError,
					"Need more than 2 profiles");

		for (final String json : json_profiles) {
			final LangProfile profile = new Json().fromJson(json,
					LangProfile.class);
			addProfile(profile, index, langsize);
			++index;
		}
	}

	/**
	 * @param profile
	 * @param langsize
	 * @param index
	 * @throws LangDetectException
	 */
	public static/* package scope */void addProfile(final LangProfile profile,
			final int index, final int langsize) throws LangDetectException {
		final String lang = profile.name;
		if (instance_.langlist.contains(lang)) {
			throw new LangDetectException(ErrorCode.DuplicateLangError,
					"duplicate the same language profile");
		}
		instance_.langlist.add(lang);
		for (final String word : profile.freq.keySet()) {
			if (!instance_.wordLangProbMap.containsKey(word)) {
				instance_.wordLangProbMap.put(word, new double[langsize]);
			}
			final int length = word.length();
			if (length >= 1 && length <= 3) {
				final double prob = profile.freq.get(word).doubleValue()
						/ profile.n_words[length - 1];
				instance_.wordLangProbMap.get(word)[index] = prob;
			}
		}
	}

	/**
	 * Clear loaded language profiles (reinitialization to be available)
	 */
	static public void clear() {
		instance_.langlist.clear();
		instance_.wordLangProbMap.clear();
	}

	/**
	 * Construct Detector instance
	 * 
	 * @return Detector instance
	 * @throws LangDetectException
	 * @throws IOException
	 * @throws NotFoundException
	 */
	static public Detector create(final Context context)
			throws LangDetectException, NotFoundException, IOException {
		Messages.setPropertyResource(context.getResources().openRawResource(
				R.raw.messages));
		return createDetector();
	}

	/**
	 * Construct Detector instance with smoothing parameter
	 * 
	 * @param alpha
	 *            smoothing parameter (default value = 0.5)
	 * @return Detector instance
	 * @throws LangDetectException
	 */
	public static Detector create(final double alpha)
			throws LangDetectException {
		final Detector detector = createDetector();
		detector.setAlpha(alpha);
		return detector;
	}

	static private Detector createDetector() throws LangDetectException {
		if (instance_.langlist.size() == 0)
			throw new LangDetectException(ErrorCode.NeedLoadProfileError,
					"need to load profiles");
		final Detector detector = new Detector(instance_);
		return detector;
	}

	public static void setSeed(final long seed) {
		instance_.seed = seed;
	}

	public static final List<String> getLangList() {
		return Collections.unmodifiableList(instance_.langlist);
	}
}
