package toto.di.langdetect;

/**
 * {@link TagExtractor} is a class which extracts inner texts of specified tag.
 * Users don't use this class directly.
 * 
 */
public class TagExtractor {
	/* package scope */public String target_;
	/* package scope */public int threshold_;
	/* package scope */public StringBuffer buf_;
	/* package scope */public String tag_;
	private int count_;

	public TagExtractor(final String tag, final int threshold) {
		target_ = tag;
		threshold_ = threshold;
		count_ = 0;
		clear();
	}

	public int count() {
		return count_;
	}

	public void clear() {
		buf_ = new StringBuffer();
		tag_ = null;
	}

	public void setTag(final String tag) {
		tag_ = tag;
	}

	public void add(final String line) {
		if (tag_ == target_ && line != null) {
			buf_.append(line);
		}
	}

	public String closeTag() {
		String st = null;
		if (tag_ == target_ && buf_.length() > threshold_) {
			st = buf_.toString();
			++count_;
		}
		clear();
		return st;
	}

}
