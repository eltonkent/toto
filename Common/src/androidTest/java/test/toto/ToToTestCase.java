package test.toto;

import java.io.File;
import java.util.Arrays;

//import toto.io.file.SDCardUtils;

import android.test.AndroidTestCase;
import android.util.Log;

public class ToToTestCase extends AndroidTestCase {

	static final String TAG = "ToToTest";
	protected final String  TEST_DIR_NAME="ToToTest";

	/**
	 * Log heading
	 * 
	 * @param module
	 * @param info
	 */
	protected void logH(String module, String info) {
		Log.d(TAG, "[" + module + "] " + info);
	}

	/**
	 * Log info
	 * 
	 * @param module
	 * @param info
	 */
	protected void logI(String info) {
		Log.d(TAG, info);
	}

	protected void assertArrayEquals(byte[] a1,byte[] a2){
		assertEquals(true,Arrays.equals(a1, a2));
	}
	
//	public String getTestStorageLocation(){
//		return SDCardUtils.getDirectory().toString()+File.separator+"ToToTest";
//	}
}
