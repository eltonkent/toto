package com.mi.toto;

public class RageRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 6735854402467673117L;

	public RageRuntimeException(final String message) {
		super(message);
	}

	public RageRuntimeException(final Throwable t) {
		super(t);
	}

	public RageRuntimeException(final String message, final Throwable t) {
		super(message, t);
	}
}
