package com.mi.toto;

/**
 * Error to illustrate that something went horribly wrong at ToTo's core
 * services.
 * 
 * @author Mobifluence Interactive
 * 
 */
public class ToToError extends Error {
	ToToError() {
		super("Fatal error has occured");
	}

	ToToError(final String description) {
		super(description);
	}
}
