package com.mi.toto;

//import toto.async.AsyncCallback;
//import toto.log.DDMSLogger;
//import toto.log.Logger;

import android.util.Log;

public class ToTo {

	private static boolean loadToToNativeLib(final String libName,String error) {
		try {
			System.loadLibrary(libName);
			return true;
		} catch (final UnsatisfiedLinkError e) {
			if(error!=null)
			Log.e("ToTo",error);
			e.printStackTrace();
//			log().e("Could NOT load ToTo native binary:" + "lib" + libName);
			return false;
		}
	}

	/**
	 * Load a
	 * 
	 * @param libName
	 * @return
	 */
	public static boolean loadLib(final String libName) {
		return loadToToNativeLib(libName,null);
	}

	public static boolean loadLib(final String libName,String errorMessage) {
		return loadToToNativeLib(libName,errorMessage);
	}

//	/**
//	 * Check that ensures that the distribution of RAGE is the
//	 *
//	 * @param callback
//	 */
//	public void performLicenseCheck(final AsyncCallback<Boolean> callback) {
//		callback.onSuccess(true);
//	}

	private static boolean isProduction = false;
//	private static Logger logger = new DDMSLogger("ToTo");

//	public static Logger log() {
//		return logger;
//	}

	public static void logException(final Throwable t) {
//		if (!isProduction)
//			logger.e(t.getMessage()/*.getStackTrace(t)*/);
	}
}
