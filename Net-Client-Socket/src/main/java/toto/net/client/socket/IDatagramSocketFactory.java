/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.socket;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/***
 * The DatagramSocketFactory interface provides a means for the programmer to
 * control the creation of datagram sockets and provide his own DatagramSocket
 * implementations for use by all classes derived from
 * {@link toto.net.client.DatagramSocketClient} . This allows you to provide
 * your own DatagramSocket implementations and to perform security checks or
 * browser capability requests before creating a DatagramSocket.
 * <p>
 * <p>
 * 
 ***/

public interface IDatagramSocketFactory {

	/***
	 * Creates a DatagramSocket on the local host at the first available port.
	 * <p>
	 * 
	 * @exception SocketException
	 *                If the socket could not be created.
	 ***/
	public DatagramSocket createDatagramSocket() throws SocketException;

	/***
	 * Creates a DatagramSocket on the local host at a specified port.
	 * <p>
	 * 
	 * @param port
	 *            The port to use for the socket.
	 * @exception SocketException
	 *                If the socket could not be created.
	 ***/
	public DatagramSocket createDatagramSocket(int port) throws SocketException;

	/***
	 * Creates a DatagramSocket at the specified address on the local host at a
	 * specified port.
	 * <p>
	 * 
	 * @param port
	 *            The port to use for the socket.
	 * @param laddr
	 *            The local address to use.
	 * @exception SocketException
	 *                If the socket could not be created.
	 ***/
	public DatagramSocket createDatagramSocket(int port, InetAddress laddr)
			throws SocketException;
}
