/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.socket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

/***
 * The DiscardUDPClient class is a UDP implementation of a client for the
 * Discard protocol described in RFC 863. To use the class, just open a local
 * UDP port with {@link toto.net.client.DatagramSocketClient#open open } and
 * call {@link #send send } to send datagrams to the server After you're done
 * sending discard data, call {@link toto.net.client.DatagramSocketClient#close
 * close() } to clean up properly.
 * <p>
 * <p>
 * 
 * @see DiscardTCPClient
 ***/

public class DiscardUDPClient extends DatagramSocketClient {
	/*** The default discard port. It is setKey to 9 according to RFC 863. ***/
	public static final int DEFAULT_PORT = 9;

	DatagramPacket _sendPacket;

	public DiscardUDPClient() {
		_sendPacket = new DatagramPacket(new byte[0], 0);
	}

	/***
	 * Same as
	 * <code>send(data, data.length, host. DiscardUDPClient.DEFAULT_PORT)</code>
	 * .
	 ***/
	public void send(final byte[] data, final InetAddress host)
			throws IOException {
		send(data, data.length, host, DEFAULT_PORT);
	}

	/***
	 * Same as
	 * <code>send(data, length, host. DiscardUDPClient.DEFAULT_PORT)</code>.
	 ***/
	public void send(final byte[] data, final int length, final InetAddress host)
			throws IOException {
		send(data, length, host, DEFAULT_PORT);
	}

	/***
	 * Sends the specified data to the specified server at the specified port.
	 * <p>
	 * 
	 * @param data
	 *            The discard data to send.
	 * @param length
	 *            The length of the data to send. Should be less than or equal
	 *            to the length of the data byte array.
	 * @param host
	 *            The address of the server.
	 * @param port
	 *            The service port.
	 * @exception IOException
	 *                If an error occurs during the datagram send operation.
	 ***/
	public void send(final byte[] data, final int length,
			final InetAddress host, final int port) throws IOException {
		_sendPacket.setData(data);
		_sendPacket.setLength(length);
		_sendPacket.setAddress(host);
		_sendPacket.setPort(port);
		_socket_.send(_sendPacket);
	}

}
