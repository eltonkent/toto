/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.socket;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/***
 * DefaultDatagramSocketFactory implements the DatagramSocketFactory interface
 * by simply wrapping the java.net.DatagramSocket constructors. It is the
 * default DatagramSocketFactory used by
 * {@link toto.net.client.DatagramSocketClient} implementations.
 * <p>
 * <p>
 * 
 * @see IDatagramSocketFactory
 * @see DatagramSocketClient
 * @see DatagramSocketClient#setDatagramSocketFactory
 ***/

public class DefaultDatagramSocketFactory implements IDatagramSocketFactory {

	/***
	 * Creates a DatagramSocket on the local host at the first available port.
	 * <p>
	 * 
	 * @exception SocketException
	 *                If the socket could not be created.
	 ***/
	@Override
	public DatagramSocket createDatagramSocket() throws SocketException {
		return new DatagramSocket();
	}

	/***
	 * Creates a DatagramSocket on the local host at a specified port.
	 * <p>
	 * 
	 * @param port
	 *            The port to use for the socket.
	 * @exception SocketException
	 *                If the socket could not be created.
	 ***/
	@Override
	public DatagramSocket createDatagramSocket(final int port)
			throws SocketException {
		return new DatagramSocket(port);
	}

	/***
	 * Creates a DatagramSocket at the specified address on the local host at a
	 * specified port.
	 * <p>
	 * 
	 * @param port
	 *            The port to use for the socket.
	 * @param laddr
	 *            The local address to use.
	 * @exception SocketException
	 *                If the socket could not be created.
	 ***/
	@Override
	public DatagramSocket createDatagramSocket(final int port,
			final InetAddress laddr) throws SocketException {
		return new DatagramSocket(port, laddr);
	}
}
