/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.socket;

import java.io.InputStream;

/***
 * The EchoTCPClient class is a TCP implementation of a client for the Echo
 * protocol described in RFC 862. To use the class, merely establish a
 * connection with {@link toto.net.client.socket.SocketClient#connect connect } and
 * call {@link DiscardTCPClient#getOutputStream getOutputStream() * } to
 * retrieve the echo output stream and {@link #getInputStream getInputStream() }
 * to get the echo input stream. Don't close either stream when you're done
 * using them. Rather, call {@link toto.net.client.socket.SocketClient#disconnect
 * disconnect } to clean up properly.
 * <p>
 * <p>
 * 
 * @see EchoUDPClient
 * @see DiscardTCPClient
 ***/

public final class EchoTCPClient extends DiscardTCPClient {
	/*** The default echo port. It is setKey to 7 according to RFC 862. ***/
	public static final int DEFAULT_PORT = 7;

	/***
	 * The default EchoTCPClient constructor. It merely sets the default port to
	 * <code> DEFAULT_PORT </code>.
	 ***/
	public EchoTCPClient() {
		setDefaultPort(DEFAULT_PORT);
	}

	/***
	 * Returns an InputStream from which you may read echoed data from the
	 * server. You should NOT close the InputStream when you're finished reading
	 * from it. Rather, you should call
	 * {@link toto.net.client.socket.SocketClient#disconnect disconnect * } to clean up
	 * properly.
	 * <p>
	 * 
	 * @return An InputStream from which you can read echoed data from the
	 *         server.
	 ***/
	public InputStream getInputStream() {
		return _input_;
	}

}
