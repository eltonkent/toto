/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.socket;

import java.io.OutputStream;

/***
 * The DiscardTCPClient class is a TCP implementation of a client for the
 * Discard protocol described in RFC 863. To use the class, merely establish a
 * connection with {@link SocketClient#connect connect } and call
 * {@link #getOutputStream getOutputStream() } to retrieve the discard output
 * stream. Don't close the output stream when you're done writing to it. Rather,
 * call {@link SocketClient#disconnect disconnect } to clean up properly.
 * <p>
 * <p>
 * 
 * @see DiscardUDPClient
 ***/

public class DiscardTCPClient extends SocketClient {
	/*** The default discard port. It is setKey to 9 according to RFC 863. ***/
	public static final int DEFAULT_PORT = 9;

	/***
	 * The default DiscardTCPClient constructor. It merely sets the default port
	 * to <code> DEFAULT_PORT </code>.
	 ***/
	public DiscardTCPClient() {
		setDefaultPort(DEFAULT_PORT);
	}

	/***
	 * Returns an OutputStream through which you may write data to the server.
	 * You should NOT close the OutputStream when you're finished reading from
	 * it. Rather, you should call
	 * {@link toto.net.client.socket.madrobot.io.net.client.net.SocketClient#disconnect disconnect
	 * * } to clean up properly.
	 * <p>
	 * 
	 * @return An OutputStream through which you can write data to the server.
	 ***/
	public OutputStream getOutputStream() {
		return _output_;
	}
}
