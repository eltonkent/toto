#include "CityHash.h"
#include "cityhash/city.h"

static jclass OutOfMemoryError;

JNIEXPORT void JNICALL Java_toto_security_hash_CityHash_init
(JNIEnv *env, jclass clz) {
	OutOfMemoryError = (env)->FindClass("java/lang/OutOfMemoryError");
}

static void throw_OOM(JNIEnv *env) {
	(env)->ThrowNew(OutOfMemoryError,
			"Out of memory while performing city hash");
}

JNIEXPORT jlong JNICALL Java_toto_security_hash_CityHash_digest___3BII(
		JNIEnv *env, jclass clz, jbyteArray buf, jint offset, jint length) {
	char* in;
	jlong h32;
	in = (char*) (env)->GetPrimitiveArrayCritical(buf, JNI_FALSE);
	if (in == NULL) {
		throw_OOM(env);
		return 0;
	}
	h32 = CityHash64(in, length);
	(env)->ReleasePrimitiveArrayCritical(buf, in, 0);
	return h32;
}

/*
 * Class:     rage_security_hash_CityHash
 * Method:    digest
 * Signature: ([BI)I
 */
JNIEXPORT jint JNICALL Java_toto_security_hash_CityHash_digest___3BI(
		JNIEnv * env, jclass clz, jbyteArray buf, jint length) {
	char* in;
	jint h32;
	in = (char*) (env)->GetPrimitiveArrayCritical(buf, JNI_FALSE);
	if (in == NULL) {
		throw_OOM(env);
		return 0;
	}
	h32 = CityHash32(in, length);
	(env)->ReleasePrimitiveArrayCritical(buf, in, 0);
	return h32;
}

/*
 * Class:     rage_security_hash_CityHash
 * Method:    digest
 * Signature: ([BIIJ)J
 */
JNIEXPORT jlong JNICALL Java_toto_security_hash_CityHash_digest___3BIIJ(
		JNIEnv *env, jclass clz, jbyteArray buf, jint offset, jint length,
		jlong seed) {
	char* in;
	jlong h32;
	in = (char*) (env)->GetPrimitiveArrayCritical(buf, JNI_FALSE);
	if (in == NULL) {
		throw_OOM(env);
		return 0;
	}
	h32 = CityHash64WithSeed(in, length, seed);
	(env)->ReleasePrimitiveArrayCritical(buf, in, 0);
	return h32;
}

/*
 * Class:     rage_security_hash_CityHash
 * Method:    digest
 * Signature: ([BIIJJ)J
 */
JNIEXPORT jlong JNICALL Java_toto_security_hash_CityHash_digest___3BIIJJ(
		JNIEnv *env, jclass clz, jbyteArray buf, jint offset, jint length,
		jlong seed1, jlong seed2) {
	char* in;
	jlong h32;
	in = (char*) (env)->GetPrimitiveArrayCritical(buf, JNI_FALSE);
	if (in == NULL) {
		throw_OOM(env);
		return 0;
	}
	h32 = CityHash64WithSeeds(in, length, seed1,seed2);
	(env)->ReleasePrimitiveArrayCritical(buf, in, 0);
	return h32;
}

