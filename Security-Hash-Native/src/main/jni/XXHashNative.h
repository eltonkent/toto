
#include <jni.h>


JNIEXPORT void JNICALL Java_toto_security_hash_XXHashNative_init
  (JNIEnv *, jclass );


JNIEXPORT jint JNICALL Java_toto_security_hash_XXHashNative_XXH32
  (JNIEnv *, jclass , jbyteArray , jint , jint , jint );


JNIEXPORT jlong JNICALL Java_toto_security_hash_XXHashNative_XXH32_1init
  (JNIEnv *, jclass , jint ) ;

JNIEXPORT void JNICALL Java_toto_security_hash_XXHashNative_XXH32_1update
  (JNIEnv *, jclass , jlong , jbyteArray , jint , jint );

JNIEXPORT jint JNICALL Java_toto_security_hash_XXHashNative_XXH32_1intermediateDigest
  (JNIEnv *, jclass , jlong );


JNIEXPORT jint JNICALL Java_toto_security_hash_XXHashNative_XXH32_1digest
  (JNIEnv *, jclass , jlong );


JNIEXPORT void JNICALL Java_toto_security_hash_XXHashNative_XXH32_1free
  (JNIEnv *, jclass , jlong );

