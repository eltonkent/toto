#include <stdlib.h>
#include "xxhash/hash.h"
#include "XXHashNative.h"

static jclass OutOfMemoryError;


JNIEXPORT void JNICALL Java_toto_security_hash_XXHashNative_init
  (JNIEnv *env, jclass cls) {
  OutOfMemoryError = (*env)->FindClass(env, "java/lang/OutOfMemoryError");
}

static void throw_OOM(JNIEnv *env) {
  (*env)->ThrowNew(env, OutOfMemoryError, "Out of memory");
}




JNIEXPORT jint JNICALL Java_toto_security_hash_XXHashNative_XXH32
  (JNIEnv *env, jclass cls, jbyteArray buf, jint off, jint len, jint seed) {

  char* in;
  jint h32;

  in = (char*) (*env)->GetPrimitiveArrayCritical(env, buf, 0);
  if (in == NULL) {
    throw_OOM(env);
    return 0;
  }

  h32 = XXH32(in + off, len, seed);

  (*env)->ReleasePrimitiveArrayCritical(env, buf, in, 0);

  return h32;
}


JNIEXPORT jlong JNICALL Java_toto_security_hash_XXHashNative_XXH32_1init
  (JNIEnv *env, jclass cls, jint seed) {

  return (jlong) XXH32_init(seed);

}


JNIEXPORT void JNICALL Java_toto_security_hash_XXHashNative_XXH32_1update
  (JNIEnv *env, jclass cls, jlong state, jbyteArray src, jint off, jint len) {

  char* in = (char*) (*env)->GetPrimitiveArrayCritical(env, src, 0);
  if (in == NULL) {
    throw_OOM(env);
    return;
  }

  XXH32_update((void*) state, in + off, len);

  (*env)->ReleasePrimitiveArrayCritical(env, src, in, 0);

}


JNIEXPORT jint JNICALL Java_toto_security_hash_XXHashNative_1intermediateDigest
  (JNIEnv *env, jclass cls, jlong state) {

  return XXH32_intermediateDigest((void*) state);

}


JNIEXPORT jint JNICALL Java_toto_security_hash_XXHashNative_XXH32_1digest
  (JNIEnv *env, jclass cls, jlong state) {

  return XXH32_digest((void*) state);

}


JNIEXPORT void JNICALL Java_toto_security_hash_XXHashNative_XXH32_1free
  (JNIEnv *env, jclass cls, jlong state) {

  free((void*) state);

}

