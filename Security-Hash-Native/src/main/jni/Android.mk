LOCAL_PATH := $(call my-dir)

#xxhash hash
include $(CLEAR_VARS)
LOCAL_MODULE := Security_Hash
LOCAL_CPP_EXTENSION := .cc
LOCAL_C_INCLUDES :=$(LOCAL_PATH)/cityhash $(LOCAL_PATH)/spooky-2
LOCAL_SRC_FILES := XXHashNative.c  xxhash/hash.c CityHash.cc cityhash/city.cc SpookyHash.cpp spooky-2/SpookyV2.cpp
include $(BUILD_SHARED_LIBRARY)
