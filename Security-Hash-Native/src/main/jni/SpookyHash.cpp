#include "SpookyHash.h"
#include "spooky-2/SpookyV2.h"

static jclass OutOfMemoryError;

JNIEXPORT void JNICALL Java_toto_security_hash_SpookyHash_init
(JNIEnv *env, jclass clz) {
	OutOfMemoryError = (env)->FindClass("java/lang/OutOfMemoryError");
}

static void throw_OOM(JNIEnv *env) {
	(env)->ThrowNew(OutOfMemoryError,
			"Out of memory while performing Spooky hash");
}


JNIEXPORT jint JNICALL Java_toto_security_hash_SpookyHash_digest___3BIII
  (JNIEnv * env, jobject clz, jbyteArray buf, jint offset, jint length, jint seed){

	char* in;
		jint h32;
		in = (char*) (env)->GetPrimitiveArrayCritical(buf, JNI_FALSE);
		if (in == NULL) {
			throw_OOM(env);
			return 0;
		}
		h32 = SpookyHash::Hash32(in, length,seed);
		(env)->ReleasePrimitiveArrayCritical(buf, in, 0);
		return h32;
}

/*
 * Class:     rage_security_hash_SpookyHash
 * Method:    digest
 * Signature: ([BIIJ)J
 */
JNIEXPORT jlong JNICALL Java_toto_security_hash_SpookyHash_digest___3BIIJ
  (JNIEnv *env, jobject clz, jbyteArray buf, jint offset, jint length, jlong seed)
{

	char* in;
		jlong h32;
		in = (char*) (env)->GetPrimitiveArrayCritical(buf, JNI_FALSE);
		if (in == NULL) {
			throw_OOM(env);
			return 0;
		}
		h32 = SpookyHash::Hash64(in, length,seed);
		(env)->ReleasePrimitiveArrayCritical(buf, in, 0);
		return h32;
	return 0;
}
