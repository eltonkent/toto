package toto.security.hash.nativ;

import java.math.BigInteger;

/**
 * Internal class
 */
 class EncodeUtils {

	static interface EndianReader {
		int toInt(byte[] b, int i);

		long toLong(byte[] b, int i);
	}

	private static class LittleEndianReader implements EndianReader {
		public int toInt(final byte[] b, final int i) {
			return toIntLE(b, i);
		}

		public long toLong(final byte[] b, final int i) {
			return toLongLE(b, i);
		}
	}

	private static class BigEndianReader implements EndianReader {
		public int toInt(final byte[] b, final int i) {
			return toIntBE(b, i);
		}

		public long toLong(final byte[] b, final int i) {
			return toLongBE(b, i);
		}
	}

	static final LittleEndianReader LEReader = new LittleEndianReader();
	static final BigEndianReader BEReader = new BigEndianReader();

	/**
	 * 
	 * @param b
	 * @param i
	 */
	static int toIntBE(final byte[] b, final int i) {
		return (((b[i + 0] & 255) << 24) + ((b[i + 1] & 255) << 16)
				+ ((b[i + 2] & 255) << 8) + ((b[i + 3] & 255) << 0));
	}

	/**
	 * 
	 * @param b
	 * @param i
	 */
	static int toIntLE(final byte[] b, final int i) {
		return (((b[i + 3] & 255) << 24) + ((b[i + 2] & 255) << 16)
				+ ((b[i + 1] & 255) << 8) + ((b[i + 0] & 255) << 0));
	}

	/**
	 * 
	 * @param b
	 * @param i
	 */
	static long toLongBE(final byte[] b, final int i) {

		return (((long) b[i + 0] << 56) + ((long) (b[i + 1] & 255) << 48)
				+ ((long) (b[i + 2] & 255) << 40)
				+ ((long) (b[i + 3] & 255) << 32)
				+ ((long) (b[i + 4] & 255) << 24) + ((b[i + 5] & 255) << 16)
				+ ((b[i + 6] & 255) << 8) + ((b[i + 7] & 255) << 0));

	}

	/**
	 * 
	 * @param b
	 * @param i
	 */
	static long toLongLE(final byte[] b, final int i) {

		return (((long) b[i + 7] << 56) + ((long) (b[i + 6] & 255) << 48)
				+ ((long) (b[i + 5] & 255) << 40)
				+ ((long) (b[i + 4] & 255) << 32)
				+ ((long) (b[i + 3] & 255) << 24) + ((b[i + 2] & 255) << 16)
				+ ((b[i + 1] & 255) << 8) + ((b[i + 0] & 255) << 0));

	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	static byte[] toBytesBE(final long v) {
		return new byte[] { (byte) (v >>> 56), (byte) (v >>> 48),
				(byte) (v >>> 40), (byte) (v >>> 32), (byte) (v >>> 24),
				(byte) (v >>> 16), (byte) (v >>> 8), (byte) (v >>> 0), };
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	static byte[] toBytesBE(final int v) {
		return new byte[] { (byte) ((v >>> 24) & 0xFF),
				(byte) ((v >>> 16) & 0xFF), (byte) ((v >>> 8) & 0xFF),
				(byte) ((v >>> 0) & 0xFF) };
	}

	/**
     */
	static long toUnsigned(final int value) {
		return 0xffffffffL & value;
	}

	/**
	 * @param value
	 * @return
	 */
	static BigInteger toUnsigned(final long value) {
		final byte[] v = toBytesBE(value);
		final byte[] vv = new byte[v.length + 1];
		System.arraycopy(v, 0, vv, 1, v.length);
		return new BigInteger(vv);
	}

	/**
	 * @param values
	 * @return
	 */
	static BigInteger toUnsigned(final int[] values) {

		final byte[] buffer = new byte[values.length * 4 + 1];
		for (int i = 0; i < values.length; i++) {
			final byte[] ival = toBytesBE(values[i]);
			System.arraycopy(ival, 0, buffer, i * 4 + 1, 4);
		}

		return new BigInteger(buffer);

	}

	/**
	 * @param values
	 * @return
	 */
	static BigInteger toUnsigned(final long[] values) {

		final byte[] buffer = new byte[values.length * 8 + 1];
		for (int i = 0; i < values.length; i++) {
			final byte[] ival = toBytesBE(values[i]);
			System.arraycopy(ival, 0, buffer, i * 8 + 1, 8);
		}

		return new BigInteger(buffer);

	}

}
