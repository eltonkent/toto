package toto.security.hash.nativ;

import toto.lang.ArrayUtils;
import toto.security.hash.nativ.EncodeUtils.EndianReader;
import toto.security.hash.ToToHash;

/**
 * Native and java based XXHash implementation
 * 
 * @author ekent4
 * 
 */
public class XXHash extends ToToHash {

	private long state;
	private final int seed;

	/**
	 * create an XHash instance with the given seed
	 * 
	 * @param seed
	 */
	public XXHash(final int seed) {
		this.seed = seed;
		state = XXHashNative.XXH32_init(seed);
	}

	public static int digestNative(final byte[] buf, final int off,
			final int len, final int seed) {
		ArrayUtils.checkRange(buf, off, len);
		return XXHashNative.XXH32(buf, off, len, seed);
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		// free memory
		XXHashNative.XXH32_free(state);
		state = 0;
	}

	public void reset() {
		checkState();
		XXHashNative.XXH32_free(state);
		state = XXHashNative.XXH32_init(seed);
	}

	private void checkState() {
		if (state == 0) {
			throw new AssertionError("Already finalized");
		}
	}

	public int getValue() {
		checkState();
		return XXHashNative.XXH32_intermediateDigest(state);
	}

	public void n_update(final byte[] bytes, final int off, final int len) {
		checkState();
		XXHashNative.XXH32_update(state, bytes, off, len);
	}

	private static final int PRIME1 = (int) 2654435761L;
	private static final int PRIME2 = (int) 2246822519L;
	private static final int PRIME3 = (int) 3266489917L;
	private static final int PRIME4 = 668265263;
	private static final int PRIME5 = 0x165667b1;

	public static int digestSmall(final byte[] data, final int seed,
			final boolean bigendian) {

		final EndianReader er = bigendian ? EncodeUtils.BEReader
				: EncodeUtils.LEReader;
		final int len = data.length;
		final int bEnd = len;
		final int limit = bEnd - 4;

		int idx = seed + PRIME1;
		int crc = PRIME5;
		int i = 0;

		while (i < limit) {
			crc += er.toInt(data, i) + (idx++);
			crc += Integer.rotateLeft(crc, 17) * PRIME4;
			crc *= PRIME1;
			i += 4;
		}

		while (i < bEnd) {
			crc += (data[i] & 0xFF) + (idx++);
			crc *= PRIME1;
			i++;
		}

		crc += len;

		crc ^= crc >>> 15;
		crc *= PRIME2;
		crc ^= crc >>> 13;
		crc *= PRIME3;
		crc ^= crc >>> 16;

		return crc;

	}

	public static int digestFast32(final byte[] data, final int seed,
			final boolean bigendian) {

		final int len = data.length;

		if (len < 16) {
			return digestSmall(data, seed, bigendian);
		}

		final EndianReader er = bigendian ? EncodeUtils.BEReader
				: EncodeUtils.LEReader;
		final int bEnd = len;
		final int limit = bEnd - 16;
		int v1 = seed + PRIME1;
		int v2 = v1 * PRIME2 + len;
		int v3 = v2 * PRIME3;
		int v4 = v3 * PRIME4;

		int i = 0;
		int crc = 0;
		while (i < limit) {
			v1 = Integer.rotateLeft(v1, 13) + er.toInt(data, i);
			i += 4;
			v2 = Integer.rotateLeft(v2, 11) + er.toInt(data, i);
			i += 4;
			v3 = Integer.rotateLeft(v3, 17) + er.toInt(data, i);
			i += 4;
			v4 = Integer.rotateLeft(v4, 19) + er.toInt(data, i);
			i += 4;
		}

		i = bEnd - 16;
		v1 += Integer.rotateLeft(v1, 17);
		v2 += Integer.rotateLeft(v2, 19);
		v3 += Integer.rotateLeft(v3, 13);
		v4 += Integer.rotateLeft(v4, 11);

		v1 *= PRIME1;
		v2 *= PRIME1;
		v3 *= PRIME1;
		v4 *= PRIME1;

		v1 += er.toInt(data, i);
		i += 4;
		v2 += er.toInt(data, i);
		i += 4;
		v3 += er.toInt(data, i);
		i += 4;
		v4 += er.toInt(data, i);

		v1 *= PRIME2;
		v2 *= PRIME2;
		v3 *= PRIME2;
		v4 *= PRIME2;

		v1 += Integer.rotateLeft(v1, 11);
		v2 += Integer.rotateLeft(v2, 17);
		v3 += Integer.rotateLeft(v3, 19);
		v4 += Integer.rotateLeft(v4, 13);

		v1 *= PRIME3;
		v2 *= PRIME3;
		v3 *= PRIME3;
		v4 *= PRIME3;

		crc = v1 + Integer.rotateLeft(v2, 3) + Integer.rotateLeft(v3, 6)
				+ Integer.rotateLeft(v4, 9);
		crc ^= crc >>> 11;
		crc += (PRIME4 + len) * PRIME1;
		crc ^= crc >>> 15;
		crc *= PRIME2;
		crc ^= crc >>> 13;

		return crc;
	}

	public static int digestStrong32(final byte[] data, final int seed,
			final boolean bigendian) {

		final int len = data.length;

		if (len < 16) {
			return digestSmall(data, seed, bigendian);
		}

		final EndianReader er = bigendian ? EncodeUtils.BEReader
				: EncodeUtils.LEReader;
		final int bEnd = len;
		final int limit = bEnd - 16;
		int v1 = seed + PRIME1;
		int v2 = v1 * PRIME2 + len;
		int v3 = v2 * PRIME3;
		int v4 = v3 * PRIME4;

		int i = 0;
		int crc = 0;

		while (i < limit) {
			v1 += Integer.rotateLeft(v1, 13);
			v1 *= PRIME1;
			v1 += er.toInt(data, i);
			i += 4;

			v2 += Integer.rotateLeft(v2, 11);
			v2 *= PRIME1;
			v2 += er.toInt(data, i);
			i += 4;

			v3 += Integer.rotateLeft(v3, 17);
			v3 *= PRIME1;
			v3 += er.toInt(data, i);
			i += 4;

			v4 += Integer.rotateLeft(v4, 19);
			v4 *= PRIME1;
			v4 += er.toInt(data, i);
			i += 4;

		}

		i = bEnd - 16;
		v1 += Integer.rotateLeft(v1, 17);
		v2 += Integer.rotateLeft(v2, 19);
		v3 += Integer.rotateLeft(v3, 13);
		v4 += Integer.rotateLeft(v4, 11);

		v1 *= PRIME1;
		v2 *= PRIME1;
		v3 *= PRIME1;
		v4 *= PRIME1;

		v1 += er.toInt(data, i);
		i += 4;
		v2 += er.toInt(data, i);
		i += 4;
		v3 += er.toInt(data, i);
		i += 4;
		v4 += er.toInt(data, i);

		v1 *= PRIME2;
		v2 *= PRIME2;
		v3 *= PRIME2;
		v4 *= PRIME2;

		v1 += Integer.rotateLeft(v1, 11);
		v2 += Integer.rotateLeft(v2, 17);
		v3 += Integer.rotateLeft(v3, 19);
		v4 += Integer.rotateLeft(v4, 13);

		v1 *= PRIME3;
		v2 *= PRIME3;
		v3 *= PRIME3;
		v4 *= PRIME3;

		crc = v1 + Integer.rotateLeft(v2, 3) + Integer.rotateLeft(v3, 6)
				+ Integer.rotateLeft(v4, 9);
		crc ^= crc >>> 11;
		crc += (PRIME4 + len) * PRIME1;
		crc ^= crc >>> 15;
		crc *= PRIME2;
		crc ^= crc >>> 13;

		return crc;
	}

	@Override
	public long hash(final byte[] data) {
		return digestFast32(data, 0xf3232444, true);
	}
}
