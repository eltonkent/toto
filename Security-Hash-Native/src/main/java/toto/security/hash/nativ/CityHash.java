package toto.security.hash.nativ;

import com.mi.toto.ToTo;
import toto.security.hash.ToToHash;

/**
 * City hash implementation in native code.
 * 
 * @author Mobifluence Interactive
 * 
 */
public class CityHash extends ToToHash {
	static {
		ToTo.loadLib("Security_Hash");
		init();
	}

	/**
	 * Integer width based city hash
	 * 
	 * @param buf
	 * @return
	 */
	public static int digest(final byte[] buf) {
		return digest(buf, buf.length);
	}

	private static native void init();

	public static native long digest(final byte[] buf, int offset, int length);

	/**
	 * Integer width based city hash
	 * 
	 * @param buf
	 * @return
	 */
	private static native int digest(final byte[] buf, int length);

	/**
	 * Single seed city hash
	 * 
	 * @param buf
	 * @param offset
	 * @param length
	 * @param seed
	 * @return
	 */
	public static native long digest(final byte[] buf, int offset, int length,
			long seed);

	/**
	 * Dual seed city hash
	 * 
	 * @param buf
	 * @param offset
	 * @param length
	 * @param seed1
	 * @param seed2
	 * @return
	 */
	public static native long digest(final byte[] buf, final int offset,
			final int length, final long seed1, long seed2);

	@Override
	public long hash(final byte[] data) {
		return digest(data);
	}

}
