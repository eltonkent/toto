package toto.security.hash.nativ;

import com.mi.toto.ToTo;
import toto.security.hash.ToToHash;

public class SpookyHash extends ToToHash {

	static {
		ToTo.loadLib("Security_Hash");
		init();
	}

	private static native void init();

	public static native int digest(byte[] buf, int offset, int length, int seed);

	public static native long digest(byte[] buf, int offset, int length,
			long seed);

	@Override
	public long hash(final byte[] data) {
		return digest(data, 0, data.length, 0xf3434343);
	}
}
