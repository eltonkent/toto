package toto.util.random;

/**
 * Random Color generator
 * 
 * @author ekent4
 * 
 */
public class RandomColor {

	/**
	 * Generate a random color
	 * 
	 * @return
	 */
	public static int getRandomColor() {
		final int i = RandomPrimitives.randomInt(16777215);
		final int j = RandomPrimitives.randomInt(255);
		return i & 0xFFFFFF | j << 24;
	}
}
