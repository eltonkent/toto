package toto.util.random;

import java.util.Random;

public class RandomPrimitives {
	static public Random random = new Random();

	public static int randomInt() {
		return random.nextInt();
	}

	/**
	 * Returns a random number between 0 (inclusive) and the specified value
	 * (inclusive).
	 */
	static public final int randomInt(final int range) {
		return random.nextInt(range + 1);
	}

	/**
	 * Returns a random number between start (inclusive) and end (inclusive).
	 */
	static public final int randomInt(final int start, final int end) {
		return start + random.nextInt(end - start + 1);
	}

	/**
	 * Returns a random float
	 * 
	 * @return
	 */
	static public final float randomFloat() {
		return random.nextFloat();
	}

	/**
	 * Returns a random number between 0 (inclusive) and the specified value
	 * (inclusive).
	 */
	static public final float randomFloat(final float range) {
		return random.nextFloat() * range;
	}

	/**
	 * Returns a random number between start (inclusive) and end (inclusive).
	 */
	static public final float randomFloat(final float start, final float end) {
		return start + random.nextFloat() * (end - start);
	}

	static public final boolean randomBoolean() {
		return random.nextBoolean();
	}

	/**
	 * Returns true if a random value between 0 and 1 is less than the specified
	 * value.
	 */
	static public final boolean randomBoolean(final float chance) {
		return random.nextFloat() < chance;
	}

}
