package test.toto.util.random;

import test.toto.ToToTestCase;

public class RandomStringTest extends ToToTestCase {

	public void testRandomString(){
		logH("Util:Ramdom", "Starting Random Text Test");
		String rand=toto.util.random.RandomString.random(10);
		logI("Random String "+rand); 
		assertEquals(rand.length(), 10);
	}
}
