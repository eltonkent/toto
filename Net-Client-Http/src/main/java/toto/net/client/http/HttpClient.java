/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http;

import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Higly flexible utility to perform any operation over HTTP.
 * <p>
 * <b> Usage</b><br/>
 * 
 * <pre>
 * HttpHelperSettings settings = new HttpHelperSettings();
 * <font color="green">// add request parameters (if any).</font>
 * List&lt;NameValuePair&gt; requestParameter = new ArrayList&lt;NameValuePair&gt;();
 * NameValuePair param = new BasicNameValuePair(&quot;q&quot;, &quot;Android library&quot;);
 * requestParameter.add(param);
 * <font color="green">// setKey the parameters</font>
 * settings.setHttpRequestParameters(requestParameter);
 * HttpClient httpTask = new HttpClient(new URI(&quot;http://www.google.com&quot;), settings);
 * <font color="green">// execute the http request and return the &lt;code&gt;HttpResponse&lt;/code&gt;</font>
 * httpTask.execute();
 * </pre>
 * 
 * </p>
 * 
 */
public class HttpClient {

	private static final String TAG = "ToTo";

	private HttpHelperSettings httpSettings = new HttpHelperSettings();
	// private Map<String, String> requestHeader;
	private URI requestUrl;
	private Map<String, String> responseHeader;
	private HttpContext httpContext;

	public HttpContext getHttpContext() {
		return httpContext;
	}

	public void setHttpContext(final HttpContext httpContext) {
		this.httpContext = httpContext;
	}

	/**
	 * 
	 * Parameterized constructor to initialize the request details.
	 * 
	 * @param requestUrl
	 *            request url
	 */
	public HttpClient(final URI requestUrl) {
		this(requestUrl, new HttpHelperSettings());
	}

	public HttpClient(final URI requestUrl, final HttpHelperSettings settings) {
		this.requestUrl = requestUrl;
		this.httpSettings = settings;
	}

	/**
	 * Access point to add the query parameter to the request url
	 * 
	 * @throws URISyntaxException
	 *             if request url syntax is wrong
	 */
	private void addQueryParameter() throws URISyntaxException {
		this.requestUrl = URIUtils.createURI(this.requestUrl.getScheme(),
				this.requestUrl.getAuthority(), -1, this.requestUrl.getPath(),
				URLEncodedUtils.format(httpSettings.getHttpRequestParameters(),
						HTTP.UTF_8), null);
	}

	/**
	 * Add the response header into response
	 * 
	 * @param responseHeader
	 *            - http headers
	 */
	private void addResponseHeaders(final Header[] responseHeader) {
		for (final Header header : responseHeader) {
			getResponseHeader().put(header.getName(), header.getValue());

		}
	}

	/**
	 * Cancel the request after the {@link HttpClient#execute()} method is
	 * called
	 */
	public void cancel() {
		httpSettings.getHttpClient().getConnectionManager().shutdown();
	}

	/**
	 * 
	 * Get the http response
	 * 
	 * @return HttpEntity - the response to the request. This is always a final
	 *         response, never an intermediate response with an 1xx status code.
	 * 
	 * @throws IOException
	 *             - in case of a problem or the connection was aborted
	 * 
	 * @throws URISyntaxException
	 *             in case of request url syntax error
	 */
	public HttpResponse execute() throws IOException, URISyntaxException {

		switch (httpSettings.getHttpMethod()) {
		case GET:
			return handleOtherMethods(new HttpGet(requestUrl));
		case POST:
			return handleHttpPost();
		case DELETE:
			return handleOtherMethods(new HttpDelete(requestUrl));
		case HEAD:
			return handleOtherMethods(new HttpHead(requestUrl));
		case OPTIONS:
			return handleOtherMethods(new HttpOptions(requestUrl));
		case PUT:
			return handleOtherMethods(new HttpPut(requestUrl));
		case TRACE:
			return handleOtherMethods(new HttpTrace(requestUrl));
		}
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * Hold the responsibility of initialize and sending the http HEAD request
	 * to the requestURL and return the http response object
	 * 
	 * @return returns the HttpEntity object
	 * 
	 * @throws IOException
	 *             - in case of a problem or the connection was aborted
	 * 
	 * @throws URISyntaxException
	 *             in case of request url syntax error
	 */
	private HttpResponse handleOtherMethods(final HttpRequestBase method)
			throws IOException, URISyntaxException {

		if (hasRequestParameter()) {
			addQueryParameter();
		}
		Log.d(TAG, "Request URL:[GET] " + requestUrl);
		this.initRequest(method);
		HttpResponse httpResponse;
		if (httpContext != null) {
			httpResponse = httpSettings.getHttpClient().execute(method,
					httpContext);
		} else {
			httpResponse = httpSettings.getHttpClient().execute(method);
		}
		addResponseHeaders(httpResponse.getAllHeaders());
		return httpResponse;
	}

	public HttpHelperSettings getHttpSettings() {
		return httpSettings;
	}

	// ////////////////////////////////////////////////////////////////////
	// Private Method's

	/**
	 * Access point to get the response headers
	 * 
	 * @return http response headers
	 */
	public Map<String, String> getResponseHeader() {
		if (responseHeader == null) {
			responseHeader = new HashMap<String, String>();
		}
		return responseHeader;
	}

	/**
	 * Hold the responsibility of initialize and sending the http post request
	 * to the requestURL and return the http resposne object
	 * 
	 * @return returns the HttpEntity object
	 * 
	 * @throws IOException
	 *             - in case of a problem or the connection was aborted
	 * 
	 * @throws URISyntaxException
	 *             in case of request url syntax error
	 */
	private HttpResponse handleHttpPost() throws IOException,
			URISyntaxException {

		final HttpPost httpPost = new HttpPost(requestUrl);
		this.initRequest(httpPost);
		try {
			if (hasRequestParameter()) {
				httpPost.setEntity(new UrlEncodedFormEntity(httpSettings
						.getHttpRequestParameters(), "UTF-8"));
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		Log.d(TAG, "Request URL:[POST] " + requestUrl);
		HttpResponse httpResponse;
		if (httpContext != null) {
			httpResponse = httpSettings.getHttpClient().execute(httpPost,
					httpContext);
		} else {
			httpResponse = httpSettings.getHttpClient().execute(httpPost);
		}

		addResponseHeaders(httpResponse.getAllHeaders());
		return httpResponse;
	}

	/**
	 * Return the decision result for adding the headers
	 * 
	 * @return true - if the request header is empty otherwise false
	 */
	private boolean hasRequestHeader() {
		return httpSettings.getHttpRequestHeaders() != null;
	}

	/**
	 * Return the decision result for adding the request parameters
	 * 
	 * @return true - if the request parameter is empty otherwise false
	 */
	private boolean hasRequestParameter() {
		return httpSettings.getHttpRequestParameters() != null;
	}

	/**
	 * Access point to init the http request
	 * 
	 * @param httpRequestBase
	 *            Represents the http base
	 */
	private void initRequest(final HttpRequestBase httpRequestBase) {
		setDefaultRequestHeaders(httpRequestBase);
		if (hasRequestHeader()) {
			setRequestHeaders(httpRequestBase);
		}
	}

	/**
	 * Add the default headers if request headers is empty
	 * 
	 * @param httpRequestBase
	 *            - base object for http methods
	 */
	private void setDefaultRequestHeaders(final HttpRequestBase httpRequestBase) {
		final List<NameValuePair> requestHeaders = httpSettings
				.getHttpRequestHeaders();
		if (requestHeaders != null) {
			NameValuePair pair = null;
			for (int i = 0; i < requestHeaders.size(); i++) {
				pair = requestHeaders.get(i);
				httpRequestBase.setHeader(pair.getName(), pair.getValue());
			}
		}
	}

	/**
	 * 
	 * @param httpSettings
	 */
	public void setHttpSettings(final HttpHelperSettings httpSettings) {

		this.httpSettings = httpSettings;
	}

	/**
	 * Add the request header into request
	 * 
	 * @param httpRequestBase
	 *            - base object for http methods
	 */
	private void setRequestHeaders(final HttpRequestBase httpRequestBase) {
		final List<NameValuePair> requestHeaders = httpSettings
				.getHttpRequestHeaders();
		NameValuePair pair = null;
		for (int i = 0; i < requestHeaders.size(); i++) {
			pair = requestHeaders.get(i);
			httpRequestBase.setHeader(pair.getName(), pair.getValue());
		}
	}

}
