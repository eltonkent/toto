/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * An implementation of {@link UploadData} that provides data from a
 * {@code File}.
 * 
 * 
 */
public class FileUploadData implements UploadData {

	private final File file;
	private FileInputStream stream;

	public FileUploadData(final File file) throws IOException {
		if (file == null) {
			throw new IOException();
		}
		this.file = file;
		if (!file.exists() || !file.canRead()) {
			throw new IOException();
		}
		stream = new FileInputStream(file);

	}

	@Override
	public long length() {
		return file.length();
	}

	@Override
	public void read(final byte[] destination) throws IOException {
		stream.read(destination);
	}

	@Override
	public void setPosition(final long position) throws IOException {
		stream = new FileInputStream(file);
		stream.skip(position);
	}

	@Override
	public int read(final byte[] chunk, final int i, final int length)
			throws IOException {
		return stream.read(chunk, i, length);
	}

	/**
	 * Gets the filename.
	 * 
	 * @return the local file name
	 */
	public String getFileName() {
		return file.getName();
	}
}
