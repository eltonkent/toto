package toto.net.client.http;

import java.util.HashMap;
import java.util.Map;

import toto.net.client.http.HttpConstants.HttpMethod;

/**
 * ToTo's client implementation interface
 * 
 * @author Mobifluence Interactive
 * @since 3.1
 */
public abstract class AbstractTHttpClient implements THttpClient {

	private final Map<String, String> headers = new HashMap<String, String>();

	protected String contentType;

	protected HttpMethod httpMethod;

	protected String userAgent;

	public void addHeader(final String name, final String value) {
		if (name != null) {
			headers.put(name, value);
		}
	}

	public void clearHeaders() {
		headers.clear();
	}

	public abstract void connect();

	public abstract void disconnect();

	public Map<String, String> getHeaders() {
		return headers;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	public void setHttpMethod(final HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public void setUserAgent(final String userAgent) {
		this.userAgent = userAgent;
	}

}