/**
 * File upload client that supports notifications for spolling, progress and resumability.
 *
 */
package toto.net.client.http.upload;