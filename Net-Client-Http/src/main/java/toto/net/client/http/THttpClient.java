package toto.net.client.http;

import org.apache.http.HttpVersion;

import java.util.Map;

import toto.net.client.http.HttpConstants.HttpMethod;

public interface THttpClient {
	public void addHeader(final String name, final String value);

	public void clearHeaders();

	public void connect();

	public void disconnect();

	public Map<String, String> getHeaders();

	public HttpMethod getHttpMethod();

	public String getUserAgent();

	public void setContentType(final String contentType);

	public void setHttpMethod(final HttpMethod httpMethod);

	public void setUserAgent(final String userAgent);

	public void setSocketTimeout(int timeout);

	public int getSocketTimeout();

	public void setConnectionTimeout(int timeout);

	public int getConnectionTimeout();

	public void setHttpVersion(HttpVersion version);

	public HttpVersion getHttpVersion();

	public void setRetry(boolean canRetry);

	public boolean canRetry();

	public void setRetryCount(int retry);

	public int getRetryCount();

}
