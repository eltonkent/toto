/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http;

import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.List;

import toto.net.client.http.HttpConstants.HttpMethod;

/**
 * 
 * @author elton.stephen.kent
 * 
 */
public class HttpHelperSettings {

	/**
	 * Default HTTP payload buffer size 4Kb
	 */
	private int defaultBufferSize = 1042 * 4;

	private boolean expectContinue = true;

	private HttpMethod httpMethod = HttpMethod.GET;

	private boolean isSingleCookieHeader = false;

	private static List<NameValuePair> defaultHttpRequestHeaders;

	public static List<NameValuePair> getDefaultHttpRequestHeaders() {
		return defaultHttpRequestHeaders;
	}

	/**
	 * Set the default http headers that will be used by all Http calls.
	 * 
	 * @param defaultHttpRequestHeaders
	 */
	public static void setDefaultHttpRequestHeaders(
			final List<NameValuePair> defaultHttpRequestHeaders) {
		HttpHelperSettings.defaultHttpRequestHeaders = defaultHttpRequestHeaders;
	}

	private HttpClient httpClient = new DefaultHttpClient();
	{
		httpClient.getParams().setParameter(
				HttpConstants.HTTP_SOCKET_TIME_OUT_PARAM, getSocketTimeout());
		httpClient.getParams().setParameter(
				HttpConstants.HTTP_PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		httpClient.getParams().setParameter(
				HttpConstants.HTTP_SINGLE_COOKIE_PARAM, isSingleCookieHeader());
		httpClient.getParams().setParameter(
				HttpConstants.HTTP_EXPECT_CONTINUE_PARAM, isExpectContinue());
	}

	/**
	 * HTTP session timeout. Default is 30 seconds
	 */
	private int socketTimeout = 30000;

	private List<NameValuePair> requestParameter;
	private List<NameValuePair> httpRequestHeaders;

	public int getDefaultBufferSize() {
		return defaultBufferSize;
	}

	public HttpClient getHttpClient() {
		return httpClient;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public List<NameValuePair> getHttpRequestHeaders() {
		return httpRequestHeaders;
	}

	public List<NameValuePair> getHttpRequestParameters() {
		return requestParameter;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	};

	public boolean isExpectContinue() {
		return expectContinue;
	}

	public boolean isSingleCookieHeader() {
		return isSingleCookieHeader;
	}

	/**
	 * default: 4kb
	 */
	public void setDefaultBufferSize(final int defaultBufferSize) {
		this.defaultBufferSize = defaultBufferSize;
	}

	/**
	 * default :true
	 * 
	 * @param expectContinue
	 */
	public void setExpectContinue(final boolean expectContinue) {
		this.expectContinue = expectContinue;
		httpClient.getParams().setParameter(
				HttpConstants.HTTP_EXPECT_CONTINUE_PARAM, isExpectContinue());
	}

	/**
	 * Set the HttpClient implementation to use.
	 * <p>
	 * By default, the <code>DefaultHttpClient</code> is used.
	 * 
	 * </p>
	 * 
	 * @see CachingHttpClient
	 * @see DefaultHttpClient
	 * @param httpClient
	 */
	public void setHttpClient(final HttpClient httpClient) {
		this.httpClient = httpClient;
		httpClient.getParams().setParameter(
				HttpConstants.HTTP_PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
	}

	public void setHttpMethod(final HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public void setHttpRequestHeaders(
			final List<NameValuePair> httpRequestHeaders) {
		this.httpRequestHeaders = httpRequestHeaders;
	}

	public void setHttpRequestParameters(
			final List<NameValuePair> requestParameter) {
		this.requestParameter = requestParameter;
	}

	public void setSingleCookieHeader(final boolean isSingleCookieHeader) {
		this.isSingleCookieHeader = isSingleCookieHeader;
		httpClient.getParams().setParameter(
				HttpConstants.HTTP_SINGLE_COOKIE_PARAM, isSingleCookieHeader());
	}

	/**
	 * default: 30 secs
	 * 
	 * @param timeout
	 */
	public void setSocketTimeout(final int timeout) {
		this.socketTimeout = timeout;
		httpClient.getParams().setParameter(
				HttpConstants.HTTP_SOCKET_TIME_OUT_PARAM, getSocketTimeout());
	}

}
