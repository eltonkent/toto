/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.net.client.http.upload;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Factory for creating HTTP connections.
 * 
 * 
 */
public interface UrlConnectionFactory {

	/**
	 * Default URL connection factory.
	 */
	public static final UrlConnectionFactory DEFAULT = new UrlConnectionFactory() {
		@Override
		public HttpURLConnection create(final URL url) throws IOException {
			return (HttpURLConnection) url.openConnection();
		}
	};

	/**
	 * Creates an HTTP connection to <code>url</code>.
	 * 
	 * @param url
	 *            denoting the location to connect to
	 * @return an HTTP connection to <code>url</code>
	 */
	public HttpURLConnection create(URL url) throws IOException;
}
