package toto.doc.pdf;

/**
 * Fonts to PDF authoring
 * 
 * @see PDFWriter#setFont(String, String)
 */
public class Fonts {

	/**
	 * Font sub-type
	 */
	public static final String SUBTYPE = "Type1";

	public static final String FONT_TIMES_ROMAN = "Times-Roman";
	public static final String FONT_TIMES_BOLD = "Times-Bold";
	public static final String FONT_TIMES_ITALIC = "Times-Italic";
	public static final String FONT_TIMES_BOLDITALIC = "Times-BoldItalic";
	public static final String FONT_HELVETICA = "Helvetica";
	public static final String FONT_HELVETICA_BOLD = "Helvetica-Bold";
	public static final String FONT_HELVETICA_OBLIQUE = "Helvetica-Oblique";
	public static final String FONT_HELVETICA_BOLDOBLIQUE = "Helvetica-BoldOblique";
	public static final String FONT_COURIER = "Courier";
	public static final String FONT_COURIER_BOLD = "Courier-Bold";
	public static final String FONT_COURIER_OBLIQUE = "Courier-Oblique";
	public static final String FONT_COURIER_BOLDOBLIQUE = "Courier-BoldOblique";
	public static final String FONT_SYMBOL = "Symbol";
	public static final String FONT_ZAPDINGBATS = "ZapfDingbats";

	/**
	 * MAC encoding format
	 */
	public static final String ENCODING_MAC_ROMAN = "MacRomanEncoding";
	/**
	 * Windows ANSI encoding format
	 */
	public static final String ENCODING_WIN_ANSI = "WinAnsiEncoding";

}
