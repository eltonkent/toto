package toto.doc.pdf;

import java.util.ArrayList;

class Page {

	private final PDFDocument mDocument;
	private final IndirectObject mIndirectObject;
	private final ArrayList<IndirectObject> mPageFonts;
	private final ArrayList<XObjectImage> mXObjects;
	private final IndirectObject mPageContents;

	Page(final PDFDocument document) {
		mDocument = document;
		mIndirectObject = mDocument.newIndirectObject();
		mPageFonts = new ArrayList<IndirectObject>();
		mXObjects = new ArrayList<XObjectImage>();
		setFont(Fonts.SUBTYPE, Fonts.FONT_TIMES_ROMAN,
				Fonts.ENCODING_WIN_ANSI);
		mPageContents = mDocument.newIndirectObject();
		mDocument.includeIndirectObject(mPageContents);
	}

	IndirectObject getIndirectObject() {
		return mIndirectObject;
	}

	private String getFontReferences() {
		String result = "";
		if (!mPageFonts.isEmpty()) {
			result = "    /Font <<\n";
			int x = 0;
			for (final IndirectObject lFont : mPageFonts) {
				result += "      /F" + Integer.toString(++x) + " "
						+ lFont.getIndirectReference() + "\n";
			}
			result += "    >>\n";
		}
		return result;
	}

	private String getXObjectReferences() {
		String result = "";
		if (!mXObjects.isEmpty()) {
			result = "    /XObject <<\n";
			for (final XObjectImage xObj : mXObjects) {
				result += "      " + xObj.asXObjectReference() + "\n";
			}
			result += "    >>\n";
		}
		return result;
	}

	public void render(final String pagesIndirectReference) {
		mIndirectObject.setDictionaryContent("  /Type /Page\n  /Parent "
				+ pagesIndirectReference + "\n" + "  /Resources <<\n"
				+ getFontReferences() + getXObjectReferences() + "  >>\n"
				+ "  /Contents " + mPageContents.getIndirectReference() + "\n");
	}

	public void setFont(final String subType, final String baseFont) {
		final IndirectObject lFont = mDocument.newIndirectObject();
		mDocument.includeIndirectObject(lFont);
		lFont.setDictionaryContent("  /Type /Font\n  /Subtype /" + subType
				+ "\n  /BaseFont /" + baseFont + "\n");
		mPageFonts.add(lFont);
	}

	public void setFont(final String subType, final String baseFont,
			final String encoding) {
		final IndirectObject lFont = mDocument.newIndirectObject();
		mDocument.includeIndirectObject(lFont);
		lFont.setDictionaryContent("  /Type /Font\n  /Subtype /" + subType
				+ "\n  /BaseFont /" + baseFont + "\n  /Encoding /" + encoding
				+ "\n");
		mPageFonts.add(lFont);
	}

	private void addContent(final String content) {
		mPageContents.addStreamContent(content);
		final String streamContent = mPageContents.getStreamContent();
		mPageContents.setDictionaryContent("  /Length "
				+ Integer.toString(streamContent.length()) + "\n");
		mPageContents.setStreamContent(streamContent);
	}

	void addRawContent(final String rawContent) {
		addContent(rawContent);
	}

	void addText(final int leftPosition, final int topPositionFromBottom,
			final int fontSize, final String text) {
		addText(leftPosition, topPositionFromBottom, fontSize, text,
				Transformation.DEGREES_0_ROTATION);
	}

	void addText(final int leftPosition, final int topPositionFromBottom,
			final int fontSize, final String text, final String transformation) {
		addContent("BT\n" + transformation + " "
				+ Integer.toString(leftPosition) + " "
				+ Integer.toString(topPositionFromBottom) + " Tm\n" + "/F"
				+ Integer.toString(mPageFonts.size()) + " "
				+ Integer.toString(fontSize) + " Tf\n" + "(" + text + ") Tj\n"
				+ "ET\n");
	}

	void addTextAsHex(final int leftPosition, final int topPositionFromBottom,
			final int fontSize, final String hex) {
		addTextAsHex(leftPosition, topPositionFromBottom, fontSize, hex,
				Transformation.DEGREES_0_ROTATION);
	}

	void addTextAsHex(final int leftPosition, final int topPositionFromBottom,
			final int fontSize, final String hex, final String transformation) {
		addContent("BT\n" + transformation + " "
				+ Integer.toString(leftPosition) + " "
				+ Integer.toString(topPositionFromBottom) + " Tm\n" + "/F"
				+ Integer.toString(mPageFonts.size()) + " "
				+ Integer.toString(fontSize) + " Tf\n" + "<" + hex + "> Tj\n"
				+ "ET\n");
	}

	void addLine(final int fromLeft, final int fromBottom, final int toLeft,
			final int toBottom) {
		addContent(Integer.toString(fromLeft) + " "
				+ Integer.toString(fromBottom) + " m\n"
				+ Integer.toString(toLeft) + " " + Integer.toString(toBottom)
				+ " l\nS\n");
	}

	void addRectangle(final int fromLeft, final int fromBottom,
			final int toLeft, final int toBottom) {
		addContent(Integer.toString(fromLeft) + " "
				+ Integer.toString(fromBottom) + " " + Integer.toString(toLeft)
				+ " " + Integer.toString(toBottom) + " re\nS\n");
	}

	private String ensureXObjectImage(final XObjectImage xObject) {
		for (final XObjectImage x : mXObjects) {
			if (x.getId().equals(xObject.getId())) {
				return x.getName();
			}
		}
		mXObjects.add(xObject);
		xObject.appendToDocument();
		return xObject.getName();
	}

	void addImage(final int fromLeft, final int fromBottom, final int width,
			final int height, final XObjectImage xImage,
			final String transformation) {
		final String name = ensureXObjectImage(xImage);
		final String translate = "1 0 0 1 " + fromLeft + " " + fromBottom;
		final String scale = "" + width + " 0 0 " + height + " 0 0";
		final String rotate = transformation + " 0 0";
		addContent("q\n" + translate + " cm\n" + rotate + " cm\n" + scale
				+ " cm\n" + name + " Do\n" + "Q\n");
	}
}
