package toto.doc.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import toto.io.codec.ASCII85Encoder;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

class XObjectImage {

	public static final int BITSPERCOMPONENT_8 = 8;
	public static final String DEVICE_RGB = "/DeviceRGB";

	public static boolean INTERPOLATION = false;
	public static int BITSPERCOMPONENT = BITSPERCOMPONENT_8;
	public static String COLORSPACE = DEVICE_RGB;

	public static int COMPRESSION_LEVEL = Deflater.NO_COMPRESSION;
	public static String ENCODING = "ISO-8859-1";

	private static int mImageCount = 0;

	private final PDFDocument mDocument;
	private IndirectObject mIndirectObject;
	private int mDataSize = 0;
	private int mWidth = -1;
	private int mHeight = -1;
	private String mName = "";
	private String mId = "";
	private String mProcessedImage = "";

	XObjectImage(final PDFDocument document, final Bitmap bitmap) {
		mDocument = document;
		mProcessedImage = processImage(configureBitmap(bitmap));
		mId = Indentifiers.generateId(mProcessedImage);
		mName = "/img" + (++mImageCount);
	}

	void appendToDocument() {
		mIndirectObject = mDocument.newIndirectObject();
		mDocument.includeIndirectObject(mIndirectObject);
		mIndirectObject.addDictionaryContent(" /Type /XObject\n"
				+ " /Subtype /Image\n"
				+ " /Filter [/ASCII85Decode /FlateDecode]\n" + " /Width "
				+ mWidth + "\n" + " /Height " + mHeight + "\n"
				+ " /BitsPerComponent " + Integer.toString(BITSPERCOMPONENT)
				+ "\n" + " /Interpolate " + Boolean.toString(INTERPOLATION)
				+ "\n" + " /ColorSpace " + DEVICE_RGB + "\n" + " /Length "
				+ mProcessedImage.length() + "\n");
		mIndirectObject.addStreamContent(mProcessedImage);
	}

	private Bitmap configureBitmap(final Bitmap bitmap) {
		final Bitmap img = bitmap.copy(Config.ARGB_8888, false);
		if (img != null) {
			mWidth = img.getWidth();
			mHeight = img.getHeight();
			mDataSize = mWidth * mHeight * 3;
		}
		return img;
	}

	private byte[] getBitmapData(final Bitmap bitmap) {
		byte[] data = null;
		if (bitmap != null) {
			data = new byte[mDataSize];
			int intColor;
			int offset = 0;
			for (int y = 0; y < mHeight; y++) {
				for (int x = 0; x < mWidth; x++) {
					intColor = bitmap.getPixel(x, y);
					data[offset++] = (byte) ((intColor >> 16) & 0xFF);
					data[offset++] = (byte) ((intColor >> 8) & 0xFF);
					data[offset++] = (byte) ((intColor >> 0) & 0xFF);
				}
			}
		}
		return data;
	}

	private boolean deflateImageData(final ByteArrayOutputStream baos,
			final byte[] data) {
		if (data != null) {
			final Deflater deflater = new Deflater(COMPRESSION_LEVEL);
			final DeflaterOutputStream dos = new DeflaterOutputStream(baos,
					deflater);
			try {
				dos.write(data);
				dos.close();
				deflater.end();
				return true;
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	private String encodeImageData(final ByteArrayOutputStream baos) {
		final ByteArrayOutputStream sob = new ByteArrayOutputStream();
		final ASCII85Encoder enc85 = new ASCII85Encoder(sob);
		try {
			int i = 0;
			for (final byte b : baos.toByteArray()) {
				enc85.write(b);
				if (i++ == 255) {
					sob.write('\n');
					i = 0;
				}
			}
			return sob.toString(ENCODING);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	private String processImage(final Bitmap bitmap) {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (deflateImageData(baos, getBitmapData(bitmap))) {
			return encodeImageData(baos);
		}
		return null;
	}

	String asXObjectReference() {
		return mName + " " + mIndirectObject.getIndirectReference();
	}

	String getName() {
		return mName;
	}

	String getId() {
		return mId;
	}

	int getWidth() {
		return mWidth;
	}

	int getHeight() {
		return mHeight;
	}
}
