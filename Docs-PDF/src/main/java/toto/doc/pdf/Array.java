package toto.doc.pdf;

class Array extends EnclosedContent {

	Array() {
		super();
		setBeginKeyword("[ ", false, false);
		setEndKeyword("]", false, false);
	}

	void addItem(final String s) {
		addContent(s);
		addSpace();
	}

	void addItemsFromStringArray(final String[] content) {
		for (final String s : content) {
			addItem(s);
		}
	}
}
