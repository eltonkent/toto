package toto.doc.pdf;

class Dictionary extends EnclosedContent {

	Dictionary() {
		super();
		setBeginKeyword("<<", false, true);
		setEndKeyword(">>", false, true);
	}

}
