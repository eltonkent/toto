package toto.doc.pdf;

class Stream extends EnclosedContent {

	Stream() {
		super();
		setBeginKeyword("stream", false, true);
		setEndKeyword("endstream", false, true);
	}

}
