package toto.doc.pdf;

class EnclosedContent extends Base {

	private String mBegin;
	private String mEnd;
	protected StringBuilder mContent;

	EnclosedContent() {
		clear();
	}

	void setBeginKeyword(final String Value, final boolean NewLineBefore,
			final boolean NewLineAfter) {
		if (NewLineBefore)
			mBegin = "\n" + Value;
		else
			mBegin = Value;
		if (NewLineAfter)
			mBegin += "\n";
	}

	void setEndKeyword(final String Value, final boolean NewLineBefore,
			final boolean NewLineAfter) {
		if (NewLineBefore)
			mEnd = "\n" + Value;
		else
			mEnd = Value;
		if (NewLineAfter)
			mEnd += "\n";
	}

	boolean hasContent() {
		return mContent.length() > 0;
	}

	void setContent(final String Value) {
		clear();
		mContent.append(Value);
	}

	String getContent() {
		return mContent.toString();
	}

	void addContent(final String Value) {
		mContent.append(Value);
	}

	void addNewLine() {
		mContent.append("\n");
	}

	void addSpace() {
		mContent.append(" ");
	}

	@Override
	void clear() {
		mContent = new StringBuilder();
	}

	@Override
	String toPDFString() {
		return mBegin + mContent.toString() + mEnd;
	}

}
