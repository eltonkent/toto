package toto.doc.pdf;

abstract class Base {
	abstract void clear();

	abstract String toPDFString();
}
