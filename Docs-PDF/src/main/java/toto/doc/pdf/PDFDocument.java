package toto.doc.pdf;

class PDFDocument extends Base {

	private final Header mHeader;
	private final Body mBody;
	private final CrossReferenceTable mCRT;
	private final Trailer mTrailer;

	PDFDocument() {
		mHeader = new Header();
		mBody = new Body();
		mBody.setByteOffsetStart(mHeader.getPDFStringSize());
		mBody.setObjectNumberStart(0);
		mCRT = new CrossReferenceTable();
		mTrailer = new Trailer();
	}

	IndirectObject newIndirectObject() {
		return mBody.getNewIndirectObject();
	}

	IndirectObject newRawObject(final String content) {
		final IndirectObject iobj = mBody.getNewIndirectObject();
		iobj.setContent(content);
		return iobj;
	}

	IndirectObject newDictionaryObject(final String dictionaryContent) {
		final IndirectObject iobj = mBody.getNewIndirectObject();
		iobj.setDictionaryContent(dictionaryContent);
		return iobj;
	}

	IndirectObject newStreamObject(final String streamContent) {
		final IndirectObject iobj = mBody.getNewIndirectObject();
		iobj.setDictionaryContent("  /Length "
				+ Integer.toString(streamContent.length()) + "\n");
		iobj.setStreamContent(streamContent);
		return iobj;
	}

	void includeIndirectObject(final IndirectObject iobj) {
		mBody.includeIndirectObject(iobj);
	}

	@Override
	String toPDFString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(mHeader.toPDFString());
		sb.append(mBody.toPDFString());
		mCRT.setObjectNumberStart(mBody.getObjectNumberStart());
		int x = 0;
		while (x < mBody.getObjectsCount()) {
			final IndirectObject iobj = mBody.getObjectByNumberID(++x);
			if (iobj != null) {
				mCRT.addObjectXRefInfo(iobj.getByteOffset(),
						iobj.getGeneration(), iobj.getInUse());
			}
		}
		mTrailer.setObjectsCount(mBody.getObjectsCount());
		mTrailer.setCrossReferenceTableByteOffset(sb.length());
		mTrailer.setId(Indentifiers.generateId());
		return sb.toString() + mCRT.toPDFString() + mTrailer.toPDFString();
	}

	@Override
	void clear() {
		mHeader.clear();
		mBody.clear();
		mCRT.clear();
		mTrailer.clear();
	}
}
