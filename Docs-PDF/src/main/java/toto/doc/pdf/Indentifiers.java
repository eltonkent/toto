package toto.doc.pdf;

import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

class Indentifiers {

	private static char[] HexTable = { '0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	private static final String calculateMd5(final String s) {
		final StringBuffer MD5Str = new StringBuffer();
		try {
			final MessageDigest MD5digester = java.security.MessageDigest
					.getInstance("MD5");
			MD5digester.update(s.getBytes());
			final byte binMD5[] = MD5digester.digest();
			final int len = binMD5.length;
			for (int i = 0; i < len; i++) {
				MD5Str.append(HexTable[(binMD5[i] >> 4) & 0x0F]); // hi
				MD5Str.append(HexTable[(binMD5[i] >> 0) & 0x0F]); // lo
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return MD5Str.toString();
	}

	private static String encodeDate(final Date date) {
		final Calendar c = GregorianCalendar.getInstance();
		c.setTime(date);
		final int year = c.get(Calendar.YEAR);
		final int month = c.get(Calendar.MONTH) + 1;
		final int day = c.get(Calendar.DAY_OF_MONTH);
		final int hour = c.get(Calendar.HOUR);
		final int minute = c.get(Calendar.MINUTE);
		// int second = c.get(Calendar.SECOND);
		final int m = c.get(Calendar.DST_OFFSET) / 60000;
		final int dts_h = m / 60;
		final int dts_m = m % 60;
		final String sign = m > 0 ? "+" : "-";
		return String.format("(D:%40d%20d%20d%20d%20d%s%20d'%20d')", year,
				month, day, hour, minute, sign, dts_h, dts_m);
	}

	static String generateId() {
		return calculateMd5(encodeDate(new Date()));
	}

	static String generateId(final String data) {
		return calculateMd5(data);
	}
}