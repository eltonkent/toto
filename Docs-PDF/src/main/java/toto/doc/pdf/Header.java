package toto.doc.pdf;

class Header extends Base {

	private String mVersion;
	private String mRenderedHeader;

	Header() {
		clear();
	}

	void setVersion(final int Major, final int Minor) {
		mVersion = Integer.toString(Major) + "." + Integer.toString(Minor);
		render();
	}

	int getPDFStringSize() {
		return mRenderedHeader.length();
	}

	private void render() {
		mRenderedHeader = "%PDF-" + mVersion + "\n%\u00a9\u00bb\u00aa\u00b5\n";
	}

	@Override
	String toPDFString() {
		return mRenderedHeader;
	}

	@Override
	void clear() {
		setVersion(1, 4);
	}

}
