package toto.doc.pdf;

import java.util.ArrayList;

class Body extends List {

	private int mByteOffsetStart;
	private int mObjectNumberStart;
	private int mGeneratedObjectsCount;
	private ArrayList<IndirectObject> mObjectsList;

	public Body() {
		super();
		clear();
	}

	public int getObjectNumberStart() {
		return mObjectNumberStart;
	}

	public void setObjectNumberStart(final int Value) {
		mObjectNumberStart = Value;
	}

	public int getByteOffsetStart() {
		return mByteOffsetStart;
	}

	public void setByteOffsetStart(final int Value) {
		mByteOffsetStart = Value;
	}

	public int getObjectsCount() {
		return mObjectsList.size();
	}

	private int getNextAvailableObjectNumber() {
		return ++mGeneratedObjectsCount + mObjectNumberStart;
	}

	public IndirectObject getNewIndirectObject() {
		return getNewIndirectObject(getNextAvailableObjectNumber(), 0, true);
	}

	public IndirectObject getNewIndirectObject(final int Number,
			final int Generation, final boolean InUse) {
		final IndirectObject iobj = new IndirectObject();
		iobj.setNumberID(Number);
		iobj.setGeneration(Generation);
		iobj.setInUse(InUse);
		return iobj;
	}

	public IndirectObject getObjectByNumberID(final int Number) {
		IndirectObject iobj;
		int x = 0;
		while (x < mObjectsList.size()) {
			iobj = mObjectsList.get(x);
			if (iobj.getNumberID() == Number)
				return iobj;
			x++;
		}
		return null;
	}

	public void includeIndirectObject(final IndirectObject iobj) {
		mObjectsList.add(iobj);
	}

	private String render() {
		int x = 0;
		int offset = mByteOffsetStart;
		while (x < mObjectsList.size()) {
			final IndirectObject iobj = getObjectByNumberID(++x);
			String s = "";
			if (iobj != null)
				s = iobj.toPDFString() + "\n";
			mList.add(s);
			iobj.setByteOffset(offset);
			offset += s.length();
		}
		return renderList();
	}

	@Override
	public String toPDFString() {
		return render();
	}

	@Override
	public void clear() {
		super.clear();
		mByteOffsetStart = 0;
		mObjectNumberStart = 0;
		mGeneratedObjectsCount = 0;
		mObjectsList = new ArrayList<IndirectObject>();
	}

}
