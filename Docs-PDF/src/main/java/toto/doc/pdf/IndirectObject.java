package toto.doc.pdf;

class IndirectObject extends Base {

	private EnclosedContent mContent;
	private Dictionary mDictionaryContent;
	private Stream mStreamContent;
	private IndirectIdentifier mID;
	private int mByteOffset;
	private boolean mInUse;

	IndirectObject() {
		clear();
	}

	void setNumberID(final int Value) {
		mID.setNumber(Value);
	}

	int getNumberID() {
		return mID.getNumber();
	}

	void setGeneration(final int Value) {
		mID.setGeneration(Value);
	}

	int getGeneration() {
		return mID.getGeneration();
	}

	String getIndirectReference() {
		return mID.toPDFString() + " R";
	}

	void setByteOffset(final int Value) {
		mByteOffset = Value;
	}

	int getByteOffset() {
		return mByteOffset;
	}

	void setInUse(final boolean Value) {
		mInUse = Value;
	}

	boolean getInUse() {
		return mInUse;
	}

	void addContent(final String Value) {
		mContent.addContent(Value);
	}

	void setContent(final String Value) {
		mContent.setContent(Value);
	}

	String getContent() {
		return mContent.getContent();
	}

	void addDictionaryContent(final String Value) {
		mDictionaryContent.addContent(Value);
	}

	void setDictionaryContent(final String Value) {
		mDictionaryContent.setContent(Value);
	}

	String getDictionaryContent() {
		return mDictionaryContent.getContent();
	}

	void addStreamContent(final String Value) {
		mStreamContent.addContent(Value);
	}

	void setStreamContent(final String Value) {
		mStreamContent.setContent(Value);
	}

	String getStreamContent() {
		return mStreamContent.getContent();
	}

	protected String render() {
		final StringBuilder sb = new StringBuilder();
		sb.append(mID.toPDFString());
		sb.append(" ");
		// j-a-s-d: this can be performed in inherited classes DictionaryObject
		// and StreamObject
		if (mDictionaryContent.hasContent()) {
			mContent.setContent(mDictionaryContent.toPDFString());
			if (mStreamContent.hasContent())
				mContent.addContent(mStreamContent.toPDFString());
		}
		sb.append(mContent.toPDFString());
		return sb.toString();
	}

	@Override
	void clear() {
		mID = new IndirectIdentifier();
		mByteOffset = 0;
		mInUse = false;
		mContent = new EnclosedContent();
		mContent.setBeginKeyword("obj", false, true);
		mContent.setEndKeyword("endobj", false, true);
		mDictionaryContent = new Dictionary();
		mStreamContent = new Stream();
	}

	@Override
	String toPDFString() {
		return render();
	}

}
