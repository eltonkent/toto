package toto.doc.pdf;

import java.util.ArrayList;

abstract class List extends Base {

	protected ArrayList<String> mList;

	List() {
		mList = new ArrayList<String>();
	}

	protected String renderList() {
		final StringBuilder sb = new StringBuilder();
		int x = 0;
		while (x < mList.size()) {
			sb.append(mList.get(x).toString());
			x++;
		}
		return sb.toString();
	}

	@Override
	void clear() {
		mList.clear();
	}
}
