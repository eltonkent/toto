package toto.doc.pdf;

class IndirectIdentifier extends Base {

	private int mNumber;
	private int mGeneration;

	IndirectIdentifier() {
		clear();
	}

	void setNumber(final int Number) {
		this.mNumber = Number;
	}

	int getNumber() {
		return mNumber;
	}

	void setGeneration(final int Generation) {
		this.mGeneration = Generation;
	}

	int getGeneration() {
		return mGeneration;
	}

	@Override
	void clear() {
		mNumber = 0;
		mGeneration = 0;
	}

	@Override
	String toPDFString() {
		return Integer.toString(mNumber) + " " + Integer.toString(mGeneration);
	}
}
