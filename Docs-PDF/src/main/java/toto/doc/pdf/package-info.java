/**
 * Adobe Portable Document File format creation.
 * <p>
 * <div>
 * <h3>Using PDF writer</h3>
 * Creating a PDF document is simple as specifying the layout size, adding content and writing content.
 * <h4>Specifying Paper Size</h4>
 * <pre>
 * final PDFWriter pdfWriter = new PDFWriter(PaperSize.A4_WIDTH,
 * PaperSize.A4_HEIGHT);<br/>
 * </pre>
 * <h4>Adding content</h4>
 * <pre>
 * final Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
 * R.drawable.logo);<br/>
 * pdfWriter.addImage(300, 500, bitmap);<br/>
 * pdfWriter.addText(85, 75, 18, "Rage PDF Generation");<br/>
 * pdfWriter.addLine(150, 140, 270, 140);<br/>
 * <span>// Add a new page</span><br/>
 * pdfWriter.newPage();<br/>
 * pdfWriter.addRectangle(40, 50, 280, 50);<br/>
 * </pre>
 * <h4>Compiling and Writing the PDF file</h4>
 * <pre>
 * <span>// complie pdf</span><br/>
 * final String pdfString = pdfWriter.asString();<br/>
 * <span>// write to file</span><br/>
 * FileUtils.N_writeStringToFile(new File("/sdcard/mypdf.pdf"), pdfString);<br/>
 * </pre>
 * </div>
 * </p>
 */
package toto.doc.pdf;