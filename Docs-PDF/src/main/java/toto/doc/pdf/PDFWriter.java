package toto.doc.pdf;

import toto.geom2d.Rectangle;
import android.graphics.Bitmap;

/**
 * PDF authoring utility.
 * <p>
 * <div> A new page is automatically created when a PDFWriter instance is
 * created. Additional pages can be created using the {@link #newPage()} method
 * </div>
 * </p>
 * 
 */
public class PDFWriter {

	private PDFDocument mDocument;
	private IndirectObject mCatalog;
	private Pages mPages;
	private Page mCurrentPage;

	/**
	 * Create a new PDF Writer instance with {@link PaperSize#A4_WIDTH}
	 */
	public PDFWriter() {
		newDocument(PaperSize.A4_WIDTH, PaperSize.A4_HEIGHT);
	}

	/**
	 * Create a PDF writer instance with the specified paper size.
	 * 
	 * @see PaperSize
	 * @param pageWidth
	 * @param pageHeight
	 */
	public PDFWriter(final int pageWidth, final int pageHeight) {
		newDocument(pageWidth, pageHeight);
	}

	private void newDocument(final int pageWidth, final int pageHeight) {
		mDocument = new PDFDocument();
		mCatalog = mDocument.newIndirectObject();
		mDocument.includeIndirectObject(mCatalog);
		mPages = new Pages(mDocument, pageWidth, pageHeight);
		mDocument.includeIndirectObject(mPages.getIndirectObject());
		renderCatalog();
		newPage();
	}

	private void renderCatalog() {
		mCatalog.setDictionaryContent("  /Type /Catalog\n  /Pages "
				+ mPages.getIndirectObject().getIndirectReference() + "\n");
	}

	/**
	 * Add a new page.
	 */
	public void newPage() {
		mCurrentPage = mPages.newPage();
		mDocument.includeIndirectObject(mCurrentPage.getIndirectObject());
		mPages.render();
	}

	/**
	 * Set the current page for page authoring.
	 * 
	 * @see #newPage()
	 * @see #getPageCount()
	 * @param pageNumber
	 */
	public void setCurrentPage(final int pageNumber) {
		mCurrentPage = mPages.getPageAt(pageNumber);
	}

	/**
	 * Get the number of pages in this document.
	 * 
	 * @return
	 */
	public int getPageCount() {
		return mPages.getCount();
	}

	/**
	 * Set the font for the text that is added next.
	 * 
	 * @see #addText(int, int, int, String)
	 * @see #addText(int, int, int, String, String)
	 * @param subType
	 *            {@link Fonts#SUBTYPE}
	 * @param baseFont
	 *            {@link Fonts}
	 */
	public void setFont(final String subType, final String baseFont) {
		mCurrentPage.setFont(subType, baseFont);
	}

	/**
	 * Set the font for the text that is added next.
	 * 
	 * @see #addText(int, int, int, String)
	 * @see #addText(int, int, int, String, String)
	 * @param subType
	 * @param baseFont
	 * @param encoding
	 */
	public void setFont(final String subType, final String baseFont,
			final String encoding) {
		mCurrentPage.setFont(subType, baseFont, encoding);
	}

	public void addRawContent(final String rawContent) {
		mCurrentPage.addRawContent(rawContent);
	}

	/**
	 * Add text with no transformation.
	 * 
	 * @param leftPosition
	 * @param topPositionFromBottom
	 * @param fontSize
	 * @param text
	 */
	public void addText(final int leftPosition,
			final int topPositionFromBottom, final int fontSize,
			final String text) {
		addText(leftPosition, topPositionFromBottom, fontSize, text,
				Transformation.DEGREES_0_ROTATION);
	}

	/**
	 * 
	 * @param leftPosition
	 * @param topPositionFromBottom
	 * @param fontSize
	 * @param text
	 * @param transformation
	 *            {@link Transformation}
	 */
	public void addText(final int leftPosition,
			final int topPositionFromBottom, final int fontSize,
			final String text, final String transformation) {
		mCurrentPage.addText(leftPosition, topPositionFromBottom, fontSize,
				text, transformation);
	}

	public void addTextAsHex(final int leftPosition,
			final int topPositionFromBottom, final int fontSize,
			final String hex) {
		addTextAsHex(leftPosition, topPositionFromBottom, fontSize, hex,
				Transformation.DEGREES_0_ROTATION);
	}

	public void addTextAsHex(final int leftPosition,
			final int topPositionFromBottom, final int fontSize,
			final String hex, final String transformation) {
		mCurrentPage.addTextAsHex(leftPosition, topPositionFromBottom,
				fontSize, hex, transformation);
	}

	public void addLine(final int fromLeft, final int fromBottom,
			final int toLeft, final int toBottom) {
		mCurrentPage.addLine(fromLeft, fromBottom, toLeft, toBottom);
	}

	public void addRectangle(final int fromLeft, final int fromBottom,
			final int toLeft, final int toBottom) {
		mCurrentPage.addRectangle(fromLeft, fromBottom, toLeft, toBottom);
	}

	public void addRectangle(final Rectangle rect) {
		addRectangle((int) rect.getLeft(), (int) rect.getTop(),
				(int) rect.getRight(), (int) rect.getBottom());
	}

	public void addImage(final int fromLeft, final int fromBottom,
			final Bitmap bitmap) {
		addImage(fromLeft, fromBottom, bitmap,
				Transformation.DEGREES_0_ROTATION);
	}

	/**
	 * Add a bitmap with the given coordinates
	 * 
	 * @param fromLeft
	 * @param fromBottom
	 * @param bitmap
	 * @param transformation
	 *            {@link Transformation}
	 */
	public void addImage(final int fromLeft, final int fromBottom,
			final Bitmap bitmap, final String transformation) {
		final XObjectImage xImage = new XObjectImage(mDocument, bitmap);
		mCurrentPage.addImage(fromLeft, fromBottom, xImage.getWidth(),
				xImage.getHeight(), xImage, transformation);
	}

	public void addImage(final int fromLeft, final int fromBottom,
			final int toLeft, final int toBottom, final Bitmap bitmap) {
		addImage(fromLeft, fromBottom, toLeft, toBottom, bitmap,
				Transformation.DEGREES_0_ROTATION);
	}

	public void addImage(final int fromLeft, final int fromBottom,
			final int toLeft, final int toBottom, final Bitmap bitmap,
			final String transformation) {
		mCurrentPage.addImage(fromLeft, fromBottom, toLeft, toBottom,
				new XObjectImage(mDocument, bitmap), transformation);
	}

	/**
	 * Add image, preserving aspect ratio.
	 * 
	 * @param fromLeft
	 * @param fromBottom
	 * @param width
	 * @param height
	 * @param bitmap
	 */
	public void addImageKeepRatio(final int fromLeft, final int fromBottom,
			final int width, final int height, final Bitmap bitmap) {
		addImageKeepRatio(fromLeft, fromBottom, width, height, bitmap,
				Transformation.DEGREES_0_ROTATION);
	}

	public void addImageKeepRatio(final int fromLeft, final int fromBottom,
			int width, int height, final Bitmap bitmap,
			final String transformation) {
		final XObjectImage xImage = new XObjectImage(mDocument, bitmap);
		final float imgRatio = (float) xImage.getWidth()
				/ (float) xImage.getHeight();
		final float boxRatio = (float) width / (float) height;
		float ratio;
		if (imgRatio < boxRatio) {
			ratio = (float) width / (float) xImage.getWidth();
		} else {
			ratio = (float) height / (float) xImage.getHeight();
		}
		width = (int) (xImage.getWidth() * ratio);
		height = (int) (xImage.getHeight() * ratio);
		mCurrentPage.addImage(fromLeft, fromBottom, width, height, xImage,
				transformation);
	}

	public String asString() {
		mPages.render();
		return mDocument.toPDFString();
	}
}
