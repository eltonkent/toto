package toto.doc.pdf;

class CrossReferenceTable extends List {

	private int mObjectNumberStart;

	CrossReferenceTable() {
		super();
		clear();
	}

	void setObjectNumberStart(final int Value) {
		mObjectNumberStart = Value;
	}

	int getObjectNumberStart() {
		return mObjectNumberStart;
	}

	private String getObjectsXRefInfo() {
		return renderList();
	}

	void addObjectXRefInfo(final int ByteOffset, final int Generation,
			final boolean InUse) {
		final StringBuilder sb = new StringBuilder();
		sb.append(String.format("%010d", ByteOffset));
		sb.append(" ");
		sb.append(String.format("%05d", Generation));
		if (InUse) {
			sb.append(" n ");
		} else {
			sb.append(" f ");
		}
		sb.append("\r\n");
		mList.add(sb.toString());
	}

	private String render() {
		final StringBuilder sb = new StringBuilder();
		sb.append("xref");
		sb.append("\r\n");
		sb.append(mObjectNumberStart);
		sb.append(" ");
		sb.append(mList.size());
		sb.append("\r\n");
		sb.append(getObjectsXRefInfo());
		return sb.toString();
	}

	@Override
	String toPDFString() {
		return render();
	}

	@Override
	void clear() {
		super.clear();
		addObjectXRefInfo(0, 65536, false); // free objects linked list head
		mObjectNumberStart = 0;
	}

}
