package toto.doc.pdf;

class Trailer extends Base {

	private int mXRefByteOffset;
	private int mObjectsCount;
	private String mId;
	private Dictionary mTrailerDictionary;

	Trailer() {
		clear();
	}

	void setId(final String Value) {
		mId = Value;
	}

	void setCrossReferenceTableByteOffset(final int Value) {
		mXRefByteOffset = Value;
	}

	void setObjectsCount(final int Value) {
		mObjectsCount = Value;
	}

	void renderDictionary() {
		mTrailerDictionary.setContent("  /Size "
				+ Integer.toString(mObjectsCount));
		mTrailerDictionary.addNewLine();
		mTrailerDictionary.addContent("  /Root 1 0 R");
		mTrailerDictionary.addNewLine();
		mTrailerDictionary.addContent("  /ID [<" + mId + "> <" + mId + ">]");
		mTrailerDictionary.addNewLine();
	}

	private String render() {
		renderDictionary();
		final StringBuilder sb = new StringBuilder();
		sb.append("trailer");
		sb.append("\n");
		sb.append(mTrailerDictionary.toPDFString());
		sb.append("startxref");
		sb.append("\n");
		sb.append(mXRefByteOffset);
		sb.append("\n");
		sb.append("%%EOF");
		sb.append("\n");
		return sb.toString();
	}

	@Override
	String toPDFString() {
		return render();
	}

	@Override
	void clear() {
		mXRefByteOffset = 0;
		mTrailerDictionary = new Dictionary();
	}
}
