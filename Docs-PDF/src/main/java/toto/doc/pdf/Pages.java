package toto.doc.pdf;

import java.util.ArrayList;

class Pages {

	private final PDFDocument mDocument;
	private final ArrayList<Page> mPageList;
	private final IndirectObject mIndirectObject;
	private final Array mMediaBox;
	private final Array mKids;

	Pages(final PDFDocument document, final int pageWidth, final int pageHeight) {
		mDocument = document;
		mIndirectObject = mDocument.newIndirectObject();
		mPageList = new ArrayList<Page>();
		mMediaBox = new Array();
		final String content[] = { "0", "0", Integer.toString(pageWidth),
				Integer.toString(pageHeight) };
		mMediaBox.addItemsFromStringArray(content);
		mKids = new Array();
	}

	IndirectObject getIndirectObject() {
		return mIndirectObject;
	}

	Page newPage() {
		final Page lPage = new Page(mDocument);
		mPageList.add(lPage);
		mKids.addItem(lPage.getIndirectObject().getIndirectReference());
		return lPage;
	}

	Page getPageAt(final int position) {
		return mPageList.get(position);
	}

	int getCount() {
		return mPageList.size();
	}

	void render() {
		mIndirectObject.setDictionaryContent("  /Type /Pages\n"
				+ "  /MediaBox " + mMediaBox.toPDFString() + "\n" + "  /Count "
				+ Integer.toString(mPageList.size()) + "\n" + "  /Kids "
				+ mKids.toPDFString() + "\n");
		for (final Page lPage : mPageList) {
			lPage.render(mIndirectObject.getIndirectReference());
		}
	}
}
