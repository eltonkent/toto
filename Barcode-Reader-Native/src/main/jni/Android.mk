LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libiconv 

LIBICONV := libiconv

LOCAL_CFLAGS := -I$(LOCAL_PATH)/$(LIBICONV)
LOCAL_SRC_FILES := iconv.c

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := BarcodeReader
LOCAL_SRC_FILES := zbar/convert.c \
		zbar/decoder.c \
		zbar/error.c \
		zbar/image.c \
		zbar/img_scanner.c \
        zbar/refcnt.c \
        zbar/scanner.c \
        zbar/symbol.c \
        \
        zbar/qrcode/bch15_5.c \
        zbar/qrcode/binarize.c \
        zbar/qrcode/isaac.c \
        zbar/qrcode/qrdec.c \
        zbar/qrcode/qrdectxt.c \
        zbar/qrcode/rs.c \
        zbar/qrcode/util.c \
        \
        zbar/processor/null.c \
        zbar/decoder/qr_finder.c \
        \
        zbar/decoder/code128.c \
        zbar/decoder/code39.c \
        zbar/decoder/code93.c \
        zbar/decoder/ean.c \
        zbar/decoder/databar.c \
        zbar/decoder/i25.c \
        \
        BarcodeReader.c
		
LOCAL_CPPFLAGS += -ffunction-sections -fdata-sections -fvisibility=hidden
LOCAL_CFLAGS += -ffunction-sections -fdata-sections -I$(LOCAL_PATH) -I$(LOCAL_PATH)/include -I$(LOCAL_PATH)/zbar

#remove duplicate code.
ifeq ($(TARGET_ARCH),mips)
  LOCAL_LDFLAGS += -Wl,--gc-sections
else
  LOCAL_LDFLAGS += -Wl,--gc-sections,--icf=safe
endif

LOCAL_LDLIBS := -llog

LOCAL_STATIC_LIBRARIES := libiconv

include $(BUILD_SHARED_LIBRARY)
