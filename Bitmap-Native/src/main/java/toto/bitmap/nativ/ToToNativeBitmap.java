
package toto.bitmap.nativ;

import android.graphics.Bitmap;
import android.graphics.Rect;
import toto.bitmap.ToToBitmap;
import com.mi.toto.ToTo;

/**
 * Java representation of a native Leptonica PIX object.
 *
 */
public class ToToNativeBitmap  implements ToToBitmap {
    /** Index of the image width within the dimensions array. */
    public static final int INDEX_W = 0;
    /** Index of the image height within the dimensions array. */
    public static final int INDEX_H = 1;
    /** Index of the image bit-depth within the dimensions array. */
    public static final int INDEX_D = 2;

    static {
        ToTo.loadLib("Bitmap_Native");
    }

    /** Package-accessible pointer to native pix */
    private final long mNativePix;

    private boolean mRecycled;

    private int filterX;

    private int filterY;

    private int filterWidth;

    private int filterHeight;

    /**
     * Creates a new Pix wrapper for the specified native PIX object.
     *
     * @param nativePix A pointer to the native PIX object.
     */
    public ToToNativeBitmap(long nativePix) {
        mNativePix = nativePix;
        mRecycled = false;
    }

    public ToToNativeBitmap(int width, int height, int depth) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Pix width and height must be > 0");
        } else if (depth != 1 && depth != 2 && depth != 4 && depth != 8 && depth != 16
                && depth != 24 && depth != 32) {
            throw new IllegalArgumentException("Depth must be one of 1, 2, 4, 8, 16, or 32");
        }

        mNativePix = nativeCreatePix(width, height, depth);
        mRecycled = false;
    }

    /**
     * Creates a new Pix from raw Pix data obtained from getData().
     *
     * @param pixData Raw pix data obtained from getData().
     * @param width The width of the original Pix.
     * @param height The height of the original Pix.
     * @param depth The bit-depth of the original Pix.
     * @return a new Pix or <code>null</code> on error
     */
    public static ToToNativeBitmap createFromPix(byte[] pixData, int width, int height, int depth) {
        long nativePix = nativeCreateFromData(pixData, width, height, depth);

        if (nativePix == 0) {
            throw new OutOfMemoryError();
        }

        return new ToToNativeBitmap(nativePix);
    }

    public static ToToNativeBitmap createFromBitmap(final Bitmap bitmap) {
        return ReadFile.readBitmap(bitmap);
    }

    private static native int nativeGetRefCount(long nativePix);

    private static native long nativeCreatePix(int w, int h, int d);

    private static native long nativeCreateFromData(byte[] data, int w, int h, int d);

    private static native byte[] nativeGetData(long nativePix);

    private static native long nativeClone(long nativePix);

    private static native long nativeCopy(long nativePix);

    private static native boolean nativeInvert(long nativePix);

    private static native void nativeDestroy(long nativePix);

    private static native boolean nativeGetDimensions(long nativePix, int[] dimensions);

    private static native int nativeGetWidth(long nativePix);

    private static native int nativeGetHeight(long nativePix);

    private static native int nativeGetDepth(long nativePix);

    private static native int nativeGetPixel(long nativePix, int x, int y);

    private static native void nativeSetPixel(long nativePix, int x, int y, int color);

    /**
     * Returns a pointer to the native Pix object. This is used by native code
     * and is only valid within the same process in which the Pix was created.
     *
     * @return a native pointer to the Pix object
     */
    public long getNativePix() {
        if (mRecycled)
            throw new IllegalStateException();

        return mNativePix;
    }

    // ***************
    // * NATIVE CODE *
    // ***************

    /**
     * Return the raw bytes of the native PIX object. You can reconstruct the
     * Pix from this data using createFromPix().
     *
     * @return a copy of this PIX object's raw data
     */
    public byte[] getData() {
        if (mRecycled)
            throw new IllegalStateException();

        byte[] buffer = nativeGetData(mNativePix);

        if (buffer == null) {
            throw new RuntimeException("native getData failed");
        }

        return buffer;
    }

    /**
     * Returns an array of this image's dimensions. See Pix.INDEX_* for indices.
     *
     * @return an array of this image's dimensions or <code>null</code> on
     *         failure
     */
    public int[] getDimensions() {
        if (mRecycled)
            throw new IllegalStateException();

        int[] dimensions = new int[3];

        if (getDimensions(dimensions)) {
            return dimensions;
        }

        return null;
    }

    /**
     * Fills an array with this image's dimensions. The array must be at least 3
     * elements long.
     *
     * @param dimensions An integer array with at least three elements.
     * @return <code>true</code> on success
     */
    public boolean getDimensions(int[] dimensions) {
        if (mRecycled)
            throw new IllegalStateException();

        return nativeGetDimensions(mNativePix, dimensions);
    }

    /**
     * Returns a clone of this Pix. This does NOT create a separate copy, just a
     * new pointer that can be recycled without affecting other clones.
     *
     * @return a clone (shallow copy) of the Pix
     */
    @Override
    public ToToNativeBitmap clone() {
        if (mRecycled)
            throw new IllegalStateException();

        long nativePix = nativeClone(mNativePix);

        if (nativePix == 0) {
            throw new OutOfMemoryError();
        }

        return new ToToNativeBitmap(nativePix);
    }

    /**
     * Returns a deep copy of this Pix that can be modified without affecting
     * the original Pix.
     *
     * @return a copy of the Pix
     */
    public ToToNativeBitmap copy() {
        if (mRecycled)
            throw new IllegalStateException();

        long nativePix = nativeCopy(mNativePix);

        if (nativePix == 0) {
            throw new OutOfMemoryError();
        }

        return new ToToNativeBitmap(nativePix);
    }

    /**
     * Inverts this Pix in-place.
     *
     * @return <code>true</code> on success
     */
    public boolean invert() {
        if (mRecycled)
            throw new IllegalStateException();

        return nativeInvert(mNativePix);
    }

    /**
     * Releases resources and frees any memory associated with this Pix. You may
     * not modify or access the pix after calling this method.
     */
    public void recycle() {
        if (!mRecycled) {
            nativeDestroy(mNativePix);

            mRecycled = true;
        }
    }

    /**
     * Returns a Rect with the width and height of this Pix.
     *
     * @return a Rect with the width and height of this Pix
     */
    public Rect getRect() {
        int w = getWidth();
        int h = getHeight();

        return new Rect(0, 0, w, h);
    }

    /**
     * Returns the width of this Pix.
     *
     * @return the width of this Pix
     */
    public int getWidth() {
        if (mRecycled)
            throw new IllegalStateException();

        return nativeGetWidth(mNativePix);
    }

    /**
     * Returns the height of this Pix.
     *
     * @return the height of this Pix
     */
    public int getHeight() {
        if (mRecycled)
            throw new IllegalStateException();

        return nativeGetHeight(mNativePix);
    }

    /**
     * Returns the depth of this Pix.
     *
     * @return the depth of this Pix
     */
    public int getDepth() {
        if (mRecycled)
            throw new IllegalStateException();

        return nativeGetDepth(mNativePix);
    }

    public int getRefCount() {
        return nativeGetRefCount(mNativePix);
    }

    /**
     * Returns the {@link android.graphics.Color} at the specified location.
     *
     * @param x The x coordinate (0...width-1) of the pixel to return.
     * @param y The y coordinate (0...height-1) of the pixel to return.
     * @return The argb {@link android.graphics.Color} at the specified
     *         coordinate.
     * @throws IllegalArgumentException If x, y exceeds the image bounds.
     */
    public int getPixel(int x, int y) {
        if (mRecycled)
            throw new IllegalStateException();

        if (x < 0 || x >= getWidth()) {
            throw new IllegalArgumentException("Supplied x coordinate exceeds image bounds");
        } else if (y < 0 || y >= getHeight()) {
            throw new IllegalArgumentException("Supplied y coordinate exceeds image bounds");
        }

        return nativeGetPixel(mNativePix, x, y);
    }

    /**
     * Sets the {@link android.graphics.Color} at the specified location.
     *
     * @param x The x coordinate (0...width-1) of the pixel to set.
     * @param y The y coordinate (0...height-1) of the pixel to set.
     * @param color The argb {@link android.graphics.Color} to set at the
     *            specified coordinate.
     * @throws IllegalArgumentException If x, y exceeds the image bounds.
     */
    public void setPixel(int x, int y, int color) {
        if (mRecycled)
            throw new IllegalStateException();

        if (x < 0 || x >= getWidth()) {
            throw new IllegalArgumentException("Supplied x coordinate exceeds image bounds");
        } else if (y < 0 || y >= getHeight()) {
            throw new IllegalArgumentException("Supplied y coordinate exceeds image bounds");
        }

        nativeSetPixel(mNativePix, x, y, color);
    }

    @Override
    public Rect getFilterBounds() {
        return new Rect(filterX, filterY, filterWidth, filterHeight);
    }

    /**
     * Set the bounds for any filter operations. the default bounds are the
     * width and height of the bitmap
     *
     * @param x
     * @param y
     * @param width
     *            width of the filter bounds. if the width is greater than the
     *            bitmap width, then the bitmap width is used.
     * @param height
     *            height of the filter bounds. if the height is greater than the
     *            bitmap height, then the bitmap width is height.
     */
    @Override
    public void setFilterBounds(final int x, final int y, final int width,
                                final int height) {
        if (x < 0 || y < 0 || width < 0 || height < 0) {
            throw new IllegalArgumentException("Negative filter bounds");
        }
        if (x > width || y > height) {
            throw new IllegalArgumentException(
                    "X/Y corrdinate is greater than Width/Height");
        }
        this.filterX = x;
        this.filterY = y;
        this.filterHeight = ((filterY + height) > this.getHeight()) ? this
                .getHeight() : filterY + height;
        this.filterWidth = ((filterX + width) > this.getWidth()) ? this
                .getWidth() : filterX + width;

    }

    @Override
    public int getFilterHeight() {
        return filterHeight;
    }

    @Override
    public int getFilterWidth() {
        return filterWidth;
    }

    @Override
    public int getFilterX() {
        return filterX;
    }

    @Override
    public int getFilterY() {
        return filterY;
    }

    @Override
    public Bitmap getBitmap() {
        final int w = getWidth();
        final int h = getHeight();
        int x, y, pixel;
        final Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
        for (y = 0; y < h; y++) {
            for (x = 0; x < w; x++) {
                pixel = getPixel(x, y);
                bitmap.setPixel(x, y, pixel);
            }
        }
        return bitmap;
    }

    @Override
    public int[] getPixels() {
        final int w = getWidth();
        final int h = getHeight();
        int y, x;
        final int[] pixels = new int[w * h];
        for (y = 0; y < h; y++) {
            for (x = 0; x < w; x++) {
                pixels[y * w + x] = getPixel(x, y);
            }
        }
        return pixels;
    }

    @Override
    public void setPixels(final int[] pixels) {
        setPixels(pixels, getWidth(), getHeight());

    }

    @Override
    public void setPixels(final int[] pixels, final int w, final int h) {
        int y, x;
        for (y = 0; y < h; y++) {
            for (x = 0; x < w; x++) {
                setPixel(x, y, pixels[y * w + x]);
            }
        }

    }
}
