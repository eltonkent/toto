
package toto.bitmap.nativ;

import com.mi.toto.ToTo;

/**
 * Image bit-depth conversion methods.
 * 
 * @author alanv@google.com (Alan Viverette)
 */
public class Convert {
    static {
        ToTo.loadLib("Bitmap_Native");
    }
    
    /**
     * Converts an image of any bit depth to 8-bit grayscale.
     *
     * @param pixs Source pix of any bit-depth.
     * @return a new Pix image or <code>null</code> on error
     */
    public static ToToNativeBitmap convertTo8(ToToNativeBitmap pixs) {
        if (pixs == null)
            throw new IllegalArgumentException("Source pix must be non-null");

        long nativePix = nativeConvertTo8(pixs.getNativePix());

        if (nativePix == 0)
            throw new RuntimeException("Failed to natively convert pix");

        return new ToToNativeBitmap(nativePix);
    }

    // ***************
    // * NATIVE CODE *
    // ***************

    private static native long nativeConvertTo8(long nativePix);
}
