
package toto.bitmap.nativ;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.mi.toto.ToTo;

import java.io.File;

/**
 * Image input and output methods.
 *
 * @author alanv@google.com (Alan Viverette)
 */
public class ReadFile {
    private static final String LOG_TAG = ReadFile.class.getSimpleName();

    static {
        ToTo.loadLib("Bitmap_Native");
    }

    /**
     * Creates a 32bpp ToToNativeBitmap object from encoded data. Supported formats are BMP,
     * JPEG, and PNG.
     *
     * @param encodedData BMP, JPEG, or PNG encoded byte data.
     * @return a 32bpp ToToNativeBitmap object
     */
    public static ToToNativeBitmap readMem(byte[] encodedData) {
        if (encodedData == null) {
            Log.e(LOG_TAG, "Image data byte array must be non-null");
            return null;
        }

        final BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.ARGB_8888;

        final Bitmap bmp = BitmapFactory.decodeByteArray(encodedData, 0, encodedData.length,
                opts);
        final ToToNativeBitmap pix = readBitmap(bmp);

        bmp.recycle();

        return pix;
    }

    /**
     * Creates an 8bpp ToToNativeBitmap object from raw 8bpp grayscale pixels.
     *
     * @param pixelData 8bpp grayscale pixel data.
     * @param width     The width of the input image.
     * @param height    The height of the input image.
     * @return an 8bpp ToToNativeBitmap object
     */
    public static ToToNativeBitmap readBytes8(byte[] pixelData, int width, int height) {
        if (pixelData == null)
            throw new IllegalArgumentException("Byte array must be non-null");
        if (width <= 0)
            throw new IllegalArgumentException("Image width must be greater than 0");
        if (height <= 0)
            throw new IllegalArgumentException("Image height must be greater than 0");
        if (pixelData.length < width * height)
            throw new IllegalArgumentException("Array length does not match dimensions");

        long nativePix = nativeReadBytes8(pixelData, width, height);

        if (nativePix == 0)
            throw new RuntimeException("Failed to read pix from memory");

        return new ToToNativeBitmap(nativePix);
    }

    /**
     * Replaces the bytes in an 8bpp ToToNativeBitmap object with raw grayscale 8bpp pixels.
     * Width and height be identical to the input ToToNativeBitmap.
     *
     * @param pixs      The ToToNativeBitmap whose bytes will be replaced.
     * @param pixelData 8bpp grayscale pixel data.
     * @param width     The width of the input image.
     * @param height    The height of the input image.
     * @return an 8bpp ToToNativeBitmap object
     */
    public static boolean replaceBytes8(ToToNativeBitmap pixs, byte[] pixelData, int width, int height) {
        if (pixs == null)
            throw new IllegalArgumentException("Source pix must be non-null");
        if (pixelData == null)
            throw new IllegalArgumentException("Byte array must be non-null");
        if (width <= 0)
            throw new IllegalArgumentException("Image width must be greater than 0");
        if (height <= 0)
            throw new IllegalArgumentException("Image height must be greater than 0");
        if (pixelData.length < width * height)
            throw new IllegalArgumentException("Array length does not match dimensions");
        if (pixs.getWidth() != width)
            throw new IllegalArgumentException("Source pix width does not match image width");
        if (pixs.getHeight() != height)
            throw new IllegalArgumentException("Source pix height does not match image height");

        return nativeReplaceBytes8(pixs.getNativePix(), pixelData, width,
                height);
    }

    /**
     * Creates a ToToNativeBitmap object from encoded file data. Supported formats are BMP,
     * JPEG, and PNG.
     *
     * @param file The BMP, JPEG, or PNG-encoded file to read in as a ToToNativeBitmap.
     * @return a ToToNativeBitmap object
     */
    public static ToToNativeBitmap readFile(File file) {
        if (file == null) {
            Log.e(LOG_TAG, "File must be non-null");
            return null;
        }
        if (!file.exists()) {
            Log.e(LOG_TAG, "File does not exist");
            return null;
        }
        if (!file.canRead()) {
            Log.e(LOG_TAG, "Cannot read file");
            return null;
        }

        final long nativePix = nativeReadFile(file.getAbsolutePath());

        if (nativePix != 0) {
            return new ToToNativeBitmap(nativePix);
        }

        final BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.ARGB_8888;

        final Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
        if (bmp == null) {
            Log.e(LOG_TAG, "Cannot decode bitmap");
            return null;
        }
        final ToToNativeBitmap pix = readBitmap(bmp);

        bmp.recycle();

        return pix;
    }

    /**
     * Creates a ToToNativeBitmap object from Bitmap data. Currently supports only
     * ARGB_8888-formatted bitmaps.
     *
     * @param bmp The Bitmap object to convert to a ToToNativeBitmap.
     * @return a ToToNativeBitmap object
     */
    public static ToToNativeBitmap readBitmap(Bitmap bmp) {
        if (bmp == null) {
            Log.e(LOG_TAG, "Bitmap must be non-null");
            return null;
        }
        if (bmp.getConfig() != Bitmap.Config.ARGB_8888) {
            Log.e(LOG_TAG, "Bitmap config must be ARGB_8888");
            return null;
        }

        long nativePix = nativeReadBitmap(bmp);

        if (nativePix == 0) {
            Log.e(LOG_TAG, "Failed to read pix from bitmap");
            return null;
        }

        return new ToToNativeBitmap(nativePix);
    }

    public static ToToNativeBitmap readPixels(final int[] pixels,
                                              final int w, final int height) {
        final int nativePix = nativeReadPixels(pixels, w, w);
        if (nativePix == 0)
            throw new RuntimeException("Failed to create bitmap from pixels");
        return new ToToNativeBitmap(nativePix);
    }


    // ***************
    // * NATIVE CODE *
    // ***************

    private static native long nativeReadMem(byte[] data, int size);

    private static native long nativeReadBytes8(byte[] data, int w, int h);

    private static native boolean nativeReplaceBytes8(long nativePix, byte[] data, int w, int h);

    private static native long nativeReadFile(String filename);

    private static native long nativeReadBitmap(Bitmap bitmap);

    private static native int nativeReadPixels(int[] pixels, int width,
                                               int height);
}
