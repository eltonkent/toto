
package toto.bitmap.nativ;

import com.mi.toto.ToTo;

/**
 * Extract rectangular regions.
 */
public class Clip {
    static {
        ToTo.loadLib("Bitmap_Native");
    }

    /**
     * Extract a region from a Pix.
     * <p>
     * Notes:
     * <p>
     * This should be simple, but there are choices to be made. The box is
     * defined relative to the pix coordinates.  However, if the box is not
     * contained within the pix, we have two choices:
     * <p>
     * <p>     (1) clip the box to the pix
     * <p>     (2) make a new pix equal to the full box dimensions,
     *             but let rasterop do the clipping and positioning
     *             of the src with respect to the dest
     * <p>
     * Choice (2) immediately brings up the problem of what pixel values
     * to use that were not taken from the src.  For example, on a grayscale
     * image, do you want the pixels not taken from the src to be black
     * or white or something else?  To implement choice 2, one needs to
     * specify the color of these extra pixels.
     * <p>
     * So we adopt (1), and clip the box first, if necessary,
     * before making the dest pix and doing the rasterop.  But there
     * is another issue to consider.  If you want to paste the
     * clipped pix back into pixs, it must be properly aligned, and
     * it is necessary to use the clipped box for alignment.
     *
     * @param source Source pix
     * @param box Requested clipping region
     * @return clipped pix, or null if rectangle doesn't intersect source pix
     */
    public static ToToNativeBitmap clipRectangle(ToToNativeBitmap source, Box box) {
        int result = nativeClipRectangle(source.getNativePix(),
                box.getNativeBox());
        if (result != 0) {
            return new ToToNativeBitmap(result);
        }
        return null;
    }

    // ***************
    // * NATIVE CODE *
    // ***************

    private static native int nativeClipRectangle(long nativePix, long nativeBox);
}