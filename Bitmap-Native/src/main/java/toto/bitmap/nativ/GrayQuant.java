
package toto.bitmap.nativ;

import com.mi.toto.ToTo;

public class GrayQuant {
    static {
        ToTo.loadLib("Bitmap_Native");
    }

    /**
     * Perform simple (pixelwise) binarization with fixed threshold
     * <p>
     * Notes:
     * <ol>
     * <li> If the source pixel is less than the threshold value, the dest will 
     * be 1; otherwise, it will be 0
     * </ol>
     *
     * @param pixs Source pix (4 or 8 bpp)
     * @param thresh Threshold value
     * @return a new Pix image, 1 bpp
     */
    public static ToToNativeBitmap pixThresholdToBinary(ToToNativeBitmap pixs, int thresh) {
        if (pixs == null)
            throw new IllegalArgumentException("Source pix must be non-null");
        int depth = pixs.getDepth();
        if (depth != 4 && depth != 8)
            throw new IllegalArgumentException("Source pix depth must be 4 or 8 bpp");
        if (depth == 4 && thresh > 16)
            throw new IllegalArgumentException("4 bpp thresh not in {0-16}");
        if (depth == 8 && thresh > 256)
            throw new IllegalArgumentException("8 bpp thresh not in {0-256}");

        long nativePix = nativePixThresholdToBinary(pixs.getNativePix(), 
                thresh);

        if (nativePix == 0)
            throw new RuntimeException("Failed to perform binarization");

        return new ToToNativeBitmap(nativePix);
    }

    // ***************
    // * NATIVE CODE *
    // ***************

    private static native long nativePixThresholdToBinary(long nativePix, int thresh);
}
