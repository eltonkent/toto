
package toto.bitmap.nativ;

import com.mi.toto.ToTo;

/**
 * Image sharpening methods.
 *
 */
public class Enhance {
    public final static int DEFAULT_UNSHARP_HALFWIDTH = 1;

    // Unsharp masking constants
    public final static float DEFAULT_UNSHARP_FRACTION = 0.3f;
    
    static {
        ToTo.loadLib("Bitmap_Native");
    }
    
    /**
     * Performs unsharp masking (edge enhancement) using default values.
     * 
     * @see #unsharpMasking(ToToNativeBitmap, int, float)
     * 
     * @param pixs Source image
     * @return an edge-enhanced Pix image or copy if no enhancement requested
     */
    public static ToToNativeBitmap unsharpMasking(ToToNativeBitmap pixs) {
        return unsharpMasking(pixs, DEFAULT_UNSHARP_HALFWIDTH, 
                DEFAULT_UNSHARP_FRACTION);
    }
    
    /**
     * Performs unsharp masking (edge enhancement).
     * <p>
     * Notes:
     * <ul>
     * <li>We use symmetric smoothing filters of odd dimension, typically use
     * sizes of 3, 5, 7, etc. The <code>halfwidth</code> parameter for these is
     * (size - 1)/2; i.e., 1, 2, 3, etc.</li>
     * <li>The <code>fract</code> parameter is typically taken in the range: 0.2
     * &lt; <code>fract</code> &lt; 0.7</li>
     * </ul>
     *
     * @param halfwidth The half-width of the smoothing filter.
     * @param fraction The fraction of edge to be added back into the source
     *            image.
     * @return an edge-enhanced Pix image or copy if no enhancement requested
     */
    public static ToToNativeBitmap unsharpMasking(ToToNativeBitmap pixs, int halfwidth, float fraction) {
        if (pixs == null)
            throw new IllegalArgumentException("Source pix must be non-null");

        long nativePix = nativeUnsharpMasking(pixs.getNativePix(), halfwidth, 
                fraction);

        if (nativePix == 0) {
            throw new OutOfMemoryError();
        }

        return new ToToNativeBitmap(nativePix);
    }

    // ***************
    // * NATIVE CODE *
    // ***************

    private static native long nativeUnsharpMasking(long nativePix, int halfwidth, float fract);
}
