
package toto.bitmap.nativ;

import com.mi.toto.ToTo;

/**
 */
public class Rotate {
    /** Default rotation quality is high. */
    public static final boolean ROTATE_QUALITY = true;


    // Rotation default

    static {
        ToTo.loadLib("Bitmap_Native");
    }

    /**
     * Performs rotation using the default parameters.
     *
     * @param pixs The source pix.
     * @param degrees The number of degrees to rotate; clockwise is positive.
     * @return the rotated source image
     */
    public static ToToNativeBitmap rotate(ToToNativeBitmap pixs, float degrees) {
        return rotate(pixs, degrees, false);
    }

    /**
     * Performs rotation with resizing using the default parameters.
     *
     * @param pixs The source pix.
     * @param degrees The number of degrees to rotate; clockwise is positive.
     * @param quality Whether to use high-quality rotation.
     * @return the rotated source image
     */
    public static ToToNativeBitmap rotate(ToToNativeBitmap pixs, float degrees, boolean quality) {
        return rotate(pixs, degrees, quality, true);
    }

    /**
     * Performs basic image rotation about the center.
     * <p>
     * Notes:
     * <ol>
     * <li>Rotation is about the center of the image.
     * <li>For very small rotations, just return a clone.
     * <li>Rotation brings either white or black pixels in from outside the
     * image.
     * <li>Above 20 degrees, if rotation by shear is requested, we rotate by
     * sampling.
     * <li>Colormaps are removed for rotation by area map and shear.
     * <li>The dest can be expanded so that no image pixels are lost. To invoke
     * expansion, input the original width and height. For repeated rotation,
     * use of the original width and height allows the expansion to stop at the
     * maximum required size, which is a square with side = sqrt(w*w + h*h).
     * </ol>
     *
     * @param pixs The source pix.
     * @param degrees The number of degrees to rotate; clockwise is positive.
     * @param quality Whether to use high-quality rotation.
     * @param resize Whether to expand the output so that no pixels are lost.
     *         <strong>Note:</strong> 1bpp images are always resized when
     *         quality is {@code true}.
     * @return the rotated source image
     */
    public static ToToNativeBitmap rotate(ToToNativeBitmap pixs, float degrees, boolean quality, boolean resize) {
        if (pixs == null)
            throw new IllegalArgumentException("Source pix must be non-null");

        long nativePix = nativeRotate(pixs.getNativePix(), degrees, quality, 
                resize);

        if (nativePix == 0)
            return null;

        return new ToToNativeBitmap(nativePix);
    }

    /**
     * Performs top-level rotation by multiples of 90 degrees.
     *
     * @param pixs The source pix (all depths)
     * @param quads 0-3; number of 90 degree cw rotations
     * @return the rotated source image
     */
    public static ToToNativeBitmap rotateOrth(ToToNativeBitmap pixs, int quads) {
        if (pixs == null)
            throw new IllegalArgumentException("Source pix must be non-null");
        if (quads < 0 || quads > 3)
            throw new IllegalArgumentException("quads not in {0,1,2,3}");

        int nativePix = nativeRotateOrth(pixs.getNativePix(), quads);

        if (nativePix == 0)
            return null;

        return new ToToNativeBitmap(nativePix);
    }

    // ***************
    // * NATIVE CODE *
    // ***************

    private static native int nativeRotateOrth(long nativePix, int quads);

    private static native long nativeRotate(long nativePix, float degrees, boolean quality,
            boolean resize);
}
