/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.graphics._2D;

import toto.graphics.color.RGBUtils;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

public class Graphics2DUtils {

	/**
	 * Elliptical arc implementation based on the SVG specification notes
	 * Adapted from the Batik library (Apache-2 license) by SAU
	 * 
	 */
	public static void drawArc(final Path path, final double x0,
			final double y0, final double x, final double y, double rx,
			double ry, double angle, final boolean largeArcFlag,
			final boolean sweepFlag) {
		final double dx2 = (x0 - x) / 2.0;
		final double dy2 = (y0 - y) / 2.0;
		angle = Math.toRadians(angle % 360.0);
		final double cosAngle = Math.cos(angle);
		final double sinAngle = Math.sin(angle);

		final double x1 = (cosAngle * dx2 + sinAngle * dy2);
		final double y1 = (-sinAngle * dx2 + cosAngle * dy2);
		rx = Math.abs(rx);
		ry = Math.abs(ry);

		double Prx = rx * rx;
		double Pry = ry * ry;
		final double Px1 = x1 * x1;
		final double Py1 = y1 * y1;

		// check that radii are large enough
		final double radiiCheck = Px1 / Prx + Py1 / Pry;
		if (radiiCheck > 1) {
			rx = Math.sqrt(radiiCheck) * rx;
			ry = Math.sqrt(radiiCheck) * ry;
			Prx = rx * rx;
			Pry = ry * ry;
		}

		// Step 2 : Compute (cx1, cy1)
		double sign = (largeArcFlag == sweepFlag) ? -1 : 1;
		double sq = ((Prx * Pry) - (Prx * Py1) - (Pry * Px1))
				/ ((Prx * Py1) + (Pry * Px1));
		sq = (sq < 0) ? 0 : sq;
		final double coef = (sign * Math.sqrt(sq));
		final double cx1 = coef * ((rx * y1) / ry);
		final double cy1 = coef * -((ry * x1) / rx);

		final double sx2 = (x0 + x) / 2.0;
		final double sy2 = (y0 + y) / 2.0;
		final double cx = sx2 + (cosAngle * cx1 - sinAngle * cy1);
		final double cy = sy2 + (sinAngle * cx1 + cosAngle * cy1);

		// Step 4 : Compute the angleStart (angle1) and the angleExtent (dangle)
		final double ux = (x1 - cx1) / rx;
		final double uy = (y1 - cy1) / ry;
		final double vx = (-x1 - cx1) / rx;
		final double vy = (-y1 - cy1) / ry;
		double p, n;

		// Compute the angle start
		n = Math.sqrt((ux * ux) + (uy * uy));
		p = ux; // (1 * ux) + (0 * uy)
		sign = (uy < 0) ? -1.0 : 1.0;
		double angleStart = Math.toDegrees(sign * Math.acos(p / n));

		// Compute the angle extent
		n = Math.sqrt((ux * ux + uy * uy) * (vx * vx + vy * vy));
		p = ux * vx + uy * vy;
		sign = (ux * vy - uy * vx < 0) ? -1.0 : 1.0;
		double angleExtent = Math.toDegrees(sign * Math.acos(p / n));
		if (!sweepFlag && angleExtent > 0) {
			angleExtent -= 360f;
		} else if (sweepFlag && angleExtent < 0) {
			angleExtent += 360f;
		}
		angleExtent %= 360f;
		angleStart %= 360f;

		final RectF oval = new RectF((float) (cx - rx), (float) (cy - ry),
				(float) (cx + rx), (float) (cy + ry));
		path.addArc(oval, (float) angleStart, (float) angleExtent);
	}

	/**
	 * Add glow effect to the given {@link Paint} object
	 * <p>
	 * This paint object should be painted over a normal paint object to create
	 * a glow
	 * </p>
	 * 
	 * @param paint
	 * @param glowColor
	 *            color for the glow
	 * @param glowRadius
	 *            radius of the glow
	 */
	public static void makePaintGlow(final Paint paint, final int glowColor,
			final float glowRadius) {
		paint.setColor(glowColor);
		paint.setMaskFilter(new BlurMaskFilter(glowRadius,
				BlurMaskFilter.Blur.NORMAL));
	}

	/**
	 * Draw a 3D rectangle
	 * <p>
	 * <table>
	 * <tr>
	 * <td><img src="../../../resources/3drect.png"></td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <code>highLightColor</code>=0xADAAAD<br/>
	 * <code>shadowColor</code>=0xff0000 <br/>
	 * <code>isRaised</code>=true<br/>
	 * <code>lineWidth</code>=15<br/>
	 * <td>
	 * </tr>
	 * </table>
	 * </p>
	 * 
	 * @param canvas
	 *            Graphics context
	 * @param highLightColor
	 * @param shadowColor
	 * @param lineWidth
	 *            of rectangle side
	 * @param isRaised
	 *            raised or lowered 3d rectangle
	 * @param x
	 *            coordinate of 3D rect
	 * @param y
	 *            coordiate of 3D rect
	 * @param width
	 *            of the 3D rect
	 * @param height
	 *            of the 3D rect
	 */
	public final static void draw3DRect(final Canvas canvas,
			int highLightColor, int shadowColor, final boolean isRaised,
			int lineWidth, float x, float y, final int width, final int height) {
		final Paint paint = new Paint();
		paint.setAntiAlias(true);

		float x2 = x + width - 1;
		float y2 = y + height - 1;
		final int BEVEL_LOWERED = 0x2000;
		final int ETCHED_IN = 0x8000;
		final int BEVEL_RAISED = 0x1000;
		final int nLowered = BEVEL_LOWERED | ETCHED_IN;
		final int nBevel = BEVEL_LOWERED | BEVEL_RAISED;
		final boolean bLowered = (isRaised ? 0x1000 : 0x2000 & nLowered) != 0;
		final boolean bBevelled = (isRaised ? 0x1000 : 0x2000 & nBevel) != 0;
		final int nHalfWidth = lineWidth / 2;

		while (lineWidth > 0) {
			paint.setColor(bLowered ? shadowColor : highLightColor);

			canvas.drawLine(x, y, x, y2, paint);
			canvas.drawLine(x, y, x2 - 1, y, paint);
			paint.setColor(bLowered ? highLightColor : shadowColor);
			canvas.drawLine(x2, y, x2, y2, paint);
			canvas.drawLine(x + 1, y2, x2, y2, paint);

			x++;
			y++;
			x2--;
			y2--;
			lineWidth--;
			if (bBevelled) {
				// soften the bevel by slightly fading the colors
				highLightColor = RGBUtils.adjustBrightness(highLightColor, 10);
				shadowColor = RGBUtils.adjustBrightness(shadowColor, 10);
			} else if (lineWidth == nHalfWidth) {
				final int tmp = highLightColor;

				highLightColor = shadowColor;
				shadowColor = tmp;
			}
		}
	}
}
