/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.xc.plist;

import java.io.IOException;

/**
 * A UID. Only found in binary property lists that are keyed archives.
 */
public class UID extends NSObject {

	private final byte[] bytes;
	private final String name;

	public UID(final String name, final byte[] bytes) {
		this.name = name;
		this.bytes = bytes;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public String getName() {
		return name;
	}

	/**
	 * There is no XML representation specified for UIDs. In this implementation
	 * UIDs are represented as strings in the XML output.
	 * 
	 * @param xml
	 *            The xml StringBuilder
	 * @param level
	 *            The indentation level
	 */
	@Override
	void toXML(final StringBuilder xml, final int level) {
		indent(xml, level);
		xml.append("<string>");
		xml.append(new String(bytes));
		xml.append("</string>");
	}

	@Override
	void toBinary(final BinaryPropertyListWriter out) throws IOException {
		out.write(0x80 + bytes.length - 1);
		out.write(bytes);
	}
}
