/**
 * Utilities to handle iOS's <b>Property List</b>(.plist) format.
 * <p>
 * Supported formats<a href="#Supported_formats" class="section_anchor"></a></h1><p><table class="wikitable"><tr><td style="border: 1px solid #ccc; padding: 5px;"> <strong>Format</strong> </td><td style="border: 1px solid #ccc; padding: 5px;"> <strong>Parse</strong> </td><td style="border: 1px solid #ccc; padding: 5px;"> <strong>Create</strong> </td></tr> <tr><td style="border: 1px solid #ccc; padding: 5px;"> OS X / iOS XML </td><td style="border: 1px solid #ccc; padding: 5px;"> yes </td><td style="border: 1px solid #ccc; padding: 5px;"> yes </td></tr> <tr><td style="border: 1px solid #ccc; padding: 5px;"> OS X / iOS Binary </td><td style="border: 1px solid #ccc; padding: 5px;"> yes<strong> </td><td style="border: 1px solid #ccc; padding: 5px;"> yes </td></tr> <tr><td style="border: 1px solid #ccc; padding: 5px;"> OS X / iOS ASCII </td><td style="border: 1px solid #ccc; padding: 5px;"> yes </td><td style="border: 1px solid #ccc; padding: 5px;"> no </td></tr> <tr><td style="border: 1px solid #ccc; padding: 5px;"> GNUstep ASCII </td><td style="border: 1px solid #ccc; padding: 5px;"> yes </td><td style="border: 1px solid #ccc; padding: 5px;"> no </td></tr> <tr><td style="border: 1px solid #ccc; padding: 5px;"> NeXTSTEP ASCII </td><td style="border: 1px solid #ccc; padding: 5px;"> yes </td><td style="border: 1px solid #ccc; padding: 5px;"> no </td></tr> </table>
 * <br/>
 * <b>Parsing a .Plist file</b><br/>
 * <code>
 * <pre>
 *  <font color="green">// parse an example plist file</font>
 *  NSObject x = PropertyListParser.parse(new File("test/test1.plist"));
 *  <font color="green">// check the data in it</font>
 *  NSDictionary d = (NSDictionary)x;
 *  </pre>
 *  </code>
 * </p>
 */
package toto.xc.plist;