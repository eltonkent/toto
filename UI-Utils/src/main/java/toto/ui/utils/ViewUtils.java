package toto.ui.utils;

import toto.ui.drawable.ViewPressedDrawable;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;

public class ViewUtils {
	/**
	 * Hide soft keyboard.
	 * 
	 * @param textView
	 *            text view containing current window token
	 */
	public static void hideSoftInput(final View textView) {
		try {
			final InputMethodManager imm = (InputMethodManager) textView
					.getContext()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the Centre of the View
	 * 
	 * @return The centre of the given view.
	 */
	public static int getCenter(final View view) {
		return view.getLeft() + view.getWidth() / 2;
	}

	public static void makeGlow(final TextView txtView, final int glowColor) {
		txtView.setShadowLayer(2, 0, 0, glowColor);
	}

	/**
	 * Show soft keyboard.
	 * 
	 * @param textView
	 *            text view containing current window token
	 */
	public static void showSoftInput(final View textView) {
		try {
			final InputMethodManager imm = (InputMethodManager) textView
					.getContext()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(textView, InputMethodManager.SHOW_FORCED);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Toggles keyboard visibility.
	 * 
	 * @param textView
	 *            text view containing current window token
	 */
	public static void toggleSoftInput(final View textView) {
		try {
			final InputMethodManager imm = (InputMethodManager) textView
					.getContext()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInputFromWindow(textView.getWindowToken(), 0, 0);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set text view value or change its visibility in case of empty value.
	 * 
	 * @param view
	 *            view instance
	 * @param text
	 *            text value
	 * @param hvisibility
	 *            visibility value
	 */
	public static void setTextOrHide(final TextView view,
			final CharSequence text, final int hvisibility) {
		if (TextUtils.isEmpty(text)) {
			view.setVisibility(hvisibility);
		} else {
			view.setText(text);
			view.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Get the given view copied onto a bitmap. Best used after the view is
	 * attached to a viewgroup or window.
	 * 
	 * @param view
	 * @param outputConfig
	 *            for the bitmap that is returned
	 * @return Bitmap of the given view, <code>null</code> if the view has a
	 *         width or height of 0.
	 */
	public static Bitmap getSnap(final View view,
			final Bitmap.Config outputConfig) {
		final int width = view.getWidth();
		final int height = view.getHeight();
		if (view != null && width > 0 && height > 0) {
			final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
					view.getHeight(), outputConfig);
			final android.graphics.Canvas canvas = new android.graphics.Canvas(
					bitmap);
			view.draw(canvas);
			return bitmap;
		}
		return null;
	}

	/**
	 * @param view
	 */
	public static void setPressedState(final View view) {
		final Drawable bg = view.getBackground();
		if (bg == null) {
			throw new IllegalStateException(
					"View needs to have a background drawable");
		}
		view.setBackgroundDrawable(new ViewPressedDrawable(bg));
	}

	/**
	 * Enable all view in the given View or ViewGroup
	 * 
	 * @param view
	 * @param enable
	 */
	public static void setEnabledAll(final View view, final boolean enable) {
		view.setEnabled(enable);
		if ((view instanceof ViewGroup)) {
			final ViewGroup localViewGroup = (ViewGroup) view;
			final int i = localViewGroup.getChildCount();
			for (int j = 0; j < i; j++)
				setEnabledAll(localViewGroup.getChildAt(j), enable);
		}
	}

	/**
	 * Set part of the text in a textview clickable.
	 * <p>
	 * The entire text needs to setKey to the textview before calling this
	 * method
	 * </p>
	 * 
	 * @param view
	 *            Textview to assign the clickage text to.
	 * @param clickableText
	 *            Text in the textview that needs to be made clickable.
	 * @param span
	 */
	public static void clickify(final TextView view,
			final String clickableText, final UIUtils.ClickSpan span) {

		final CharSequence text = view.getText();
		final String string = text.toString();

		final int start = string.indexOf(clickableText);
		final int end = start + clickableText.length();
		if (start == -1)
			return;

		if (text instanceof Spannable) {
			((Spannable) text).setSpan(span, start, end,
					Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		} else {
			final SpannableString s = SpannableString.valueOf(text);
			s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			view.setText(s);
		}

		final MovementMethod m = view.getMovementMethod();
		if ((m == null) || !(m instanceof LinkMovementMethod)) {
			view.setMovementMethod(LinkMovementMethod.getInstance());
		}
	}

	/**
	 * Display a system Toast using a custom ui view.
	 * 
	 * @param viewResId
	 *            the view res id
	 * @param duration
	 *            the duration
	 * @param gravity
	 *            the gravity
	 */
	public static void showCustomToast(final Context mContext,
			final int viewResId, final int duration, final int gravity) {
		final View layout = getLayoutInflater(mContext)
				.inflate(viewResId, null);

		final Toast toast = new Toast(mContext.getApplicationContext());

		toast.setGravity(gravity, 0, 0);
		toast.setDuration(duration);
		toast.setView(layout);
		toast.show();
	}

	public static LayoutInflater getLayoutInflater(final Context mContext) {
		return (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	/**
	 * Sets bitmap along with a text in a TextView
	 * <p>
	 * <img src="../../../../resources/bitmapinlined.png"><br/>
	 * </p>
	 * 
	 * @param tv
	 *            text view to setKey the text along with the image
	 * @param string
	 *            string to setKey to the textview
	 * @param bitmap
	 *            bitmap to insert in the string
	 * @param insertionIndex
	 *            index in the string at which to insert the <code>bitmap</code>
	 */
	public static void insertBitmapIntoTextView(final TextView tv,
			final String string, final Bitmap bitmap, final int insertionIndex) {
		final StringBuilder appender = new StringBuilder(string);
		appender.insert(insertionIndex, ' ');
		final SpannableStringBuilder builder = new SpannableStringBuilder(
				appender.toString());
		final ImageSpan imageSpan = new ImageSpan(bitmap);
		builder.setSpan(imageSpan, insertionIndex, insertionIndex + 1,
				Spanned.SPAN_INCLUSIVE_INCLUSIVE);
		tv.setText(builder, BufferType.SPANNABLE);
	}

	/**
	 * Perform the measure operation on the given view.
	 * <p>
	 * <b>Important</b>:setMeasuredDimension(w, h) should be called with the
	 * values returned from this method.
	 * <p/>
	 * 
	 * @param widthMeasureSpec
	 *            provided in onMeasure
	 * @param heightMeasureSpec
	 *            provided in onMeasure
	 * @param desiredWidth
	 *            of the view if its not explicitly provided.
	 * @param desiredHeight
	 *            of the view if its not explicitly provided.
	 * @return an array of size 2 containing the width [0] and height[1]
	 */
	public static int[] doOnMeasure(final int widthMeasureSpec,
			final int heightMeasureSpec, final int desiredWidth,
			final int desiredHeight) {

		final int widthMode = View.MeasureSpec.getMode(widthMeasureSpec);
		final int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
		final int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);
		final int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);

		int width;
		int height;

		// Measure Width
		if (widthMode == View.MeasureSpec.EXACTLY) {
			// Must be this size
			width = widthSize;
		} else if (widthMode == View.MeasureSpec.AT_MOST) {
			// Can't be bigger than...
			width = Math.min(desiredWidth, widthSize);
		} else {
			// Be whatever you want
			width = desiredWidth;
		}

		// Measure Height
		if (heightMode == View.MeasureSpec.EXACTLY) {
			// Must be this size
			height = heightSize;
		} else if (heightMode == View.MeasureSpec.AT_MOST) {
			// Can't be bigger than...
			height = Math.min(desiredHeight, heightSize);
		} else {
			// Be whatever you want
			height = desiredHeight;
		}
		return new int[] { width, height };
	}

	/**
	 * Increase the touchable area of the given view.
	 * <p>
	 * Source: <a
	 * href="http://stackoverflow.com/a/1343796/5210">http://stackoverflow
	 * .com/a/1343796/5210</a>
	 * </p>
	 * 
	 * @param view
	 *            The view that needs to have its touch area increased.
	 * @param byAmount
	 *            The amount of dp's to be added to all four sides of the view
	 *            hit purposes.
	 */
	public static void increaseTouchArea(final View view, final int byAmount) {
		increaseHitRectBy(byAmount, byAmount, byAmount, byAmount, view);
	}

	private static void increaseHitRectBy(final int top, final int left,
			final int bottom, final int right, final View delegate) {
		final View parent = (View) delegate.getParent();
		if (parent != null && delegate.getContext() != null) {
			parent.post(new Runnable() {
				// Post in the parent's message queue to make sure the parent
				// lays out its children before we call getHitRect()
				public void run() {
					final float densityDpi = delegate.getContext()
							.getResources().getDisplayMetrics().densityDpi;
					final Rect r = new Rect();
					delegate.getHitRect(r);
					r.top -= transformToDensityPixel(top, densityDpi);
					r.left -= transformToDensityPixel(left, densityDpi);
					r.bottom += transformToDensityPixel(bottom, densityDpi);
					r.right += transformToDensityPixel(right, densityDpi);
					parent.setTouchDelegate(new TouchDelegate(r, delegate));
				}
			});
		}
	}

	private static int transformToDensityPixel(final int regularPixel,
			final float densityDpi) {
		return (int) (regularPixel * densityDpi);
	}
}
