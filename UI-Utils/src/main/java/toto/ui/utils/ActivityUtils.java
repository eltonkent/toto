package toto.ui.utils;

import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

public class ActivityUtils {

	public static void unlockOrientation(final Activity activity) {
		if (activity == null)
			return;
		activity.setRequestedOrientation(-1);
	}

	public static void lockOrientation(final Activity activity) {
		if (activity == null)
			return;
		activity.setRequestedOrientation(0);
	}

	public static void changeToLandscape(final Activity activity) {
		if (isApiHigher9()) {
			activity.setRequestedOrientation(6);
			return;
		}
		activity.setRequestedOrientation(0);
	}

	public static void changeToPortrait(final Activity activity) {
		if (isApiHigher9()) {
			activity.setRequestedOrientation(7);
			return;
		}
		activity.setRequestedOrientation(1);
	}

	public static void keepScreenOn(final Activity pActivity) {
		pActivity.getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	/**
	 * Set screen brightness. (Value between 0 and 1)
	 * 
	 * @param pActivity
	 * @param pScreenBrightness
	 *            [0..1]
	 */
	public static void setScreenBrightness(final Activity pActivity,
			final float pScreenBrightness) {
		final Window window = pActivity.getWindow();
		final WindowManager.LayoutParams windowLayoutParams = window
				.getAttributes();
		windowLayoutParams.screenBrightness = pScreenBrightness;
		window.setAttributes(windowLayoutParams);
	}

	public static void requestFullscreen(final Activity pActivity) {
		final Window window = pActivity.getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
		window.requestFeature(Window.FEATURE_NO_TITLE);
	}

	private static boolean isApiHigher9() {
		return Build.VERSION.SDK_INT >= 9;
	}
}
