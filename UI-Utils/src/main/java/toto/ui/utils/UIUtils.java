/*******************************************************************************
 * Mobifluence Interactive 
 * mobifluence.com (c) 2013 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Mobifluence Interactive and its suppliers, if any.  The intellectual
 * and technical concepts contained herein are proprietary to
 * Mobifluence Interactive and its suppliers  are protected by 
 * trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Mobifluence Interactive.
 * 
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of the binary distribution.
 * API Design - Elton Kent
 ******************************************************************************/
package toto.ui.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Looper;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.Toast;

public class UIUtils {
	public abstract static class ClickSpan extends ClickableSpan {

	}

	/**
	 * Returns the scaled size for different resolutions.
	 * 
	 * @param fntSize
	 * @return
	 */
	public static int getDensityIndependentSize(final int size,
			final Context ctx) {
		final int density = ctx.getResources().getDisplayMetrics().densityDpi;
		if (160 == density) {
			final int newSize = (int) (size / (1.5));
			return newSize;
		} else if (density == 120) {
			final int newSize = (size / (2));
			return newSize;
		}

		return size;
	}

	/**
	 * DIP to pixels.
	 * 
	 * @param ctx
	 * @param dip
	 * @return {@link #fromDip(android.content.Context, int)}
	 */
	public static int dip2Px(final Context ctx, final float dip) {
		return (int) (dip * ctx.getResources().getDisplayMetrics().density + 0.5f);
	}

	/**
	 * Gets the optimal number of columns that can be used for the given width
	 * 
	 * @param drawable_width
	 *            the drawable_width in dip
	 * @return the screen optimal columns
	 */
	public static int getScreenOptimalColumns(final Context mContext,
			final int drawable_width) {
		final DisplayMetrics metrics = mContext.getResources()
				.getDisplayMetrics();
		final double a = (double) metrics.widthPixels
				/ (double) metrics.densityDpi; // 2.25
		final int b = (int) Math.ceil(a * 2.0); // 5

		if ((b * drawable_width) > metrics.widthPixels) {
			return metrics.widthPixels / drawable_width;
		}

		return Math.min(Math.max(b, 3), 10);
	}

	/**
	 * Get the optimal number of rows for the given height of an item
	 * 
	 * @param mContext
	 * @param drawable_height
	 *            in dip
	 * @return
	 */
	public static int getScreenOptimalRows(final Context mContext,
			final int drawable_height) {
		final DisplayMetrics metrics = mContext.getResources()
				.getDisplayMetrics();
		final double a = (double) metrics.heightPixels
				/ (double) metrics.densityDpi; // 2.25
		final int b = (int) Math.ceil(a * 2.0); // 5

		if ((b * drawable_height) > metrics.heightPixels) {
			return metrics.widthPixels / drawable_height;
		}

		return Math.min(Math.max(b, 3), 10);
	}

	/**
	 * Convert to pix from DPI.
	 * 
	 * @param context
	 * @param dips
	 * @return {@link #dip2Px(android.content.Context, float)}
	 */
	public static int fromDip(final Context context, final int dips) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				dips, context.getResources().getDisplayMetrics());
	}

	/**
	 * Check if the device is a tablet
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isTablet(final Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	public static boolean isGoogleTV(final Context context) {
		return context.getPackageManager().hasSystemFeature(
				"com.google.android.tv");
	}

	/**
	 * Shows a toast to the user - can be called from any thread, toast will be
	 * displayed using the UI-thread.
	 * <p>
	 * The important thing about the delayed aspect of the UI-thread code used
	 * by this method is that it may actually run <em>after</em> the associated
	 * activity has been destroyed - so it can not keep a reference to the
	 * activity. Calling methods on a destroyed activity may throw exceptions,
	 * and keeping a reference to it is technically a short-term memory-leak:
	 * http
	 * ://developer.android.com/resources/articles/avoiding-memory-leaks.html
	 * 
	 * @param activity
	 *            Activity to show it on
	 * @param message
	 *            Toast message
	 * @param duration
	 *            duration of the toast
	 */
	public static void toastOnUiThread(final Activity activity,
			final String message, final int duration) {
		final Application application = activity.getApplication();
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(application, message, duration).show();
			}
		});
	}

	/**
	 * Check if the calling method is on the main thread.
	 * 
	 * @return
	 */
	public static boolean isInMainThread() {
		return Looper.getMainLooper().equals(Looper.myLooper());
	}

	public static <T> T callInMainThread(final Callable<T> call)
			throws Exception {
		if (isInMainThread())
			return call.call();
		else {
			final FutureTask<T> task = new FutureTask<T>(call);
			new Handler().post(task);
			return task.get();
		}
	}

}
